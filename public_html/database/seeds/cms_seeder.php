<?php

use Illuminate\Database\Seeder;
use App\cms;
class cms_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        cms::create(['page_alias' => 'about-us', "status" => "Published"]);
        cms::create(['page_alias' => 'terms-services', "status" => "Published"]);
        cms::create(['page_alias' => 'privacy', "status" => "Published"]);
    }
}
