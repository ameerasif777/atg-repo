<?php

use Illuminate\Database\Seeder;
use App\trans_cms;

class trans_cms_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        trans_cms::create(["page_title" => "About Us", "page_content" => "About Us About Us About Us", "page_seo_title_lang" => "About UsAbout UsAbout Us", "page_meta_keyword" => "About UsAbout UsAbout Us", "page_meta_description" => "About About About About  Usvjoy@panaceatek.comjoy@panaceatek.com", 'cms_id_fk' => '1']);
        trans_cms::create(["page_title" => "Terms Services", "page_content" => "Terms Services Terms Services Terms Services", "page_seo_title_lang" => "Terms ServicesTerms ServicesTerms Services", "page_meta_keyword" => "Terms ServicesTerms ServicesTerms Services", "page_meta_description" => "Terms ServiesTerms ServiesTerms Servies", 'cms_id_fk' => '2']);
        trans_cms::create(["page_title" => "Privacy Policy", "page_content" => "Privacy Policy Privacy Policy Privacy Policy", "page_seo_title_lang" => "Privacy Policy Privacy Policy Privacy Policy", "page_meta_keyword" => "Privacy Policy Privacy Policy Privacy Policy", "page_meta_description" => "Privacy Policy Privacy Policy Privacy Policy", 'cms_id_fk' => '3']);
   
    }
}
