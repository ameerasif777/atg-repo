<?php

use Illuminate\Database\Seeder;
use App\privileges;
class privileges_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        privileges::create(['privilege_name' => 'Manage Email Template']);
        privileges::create(['privilege_name' => 'Manage Cms Pages']);
//        privileges::create(['privilege_name' => 'Manage contact Us']);
    }
}
