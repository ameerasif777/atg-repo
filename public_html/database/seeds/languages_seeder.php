<?php

use Illuminate\Database\Seeder;
use App\languages;
class languages_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        languages::create(['lang_name' => 'Afrikaans', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Arabic', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Armenian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Basque', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Belarusian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Bengali', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Bulgarian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Catalan', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Chinese', 'lang_icon' => '', 'status' => 'A']);
        languages::create(['lang_name' => 'Croatian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Czech', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Danish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Dutch', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'English', 'lang_icon' => '', 'status' => 'A']);
        languages::create(['lang_name' => 'Esperanto', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Estonian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Filipino', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Finnish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'French', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Galician', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Georgian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'German', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Greek', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Gujarati', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Haitian Creole', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Hebrew', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Hindi', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Hungarian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Icelandic', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Indonesian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Irish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Italian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Japanese', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Kannada', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Korean', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Latin', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Latvian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Lithuanian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Macedonian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Malay', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Maltese', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Norwegian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Persian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Polish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Portuguese', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Romanian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Russian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Serbian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Slovak', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Slovenian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Spanish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Swahili', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Swedish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Tamil', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Telugu', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Thai', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Turkish', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Ukrainian', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Urdu', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Vietnamese', 'lang_icon' => '', 'status' => 'I']);
        languages::create(['lang_name' => 'Welsh', 'lang_icon' => '', 'status' => 'I']);
    }
}
