<?php

use Illuminate\Database\Seeder;
use App\trans_global_settings;
class trans_global_settings_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        trans_global_settings::create(['global_name_id' => '1','value'=>'emma@panaceatek.com']);   
        trans_global_settings::create(['global_name_id' => '2','value'=>'p1036']);   
        trans_global_settings::create(['global_name_id' => '3','value'=>'emma@panaceatek.com']);   
        trans_global_settings::create(['global_name_id' => '4','value'=>'10']);   
        trans_global_settings::create(['global_name_id' => '5','value'=>'Y-m-d']);   
        trans_global_settings::create(['global_name_id' => '6','value'=>'77477641419']);   
    }
}
