<?php

use Illuminate\Database\Seeder;
use App\email_template_macros;
class email_temnplates_macros_seeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        email_template_macros::create(['macros' => '||USER_NAME||']);
        email_template_macros::create(['macros' => '||USER_EMAIL||']);
        email_template_macros::create(['macros' => '||PASSWORD||']);
        email_template_macros::create(['macros' => '||SITE_TITLE||']);
        email_template_macros::create(['macros' => '||LOGIN_LINK||']);
        email_template_macros::create(['macros' => '||ADMIN_NAME||']);
        email_template_macros::create(['macros' => '||ADMIN_ACTIVATION_LINK||']);
        email_template_macros::create(['macros' => '||SITE_PATH||']);
        email_template_macros::create(['macros' => '||ADMIN_EMAIL||']);
        email_template_macros::create(['macros' => '||ADMIN_LOGIN_LINK||']);
        email_template_macros::create(['macros' => '||SITE_URL||']);
        email_template_macros::create(['macros' => '||RESET_PASSWORD_LINK||']);
        email_template_macros::create(['macros' => '||USER||']);
        email_template_macros::create(['macros' => '||FIRST_NAME||']);
        email_template_macros::create(['macros' => '||LAST_NAME||']);
        email_template_macros::create(['macros' => '||VARIFICATION_LINK||']);
        email_template_macros::create(['macros' => '||ACTIVATION_LINK||']);
    }

}
