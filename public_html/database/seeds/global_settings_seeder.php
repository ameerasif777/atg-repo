<?php

use Illuminate\Database\Seeder;
use App\global_settings;
class global_settings_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        global_settings::create(['name' => 'site_email']);   
        global_settings::create(['name' => 'site_title']);   
        global_settings::create(['name' => 'contact_email']);   
        global_settings::create(['name' => 'per_page_record']);   
        global_settings::create(['name' => 'date_format']);   
        global_settings::create(['name' => 'phone_no']);   
    }
}
