<?php

use Illuminate\Database\Seeder;
use App\role;
class role_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        role::create(['role_name' => 'Super Admin']);
    }
}
