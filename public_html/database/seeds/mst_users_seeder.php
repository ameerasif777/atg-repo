<?php

use Illuminate\Database\Seeder;
use App\User;
class mst_users_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['email' => 'emma@panaceatek.com', "user_name" => "admin",
            "user_name" => "admin", "gender" => '1', "user_type" => 2, "user_country" => 1,
            "user_status" => 1, "email_verified" => 1, "first_name" => 'emma', "last_name" => 'emma',
            "password" => bcrypt('admin'),"role_id" => 1]);
    }
}
