<?php

use Illuminate\Database\Seeder;
use App\email_templates;
class email_templates_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        email_templates::create(['email_template_title' => 'admin-added', "email_template_subject" => "You have been added as admin user new", "email_template_content" => "Your account has been added", "created_by" => "1","lang_id" => '14']);
        email_templates::create(['email_template_title' => 'admin-updated', "email_template_subject" => "Your account has been updated", "email_template_content" => "Your account has been updated", "created_by" => "1" ,"lang_id" => '14']);
        email_templates::create(['email_template_title' => 'admin-deleted', "email_template_subject" => "Your account has been deleted", "email_template_content" => "Your account has been deleted", "created_by" => "1" ,"lang_id" => '14']);
        email_templates::create(['email_template_title' => 'admin-email-updated', "email_template_subject" => "Verify updated Account", "email_template_content" => "Verify updated Account", "created_by" => "1","lang_id" => '14']);

    }
}
