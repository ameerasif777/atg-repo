<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_users', function (Blueprint $table) {

            $table->increments('id');

            $table->string('user_name');

            $table->string('email')->unique();

            $table->string('password', 60);

            $table->string('profile_picture', 60);
            $table->string('area', 60);
            $table->text('tagline');
            $table->text('profession');
            $table->text('location');
            $table->text('hometown');

            $table->enum('gender', array(0, 1));

            $table->enum('user_type', array(1, 2, 3,4));

            $table->enum('user_status', array(0, 1, 2));

            $table->string('activation_code', 50);

            $table->string('reset_password_code', 50);

            $table->enum('email_verified', array(0, 1));

            $table->dateTime('last_login');

            $table->string('fb_id');

            $table->enum('send_email_notification', array(0, 1));

            $table->string('ip_address');

            $table->integer('user_country')->index()->unsigned();

            $table->integer('user_state')->index()->unsigned();

            $table->integer('user_city')->index()->unsigned();

            $table->string('user_birth_date');

            $table->integer('user_age');
            $table->integer('mob_no');
            $table->integer('phone_no');
            $table->float('height');
            $table->float('weight');

            $table->string('first_name');

            $table->string('last_name');

            $table->enum('is_member', array(0, 1));

            $table->datetime('last_logout_time');
            $table->date('date_of_birth');

            $table->string('about_me');
            
            $table->integer('role_id');

//            $table->foreign('user_country')->references('id')->on('mst_countries');

            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_users');
    }
}
