<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeetupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('mst_meetup', function (Blueprint $table) {

            $table->increments('meetup_id');
            $table->string('title',100);
            $table->string('description',255);
            $table->string('image',100);
            $table->string('location',100);
            $table->string('address',200);
            $table->string('city',50);
            $table->string('fees',10);
            $table->string('lat',50);
            $table->string('lap',50);
            $table->string('no_of_ticket',11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
              Schema::drop('mst_meetup');
    }
}
