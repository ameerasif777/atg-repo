<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransAtricle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
  Schema::create('trans_article', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('article_id_fk');
            $table->integer('group_id_fk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::create('trans_article');
    }
}
