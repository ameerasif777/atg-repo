<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstEmailTemplateMacros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_email_template_macros', function (Blueprint $table1) {

            $table1->increments('id');

            $table1->string('macros', 100);

            $table1->string('value', 100);

            $table1->dateTime('created_date');

            $table1->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_email_template_macros');
    }
}
