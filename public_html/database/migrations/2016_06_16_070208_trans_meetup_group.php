<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransMeetupGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('trans_meetup_group', function (Blueprint $table) {

            $table->increments('meetup_group_id');
            $table->integer('meetup_id_fk');
            $table->integer('group_id_fk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('trans_meetup_group');
    }
}
