<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_faqs', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('category_id')->unsigned();

            $table->index('category_id');

            $table->string('question', 255);

            $table->longText('answer');

            $table->date('created_on');

            $table->integer('lang_id');

            $table->enum('status', array('Active', 'Inactive'));

            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('mst_faq_categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_faqs');
    }
}
