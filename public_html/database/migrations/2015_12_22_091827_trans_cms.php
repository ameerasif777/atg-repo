<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransCms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_cms', function (Blueprint $table1) {

            $table1->increments('id');

            $table1->string('page_title', 225);

            $table1->longText('page_content');

            $table1->string('page_seo_title_lang', 500);

            $table1->string('page_meta_keyword', 500);

            $table1->string('page_meta_description', 500);

            $table1->integer('cms_id_fk')->unsigned();

            $table1->index('cms_id_fk');

            $table1->integer('lang_id');

            $table1->timestamps();

            $table1->foreign('cms_id_fk')->references('id')->on('mst_cms')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_cms');
    }
}
