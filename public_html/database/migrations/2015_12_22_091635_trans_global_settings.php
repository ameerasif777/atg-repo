<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransGlobalSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_global_settings', function (Blueprint $table1) {

            $table1->increments('id');

            $table1->integer('global_name_id')->unsigned();

            $table1->index('global_name_id');

            $table1->string('value', 1000);

            $table1->integer('lang_id');

            $table1->timestamps();

            $table1->foreign('global_name_id')->references('id')->on('mst_global_settings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_global_settings');
    }
}
