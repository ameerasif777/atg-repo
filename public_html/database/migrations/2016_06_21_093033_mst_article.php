<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_article', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id_fk');
            $table->string('title',200);
            $table->text('comment',255);
            $table->text('tags',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('mst_article');
    }
}
