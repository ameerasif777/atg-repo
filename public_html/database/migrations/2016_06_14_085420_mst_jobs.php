<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstJobs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('mst_jobs', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id_fk');
            $table->string('job_title');
            $table->string('job_location');
            $table->string('job_description');
            $table->integer('year_of_experience');
            $table->string('preferred_qualification');
            $table->string('role');
            $table->string('skill');
            $table->string('tags');
            $table->string('sub_groups');
            $table->string('external_link_to_apply');
            $table->string('functional_area');
            $table->enum('employment_type', array(1, 2, 3, 4));
            $table->dateTime('application_deadline');
            $table->float('salary_range');
            $table->string('phone_number');
            $table->string('email_address');
            $table->string('website');
            $table->enum('status', array(0, 1));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('mst_jobs');
    }

}
