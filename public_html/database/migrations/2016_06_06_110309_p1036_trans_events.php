<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class P1036TransEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_events', function (Blueprint $table) {

            $table->increments('event_id');
            $table->string('title',60);
            $table->string('contact_name',60);
            $table->string('contact_number',60);
            $table->string('email_address',100);
            $table->dateTime('event_time');
            $table->text('agenda');
            $table->text('description');
            $table->string('guest',5);
            $table->float('cost');
            $table->dateTime('last_date');
            $table->string('age_group',5);
            

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_events');
    }
}
