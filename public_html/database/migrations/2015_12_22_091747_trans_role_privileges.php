<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransRolePrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_role_privileges', function (Blueprint $table1) {

            $table1->increments('id');

            $table1->integer('role_id')->unsigned();

            $table1->index('role_id');

            $table1->integer('privilege_id')->unsigned();

            $table1->index('privilege_id');

            $table1->timestamps();

            $table1->foreign('role_id')->references('id')->on('mst_role')->onDelete('cascade')->onUpdate('cascade');
            $table1->foreign('privilege_id')->references('id')->on('mst_privileges')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_role_privileges');
    }
}
