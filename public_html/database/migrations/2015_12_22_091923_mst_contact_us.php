<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstContactUs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_contact_us', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name', 500);

            $table->string('subject', 255);

            $table->string('mail_id', 255);

            $table->string('message', 5000);

            $table->enum('reply_status', array(0, 1));

            $table->dateTime('date');

            $table->integer('user_id_fk')->unsigned();

            $table->index('user_id_fk');

            $table->timestamps();

            $table->foreign('user_id_fk')->references('id')->on('mst_users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_contact_us');
    }
}
