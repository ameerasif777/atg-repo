<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstGroup extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('mst_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('group_name', 225);
            $table->text('group_description');
            $table->enum('status', array('0', '1'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
         Schema::drop('mst_group');
    }

}
