<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactFeedbackReply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_contact_feedback_reply', function (Blueprint $table1) {

            $table1->increments('id');

            $table1->integer('contact_id')->unsigned();

            $table1->index('contact_id');

            $table1->string('message_to', 100);

            $table1->string('message_from_name', 100);

            $table1->string('essage_from_email', 100);

            $table1->string('message_subject', 500);

            $table1->text('message_body', 500);

            $table1->date('reply_date');

            $table1->timestamps();

            $table1->foreign('contact_id')->references('id')->on('mst_contact_us')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trans_contact_feedback_reply');
    }
}
