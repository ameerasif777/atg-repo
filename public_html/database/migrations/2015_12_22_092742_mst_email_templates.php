<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MstEmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mst_email_templates', function (Blueprint $table) {

            $table->increments('id');

            $table->string('email_template_title', 100);

            $table->string('email_template_subject', 100);

            $table->text('email_template_content', 100);

            $table->integer('lang_id');

            $table->integer('created_by');

            $table->dateTime('date_created');

            $table->dateTime('date_updated');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mst_email_templates');
    }
}
