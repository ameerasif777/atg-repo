<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransUserApplicantToJob extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('trans_user_applicant_to_job', function (Blueprint $table) {
            $table->increments('applicant_id');
            $table->integer('user_id_fk');
            $table->integer('job_id_fk');
            $table->integer('transaction_id');
            $table->date('application_date');
            $table->enum('status', array(0, 1));
            $table->date('payment_date');
            $table->integer('payment_amount');
            $table->integer('resume_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('trans_user_applicant_to_job');
    }

}
