<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_blogs', function (Blueprint $table) {
           $table->increments('id');
            $table->integer('blog_id_fk');
            $table->integer('tag_id_fk');
            $table->string('tags',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        Schema::drop('trans_blogs');
    }
}
