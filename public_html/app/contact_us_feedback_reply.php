<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact_us_feedback_reply extends Model {

    protected $table = 'trans_contact_feedback_reply';
    protected $fillable = array('contact_id','message_from_name','message_to');

//    public function values() {
//        return $this->hasMany('App\contact_us_feedback_reply', 'contact_id');
//    }

}
