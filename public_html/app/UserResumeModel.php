<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserResumeModel extends Model
{
    protected $table = 'trans_users_resume';
    protected $fillable = array('resume_id','user_id_fk','uploaded_path');
}
