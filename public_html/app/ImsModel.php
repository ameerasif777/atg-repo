<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImsModel extends Model {
    
    protected $table = 'mst_ims';
    protected $fillable = array('title','user_id_fk');
}
