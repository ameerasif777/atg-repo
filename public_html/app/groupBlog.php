<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupBlog extends Model
{
    public $table='trans_blog_group';
    protected $fillable = array('blog_group_id','group_id_fk','blog_id_fk');
}
