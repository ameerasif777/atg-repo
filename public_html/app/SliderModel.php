<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderModel extends Model
{
    protected $table = 'mst_sliders';
    protected $fillable = array('id','slider_title','slider_description','status','created_at','slider_banner_image');
}
