<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable,
		Authorizable,
		CanResetPassword,
		HasPushSubscriptions,
		Notifiable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mst_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['password_change_security_code', 'reset_password_code', 'send_email_notification', 'user_birth_date', 'user_age', 'first_name', 'ip_address', 'is_member', 'fb_id', 'last_login', 'activation_code', 'user_name', 'user_type', 'gender', 'user_country', 'user_city', 'user_birth_date', 'user_age', 'about_me', 'user_status', 'profile_picture', 'first_name', 'last_name', 'email', 'password', 'role_id', 'email_verified', 'created_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function getUserEmailSchedule() {
        return $this->hasOne('App\UserEmailSchedule','user_id','id');
    }
    
    public function userGroup() {
        return $this->hasMany('App\GroupUser','user_id_fk','id');
    }
    
    public function from_user_connections() {
        return $this->hasMany('\App\UserConnection', 'from_user_id');
    }

    public function to_user_connections() {
        return $this->hasMany('\App\UserConnection', 'to_user_id');
    }
}
