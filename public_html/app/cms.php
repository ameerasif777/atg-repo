<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cms extends Model
{
    protected $table = 'mst_cms';
    protected $fillable = array('page_alias','status');
    public function values()
    {
        return $this->hasMany('App\trans_cms','cms_id_fk');     
    }
}
