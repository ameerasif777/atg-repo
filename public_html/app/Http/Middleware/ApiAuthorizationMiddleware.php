<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthorizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('X-Authorization')) {
            if ($request->header('X-Authorization') !== config('app.atg_api_authorization_key')) {
                return response()->json([
                    'error' => true,
                    'description' => 'wrong API key'
                ]);
            }
        } else {
            return response()->json([
                'error' => true,
                'description' => 'missing API key'
            ]);
        }

        return $next($request);
    }
}
