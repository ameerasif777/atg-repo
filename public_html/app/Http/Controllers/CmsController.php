<?php

namespace App\Http\Controllers;

use DB;
use App\cms;
use App\User;
use App\trans_cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\role_privileges;
use App\CommonModel;
use App\CmsModel;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;;

class CmsController extends Controller {
    /* construction function used to load all the models used in the controller.	   */

    public function __construct() {
        
    }

    public function cmsPage($page_name) {
        $arr_cms = array();
        $arr_cms = cms::with('values')->where('page_alias', $page_name)->first();
        return view('Frontend.cms.cms-page', ['arr_cms' => $arr_cms, 'site_title' => $page_name]);
    }

    /* function to list all the states */

    public function listCMS() {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('2', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        $lang_id = 14;
        $get_cms_list = \App\cms::with('values')->get();
        $site_title = 'Manage CMS';
        return view('Backend.cms.cms-list', ['get_cms_list' => $get_cms_list, 'site_title' => $site_title]);
    }

    /* function for edi temail template */

    public function editCMS($edit_id = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
         /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('2', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        $arr_cms_details = cms::with('values')->find($edit_id);
        $site_title = 'Edit CMS Page';
        return view('Backend.cms.edit-cms', ['arr_cms_details' => $arr_cms_details, 'site_title' => $site_title]);
    }

    public function editCMSPost(Request $request) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $edit_id = $request->input('id', '');

        if (!empty($edit_id) && $edit_id != 0) {
            $lang_id = 14;
            //updating data in cms tables.
            $update = trans_cms::where(array('cms_id_fk' => $edit_id, 'lang_id' => $lang_id))->update([ "page_content" => $request->input('cms_content', ''),
                "page_title" => $request->input('cms_page_title', ''),
                "page_meta_keyword" => $request->input('cms_page_meta_keywords', ''),
                "page_meta_description" => $request->input('cms_page_meta_description', ''),
                "page_seo_title_lang" => $request->input('cms_page_seo_title', '')]);
        }
        Session::flash('message', 'Cms info has been updated successfully.');
        return redirect('admin/cms');
        exit;
    }

    public function editorImageUpload() {
        $data = Input::all();
        if ($_FILES['upload']['name'] != '') {

            $absolute_path = $this->absolutePath();
            if ($data['upload'] != '') {
                $file_type = explode('/', $_FILES['upload']['type']);
                if ($file_type[0] != 'image') {
                    echo "<script type='text/javascript'>alert('Sorry!!!.Please upload image only.');</script>";
                    return false;
                } else {
                    $file_data = $data['upload'];
                    $file = $_FILES['upload']['name'];
                    $destinationPath = $absolute_path = $this->absolutePath() . 'cmsfiles/';
                    $destinationPath1 = $absolute_path = $this->absolutePath() . 'cmsfiles/thumbs/';
                    $new_filename = time() . '.' . $file_data->getClientOriginalExtension();
                    $extension = $file_data->getClientOriginalExtension();
                    $thumb_path = public_path($destinationPath1 . $new_filename);
                    //@\Image::make($file_data->getRealPath())->save($thumb_path);
                    $upload_success = $file_data->move($destinationPath, $new_filename);
                }
                ?>
                <script  type="text/javascript">window.parent.CKEDITOR.tools.callFunction(1, "<?php echo url('/'); ?>/cmsfiles/<?php echo $new_filename; ?>", '');</script>
                <?php
            }
        } else {
            echo "false";
            exit;
        }
    }

    public function absolutePath($path = '') {
        $abs_path = str_replace('system/', $path, base_path());
        //Add a trailing slash if it doesn't exist.
        $abs_path = preg_replace("#([^/])/*$#", "\\1/", $abs_path);
        return $abs_path;
    }

    public function cmsDetails($page_name) {
        $commonModel = new CommonModel();
        $CmsModel = new CmsModel();
        //Getting Common data
        $data = $commonModel->commonFunction();
        $data['global'] = $commonModel->getGlobalSettings();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        
        $lang_id = '17';
        $condition = array('A.page_alias' => $page_name, 'A.status' => 'Published');
        $arr_cms = $CmsModel->getCmsPageDetailsFront($condition);
//        echo '<pre>'; print_r($arr_cms);die;
        if (count($arr_cms) == 0)
            return redirect(url('/'));
        
        $data['header'] = array(
            "title" => $arr_cms[0]->page_title,
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.cms.cms-page')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('arr_cms', $arr_cms);
    }

}
