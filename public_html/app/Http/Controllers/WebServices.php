<?php

namespace App\Http\Controllers;

use App\agendaEventSessionTime;
use App\agendaSessionTime;
use App\Answer;
use App\ApplicantToJobModel;
use App\articleCommentModel;
use App\ArticleModel;
use App\Comment;
use App\commentMeetup;
use App\CommonModel;
use App\educationCommentModel;
use App\education_model;
use App\email_templates;
use App\email_template_macros;
use App\Event;
use App\eventAgenda;
use App\eventCommentModel;
use App\eventCost;
use App\eventGuest;
use App\Filter;
use App\FollowerUser;
use App\group;
use App\groupArticle;
use App\groupEducation;
use App\groupEvent;
use App\groupMeetup;
use App\groupQuestion;
use App\GroupUser;
use App\Http\Controllers\Controller;
use App\JobGroupModel;
use App\JobModel;
use App\meetupAgenda;
use App\meetupCommentModel;
use App\meetupCost;
use App\meetupGuest;
use App\meetup_model;
use App\questionCommentModel;
use App\QuestionModel;
use App\saveUserPost;
use App\SliderModel;
use App\Traits\MarkAllNotificationsAsRead;
use App\Traits\Post\PostActions;
use App\Traits\sendsignupmail;
use App\Traits\UserDetails;
use App\Traits\JobTrait;
use App\transmeetuprecurringday;
use App\trans_global_settings;
use App\User;
use App\UserConnection;
use App\UserResumeModel;
use DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use App\Traits\commentsTrait;
use Illuminate\Support\Facades\Storage;

class WebServices extends Controller {
	use PostActions;
	use MarkAllNotificationsAsRead;
	use UserDetails;
	use sendsignupmail;
	use JobTrait;
	use commentsTrait;

	/**
	 * @SWG\Info(title="Across The World - API", version="0.1")
	 */
    function fill_br($desc){
        return ('<p>'.preg_replace('/\n/','</p><p>',$desc).'</p>');
    }
    
	function convertToObject($array) {
		$object = new \stdClass();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$value = convertToObject($value);
			}
			$object->$key = $value;
		}
		return $object;
	}

	//android push notification
	public function send_notification($registration_id, $message = '') {
//    public function send_notification() {
		//        $request = Input::all();
		//        $registration_id = $request['registration_id'];
		//        $message = $request['message'];
		//using fcm
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => array($registration_id),
			'data' => array("message" => $message),
		);
		$fields = json_encode($fields);
		$headers = array(
//            'Authorization: key=' . env('GOOGLE_API_KEY'),
			'Authorization: key=AIzaSyD_3Vkt_NtklQGQ9CwEDSbPXcsOGKIQh_k',
			'Content-Type: application/json',
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$result = curl_exec($ch);
//        echo '<pre>';print_r($result);die;
		// Close connection
		curl_close($ch);
		return $result;
//        echo $result;
	}

//ios_notificaton
	function ios_notificaton($registrationId, $message) {
		$fcmApiKey = 'AAAAo_NpfIQ:APA91bF3zwOpc8skjySGSN5xDyu_sJ28cFgkp2UsKkt02zl-f4cNkJQHp9qwfgrDdVFAKe5SZbR8aV9zxTOf45DYMx25M-Z6s6lgXmkLQ1UgT8dY6zYZkLCl74FjRUwcCZBSC4Jc43zZ'; //App API Key(This is google cloud messaging api key not web api key)
		$url = 'https://fcm.googleapis.com/fcm/send'; //Google URL
		//Fcm Device ids array
		//        $registrationId = "eA31_FTuPZk:APA91bF7KBAqxaRLUqKLSKinyJile9ejZDTU3nVNNFl-C-yQVIGcsBSm6pST14EIbXzraIURket0unsUO8wOV5pGL4t9qmUSbxJH1fskR_7dpgHLJXZc4zCs_AYY8fhHcquzTSo5w040";
		// prepare the bundle
		$title = "atgworld";
		//Body of the Notification.
		//        $body = "Bear island knows no king but the king in the north, whose name is stark.";
		//        $message = "Bear island knows no king but the king in the north, whose name is stark.";
		//Creating the notification array.
		$notification = array('title' => $title, 'text' => $message, 'flag' => '1', 'order_id' => '12466564');

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('to' => $registrationId, 'notification' => $notification, 'priority' => 'high');

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);

		$headers = array(
			'Authorization: key=' . $fcmApiKey,
			'Content-Type: application/json',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		// Execute post
		$result = curl_exec($ch);
		// if ($result === FALSE) {
		//     die('Curl failed: ' . curl_error($ch));
		// }
		// Close connection
		curl_close($ch);
		echo $result;
		$array_json_return = array("success" => "1");
		return json_encode($array_json_return);
//        $array_json_return = array("error_code" => '0', "msg" => "Success", "Response" => $result);
		//        return json_encode($array_json_return);
	}

	//test ios notification
	function testIOSNotificaton() {
		$ios_fcmApiKey = '';
//        $ios_fcmApiKey = $data['global']['ios_push_notification_server_key'];
		$fcmApiKey = 'AAAAo_NpfIQ:APA91bF3zwOpc8skjySGSN5xDyu_sJ28cFgkp2UsKkt02zl-f4cNkJQHp9qwfgrDdVFAKe5SZbR8aV9zxTOf45DYMx25M-Z6s6lgXmkLQ1UgT8dY6zYZkLCl74FjRUwcCZBSC4Jc43zZ'; //App API Key(This is google cloud messaging api key not web api key)
		$fcmApiKey = $fcmApiKey; //App API Key(This is google cloud messaging api key not web api key)
		$url = 'https://fcm.googleapis.com/fcm/send'; //Google URL
		//Fcm Device ids array
		$registrationId = "cyXWlk2ZVxg:APA91bGaVkppfKMPjljlr1i0KePUYg0fin0c__SUd6Knn_QquVZO4qQLAXByNp1HG5DhovspNh9JxLW6S-m2HwctpwfpM7J9-PsnH9dmP61iIBWvzCH6IxTfdYc8qYPfxQwgnUYp8__N";
//        $registrationId = "e8a2cGvjgsE:APA91bEcrswvTgzx1P2IjPeo4kpuAoHeY1aWhIULjPg0S4fbn4JGEILhMb9cYJGA5wWwWrdEaie4hgwFs3ZaakF2mIHlVDi2hNUEZbUOfxt7P6kSzlci1d3sMz82MlFUJDebhHkjIpAR";
		// prepare the bundle
		$title = "UPGLEE";
		//Body of the Notification.
		//        $body = "Bear island knows no king but the king in the north, whose name is stark.";
		$message = "Bear island knows no king but the king in the north, whose name is stark.";
		//Creating the notification array.
		$notification = array('title' => $title, 'text' => $message, 'flag' => '1', 'order_id' => '12466564');

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('to' => $registrationId, 'notification' => $notification, 'priority' => 'high');

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);

		$headers = array(
			'Authorization: key=' . $fcmApiKey,
			'Content-Type: application/json',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		// Execute post
		$result = curl_exec($ch);
		// if ($result === FALSE) {
		//     die('Curl failed: ' . curl_error($ch));
		// }
		// Close connection
		curl_close($ch);
		echo $result;
		die;
		$array_json_return = array("error_code" => '0', "msg" => "Success", "Response" => $result);
		return json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-register-user",
	 *   summary="Register the user using email",
	 *   tags={"User Signup Signin"},
	 *   description="",
	 *   operationId="signup",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="first_name",
	 *       in="formData",
	 *       description="First Name",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="last_name",
	 *       in="formData",
	 *       description="Last Name",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="User Email",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="password",
	 *       in="formData",
	 *       description="Password",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="group_id",
	 *       in="formData",
	 *       description="Group Id(s) e.g.83,86,108 Can be left blank",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="device_name",
	 *       in="formData",
	 *       description="Device Name - 0:Android , 1:iOS",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="registration_id",
	 *       in="formData",
	 *       description="Registration Id (Not Sure)",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="if success the output -> ('error_code' => '0', 'msg' => 'Verification email sent to your email.', 'user_email' =>'user email ID'),      else if some internal errror then output ->('error_code' => '1', 'msg' => 'error'),               else if user already exist then output ->('error_code' => '2', 'msg' => 'Already Register'),                   else if no input data then output ->('error_code' => '3', 'msg' => 'No input data')")
	 * )
	 */
	protected function wsRegistration() {
		$request = Input::all();

		if (count($request) > 0) {
			$user_data = User::where('email', $request['email'])->first();

			if (count($user_data) == 0) {
				if (isset($request['first_name']) != '' && isset($request['last_name']) != '' && isset($request['email']) != '' && isset($request['password']) != '') {

					$salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
					$hash_password = crypt($request['password'], '$2y$12$' . $salt);

					$activation_code = time();

					$user = new User();
					$user->first_name = $request['first_name'];
					$user->last_name = $request['last_name'];
					$user->email = $request['email'];
					$user->signup_step = '3';
					$user->role_id = '0';
					$user->from_mobile_registration = '1';
					$user->user_status = '1';
					$user->user_type = '1';
					$user->password = $hash_password;
					$user->activation_code = $activation_code;
					if (isset($request['device_name'])) {
						$user->device_name = $request['device_name'];
					}
					$user->save();
					$user_id = $user->id;
					if (isset($request['group_id'])) {
						$group_ids = explode(',', $request['group_id']);
						if ($group_ids[0] != '') {
							foreach ($group_ids as $group_id) {
								$groupuser = new GroupUser();
								$groupuser->group_id_fk = $group_id;
								$groupuser->user_id_fk = $user_id;
								$groupuser->status = '1';
								$groupuser->created_at = date("Y-m-d H:i:s");
								$groupuser->save();
							}
						}
					}

					/* Activation link  */
					$site_email = trans_global_settings::where('id', '1')->first();

					$site_title = trans_global_settings::where('id', '2')->first();

					$activation_link = '<a href="' . url('/') . '/user/account-activate/' . $activation_code . '">Activate Account</a>';
					/*                     * ******** */

					$user_details = User::where('email', $request['email'])->first();
					$array_json_return = array('error_code' => '0', 'msg' => "Verification email sent to your email.", 'user_details' => $user_details);

                    /* setting reserved_words for email content */
					$macros_array_detail = array();
					$macros_array_detail = email_template_macros::all();

					$macros_array = array();
					foreach ($macros_array_detail as $row) {
						$macros_array[$row['macros']] = $row['value'];
					}
					$email_contents = email_templates::where(array('email_template_title' => 'registration-successful', 'lang_id' => '14'))->first();

					$email_contents = $email_contents;
					$content = $email_contents['email_template_content'];

					$name = "";
					$reserved_words = array();
					$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

					$reserved_arr = array(
						"||SITE_TITLE||" => stripslashes($site_title->value),
						"||SITE_PATH||" => $site_path,
						"||USER_NAME||" => $request['first_name'],
						"||USER_EMAIL||" => $request['email'],
						"||ADMIN_NAME||" => $name,
						"||PASSWORD||" => $request['password'],
						"||VERIFY_MY_EMAIL||" => $activation_link,
					);

					$reserved_words = array_replace_recursive($macros_array, $reserved_arr);

					foreach ($reserved_words as $k => $v) {
						$content = str_replace($k, $v, $content);
					}

					/* getting mail subect and mail message using email template title and lang_id and reserved works */
					$contents_array = array("email_conents" => $content);

					$data['site_email'] = $site_email->value;
					$data['site_title'] = $site_title->value;

					// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
					Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($user, $contents_array, $data, $email_contents, $name) {
						$message->from($data['site_email'], $data['site_title']);
						$message->to($user->email, $name)->subject($email_contents['email_template_subject']);
					});
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'error');
				}
			} else {
				$array_json_return = array('error_code' => '2', 'msg' => 'Already Register');
			}
		} else {
			$array_json_return = array('error_code' => '3', 'msg' => 'No input data');
		}

		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-login-user",
	 *   summary="Login usign email and password",
	 *   tags={"User Signup Signin"},
	 *   description="",
	 *   operationId="login",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="email id",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="password",
	 *       in="formData",
	 *       description="password",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="registration_id",
	 *       in="formData",
	 *       description="Registration Id (Deprecated - leave blank)",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="device_name",
	 *       in="formData",
	 *       description="Device Name - 0:Android , 1:iOS",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(response="200", description="if sucess then output ->('error_code':'0','msg':'Login success','user_details':all user details), else if inputs are invalid then output ->('error_code' => '1', 'msg' => 'Invalid'), if user doesnot exist output ->('error_code' => '2', 'msg' => 'Not Register'), else if no input then output ->('error_code' => '3', 'msg' => 'No input data')")
	 * )
	 */
	protected function wsLogin() {
		$request = Input::all();
		$device_name = $request['device_name'];

		if (count($request) > 0) {
			$user_data = User::where('email', $request['email'])->first();

			if (count($user_data) > 0) {

				if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
//                if ($request['email'] != '' && $request['password'] != '') {

					$user_data = User::where('email', $request['email'])->first();

					if ($user_data->fb_id != "") {
						$user_data->fb_login = "1";
					} else {
						$user_data->fb_login = "0";
					}
					if ($user_data->linkdin_id != "") {
						$user_data->linkdin_login = "1";
					} else {
						$user_data->linkdin_login = "0";
					}
					if ($user_data->twitter_id != "") {
						$user_data->twitter_login = "1";
					} else {
						$user_data->twitter_login = "0";
					}
					if ($user_data->google_id != "") {
						$user_data->google_login = "1";
					} else {
						$user_data->google_login = "0";
					}
					$activation_link = '<a style="color:springgreen; text-decoration:underline;" href="' . url('/') . '/again-send-mail"> Click here to receive the e-mail again.</a>';
					if (count($user_data) > 0) {

						$update_data = array("device_name" => $device_name);
						User::where('email', $request['email'])->update($update_data);
					}
					if(isset($user_data->cover_photo) && !empty($user_data->cover_photo))
					$user_data->cover_photo = config('cover_image_url').$user_data->id.'/thumb/'.$user_data->cover_photo;

					
					if(isset($user_data->profile_picture) && !empty($user_data->profile_picture))
					$user_data->profile_picture = config('profile_pic_url').$user_data->id.'/thumb/'.$user_data->profile_picture;

					$array_json_return = array('error_code' => '0', 'msg' => 'Login success', 'user_details' => $user_data);
				} else {

					$array_json_return = array('error_code' => '1', 'msg' => 'Invalid');
				}
			} else {
				$array_json_return = array('error_code' => '2', 'msg' => 'Not Register');
			}
		} else {
			$array_json_return = array('error_code' => '3', 'msg' => 'No input data');
		}
		echo json_encode($array_json_return);
	}

	/*     * *****SOCIAL MEDIA********* */

	/**
	 * @SWG\Post(
	 *   path="/ws-social-media",
	 *   summary="Register the user through Social Media",
	 *   tags={"User Signup Signin"},
	 *   description="",
	 *   operationId="signup",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="fb_id",
	 *       in="formData",
	 *       description="Facebook ID",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="twitter_id",
	 *       in="formData",
	 *       description="Twitter ID",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="google_id",
	 *       in="formData",
	 *       description="Google ID",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="first_name",
	 *       in="formData",
	 *       description="First Name",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="last_name",
	 *       in="formData",
	 *       description="Last Name",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="User Email",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="password",
	 *       in="formData",
	 *       description="Password",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="group_id",
	 *       in="formData",
	 *       description="Group Id(s) e.g.83,86,108 Can be left blank",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="registration_id",
	 *       in="formData",
	 *       description="Registration Id (deprecated - leave blank)",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="device_name",
	 *       in="formData",
	 *       description="Device Name 0:Android 1:iOS",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="0: New user Signup Success, 1:User Exists & signin success, 2:No groups joined by user, 4: Junk Data")
	 * )
	 */
	public function wsSocialMedia() {
		$request = Input::all();

		if (isset($request['fb_id']) && $request['fb_id'] != '' && $request['fb_id'] != '0') {
			$first_name = $request['first_name'];
			$last_name = $request['last_name'];
			$email = $request['email'];

			$arrUser = User::where("email", $email)->first();
			$arr_user_info = User::where('email', $email)->first();

			if (count($arr_user_info) > 0) {

				$arrUserLogin = User::where(array("email" => $email))->first();
				$arr_user_info = User::where('id', '=', $arrUserLogin->id)->first();
				$user_data = User::where('email', $email)->first();
				if (isset($request['registration_id'])) {
					$user_data->registration_id = $request['registration_id'];
				}
				if (isset($request['device_name'])) {
					$user_data->device_name = $request['device_name'];
				}
				$user_data->save();
				$get_groups = DB::table('trans_group_users')
					->where('user_id_fk', $arrUserLogin->id)
					->get();
				if (count($get_groups) > 0) {
					$array_json_return = array("error_code" => 1, "msg" => "Login success", "user_details" => $user_data);
				} else {
					$array_json_return = array("error_code" => 2, "msg" => "Groups not found.", "user_details" => $user_data);
				}
			} else {
				if (count($arrUser) > 0) {
					if ($arrUser->fb_id != "") {
						$arrUser["fb_login"] = "1";
					} else {
						$arrUser["fb_login"] = "0";
					}
					if ($arrUser->linkdin_id != "") {
						$arrUser["linkdin_login"] = "1";
					} else {
						$arrUser["linkdin_login"] = "0";
					}
					if ($arrUser->twitter_id != "") {
						$arrUser["twitter_login"] = "1";
					} else {
						$arrUser["twitter_login"] = "0";
					}
					if ($arrUser->google_id != "") {
						$arrUser["google_login"] = "1";
					} else {
						$arrUser["google_login"] = "0";
					}
					$array_json_return = array("error_code" => 0, "msg" => "Email already exist.", "user_data" => $arrUser);
				} else {
					if ($request['first_name'] != '' && $request['last_name'] != '') {
						$activation_code = time();

						$user = new User();
						$user->first_name = $request['first_name'];
						$user->last_name = $request['last_name'];
						$user->email = $request['email'];
						$user->fb_id = $request['fb_id'];
						$user->user_type = '1';
						$user->user_status = '1';
						$user->email_verified = '1';
						$user->activation_code = $activation_code;
						if (isset($request['registration_id'])) {
							$user->registration_id = $request['registration_id'];
						}
						if (isset($request['device_name'])) {
							$user->device_name = $request['device_name'];
						}
						$user->save();
						$user_id = $user->id;

						$user_info = User::find($user_id);
						$facebook_id = $request['fb_id'];
						if ($facebook_id != "") {

								$fb_link = 'https://graph.facebook.com/' . $facebook_id . '/picture?type=large';
								$imageData = file_get_contents($fb_link);
								$resp=$this->upload_pic(0,$user_id,$imageData);
								$resp1=json_decode($resp);
								$user_info->profile_picture = $resp1->profile_picture;
						}

						$user_info->save();

						if (isset($request['group_id'])) {
							$group_ids = explode(',', $request['group_id']);
							if ($group_ids[0] != '') {
								foreach ($group_ids as $group_id) {
									$groupuser = new GroupUser();
									$groupuser->group_id_fk = $group_id;
									$groupuser->user_id_fk = $user_id;
									$groupuser->status = '1';
									$groupuser->created_at = date("Y-m-d H:i:s");
									$groupuser->save();
								}
							}
						}
						$user_data = User::where('email', $request['email'])->first();
						$sendmailatuser = $this->sendmailsocialsignup($user->first_name . " " . $user->last_name, "Facebook", $user->email);
						$array_json_return = array('error_code' => 0, 'msg' => 'Register success', "user_details" => $user_data);
					}
				}
			}
		} else if (isset($request['twitter_id']) && $request['twitter_id'] != '' && $request['twitter_id'] != '0') {
			$arrUser = User::where("twitter_id", $request['twitter_id'])->first();
			if (count($arrUser) > 0) {

				$arrUserLogin = User::where("twitter_id", $request['twitter_id'])->first();
				$arr_user_info = User::where('id', '=', $arrUserLogin->id)->first();
				$get_groups = DB::table('trans_group_users')
					->where('user_id_fk', $arrUserLogin->id)
					->get();
				if (count($get_groups) > 0) {
					$array_json_return = array("error_code" => 1, "msg" => "Login success", "user_details" => $arrUserLogin);
				} else {
					$array_json_return = array("error_code" => 2, "msg" => "Groups not found.", "user_details" => $arrUserLogin);
				}

			} else {
				if ($request['user_name'] != '') {
					$activation_code = time();

					$user = new User();
					$user->first_name = $request['user_name'];
					$user->twitter_id = $request['twitter_id'];
					$user->user_type = '1';
					$user->user_status = '1';
					$user->activation_code = $activation_code;
					if (isset($request['registration_id'])) {
						$user->registration_id = $request['registration_id'];
					}
					if (isset($request['device_name'])) {
						$user->device_name = $request['device_name'];
					}
					$user->save();
					$user_id = $user->id;
					if (isset($request['group_id'])) {
						$group_ids = explode(',', $request['group_id']);
						if ($group_ids[0] != '') {
							foreach ($group_ids as $group_id) {
								$groupuser = new GroupUser();
								$groupuser->group_id_fk = $group_id;
								$groupuser->user_id_fk = $user_id;
								$groupuser->status = '1';
								$groupuser->created_at = date("Y-m-d H:i:s");
								$groupuser->save();
							}
						}
					}
					$arrUserLogin = User::where("twitter_id", $request['twitter_id'])->first();
					//$sendmailatuser=$this->sendmailsocialsignup($user->first_name." ".$user->last_name,"Twitter",$user->email);
					$array_json_return = array('error_code' => 0, 'msg' => 'Register success', "user_details" => $arrUserLogin);
				}
			}
		} else if (isset($request['google_id']) && $request['google_id'] != '' && $request['google_id'] != '0') {
			$first_name = $request['first_name'];
			$last_name = $request['last_name'];
			$email = $request['email'];

			$arrUser = User::where("email", $email)->first();
			$arr_user_info = User::where("google_id", $request['google_id'])->where('email', $email)->first();

			if (count($arr_user_info) > 0) {
				$arrUserLogin = User::where(array("email" => $email))->first();
				$arr_user_info = User::where('id', '=', $arrUserLogin->id)->first();
				$get_groups = DB::table('trans_group_users')
					->where('user_id_fk', $arrUserLogin->id)
					->get();

				$user_data = User::where('email', $email)->first();
				if (isset($request['registration_id'])) {
					$user_data->registration_id = $request['registration_id'];
				}
				if (isset($request['device_name'])) {
					$user->device_name = $request['device_name'];
				}
				$user_data->save();
				if (count($get_groups) > 0) {
					$array_json_return = array("error_code" => 1, "msg" => "Login success", "user_details" => $user_data);
				} else {
					$array_json_return = array("error_code" => 2, "msg" => "Groups not found.", "user_details" => $user_data);
				}
			} else {
				if (count($arrUser) > 0) {
					// TODO : This code is never hit. remove it for all three logins
					$arrUser = User::where("email", $email)->first();
					$array_json_return = array("error_code" => 0, "msg" => "Email already exist.", "user_details" => $arrUser);
				} else {
					if ($request['first_name'] != '' && $request['last_name'] != '') {
						$activation_code = time();

						$user = new User();
						$user->first_name = $request['first_name'];
						$user->last_name = $request['last_name'];
						$user->email = $request['email'];
						$user->google_id = $request['google_id'];
						$user->user_status = '1';
						$user->email_verified = '1';
						$user->user_type = '1';
						$user->activation_code = $activation_code;
						if (isset($request['registration_id'])) {
							$user->registration_id = $request['registration_id'];
						}
						if (isset($request['device_name'])) {
							$user->device_name = $request['device_name'];
						}
						$user->save();
						$user_id = $user->id;

						if (isset($request['group_id'])) {
							$group_ids = explode(',', $request['group_id']);
							if ($group_ids[0] != '') {
								foreach ($group_ids as $group_id) {
									$groupuser = new GroupUser();
									$groupuser->group_id_fk = $group_id;
									$groupuser->user_id_fk = $user_id;
									$groupuser->status = '1';
									$groupuser->created_at = date("Y-m-d H:i:s");
									$groupuser->save();
								}
							}
						}
						$user_data = User::where('email', $request['email'])->first();
						$sendmailatuser = $this->sendmailsocialsignup($user->first_name . " " . $user->last_name, "Google", $user->email);
						$array_json_return = array('error_code' => 0, 'msg' => 'Register success', "user_details" => $user_data);
					}
				}
			}
		} else {
			$array_json_return = array('error_code' => 4, 'msg' => 'Insert Data');
		}
		echo json_encode($array_json_return);
	}

	public function saveFBImage($file_content = '', $fb_id = '') {

		$data = $this->common_model->commonFunction();
		$imageData = file_get_contents($file_content);
		file_put_contents($this->common_model->absolutePath() . 'media/front/img/user-profile-pictures/' . $fb_id . ".jpg", $imageData);
		$this->createThumbnail($fb_id . '.jpg', '75', 'media/front/img/user-profile-pictures/', 'media/front/img/user-profile-pictures/thumb/');
		//        $this->createThumbnail($fb_id . '.jpg', '150', 'media/front/img/user-profile-pictures/', 'media/front/img/user-profile-pictures/large-thumb/');
		return $fb_id . ".jpg";
	}

	/*     * *****Resend Email********* */

	/**
	 * @SWG\Post(
	 *     path="/ws-resend-mail",
	 *     summary="resend mail to the user",
	 *   tags={"User Signup Signin"},
	 *   description="resending the email to the given user email-id",
	 *   operationId="resend-email",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_email",
	 *       in="formData",
	 *       description="User Email-id",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="")
	 * )
	 */

	public function wsResendMail() {
		$request = Input::all();
		$email = $request['user_email'];
		if (count($request) > 0) {
			$user_data = User::where('email', $email)->first();
			if (count($user_data) > 0) {

				$activation_code = time();
				DB::table('mst_users')
					->where('email', $user_data['email'])
					->update([
						'activation_code' => $activation_code,
						'created_at' => date("Y-m-d H:i:s"),
					]);

				/* Activation link  */
				$site_email = trans_global_settings::where('id', '1')->first();
				$site_title = trans_global_settings::where('id', '2')->first();

				$activation_link = '<a href="' . url('/') . '/user/account-activate/' . $activation_code . '">Activate Account</a>';

				/* setting reserved_words for email content */
				$macros_array_detail = array();
				$macros_array_detail = email_template_macros::all();

				$macros_array = array();
				foreach ($macros_array_detail as $row) {
					$macros_array[$row['macros']] = $row['value'];
				}
				$email_contents = email_templates::where(array('email_template_title' => 'registration-successful', 'lang_id' => '14'))->first();

				$email_contents = $email_contents;
				$content = $email_contents['email_template_content'];

				$name = "";
				$reserved_words = array();
				$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

				$reserved_arr = array(
					"||SITE_TITLE||" => stripslashes($site_title->value),
					"||SITE_PATH||" => $site_path,
					"||USER_NAME||" => $user_data['first_name'],
					"||USER_EMAIL||" => $user_data['user_email'],
					"||ADMIN_NAME||" => $name,
					"||PASSWORD||" => $user_data['user_password'],
					"||VERIFY_MY_EMAIL||" => $activation_link,
				);
				$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
				foreach ($reserved_words as $k => $v) {
					$content = str_replace($k, $v, $content);
				}
				/* getting mail subect and mail message using email template title and lang_id and reserved works */
				$contents_array = array("email_conents" => $content);
				$data = array();
				$data['site_email'] = $site_email->value;
				$data['site_title'] = $site_title->value;
				$user = $user_data['email'];
				// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
				Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($user, $contents_array, $data, $email_contents, $name) {
					$message->from($data['site_email'], $data['site_title']);
					$message->to($user, $name)->subject($email_contents['email_template_subject']);
				});
				$array_json_return = array('error_code' => '0', 'msg' => 'Verification email sent to your email.');
			} else {
				$array_json_return = array('error_code' => '2', 'msg' => 'Not Register');
			}
		} else {
			$array_json_return = array('error_code' => '3', 'msg' => 'No input data');
		}
		echo json_encode($array_json_return);
	}

	/**   ***forgot password*** */

	/**
	 * @SWG\Post(
	 *   path="/ws-forgot-password",
	 *   summary="forgot password authentication mail",
	 *   tags={"User Signup Signin"},
	 *   description="send mail for resetting password",
	 *   operationId="forgot password",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="User Email-id",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="")
	 * )
	 */

	protected function wsForgotPassword() {
		$request = Input::all();
		$user_data = User::where('email', $request['email'])->first();
		if (count($user_data) > 0) {
			if (isset($request['email']) != '') {
				$site_email = trans_global_settings::where('id', '1')->first();
				$site_title = trans_global_settings::where('id', '2')->first();
				$data['global']['site_email'] = $site_email->value;
				$data['global']['site_title'] = $site_title->value;
				/* sending admin credentail on admin account mail on user email */
				$code = rand(9991, 999899);
				$activation_code = time();
				$arr_admin_detail = User::where('email', $request['email'])->first();
				$update_data = array('reset_password_code' => $activation_code);
				User::where('email', $request['email'])->update($update_data);
				$reset_password_link = '<a href="' . url('/') . '/front/reset-password/' . base64_encode($activation_code) . '">Click here</a>';
				$lang_id = 17;
				$admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';

				$macros_array_detail = array();
				$macros_array_detail = email_template_macros::all();
				$macros_array = array();
				foreach ($macros_array_detail as $row) {
					$macros_array[$row['macros']] = $row['value'];
				}
				$email_contents = email_templates::where(array('email_template_title' => 'user-forgot-password', 'lang_id' => '17'))->first();

				$content = $email_contents['email_template_content'];

				$reserved_words = array();
				$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
				$reserved_arr = array(
					"||SITE_TITLE||" => stripslashes($data['global']['site_title']),
					"||SITE_PATH||" => url('/'),
					"||USER_NAME||" => $arr_admin_detail['user_name'],
					"||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
					"||USER_EMAIL||" => $arr_admin_detail['email'],
					"||RESET_PASSWORD_LINK||" => $reset_password_link,
					"||LOGIN_LINK||" => $admin_login_link,
				);
				$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
				foreach ($reserved_words as $k => $v) {
					$content = str_replace($k, $v, $content);
				}
				$contents_array = array("email_conents" => $content);

				Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($arr_admin_detail, $contents_array, $data, $email_contents) {
					$message->from($data['global']['site_email'], $data['global']['site_title']);
					$message->to($arr_admin_detail['email'], $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'])->subject($email_contents['email_template_subject']);
				});

				$array_json_return = array('error_code' => '0', 'msg' => 'success');
				echo json_encode($array_json_return);
			} else {
				$array_json_return = array('error_code' => '1', 'msg' => 'error');
				echo json_encode($array_json_return);
			}
		} else {
			$array_json_return = array('error_code' => '2', 'msg' => 'Email not Register');
			echo json_encode($array_json_return);
		}
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-change-password",
	 *   summary="change password",
	 *   tags={"User"},
	 *   description="reset your password",
	 *   operationId="reset password",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="User-id",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="new_password",
	 *       in="formData",
	 *       description="new password",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="Success: Password changed ")
	 * )
	 */

	protected function wsChangePassword() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$new_password = $request['new_password'];
		$user_data = User::where('id', $user_id)->first();

		if (count($user_data) > 0) {
			$salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
			$password = crypt($new_password, '$2y$12$' . $salt);
			$update_data = array('password' => $password);
			User::where('id', $user_id)->update($update_data);

			$array_json_return = array('error_code' => '0', 'msg' => 'Success');
		} else {
			$array_json_return = array('error_code' => '2', 'msg' => 'Record Not found');
		}
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-group",
	 *   summary="Get the top level groups only",
	 *   tags={"Groups"},
	 *   description="Get the top level groups (no information about any child groups)",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Response(response="200", description="Success. JSON is returned")
	 * )
	 **/
	public function wsGroup() {

		$get_parent_group_id = DB::table('mst_group')
			->where('parent_id', 0)
			->select('id', 'parent_id', 'group_name')
			->get();

		$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'group' => $get_parent_group_id);
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-sub-group",
	 *   summary="Get the child groups for a group",
	 *   tags={"Groups"},
	 *   description="Get the child groups for a group",
	 *   operationId="sub groups",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="id",
	 *       in="query",
	 *       description="ID of the group whose sub-groups are fetched",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Response(response="200", description="Success. JSON is returned")
	 * )
	 **/
	public function wsChildGroup() {

		$request = Input::all();
		$group_id = $request['id'];
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$domainName = $_SERVER['HTTP_HOST'];
		$url = $protocol . $domainName;
		$get_sub_group = DB::table('mst_group')
			->where('parent_id', $group_id)
			->select('id', 'group_name', 'icon_img', 'cover_img', 'profession', 'parent_id')
			->get();

		foreach ($get_sub_group as $key => $value) {
			$arr_chlid_group = DB::table('mst_group')
				->select('id', 'group_name', 'icon_img', 'cover_img', 'profession', 'parent_id')
				->where('parent_id', $value->id)
				->get();
			$get_sub_group[$key]->chlid = $arr_chlid_group;
			$value->icon_img =config('backend_img')."group_img/icon/" . $value->icon_img;
			$value->cover_img =config('backend_img')."group_img/cover/" . $value->cover_img;

			foreach ($arr_chlid_group as $key => $value) {
				$arr_chlid_group1 = DB::table('mst_group')
					->select('id', 'group_name', 'icon_img', 'cover_img', 'profession', 'parent_id')
					->where('parent_id', $value->id)
					->get();
				$arr_chlid_group[$key]->chlid = $arr_chlid_group1;
				$value->icon_img =config('backend_img')."group_img/icon/" . $value->icon_img;
				$value->cover_img =config('backend_img')."group_img/cover/" . $value->cover_img;
			}
		}

		$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'subgroup' => $get_sub_group);
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-dashboard",
	 *   summary="Get posts on the Homepage/Dashboard",
	 *   tags={"Homepage Posts"},
	 *   description="Fetch posts on the homepage. IMPORTANT CAVEATS : 1) API is meant to be accessed sequentially increasing page_number starting from page_number=1 otherwise it will not return correct data  2) Stop accessing the data when page_number=X returns empty dataset  3) Do NOT access the API randomly or go non-sequentially  4) session_id is absolutely recommended to be passed  5) do NOT access API on page_number=0 or negative 6) API will return atleast 7(default) or posts_per_page(if passed). It will return atleast that number of posts, but may return more than that. The number of posts returned on a particular page number is NOT fixed. 7) OPEN BUGS : 470, 481",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="filter",
	 *       in="formData",
	 *       description="(Options -0:ALL, 1: article, 2-education, 5-job, 4- event, 3-meetup, 6- qrious  Can accept multiple values using commas)",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="page_number",
	 *       in="formData",
	 *       description="The page number that is being fetched",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="posts_per_page",
	 *       in="formData",
	 *       description="Number of minimum posts per page; Posts returned will be atleastX; Posts can however be a bit more also and will not be exact number X, Default:7",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="session_id",
	 *       in="formData",
	 *       description="You can just use a simple epoch time() as session_id, but it needs to be same whenever user is scrolling down the homepage and we are calling API again with updated page_number. Needs to change when user refreshes homepage or views it again. Helps us track user sessions if user is using ATG from multiple devices. Although optional, but highly RECOMMENDED to implement. Default:0",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="This is the user ID of the user who is viewing the homepage",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(
	 *       response="200",
	 *       description="Success. 0:Success, 1:Groups Not Found, 2:Invalid Output, 3:No Records Found"
	 *   )
	 * )
	 **/
	public function wsDashboard() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if (isset($request['filter']) && $request['filter'] != '') {
			$filter = $request['filter'];
		} else {
			$filter = 0;
		}
		if (isset($request['page_number']) && $request['page_number'] != '') {
			$page = $request['page_number'];
		} else {
			$page = '1';
		}

		if (isset($request['posts_per_page']) && $request['posts_per_page'] != '') {
			$posts_per_page = $request['posts_per_page'];
		} else {
			$posts_per_page = '7';
		}

		if (isset($request['session_id']) && $request['session_id'] != '') {
			$session_id = $request['session_id'];
		} else {
			$session_id = '0';
		}

		if ($user_id != '') {
			/*             * ****** */
			$get_groups = DB::table('trans_group_users')
				->where('user_id_fk', $user_id)
				->get();
			$user_group = array();
			if (count($get_groups) > 0) {
				foreach ($get_groups as $key => $value) {
					$user_group[] = $value->group_id_fk;
				}
				$group = implode(',', $user_group);
			}
			$prifix = \DB::getTablePrefix();
			$filter = explode(',', $filter);

			if (count($user_group) > 0) {
				$arr_all_records = $this->HomePagePostsAlgofx($user_id, 'app', $session_id, $page, $filter, $posts_per_page, 3);

				$arr_all_records12 = [];
				foreach ($arr_all_records as $post) {
					$arr_all_records12[] = $post;
				}

				//Passing cover image dimensions for the cover images - commented because of performance impact
				foreach ($arr_all_records12 as $x => $x_value) {
					if (collect($x_value)->has('image') && collect($x_value)->get('image') != '') {
						unset($image_width);
						unset($image_height);

						if (Storage::exists(config("feature_image_path"). strtolower(collect($x_value)->get('type')) . '_image/' . collect($x_value)->get('image'))) {
							//    $image_width = Image::make(public_path() . '/assets/Frontend/images/'.strtolower(collect($x_value)->get('type')).'_image/'.collect($x_value)->get('image'))->width();
							//    $image_height = Image::make(public_path() . '/assets/Frontend/images/'.strtolower(collect($x_value)->get('type')).'_image/'.collect($x_value)->get('image'))->height();
							$x_value->image = config("feature_pic_url"). strtolower(collect($x_value)->get('type')) . '_image/' . collect($x_value)->get('image');

						}
						if (Storage::exists(config("feature_image_path"). strtolower(collect($x_value)->get('type')) . '_image/thumb/' . collect($x_value)->get('image'))) {
							//    $image_width = Image::make(public_path() .'/assets/Frontend/images/'.strtolower(collect($x_value)->get('type')).'_image/thumb/'.collect($x_value)->get('image'))->width();
							//    $image_height = Image::make(public_path() .'/assets/Frontend/images/'.strtolower(collect($x_value)->get('type')).'_image/thumb/'.collect($x_value)->get('image'))->height();
							$x_value->image = config("feature_pic_url") . strtolower(collect($x_value)->get('type')) . '_image/thumb/' . collect($x_value)->get('image');

						}
						if ((isset($image_width)) && $image_width != '' && isset($image_height) && $image_height != '') {
							//put image_width and height in API response
							$x_value->img_w = $image_width;
							$x_value->img_h = $image_height;
						}
					}

					if (!empty($x_value->profile_picture)) {
						$x_value->profile_picture=config('profile_pic_url').$x_value->user_id.'/thumb/'.$x_value->profile_picture;
						

					}
					if(!empty($x_value->type))
						$x_value->post_url=url('/').'/view-'.strtolower($x_value->type).'/'.preg_replace('/[^A-Za-z0-9\s]/', '', $x_value->title).'-'.$x_value->id;
				}

				$user_detail = user::where('id', $user_id)->select('*')->first();
				if (count($user_detail) > 0) {
					$email_verified = '';
					if ($user_detail->fb_id != "" || $user_detail->linkdin_id != "" || $user_detail->twitter_id != "" || $user_detail->google_id != "") {
						$email_verify_sts = "1";
					} else {
						$email_verify_sts = $user_detail->email_verified;
					}
				}

				if (count($arr_all_records12) > 0) {
					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'dashboard' => $arr_all_records12, 'email_verified' => $email_verify_sts);
					echo json_encode($array_json_return);
				} else {
					$array_json_return = array('error_code' => '3', 'msg' => 'No Record found', 'dashboard' => $arr_all_records12, 'email_verified', $email_verify_sts);
					echo json_encode($array_json_return);
				}
			} else {
				$array_json_return = array('error_code' => '1', 'msg' => 'No groups found');
				echo json_encode($array_json_return);
			}
		} else {
			$array_json_return = array('error_code' => '2', 'msg' => 'Invalid Input');
			echo json_encode($array_json_return);
		}
	}

	/** Calculates the difference between upvotes and downvotes
	 *     Used by hotness function */
	public function vote_diff($up = 0, $down = 0) {
		return ($up - $down);
	}

	/**
	 * Hotness algorithm takes the no of upvotes, downvotes and CREATED_DATE
	 * of any post and returns a hotness number
	 * medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9
	 */
	public function hotness($upvotes = 0, $downvotes = 0, $posted_date = 0) {

		$s = $this->vote_diff($upvotes, $downvotes);

		if ($s > 0) {
			$y = 1;
		} elseif ($s < 0) {
			$y = -1;
		} else {
			$y = 0;
		}

		if (abs($s >= 1)) {
			$z = abs($s);
		} else {
			$z = 1;
		}

		/**
		 * Addresses the corner case where posts do not have a posted date
		 * TODO : Give CREATED_AT in database for such posts
		 * Verify that none of the posts after 26.04.17 have CREATE_AT=0
		 */
		if ($posted_date == 0) {
			$posted_date = '2016-09-29 12:08:53';
		}

		/** \Log::info($posted_date);
		\Log::info($epoch_updated);
		\Log::info(strtotime($posted_date)); */

		$epoch_updated = strtotime($posted_date);
		//$epoch_updated = Carbon::parse($posted_date)->format('U');
		$order = log10(max(abs($s), 1));
		$second = $epoch_updated - 1134028003;
		return round($y * 20 * $order + $second / 45000, 7);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-feed-detail",
	 *   summary="Get details about a particular post",
	 *   tags={"Posts"},
	 *   description="Get details about a particular post",
	 *   operationId="Fetch Post Detail",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="type",
	 *       in="formData",
	 *       description="Type of post (Options : article, qrious, event, meetup, education, job)",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="feed_id",
	 *       in="formData",
	 *       description="This is the ID of the post that is being fetched",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="This is the user ID of the user who is viewing the post",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(
	 *       response="200",
	 *       description="Success. JSON is returned"
	 *   )
	 * )
	 **/
	public function wsFeedDetail() {
		$request = Input::all();
		$type = $request['type'];
		$feed_id = $request['feed_id'];
		$user_id = $request['user_id'];
		if ($feed_id != '' && $user_id != "") {
			/*             * *****article******* */
			if ($type == 'article') {
				$array_post = DB::table('mst_article as ma')
					->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'inner')
					->join('trans_follow_user as tfu', 'ma.user_id_fk', '=', 'tfu.user_id_fk', 'left')
					->where('ma.id', $feed_id)
					->select('ma.title', 'ma.user_id_fk', 'ma.tags', 'ma.updated_at', 'ma.created_at', 'ma.profile_image', 'ma.comment', 'ma.status', 'ma.id as article_id', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name', DB::raw('count(follower_id_fk) as follower_count'))
					->get();
				$array_post1 = DB::table('mst_article as ma')
					->where('ma.id', $feed_id)
					->select('ma.description')
					->first();

				if (count($array_post[0]->title) != '') {
					/*                     * ****follower***** */
					$user_id_fk = $array_post[0]->user_id_fk;
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();

					$array_post->follower_count = count($arr_follower);

					foreach ($array_post as $key => $value) {

						if ($value->tags != '') {
							$arr_tag = explode(',', $value->tags);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						$article_comment = DB::table('mst_comments as mc')
							->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
							->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
							->where('tac.article_id_fk', $value->article_id)
							->orderBy('id', 'DESC')
							->select('mc.*', 'u.user_name', 'u.profile_picture', 'tac.comment_image')
							->get();
						$array_post[$key]->arr_comment = $article_comment;
						$array_post[$key]->count = count($article_comment);

						/*                         * ****posed by group****** */
						$get_groups = DB::table('trans_article_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('t.article_id_fk', $value->article_id)
							->select('mg.group_name', 'mg.id', 'mg.icon_img')
							->get();
						$array_post[$key]->posted_by = $get_groups;
						/**                         * *********** AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->article_id)
							->where('type', '3')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);

						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_article_upvote_downvote')
							->where('article_id_fk', $value->article_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_article_upvote_downvote')
							->where('article_id_fk', $value->article_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('article_id', $value->article_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$link = $value->article_id;
						$array_post[$key]->link = url("/") . '/view-article/' . $link;
						$array_post[$key]->descriptionURL = url("/") . '/view-article/' . $value->article_id . '/wv/description';
					}
					$arr_current_up_down_status = DB::table('trans_article_upvote_downvote')
						->where('article_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->select('status')->get();

					if (count($arr_current_up_down_status) > 0) {
						$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
					} else {
						$array_post[$key]->current_upvote_downvote_status = '';
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}

					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not found');
				}
			}
			/*             * *****event******* */
			if ($type == 'event') {

				$array_post = DB::table('mst_event as te')
					->join('mst_users as u', 'te.user_id_fk', '=', 'u.id', 'left')
					->join('trans_follow_user as tfu', 'te.user_id_fk', '=', 'tfu.user_id_fk', 'left')
					->where('te.id', $feed_id)
					->select('te.title', 'te.agenda', 'te.user_id_fk', 'te.venue', 'te.start_date', 'te.start_time', 'te.end_date', 'te.end_time', 'te.min_age', 'te.max_age', 'te.latitude', 'te.longitude', 'te.location', 'te.website', 'te.terms_condition', 'te.contact_name', 'te.contact_number', 'te.email_address', 'te.agenda', 'te.guest', 'te.created_at', 'te.updated_at', 'te.currency', 'te.tag', 'te.profile_image', 'te.status', 'te.id as event_id', 'te.cost as main_cost', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name', DB::raw('count(follower_id_fk) as follower_count'))
					->get();

				$array_post1 = DB::table('mst_event as te')
					->where('te.id', $feed_id)
					->select('te.description')
					->first();
				if (count($array_post) > 0) {
					if (count($array_post1) > 0) {
						$strHtml = $array_post1->description;
						$arr_return = $this->descriptionData($strHtml);
					} else {
						$arr_return = "";
					}

					/*                     * ***follower***** */
					$user_id_fk = $array_post[0]->user_id_fk;
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();
					$array_post->follower_count = $arr_follower;
					foreach ($array_post as $key => $value) {
						if ($value->agenda != '') {
							$array_post[$key]->agenda_title = $value->agenda;
						} else {
							$array_post[$key]->agenda_title = '';
						}
						if ($value->tag != '') {
							$arr_tag = explode(',', $value->tag);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						/*                         * **comment*** */
						$arr_event_comment = DB::table('mst_comments as mc')
							->join('trans_event_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
							->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
							->where('tbc.status', '1')
							->where('tbc.event_id_fk', $value->event_id)
							->orderBy('id', 'DESC')
							->select('mc.*', 'u.user_name', 'u.profile_picture')
							->get();
						$array_post[$key]->arr_comment = $arr_event_comment;
						$array_post[$key]->count = count($arr_event_comment);

						$get_groups = DB::table('trans_event_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('t.event_id_fk', $value->event_id)
							->select('mg.group_name', 'mg.id', 'mg.icon_img')
							->get();
						$array_post[$key]->posted_by = $get_groups;

						/*                         * **for cost status**** */
						$arr_cost_detail = DB::table('mst_event_cost as mec')
							->join('trans_event_rsvp_status as ters', 'ters.cost_id_fk', '=', 'mec.id', 'left')
							->where('mec.event_id_fk', $value->event_id)
							->groupBy('mec.id')
							->select('mec.*', 'ters.status')
							->get();

						if (count($arr_cost_detail) > 0) {
							foreach ($arr_cost_detail as $k => $val) {

								if ($val->status == "" || $val->status == null) {
									$arr_cost_detail[$k]->status = "0";
								}
							}
							$array_post[$key]->cost = $arr_cost_detail;
						} else {
							$array_post[$key]->cost = '';
						}

						/*                         * ****guest******** */
						$arr_event_guest = eventGuest::where('event_id_fk', '=', $value->event_id)->get();
						$array_post[$key]->guest = $arr_event_guest;

						/** For Agenda Session and Time *** */
						$arr_agenda = eventAgenda::where('event_id_fk', '=', $value->event_id)->get();

						$arr_event_agenda_details = array();
						if (count($arr_agenda) > 0) {
							foreach ($arr_agenda as $key1 => $agenda) {
								$arr_event_agenda_details[$key1] = $agenda;

								$arr_agenda_session_details = DB::table('trans_event_agenda_session_time')
									->where('agenda_id_fk', $agenda->id)
									->select('*')
									->get();

								$arr_event_agenda_details[$key1]->session_details = $arr_agenda_session_details;
								$array_post[$key]->agenda = $arr_event_agenda_details;
							}
						} else {
							$array_post[$key]->agenda = [];
						}
						/*                         * ************ AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->event_id)
							->where('type', '1')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);
						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_event_upvote_downvote')
							->where('event_id_fk', $value->event_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_event_upvote_downvote')
							->where('event_id_fk', $value->event_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('event_id', $value->event_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						$array_post[$key]->get_save_user_post = count(saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $value->event_id)->where('type', '1')->first());
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$array_post[$key]->link = url("/") . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->event_id;
						$array_post[$key]->descriptionURL = url("/") . '/view-event/' . $value->event_id . '/wv/description';
						$arr_current_up_down_status = DB::table('trans_event_upvote_downvote')
							->where('event_id_fk', $feed_id)
							->where('user_id_fk', $user_id)
							->select('status')->get();

						if (count($arr_current_up_down_status) > 0) {
							$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
						} else {
							$array_post[$key]->current_upvote_downvote_status = '';
						}
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}

					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not founds');
				}
			}
			/**             * ****meetup******* */
			if ($type == 'meetup') {

				$array_post = DB::table('mst_meetup as mm')
					->join('mst_users as u', 'mm.user_id_fk', '=', 'u.id', 'left')
					->join('trans_follow_user as tfu', 'mm.user_id_fk', '=', 'tfu.user_id_fk', 'left')
					->where('mm.id', $feed_id)
					->select('mm.title', 'mm.agenda', 'mm.user_id_fk', 'mm.venue', 'mm.start_date', 'mm.start_time', 'mm.end_date', 'mm.end_time', 'mm.min_age', 'mm.max_age', 'mm.latitude', 'mm.longitude', 'mm.location', 'mm.website', 'mm.terms_condition', 'mm.contact_name', 'mm.contact_number', 'mm.email_address', 'mm.agenda', 'mm.guest', 'mm.created_at', 'mm.updated_at', 'mm.currency', 'mm.tag', 'mm.profile_image', 'mm.status', 'mm.id as meetup_id', 'mm.cost as main_cost', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name', DB::raw('count(follower_id_fk) as follower_count'))
					->get();
				$array_post1 = DB::table('mst_meetup as mm')
					->where('mm.id', $feed_id)
					->select('mm.description')
					->first();

				if (count($array_post1) > 0) {
					$strHtml = $array_post1->description;
					$arr_return = $this->descriptionData($strHtml);
				} else {
					$arr_return = "";
				}

				if (count($array_post[0]->title) != '') {

					$rec_day = transmeetuprecurringday::where('meetup_id_fk', $feed_id)->get();

					//dd($rec_day);
					/*                     * ***follower***** */
					$user_id_fk = $array_post[0]->user_id_fk;
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();
					$array_post->follower_count = $arr_follower;

					foreach ($array_post as $key => $value) {
						/*                         * ****tag array**** */

						if ($value->agenda != '') {
							$array_post[$key]->agenda_title = $value->agenda;
						} else {
							$array_post[$key]->agenda_title = '';
						}
						if ($rec_day != '' && isset($rec_day)) {
							$array_post[$key]->recursive_day = $rec_day;
						}
						if ($value->tag != '') {
							$arr_tag = explode(',', $value->tag);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						/*                         * ****** */
						$user_id_fk = $array_post[0]->user_id_fk;
						$meetup_comment = DB::table('mst_comments as mc')
							->join('trans_meetup_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
							->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
							->where('tbc.meetup_id_fk', $value->meetup_id)
							->select('mc.*', 'u.user_name', 'u.profile_picture')
							->get();
						$array_post[$key]->arr_comment = $meetup_comment;
						$array_post[$key]->count = count($meetup_comment);

						/*                         * ****posted by group******* */
						$get_groups = DB::table('trans_meetup_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('t.meetup_id_fk', $value->meetup_id)
							->select('mg.group_name', 'mg.id', 't.meetup_id_fk', 'mg.icon_img')
							->get();

						$array_post[$key]->posted_by = $get_groups;
						/*                         * ***cost**** */

						/**                         * ***** cost rsvp********* */
						$arr_cost_detail = DB::table('mst_meetup_cost as mmc')
							->join('trans_meetup_rsvp_status as tmrs', 'tmrs.cost_id_fk', '=', 'mmc.id', 'left')
							->where('mmc.meetup_id_fk', $value->meetup_id)
							->select('mmc.*', 'tmrs.status')
							->orderBy('mmc.id')
							->get();

						$cost_det = array();

						if (count($arr_cost_detail) > 0) {

							foreach ($arr_cost_detail as $k => $val) {

								if ($val->status == "" || $val->status == null) {
									$arr_cost_detail[$k]->status = "0";
								}
							}
							$array_post[$key]->cost = $arr_cost_detail;
						}

						/*                         * ****guest******** */
						$arr_meetup_guest = meetupGuest::where('meetup_id_fk', '=', $value->meetup_id)->get();
						$array_post[$key]->guest = $arr_meetup_guest;

						/*                         * ** For Agenda Session and Time */
						$arr_agenda = meetupAgenda::where('meetup_id_fk', '=', $value->meetup_id)->get();

						$arr_meetup_agenda_details = array();
						if (count($arr_agenda) > 0) {
							foreach ($arr_agenda as $key1 => $agenda) {
								$arr_meetup_agenda_details[$key1] = $agenda;

								$arr_agenda_session_details = DB::table('trans_agenda_session_time')
									->where('agenda_id_fk', $agenda->id)
									->select('*')
									->get();

								$arr_meetup_agenda_details[$key1]->session_details = $arr_agenda_session_details;
								$array_post[$key]->agenda = $arr_meetup_agenda_details;
							}
						} else {
							$array_post[$key]->agenda = [];
						}
						/*                         * ************ AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->meetup_id)
							->where('type', '2')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);
						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_meetup_upvote_downvote')
							->where('meetup_id_fk', $value->meetup_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_meetup_upvote_downvote')
							->where('meetup_id_fk', $value->meetup_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('meetup_id', $value->meetup_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						$array_post[$key]->get_save_user_post = count(saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $value->meetup_id)->where('type', '0')->first());
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$array_post[$key]->link = url("/") . '/view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->meetup_id;
						$array_post[$key]->descriptionURL = url("/") . '/view-meetup/' . $value->meetup_id . '/wv/description';
						$arr_current_up_down_status = DB::table('trans_meetup_upvote_downvote')
							->where('meetup_id_fk', $feed_id)
							->where('user_id_fk', $user_id)
							->select('status')->get();

						if (count($arr_current_up_down_status) > 0) {
							$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
						} else {
							$array_post[$key]->current_upvote_downvote_status = '';
						}
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}

					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not founds');
				}
			}
			/*             * *****qrious******* */
			if ($type == 'qrious') {
				$array_post = DB::table('mst_question as ma')
					->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
					->join('trans_follow_user as tfu', 'ma.user_id_fk', '=', 'tfu.user_id_fk', 'left')
					->where('ma.id', $feed_id)
//                        ->select('ma.*', 'ma.id as qrious_id', 'u.*', DB::raw('count(follower_id_fk) as follower_count'))
					->select('ma.user_id_fk', 'ma.title', 'ma.post_short_description', 'ma.answer', 'ma.status', 'ma.created_at', 'ma.updated_at', 'ma.tags', 'ma.profile_image', 'ma.status', 'ma.id as qrious_id', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name', DB::raw('count(follower_id_fk) as follower_count'))
					->get();
				$array_post1 = DB::table('mst_question as ma')
					->where('ma.id', $feed_id)
					->select('ma.description')
					->first();

				if (count($array_post1) > 0) {
					$strHtml = $array_post1->description;
					$arr_return = $this->descriptionData($strHtml);
				} else {
					$arr_return = "";
				}

				if (count($array_post) > 0) {
					$user_id_fk = $array_post[0]->user_id_fk;
					/*                     * ***follower***** */
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();
					$array_post->follower_count = $arr_follower;

					foreach ($array_post as $key => $value) {
						/*                         * ****tag array**** */
						if ($value->tags != '') {
							$arr_tag = explode(',', $value->tags);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						/*                         * ****** */
						/*                         * **comment**** */
						$question_answer = DB::table('mst_answers as mc')
							->join('trans_question_answers as tac', 'mc.id', '=', 'tac.answer_id_fk', 'left')
							->join('mst_users as u', 'u.id', '=', 'mc.answered_by', 'left')
//                        ->where('tac.status', '1')
							->where('tac.question_id_fk', $value->qrious_id)
							->orderBy('id', 'DESC')
							->select('mc.*', 'mc.answer as comment', 'answer_on as comment_on', 'u.user_name', 'u.profile_picture', 'tac.answer_image as comment_image', 'mc.answered_by as commented_by')
							->get();
						if (count($question_answer) > 0) {

							foreach ($question_answer as $key1 => $value1) {
								/*                                 * **************upvote answer Downvote********************* */
								$question_answer[$key1]->is_upvotes = DB::table('trans_answer_upvote_downvote as t')
									->where('user_id_fk', $user_id)
									->where('answer_id_fk', $value1->id)
									->select('t.status')
									->get();

								$question_answer[$key1]->total_upvotes = count(DB::table('trans_answer_upvote_downvote')
										->where('answer_id_fk', $value1->id)
										->where('status', '0')
										->select('status')
										->get());

								$question_answer[$key1]->total_downvotes = count(DB::table('trans_answer_upvote_downvote')
										->where('answer_id_fk', $value1->id)
										->where('status', '1')
										->select('status')
										->get());
							}


							$array_post[$key]->arr_comment = $question_answer;
							$array_post[$key]->count = count($question_answer);
						}

						/*                         * *********for group************ */
						$get_groups = DB::table('trans_question_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('question_id_fk', $value->qrious_id)
							->select('mg.group_name', 'mg.id', 'mg.icon_img')
							->get();
						$array_post[$key]->posted_by = $get_groups;
						/**                         * *********** AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->qrious_id)
							->where('type', '6')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);
						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_question_upvote_downvote')
							->where('question_id_fk', $value->qrious_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_question_upvote_downvote')
							->where('question_id_fk', $value->qrious_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('question_id', $value->qrious_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$array_post[$key]->link = url("/") . '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->qrious_id;
						$array_post[$key]->descriptionURL = url("/") . '/view-question/' . $value->qrious_id . '/wv/description';

						$arr_current_up_down_status = DB::table('trans_meetup_upvote_downvote')
							->where('meetup_id_fk', $feed_id)
							->where('user_id_fk', $user_id)
							->select('status')->get();
						if (count($arr_current_up_down_status) > 0) {
							$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
						} else {
							$array_post[$key]->current_upvote_downvote_status = '';
						}
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}

					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not founds');
				}
			}
			/*             * *****education******* */
			if ($type == 'education') {
				$array_post = DB::table('mst_education as me')
					->join('mst_users as u', 'me.user_id_fk', '=', 'u.id', 'left')
					->join('trans_follow_user as tfu', 'me.user_id_fk', '=', 'tfu.user_id_fk', 'left')
					->where('me.id', $feed_id)
//                        ->select('me.*', 'me.id as education_id', 'u.*', DB::raw('count(follower_id_fk) as follower_count'))
					->select('me.user_id_fk', 'me.id as edu_id', 'me.title', 'me.website', 'me.cost', 'me.education_type', 'me.created_at', 'me.updated_at', 'me.currency', 'me.tag', 'me.profile_image', 'me.status', 'me.id as education_id', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name', DB::raw('count(follower_id_fk) as follower_count'))
					->get();

				$array_post1 = DB::table('mst_education as me')
					->where('me.id', $feed_id)
					->select('me.description')
					->first();
				if (count($array_post) > 0) {
					if (count($array_post1) > 0) {
						$strHtml = $array_post1->description;
						$arr_return = $this->descriptionData($strHtml);
					} else {
						$arr_return = "";
					}

					/*                     * ***follower***** */
					$user_id_fk = $array_post[0]->user_id_fk;
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();
					$array_post->follower_count = $arr_follower;
					foreach ($array_post as $key => $value) {
						/*                         * ****tag array**** */
						$arr_edu_cost = DB::table('mst_education_cost')
							->where('education_id_fk', $value->edu_id)
							->select('*')
							->get();

						if (count($arr_edu_cost) > 0) {
							$array_post[$key]->cost = $arr_edu_cost;
						}

						if ($value->tag != '') {
							$arr_tag = explode(',', $value->tag);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						/*                         * ****** */
						/**                         * comment*** */
						$education_comment = DB::table('mst_comments as mc')
							->join('trans_education_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
							->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
							->where('tbc.education_id_fk', $value->education_id)
							->select('mc.*', 'u.user_name', 'u.profile_picture')
							->get();
						$array_post[$key]->arr_comment = $education_comment;
						$array_post[$key]->count = count($education_comment);
						/*                         * ***group***** */
						$get_groups = DB::table('trans_education_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('education_id_fk', $value->education_id)
							->select('mg.group_name', 'mg.id', 't.education_id_fk', 'mg.icon_img')
							->get();
						$array_post[$key]->posted_by = $get_groups;
						/**                         * *********** AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->education_id)
							->where('type', '4')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);
						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_education_upvote_downvote')
							->where('education_id_fk', $value->education_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_education_upvote_downvote')
							->where('education_id_fk', $value->education_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('education_id', $value->education_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$array_post[$key]->link = url("/") . '/view-education/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->education_id;
						$array_post[$key]->descriptionURL = url("/") . '/view-education/' . $value->education_id . '/wv/description';
						$arr_current_up_down_status = DB::table('trans_question_upvote_downvote')
							->where('question_id_fk', $feed_id)
							->where('user_id_fk', $user_id)
							->select('status')->get();

						if (count($arr_current_up_down_status) > 0) {
							$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
						} else {
							$array_post[$key]->current_upvote_downvote_status = '';
						}
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}
					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not founds');
				}
			}
			/*             * *****job******* */
			if ($type == 'job') {
				$array_post = DB::table('mst_job as mj')
					->join('mst_users as u', 'mj.user_id_fk', '=', 'u.id', 'left')
					->where('mj.id', $feed_id)
					->select('mj.title', 'mj.user_id_fk', 'mj.job_location', 'mj.min', 'mj.preferred_qualification', 'mj.role', 'mj.skill', 'mj.tags', 'mj.external_link_to_apply', 'mj.functional_area', 'mj.employment_type', 'mj.application_deadline', 'mj.salary_range', 'mj.phone_number', 'mj.email_address', 'mj.website', 'mj.url', 'mj.about_us', 'mj.contact_us', 'mj.created_at', 'mj.updated_at', 'mj.how_to_apply', 'mj.hiring_process', 'mj.status', 'mj.latitude', 'mj.longitude', 'mj.cmp_name', 'mj.max', 'mj.telephone_number', 'mj.id', 'mj.id as job_id', 'u.id', 'u.user_name', 'u.profile_picture', 'u.tagline', 'u.profession', 'u.location', 'u.gender', 'u.user_type', 'u.user_city', 'u.first_name', 'u.last_name')
					->get();

				$apply_job_count = ApplicantToJobModel::where(['user_id_fk' => $user_id, 'job_id_fk' => $feed_id])->count();

				$array_post1 = DB::table('mst_job as mj')
					->where('mj.id', $feed_id)
					->select('mj.description')
					->first();
				if (count($array_post) > 0) {
					if (count($array_post1) > 0) {
						$strHtml = $array_post1->description;
						$arr_return = $this->descriptionData($strHtml);
					} else {
						$arr_return = "";
					}

					/*                     * ***follower***** */
					$user_id_fk = $array_post[0]->user_id_fk;
					$arr_follower = DB::table('trans_follow_user')
						->where('user_id_fk', $user_id_fk)
						->select(DB::raw('count(follower_id_fk) as follower'))
						->get();

					$array_post->follower_count = $arr_follower[0];
					foreach ($array_post as $key => $value) {
						/*                         * ****tag array**** */
						if ($value->tags != '') {
							$arr_tag = explode(',', $value->tags);
							$tag = array();
							foreach ($arr_tag as $k => $val1) {
								$tag[$k]['tag_name'] = $val1;
							}
							$array_post[$key]->arr_tag = $tag;
						} else {
							$array_post[$key]->arr_tag = [];
						}
						/*                         * ****** */
						/*                         * ***group***** */
						$get_groups = DB::table('trans_job_group as t')
							->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
							->where('job_id_fk', $value->job_id)
							->select('mg.group_name', 'mg.id', 't.job_id_fk', 'mg.icon_img')
							->get();
						$array_post[$key]->posted_by = $get_groups;
						$array_post[$key]->apply_count = $apply_job_count;
						/**                         * *********** AUDIENCE ************** */
						$audience = DB::table('mst_visitor')
							->where('feature_id', $value->job_id)
							->where('type', '4')
							->select('*')
							->get();
						$array_post[$key]->audience = count($audience);
						/*                         * **************upvote Downvote********************* */
						$total_upvotes = DB::table('trans_job_upvote_downvote')
							->where('job_id_fk', $value->job_id)
							->where('status', '0')
							->select('status')
							->get();
						$array_post[$key]->total_upvote = count($total_upvotes);
						$total_downvotes = DB::table('trans_job_upvote_downvote')
							->where('job_id_fk', $value->job_id)
							->where('status', '1')
							->select('status')
							->get();
						$array_post[$key]->total_downvote = count($total_downvotes);
						$is_reported = DB::table('mst_make_report')
							->where('job_id', $value->job_id)
							->where('user_id_fk', $user_id)
							->get();
						$array_post[$key]->is_reported = count($is_reported);
						/*                         * **for follow unfollow status*** */
						$arr_follower = DB::table('trans_follow_user')
							->where('follower_id_fk', $user_id)
							->where('user_id_fk', $user_id_fk)
							->select('status')
							->first();
						$array_post[$key]->follower_status = isset($arr_follower->status) ? $arr_follower->status : '';
						$array_post[$key]->link = url("/") . '/view-job/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$array_post[$key]->descriptionURL = url("/") . '/view-job/' . $value->job_id . '/wv/description';
						$arr_current_up_down_status = DB::table('trans_job_upvote_downvote')
							->where('job_id_fk', $feed_id)
							->where('user_id_fk', $user_id)
							->select('status')->get();

						if (count($arr_current_up_down_status) > 0) {
							$array_post[$key]->current_upvote_downvote_status = $arr_current_up_down_status[0]->status;
						} else {
							$array_post[$key]->current_upvote_downvote_status = '';
						}
					}
					foreach ($array_post as $key => $val) {
						$array_post = $val;
					}
					$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'PostDetail' => $array_post);
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'records not founds');
				}
			}
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'Invalid');
		}
		echo json_encode($array_json_return, JSON_UNESCAPED_SLASHES);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-account-setting",
	 *   summary="detailed user account info",
	 *   tags={"User"},
	 *   description="account details of a given user_id",
	 *   operationId="account settings",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user id of the user who is logged in",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *     @SWG\Response(response="200", description="Success: JSON file returned")
	 * )
	 **/

	public function wsAccountSetting() {
		$request = Input::all();
		$user_id = $request['user_id'];

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$domainName = $_SERVER['HTTP_HOST'];
		$url = $protocol . $domainName;

		if ($user_id != '') {
			$arr_user_data = User::find($user_id);
			if(!empty($arr_user_data->profile_picture))
			$arr_user_data->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_user_data->profile_picture;
			if(!empty($arr_user_data->cover_photo))
			$arr_user_data->cover_photo=config("cover_image_url").$user_id.'/thumb/'.$arr_user_data->cover_photo;
			
			if ($arr_user_data != '') {
				$arr_user_following_count =DB::table('trans_follow_user as follower')
				->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
				->where('follower_id_fk', $user_id)
				->where('status', '1')
				->count();

				$arr_user_follower_count = DB::table('trans_follow_user as follower')
				->leftjoin('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
				->where('user_id_fk', $user_id)
				->where('status', '1')
				->count();

				$arr_user_data->followers=$arr_user_follower_count;
				$arr_user_data->following=$arr_user_following_count;

                $profile_pic = $arr_user_data->profile_picture;
                $cover_pic = $arr_user_data->cover_photo;

                $arr_user_data->profile_picture = Storage::exists(config('profile_path').$user_id."/".$profile_pic) ? config("profile_pic_url").$user_id."/".$profile_pic : '';
                $arr_user_data->cover_photo = Storage::exists(config('cover_path').$user_id."/" . $cover_pic) ? config("cover_image_url").$user_id."/" . $cover_pic : '';
                $arr_user_data->profile_picture_thumb = Storage::exists(config('profile_path').$user_id."/thumb/" . $profile_pic) ? config("profile_pic_url").$user_id."/thumb/" . $profile_pic : '';
                $arr_user_data->cover_photo_thumb = Storage::exists(config('cover_path').$user_id."/thumb/" . $cover_pic) ? config("cover_image_url").$user_id."/thumb/" . $cover_pic : '';

				$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'AccountSetting' => $arr_user_data);

			} else {
				$array_json_return = array("error_code" => '2', "msg" => "User is Not Registered.");
			}
		} else {
			$array_json_return = array("error_code" => '3', "msg" => "value cant be null.");
		}

		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-update-user-location",
	 *   summary="update user location",
	 *   tags={"User"},
	 *   description="updating location of a user",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user id of the user who is logged in",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="latitude",
	 *       in="formData",
	 *       description="Latitude",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="longitude",
	 *       in="formData",
	 *       description="Longitude",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="cc",
	 *       in="formData",
	 *       description="countrycode. e.g. IN, US, CA",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="rc",
	 *       in="formData",
	 *       description="regioncode. e.g. KA, KL, TN",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="cn",
	 *       in="formData",
	 *       description="countryname. e.g. India",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="rn",
	 *       in="formData",
	 *       description="region name. e.g. Karnataka",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="city",
	 *       in="formData",
	 *       description="city name. e.g. Bangalore (Preferred to Pass)",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="agent",
	 *       in="formData",
	 *       description="details about mobile device used by client",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="token",
	 *       in="formData",
	 *       description="token ID used for push notifications to the client. DB supports string value of 255 length",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="0: success, 2:fail")
	 * )
	 **/
	public function wsUpdateLocation() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$city = (isset($request['city']) ? $request['city'] : '');
		$rn = (isset($request['rn']) ? $request['rn'] : '');
		$cn = (isset($request['cn']) ? $request['cn'] : '');
		$cc = (isset($request['cc']) ? $request['cc'] : '');
		$rc = (isset($request['rc']) ? $request['rc'] : '');
		$token = (isset($request['token']) ? $request['token'] : '');
		$agent = (isset($request['agent']) ? $request['agent'] : '');

		if ($user_id != '') {
			$this->UpdateUserLocationfx($user_id, 'app', $request['latitude'], $request['longitude'], $agent, $city, $cn, $rn, $cc, $rc, $token);
			$array_json_return = array("error_code" => '0', "msg" => "success");
		} else {
			$array_json_return = array("error_code" => '2', "msg" => "fail");
		}
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-edit-account-setting",
	 *   summary="editing user account info",
	 *   tags={"User"},
	 *   description="editing account details of a given user_id",
	 *   operationId="account settings",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user id of the user who is logged in",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 * @SWG\Parameter(
	 *       name="user_profile",
	 *       in="formData",
	 *       description="profile pic of the user",
	 *       required=false,
	 *       type="file",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="user_name",
	 *       in="formData",
	 *       description="usename of the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="first_name",
	 *       in="formData",
	 *       description="first name of the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="last_name",
	 *       in="formData",
	 *       description="last name of the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="tagline",
	 *       in="formData",
	 *       description="tagline",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="email id of the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="profession",
	 *       in="formData",
	 *       description="profession of the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 * @SWG\Parameter(
	 *       name="about_me",
	 *       in="formData",
	 *       description="about the user who is logged in",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="mob_no",
	 *       in="formData",
	 *       description="mobile number of the user",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="phone_no",
	 *       in="formData",
	 *       description="phone number of the user",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="location",
	 *       in="formData",
	 *       description="location of the user",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="user_type",
	 *       enum="1234",
	 *       in="formData",
	 *       description="user type 1=>Student,2=>Admin,3=>Professional,4=>Company",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *
	 *     @SWG\Response(response="200", description="Success: JSON file returned")
	 * )
	 **/
	public function wsEditAcountSetting() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$request = Input::all();
		$user_id = $request['user_id'];

		if ($user_id != '') {
			$account_setting = User::where('id', $user_id)->first();
			$request['user_type']=isset($request['user_type'])?$request['user_type']:$account_setting->user_type;
			if ($request['user_type'] != '2') {
				
				if (count($account_setting) > 0) {
					if (isset($request['user_profile']) && $request['user_profile'] != "") {
						$resp=json_decode($this->upload_pic(0,$user_id,Input::file('user_profile')));
						$account_setting->profile_picture = config('profile_pic_url').$user_id.'/thumb/'.$resp->profile_picture;
					}
					$account_setting->user_name = isset($request['user_name'])?$request['user_name']:$account_setting->user_name;
					$account_setting->first_name =  isset($request['first_name'])?$request['first_name']:$account_setting->first_name;
					$account_setting->last_name =  isset($request['last_name'])?$request['last_name']:$account_setting->last_name;
					$account_setting->tagline =  isset($request['tagline'])?$request['tagline']:$account_setting->tagline;
					$account_setting->email =  isset($request['email'])?$request['email']:$account_setting->email;
					$account_setting->profession =  isset($request['profession'])?$request['profession']:$account_setting->profession;
					$account_setting->about_me = isset($request['about_me'])?$request['about_me']:$account_setting->about_me; 
					$account_setting->mob_no =  isset($request['mob_no'])?$request['mob_no']:$account_setting->mob_no;
					$account_setting->phone_no =  isset($request['phone_no'])?$request['phone_no']:$account_setting->phone_no;
					$account_setting->location =  isset($request['location'])?$request['location']:$account_setting->location;
					$account_setting->user_type =  isset($request['user_type'])?$request['user_type']:$account_setting->user_type;
					$account_setting->updated_at = date('Y-m-d H:i:s');
					$account_setting->save();

					$array_json_return = array('error_code' => '0', 'msg' => 'successfully Updated', 'UserInformation' => $account_setting);
				} else {
					$array_json_return = array("error_code" => '4', "msg" => "Not Register.");
				}
			} else {
				$array_json_return = array("error_code" => '1', "msg" => "You are login with Admin.");
			}
		} else {
			$array_json_return = array("error_code" => '2', "msg" => "value cant be null.");
		}
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-audience",
	 *   summary="Get or post audience stats for user",
	 *   tags={"Posts"},
	 *   description="Get or post audience stats for user",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="type",
	 *       in="formData",
	 *       description="(Options - 1-6, 1:event, 2:meetup, 3:article, 4:education, 5:job, 6:qrious.)",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user ID of the user who is currently logged in and viewing the post",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="post_id",
	 *       in="formData",
	 *       description="ID of the post user is viewing",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(response="200", description="Success. JSON is returned and post_status 0 means error and 1 means success")
	 * )
	 **/
	public function wsAudience() {
		$request = Input::all();
		$type = $request['type'];
		$visitor_id = $request['user_id'];
		$post_id = $request['post_id'];

		$audience_count = $this->Audiencefx($type, $post_id, $visitor_id);

		if ($audience_count == '-1') {
			$array_json_return = array("status" => '0', "msg" => "type should be between 1 to 6");
		} else {
			$array_json_return = array("status" => '1', "count" => $audience_count);
		}

		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-slider-banner",
	 *   summary="slider banner",
	 *   tags={"slider banner"},
	 *   description="slider banner",
	 *   operationId="slider banner",
	 *   produces={"application/json"},
	 *
	 *     @SWG\Response(response="200", description="Success: JSON file returned")
	 * )
	 **/
	public function wsSliderBanner() {

		$slider_banner_array = SliderModel::orderBy('id', 'DESC')->get();
		$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'slider_banner' => $slider_banner_array);
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-upvote-downvote",
	 *   summary="Upvote or Downvote a post",
	 *   tags={"Posts"},
	 *   description="Upvote or Downvote a post (article, event, meetup, qrious, education)",
	 *   operationId="Updown vote",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="status",
	 *       in="query",
	 *       description="status = 0 means upvote ; status = 1 means downvote",
	 *       required=true,
	 *       type="boolean",
	 *       @SWG\Items(type="boolean")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="type",
	 *       in="query",
	 *       description="Type of post (Options : article, qrious, event, meetup, education)",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="feed_id",
	 *       in="query",
	 *       description="This is the ID of the post that is being upvoted or downvoted",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="query",
	 *       description="This is the user ID of the user who is upvoting or downvoting the post",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(
	 *       response="200",
	 *       description="Success. JSON is returned"
	 *   )
	 * )
	 */
	public function wsUpvoteDownvote() {
		$request = Input::all();
		$commonModel = new CommonModel();
		$user_id = $request['user_id'];
		$type = $request['type'];
		$feed_id = $request['feed_id'];
		$status = $request['status'];

		if ($feed_id != '') {
			/*             * *****article******* */
			if ($type == 'article') {

				$get_data = DB::table('trans_article_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('article_id_fk', $feed_id)
					->get();

				if (count($get_data) == 0) {
					DB::table('trans_article_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'article_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_article_upvote_downvote')
						->where('article_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(['status' => $status]);
				}
				$total_downvotes = DB::table('trans_article_upvote_downvote')
					->where('status', '1')
					->where('article_id_fk', $feed_id)
					->get();
				$total_downvotes = count($total_downvotes);
				$total_upvotes = DB::table('trans_article_upvote_downvote')
					->where('status', '0')
					->where('article_id_fk', $feed_id)
					->get();
				$total_upvotes = count($total_upvotes);

				/*                 * ****notification****** */
				$get_article_data = DB::table('mst_article')
					->where('id', $feed_id)
					->get();

				$arr_notofication = $this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'article', $status, $get_article_data, $user_id);

			}
			/*             * *****qrious******* */
			if ($type == 'qrious') {

				$get_data = DB::table('trans_question_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('question_id_fk', $feed_id)
					->get();
				if (count($get_data) == 0) {
					DB::table('trans_question_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'question_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_question_upvote_downvote')
						->where('question_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(['status' => $status]);
				}

				$total_downvotes = DB::table('trans_question_upvote_downvote')
					->where('status', '1')
					->where('question_id_fk', $feed_id)
					->get();
				$total_downvotes = count($total_downvotes);
				$total_upvotes = DB::table('trans_question_upvote_downvote')
					->where('status', '0')
					->where('question_id_fk', $feed_id)
					->get();
				$total_upvotes = count($total_upvotes);

				/*                 * ***notification***** */
				$get_question_data = DB::table('mst_question')
					->where('id', $feed_id)
					->get();

				// Send Notification on ATG, Android, iOS and emails
				$arr_notofication = $this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'question', $status, $get_question_data, $user_id);
			}
			/*             * *****event******* */
			if ($type == 'event') {
				$get_data = DB::table('trans_event_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('event_id_fk', $feed_id)
					->get();
				if (count($get_data) == 0) {
					DB::table('trans_event_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'event_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_event_upvote_downvote')
						->where('event_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(['status' => $status]);
				}
				$total_downvotes = DB::table('trans_event_upvote_downvote')
					->where('status', '1')
					->where('event_id_fk', $feed_id)
					->get();
				$total_downvotes = count($total_downvotes);
				$total_upvotes = DB::table('trans_event_upvote_downvote')
					->where('status', '0')
					->where('event_id_fk', $feed_id)
					->get();
				$total_upvotes = count($total_upvotes);
				/*                 * ****notification***** */
				$get_event_data = DB::table('mst_event')
					->where('id', $feed_id)
					->get();

				$arr_notofication = $this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'event', $status, $get_event_data, $user_id);

			}
			/*             * *****meetup******* */
			if ($type == 'meetup') {
				$get_data = DB::table('trans_meetup_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('meetup_id_fk', $feed_id)
					->get();
				if (count($get_data) == 0) {
					DB::table('trans_meetup_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'meetup_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_meetup_upvote_downvote')
						->where('meetup_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['status' => $status]
						);
				}
				$get_data = DB::table('trans_meetup_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('meetup_id_fk', $feed_id)
					->get();
				$total_upvotes = DB::table('trans_meetup_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('meetup_id_fk', $feed_id)
					->where('status', '0')
					->count();
				$total_downvotes = DB::table('trans_meetup_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('meetup_id_fk', $feed_id)
					->where('status', '1')
					->count();
				if (count($get_data) == 0) {
					DB::table('trans_meetup_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'meetup_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_meetup_upvote_downvote')
						->where('meetup_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['status' => $status]
						);
				}
				/*                 * **notification***** */
				$get_meetup_data = DB::table('mst_meetup')
					->where('id', $feed_id)
					->get();

				// Following function defined in App\Traits\UpvoteDownvote
				// Send Notification on ATG, Android, iOS and emails
				$arr_notofication = $this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'meetup', $status, $get_meetup_data, $user_id);
			}

			if ($type == 'education') {
				$get_data = DB::table('trans_education_upvote_downvote')
					->where('user_id_fk', $user_id)
					->where('education_id_fk', $feed_id)
					->get();
				if (count($get_data) == 0) {
					DB::table('trans_education_upvote_downvote')->insert([
						['user_id_fk' => $user_id, 'education_id_fk' => $feed_id, 'status' => $status],
					]);
				} else {
					$update = DB::table('trans_education_upvote_downvote')
						->where('education_id_fk', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['status' => $status]
						);
				}
				$total_downvotes = DB::table('trans_education_upvote_downvote')
					->where('status', '1')
					->where('education_id_fk', $feed_id)
					->get();
				$total_downvotes = count($total_downvotes);
				$total_upvotes = DB::table('trans_education_upvote_downvote')
					->where('status', '0')
					->where('education_id_fk', $feed_id)
					->get();
				$total_upvotes = count($total_upvotes);
				/*                 * ****notification**** */
				$get_education_data = DB::table('mst_education')
					->where('id', $feed_id)
					->get();

				// Following function defined in App\Traits\UpvoteDownvote
				// Send Notification on ATG, Android, iOS and emails
				$arr_notofication = $this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'education', $status, $get_education_data, $user_id);
			}

			$arr_to_user = User::where('id', '=', $user_id)->select('*')->first();

			$array_json_return = array("error_code" => '0', "msg" => "Success", "Upvote count" => $total_upvotes, "Downvote count" => $total_downvotes, 'first_name' => $arr_to_user['first_name'], 'last_name' => $arr_to_user['last_name'], 'notification' => $arr_notofication);
		}
		echo json_encode($array_json_return);
	}

	public function wsRsvpEvent() {
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$request = Input::all();
		$cost_id = $request['cost_id'];
		$event_id = $request['event_id'];
		$status = $request['status'];

		$get_data = DB::table('trans_event_rsvp_status')
			->where('event_id_fk', $event_id)
			->where('user_id_fk', $user_id)
			->get();

		$cost_id_fk = $get_data[0]->cost_id_fk;
		if ($cost_id_fk == $cost_id) {
			$arr_to_update = array("status" => $status);
			DB::table('trans_event_rsvp_status')
				->where('event_id_fk', $event_id)
				->where('cost_id_fk', $cost_id)
				->where('user_id_fk', $user_id)
				->update($arr_to_update);

			$array_json_return = array("error" => '0', "error_message" => "Status has been changed successfully.");
		} else {
			DB::table('trans_event_rsvp_status')->insert([
				['event_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
			]);

			$array_json_return = array("error" => '1', "msg" => "success.");
		}

		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-report-abuse",
	 *   summary="report abuse",
	 *   tags={"Posts"},
	 *   description="report abuse to ",
	 *   operationId="report abuse",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user id of the user who is logged in",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 * @SWG\Parameter(
	 *       name="feed_id",
	 *       in="formData",
	 *       description="feed_id",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="type",
	 *       in="formData",
	 *       description="type of the post : 'article', 'qrious', 'event', 'meetup', 'education', 'job'",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="report",
	 *       enum="012",
	 *       in="formData",
	 *       description="report type 0=>junk,1=>uninteresting,2=>hatred or Vulgarity",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *
	 *     @SWG\Response(response="200", description="Success: JSON file returned")
	 * )
	 **/
	public function wsReport() {
		$request = Input::all();

		$user_id = $request['user_id'];
		$type = $request['type'];
		$feed_id = $request['feed_id'];

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$arr_user_data = User::find($user_id);

		if (isset($request['report']) && $request['report'] != '') {
			$report = $request['report'];
		} else {
			$report = '';
		}

		if ($report == '0') {
			$report_name = 'Junk';
		} elseif ($report == '1') {
			$report_name = 'Uninteresting';
		} elseif ($report == '2') {
			$report_name = 'Hatred or Vulgarity';
		} else {
			$report_name = '';
		}
		if ($feed_id != '') {
			$error_code = '0';
			if ($type == 'article') {
				$arr_article_data = ArticleModel::find($feed_id);
				$name = 'Article';
				$title = $arr_article_data['title'];

				if (count(DB::table('mst_make_report')->where('article_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('article_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(
							['user_id_fk' => $user_id, 'article_id' => $feed_id, 'report_type' => $report]
						);
				}
				/*                 * **Notification*** */
				$get_article_data = DB::table('mst_article')
					->where('id', $feed_id)
					->get();
				$subject = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . '"';
				$link = 'view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_article_data[0]->title) . '-' . $get_article_data[0]->id;
				$message = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . '"';
				$insert_data = array(
					//                        'notification_from' => $user_account['id'],
					'notification_to' => $get_article_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_article_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_article_data[0]->user_id_fk, 'article_id' => $feed_id, 'flag' => 'Report')));
				}
				/*                 * end*** */
			}
			/*             * ***qrious**** */
			if ($type == 'qrious') {
				$arr_question_data = QuestionModel::find($feed_id);
				$name = 'Qrious';
				$title = $arr_question_data['title'];

				if (count(DB::table('mst_make_report')->where('question_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('question_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(
							['user_id_fk' => $user_id, 'question_id' => $feed_id, 'report_type' => $report]
						);
				}
				/*                 * *****notification****** */
				$get_qrious_data = DB::table('mst_question')
					->where('id', $feed_id)
					->get();
				$subject = 'Your qrious "' . $get_qrious_data[0]->title . '" has been reported as "' . $report_name . '"';
				$link = 'view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_qrious_data[0]->title) . '-' . $get_qrious_data[0]->id;
				$message = 'Your qrious "' . $get_qrious_data[0]->title . '" has been reported as "' . $report_name . '"';

				$insert_data = array(
					//            'notification_from' => $data['user_session']['id'],
					'notification_to' => $get_qrious_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_qrious_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_qrious_data[0]->user_id_fk, 'question_id' => $feed_id, 'flag' => 'Report')));
				}
				/*                 * ****************** */
			}
			if ($type == 'event') {
				$arr_event_data = Event::find($feed_id);
				$name = 'Event';
				$title = $arr_event_data['title'];

				if (count(DB::table('mst_make_report')->where('event_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('event_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(['user_id_fk' => $user_id, 'event_id' => $feed_id, 'report_type' => $report]);
				}
				/*                 * ***Notification***** */
				$get_event_data = DB::table('mst_event')
					->where('id', $feed_id)
					->get();
				$subject = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . '"';
				$link = 'view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_event_data[0]->title) . '-' . $get_event_data[0]->id;
				$message = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . '"';
				$insert_data = array(
					//                        'notification_from' => $user_account['id'],
					'notification_to' => $get_event_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);

				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_event_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_event_data[0]->user_id_fk, 'event_id' => $feed_id, 'flag' => 'Report')));
				}

				/*                 * ***end***** */
			}
			if ($type == 'meetup') {
				$arr_meetup_data = meetup_model::find($feed_id);
				$name = 'Meetup';
				$title = $arr_meetup_data['title'];

				if (count(DB::table('mst_make_report')->where('meetup_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('meetup_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(
							['user_id_fk' => $user_id,
								'meetup_id' => $feed_id,
								'report_type' => $report]
						);
				}
				/*                 * *Notification** */
				$get_meetup_data = DB::table('mst_meetup')
					->where('id', $feed_id)
					->get();
				$subject = 'Your meetup "' . $get_meetup_data[0]->title . '" has been reported as "' . $report_name . '"';
				$link = 'view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_meetup_data[0]->title) . '-' . $get_meetup_data[0]->id;
				$message = 'Your meetup "' . $get_meetup_data[0]->title . '" has been reported as "' . $report_name . '"';
				$insert_data = array(
					//                        'notification_from' => $user_account['id'],
					'notification_to' => $get_meetup_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_meetup_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_meetup_data[0]->user_id_fk, 'meetup_id' => $feed_id, 'flag' => 'Report')));
				}
			}
			if ($type == 'education') {
				$arr_education_data = education_model::find($feed_id);
				$name = 'Education';
				$title = $arr_education_data['title'];

				if (count(DB::table('mst_make_report')->where('education_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('education_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(
							['user_id_fk' => $user_id,
								'education_id' => $feed_id,
								'report_type' => $report]
						);
				}
				/*                 * *Notification*** */
				$get_education_data = DB::table('mst_education')
					->where('id', $feed_id)
					->get();
				$subject = 'Your education "' . $get_education_data[0]->title . '" has been reported as "' . $report_name . '" by a user';
				$link = 'view-education/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_education_data[0]->title) . '-' . $get_education_data[0]->id;
				$message = 'Your education "' . $get_education_data[0]->title . '" has been reported as "' . $report_name . '" by a user';
				$insert_data = array(
					//                        'notification_from' => $user_account['id'],
					'notification_to' => $get_education_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_education_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_education_data[0]->user_id_fk, 'education_id' => $feed_id, 'flag' => 'Report')));
				}
				/*                 * ****** */
			}
			if ($type == 'job') {
				$arr_job_data = JobModel::find($feed_id);
				$name = 'Job';
				$title = $arr_job_data['title'];

				if (count(DB::table('mst_make_report')->where('job_id', $feed_id)->where('user_id_fk', $user_id)->get()) > 0) {
					$error_code = '1';
					$update = DB::table('mst_make_report')
						->where('job_id', $feed_id)
						->where('user_id_fk', $user_id)
						->update(
							['report_type' => $report]);
				} else {
					$insert = DB::table('mst_make_report')
						->insert(
							['user_id_fk' => $user_id,
								'job_id' => $feed_id,
								'report_type' => $report]
						);
				}
				/*                 * *****notification****** */
				$get_job_data = DB::table('mst_job')
					->where('id', $feed_id)
					->get();
				$subject = 'Your job "' . $get_job_data[0]->title . '" has been reported as "' . $report_name . '"';
				$link = 'view-job/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_job_data[0]->title) . '-' . $get_job_data[0]->id;
				$message = 'Your job "' . $get_job_data[0]->title . '" has been reported as "' . $report_name . '"';

				$insert_data = array(
					//            'notification_from' => $data['user_session']['id'],
					'notification_to' => $get_job_data[0]->user_id_fk,
					'notification_type' => "6",
					'subject' => $subject,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
					'link' => $link,
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				/*                 * ******push Notification******** */
				$arr_to_user = User::where('id', '=', $get_job_data[0]->user_id_fk)->get();
				if ($arr_to_user[0]->device_name == "1") {
					//ios
					//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
					$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
				} else {
					//android
					$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_job_data[0]->user_id_fk, 'job_id' => $feed_id, 'flag' => 'Report')));
				}
				/*                 * ****************** */
			}

			$array_json_return = array('error_code' => $error_code, 'msg' => 'Success', 'msg' => "Thank you for reporting us.Your report has been sent successfully.");

			$email = $arr_user_data['email'];
			$contact_email = $data['global']['contact_email'];
			$macros_array_detail = array();
			$macros_array_detail = email_template_macros::all();
			$macros_array = array();
			foreach ($macros_array_detail as $row) {
				$macros_array[$row['macros']] = $row['value'];
			}
			$email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

			$arr_admin_detail = User::where('email', $contact_email)->first();
			$admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
			$content = $email_contents['email_template_content'];

			$reserved_words = array();
			$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
			$reserved_arr = array(
				"||SITE_TITLE||" => stripslashes($data['global']['site_title']),
				"||SITE_PATH||" => url('/'),
				"||ADMIN_NAME||" => $arr_admin_detail['user_name'],
				"||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
				"||USER_EMAIL||" => $arr_admin_detail['email'],
				"||PROGRAMME_TYPE||" => $name,
				"||PROGRAMME_TITLE||" => ucwords($title),
				"||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
				"||LOGIN_LINK||" => $admin_login_link,
			);
			if ($report_name != '') {
				$reserved_arr['||NAME||'] = ' as ' . $report_name;
			}
			$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
			foreach ($reserved_words as $k => $v) {
				$content = str_replace($k, $v, $content);
			}
			$contents_array = array("email_conents" => $content);

			Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($email, $email_contents, $data) {
				$message->from($email, $data['global']['site_title']);
				$message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
			});
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode($array_json_return);
	}
/** its already done */
	protected function wsSelectedGroup() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if ($user_id != '') {
			$arr_group = DB::table('trans_group_users as tg')
				->join('mst_group as g', 'g.id', '=', 'tg.group_id_fk')
				->where('tg.user_id_fk', $user_id)
				->groupBy('g.id')
				->select('*')
				->get();
			$array_json_return = array('error_code' => '0', 'msg' => 'Succsess', 'Selected Group' => $arr_group);
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'No input data');
		}
		echo json_encode($array_json_return);
	}

	/*     * ****ARTICLE STEPS****** */

/**
 * @SWG\Post(
 *   path="/ws-post-article-step-one",
 *   summary="post artical step one",
 *   tags={"article"},
 *   description="step one of posting article",
 *   operationId="post article",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="group id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="already_exist_article_id",
 *       in="formData",
 *       description="if a user click on back then this id will help to remove duplicate entries",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="description",
 *       in="formData",
 *       description="description of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="Success: JSON file returned")
 * )
 **/
	public function wsPostArticleStepOne() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if ($user_id != '') {
			$arr_user = User::find($user_id);
			$request['description'] = $this->fill_br($request['description']);

			/*             * ***this is manage for disign (repteted data was inserted after clicking on back button and save again)***** */
			if (!empty($request['already_exist_article_id'])) {
				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
				);
				$already_exist_article_id = $request['already_exist_article_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_article_group')->where('article_id_fk', $already_exist_article_id)->delete();
					foreach ($group_id as $value) {
						$article_group = new groupArticle;
						$article_group->group_id_fk = $value;
						$article_group->article_id_fk = $already_exist_article_id;
						$article_group->save();
					}
				}
				ArticleModel::where('id', $already_exist_article_id)->update($arr_update);
				$Article = ArticleModel::where('id', $already_exist_article_id)->first();
				
				if(isset($Article->profile_image) && !empty($Article->profile_image))
					$Article->profile_image = config('feature_pic_url').'article_image/'.$Article->profile_image;
				$array_json_return = array("error_code" => 0, "msg" => "Success", 'article_id' => $already_exist_article_id, 'user_data' => $arr_user, 'article_data' => $Article);
			} else {

				if (!is_null($arr_user)) {

					$Article = new ArticleModel;
					$Article->user_id_fk = $user_id;
					$Article->title = $request['title'];
					$Article->description = $request['description'];
                    $Article->status = "1";
                    $Article->save();
					$lastInsertId = $Article->id;

					/*                     * ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {

						foreach ($group_id as $value) {
							$article_group = new groupArticle;
							$article_group->group_id_fk = $value;
							$article_group->article_id_fk = $lastInsertId;
							$article_group->save();
						}
					}
					
				if(isset($Article->profile_image) && !empty($Article->profile_image))
					$Article->profile_image = config('feature_pic_url').'article_image/'.$Article->profile_image;

					$array_json_return = array("error_code" => 0, "msg" => "Success", 'article_id' => $lastInsertId, 'user_data' => $arr_user, 'article_data' => $Article);
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

/**
 * @SWG\Post(
 *   path="/ws-post-article-step-two",
 *   summary="post artical step two",
 *   tags={"article"},
 *   description="step two of posting article",
 *   operationId="post article",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="article_id",
 *       in="formData",
 *       description="article id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="tags",
 *       in="formData",
 *       description="tags",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="article_img",
 *       in="formData",
 *       description="image for article",
 *       required=false,
 *       type="file",
 *       @SWG\Items(type="string")
 *   ),
 *   @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:Value can't be empty, 2:Image Not Supported")
 * )
 **/
	public function wsPostArticleStepTwo() {
		$request = Input::all();
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$user_id = $request['user_id'];
		$article_id = $request['article_id'];
		$tags = $request['tags'];
		if ($user_id != '') {

			$arr_article = ArticleModel::where('id', $article_id)->first();
			if (count($arr_article) > 0) {

				$arr_article->tags = $tags;
				$arr_article->status = '1';
				/*                 * ***image***** */
				if (isset($request['article_img']) && $request['article_img'] != "") {
					$image_upload_success = $this->UploadCoverImagefx(\Input::file('article_img'), 'article');
					if ($image_upload_success["return_code"] == 0) {
						$arr_article->profile_image = trim($image_upload_success["msg"]);
					} else if ($image_upload_success["return_code"] == 1) {
						$array_json_return = array("error_code" => 2, "msg" => "Image is not supported", 'article_id' => $arr_article['id']);
						echo json_encode($array_json_return);
					}
				}
				$arr_article->save();
				$title = $request['title'];
				$link = url('/') . '/view-article/' . $title . '-' . $article_id;
				
				if(isset($arr_article->profile_image) && !empty($arr_article->profile_image))
					$arr_article->profile_image = config('feature_pic_url').'article_image/'.$arr_article->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'article_id' => $arr_article['id'], 'title' => $arr_article['title'], 'article_image' => $arr_article['profile_image'], 'link' => $link);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ****EVENT STEPS****** */

/**
 * @SWG\Post(
 *   path="/ws-post-event-step-one",
 *   summary="post event step one",
 *   tags={"event"},
 *   description="step one of posting event",
 *   operationId="post event",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="group id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="already_exist_event_id",
 *       in="formData",
 *       description="if a user click on back then this id will help to remove duplicate entries",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the event",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *   @SWG\Parameter(
 *       name="description",
 *       in="formData",
 *       description="description of the event",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="venue",
 *       in="formData",
 *       description="venue of the event",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="city",
 *       in="formData",
 *       description="city of the event",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="start_date",
 *       in="formData",
 *       description="start date of the post, FORMAT : 2018-01-18",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="start_time",
 *       in="formData",
 *       description="start time of the post FORMAT : 02:15 pm",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="end_date",
 *       in="formData",
 *       description="end date of the post",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="end_time",
 *       in="formData",
 *       description="end time of the post FORMAT : 02:15 pm",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="Success: JSON file returned")
 * )
 **/
	public function wsPostEventStepOne() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if (!isset($request['end_date'])) {
			$request['end_date'] = "";

		}
		if (!isset($request['end_time'])) {
			$request['end_time'] = "";
		}
		if ($user_id != '') {
			$arr_user = User::find($user_id);
			$request['description'] = $this->fill_br($request['description']);
			/*             * ***this is manage for disign (repteted data was inserted after clicking on back button and save again)***** */
			if (!empty($request['already_exist_event_id'])) {
				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
					'venue' => $request['venue'],
					'location' => $request['city'],
					'start_date' => date('Y-m-d', strtotime($request['start_date'])),
					'start_time' => $request['start_time'],
					'end_date' => date('Y-m-d', strtotime($request['end_date'])),
					'end_time' => $request['end_time'],
				);
				$already_exist_event_id = $request['already_exist_event_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_event_group')->where('event_id_fk', $already_exist_event_id)->delete();
					foreach ($group_id as $value) {
						$event_group = new groupEvent;
						$event_group->group_id_fk = $value;
						$event_group->event_id_fk = $already_exist_event_id;
						$event_group->save();
					}
				}
				Event::where('id', $already_exist_event_id)->update($arr_update);
				$Event = Event::where('id', $already_exist_event_id)->first();

				
				if(isset($Event->profile_image) && !empty($Event->profile_image))
					$Event->profile_image = config('feature_pic_url').'event_image/'.$Event->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'event_id' => $already_exist_event_id, 'user_data' => $arr_user, 'event_data' => $Event);
			} else {

				if (!is_null($arr_user)) {

					$Event = new Event;
					$Event->user_id_fk = $user_id;
					$Event->title = $request['title'];
					$Event->description = $request['description'];
					$Event->venue = $request['venue'];
					$Event->location = $request['city'];
					$Event->start_date = date('Y-m-d', strtotime($request['start_date']));
					$Event->start_time = $request['start_time'];
					$Event->end_date = date('Y-m-d', strtotime($request['end_date']));
					$Event->end_time = $request['end_time'];
                    $Event->status = "1";
                    $Event->save();
					$lastInsertId = $Event->id;

					/*                     * ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {

						foreach ($group_id as $value) {
							$event_group = new groupEvent;
							$event_group->group_id_fk = $value;
							$event_group->event_id_fk = $lastInsertId;
							$event_group->save();
						}
					}
					
					if(isset($Event->profile_image) && !empty($Event->profile_image))
						$Event->profile_image = config('feature_pic_url').'event_image/'.$Event->profile_image;

					$array_json_return = array("error_code" => 0, "msg" => "Success", 'event_id' => $lastInsertId, 'user_data' => $arr_user, 'event_data' => $Event);
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-post-event-step-two",
	 *   summary="post event step two",
	 *   tags={"event"},
	 *   description="step two of posting event",
	 *   operationId="event",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user id of the user who is logged in",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="event_id",
	 *       in="formData",
	 *       description="event id",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="already_exist_event_id",
	 *       in="formData",
	 *       description="already_exist_event_id used to edit existing events",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="contact_name",
	 *       in="formData",
	 *       description="contact_name",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="contact_number",
	 *       in="formData",
	 *       description="contact_number",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="email",
	 *       in="formData",
	 *       description="email",
	 *       required=true,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="min_age",
	 *       in="formData",
	 *       description="min_age",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="max_age",
	 *       in="formData",
	 *       description="max_age",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="website",
	 *       in="formData",
	 *       description="website",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="terms_condition",
	 *       in="formData",
	 *       description="terms_condition",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="agenda_title",
	 *       in="formData",
	 *       description="agenda_title",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="currency",
	 *       in="formData",
	 *       description="currency and cost both go in conjunction; Either none should be passed or both",
	 *       required=false,
	 *       type="string",
	 *       @SWG\Items(type="string")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="cost",
	 *       in="formData",
	 *       description="cost and currency both go in conjunction; Either none should be passed or both",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="arr_cost",
	 *       in="formData",
	 *       description="Advanced Cost Structure to be passed as json array. [{advanced_category:string, advanced_description:string, advanced_cost:string, advanced_currency:integer, advanced_last_date:string YYYY-MM-DD}]",
	 *       required=false,
	 *       type="array",
	 * 		 @SWG\Items(
	 *			 type="string",
	 *       	 @SWG\Property(property="advanced_category", type="string"),
	 *       	 @SWG\Property(property="advanced_description", type="string"),
	 *       	 @SWG\Property(property="advanced_cost", type="string"),
	 *       	 @SWG\Property(property="advanced_last_date", type="string"),
	 *       	 @SWG\Property(property="advanced_currency", type="integer"),
	 *	 	 )
	 *   ),
	 *     @SWG\Response(response="200", description="0:Success, 1:Error")
	 * )
	 **/
	public function wsPostEventStepTwo() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$user_id = $request['user_id'];
		$event_id = $request['event_id'];

		if ($user_id != '' && $event_id != '') {

			$arr_event = Event::find($event_id);
			if (count($arr_event) > 0) {
				$arr_event->contact_name = $request['contact_name'];
				$arr_event->contact_number = $request['contact_number'];
				$arr_event->email_address = $request['email'];
				if (isset($request['min_age'])) {
					$arr_event->min_age = $request['min_age'];
				}
				if (isset($request['max_age'])) {
					$arr_event->max_age = $request['max_age'];
				}
				if (isset($request['website'])) {
					$arr_event->website = $request['website'];
				}
				if (isset($request['terms_condition'])) {
					$arr_event->terms_condition = $request['terms_condition'];
				}
				if (isset($request['agenda_title'])) {
					$arr_event->agenda = $request['agenda_title'];
				}
				if (isset($request['currency']) && isset($request['cost'])) {
					$arr_event->currency = $request['currency'];
					$arr_event->cost = $request['cost'];
				} else {
					$arr_event->currency = '';
					$arr_event->cost = '0';
				}

				$arr_event->save();
				$lastInsertId = $arr_event['id'];

				if (!empty($request['arr_cost'])) {
					$arr_cost_temp = $request['arr_cost'];
					$arr_advance_cost = json_decode($arr_cost_temp);
					if ($arr_advance_cost != '') {
						DB::table('mst_event_cost')->where('event_id_fk', $lastInsertId)->delete();
						foreach ($arr_advance_cost as $value) {
							$event_cost = new eventCost;
							$event_cost->event_id_fk = $lastInsertId;
							$event_cost->category = $value->advanced_category;
							$event_cost->description = $value->advanced_description;
							$event_cost->cost = $value->advanced_cost;
							$event_cost->currency = $value->advanced_currency;
							$event_cost->last_date = date('Y-m-d', strtotime($value->advanced_last_date));
							$event_cost->save();
						}
					}
				}
				if (!empty($request['arr_agenda'])) {
					$arr_agenda_temp = $request['arr_agenda'];
					$arr_agenda = json_decode($arr_agenda_temp);
					if (count($arr_agenda) > 0) {
						DB::table('mst_event_agenda')->where('event_id_fk', $lastInsertId)->delete();
						foreach ($arr_agenda as $agenda) {

							$event_agenda = new eventAgenda;
							$event_agenda->event_id_fk = $lastInsertId;
							$event_agenda->start_date = date('Y-m-d', strtotime($agenda->start_date));
							$event_agenda->start_time = $agenda->start_time;
							$event_agenda->save();
							$agenda_id = $event_agenda['id'];

							foreach ($agenda->multiple_session as $key1 => $session) {

								$ses_time = new agendaEventSessionTime;
								$ses_time->agenda_id_fk = $agenda_id;
								$ses_time->session = $session->session;
								$ses_time->time = $session->time;
								//                        $ses_time->time = $agenda['time'][$key1];
								$ses_time->save();
							}
						}
					}
				}

				if (!empty($request['arr_guest'])) {
					$arr_guest_temp = $request['arr_guest'];
					$arr_to_return_guest = json_decode($arr_guest_temp);
					if ($arr_to_return_guest != '') {
						$event_data = new eventGuest();
						$pre_event_delete = eventGuest::where('event_id_fk', $event_id)->delete();
						foreach ($arr_to_return_guest as $key => $guest_value) {
							$event_guest = new eventGuest;
							$event_guest->event_id_fk = $lastInsertId;
							$event_guest->name = $guest_value->guest_name;
							$event_guest->designation = $guest_value->guest_designation;
							$event_guest->profile_image = $guest_value->guest_image;
							$event_guest->save();
						}
					}
				}
				
				if(isset($arr_event->profile_image) && !empty($arr_event->profile_image))
					$arr_event->profile_image = config('feature_pic_url').'event_image/'.$arr_event->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'event_id' => $lastInsertId, 'title' => $arr_event['title'], 'event_image' => $arr_event['profile_image']);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	public function wsPostEventStepThree() {
		$request = Input::all();
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$user_id = $request['user_id'];
		$event_id = $request['event_id'];

		if ($user_id != '') {
			$arr_event = Event::find($event_id);
			if (count($arr_event) > 0) {

				$arr_event->tag = $request['tag'];

				/*                 * ***image***** */
				if (isset($request['event_image']) && $request['event_image'] != "") {
					$image_upload_success = $this->UploadCoverImagefx(\Input::file('event_image'), 'event');
					if ($image_upload_success["return_code"] == 0) {
						$arr_event->profile_image = trim($image_upload_success["msg"]);
					} else if ($image_upload_success["return_code"] == 1) {
						$array_json_return = array("error_code" => 2, "msg" => "Image is not supported", 'event_id' => $arr_event['id']);
						echo json_encode($array_json_return);
					}
				}

				$arr_event->save();
				$title = $request['title'];
				$link = url('/') . '/view-event/' . $title . '-' . $event_id;

				
				if(isset($arr_event->profile_image) && !empty($arr_event->profile_image))
					$arr_event->profile_image = config('feature_pic_url').'event_image/'.$arr_event->profile_image;


				$array_json_return = array("error_code" => 0, "msg" => "Success", 'event_id' => $arr_event['id'], 'title' => $arr_event['title'], 'event_image' =>$arr_event['profile_image'], 'link' => $link);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ****Meetup STEPS****** */

	public function wsPostMeetupStepOne() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if ($user_id != '') {
			$arr_user = User::find($user_id);
			$request['description'] = $this->fill_br($request['description']);
			if (!empty($request['already_exist_meetup_id'])) {
				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
					'venue' => $request['venue'],
					'location' => $request['city'],
					'start_date' => date('Y-m-d', strtotime($request['start_date'])),
					'start_time' => $request['start_time'],
					'end_date' => date('Y-m-d', strtotime($request['end_date'])),
					'end_time' => $request['end_time'],
				);
				$already_exist_meetup_id = $request['already_exist_meetup_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_meetup_group')->where('meetup_id_fk', $already_exist_meetup_id)->delete();
					foreach ($group_id as $value) {
						$meetup_group = new groupMeetup;
						$meetup_group->group_id_fk = $value;
						$meetup_group->meetup_id_fk = $already_exist_meetup_id;
						$meetup_group->save();
					}
				}

				$temp_rec_day = explode(',', $request['requaring_day']);

				if ($temp_rec_day != '' && isset($temp_rec_day)) {
					transmeetuprecurringday::where('meetup_id_fk', $already_exist_meetup_id)->delete();
					foreach ($temp_rec_day as $temp_rec) {
						$meet_res_day = new transmeetuprecurringday;
						$meet_res_day->recurring_day = $temp_rec;
						$meet_res_day->meetup_id_fk = $already_exist_meetup_id;
						$meet_res_day->save();
					}
				}

				meetup_model::where('id', $already_exist_meetup_id)->update($arr_update);
				$Meetup = meetup_model::where('id', $already_exist_meetup_id)->first();
				
				if(isset($arr_event->profile_image) && !empty($arr_event->profile_image))
					$arr_event->profile_image = config('feature_pic_url').'event_image/'.$arr_event->profile_image;


				$array_json_return = array("error_code" => 0, "msg" => "Success", 'meetup_id' => $already_exist_meetup_id, 'user_data' => $arr_user, 'meetup_data' => $Meetup);
			} else {
				if (!is_null($arr_user)) {

					$Meetup = new meetup_model;
					$Meetup->user_id_fk = $user_id;
					$Meetup->title = $request['title'];
					$Meetup->description = $request['description'];
					$Meetup->venue = $request['venue'];
					$Meetup->location = $request['city'];
					$Meetup->start_date = date('Y-m-d', strtotime($request['start_date']));
					$Meetup->start_time = $request['start_time'];
					$Meetup->end_date = date('Y-m-d', strtotime($request['end_date']));
					$Meetup->end_time = $request['end_date'];
					$Meetup->status = "1";
					$Meetup->save();
					$lastInsertId = $Meetup->id;

					/*                     * ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {

						foreach ($group_id as $value) {
							$meetup_group = new groupMeetup;
							$meetup_group->group_id_fk = $value;
							$meetup_group->meetup_id_fk = $lastInsertId;
							$meetup_group->save();
						}
					}

					$temp_rec_day = explode(',', $request['requaring_day']);

					if ($temp_rec_day != '' && isset($temp_rec_day)) {
						foreach ($temp_rec_day as $temp_rec) {
							$meet_res_day = new transmeetuprecurringday;
							$meet_res_day->recurring_day = $temp_rec;
							$meet_res_day->meetup_id_fk = $lastInsertId;
							$meet_res_day->save();
						}
					}

				if(isset($Meetup->profile_image) && !empty($Meetup->profile_image))
					$Meetup->profile_image = config('feature_pic_url').'meetup_image/'.$Meetup->profile_image;


					$array_json_return = array("error_code" => 0, "msg" => "Success", 'meetup_id' => $lastInsertId, 'user_data' => $arr_user, 'meetup_data' => $Meetup);
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	public function wsPostMeetupStepTwo() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$user_id = $request['user_id'];
		$meetup_id = $request['meetup_id'];

		if ($user_id != '' && $meetup_id != '') {

			$arr_meetup = meetup_model::find($meetup_id);
			if (count($arr_meetup) > 0) {
				$arr_meetup->contact_name = $request['contact_name'];
				$arr_meetup->contact_number = $request['contact_number'];
				$arr_meetup->email_address = $request['email'];
				$arr_meetup->min_age = $request['min_age'];
				$arr_meetup->max_age = $request['max_age'];
				$arr_meetup->website = $request['website'];
				$arr_meetup->terms_condition = $request['terms_condition'];
				if (isset($request['agenda_title'])) {
					$arr_meetup->agenda = $request['agenda_title'];
				}
				if (isset($request['currency']) && isset($request['cost'])) {
					$arr_meetup->currency = $request['currency'];
					$arr_meetup->cost = $request['cost'];
				} else {
					$arr_meetup->currency = '';
					$arr_meetup->cost = '0';
				}

				$arr_meetup->save();
				$lastInsertId = $arr_meetup['id'];

				if (!empty($request['arr_agenda'])) {
					$arr_agenda_temp = $request['arr_agenda'];
					$arr_agenda = json_decode($arr_agenda_temp);
					if (count($arr_agenda) > 0) {
						DB::table('mst_meetup_agenda')->where('meetup_id_fk', $lastInsertId)->delete();
						foreach ($arr_agenda as $agenda) {

							$meetup_agenda = new meetupAgenda;
							$meetup_agenda->meetup_id_fk = $lastInsertId;
							$meetup_agenda->start_date = date('Y-m-d', strtotime($agenda->start_date));
							$meetup_agenda->start_time = $agenda->start_time;
							$meetup_agenda->save();
							$agenda_id = $meetup_agenda['id'];

							foreach ($agenda->multiple_session as $key1 => $session) {

								$ses_time = new agendaSessionTime;
								$ses_time->agenda_id_fk = $agenda_id;
								$ses_time->session = $session->session;
								$ses_time->time = $session->time;
								//                        $ses_time->time = $agenda['time'][$key1];
								$ses_time->save();
							}
						}
					}
				}

				if (!empty($request['arr_guest'])) {
					$arr_guest = $request['arr_guest'];
					$arr_to_return_guest = json_decode($arr_guest);
					if ($arr_to_return_guest != '') {
						$meetup_gue = new meetupGuest();
						$pre_meetup_delete = meetupGuest::where('meetup_id_fk', $meetup_id)->delete();
						foreach ($arr_to_return_guest as $key => $guest_value) {
							$meetup_guest = new meetupGuest;

							$meetup_guest->meetup_id_fk = $lastInsertId;
							$meetup_guest->name = $guest_value->guest_name;
							$meetup_guest->designation = $guest_value->guest_designation;
							$meetup_guest->profile_image = $guest_value->guest_image;
							$meetup_guest->save();
						}
					}
				}

				if (!empty($request['arr_cost'])) {
					$temp = $request['arr_cost'];
					$arr_advance_cost = json_decode($temp);
					if ($arr_advance_cost != '') {
						DB::table('mst_meetup_cost')->where('meetup_id_fk', $lastInsertId)->delete();
						foreach ($arr_advance_cost as $value) {
							$meetup_cost = new meetupCost;
							$meetup_cost->meetup_id_fk = $lastInsertId;
							$meetup_cost->category = $value->advanced_category;
							$meetup_cost->description = $value->advanced_description;
							$meetup_cost->cost = $value->advanced_cost;
							$meetup_cost->currency = $value->advanced_currency;
							$meetup_cost->last_date = date('Y-m-d', strtotime($value->advanced_last_date));
							$meetup_cost->save();
						}
					}
				}

				
				if(isset($arr_meetup->profile_image) && !empty($arr_meetup->profile_image))
					$arr_meetup->profile_image = config('feature_pic_url').'meetup_image/'.$arr_meetup->profile_image;


				$array_json_return = array("error_code" => 0, "msg" => "Success", 'meetup_id' => $lastInsertId, 'title' => $arr_meetup['title'], 'meetup_image' => $arr_meetup['profile_image']);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	public function wsPostMeetupStepThree() {
		$request = Input::all();
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$user_id = $request['user_id'];
		$meetup_id = $request['meetup_id'];
		if ($user_id != '') {

			$arr_meetup = meetup_model::find($meetup_id);
			if (count($arr_meetup) > 0) {

				$arr_meetup->tag = $request['tags'];

				/*                 * ***image***** */
				if (isset($request['meetup_image']) && $request['meetup_image'] != "") {
					$image_upload_success = $this->UploadCoverImagefx(\Input::file('meetup_image'), 'meetup');
					if ($image_upload_success["return_code"] == 0) {
						$arr_meetup->profile_image = trim($image_upload_success["msg"]);
					} else if ($image_upload_success["return_code"] == 1) {
						$array_json_return = array("error_code" => 2, "msg" => "Image is not supported", 'meetup_id' => $arr_meetup['id']);
						echo json_encode($array_json_return);
					}
				}
				$arr_meetup->save();
				$title = $request['title'];
				$link = url('/') . '/view-meetup/' . $title . '-' . $meetup_id;
				
				if(isset($arr_meetup->profile_image) && !empty($arr_meetup->profile_image))
					$arr_meetup->profile_image = config('feature_pic_url').'meetup_image/'.$arr_meetup->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'meetup_id' => $arr_meetup['id'], 'title' => $arr_meetup['title'], 'meetup_image' => $arr_meetup['profile_image'], 'link' => $link);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ****QRIOUS STEPS****** */


/**
 * @SWG\Post(
 *   path="/ws-post-qrious-step-one",
 *   summary="Post Qrious step one",
 *   tags={"Qrious"},
 *   description="step one of posting qrious",
 *   operationId="post Qrious",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="group id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="already_exist_qrious_id",
 *       in="formData",
 *       description="if a user click on back then this id will help to remove duplicate entries",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="description",
 *       in="formData",
 *       description="description of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="Success: JSON file returned")
 * )
 **/
	public function wsPostQriousStepOne() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if ($user_id != '') {
			$arr_user = User::find($user_id);
			$request['description'] = $this->fill_br($request['description']);
			if (!empty($request['already_exist_qrious_id'])) {
				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
				);
				$already_exist_qrious_id = $request['already_exist_qrious_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_question_group')->where('question_id_fk', $already_exist_qrious_id)->delete();
					foreach ($group_id as $value) {
						$qrious_group = new groupQuestion;
						$qrious_group->group_id_fk = $value;
						$qrious_group->question_id_fk = $already_exist_qrious_id;
						$qrious_group->save();
					}
				}
				QuestionModel::where('id', $already_exist_qrious_id)->update($arr_update);
				$Qrious = QuestionModel::where('id', $already_exist_qrious_id)->first();
				
				if(isset($Qrious->profile_image) && !empty($Qrious->profile_image))
					$Qrious->profile_image = config('feature_pic_url').'qrious_image/'.$Qrious->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'qrious_id' => $already_exist_qrious_id, 'user_data' => $arr_user, 'qrious_data' => $Qrious);
			} else {
				if (!is_null($arr_user)) {

					$Qrious = new QuestionModel;
					$Qrious->user_id_fk = $user_id;
					$Qrious->title = $request['title'];
					$Qrious->description = $request['description'];
					$Qrious->status = "1";
					$Qrious->save();
					$lastInsertId = $Qrious->id;

					/*                     * ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {

						foreach ($group_id as $value) {
							$qrious_group = new groupQuestion;
							$qrious_group->group_id_fk = $value;
							$qrious_group->question_id_fk = $lastInsertId;
							$qrious_group->save();
						}
					}
					
				if(isset($Qrious->profile_image) && !empty($Qrious->profile_image))
				$Qrious->profile_image = config('feature_pic_url').'qrious_image/'.$Qrious->profile_image;

					$array_json_return = array("error_code" => 0, "msg" => "Success", 'qrious_id' => $lastInsertId, 'user_data' => $arr_user, 'qrious_data' => $Qrious);
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

/**
 * @SWG\Post(
 *   path="/ws-post-qrious-step-two",
 *   summary="post artical step two",
 *   tags={"Qrious"},
 *   description="step two of posting Qrious",
 *   operationId="post qrious",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="qrious_id",
 *       in="formData",
 *       description="qrious id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="tags",
 *       in="formData",
 *       description="tags",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="qrious_img",
 *       in="formData",
 *       description="image for qrious",
 *       required=false,
 *       type="file",
 *       @SWG\Items(type="string")
 *   ),
 *   @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:Value can't be empty, 2:Image Not Supported")
 * )
 **/
	public function wsPostQriousStepTwo() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$user_id = $request['user_id'];
		$title = $request['title'];
		$qrious_id = $request['qrious_id'];

		if ($user_id != '') {

			$arr_qrious = QuestionModel::where('id', $qrious_id)->first();
			if (count($arr_qrious) > 0) {

				$arr_qrious->tags = $request['tags'];
				$arr_qrious->status = '1';
				/*                 * ***image***** */
				if (isset($request['qrious_image']) && $request['qrious_image'] != "") {
					$image_upload_success = $this->UploadCoverImagefx(\Input::file('qrious_image'), 'qrious');
					if ($image_upload_success["return_code"] == 0) {
						$arr_qrious->profile_image = trim($image_upload_success["msg"]);
					} else if ($image_upload_success["return_code"] == 1) {
						$array_json_return = array("error_code" => 2, "msg" => "Image is not supported", 'qrious_id' => $arr_qrious['id']);
						echo json_encode($array_json_return);
					}
				}
				$arr_qrious->save();
				$link = url('/') . '/view-question/' . $title . '-' . $qrious_id;
				
				if(isset($arr_qrious->profile_image) && !empty($arr_qrious->profile_image))
					$arr_qrious->profile_image = config('feature_pic_url').'qrious_image/'.$arr_qrious->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'qrious_id' => $arr_qrious['id'], 'title' => $arr_qrious['title'], 'qrious_image' => $arr_qrious['profile_image'], 'link' => $link);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}


/**
 * @SWG\Post(
 *   path="/ws-post-education-step-one",
 *   summary="post education step one",
 *   tags={"education"},
 *   description="step one of posting education",
 *   operationId="post education",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="comma separated group id(s) e.g. 81,82. Can be left blank.",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="already_exist_education_id",
 *       in="formData",
 *       description="if a user click on back then this id will help to remove duplicate entries",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the education",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *   @SWG\Parameter(
 *       name="description",
 *       in="formData",
 *       description="description of the education",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="website",
 *       in="formData",
 *       description="website",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="education_type",
 *       in="formData",
 *       description="Type of education. Values : Podcast, Document, Video, Audio, Webinar, Online Classes, Regional Classes, Other",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *     @SWG\Response(response="200", description="Success: JSON file returned")
 * )
 **/
	public function wsPostEducationStepOne() {
		$request = Input::all();
		$user_id = $request['user_id'];

		if ($user_id != '') {
			$arr_user = User::find($user_id);
			$request['description'] = $this->fill_br($request['description']);
			if (!empty($request['already_exist_education_id'])) {

				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
					'education_type' => (isset($request['education_type']) ? $request['education_type'] : ''),
					'website' => (isset($request['website']) ? $request['website'] : ''),
				);
				$already_exist_education_id = $request['already_exist_education_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_education_group')->where('education_id_fk', $already_exist_education_id)->delete();
					foreach ($group_id as $value) {
						DB::table('trans_education_group')->insert(
							['group_id_fk' => $value, 'education_id_fk' => $already_exist_education_id]
						);
					}
				}
				if (!empty($request['arr_cost'])) {
					$temp = $request['arr_cost'];
					$arr_advance_cost = json_decode($temp);

					if ($arr_advance_cost != '') {
						DB::table('mst_education_cost')->where('education_id_fk', $already_exist_education_id)->delete();
						foreach ($arr_advance_cost as $value) {
							DB::table('mst_education_cost')->insert(
								['education_id_fk' => $already_exist_education_id,
									'category' => $value->advanced_category,
									'description' => $value->advanced_description,
									'cost' => $value->advanced_cost,
									'currency' => $value->advanced_currency,
									'last_date' => date('Y-m-d', strtotime($value->advanced_last_date))]
							);
						}
					}
				}
				education_model::where('id', $already_exist_education_id)->update($arr_update);
				$Education = education_model::where('id', $already_exist_education_id)->first();
				
				if(isset($Education->profile_image) && !empty($Education->profile_image))
					$Education->profile_image = config('feature_pic_url').'education_image/'.$Education->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'education_id' => $already_exist_education_id, 'user_data' => $arr_user, 'education_data' => $Education);
			} else {

				if (!is_null($arr_user)) {

					$Education = new education_model;
					$Education->user_id_fk = $user_id;
					$Education->title = $request['title'];
					$Education->description = $request['description'];
					$Education->education_type = (isset($request['education_type']) ? $request['education_type'] : '' );
					$Education->website = (isset($request['website']) ? $request['website'] : '');

					if (isset($request['currency']) && isset($request['cost'])) {
						$Education->currency = $request['currency'];
						$Education->cost = $request['cost'];
					} else {
						$Education->currency = '';
						$Education->cost = '0';
					}

					$Education->status = "1";
					$Education->save();
					$lastInsertId = $Education->id;

					if (!empty($request['arr_cost'])) {
						$temp = $request['arr_cost'];
						$arr_advance_cost = json_decode($temp);

						if ($arr_advance_cost != '') {
							DB::table('mst_education_cost')->where('education_id_fk', $lastInsertId)->delete();
							foreach ($arr_advance_cost as $value) {
								DB::table('mst_education_cost')->insert(
									['education_id_fk' => $lastInsertId,
										'category' => $value->advanced_category,
										'description' => $value->advanced_description,
										'cost' => $value->advanced_cost,
										'currency' => $value->advanced_currency,
										'last_date' => date('Y-m-d', strtotime($value->advanced_last_date))]
								);
							}
						}
					}

					/*                     * ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {

						foreach ($group_id as $value) {
							$education_group = new groupEducation;
							$education_group->group_id_fk = $value;
							$education_group->education_id_fk = $lastInsertId;
							$education_group->save();
						}
					}
					
				if(isset($Education->profile_image) && !empty($Education->profile_image))
				$Education->profile_image = config('feature_pic_url').'education_image/'.$Education->profile_image;

					$array_json_return = array("error_code" => 0, "msg" => "Success", 'education_id' => $lastInsertId, 'user_data' => $arr_user, 'education_data' => $Education);
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode(
			$array_json_return);
	}

/**
 * @SWG\Post(
 *   path="/ws-post-education-step-two",
 *   summary="post education step two",
 *   tags={"education"},
 *   description="step two of posting education",
 *   operationId="post education",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="education_image",
 *       in="formData",
 *       description="image for education",
 *       required=false,
 *       type="file",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="education_id",
 *       in="formData",
 *       description="",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the education",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="tags",
 *       in="formData",
 *       description="tags, comma separated",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *     @SWG\Response(response="200", description="Success: JSON file returned")
 * )
 **/
	public function wsPostEducationStepTwo() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$user_id = $request['user_id'];
		$education_id = $request['education_id'];

		if ($user_id != '') {

			$arr_education = education_model::where('id', $education_id)->first();
			if (count($arr_education) > 0) {

				$arr_education->tag = $request['tags'];
				$arr_education->status = '1';
				/*                 * ***image***** */
				if (isset($request['education_image']) && $request['education_image'] != "") {
					$image_upload_success = $this->UploadCoverImagefx(\Input::file('education_image'), 'education');
					if ($image_upload_success["return_code"] == 0) {
						$arr_education->profile_image = trim($image_upload_success["msg"]);
					} else if ($image_upload_success["return_code"] == 1) {
						$array_json_return = array("error_code" => 2, "msg" => "Image is not supported", 'education_id' => $arr_education['id']);
						echo json_encode($array_json_return);
					}
				}
				$arr_education->save();
				$title = $request['title'];
				$link = url('/') . '/view-education/' . $title . '-' . $education_id;
				
				if(isset($arr_education->profile_image) && !empty($arr_education->profile_image))
					$arr_education->profile_image = config('feature_pic_url').'education_image/'.$arr_education->profile_image;

				$array_json_return = array("error_code" => 0, "msg" => "Success", 'education_id' => $arr_education['id'], 'title' => $arr_education['title'], 'education_image' => $arr_education['profile_image'], 'link' => $link);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

/**
 * @SWG\Post(
 *   path="/ws-post-job-step-one",
 *   summary="Post Job step one",
 *   tags={"job"},
 *   description="step one of posting job",
 *   operationId="post job",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="comma separated group id(s) e.g. 81,82",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="already_exist_job_id",
 *       in="formData",
 *       description="if a user click on back then this id will help to remove duplicate entries",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="title",
 *       in="formData",
 *       description="title of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="description",
 *       in="formData",
 *       description="description of the post",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="location",
 *       in="formData",
 *       description="location of the job",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:Fail")
 * )
 **/
	public function wsPostJobStepOne() {
		$request = Input::all();
		$user_id = (isset($request['user_id']) ? $request['user_id'] : '');
		$job_title = (isset($request['title']) ? $request['title'] : '');
		$job_description = (isset($request['description']) ? $request['description'] : '');

		if ($user_id != '' && $job_description != '' && $job_title != '') {
			$arr_user = User::find($user_id);
			$request['description']=$this->fill_br($request['description']);
			if (!empty($request['already_exist_job_id'])) {
				$arr_update = array(
					'user_id_fk' => $user_id,
					'title' => $request['title'],
					'description' => $request['description'],
					'cmp_name' => (isset($request['company_name']) ? $request['company_name'] : ''),
					'website' => (isset($request['company_website']) ? $request['company_website'] : ''),
					'role' => (isset($request['role']) ? $request['role'] : ''),
					'external_link_to_apply' => (isset($request['external_link']) ? $request['external_link'] : ''),
					'phone_number' => (isset($request['mobile_no']) ? $request['mobile_no'] : ''),
					'telephone_number' => (isset($request['telephone_no']) ? $request['telephone_no'] : ''),
					'email_address' => (isset($request['email_id']) ? $request['email_id'] : ''),
					'functional_area' => (isset($request['functional_area']) ? $request['functional_area'] : ''),
					'salary_range' => (isset($request['salary_range']) ? $request['salary_range'] : ''),
					'job_location' => $request['location']
				);

				$job_id = $already_exist_job_id = $request['already_exist_job_id'];
				$group_id = explode(',', $request['group_id']);
				if (isset($request['group_id']) && $request['group_id'] != '') {
					DB::table('trans_job_group')->where('job_id_fk', $already_exist_job_id)->delete();
					foreach ($group_id as $value) {
						DB::table('trans_job_group')->insert(
							['group_id_fk' => $value, 'job_id_fk' => $already_exist_job_id]
						);
					}
				}
				JobModel::where('id', $already_exist_job_id)->update($arr_update);
				$job = JobModel::where('id', $already_exist_job_id)->first();
				$array_json_return = array("error_code" => 0, "msg" => "Success", 'job_id' => $already_exist_job_id, 'user_data' => $arr_user, 'job_data' => $job);
			} else {
				if (!is_null($arr_user)) {

					$Job = new JobModel;
					$Job->user_id_fk = $user_id;
					$Job->title = $request['title'];
					$Job->description = $request['description'];
					//$Job->url = (isset($request['site_url']) ? $request['site_url'] : '');
					$Job->cmp_name = (isset($request['company_name']) ? $request['company_name'] : '');
					$Job->website = (isset($request['company_website']) ? $request['company_website'] : '');
					$Job->role = (isset($request['role']) ? $request['role'] : '');
					$Job->external_link_to_apply = (isset($request['external_link']) ? $request['external_link'] : '');
					$Job->phone_number = (isset($request['mobile_no']) ? $request['mobile_no'] : '');
					$Job->telephone_number = (isset($request['telephone_no']) ? $request['telephone_no'] : '');
					$Job->email_address = (isset($request['email_id']) ? $request['email_id'] : '');
					$Job->functional_area = (isset($request['salary_range']) ? $request['salary_range'] : '');
					$Job->salary_range = (isset($request['salary_range']) ? $request['salary_range'] : '');
					$Job->job_location = $request['location'];
					$Job->status = "1";
			        $application_deadline = (isset($request['dead_line']) ? $request['dead_line'] : date('Y-m-d', strtotime("+30 days")));
			        $Job->application_deadline = $application_deadline;
					$Job->save();
					$job_id = $lastInsertId = $Job->id;

					/** ******for group********* */
					$group_id = explode(',', $request['group_id']);
					if (isset($request['group_id']) && $request['group_id'] != '') {
						foreach ($group_id as $value) {
							$job_group = new JobGroupModel;
							$job_group->group_id_fk = $value;
							$job_group->job_id_fk = $lastInsertId;
							$job_group->save();
						}
					}
					$array_json_return = array("error_code" => 0, "msg" => "Success", 'job_id' => $lastInsertId, 'user_data' => $arr_user, 'job_data' => $Job);
				}
			}
			
			$this->createSEOfx($request);
			$this->createUserActivityRecordfx($request, $job_id, $user_id);

		} else {
			$array_json_return = array("error_code" => 1, "msg" => "Fail");
		}
		echo json_encode($array_json_return);
	}


/**
 * @SWG\Post(
 *   path="/ws-post-job-step-two",
 *   summary="Post Job step two",
 *   tags={"job"},
 *   description="step two of posting job",
 *   operationId="post job",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="job_id",
 *       in="formData",
 *       description="job id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="company_name",
 *       in="formData",
 *       description="",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="company_website",
 *       in="formData",
 *       description="",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="role",
 *       in="formData",
 *       description="Role for which job is being posted",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="external_link",
 *       in="formData",
 *       description="Any external link to apply to the job",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="email_id",
 *       in="formData",
 *       description="Email ID of recruiter",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="dead_line",
 *       in="formData",
 *       description="Application Deadline",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="education",
 *       in="formData",
 *       description="Desired education/skills",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="min_exp",
 *       in="formData",
 *       description="Minimum Experience required by applicant",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="location",
 *       in="formData",
 *       description="Location of job",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="tags",
 *       in="formData",
 *       description="tags",
 *       required=false,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *  @SWG\Parameter(
 *       name="emp_type",
 *       in="formData",
 *       description="Employment Type, values - 1:FullTime,2:PartTime,3:Internship,4:Fellowship ",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:Fail")
 * )
 **/
	public function wsPostJobStepTwo() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$user_id = (isset($request['user_id']) ? $request['user_id'] : '');
		$job_id = (isset($request['job_id']) ? $request['job_id'] : '');

		if ($user_id != '' && $job_id != '') {

			$arr_job = JobModel::where('id', $job_id)->first();
			if (count($arr_job) > 0) {
				$arr_job->cmp_name = (isset($request['company_name']) ? $request['company_name'] : '');
				$arr_job->website = (isset($request['company_website']) ? $request['company_website'] : '');
				$arr_job->role = (isset($request['role']) ? $request['role'] : '');
				$arr_job->external_link_to_apply = (isset($request['external_link']) ? $request['external_link'] : '');
				//$arr_job->phone_number = (isset($request['mobile_no']) ? $request['mobile_no'] : '');
				//$arr_job->telephone_number = (isset($request['telephone_no']) ? $request['telephone_no'] : '');
				$arr_job->email_address = (isset($request['email_id']) ? $request['email_id'] : '');
				//$arr_job->functional_area = (isset($request['functional_area']) ? $request['functional_area'] : '');
				//$arr_job->salary_range = (isset($request['salary_range']) ? $request['salary_range'] : '');
		        $arr_job->application_deadline = (isset($request['dead_line']) ? $request['dead_line'] : '');
				$arr_job->status = '1';
				$arr_job->preferred_qualification = (isset($request['education']) ? $request['education'] : '');
				//$arr_job->skill = (isset($request['skills']) ? $request['skills'] : '');
				$arr_job->min = (isset($request['min_exp']) ? $request['min_exp'] : '');
				//$arr_job->max = (isset($request['max_exp']) ? $request['max_exp'] : '');
				$arr_job->job_location = (isset($request['location']) ? $request['location'] : '');
				// $arr_job->about_us = $request['about_us'];
				// $arr_job->contact_us = $request['contact_us'];
				// $arr_job->how_to_apply = $request['how_to_apply'];
				// $arr_job->hiring_process = $request['hiring_process'];
				$arr_job->tags = (isset($request['tags']) ? $request['tags'] : '');
				$arr_job->employment_type = (isset($request['emp_type']) ? $request['emp_type'] : '');
				$arr_job->save();

				$link = url('/') . '/view-job/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_job['title']) . '-' . $job_id;
				$array_json_return = array("error_code" => 0, "msg" => "Success", 'job_id' => $arr_job['id'], 'title' => $arr_job['title'], 'link' => $link, 'arr_job' => $arr_job);
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "Fail");
		}
		echo json_encode($array_json_return);
	}

	/*     * ******for my Gallary********* */

	public function wsGallaryFeeds() {
		$request = Input::all();
		$type = $request['type'];

		/*         * *********** */
		$user_id = $request['user_id'];
		$arr_user_data = User::find($user_id);
		if ($user_id != '' && $type != '') {
			$get_groups = DB::table('trans_group_users')->where('user_id_fk', $user_id)
				->get();
			foreach ($get_groups as $key => $value) {
				$user_group[$key] = $value->group_id_fk;
			}
			$condition = implode(',', $user_group);

			$get_connections = DB::table('trans_user_connection')->where('from_user_id', $user_id)->orWhere('to_user_id', $user_id)->where('status', '1')->select('to_user_id', 'from_user_id')
				->get();

			$condition1 = 'null';
			if (count($get_connections) > 0) {
				$temp_array = array();
				$user_ids = array();
				foreach ($get_connections as $key => $value) {
					$user_ids[] = $value->to_user_id;
					$user_ids[] = $value->from_user_id;
					$arr_ids = array();
					foreach ($user_ids as $ids) {
						if (!in_array($ids, $arr_ids)) {
							$arr_ids[] = $ids;
						}
					}
				}
				$condition1 = implode(',', $arr_ids);
			}

			if ($type == 'article') {
				$arr_gallary_feed = DB::table('mst_article as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id', 'left')
					->where('e.status', '1')
					->WhereIn('e.user_id_fk', explode(',', $condition1))
					->whereIn('g.group_id_fk', explode(',', $condition))
					->groupBy('e.id')
					->select('e.*', 'u.user_name', 'u.profile_picture as profile_picture')
					->get();

				foreach ($arr_gallary_feed as $key => $value) {
					$arr_article_comment = DB::table('mst_comments as mc')
						->join('trans_article_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
						->where('tec.article_id_fk', $arr_gallary_feed[$key]->id)
						->where('mc.parent_id', '==', '0')
						->select('mc.*')
						->get();
					$arr_gallary_feed[$key]->count = count($arr_article_comment);
				}
				$total_count = count($arr_gallary_feed);
			}
			if ($type == 'qrious') {
				$arr_gallary_feed = DB::table('mst_question as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
					->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id', 'left')
					->where('e.status', '1')
					->WhereIn('e.user_id_fk', explode(',', $condition1))
					->whereIn('g.group_id_fk', explode(',', $condition))
					->groupBy('e.id')
					->select('e.*', 'u.user_name')
					->get();

				foreach ($arr_gallary_feed as $key => $value) {

					$arr_questions_answer = DB::table('mst_answers as mc')
						->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
						->where('tec.question_id_fk', $arr_gallary_feed[$key]->id)
						->select('mc.*')
						->get();
					$arr_gallary_feed[$key]->count = count($arr_questions_answer);
				}
				$total_count = count($arr_gallary_feed);
			}
			if ($type == 'event') {
				$arr_gallary_feed = DB::table('mst_event as e')->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left')
					->where('e.status', '=', '1')
					->WhereIn('e.user_id_fk', $arr_ids)
					->whereIn('e.user_id_fk', explode(',', $condition1))
					->OrWhereIn('g.group_id_fk', explode(',', $condition))
					->groupBy('e.id')
					->orderBy('e.updated_at', 'DESC')
					->select('e.*', 'e.title as name', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description', 'e.profile_image as profile_picture')
					->get();

				foreach ($arr_gallary_feed as $key => $value) {

					$arr_event_comment = DB::table('mst_comments as mc')
						->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
						->where('tec.event_id_fk', $arr_gallary_feed[$key]->id)
						->select('mc.*')
						->get();
					$arr_gallary_feed[$key]->count = count($arr_event_comment);
				}
				$total_count = count($arr_gallary_feed);
			}
			if ($type == 'meetup') {
				$arr_gallary_feed = DB::table('mst_meetup as e')->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'e.id', 'left')
					->where('e.status', '1')
					->whereIn('e.user_id_fk', explode(',', $condition1))
					->OrWhereIn('g.group_id_fk', explode(',', $condition))
					->groupBy('e.id')
					->select('e.*', 'e.title as name', 'e.profile_image as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
					->get();
				foreach ($arr_gallary_feed as $key => $value) {
					$meetup_comment = commentMeetup::where('meetup_id_fk', $value->id)->get();
					$arr_gallary_feed[$key]->count = count($meetup_comment);
				}
			}
			if ($type == 'education') {
				$arr_gallary_feed = DB::table('mst_education as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
					->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left')
					->where('e.status', '1')
					->WhereIn('e.user_id_fk', explode(',', $condition1))
					->whereIn('g.group_id_fk', explode(',', $condition))
					->groupBy('e.id')
					->select('e.*', 'u.user_name')
					->get();

				foreach ($arr_gallary_feed as $key => $value) {
					$education_comment = educationCommentModel::where('education_id_fk', $value->id)->get();
					$arr_gallary_feed[$key]->count = count($education_comment);
				}
				$total_count = count($arr_gallary_feed);
			}
			if ($type == 'job') {
				$arr_gallary_feed = DB::table('mst_job as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id', 'left')
					->where('e.status', '1')
					->whereIn('g.group_id_fk', explode(',', $condition))
					->WhereIn('e.user_id_fk', explode(',', $condition1))
					->groupBy('e.id')
					->select('e.*', 'u.user_name')
					->get();
			}
			$array_json_return = array("error_code" => 0, "msg" => "Gallary", 'Gallary Feeds' => $arr_gallary_feed);
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ******for my feeds********* */

	public function wsMyFeeds() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$arr_user_data = User::find($user_id);
		$type = $request['type'];

		if ($user_id != '') {
			if ($type == 'article') {
				$arr_my_feeds = DB::table('mst_article as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
					->where('user_id_fk', $user_id)
					->groupBy('e.id')
					->select('e.*', 'u.user_name', 'u.profile_picture as profile_picture')
					->get();
				foreach ($arr_my_feeds as $key => $value) {

					$arr_article_comment = DB::table('mst_comments as mc')
						->join('trans_article_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
						->where('tec.article_id_fk', $arr_my_feeds[$key]->id)
						->select('mc.*')
						->get();
					$arr_my_feeds[$key]->count = count($arr_article_comment);
				}
			}
			if ($type == 'qrious') {
				$arr_my_feeds = DB::table('mst_question as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
					->where('status', '1')
					->where('user_id_fk', $user_id)
					->groupBy('e.id')
					->select('e.*', 'u.user_name')
					->get();
				foreach ($arr_my_feeds as $key => $value) {

					$arr_questions_answer = DB::table('mst_answers as mc')
						->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
						->where('tec.question_id_fk', $arr_my_feeds[$key]->id)
						->select('mc.*')
						->get();
					$arr_my_feeds[$key]->count = count($arr_questions_answer);
				}
			}
			if ($type == 'event') {
				$arr_my_feeds = DB::table('mst_event as e')
					->where('user_id_fk', $user_id)
					->groupBy('e.id')
					->select('e.*', 'e.title as name', 'e.profile_image as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
					->get();
				foreach ($arr_my_feeds as $key => $value) {
					$arr_event_comment = DB::table('mst_comments as mc')
						->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
						->where('tec.event_id_fk', $arr_my_feeds[$key]->id)
						->select('mc.*')
						->get();
					$arr_my_feeds[$key]->count = count($arr_event_comment);
				}
			}
			if ($type == 'meetup') {
				$arr_my_feeds = DB::table('mst_meetup as e')
					->where('user_id_fk', $user_id)
					->groupBy('e.id')
					->select('e.*', 'e.title as name', 'e.profile_image as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
					->get();
				foreach ($arr_my_feeds as $key => $value) {
					$meetup_comment = commentMeetup::where('meetup_id_fk', $value->id)->get();
					$arr_my_feeds[$key]->count = count($meetup_comment);
				}
			}
			if ($type == 'education') {
				$arr_my_feeds = DB::table('mst_education as e')->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
					->where('user_id_fk', $user_id)
					->groupBy('e.id')
					->select('e.*', 'u.user_name')
					->get();
				foreach ($arr_my_feeds as $key => $value) {
					$education_comment = educationCommentModel::where('education_id_fk', $value->id)->get();
					$arr_my_feeds[$key]->count = count($education_comment);
				}
			}
			if ($type == 'job') {
				$arr_my_feeds = JobModel::where('user_id_fk', $user_id)
					->orderBy('updated_at', 'DESC')
					->get();
			}
			$array_json_return = array("error_code" => 0, "msg" => "My Feeds", 'My Feeds' => $arr_my_feeds);
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ******for saved feeds********* */

	public function wsSavedFeeds() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$type = $request['type'];
		$feed_id = $request['feed_id'];

		if ($user_id != '') {
			if ($type == 'event') {

				$get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $feed_id)->where('type', '1')->first();
				if (count($get_save_user_post) == 0) {

					$add_post = new saveUserPost();
					$add_post->user_id_fk = $user_id;
					$add_post->post_id_fk = $feed_id;
					$add_post->type = '1';
					$add_post->save();
					$array_json_return = array("error_code" => 0, "msg" => "This Event is saved");
				} else {
					$array_json_return = array("error_code" => 1, "msg" => "This Event is already saved");
				}
			}
			if ($type == 'meetup') {

				$get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $feed_id)->where('type', '0')->first();

				if (count($get_save_user_post) == 0) {
					$add_post = new saveUserPost();
					$add_post->user_id_fk = $user_id;
					$add_post->post_id_fk = $feed_id;
					$add_post->type = '0';
					$add_post->save();
					$array_json_return = array("error_code" => 0, "msg" => "This Meetup is saved ");
				} else {
					$array_json_return = array("error_code" => 1, "msg" => "This meetup is already saved");
				}
			}
			if ($type == 'job') {
				$get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $feed_id)->where('type', '4')->first();

				if (count($get_save_user_post) == 0) {
					$add_post = new saveUserPost();
					$add_post->user_id_fk = $user_id;
					$add_post->post_id_fk = $feed_id;
					$add_post->type = '4';
					$add_post->save();
					$array_json_return = array("error_code" => 0, "msg" => "This Job is saved ");
				} else {
					$array_json_return = array("error_code" => 1, "msg" => "This job is already saved");
				}
			}
		} else {
			$array_json_return = array("error_code" => 1, "msg" => "value cant be empty");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsFollowUnfollow() {
		$commonModel = new CommonModel();
		$request = Input::all();
		$from_id = $request['from_id'];
		$to_id = $request['to_id'];
//        $status = $request['status'];
		$arr_to_user = User::where('id', '=', $to_id)->select('*')->first();
		$arr_follower = FollowerUser::where('user_id_fk', '=', $to_id)->where('follower_id_fk', '=', $from_id)->first();
//dd($arr_follower);
		if ($from_id != '' && $to_id != '') {
			if (count($arr_follower) == 0) {
				$arr_follower = new FollowerUser();
				$arr_follower->user_id_fk = $to_id;
				$arr_follower->follower_id_fk = $from_id;
				$arr_follower->status = '0';
				$arr_follower->created_at = date("Y-m-d H:i:s");
				$arr_follower->save();

//                /******add notification*********/
				$check_exists = DB::table('mst_notifications')
					->where('notification_from', $from_id)
					->where('notification_to', $to_id)
					->where('notification_type', '1')
					->get();
				if (count($check_exists) > 0) {
					$delete = DB::table('mst_notifications')
						->where('notification_from', $from_id)
						->where('notification_to', $to_id)
						->where('notification_type', '1')
						->delete();
					$subject = ($request['first_name']) . ' ' . ($request['last_name']) . ' followed you.';
					$message = ($request['first_name']) . ' ' . ($request['last_name']) . ' followed you.';
					$insert_data = array(
						'notification_from' => $from_id,
						'notification_to' => $to_id,
						'notification_type' => "1",
						'subject' => $subject,
						'message' => trim($message),
						'notification_status' => 'send',
						'created_at' => date('Y-m-d H:i:s'),
					);
					$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				} else {
					$subject = ($request['first_name']) . ' ' . ($request['last_name']) . ' followed you.';
					$message = ($request['first_name']) . ' ' . ($request['last_name']) . ' followed you.';
					$insert_data = array(
						'notification_from' => $from_id,
						'notification_to' => $to_id,
						'notification_type' => "1",
						'subject' => $subject,
						'message' => trim($message),
						'notification_status' => 'send',
						'created_at' => date('Y-m-d H:i:s'),
					);
					$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				}
				$array_json_return = array('error_code' => 1, 'msg' => 'success', 'follow_status' => '0', 'follower_count' => '1', 'first_name' => $arr_to_user['first_name'], 'last_name' => $arr_to_user['last_name']);
			} else {
				$message = "success";
				FollowerUser::where('user_id_fk', '=', $to_id)->where('follower_id_fk', '=', $from_id)->delete();
				$array_json_return = array('error_code' => 0, 'msg' => 'success', 'follow_status' => '1', 'follower_count' => '0', 'first_name' => $arr_to_user['first_name'], 'last_name' => $arr_to_user['last_name']);
			}
		}
		echo json_encode($array_json_return);
		/*         * ******push Notification******** */
		if ($message != 'success') {
			$arr_to_user = User::where('id', '=', $to_id)->first();
//        dd($arr_to_user);
			//        $arr_to_user = User::where('id', '=', $to_id)->get();
			if ($arr_to_user->device_name == "1") {
				//ios
				//            $this->ios_notificaton($arr_to_user[0]->registration_id);
				$this->ios_notificaton($arr_to_user->registration_id, $message);
			} else {
				//android
				//            $this->send_notification($arr_to_user[0]->registration_id, 'success');
				$this->send_notification($arr_to_user->registration_id, $message);
			}
		}
//        dd($arr_to_user);
	}

	public function wsConnectDisconnect() {

		$request = Input::all();
		$from_id = $request['from_id'];
		$to_id = $request['to_id'];
		$status = $request['status'];
		$userconnection = UserConnection::where('to_user_id', '=', $to_id)->where('from_user_id', '=', $from_id)->first();

		if ($from_id != '' && $to_id != '') {
			if (count($userconnection) > 0) {

				UserConnection::where('to_user_id', '=', $to_id)->where('from_user_id', '=', $from_id)->delete();
				$array_json_return = array('success' => 0, 'msg' => 'success', 'connection_count' => '0');
			} else {
				$userconnection = new UserConnection();
				$userconnection->to_user_id = $to_id;
				$userconnection->from_user_id = $from_id;
				$userconnection->status = $status;
				$userconnection->created_at = date("Y-m-d H:i:s");
				$userconnection->save();

//                /******add connection notification*********/
				//                 $check_exists = DB::table('mst_notifications')
				//                    ->where('notification_from', $from_id)
				//                    ->where('notification_to', $to_id)
				//                    ->where('notification_type', '2')
				//                    ->get();
				//            if (count($check_exists) > 0) {
				//                $delete = DB::table('mst_notifications')
				//                        ->where('notification_from', $from_id)
				//                        ->where('notification_to', $to_id)
				//                        ->where('notification_type', '2')
				//                        ->delete();
				//                $subject = '  requests to connect with you.';
				//                $message = ucfirst($request['first_name']) . ' ' . ucfirst($request['last_name']) . '  requests to connect.';
				//                $insert_data = array(
				//                    'notification_from' => $from_id,
				//                    'notification_to' => $to_id,
				//                    'notification_type' => "2",
				//                    'subject' => $subject,
				//                    'message' => trim($message),
				//                    'notification_status' => 'send',
				//                    'created_at' => date('Y-m-d H:i:s'),
				//                );
				//                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				//            } else {
				//                $subject = '  requests to connect with you.';
				//                $message = ucfirst($request['first_name']) . ' ' . ucfirst($request['last_name']) . ' requests to connect. ';
				//                $insert_data = array(
				//                    'notification_from' => $from_id,
				//                    'notification_to' => $to_id,
				//                    'notification_type' => "2",
				//                    'subject' => $subject,
				//                    'message' => trim($message),
				//                    'notification_status' => 'send',
				//                    'created_at' => date('Y-m-d H:i:s'),
				//                );
				//                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
				//            }
				//            /*********************/
				$array_json_return = array('error' => 1, 'msg' => 'success', 'connection_count' => '1');
			}
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsRsvp() {

		$request = Input::all();
		$user_id = $request['user_id'];
		$cost_id = $request['cost_id'];
		$feed_id = $request['feed_id'];
		$type = $request['type'];
		$status = $request['status'];

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		if ($user_id != '') {
			if ($type == 'event') {
				$get_data = DB::table('trans_event_rsvp_status')->where('event_id_fk', $feed_id)->where('user_id_fk', $user_id)->get();

				if (count($get_data) > 0) {
					$cost_id_fk = $get_data[0]->cost_id_fk;
					if ($cost_id_fk == $cost_id) {
						if (count($get_data) > 0) {
							$arr_to_update = array("status" => $status);
							DB::table('trans_event_rsvp_status')
								->where('event_id_fk', $feed_id)
								->where('cost_id_fk', $cost_id)
								->where('user_id_fk', $user_id)
								->update($arr_to_update);
							$array_json_return = array('error_code' => 0, 'msg' => 'success');
						} else {
							DB::table('trans_event_rsvp_status')->insert([
								['event_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
							]);

							$array_json_return = array('error_code' => 0, 'msg' => 'success');
						}
					} else {
						/* delete previous entry and make new entry */
						DB::table('trans_event_rsvp_status')
							->where('event_id_fk', $feed_id)
							->where('cost_id_fk', $cost_id)
							->where('user_id_fk', $user_id)
							->delete();
						$add_new_cost = DB::table('trans_event_rsvp_status')->insert([
							['event_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
						]);
						$array_json_return = array('error_code' => 0, 'msg' => 'success');
					}
				} else {
					DB::table('trans_event_rsvp_status')->insert([
						['event_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
					]);
					$array_json_return = array('error_code' => 0, 'msg' => 'success');
				}

				if ($status != 0) {
					$get_event_data = DB::table('mst_event')
						->where('id', $feed_id)
						->get();

					$arr_from_user = User::where('id', '=', $user_id)->get();
					$message = $arr_from_user[0]->first_name . ' goes to event ' . '"' . $get_event_data[0]->title . '"';
					$subject = $arr_from_user[0]->first_name . ' goes to event ' . '"' . $get_event_data[0]->title . '"';
					$arr_to_user = User::where('id', '=', $get_event_data[0]->user_id_fk)->get();

					$insert_data = array(
						'notification_from' => $user_id,
						'notification_to' => $get_event_data[0]->user_id_fk,
						'notification_type' => "7",
						'subject' => $subject,
						'team_id_fk' => $feed_id,
						'message' => trim($message),
						'notification_status' => 'send',
						'created_at' => date('Y-m-d H:i:s'),
						'post_id' => $feed_id,
					);
					$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);

					if ($arr_to_user[0]->device_name == "1") {
						//ios
						//                        $this->ios_notificaton($arr_to_user[0]->registration_id);
						$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
					} else {
						//android
						$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_event_data[0]->user_id_fk, 'event_id' => $feed_id, 'flag' => 'rsvp')));
					}
				}
			}

			if ($type == 'meetup') {
				$get_data = DB::table('trans_meetup_rsvp_status')->where('meetup_id_fk', $feed_id)->where('user_id_fk', $user_id)->get();

				if (count($get_data) > 0) {
					$cost_id_fk = $get_data[0]->cost_id_fk;
//                    if ($cost_id_fk == $cost_id) {
					if (count($get_data) > 0) {
						$arr_to_update = array("status" => $status);
						DB::table('trans_meetup_rsvp_status')
							->where('meetup_id_fk', $feed_id)
							->where('cost_id_fk', $cost_id)
							->where('user_id_fk', $user_id)
							->update($arr_to_update);
						$array_json_return = array('error_code' => 0, 'msg' => 'success');
					}
//                        else {
					//                            DB::table('trans_meetup_rsvp_status')->insert([
					//                                ['meetup_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
					//                            ]);
					//
					//                            $array_json_return = array('error_code' => 0, 'msg' => 'success');
					//                        }
					//                    } else {
					//                        /* delete previous entry and make new entry */
					//                        DB::table('trans_meetup_rsvp_status')
					//                                ->where('meetup_id_fk', $feed_id)
					//                                ->where('cost_id_fk', $cost_id)
					//                                ->where('user_id_fk', $user_id)
					//                                ->delete();
					//                        $add_new_cost = DB::table('trans_meetup_rsvp_status')->insert([
					//                            ['meetup_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
					//                        ]);
					//                        $array_json_return = array('error_code' => 0, 'msg' => 'success');
					//                    }
				} else {
					DB::table('trans_meetup_rsvp_status')->insert([
						['meetup_id_fk' => $feed_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
					]);
					$array_json_return = array('error_code' => 0, 'msg' => 'success');
				}

				if ($status != 0) {

					$get_meetup_data = DB::table('mst_meetup')
						->where('id', $feed_id)
						->get();

					/*                     * ******push Notification******** */
					$arr_to_user = User::where('id', '=', $get_meetup_data[0]->user_id_fk)->get();
					$arr_from_user = User::where('id', '=', $user_id)->get();
					$message = $arr_from_user[0]->first_name . ' goes to meetup ' . '"' . $get_meetup_data[0]->title . '"';
					$subject = $arr_from_user[0]->first_name . ' goes to meetup ' . '"' . $get_meetup_data[0]->title . '"';

					$insert_data = array(
						'notification_from' => $user_id,
						'notification_to' => $get_meetup_data[0]->user_id_fk,
						'notification_type' => "7",
						'subject' => $subject,
						'team_id_fk' => $feed_id,
						'message' => trim($message),
						'notification_status' => 'send',
						'created_at' => date('Y-m-d H:i:s'),
					);
					$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);

					if ($arr_to_user[0]->device_name == "1") {
						//ios
						//                    $this->ios_notificaton($arr_to_user[0]->registration_id);
						$this->ios_notificaton($arr_to_user[0]->registration_id, $message);
					} else {
						//android
						$result = $this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_meetup_data[0]->user_id_fk, 'meetup_id' => $feed_id, 'flag' => 'revp')));
					}
				}
			}
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Insert Data');
		}

		echo json_encode(
			$array_json_return);
	}

	/**
 * @SWG\Post(
 *   path="/ws-profile",
 *   summary="user profile details",
 *   tags={"User"},
 *   description="get user profile details",
 *   operationId="get user following list",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="query",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 * 
 *
 *    @SWG\Response(response="200", description="0:Success, 1:user_id missing, return:  ['error_code' => '0', 'msg' => 'Success', 'arr_u_data' => $arr_u_data, 'arr_u_follower' => $arr_u_follower, 'arr_u_following' => $arr_u_following, 'arr_u_group' => $arr_u_group, 'arr_u_cover' => $arr_u_cover, 'arr_u_profile_pic' => $arr_u_profile_pic, 'arr_u_connection' => $arr_u_connection, 'arr_u_notification' => $arr_u_notification]")
 * )
 **/


	public function wsProfile() {
		$request = Input::all();
		$user_id = $request['user_id'];
		if ($user_id != '') {
			$arr_u_data = DB::table('mst_users as user')
				->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
				->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
				->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')->where('user.id', $user_id)
				->first();

				if(!empty($arr_u_data->profile_picture))
				$arr_u_data->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_u_data->profile_picture;
				if(!empty($arr_u_data->cover_photo))
				$arr_u_data->cover_photo=config("cover_image_url").$user_id.'/thumb/'.$arr_u_data->cover_photo;
		
		
			$arr_u_follower = DB::table('trans_follow_user as follower')->join('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
				->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'follower.status', 'user.profile_picture', 'user.last_name')->where('user_id_fk', $user_id)
				->get();

				foreach($arr_u_follower as $k => $V){
					if(!empty($arr_u_follower[$k]->profile_picture))
						$arr_u_follower[$k]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_u_follower[$k]->profile_picture;
				}

			$arr_u_following = DB::table('trans_follow_user as follower')->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')->select('follower.user_id_fk', 'follower.status', 'user.first_name', 'user.gender', 'user.last_name', 'user.gender', 'user.profile_picture')->where('follower_id_fk', $user_id)
				->get();

				foreach($arr_u_following as $k => $V){
					if(!empty($arr_u_following[$k]->profile_picture))
						$arr_u_following[$k]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_u_following[$k]->profile_picture;
				}

			$arr_u_group = DB::table('trans_group_users as group_user')->leftjoin('mst_group as group', 'group.id', '=', 'group_user.group_id_fk')->select('group.group_name', 'group.id', 'group_user.user_id_fk')->where('user_id_fk', $user_id)->where('group_user.status', '=', '1')
				->distinct()
				->get('group_name');

			$arr_user_group = array();

			$arr_u_cover = DB::table('trans_user_cover_photo')->select('cover_photo', 'created_at')->where('user_id_fk', $user_id)->orderBy('id', 'DESC')
				->get('group_name');

			$arr_u_profile_pic = DB::table('trans_user_profile_picture')->select('profile_picture', 'created_at')->where('user_id_fk', $user_id)->orderBy('id', 'DESC')
				->get();

			foreach($arr_u_profile_pic as $k => $V){
				if(!empty($arr_u_profile_pic[$k]->profile_picture))
					$arr_u_profile_pic[$k]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_u_profile_pic[$k]->profile_picture;
			}
			foreach($arr_u_cover as $k => $V){
				if(!empty($arr_u_cover[$k]->cover_photo))
					$arr_u_cover[$k]->cover_photo=config("cover_image_url").$user_id.'/thumb/'.$arr_u_cover[$k]->cover_photo;
			}
			$arr_u_connection = DB::table('trans_user_connection as user_connection')->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
				->select('to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
				->where('user_connection.from_user_id', $user_id)
				->orWhere('user_connection.to_user_id', $user_id)
				->get('group_name');

			$arr_u_notification = DB::table('mst_notifications as n')->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')->select('n.*', 'user.first_name', 'user.last_name', 'user.profile_picture')->where('n.notification_to', $user_id)->where('n.read_status', '0')->take(5)->orderBy('n.created_at', 'DESC')
				->get();

				
			foreach($arr_u_notification as $k => $V){
				if(!empty($arr_u_notification[$k]->profile_picture))
					$arr_u_notification[$k]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_u_notification[$k]->profile_picture;
			}
	

			$array_json_return = array("error_code" => '0', "msg" => "Success", 'arr_u_data' => $arr_u_data, 'arr_u_follower' => $arr_u_follower, 'arr_u_following' => $arr_u_following, 'arr_u_group' => $arr_u_group, 'arr_u_cover' => $arr_u_cover, 'arr_u_profile_pic' => $arr_u_profile_pic, 'arr_u_connection' => $arr_u_connection, 'arr_u_notification' => $arr_u_notification);
		} else {
			$array_json_return = array("error_code" => '1', "msg" => "value cant be empty");
		}
		echo json_encode($array_json_return);
	}

	/*     * ********calender********** */

	public function wsCalenderList() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$cal_month = $request['cal_month'];
		$cal_year = $request['cal_year'];
		$cal_flag = $request['cal_flag'];
		$prifix = \DB::getTablePrefix();
		if ($user_id != '' && $cal_month != '' && $cal_year != '') {
			if ($cal_flag == '0') {
				/*                 * ***for event****** */
				$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
				$today_date = date('Y-m-d');

				$get_event = DB::table('mst_event as te');
				$get_event->join('trans_event_rsvp_status as rs', 'rs.event_id_fk', '=', 'te.id', 'left');
				$get_event->join('mst_users as u', 'te.user_id_fk', '=', 'u.id', 'left');
				$where = "MONTH(`" . $prifix . "te`.`start_date`) = '$cal_month' AND YEAR(`" . $prifix . "te`.`start_date`) = '$cal_year' AND `" . $prifix . "te`.`status` = '1'";
				$get_event->whereRaw($where);
				$get_event->where('te.start_date', '>=', $today_date);
				$get_event->where(['rs.status' => '1', 'rs.user_id_fk' => $user_id]);
				$get_event->groupBy('te.id');
				$get_event->select('te.id', 'te.user_id_fk as user_id', 'te.title', 'te.profile_image', 'u.profile_picture as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'te.location', 'te.start_date', 'te.start_time', DB::raw('substr(description,1,50) as description'));
				$get_event = collect($get_event->get());
				$get_events = array();
				if (count($get_event) > 0) {

					foreach ($get_event as $key => $value) {
						$get_event[$key]->color = "1";
						$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_image;
						$get_event[$key]->type = 'event';
					}

					foreach ($get_event as $object) {
						$get_events[] = (array) $object;
					}
				}

				/*                 * ***for meetup****** */
				$get_meetup = DB::table('mst_meetup as mm');
				$get_meetup->join('trans_meetup_rsvp_status as rs', 'rs.meetup_id_fk', '=', 'mm.id');
				$get_meetup->join('mst_users as u', 'mm.user_id_fk', '=', 'u.id', 'left');
				$where = "MONTH(`" . $prifix . "mm`.`start_date`) = '$cal_month' AND YEAR(`" . $prifix . "mm`.`start_date`) = '$cal_year' AND `" . $prifix . "mm`.`status` = '1'";
				$get_meetup->whereRaw($where);
//                $get_meetup->where('rs.status', '=', 1);

				$get_meetup->where('mm.start_date', '>=', $today_date);
				$get_meetup->where(['rs.status' => '1', 'rs.user_id_fk' => $user_id]);
				$get_meetup->groupBy('mm.id');
				$get_meetup->select('mm.id', 'mm.user_id_fk as user_id', 'mm.title', 'mm.profile_image', 'u.profile_picture as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'mm.location', 'mm.start_date', 'mm.start_time', DB::raw('substr(description,1,50) as description'));
				$get_meetup = collect($get_meetup->get());
//                echo '<pre>';                print_r($get_meetup);die;
				$get_meetups = array();
				if (count($get_meetup) > 0) {
					foreach ($get_meetup as $key => $value) {
						$get_meetup[$key]->color = "3";
						$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_image;
						$get_meetup[$key]->type = 'meetup';
					}

					foreach ($get_meetup as $object) {
						$get_meetups[] = (array) $object;
					}
				}
				$all_array = array();
				$all_array = array_merge($get_events, $get_meetups);
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
			}
			if ($cal_flag == '1') {
				/*                 * ***for event****** */
				$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
				$today_date = date('Y-m-d');

				$get_event = DB::table('mst_event');
				$where = "user_id_fk = '$user_id' AND MONTH(start_date) = '$cal_month' AND YEAR(start_date) = '$cal_year' AND status = '1'";
				$get_event->whereRaw($where);
				$get_event->where('start_date', '>=', $today_date);
				$get_event->groupBy('id');
				$get_event->select('id', 'title', 'profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'location', 'start_date', 'start_time', DB::raw('substr(description,1,50) as description'));
				$get_event = collect($get_event->get());
				$get_events = array();
				if (count($get_event) > 0) {

					foreach ($get_event as $key => $value) {
						$get_event[$key]->color = "1";
						$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_image;
						$get_event[$key]->type = 'event';
					}

					foreach ($get_event as $object) {
						$get_events[] = (array) $object;
					}
				}

				/*                 * ***for meetup****** */
				$get_meetup = DB::table('mst_meetup');
				$where = "user_id_fk = '$user_id' AND MONTH(start_date) = '$cal_month' AND YEAR(start_date) =  '$cal_year' AND status = '0'";
				$get_meetup->whereRaw($where);
				$get_meetup->where('start_date', '>=', $today_date);
				$get_meetup->groupBy('id');
				$get_meetup->select('id', 'title', 'profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'location', 'start_date', 'start_time', DB::raw('substr(description,1,50) as description'));
				$get_meetup = collect($get_meetup->get());
				$get_meetups = array();
				if (count($get_meetup) > 0) {
					foreach ($get_meetup as $key => $value) {
						$get_meetup[$key]->color = "3";
						$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_image;
						$get_meetup[$key]->type = 'meetup';
					}

					foreach ($get_meetup as $object) {
						$get_meetups[] = (array) $object;
					}
				}
				$all_array = array();
				$all_array = array_merge($get_events, $get_meetups);
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
			}
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsCalenderMyintrestDates() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$cal_month = $request['cal_month'];
		$cal_year = $request['cal_year'];

		$prifix = \DB::getTablePrefix();
		if ($user_id != '' && $cal_month != '' && $cal_year != '') {

			/*             * ***for event****** */
			$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
			$today_date = date('Y-m-d');

			$get_event = DB::table('mst_event as te');
//            $get_event->join('trans_event_rsvp_status as rs', 'rs.event_id_fk', '=', 'te.id', 'left');
			$where = "`MONTH(`" . $prifix . "te`.`start_date`) = '$cal_month' AND YEAR(`" . $prifix . "te`.`start_date`) = '$cal_year' AND `" . $prifix . "te`.`status` = '1'";
			$get_event->whereRaw($where);
//            $get_event->where('rs.status', '=', '1');
			$get_event->where('te.end_date', '>=', $today_date);
			$get_event->groupBy('te.id');
			$get_event->select('te.id', 'te.title', 'te.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'te.location', 'te.start_date', 'te.start_time', DB::raw('substr(description,1,50) as description'));
			$get_event = collect($get_event->get());
			$get_events = array();
			if (count($get_event) > 0) {

				foreach ($get_event as $key => $value) {
					$get_event[$key]->color = "1";
					$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
					$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_image;
					$get_event[$key]->type = 'event';
				}
				foreach ($get_event as $object) {
					$get_events[] = (array) $object;
				}
			}

			/*             * ***for meetup****** */
			$get_meetup = DB::table('mst_meetup as mm');
			$where = "`MONTH(`" . $prifix . "mm`.`start_date`) = '$cal_month' AND YEAR(`" . $prifix . "mm`.`start_date`) = '$cal_year' AND `" . $prifix . "mm`.`status` = '1'";
			$get_meetup->whereRaw($where);
			$get_meetup->where('mm.end_date', '>=', $today_date);
			$get_meetup->groupBy('mm.id');
			$get_meetup->select('mm.id', 'mm.title', 'mm.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'mm.location', 'mm.start_date', 'mm.start_time', DB::raw('substr(description,1,50) as description'));
			$get_meetup = collect($get_meetup->get());
			$get_meetups = array();
			if (count($get_meetup) > 0) {
				foreach ($get_meetup as $key => $value) {
					$get_meetup[$key]->color = "3";
					$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
					$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_image;
					$get_meetup[$key]->type = 'meetup';
				}
				foreach ($get_meetup as $object) {
					$get_meetups[] = (array) $object;
				}
			}
			$all_array = array();
			$all_array = array_merge($get_events, $get_meetups);
			$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsDateWiseCalenderList() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$cal_date = $request['cal_date'];
		$cal_flag = $request['cal_flag'];
		if (isset($request['page_number']) && $request['page_number'] != '') {
			$page = $request['page_number'];
		} else {
			$page = '1';
		}
		$end = $page * 5;

		if ($user_id != '' && $cal_date != '') {
			if ($cal_flag == '0') {
				/*                 * ***for event****** */
				$prifix = \DB::getTablePrefix();
				$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
				$today_date = date('Y-m-d');

				$get_event = DB::table('mst_event as e');
//                $get_event->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
				$get_event->join('trans_event_rsvp_status as rs', 'rs.event_id_fk', '=', 'e.id');
				$get_event->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');
				$where = "`" . $prifix . "e`.`start_date` = '$cal_date' AND `" . $prifix . "e`.`status` = '1'";
				$get_event->whereRaw($where);
//                $get_event->where('e.end_date', '>=', $today_date);
				$get_event->where('rs.status', '=', '1');
//                $get_event->orderBy('e.updated_at', 'DESC');
				$get_event->groupBy('e.id');
				$get_event->select('u.id as user_id', 'e.end_date', 'u.gender', 'u.profile_picture as profile_picture', 'u.location', 'e.id', 'e.status', 'e.title as title', 'e.user_id_fk', 'e.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'description', 'e.updated_at', 'e.created_at');
				$get_event = collect($get_event->get());

				$get_events = array();
				if (count($get_event) > 0) {

					foreach ($get_event as $key => $value) {
						$get_event[$key]->color = "1";
						$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_picture;
						$get_event[$key]->type = 'event';
					}

					foreach ($get_event as $object) {
						$get_events[] = (array) $object;
					}
				}
				/*                 * ***for meetup****** */
				$get_meetup = DB::table('mst_meetup as m');
				$get_meetup->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
				$get_meetup->join('trans_meetup_rsvp_status as rs', 'rs.meetup_id_fk', '=', 'm.id');
				$where = "`" . $prifix . "m`.`start_date` = '$cal_date' AND `" . $prifix . "m`.`status` = '1'";
				$get_meetup->whereRaw($where);
				$get_meetup->groupBy('m.id');
				$get_meetup->where('m.status', '1');
				$get_meetup->where('rs.status', '=', '1');
				$get_meetup->select('u.id as user_id', 'u.gender', 'u.profile_picture as profile_picture', 'u.location', 'm.id', 'm.title as title', 'm.user_id_fk', 'm.status', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', 'description', 'm.updated_at', 'm.created_at');
				$get_meetup = collect($get_meetup->get());

				$get_meetups = array();
				if (count($get_meetup) > 0) {
					foreach ($get_meetup as $key => $value) {
						$get_meetup[$key]->color = "3";
						$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_picture;
						$get_meetup[$key]->type = 'meetup';
					}

					foreach ($get_meetup as $object) {
						$get_meetups[] = (array) $object;
					}
				}

				$all_array_new = array();
				$all_array_new = array_merge($get_events, $get_meetups);
				$all_array1 = collect($all_array_new);

				$all_array = $all_array1->slice(0, $end);
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
			}
			if ($cal_flag == '1') {
				/*                 * ***for event****** */
				$prifix = \DB::getTablePrefix();
				$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
				$today_date = date('Y-m-d');

				$get_event = DB::table('mst_event as e');
				$get_event->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
				$get_event->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');
				$where = "`" . $prifix . "e`.`user_id_fk` = '$user_id' AND `" . $prifix . "e`.`start_date` = '$cal_date' AND `" . $prifix . "e`.`status` = '1'";
				$get_event->whereRaw($where);
//                $get_event->where('e.end_date', '>=', $today_date);
				$get_event->orderBy('e.updated_at', 'DESC');
				$get_event->groupBy('e.id');
				$get_event->select('u.id as user_id', 'e.end_date', 'u.gender', 'u.profile_picture as profile_picture', 'u.location', 'g.group_id_fk', 'e.id', 'e.status', 'e.title as title', 'e.user_id_fk', 'e.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'description', 'e.updated_at', 'e.created_at');
				$get_event = collect($get_event->get());

				$get_events = array();
				if (count($get_event) > 0) {

					foreach ($get_event as $key => $value) {
						$get_event[$key]->color = "1";
						$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_picture;
						$get_event[$key]->type = 'event';
					}

					foreach ($get_event as $object) {
						$get_events[] = (array) $object;
					}
				}
				/*                 * ***for meetup****** */
				$get_meetup = DB::table('mst_meetup as m');
				$get_meetup->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
				$get_meetup->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
//            $get_meetup->join('trans_meetup_rsvp_status as rs', 'rs.meetup_id_fk', '=', 'm.id', 'left');
				$where = "`" . $prifix . "m`.`user_id_fk` = '$user_id' AND `" . $prifix . "m`.`start_date` = '$cal_date' AND `" . $prifix . "m`.`status` = '1'";
				$get_meetup->whereRaw($where);
				$get_meetup->orderBy('m.updated_at', 'DESC');
				$get_meetup->groupBy('m.id');
				$get_meetup->where('m.status', '1');
//                $get_meetup->where('m.end_date', '>=', $today_date);
				//            $get_meetup->where('rs.status', '=', '1');
				$get_meetup->select('u.id as user_id', 'u.gender', 'u.profile_picture as profile_picture', 'u.location', 'g.group_id_fk', 'm.id', 'm.title as title', 'm.user_id_fk', 'm.status', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', 'description', 'm.updated_at', 'm.created_at');
				$get_meetup = collect($get_meetup->get());

				$get_meetups = array();
				if (count($get_meetup) > 0) {
					foreach ($get_meetup as $key => $value) {
						$get_meetup[$key]->color = "3";
						$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
						$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_picture;
						$get_meetup[$key]->type = 'meetup';
					}

					foreach ($get_meetup as $object) {
						$get_meetups[] = (array) $object;
					}
				}
//            $all_array = array();
				//            $all_array = array_merge($get_events, $get_meetups);

				$all_array_new = array();
				$all_array_new = array_merge($get_events, $get_meetups);
				$all_array1 = collect($all_array_new);

				$all_array = $all_array1->slice(0, $end);
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
			}
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsCalenderMyIntrest() {
		$request = Input::all();
		$user_id = $request['user_id'];

		if (isset($request['page_number']) && $request['page_number'] != '') {
			$page = $request['page_number'];
		} else {
			$page = '1';
		}
		$end = $page * 5;
		if ($user_id != '') {
			/*             * ***for event****** */
			$prifix = \DB::getTablePrefix();
			$today_with_plus_30_days = date('Y-m-d', strtotime("+30 days"));
			$today_date = date('Y-m-d');

			$get_groups = DB::table('trans_group_users')->where('user_id_fk', $user_id)
				->get();

			$user_group = array();
			if (count($get_groups) > 0) {
				foreach ($get_groups as $key => $value) {
					$user_group[] = $value->group_id_fk;
				}
				$group = implode(',', $user_group);
			}

			$get_event = DB::table('mst_event as e');
			$get_event->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
			$get_event->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');
			$where = "`" . $prifix . "g`.`group_id_fk` in ({$group}) AND `" . $prifix . "e`.`status` = '1'";
			$get_event->whereRaw($where);
			$get_event->where('e.start_date', '>=', $today_date);
			$get_event->groupBy('e.id');
			$get_event->orderBy('e.updated_at', 'DESC');
			$get_event->select('u.id as user_id', 'e.start_date', 'e.end_date', 'u.gender', 'u.profile_picture as profile_picture', 'u.location', 'g.group_id_fk', 'e.id', 'e.status', 'e.title as title', 'e.user_id_fk', 'e.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'description', 'e.updated_at', 'e.created_at');
			$get_event = collect($get_event->get());

			$get_events = array();
			if (isset($get_event) && count($get_event) > 0) {

				foreach ($get_event as $key => $value) {
					$get_event[$key]->color = "1";
					$get_event[$key]->event_url = "view-event/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
					$get_event[$key]->image = config("feature_pic_url")."event_image/thumb/" . $value->profile_picture;
					$get_event[$key]->type = 'event';
				}
				foreach ($get_event as $object) {
					$get_events[] = (array) $object;
				}
			}

			/*             * ***for meetup****** */
			$get_meetup = DB::table('mst_meetup as m');
			$get_meetup->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
			$get_meetup->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
			$where = "`" . $prifix . "g`.`group_id_fk` in ({$group}) AND `" . $prifix . "m`.`user_id_fk` = '$user_id' AND `" . $prifix . "m`.`status` = '1'";
			$get_meetup->whereRaw($where);
			$get_meetup->where('m.start_date', '>=', $today_date);
			$get_meetup->groupBy('m.id');
			$get_meetup->orderBy('m.updated_at', 'DESC');
			$get_meetup->select('u.id as user_id', 'u.gender', 'm.start_date', 'm.end_date', 'u.profile_picture as profile_picture', 'u.location', 'g.group_id_fk', 'm.id', 'm.title as title', 'm.user_id_fk', 'm.status', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', 'description', 'm.updated_at', 'm.created_at');
			$get_meetup = collect($get_meetup->get());
			$get_meetups = array();
			if (isset($get_meetup) && count($get_meetup) > 0) {
				foreach ($get_meetup as $key => $value) {
					$get_meetup[$key]->color = "3";
					$get_meetup[$key]->event_url = "view-meetup/" . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
					$get_meetup[$key]->image = config("feature_pic_url")."meetup_image/thumb/" . $value->profile_picture;
					$get_meetup[$key]->type = 'meetup';
				}
				foreach ($get_meetup as $object) {
					$get_meetups[] = (array) $object;
				}
			}

			$all_array_new = array();
			$all_array_new = array_merge($get_events, $get_meetups);

			$all_array1 = collect($all_array_new);
			$all_array = $all_array1->slice(0, $end);

			$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success.", 'Calender List' => $all_array);
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsUploadGuestImages() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		if (isset($_FILES['guest_image']) && $_FILES['guest_image'] != "") {
			$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image";
			$obj = new Filesystem();
			if (!$obj->exists($destinationPath)) {
				$result = $obj->makeDirectory($destinationPath, 0777, true, true);
			}
			$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image/thumb";
			$obj = new Filesystem();
			if (!$obj->exists($destinationPath)) {
				$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
			}
			$image_name = time() . '.png';
			move_uploaded_file($_FILES['guest_image']['tmp_name'], $data['absolute_path'] . "public/assets/Frontend/images/event_guest_image/thumb/" . $image_name);
			move_uploaded_file($_FILES['guest_image']['tmp_name'], $data['absolute_path'] . "public/assets/Frontend/images/event_guest_image/" . $image_name);
			$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success", 'image_name' => $image_name);
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode($array_json_return);
	}



		/**
 * @SWG\Post(
 *   path="/ws-comment",
 *   summary="post comments on feeds",
 *   tags={"Posts"},
 *   description="get user profile details",
 *   operationId="get user following list",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *  @SWG\Parameter(
 *       name="type",
 *       in="formData",
 *       description="type of the feed post {article,qrious,meetup,event,education,job}",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 * 
 *  @SWG\Parameter(
 *       name="feed_id",
 *       in="formData",
 *       description="id of post",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * 
 * @SWG\Parameter(
 *       name="comment",
 *       in="formData",
 *       description="comment",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *    @SWG\Response(response="200", description=" return:  ['error_code' => '0', 'msg' => 'Success'] ['error_code' => '1', 'msg' => 'Invalid']")
 * )
 **/
	public function wsComment() {
		$request = Input::all();
		echo $this->commentfx($request['type'],$request['feed_id'],$request['user_id']);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-get-notification-list",
	 *   summary="Get notification for a particular user",
	 *   tags={"User","Notifications"},
	 *   description="Get notifications for a user",
	 *   operationId="get notifications",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="ID of the user whose notifications are being enquired",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="page_number",
	 *       in="formData",
	 *       description="page_number starts from 0 contains 10 posts per page by default",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="count",
	 *       in="formData",
	 *       description="limit number of posts per page, 10 posts per page by default",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(response="200", description="")
	 * )
	 **/
	public function wsGetNotifications() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$domainName = $_SERVER['HTTP_HOST'];
		$url = $protocol . $domainName;

		$request = Input::all();

		$user_id = $request['user_id'];
		$no=isset($request['page_number'])?$request['page_number']:0;
		$nos=isset($request['count'])?$request['count']:10;
		$skp=$no*$nos;
		$arr_user_data = array();

		$arr_user_data[0] = User::find($user_id);
		if (count($arr_user_data) > 0) {
			$arr_u_cover = DB::table('trans_user_cover_photo')->select('cover_photo', 'created_at')->where('user_id_fk', $user_id)->orderBy('id', 'DESC')
				->get('group_name');
			$arr_u_profile_pic = DB::table('trans_user_profile_picture')->select('profile_picture', 'created_at')->where('user_id_fk', $user_id)->orderBy('id', 'DESC')
				->get();
			$arr_u_follower = DB::table('trans_follow_user as follower')->leftjoin('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
				->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'user.profile_picture', 'user.last_name')->where('user_id_fk', $user_id)
				->get();

			$arr_u_notification_count = DB::table('mst_notifications as n')->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')->where('n.notification_to', $user_id)->count();
			$arr_u_notification = DB::table('mst_notifications as n')->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')->select('n.*', 'user.id as user_id', 'user.first_name', 'user.last_name', 'user.profile_picture', 'user.profession', 'user.gender')->where('n.notification_to', $user_id)->orderBy('n.created_at', 'DESC')
				->skip($skp)->take($nos)->get();
			$arr_temp = array();

		

			foreach ($arr_u_notification as $k => $notify) {
				if ($notify->notification_type == "5" || $notify->notification_type == "7") {
					$arr_u_notification[$k]->feed_id = $notify->post_id;
					$a = strtolower($notify->subject);
					if (strpos($a, 'article') !== false) {
						$arr_u_notification[$k]->feed_type = "article";
					} elseif (strpos($a, 'education') !== false) {
						$arr_u_notification[$k]->feed_type = "education";
					} elseif (strpos($a, 'job') !== false || strpos($a, 'internship') !== false) {
						$arr_u_notification[$k]->feed_type = "job";
					} elseif (strpos($a, 'event') !== false) {
						$arr_u_notification[$k]->feed_type = "event";
					} elseif (strpos($a, 'meetup') !== false) {
						$arr_u_notification[$k]->feed_type = "meetup";
					} elseif (strpos($a, 'qrious') !== false) {
						$arr_u_notification[$k]->feed_type = "qrious";
					}
				} elseif ($notify->notification_type == "6") {
					$arr_temp = explode('-', $notify->link);
					$arr_u_notification[$k]->feed_id = end($arr_temp);
					$a = $notify->subject;
					if (strpos($a, 'article') !== false || strpos($a, 'Article') !== false) {
						$arr_u_notification[$k]->feed_type = "article";
					} elseif (strpos($a, 'education') !== false || strpos($a, 'Education') !== false) {
						$arr_u_notification[$k]->feed_type = "education";
					} elseif (strpos($a, 'job') !== false || strpos($a, 'Job') !== false) {
						$arr_u_notification[$k]->feed_type = "job";
					} elseif (strpos($a, 'event') !== false || strpos($a, 'Event') !== false) {
						$arr_u_notification[$k]->feed_type = "event";
					} elseif (strpos($a, 'meetup') !== false || strpos($a, 'Meetup') !== false) {
						$arr_u_notification[$k]->feed_type = "meetup";
					} elseif (strpos($a, 'qrious') !== false || strpos($a, 'Qrious') !== false) {
						$arr_u_notification[$k]->feed_type = "qrious";
					} else {
						$arr_u_notification[$k]->feed_type = "";
					}
				} else {
					$arr_u_notification[$k]->feed_id = "";
					$arr_u_notification[$k]->feed_type = "";
				}

                if(!empty($arr_u_notification[$k]->profile_picture))
				$arr_u_notification[$k]->profile_picture=config('profile_pic_url').$arr_u_notification[$k]->user_id.'/thumb/'.$arr_u_notification[$k]->profile_picture;
				
				if (isset($notify->user_id)) {
					$notify->profile_picture = Storage::exists(config('profile_path').$notify->user_id."/thumb/" . $notify->profile_picture) ? config("profile_pic_url").$notify->user_id."/thumb/" . $notify->profile_picture : '';
				} else {
					$notify->profile_picture = "";
				}

			}
			$all_array = array();
			$arr_notification = array();
			foreach ($arr_u_notification as $key=>$val) {
				$arr_notification[] = $val;
			}

			$array_json_return = array('error_code' => 0, 'msg' => 'error', 'notification' => $arr_notification, 'total_notifications_count' => $arr_u_notification_count);
		} else {
			$array_json_return = array('error_code' => 0, 'msg' => 'error', 'msg' => "Record not found");
		}
		echo json_encode($array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-mark-notifications-read",
	 *   summary="Mark all notifications as read for a particular user",
	 *   tags={"Notifications","User"},
	 *   description="Mark all notifications as read for a particular user",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user ID of user whose notifications are being marked as read",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *     @SWG\Response(
	 *     response="200",
	 *     description="")
	 * )
	 **/
	public function wsMarkNotificationsRead() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$update_notification = $this->MarkAllNotificationsAsReadfx($user_id);
		$array_json_return = array('code' => 0, 'msg' => $update_notification . ' notifications marked as read');
		echo json_encode($array_json_return);
	}

	public function wsSearch() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$request = Input::all();

		$event_data = array();
		$education_data = array();
		$job_data = array();
		$meetup_data = array();
		$qrious_data = array();
		$article_data = array();
		$people_data = array();

		/* Ends Here */
		if ($request['type'] != '') {
			if (isset($request['type']) && $request['type'] == 'event' || isset($request['type']) && $request['type'] == 'all') {

				$event_data = DB::table('mst_event as e');

				if (isset($request['search_word']) && $request['search_word'] != '') {

					$event_data->where('e.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}
				$event_data->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->orderBy('e.updated_at', 'DESC')->groupBy('e.id');
				$event_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.status', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description', 'e.updated_at', 'e.created_at');
				$event_data = $event_data->get();
				$event_data = $event_data->toArray();
			}

			/*             * *****job******* */
			if (isset($request['type']) && $request['type'] == 'job' || isset($request['type']) && $request['type'] == 'all') {

				$job_data = DB::table('mst_job as e');

				if (isset($request['search_word']) && $request['search_word'] != '') {
					$job_data->where('e.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}

				$job_data->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->orderBy('e.updated_at', 'DESC')->groupBy('e.id');
				$job_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$job_data = $job_data->get();
				$job_data = $job_data->toArray();
			}

			/*             * *****$education_data******* */
			if (isset($request['type']) && $request['type'] == 'education' || isset($request['type']) && $request['type'] == 'all') {

				$education_data = DB::table('mst_education as e');

				if (isset($request['search_word']) && $request['search_word'] != '') {
					$education_data->where('e.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}

				$education_data->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->orderBy('e.updated_at', 'DESC')->groupBy('e.id');
				$education_data->where('status', '0');
				$education_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$education_data = $education_data->get();
				$education_data = $education_data->toArray();
			}

			if (isset($request['type']) && $request['type'] == 'meetup' || isset($request['type']) && $request['type'] == 'all') {

				$meetup_data = DB::table('mst_meetup as m');

				if (isset($request['search_word']) && $request['search_word'] != '') {
					$meetup_data->where('m.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}

				$meetup_data->join('mst_users as u', 'm.user_id_fk', '=', 'u.id')->where('m.status', '0')->orderBy('m.updated_at', 'DESC');
				$meetup_data->groupBy('m.id');
				$meetup_data->where('m.status', '1');
				$meetup_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture', 'm.id', 'm.title as title', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', DB::raw('substr(description,1,100) as description'), 'm.updated_at', 'm.created_at');
				$meetup_data = $meetup_data->get();
				$meetup_data = $meetup_data->toArray();
			}

			if (isset($request['type']) && $request['type'] == 'qrious' || isset($request['type']) && $request['type'] == 'all') {
				$qrious_data = DB::table('mst_question as e');
				$qrious_data->where(['e.status' => '1']);

				if (isset($request['search_word']) && $request['search_word'] != '') {
					$qrious_data->where('e.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}
				$qrious_data->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
				$qrious_data->groupBy('e.id');
				$qrious_data->orderBy('e.updated_at', 'DESC');
				$qrious_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$qrious_data = $qrious_data->get();
				$qrious_data = $qrious_data->toArray();
			}

			if (isset($request['type']) && $request['type'] == 'article' || isset($request['type']) && $request['type'] == 'all') {

				$article_data = DB::table('mst_article as e');
				$article_data->where(['e.status' => '1']);
				if (isset($request['search_word']) && $request['search_word'] != '') {
					$article_data->where('e.title', 'LIKE', '%' . trim($request['search_word']) . '%');
				}
				$article_data->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')->groupBy('e.id')->orderBy('e.updated_at', 'DESC');
				$article_data->select('u.id as user_id', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$article_data = $article_data->get();
				$article_data = $article_data->toArray();
			}

			if (isset($request['type']) && $request['type'] == 'people' || isset($request['type']) && $request['type'] == 'all') {
				$people_data = DB::table('mst_users as u');
				if (isset($request['search_word']) && $request['search_word'] != '') {
					$people_data->where('first_name', 'LIKE', '%' . trim($request['search_word']) . '%');
					$people_data->orWhere('last_name', 'LIKE', '%' . trim($request['search_word']) . '%');
				}
				$people_data->where('u.user_type', '!=', '2');
				$people_data->groupBy('u.id');
				$people_data->select('u.id', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender', 'u.location');
				$people_data = $people_data->get();
				$people_data = $people_data->toArray();
			}
			foreach ($event_data as $key => $event) {
				$event_data[$key] = $event;
				$event_data[$key]->description = strip_tags($event->description);
				$event_data[$key]->type = 'event';
			}

			foreach ($job_data as $key => $job) {
				$job_data[$key] = $job;
				$job_data[$key]->description = strip_tags($job->description);
				$job_data[$key]->type = 'job';
			}
			foreach ($meetup_data as $key => $meetup) {
				$meetup_data[$key] = $meetup;
				$meetup_data[$key]->description = strip_tags($meetup->description);
				$meetup_data[$key]->type = 'meetup';
			}
			foreach ($qrious_data as $key => $qrious) {
				$qrious_data[$key] = $qrious;
				$qrious_data[$key]->description = strip_tags($qrious->description);
				$qrious_data[$key]->type = 'qrious';
			}
			foreach ($article_data as $key => $article) {
				$article_data[$key] = $article;
				$article_data[$key]->description = strip_tags($article->description);
				$article_data[$key]->type = 'article';
			}

			foreach ($education_data as $key => $edu) {
				$education_data[$key] = $edu;
				$education_data[$key]->description = strip_tags($edu->description);
				$education_data[$key]->type = 'education';
			}
			$arr_data = array_merge($event_data, $job_data, $meetup_data, $qrious_data, $article_data, $education_data);

			$array_json_return = array('error_code' => 0, 'msg' => 'success', 'search_data' => $arr_data, 'people_data' => $people_data);
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Enter valid data.');
		}
		echo json_encode(
			$array_json_return);
	}

	public function strip_tags_content($text, $tags = '', $invert = FALSE) {
		preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
		$tags = array_unique($tags[1]);

		if (is_array($tags) AND count($tags) > 0) {
			if ($invert == FALSE) {
				return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
			} else {
				return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
			}
		} elseif ($invert == FALSE) {
			return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
		}

		return $text;
	}

	public function wsPostDescription($feature_name, $view_id) {
		/**
		 * returns 'just the description'
		 * this view is used as a webview in the apps
		 * on feature_detail screen
		 */

		/* description fetched from DB */
		try {
			$array = DB::table('mst_' . $feature_name . ' as ma')
				->where('id', $view_id)
				->select('ma.description')
				->get();
		} catch (Exception $e) {
			\Log::info($e);
			abort(404);
		}

		$data['header'] = array(
			"title" => $view_id . ' ' . $feature_name . ' - Webview | ATG',
			"meta_title" => $view_id . ' ' . $feature_name . ' - Webview | ATG',
		);

		return view('Frontend.webview.view-post-description', ['post_description' => $array]);
	}

	public function descriptionData($strHtml) {

		$text_to_return = $strHtml;
		$arr_return = array();
		$arr_return['text'] = $strHtml;

		return $arr_return;
	}

	public function listCommentReplies() {
		$request = Input::all();

		$user_id = $request['user_id'];
		$type = $request['type'];
		$feed_id = $request['feed_id'];

		if ($user_id != '') {
			if ($type == 'article') {

				/*                 * **Main table**** */
				$comment = new Comment();

				$comment->commented_by = $user_id;
				$comment->comment = $request['comment'];
				$comment->comment_on = date("Y/m/d");
				$comment->status = '1';
				$comment->save();
				$last_comment_id = $comment->id;

				/*                 * **Sub table***** */
				$article_comment = new articleCommentModel();
				$article_comment->commented_by = $user_id;
				$article_comment->comment_id_fk = $last_comment_id;
				$article_comment->article_id_fk = $feed_id;
				$article_comment->status = '1';
				$article_comment->save();
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success");
			}
			if ($type == 'qrious') {

				$answer = new Answer();
				$answer->answered_by = $user_id;
				$answer->answer = $request['comment'];
				$answer->answer_on = date("Y/m/d");
				$answer->status = 1;
				$answer->save();
				$last_comment_id = $answer->id;
				$question_answer = new questionCommentModel();
				$question_answer->answered_by = $user_id;
				$question_answer->answer_id_fk = $last_comment_id;
				$question_answer->question_id_fk = $feed_id;
				$question_answer->status = 1;
				$question_answer->save();
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success");
			}
			if ($type == 'event') {

				$comment = new Comment();
				$comment->commented_by = $user_id;
				$comment->comment = $request['comment'];
				$comment->status = '1';
				$comment->comment_on = date("Y/m/d");
				$comment->save();
				$last_comment_id = $comment->id;
				$event_comment = new eventCommentModel();
				$event_comment->commented_by = $user_id;
				$event_comment->comment_id_fk = $last_comment_id;
				$event_comment->event_id_fk = $feed_id;
				$event_comment->status = '1';
				$event_comment->save();

				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success");
			}
			if ($type == 'meetup') {
				$comment = new Comment();
				$comment->commented_by = $user_id;
				$comment->comment = $request['comment'];
				$comment->status = '1';
				$comment->comment_on = date("Y/m/d");
				$comment->save();
				$last_comment_id = $comment->id;
				$meetup_comment = new meetupCommentModel();
				$meetup_comment->commented_by = $user_id;
				$meetup_comment->comment_id_fk = $last_comment_id;
				$meetup_comment->meetup_id_fk = $feed_id;
				$meetup_comment->status = '1';
				$meetup_comment->save();
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success");
			}
			if ($type == 'education') {
				$comment = new Comment();
				$comment->commented_by = $user_id;
				$comment->comment = $request['comment'];
				$comment->status = '1';
				$comment->comment_on = date("Y/m/d");
				$comment->save();
				$last_comment_id = $comment->id;
				$education_comment = new educationCommentModel();
				$education_comment->commented_by = $user_id;
				$education_comment->comment_id_fk = $last_comment_id;
				$education_comment->education_id_fk = $feed_id;
				$education_comment->status = '1';
				$education_comment->save();
				$array_json_return = array('error_code' => 0, 'msg' => 'Success', 'msg' => "Success");
			}
		} else {
			$array_json_return = array('error_code' => 1, 'msg' => 'Success', 'msg' => "Invalid.");
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsProfileInfo() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$request = Input::all();

		$user_id = $request['user_id'];

		$arr_u_data = DB::table('mst_users as user')
			->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
			->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
			->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')->where('user.id', $user_id)
			->get();
		$arr_u_follower = DB::table('trans_follow_user as follower')->join('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
			->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'follower.status', 'user.profile_picture', 'user.last_name')->where('status', '0')->where('user_id_fk', $user_id)
			->get();

		$arr_u_following = DB::table('trans_follow_user as follower')->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')->select('follower.user_id_fk', 'follower.status', 'user.first_name', 'user.gender', 'user.last_name', 'user.gender', 'user.profile_picture')->where('follower_id_fk', $user_id)->where('status', '0')
			->get();

		$arr_u_group = DB::table('trans_group_users as group_user')->leftjoin('mst_group as group', 'group.id', '=', 'group_user.group_id_fk')->select('group.group_name', 'group.id', 'group_user.user_id_fk', 'group.icon_img')->where('user_id_fk', $user_id)->where('group_user.status', '=', '1')
			->distinct()
			->get('group_name');

		$arr_user_group = array();

		$arr_u_cover = DB::table('trans_user_cover_photo')->select('cover_photo', 'created_at')->where('user_id_fk', $user_id)
			->orderBy('id', 'DESC')
			->get('group_name');

		$arr_u_profile_pic = DB::table('trans_user_profile_picture')->select('profile_picture', 'created_at')->where('user_id_fk', $user_id)
			->orderBy('id', 'DESC')
			->get();
		$arr_u_connection = DB::table('trans_user_connection as user_connection')->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
			->select('to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
			->where('user_connection.from_user_id', $user_id)
			->orWhere('user_connection.to_user_id', $user_id)
			->get('group_name');

		$arr_u_notification = DB::table('mst_notifications as n')->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')->select('n.*', 'user.first_name', 'user.last_name', 'user.profile_picture')->where('n.notification_from', $user_id)->where('n.read_status', '0')
//                ->take(5)
			->orderBy('n.created_at', 'DESC')
			->get();

		$arr_user_data = array();
		foreach ($arr_u_data as $object) {
			$arr_user_data = (array) $object;
		}
		$group_cover_image =config('backend_img_path').'group_img/cover/' . $arr_user_data['cover_photo'];
		if (Storage::exists($group_cover_image) && $arr_user_data['cover_photo'] != '') {
			$arr_user_data['img_is_available'] = "Yes";
		} else {
			$arr_user_data['img_is_available'] = "No";
		}

		$arr_user_cover_photo = array();
		foreach ($arr_u_cover as $object) {
			$arr_user_cover_photo[] = (array) $object;
		}

		$arr_notification = array();
		foreach ($arr_u_notification as $object) {
			$arr_notification[] = (array) $object;
		}

		$arr_user_profile_picture = array();
		foreach ($arr_u_profile_pic as $object) {
			$arr_user_profile_picture[] = (array) $object;
		}

		$arr_user_connection = array();
		foreach ($arr_u_connection as $object) {
			$arr_user_connection[] = (array) $object;
		}

		$arr_user_group = array();
		foreach ($arr_u_group as $object) {
			$arr_user_group[] = (array) $object;
		}
		$arr_follower = array();
		foreach ($arr_u_follower as $object) {
			$arr_follower[] = (array) $object;
		}

		$arr_following = array();
		foreach ($arr_u_following as $object) {
			$arr_following[] = (array) $object;
		}

		$is_connect = DB::table('trans_user_connection')->Where('from_user_id', $user_id)
			->get();

		$get_user_profile_picture = DB::table('trans_user_profile_picture')->where('user_id_fk', $user_id)
			->get();

		$array_json_return = array('error_code' => 0, 'msg' => 'error', 'arr_user_data' => $arr_user_data, 'arr_follower' => $arr_follower, 'is_connect' => $is_connect, 'arr_user_cover_photo' => $arr_user_cover_photo, 'arr_user_connection' => $arr_user_connection, 'arr_following' => $arr_following, 'arr_user_group' => $arr_user_group);
		echo json_encode(
			$array_json_return);
	}

	/**
	 * @SWG\Get(
	 *   path="/ws-get-group-wise-records",
	 *   summary="get group record",
	 *   tags={"Groups"},
	 *   description="Get group wise record for a user",
	 *   operationId="group record",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="ID of the user whose records to be fetched group wise",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="group_id",
	 *       in="formData",
	 *       description="group ID",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="page_number",
	 *       in="formData",
	 *       description="page_number if not given then assumed to be zero and its start from 0 therefore pages will be like 0,1,2,3,4,5 ",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 * @SWG\Parameter(
	 *       name="count",
	 *       in="formData",
	 *       description="adjust counts of post per page by default it is 10: means if you don't pass anything to it, it will take value as 10",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 * @SWG\Parameter(
	 *       name="feature",
	 *       in="formData",
	 *       description="feature 0:all(default) 1:qrious 2:article 3:event 4:meetup 5:educations 6:jobs 7:people",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(response="200", description="")
	 * )
	 **/

	public function wsGetGroupWiseRecords(Request $data) {
		$group_id = $data['group_id'];
		$user_id = $data['user_id'];
		$no=isset($data['page_number'])?$data['page_number']:0;
		$nos=isset($data['count'])?$data['count']:10;
		$feature=isset($data['feature'])?$data['feature']:0;
		$skp=$no*$nos;
		$get_parent_group_id = DB::table('mst_group')->select('parent_id', 'group_name')
			->where('id', $group_id)
			->select('mst_group.*')
			->get();
		$parent_id = isset($get_parent_group_id[0]) ? $get_parent_group_id[0]->id : '';

		$get_record = DB::table('mst_group')
			->select('id', 'group_name')->where('parent_id', $parent_id)
			->get();
		$child_group1 = array();
		foreach ($get_record as $value) {
			$child_group1[] = $value->id;
		}

		$arr_parent_group = DB::table('mst_group')->select('parent_id', 'group_name')
			->where('id', $group_id)
			->select('mst_group.*')
			->get();

		foreach ($arr_parent_group as $key => $value) {
			$arr_chlid_group = DB::table('mst_group')->select('id', 'group_name', 'icon_img', 'profession')->where('parent_id', $value->id)
				->get();
			$arr_chlid_group1 = array();
			foreach ($arr_chlid_group as $arr_chlid_group_value) {
				$arr_chlid_group1[] = $arr_chlid_group_value;
			}
			$arr_chlid_chunk_group = array_chunk($arr_chlid_group1, 3);
			$arr_parent_group[$key]->chlid = $arr_chlid_group;
		}

		$arr_joined_group_details = DB::table('trans_group_users')
			->select('id', 'group_name')->where('user_id_fk', $user_id)->where('group_id_fk', $arr_parent_group[0]->id)->select('status', 'tag_line', 'group_id_fk')
			->get();

		$data['arr_parent_group'] = $arr_parent_group;
//        dd($data['arr_parent_group']);
		/*         * ********group hirachy********** */
		$get_parent_group_id = DB::table('mst_group')
			->where('id', $arr_parent_group[0]->id)
			->select('parent_id')
			->get();

		$group_new_id = array('0' => $arr_parent_group[0]->id);
		$parent_new_id = array('0' => $get_parent_group_id[0]->parent_id);

		if ($parent_new_id['0'] != '0') {
			$get_parent_group_id1 = DB::table('mst_group')->where('id', $parent_new_id)
				->select('parent_id')
				->get();

			$parent_new_id1 = array('0' => $get_parent_group_id1[0]->parent_id);
			$parent_new_id2 = array();
			$parent_new_id3 = array();
			$child_group1 = array();

			if ($parent_new_id1['0'] != '0') {
				$get_parent_group_id2 = DB::table('mst_group')
					->where('id', $parent_new_id1)
					->select('parent_id')
					->get();

				$parent_new_id2 = array('0' => $get_parent_group_id2[0]->parent_id);

				if ($parent_new_id2['0'] != '0') {
					$get_parent_group_id3 = DB::table('mst_group')
						->where('id', $parent_new_id2)
						->select('parent_id')
						->get();
					$parent_new_id3 = array('0' => $get_parent_group_id3[0]->parent_id);
				}
			}
		} else {
			$parent_new_id1 = $group_new_id;
			$parent_new_id2 = array();
			$parent_new_id3 = array();
			$child_group1 = array();
		}
		$arr_all_data = array_merge($parent_new_id1, $parent_new_id, $parent_new_id2, $parent_new_id3, $group_new_id, $child_group1);

		/*         * ***** */

		$arr_qrious = array();
		$arr_events = array();
		$arr_article = array();
		$arr_events = array();
		$arr_meetup = array();
		$arr_education = array();
		$arr_jobs = array();
		$people_data = array();

		$arr_qrious_count=0;
		$arr_article_count=0;
		$arr_events_count=0;
		$arr_meetup_count=0;
		$arr_education_count=0;
		$arr_jobs_count=0;
		$people_data_count=0;
	if($feature==1 || $feature==0){
		$arr_querious_upvtes = array();

		$arr_qrious_count=DB::table('mst_question as e')
		->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id')
		->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
		->whereIn('g.group_id_fk', $arr_all_data)
		->where('e.status', '1')->count();

			$arr_qrious = DB::table('mst_question as e')
			->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->whereIn('g.group_id_fk', $arr_all_data)
			->where('e.status', '1')
			->groupBy('e.id')
			->orderBy('e.created_at', 'DESC')
			->select('u.id as user_id', 'u.first_name', 'u.last_name', 'u.user_name', 'u.profile_picture', 'u.gender', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.description', 'e.updated_at', 'e.created_at')
			->skip($skp)->take($nos)->get();


		foreach ($arr_qrious as $key => $value) {
			$arr_questions_answer = DB::table('mst_answers as mc')
				->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')->where('tec.question_id_fk', $value->id)
				->select('mc.*')
				->get();
			$arr_qrious[$key]->count = count($arr_questions_answer);

			$arr_querious_upvtes = DB::table('trans_question_upvote_downvote')->where('question_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_qrious[$key]->upvote_count = count($arr_querious_upvtes);
			$arr_querious_downvote = DB::table('trans_question_upvote_downvote')->where('question_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get();
			$arr_qrious[$key]->downvote_count = count($arr_querious_downvote);

			if(!empty($arr_qrious[$key]->image))
			$arr_qrious[$key]->image=config("feature_pic_url").'question_image/thumb/'.$arr_qrious[$key]->image;
			if(!empty($arr_qrious[$key]->profile_picture))
			$arr_qrious[$key]->profile_picture=config('profile_pic_url').$arr_qrious[$key]->user_id.'/thumb/'.$arr_qrious[$key]->profile_picture;
		}
	}

	if($feature==2 || $feature==0){
		$arr_article_count = DB::table('mst_article as e')
		->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id')
		->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
		->whereIn('g.group_id_fk', $arr_all_data)
		->where('e.status', '1')->count();
		
		
			$arr_article = DB::table('mst_article as e')
			->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->whereIn('g.group_id_fk', $arr_all_data)
			->where('e.status', '1')
			->groupBy('e.id')
			->orderBy('e.created_at', 'DESC')
			->select('u.id as user_id','u.first_name', 'u.last_name', 'u.user_name', 'u.gender', 'u.profile_picture', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.description', 'e.updated_at', 'e.created_at')
			->skip($skp)->take($nos)->get();


		foreach ($arr_article as $key => $value) {
			$article_comment = articleCommentModel::where('article_id_fk', $value->id)->get();
			$arr_article[$key]->count = count($article_comment);
			$arr_article_upvtes = DB::table('trans_article_upvote_downvote')->where('article_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_article[$key]->upvote_count = count($arr_article_upvtes);
			$arr_article_downvote = DB::table('trans_article_upvote_downvote')->where('article_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get();
			$arr_article[$key]->downvote_count = count($arr_article_downvote);

			if(!empty($arr_article[$key]->image))
			$arr_article[$key]->image=config("feature_pic_url").'article_image/thumb/'.$arr_article[$key]->image;
			if(!empty($arr_article[$key]->profile_picture))
			$arr_article[$key]->profile_picture=config('profile_pic_url').$arr_article[$key]->user_id.'/thumb/'.$arr_article[$key]->profile_picture;
		}

	}

	if($feature==3 || $feature==0){
		$arr_events_count = DB::table('mst_event as e')
		->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id')
		->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')
		->whereIn('g.group_id_fk', $arr_all_data)
		->where('e.status', '1')
		->count();

			$arr_events = DB::table('mst_event as e')
			->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')
			->whereIn('g.group_id_fk', $arr_all_data)
			->where('e.status', '1')
			->orderBy('e.created_at', 'DESC')
			->groupBy('e.id')
			->select('u.id as user_id', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'u.gender', 'e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.location as location', 'e.updated_at', 'e.created_at')
			->skip($skp)->take($nos)->get();
		
		foreach ($arr_events as $key => $value) {
			$arr_event_comment = DB::table('mst_comments as mc')
				->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')->where('tec.event_id_fk', $arr_events[$key]->id)
				->select('mc.*')
				->get();
			$arr_events[$key]->count = count($arr_event_comment);
			$arr_event_upvtes = DB::table('trans_event_upvote_downvote')->where('event_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_events[$key]->upvote_count = count($arr_event_upvtes);
			$arr_event_downvote = DB::table('trans_event_upvote_downvote')->where('event_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get();
			$arr_events[$key]->downvote_count = count($arr_event_downvote);

			if(!empty($arr_events[$key]->image))
			$arr_events[$key]->image=config("feature_pic_url").'event_image/thumb/'.$arr_events[$key]->image;
			if(!empty($arr_events[$key]->profile_picture))
			$arr_events[$key]->profile_picture=config('profile_pic_url').$arr_events[$key]->user_id.'/thumb/'.$arr_events[$key]->profile_picture;
		}
	}

	if($feature==4 || $feature==0){
		$arr_meetup_count = DB::table('mst_meetup as m')
		->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left')
		->join('mst_users as u', 'm.user_id_fk', '=', 'u.id')
		->whereIn('g.group_id_fk', $arr_all_data)
		->where('m.status', '1')->count();
	
			$arr_meetup = DB::table('mst_meetup as m')
			->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left')
			->join('mst_users as u', 'm.user_id_fk', '=', 'u.id')
			->whereIn('g.group_id_fk', $arr_all_data)
			->where('m.status', '1')
			->orderBy('m.created_at', 'DESC')
			->groupBy('m.id')
			->where('status', '0')
			->select('u.id as user_id', 'u.first_name', 'u.last_name', 'u.user_name', 'u.profile_picture', 'u.gender', 'm.id', 'm.title as title', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', DB::raw('substr(description,1,100) as description'), 'm.updated_at', 'm.created_at')
			->skip($skp)->take($nos)->get();
		
		foreach ($arr_meetup as $key => $value) {
			$meetup_comment = commentMeetup::where('meetup_id_fk', $value->id)->get();
			$arr_meetup[$key]->count = count($meetup_comment);

			$arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')->where('meetup_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_meetup[$key]->upvote_count = count($arr_meetup);
			$arr_meetup_downvote = DB::table('trans_meetup_upvote_downvote')->where('meetup_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get(); 
			$arr_meetup[$key]->downvote_count = count($arr_meetup_downvote);

			if(!empty($arr_meetup[$key]->image))
			$arr_meetup[$key]->image=config("feature_pic_url").'meetup_image/thumb/'.$arr_meetup[$key]->image;
			if(!empty($arr_meetup[$key]->profile_picture))
			$arr_meetup[$key]->profile_picture=config('profile_pic_url').$arr_meetup[$key]->user_id.'/thumb/'.$arr_meetup[$key]->profile_picture;
		
		}
	}

	if($feature==5 || $feature==0){
		$arr_education_count = DB::table('mst_education as e')
		->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
		->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left')
		->whereIn('g.group_id_fk', $arr_all_data)
		->where('e.status', '1')
		->count();

			$arr_education = DB::table('mst_education as e')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
			->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left')
			->whereIn('g.group_id_fk', $arr_all_data)
			->where('e.status', '1')
			->orderBy('e.created_at', 'DESC')
			->groupBy('e.id')
			->select('e.id', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'e.description', 'u.user_name', 'u.gender', 'u.profile_picture', 'u.id as user_id', 'u.first_name', 'u.last_name')
			->skip($skp)->take($nos)->get();
		

		foreach ($arr_education as $key => $value) {
			$education_comment = educationCommentModel::where('education_id_fk', $value->id)->get();
			$arr_education[$key]->count = count($education_comment);
			$arr_educatio_upvotes = DB::table('trans_education_upvote_downvote')->where('education_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_education[$key]->upvote_count = count($arr_educatio_upvotes);
			$arr_education_downvote = DB::table('trans_education_upvote_downvote')->where('education_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get();
			$arr_education[$key]->downvote_count = count($arr_education_downvote);

			if(!empty($arr_education[$key]->image))
			$arr_education[$key]->image=config("feature_pic_url").'education_image/thumb/'.$arr_education[$key]->image;
			if(!empty($arr_education[$key]->profile_picture))
			$arr_education[$key]->profile_picture=config('profile_pic_url').$arr_education[$key]->user_id.'/thumb/'.$arr_education[$key]->profile_picture;
		
		}
	}

	if($feature==6 || $feature==0){


		$arr_jobs_count = DB::table('mst_job as e')
		->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id')
		->whereIn('g.group_id_fk', $arr_all_data)
		->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->count();

			$arr_jobs = DB::table('mst_job as e')
			->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id')
			->whereIn('g.group_id_fk', $arr_all_data)
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->groupBy('e.id')
			->orderBy('e.created_at', 'DESC')
			->select('u.id as user_id', 'u.first_name', 'u.last_name', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.job_location as location', 'e.description', 'e.updated_at', 'e.created_at')
			->skip($skp)->take($nos)->get();

		foreach ($arr_jobs as $key => $value) {
			$arr_job_upvotes = DB::table('trans_job_upvote_downvote')->where('job_id_fk', $value->id)->where('status', '0')
				->select('*')
				->get();
			$arr_jobs[$key]->upvote_count = count($arr_job_upvotes);
			$arr_job_downvote = DB::table('trans_job_upvote_downvote')->where('job_id_fk', $value->id)->where('status', '1')
				->select('*')
				->get();
			$arr_jobs[$key]->downvote_count = count($arr_job_downvote);
			
			if(!empty($arr_jobs[$key]->profile_picture))
			$arr_jobs[$key]->profile_picture=config('profile_pic_url').$arr_jobs[$key]->user_id.'/thumb/'.$arr_jobs[$key]->profile_picture;
		
		}
	}

	if($feature==7 || $feature==0){

		$people_data_count = DB::table('mst_users as u')
		->leftJoin('trans_group_users as g', 'g.user_id_fk', '=', 'u.id', 'left')
		->whereIn('g.group_id_fk', $arr_all_data)
		->select('u.id', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender')
		->groupBy('u.id')
		->count();
	
			$people_data = DB::table('mst_users as u')
			->leftJoin('trans_group_users as g', 'g.user_id_fk', '=', 'u.id', 'left')
			->whereIn('g.group_id_fk', $arr_all_data)
			->select('u.id', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender')
			->groupBy('u.id')
			->orderBy('u.last_login','DESC')
			->skip($skp)->take($nos)->get();

			foreach($people_data as $key=>$val){
				if(!empty($people_data[$key]->profile_picture))
				$people_data[$key]->profile_picture=config('profile_pic_url').$people_data[$key]->id.'/thumb/'.$people_data[$key]->profile_picture;
			}
	}
		/*         * ****This loop is set key into array for divide into diffrent feeds design */
		$arr_qrious1 = array();
		foreach ($arr_qrious as $key => $qrious_value) {
			$arr_qrious1[] = $qrious_value;
			$arr_qrious1[$key]->type = 'qrious';
		}
		$arr_article1 = array();
		foreach ($arr_article as $key => $article_value) {
			$arr_article1[] = $article_value;
			$arr_article1[$key]->type = 'article';
		}

		$arr_events1 = array();
		foreach ($arr_events as $key => $event_value) {
			$arr_events1[] = $event_value;
			$arr_events1[$key]->type = 'event';
		}
		$arr_meetup1 = array();
		foreach ($arr_meetup as $key => $meetup_value) {
			$arr_meetup1[] = $meetup_value;
			$arr_meetup1[$key]->type = 'meetup';
		}
		$arr_jobs1 = array();

		foreach ($arr_jobs as $key => $job_value) {
			$arr_jobs1[] = $job_value;
			$arr_jobs1[$key]->type = 'job';
		}
		$people_data1 = array();
		foreach ($people_data as $key => $people_value) {
			$people_data1[] = $people_value;
			$people_data1[$key]->type = 'people';
		}
		$arr_education1 = array();
		foreach ($arr_education as $key => $education_value) {
			$arr_education1[] = $education_value;
			$arr_education1[$key]->type = 'education';
		}

		$arr_selected_group_data = array_merge($arr_qrious1, $arr_article1, $arr_events1, $arr_meetup1, $arr_jobs1, $people_data1, $arr_education1);

		$return_record = array('error_code' => '0', 'group_data' => $arr_selected_group_data,'count'=>['qrious_count'=>$arr_qrious_count,'event_count'=>$arr_events_count,'article_count'=>$arr_article_count,'meetup_count'=>$arr_meetup_count,'jobs_count'=>$arr_jobs_count,'people_count'=>$people_data_count,'education_count'=>$arr_education_count]);
		return json_encode($return_record);
	}

	public function wsUploadCoverPhoto(Request $data) {
		$user_id = $data['user_id'];
		$commonModel = new CommonModel();
		if (isset($_FILES['cover_image'])) {

			$resp=json_decode($this->upload_pic(1,$user_id,Input::file('cover_image')));
			$cover_photo = array("cover_photo" => $resp->cover_photo, 'user_id_fk' => $user_id, "created_at" => 'Y-m-d H:i:s');
			$commonModel->updateRow('mst_users', array('cover_photo' => $resp->cover_photo), array('id' => $user_id));
			$commonModel->insertRow('trans_user_cover_photo', $cover_photo);
			echo json_encode($resp);
			exit();
				
		}
	}

	public function wsExplorerGroup() {
		$request = Input::all();
//        $group_id = $request['id'];
		$user_id = $request['user_id'];
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$get_main_group = DB::table('mst_group')->where('parent_id', '0')->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'status', 'cover_img')
			->get();
		foreach ($get_main_group as $k => $v) {
			$get_sub_group = DB::table('mst_group')->where('parent_id', $v->id)->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
				->get();
			//check user group status
			$arr_user_group = DB::table('trans_group_users')
				->select('group_id_fk')->where('user_id_fk', $user_id)->where('group_id_fk', $v->id)
				->first();

			$group_cover_image =config('backend_img_path').'group_img/cover/' . $v->cover_img;
			if (Storage::exists($group_cover_image) && $v->cover_img != '') {
				$get_main_group[$k]->img_is_available = "Yes";
			} else {
				$get_main_group[$k]->img_is_available = "No";
			}

			if (count($arr_user_group) > 0) {
				$get_main_group[$k]->is_join = "1";
			} else {
				$get_main_group[$k]->is_join = "0";
			}
			//check user group status
			$get_main_group[$k]->subgroups = $get_sub_group;
			foreach ($get_sub_group as $key => $val) {
				$arr_chlid_group = DB::table('mst_group')
					->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
					->where('parent_id', $val->id)
					->get();
				//check user group status
				$arr_user_group = DB::table('trans_group_users')
					->select('group_id_fk')
					->where('user_id_fk', $user_id)
					->where('group_id_fk', $val->id)
					->first();
				$group_cover_image =config('backend_img_path').'group_img/cover/' . $val->cover_img;
				if (Storage::exists($group_cover_image) && $val->cover_img != '') {
					$get_main_group[$k]->subgroups[$key]->img_is_available = "Yes";
				} else {
					$get_main_group[$k]->subgroups[$key]->img_is_available = "No";
				}

				/* if ($get_main_group[$k]->is_join == "1") {
					                  $get_main_group[$k]->subgroups[$key]->is_join = "1";
				*/
				if (count($arr_user_group) > 0) {
					$get_main_group[$k]->subgroups[$key]->is_join = "1";
				} else {
					$get_main_group[$k]->subgroups[$key]->is_join = "0";
				}
				//check user group status
				$get_main_group[$k]->subgroups[$key]->child = $arr_chlid_group;
				foreach ($arr_chlid_group as $key2 => $val2) {
					$arr_chlid_group1 = DB::table('mst_group')
						->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
						->where('parent_id', $val2->id)
						->get();
//                    dd($val2->parent_id);
					//check user group status
					$arr_child_user_group = DB::table('trans_group_users')
						->select('group_id_fk')
						->where('user_id_fk', $user_id)
						->where('group_id_fk', $val2->id)
						->first();
					$group_cover_image =config('backend_img_path').'group_img/cover/' . $val2->cover_img;
					if (Storage::exists($group_cover_image) && $val2->cover_img != '') {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->img_is_available = "Yes";
					} else {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->img_is_available = "No";
					}
					/* if ($get_main_group[$k]->subgroups[$key]->is_join == "1") {
						                      $get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "1";
					*/
					if (count($arr_child_user_group) > 0) {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "1";
					} else {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "0";
					}

					foreach ($arr_chlid_group1 as $key3 => $val3) {
						//check user group status
						$arr_user_last_group = DB::table('trans_group_users')
							->select('group_id_fk')
							->where('user_id_fk', $user_id)
							->where('group_id_fk', $val3->id)
							->first();
						/* if ($get_main_group[$k]->subgroups[$key]->child[$key2]->is_join == "1") {
							                          $last_group = "1";
						*/
						if (count($arr_user_last_group) > 0) {
							$last_group = "1";
						} else {
							$last_group = "0";
						}
						$group_cover_image =config('backend_img_path').'group_img/cover/' . $val3->cover_img;
						if (Storage::exists($group_cover_image) && $val3->cover_img != '') {
							$img_is_available = "Yes";
						} else {
							$img_is_available = "No";
						}
						$arr_chlid_group1[$key3]->is_join = $last_group;
						$arr_chlid_group1[$key3]->img_is_available = $img_is_available;
						/* $last_level[$key3] = array(
							                          'id' => $val3->id,
							                          'parent_id' => $val3->parent_id,
							                          'group_name' => $val3->group_name,
							                          'icon_img' => $val3->icon_img,
							                          'profession' => $val3->profession,
							                          'cover_img' => $val3->cover_img,
							                          'is_join' => $last_group,
							                          'img_is_available' => $img_is_available
						*/
					}
					//check user group status
					$get_main_group[$k]->subgroups[$key]->child[$key2]->subchild = $arr_chlid_group1;
//                    $get_main_group[$k]->subgroups[$key]->child[$key2]->subchild = $last_level;
				}
			}
		}
//        echo '<pre>'; print_r($get_main_group); die;
		$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'arr_user_groups' => $get_main_group);
		echo json_encode(
			$array_json_return);
	}

	public function wsSingleGroupDetails() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$group_id = $request['group_id'];
		if ($user_id != '' && $group_id != '') {
			$arr_user_group = array();
			$arr_user_group = DB::table('trans_group_users')
				->select('group_id_fk')->where('user_id_fk', $user_id)->where('group_id_fk', $group_id)
				->first();

			if (count($arr_user_group) > 0) {
				$group_id = $arr_user_group->group_id_fk;
				$arr_group_data = DB::table('mst_group')
					->where('id', $group_id)
					->select('*')
					->first();

				$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'single_group_details' => $arr_group_data);
			} else {
				$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
			}
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsSingleGroupPosttList() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$request = Input::all();
		$user_id = $request['user_id'];
		$group_id = $request['group_id'];
		$type = $request['type'];
		$get_static_id = intval($type);
		$user_data = User::find($user_id);
		$arr_user_data['tagline'] = $user_data['tagline'];
		$arr_user_data['location'] = $user_data['location'];

		$get_parent_group_id = DB::table('mst_group')->select('parent_id', 'group_name')
			->where('id', $group_id)
			->select('mst_group.*')
			->get();

		$get_record = DB::table('mst_group')
			->select('id', 'group_name')->where('parent_id', $get_parent_group_id[0]->id)
			->get();
		$child_group1 = array();
		foreach ($get_record as $value) {
			$child_group1[] = $value->id;
		}

		$arr_parent_group = DB::table('mst_group')->select('parent_id', 'group_name')
			->where('id', $group_id)
			->select('mst_group.*')
			->get();

		foreach ($arr_parent_group as $key => $value) {
			$arr_chlid_group = DB::table('mst_group')->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')->where('parent_id', $value->id)
				->get();

//            $group_cover_image =config('backend_img_path').'group_img/cover/' . $value->cover_img;
			//            if (Storage::exists($group_cover_image) && $value->cover_img != '') {
			//                $arr_chlid_group->img_is_available = "Yes";
			//            } else {
			//                $arr_chlid_group->img_is_available = "No";
			//            }

			$group_cover_image =config('backend_img_path').'group_img/cover/' . $value->cover_img;
			if (Storage::exists($group_cover_image) && $value->cover_img != '') {
				$arr_parent_group[$key]->img_is_available = "Yes";
			} else {
				$arr_parent_group[$key]->img_is_available = "No";
			}

			$arr_chlid_group1 = array();
			foreach ($arr_chlid_group as $skey => $arr_chlid_group_value) {
				$arr_chlid_group1[] = $arr_chlid_group_value;
				$group_cover_image =config('backend_img_path').'group_img/cover/' . $arr_chlid_group[$skey]->cover_img;
				if (Storage::exists($group_cover_image) && $arr_chlid_group[$skey]->cover_img != '') {
					$arr_chlid_group[$skey]->img_is_available = "Yes";
				} else {
					$arr_chlid_group[$skey]->img_is_available = "No";
				}
			}
			$arr_chlid_chunk_group = array_chunk($arr_chlid_group1, 3);
			$arr_parent_group[$key]->chlid = $arr_chlid_group;
		}

		$arr_joined_group = DB::table('trans_group_users')
			->select('id', 'group_name')->where('user_id_fk', $user_id)
//                ->where('group_id_fk', $arr_parent_group[0]->id)
			->where('group_id_fk', $group_id)->select('status', 'tag_line', 'group_id_fk')
			->first();
		if (count($arr_joined_group) > 0) {
			$arr_joined_group_details = array();
			$arr_group_join = DB::table('mst_group')->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')->where('id', $arr_joined_group->group_id_fk)
				->get();
//            echo '<pre>'; print_r($arr_group_join); die;
			$group_cover_image =config('backend_img_path').'group_img/cover/' . $arr_group_join[0]->cover_img;
			if (Storage::exists($group_cover_image) && $arr_group_join[0]->cover_img != '') {
				$img_is_available = "Yes";
			} else {
				$img_is_available = "No";
			}
			$arr_joined_group_details = array(
				'id' => $arr_joined_group->group_id_fk,
				'group_name' => $arr_group_join[0]->group_name,
				'icon_img' => $arr_group_join[0]->icon_img,
				'profession' => $arr_group_join[0]->profession,
				'cover_img' => $arr_group_join[0]->cover_img,
				'is_join' => $arr_joined_group->status,
				'tag_line' => $arr_joined_group->tag_line,
				'img_is_available' => $img_is_available,
			);
		} else {
			$arr_joined_group_details = array();
			$arr_group_join = DB::table('mst_group')->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')->where('id', $group_id)
				->get();
//            echo '<pre>'; print_r($arr_group_join); die;
			$group_cover_image =config('backend_img_path').'group_img/cover/' . $arr_group_join[0]->cover_img;
			if (Storage::exists($group_cover_image) && $arr_group_join[0]->cover_img != '') {
				$img_is_available = "Yes";
			} else {
				$img_is_available = "No";
			}
			$arr_joined_group_details = array(
				'id' => $group_id,
				'group_name' => $arr_group_join[0]->group_name,
				'icon_img' => $arr_group_join[0]->icon_img,
				'profession' => $arr_group_join[0]->profession,
				'cover_img' => $arr_group_join[0]->cover_img,
				'is_join' => '0',
				'tag_line' => '',
				'img_is_available' => $img_is_available,
			);
		}
		/*         * ********group hirachy********** */
		$get_parent_group_id = DB::table('mst_group')
			->where('id', $arr_parent_group[0]->id)
			->select('parent_id')
			->get();

		$group_new_id = array('0' => $arr_parent_group[0]->id);
		$parent_new_id = array('0' => $get_parent_group_id[0]->parent_id);

		if ($parent_new_id['0'] != '0') {
			$get_parent_group_id1 = DB::table('mst_group')->where('id', $parent_new_id)
				->select('parent_id')
				->get();

			$parent_new_id1 = array('0' => $get_parent_group_id1[0]->parent_id);
			$parent_new_id2 = array();
			$parent_new_id3 = array();
			$child_group1 = array();

			if ($parent_new_id1['0'] != '0') {
				$get_parent_group_id2 = DB::table('mst_group')
					->where('id', $parent_new_id1)
					->select('parent_id')
					->get();

				$parent_new_id2 = array('0' => $get_parent_group_id2[0]->parent_id);

				if ($parent_new_id2['0'] != '0') {
					$get_parent_group_id3 = DB::table('mst_group')
						->where('id', $parent_new_id2)
						->select('parent_id')
						->get();
					$parent_new_id3 = array('0' => $get_parent_group_id3[0]->parent_id);
				}
			}
		} else {
			$parent_new_id1 = $group_new_id;
			$parent_new_id2 = array();
			$parent_new_id3 = array();
			$child_group1 = array();
		}
		$arr_all_data = array_merge($parent_new_id1, $parent_new_id, $parent_new_id2, $parent_new_id3, $group_new_id, $child_group1);
		/*         * ***** */
		$arr_qrious = array();
		$arr_events = array();
		$arr_article = array();
		$arr_events = array();
		$arr_meetup = array();
		$arr_education = array();
		$arr_jobs = array();
		$people_data = array();

		if ($get_static_id == 1 || $get_static_id == 0) {
			$arr_querious_upvtes = array();
			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_qrious = DB::table('mst_question as e');
				$arr_qrious->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id');
				$arr_qrious->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
				$arr_qrious->whereIn('g.group_id_fk', $all_data);
				$arr_qrious->where('e.status', '1');
				if (isset($request['page'])) {
					$arr_qrious->limit($total_records);
				}
				$arr_qrious->groupBy('e.id');
				$arr_qrious->orderBy('e.updated_at', 'DESC');
				$arr_qrious->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$arr_qrious = $arr_qrious->get();
			}
			foreach ($arr_qrious as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_qrious[$key]->description = $arr_desc;
				$arr_questions_answer = DB::table('mst_answers as mc')
					->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
					->where('tec.question_id_fk', $value->id)
					->select('mc.*')
					->get();
				$arr_qrious[$key]->count = count($arr_questions_answer);

				$arr_querious_upvtes = DB::table('trans_question_upvote_downvote')
					->where('question_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_qrious[$key]->upvote_count = count($arr_querious_upvtes);
				$arr_querious_downvote = DB::table('trans_question_upvote_downvote')
					->where('question_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_qrious[$key]->downvote_count = count($arr_querious_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
//                $arr_qrious[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';
				$arr_qrious[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_qrious[$key]->follower_count = count($arr_follower_count);
				if ($value->tags != '') {
					$arr_tag = explode(',', $value->tags);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_qrious[$key]->arr_tag = $tag;
				} else {
					$arr_qrious[$key]->arr_tag = [];
				}
			}
		}
		if ($get_static_id == 2 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_article = DB::table('mst_article as e');
				$arr_article->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id');
				$arr_article->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
				$arr_article->whereIn('g.group_id_fk', $all_data);
				$arr_article->where('e.status', '1');
				$arr_article->groupBy('e.id');
				$arr_article->orderBy('e.updated_at', 'DESC');
				if (isset($total_records)) {
					$arr_article->limit($total_records);
				}
				$arr_article->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender', 'u.location', 'u.tagline', 'u.profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');

				$arr_article = $arr_article->get();
			}
			foreach ($arr_article as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_article[$key]->description = $arr_desc;
				$article_comment = articleCommentModel::where('article_id_fk', $value->id)->get();
				$arr_article[$key]->count = count($article_comment);
				$arr_article_upvtes = DB::table('trans_article_upvote_downvote')
					->where('article_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_article[$key]->upvote_count = count($arr_article_upvtes);
				$arr_article_downvote = DB::table('trans_article_upvote_downvote')
					->where('article_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_article[$key]->downvote_count = count($arr_article_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
//                $arr_article[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';
				$arr_article[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_article[$key]->follower_count = count($arr_follower_count);
				/*                 * *tags*** */
				if ($value->tags != '') {
					$arr_tag = explode(',', $value->tags);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_article[$key]->arr_tags = $tag;
				} else {
					$arr_article[$key]->arr_tags = [];
				}
			}
		}
		if ($get_static_id == 3 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_events = DB::table('mst_event as e');
				$arr_events->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id');
				$arr_events->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');
				$arr_events->whereIn('g.group_id_fk', $all_data);
				$arr_events->where('e.status', '1');
				$arr_events->orderBy('e.updated_at', 'DESC');
				$arr_events->groupBy('e.id');
				$arr_events->where('status', '1');
				if (isset($total_records)) {
					$arr_events->limit($total_records);
				}
				$arr_events->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.title as title', 'e.description', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$arr_events = $arr_events->get();
			}
			foreach ($arr_events as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_events[$key]->description = $arr_desc;
				$arr_event_comment = DB::table('mst_comments as mc')
					->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
					->where('tec.event_id_fk', $arr_events[$key]->id)
					->select('mc.*')
					->get();
				$arr_events[$key]->count = count($arr_event_comment);
				$arr_event_upvtes = DB::table('trans_event_upvote_downvote')
					->where('event_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_events[$key]->upvote_count = count($arr_event_upvtes);
				$arr_event_downvote = DB::table('trans_event_upvote_downvote')
					->where('event_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_events[$key]->downvote_count = count($arr_event_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
//                $arr_events[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';
				$arr_events[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_events[$key]->follower_count = count($arr_follower_count);
			}
		}
		if ($get_static_id == 4 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_meetup = DB::table('mst_meetup as m');
				$arr_meetup->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
				$arr_meetup->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
				$arr_meetup->whereIn('g.group_id_fk', $all_data);
				$arr_meetup->where('m.status', '1');
				$arr_meetup->orderBy('m.updated_at', 'DESC');
				$arr_meetup->groupBy('m.id');
				if (isset($total_records)) {
					$arr_meetup->limit($total_records);
				}
				$arr_meetup->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.tagline', 'm.location', 'm.id', 'm.user_id_fk', 'm.title as title', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', DB::raw('substr(description,1,100) as description'), 'm.updated_at', 'm.created_at');
				$arr_meetup = $arr_meetup->get();
			}
			foreach ($arr_meetup as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_meetup[$key]->description = $arr_desc;
				$meetup_comment = commentMeetup::where('meetup_id_fk', $value->id)->get();
				$arr_meetup[$key]->count = count($meetup_comment);

				$arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')
					->where('meetup_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_meetup[$key]->upvote_count = count($arr_meetup);
				$arr_meetup_downvote = DB::table('trans_meetup_upvote_downvote')
					->where('meetup_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_meetup[$key]->downvote_count = count($arr_meetup_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
//                $arr_meetup[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';
				$arr_meetup[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_meetup[$key]->follower_count = count($arr_follower_count);
			}
		}
		if ($get_static_id == 6 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_education = DB::table('mst_education as e');
				$arr_education->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
				$arr_education->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left');
				$arr_education->whereIn('g.group_id_fk', $all_data);
				$arr_education->where('e.status', '1');
				$arr_education->orderBy('e.updated_at', 'DESC');
				$arr_education->groupBy('e.id');
				if (isset($total_records)) {
					$arr_education->limit($total_records);
				}
				$arr_education->select('e.id', 'e.tag', 'e.user_id_fk', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'u.user_name', 'u.gender', 'u.profile_picture', 'u.id as user_id');
				$arr_education = $arr_education->get();
			}
			foreach ($arr_education as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_education[$key]->description = $arr_desc;
				$education_comment = educationCommentModel::where('education_id_fk', $value->id)->get();
				$arr_education[$key]->count = count($education_comment);
				$arr_educatio_upvotes = DB::table('trans_education_upvote_downvote')
					->where('education_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_education[$key]->upvote_count = count($arr_educatio_upvotes);
				$arr_education_downvote = DB::table('trans_education_upvote_downvote')
					->where('education_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_education[$key]->downvote_count = count($arr_education_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
//                $arr_education[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';
				$arr_education[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_education[$key]->follower_count = count($arr_follower_count);
				if ($value->tag != '') {
					$arr_tag = explode(',', $value->tag);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_education[$key]->arr_tag = $tag;
				} else {
					$arr_education[$key]->arr_tag = [];
				}
			}
		}
		if ($get_static_id == 5 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$arr_jobs = DB::table('mst_job as e');
				$arr_jobs->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id');
				$arr_jobs->whereIn('g.group_id_fk', $all_data);
				$arr_jobs->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->where('e.status', '1')->orderBy('e.updated_at', 'DESC')->groupBy('e.id');
				$arr_jobs->orderBy('e.updated_at', 'DESC');
				if (isset($total_records)) {
					$arr_jobs->limit($total_records);
				}
				$arr_jobs->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
				$arr_jobs = $arr_jobs->get();
			}

			foreach ($arr_jobs as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_jobs[$key]->description = $arr_desc;
				$arr_job_upvotes = DB::table('trans_job_upvote_downvote')
					->where('job_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_jobs[$key]->upvote_count = count($arr_job_upvotes);
				$arr_job_downvote = DB::table('trans_job_upvote_downvote')
					->where('job_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_jobs[$key]->downvote_count = count($arr_job_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_jobs[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_jobs[$key]->follower_count = count($arr_follower_count);
//                if ($job_value->tags != '') {
				//                    $arr_tag = explode(',', $value->tags);
				//                    $tag = array();
				//
				//                    foreach ($arr_tag as $k => $val1) {
				//                        $tag[$k]['tag_name'] = $val1;
				//                    }
				//                    $arr_jobs[$key]->arr_tag = $tag;
				//                } else {
				//                    $arr_jobs[$key]->arr_tag = [];
				//                }
			}
		}
		if ($get_static_id == 7 || $get_static_id == 0) {

			foreach ($arr_all_data as $val) {
				$all_data[] = $val;
				$people_data = DB::table('mst_users as u');
				$people_data->leftJoin('trans_group_users as g', 'g.user_id_fk', '=', 'u.id', 'left');
				$people_data->whereIn('g.group_id_fk', $all_data);
				$people_data->select('u.id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender', 'location');
				$people_data->groupBy('u.id');
				if (isset($total_records)) {
					$people_data->limit($total_records);
				}
				$people_data = $people_data->get();
			}
		}

		/*         * ****This loop is set key into array for divide into diffrent feeds design */
		$arr_qrious1 = array();
		foreach ($arr_qrious as $key => $qrious_value) {
			$arr_qrious1[] = $qrious_value;
			$arr_qrious1[$key]->type = 'qrious';
		}
		$arr_article1 = array();
		foreach ($arr_article as $key => $article_value) {
			$arr_article1[] = $article_value;
			$arr_article1[$key]->type = 'article';
		}

		$arr_events1 = array();
		foreach ($arr_events as $key => $event_value) {
			$arr_events1[] = $event_value;
			$arr_events1[$key]->type = 'event';
		}
		$arr_meetup1 = array();
		foreach ($arr_meetup as $key => $meetup_value) {
			$arr_meetup1[] = $meetup_value;
			$arr_meetup1[$key]->type = 'meetup';
		}
		$arr_jobs1 = array();

		foreach ($arr_jobs as $key => $job_value) {
			$arr_jobs1[] = $job_value;
			$arr_jobs1[$key]->type = 'job';
		}
		$people_data1 = array();
		foreach ($people_data as $key => $people_value) {
			$people_data1[] = $people_value;
			$people_data1[$key]->type = 'people';
		}
		$arr_education1 = array();
		foreach ($arr_education as $key => $education_value) {
			$arr_education1[] = $education_value;
			$arr_education1[$key]->type = 'education';
		}

		$arr_selected_group_data = array_merge($arr_qrious1, $arr_article1, $arr_events1, $arr_meetup1, $arr_jobs1, $people_data1, $arr_education1);
		if (count($arr_selected_group_data) > 0) {
//        foreach($arr_selected_group_data as $pkey=>$pval){
			//            unset($pval->description);
			//            $arr_selected_group_data[] = $pval;
			//        }
			$arr_post_data = $arr_selected_group_data;
		} else {
			$arr_post_data = [];
		}
//        echo '<pre>'; print_r($arr_selected_group_data); die;
		//
		$arr_group_data['arr_post_data'] = $arr_post_data;
		$arr_group_data['arr_joined_group_details'] = $arr_joined_group_details;
		$arr_group_data['arr_parent_group'] = $arr_parent_group;
		$arr_group_data['arr_user_data'] = $arr_user_data;

		$array_json_return = array('error_code' => '0', 'msg' => 'success', 'arr_data' => $arr_group_data);
		echo json_encode(
			$array_json_return);
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-join-leave-group",
	 *   summary="User joins or leave a particular group",
	 *   tags={"User","Groups"},
	 *   description="User can join or leave any group using the group_id. Please note that if user joins or leaves a main group, all its niche groups are also automatically joined or left.",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="status",
	 *       in="formData",
	 *       description="0:JOIN , 1:LEAVE",
	 *       required=true,
	 *       type="boolean",
	 *       @SWG\Items(type="string")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="group_id",
	 *       in="formData",
	 *       description="group id user is joining or leaving. All sub-groups are also joined or left",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user_id",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(response="200", description=" 0 :Success"),
	 * )
	 **/
	public function wsJoinLeaveGroup() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$group_id_values = $request['group_id'];
		$group_id_arr = explode(',', $group_id_values);
		$status = $request['status'];
		//$status = 0 //join
		//$status = 1 //leave
		if ($user_id != "" && $group_id_arr != "" && $status != "") {
			$commonModel = new CommonModel();
			$data = $commonModel->commonFunction();
			if ($status == "0") {
				$status = "1";
				//join group
				foreach ($group_id_arr as $key => $group_id) {
					$get_data = DB::table('trans_group_users')
						->where('user_id_fk', $user_id)
						->where('group_id_fk', $group_id)
						->get();

					if (count($get_data) > 0) {
						continue;
						$arr_to_update = array("status" => $status);
						DB::table('trans_group_users')
							->where('user_id_fk', $user_id)
							->where('group_id_fk', $group_id)
							->update($arr_to_update);
						$array_json_return = array('error_code' => '0', 'msg' => 'joined already');
						echo json_encode($array_json_return);
					} else {
						$get_child_groups_from_parent = DB::table('mst_group')
							->where('id', $group_id)
							->where('parent_id', '0')
							->select('id', 'group_name')
							->get();

						//new code
						$arr_main_groups = \App\group::select('parent_id', 'group_name')
	//                        ->where('parent_id', $group_id)
							->where('id', $group_id)
							->select('mst_group.*')
							->get();
	//                    while ($arr_main_groups[0]->parent_id != '0') {
						/* while ($arr_main_groups[0]->parent_id != '0') {
		                      $arr_main_groups = \App\group::select('parent_id', 'group_name')
		                      ->where('id', $arr_main_groups[0]->parent_id)
		                      ->select('mst_group.*')
		                      ->get();
	*/
						foreach ($arr_main_groups as $k => $v) {
							if (isset($v->id)) {
								DB::table('trans_group_users')->insert([
									['user_id_fk' => $user_id, 'group_id_fk' => $v->id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
								]);
								if (isset($v->getChild)) {

									foreach ($v->getChild as $firstChild) {
										if (isset($firstChild->getChild)) {
	//                                    echo '<pre>'; echo 'second '; print_r($firstChild->id);
											DB::table('trans_group_users')->insert([
												['user_id_fk' => $user_id, 'group_id_fk' => $firstChild->id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
											]);
											foreach ($firstChild->getChild as $secondChild) {
												if (isset($secondChild->getChild)) {
	//                                            echo '<pre>'; echo 'third '; print_r($secondChild->id);
													DB::table('trans_group_users')->insert([
														['user_id_fk' => $user_id, 'group_id_fk' => $secondChild->id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
													]);
													foreach ($secondChild->getChild as $thirdChild) {
														if (isset($thirdChild->getChild)) {
	//                                                    echo '<pre>'; echo 'fourth '; print_r($thirdChild->id);
															DB::table('trans_group_users')->insert([
																['user_id_fk' => $user_id, 'group_id_fk' => $thirdChild->id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
															]);
														}
													}
												}
											}
										}
									}
								}
							}
						}


						$get_filter = DB::table('trans_user_filter')
							->where('user_id_fk', $user_id)
							->where('group_id_fk', $group_id)
							->get();
						if (!(count($get_filter) > 0)) {
							$filter = new Filter;
							$filter->user_id_fk = $user_id;
							$filter->qrious = 100;
							$filter->news = 100;
							$filter->blogs = 100;
							$filter->visibility = 100;
							$filter->events = 100;
							$filter->meetups = 100;
							$filter->articles = 100;
							$filter->education = 100;
							$filter->group_id_fk = $group_id;
							$filter->save();
						}
					}
				}
				$array_json_return = array('error_code' => '0', 'msg' => 'joined');
				echo json_encode($array_json_return);
			} else {

				foreach ($group_id_arr as $key => $group_id) {
					$arr_main_groups = \App\group::select('parent_id', 'group_name')
						->where('id', $group_id)
						->select('mst_group.*')
						->get();

					$group_arr = DB::table('trans_group_users as t')
						->where('t.user_id_fk', $user_id)
						->pluck('t.group_id_fk as id')
						->toArray();
					foreach ($arr_main_groups as $k => $v) {
						if (isset($v->id)) {
							if (isset($v->getChild)) {

								foreach ($v->getChild as $firstChild) {
									if (in_array($firstChild->id, $group_arr)) {
										if (isset($firstChild->getChild)) {
											foreach ($firstChild->getChild as $secondChild) {
												if (in_array($secondChild->id, $group_arr)) {
													if ($secondChild->id != "") {
														foreach ($secondChild->getChild as $thirdChild) {
															if (isset($thirdChild->getChild)) {
																GroupUser::where('group_id_fk', $thirdChild->id)->where('user_id_fk', $user_id)->delete();
															}
														}
														GroupUser::where('group_id_fk', $secondChild->id)->where('user_id_fk', $user_id)->delete();
													}
												}
											}
										}
										if ($firstChild->id != "") {
											GroupUser::where('group_id_fk', $firstChild->id)->where('user_id_fk', $user_id)->delete();
										}
									}
								}
							}
							GroupUser::where('group_id_fk', $v->id)->where('user_id_fk', $user_id)->delete();
						}
					}
					GroupUser::where('group_id_fk', $group_id)->where('user_id_fk', $user_id)->delete();
				}
				$array_json_return = array('error_code' => '0', 'msg' => 'left');
				echo json_encode($array_json_return);
			}
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'failed');
			echo json_encode($array_json_return);
		}
	}

	public function wsConnectUser() {
		$request = Input::all();

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$user_id = $request['user_id'];

		$arr_user_group = DB::table('trans_group_users')
			->select('group_id_fk')->where('user_id_fk', $user_id)
			->get();
		if (count($arr_user_group) > 0) {

			foreach ($arr_user_group as $key => $val) {
				$arr_group = DB::table('mst_group')
					->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')
					->where('id', $val->group_id_fk)
					->first();
				$arr_temp_grp[$key] = $arr_group;
			}
			$arr_user_group = $arr_group;
		} else {
			$arr_user_group = '';
		}
		$arr_to_return['arr_group_details'] = $arr_temp_grp;

		$arr_connected_user = DB::table('trans_user_connection')->where('from_user_id', $user_id)->orwhere('to_user_id', $user_id)
			->get();

		$arr_user_ids = array();
		if (count($arr_connected_user) > 0) {
			foreach ($arr_connected_user as $conected_users) {
				$arr_user_ids[] = $conected_users->to_user_id;
				$arr_user_ids[] = $conected_users->from_user_id;
			}
		}
		$arr_followed_user = DB::table('trans_follow_user')->where('follower_id_fk', $user_id)
			->get();

		$arr_follow_user = array();
		$arr_conn_user = array();

		foreach ($arr_followed_user as $object) {
			$arr_follow_user[] = $object->user_id_fk;
		}

		$arr_group_user = GroupUser::where('user_id_fk', '=', $user_id)->pluck('group_id_fk');
		$connection = implode(',', $arr_user_ids);
		$follower = implode(',', $arr_follow_user);

		$arr_all_followed_users = array();
		foreach ($arr_group_user as $group) {

			$arr_user = GroupUser::where('group_id_fk', '=', $group)->where('user_id_fk', '!=', $user_id)->groupBy('user_id_fk')->pluck('user_id_fk');

			if (count($arr_user) > 0) {

				foreach ($arr_user as $user) {
					array_push($arr_all_followed_users, $user);
				}
			}
		}
		$arr_f_users = array_unique($arr_all_followed_users);

		$arr_final_conn_user = array();
		$arr_no_group = array();

		$arr_follow_user = array();
		$arr_no_group = array();
		if (count($arr_f_users) > 0) {

			foreach ($arr_f_users as $user) {

				$array = DB::table('mst_users')
					->select('first_name', 'last_name', 'location', 'profession', 'gender', 'id', 'profile_picture')
					->where('id', $user)
					->get();

				foreach ($array as $k => $object) {

					$arr_follower_status = DB::table('trans_follow_user')
						->where('follower_id_fk', $user_id)
						->where('user_id_fk', $object->id)
						->select('status')
						->first();
					if (count($arr_follower_status) > 0) {
						$follower_status = $arr_follower_status->status;
					} else {
						$follower_status = '';
					}

					$arr_follow_user[] = array(
						'id' => $object->id,
						'first_name' => $object->first_name,
						'last_name' => $object->last_name,
						'gender' => $object->gender,
						'profile_picture' => $object->profile_picture,
						//                        'connect_status' => $connect_status,
						'location' => $object->location,
						'profession' => $object->profession,
						'follow_status' => $follower_status,
					);
				}
			}
		}

		$arr_u_data = DB::table('mst_users')->select('*')
			->where('id', $user_id)
			->get();
		$arr_user_data = array();
		foreach ($arr_u_data as $object) {
			$arr_user_data = (array) $object;
		}

		foreach ($arr_follow_user as $key => $val) {
			$arr_user_group = DB::table('trans_group_users')
				->select('group_id_fk')->where('user_id_fk', $val['id'])->groupBy('group_id_fk')
				->get();
			$arr_temp_grp = array();
			foreach ($arr_user_group as $key1 => $val1) {

				$arr_group = DB::table('mst_group')
					->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')
					->where('id', $val1->group_id_fk)
					->first();

				$arr_temp_grp[$key1] = $arr_group;
			}

			$arr_follow_user[$key]['arr_group_details'] = $arr_temp_grp;
		}

		$arr_to_return['arr_follow_user'] = $arr_follow_user;

		$array_json_return = array('error_code' => '0', 'msg' => 'success', 'arr_data' => $arr_to_return);
		echo json_encode($array_json_return);
	}

//retrieve my group

/**
 * @SWG\Post(
 *   path="/ws-my-groups",
 *   summary="Get the all groups tagged by the user",
 *   tags={"Groups"},
 *   description="get all the groups tagged by a particular user",
 *   operationId="",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user-id of the user",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *   @SWG\Response(response="200", description="Success. JSON is returned")
 * )
 **/

	public function wsMyGroups() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$request = Input::all();
		$user_id = $request['user_id'];

		if ($user_id != '') {

			$arr_main_groups = \App\group::select('parent_id', 'group_name')->where('parent_id', '0')
				->select('mst_group.*')
				->get();

			$parent_group_ids = array();
			$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
			$domainName = $_SERVER['HTTP_HOST'];
			$url = $protocol . $domainName;
			foreach ($arr_main_groups as $key2 => $value2) {
				$parent_group_ids[] = $value2->id;
			}

			$group_arr = GroupUser::where('user_id_fk', $user_id)
				->get();

			$all_groups = array();
			$user_parant_group = array();
			foreach ($group_arr as $key1 => $value1) {
				$subflag = 1;
				foreach ($arr_main_groups as $key2 => $value2) {
					if ($value1->group_id_fk == $value2->id) {
						$user_parant_group[] = $value2->id;
						$all_groups[] = $value2->id;
						$subflag = 0;
					}
				}
				if ($subflag == 1) {
					$all_groups[] = $value1->group_id_fk;

				}

			}
			$user_parant_group = array_unique($user_parant_group);
			$all_groups = array_unique($all_groups);

			$arr_my_groups = array();

			foreach ($all_groups as $last_p) {
				$arr_main_groups = \App\group::select('id', 'group_name', 'icon_img')
					->where('id', $last_p)
					->select('mst_group.*')
					->get();

				if (isset($arr_main_groups)) {

					foreach ($arr_main_groups as $pk => $pv) {
						$group_name = $pv->group_name;
						$group_icon = $pv->icon_img;
						$id = $pv->id;

						$my_groups = DB::table('trans_group_users as group_user')
							->select('group_user.user_id_fk', 'group_user.tag_line')
							->where('group_user.group_id_fk', $id)
							->where('group_user.user_id_fk', $user_id)
							->first();

						if (isset($my_groups)) {
							$arr_my_groups[] = array(
								'id' => $id,
								'name' => $group_name,
								'image' => Storage::exists(config('backend_img_path').'/group_img/icon/thumb/' . $group_icon) ?config('backend_img')."group_img/icon/thumb/" . $group_icon : '',
								'tag_line' => $my_groups->tag_line,
							);
						} else {
							$arr_my_groups[] = array(
								'id' => $id,
								'name' => $last_parent->group_name,
								'image' => Storage::exists(config('backend_img_path').'/group_img/icon/thumb/' . $last_parent->icon_img) ? config('backend_img')."group_img/icon/thumb/". $last_parent->icon_img : '',
								'tag_line' => "",
							);
						}
					}
				}

			}

			$arr_my_groups = $this->superUnique($arr_my_groups, 'id');

			$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'arr_my_groups' => $arr_my_groups, 'user_parent_group' => $user_parant_group/*,'sub_groups'=>$al_sub_groups*/);
		} else {
			$array_json_return = array("error_code" => '3', "msg" => "value cant be null.");
		}
		echo json_encode($array_json_return);
	}

	public function superUnique($array, $key) {
		$temp_array = array();
		foreach ($array as &$v) {
			if (!isset(
				$temp_array[$v[$key]])) {
				$temp_array[$v[$key]] = &$v;
			}

		}
		$array = array_values($temp_array);
		return $array;
	}

/**
 * @SWG\Post(
 *   path="/ws-group-edit-tagline",
 *   summary="Update user's tagline for a particular group",
 *   tags={"Groups"},
 *   description="Update user's tagline for a particular group",
 *   operationId="",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user-id of the user",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *   @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="group_id of the group user is updating",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *   @SWG\Parameter(
 *       name="tag_line",
 *       in="formData",
 *       description="tag_line",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *
 *   @SWG\Response(response="200", description="Success. JSON is returned")
 * )
 **/
	public function wsGroupEditTagline() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$group_id = $request['group_id'];
		$tag_line = $request['tag_line'];
		if ($user_id != '' && $group_id != "" && $tag_line != "") {
			$commonModel = new CommonModel();
			$commonModel->updateRow('trans_group_users', array('tag_line' => $tag_line), array('user_id_fk' => $user_id, 'group_id_fk' => $group_id));
			$array_json_return = array('error_code' => '0', 'msg' => 'Success');
		} else {
			$array_json_return = array("error_code" => '3', "msg" => "value cant be null.");
		}
		echo json_encode($array_json_return);
	}

/**
 * @SWG\Get(
 *   path="/ws-sub-niche-groups",
 *   summary="Get niche groups for a particular group",
 *   tags={"Groups"},
 *   description="Get niche groups for a particular group",
 *   operationId="",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user-id of the user",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *   @SWG\Parameter(
 *       name="group_id",
 *       in="formData",
 *       description="group_id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *   @SWG\Response(response="200", description="0: Success.")
 * )
 **/
	public function wsSubNicheGroups() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$group_id = $request['group_id'];
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$domainName = $_SERVER['HTTP_HOST'];
		$url = $protocol . $domainName;

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$get_main_group = DB::table('mst_group')
//            ->where('parent_id', '0')
			->where('parent_id', $group_id)->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'status', 'cover_img')
			->get();
		foreach ($get_main_group as $k => $v) {
			$get_sub_group = DB::table('mst_group')->where('parent_id', $v->id)->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
				->get();
//check user group status
			$arr_user_group = DB::table('trans_group_users')->select('group_id_fk', 'tag_line')->where('user_id_fk', $user_id)->where('group_id_fk', $v->id)
				->first();

			$group_cover_image =config('backend_img_path').'group_img/cover/' . $v->cover_img;
			if (Storage::exists($group_cover_image) && $v->cover_img != '') {
				$get_main_group[$k]->img_is_available = "Yes";
			} else {
				$get_main_group[$k]->img_is_available = "No";
			}

			if (count($arr_user_group) > 0) {
				$get_main_group[$k]->is_join = "1";
				$get_main_group[$k]->tag_line = $arr_user_group->tag_line;
			} else {
				$get_main_group[$k]->is_join = "0";
				$get_main_group[$k]->tag_line = "";
			}
//check user group status
			$get_main_group[$k]->subgroups = $get_sub_group;
			foreach ($get_sub_group as $key => $val) {
				$arr_chlid_group = DB::table('mst_group')
					->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
					->where('parent_id', $val->id)
					->get();
//check user group status
				$arr_user_group = DB::table('trans_group_users')
					->select('group_id_fk', 'tag_line')
					->where('user_id_fk', $user_id)
					->where('group_id_fk', $val->id)
					->first();
				$group_cover_image = config('backend_img_path').'group_img/cover/' . $val->cover_img;
				if (Storage::exists($group_cover_image) && $val->cover_img != '') {
					$get_main_group[$k]->subgroups[$key]->img_is_available = "Yes";
				} else {
					$get_main_group[$k]->subgroups[$key]->img_is_available = "No";
				}

				/* if ($get_main_group[$k]->is_join == "1") {
					                  $get_main_group[$k]->subgroups[$key]->is_join = "1";
				*/
				if (count($arr_user_group) > 0) {
					$get_main_group[$k]->subgroups[$key]->is_join = "1";
					$get_main_group[$k]->subgroups[$key]->tag_line = $arr_user_group->tag_line;
				} else {
					$get_main_group[$k]->subgroups[$key]->is_join = "0";
					$get_main_group[$k]->subgroups[$key]->tag_line = "";
				}
//check user group status
				$get_main_group[$k]->subgroups[$key]->child = $arr_chlid_group;
				foreach ($arr_chlid_group as $key2 => $val2) {
					$arr_chlid_group1 = DB::table('mst_group')
						->select('id', 'parent_id', 'group_name', 'icon_img', 'profession', 'parent_id', 'cover_img')
						->where('parent_id', $val2->id)
						->get();
//                    dd($val2->parent_id);
					//check user group status
					$arr_child_user_group = DB::table('trans_group_users')
						->select('group_id_fk', 'tag_line')
						->where('user_id_fk', $user_id)
						->where('group_id_fk', $val2->id)
						->first();
					$group_cover_image =config('backend_img_path').'group_img/cover/' . $val2->cover_img;
					if (Storage::exists($group_cover_image) && $val2->cover_img != '') {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->img_is_available = "Yes";
					} else {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->img_is_available = "No";
					}
					/* if ($get_main_group[$k]->subgroups[$key]->is_join == "1") {
						                      $get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "1";
					*/
					if (count($arr_child_user_group) > 0) {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "1";
						$get_main_group[$k]->subgroups[$key]->child[$key2]->tag_line = $arr_user_group->tag_line;
					} else {
						$get_main_group[$k]->subgroups[$key]->child[$key2]->is_join = "0";
						$get_main_group[$k]->subgroups[$key]->child[$key2]->tag_line = "";
					}

					foreach ($arr_chlid_group1 as $key3 => $val3) {
//check user group status
						$arr_user_last_group = DB::table('trans_group_users')
							->select('group_id_fk', 'tag_line')
							->where('user_id_fk', $user_id)
							->where('group_id_fk', $val3->id)
							->first();
						/* if ($get_main_group[$k]->subgroups[$key]->child[$key2]->is_join == "1") {
							                          $last_group = "1";
						*/
						if (count($arr_user_last_group) > 0) {
							$last_group = "1";
							$arr_chlid_group1[$key3]->tag_line = $arr_user_group->tag_line;
						} else {
							$last_group = "0";
							$arr_chlid_group1[$key3]->tag_line = "";
						}
						$group_cover_image =config('backend_img_path').'group_img/cover/' . $val3->cover_img;
						if (Storage::exists($group_cover_image) && $val3->cover_img != '') {
							$img_is_available = "Yes";
						} else {
							$img_is_available = "No";
						}
						$arr_chlid_group1[$key3]->is_join = $last_group;
						$arr_chlid_group1[$key3]->img_is_available = $img_is_available;
						/* $last_level[$key3] = array(
							                          'id' => $val3->id,
							                          'parent_id' => $val3->parent_id,
							                          'group_name' => $val3->group_name,
							                          'icon_img' => $val3->icon_img,
							                          'profession' => $val3->profession,
							                          'cover_img' => $val3->cover_img,
							                          'is_join' => $last_group,
							                          'img_is_available' => $img_is_available
						*/
					}
//check user group status
					$get_main_group[$k]->subgroups[$key]->child[$key2]->subchild = $arr_chlid_group1;
//                    $get_main_group[$k]->subgroups[$key]->child[$key2]->subchild = $last_level;
				}
			}
			$v->icon_img =config('backend_img')."group_img/icon/thumb/" . $v->icon_img;
			$v->cover_img =config('backend_img')."group_img/cover/" . $v->cover_img;
		}
//        echo '<pre>'; print_r($get_main_group); die;
		$array_json_return = array('error_code' => '0', 'msg' => 'Success', 'arr_user_groups' => $get_main_group);
		echo json_encode($array_json_return)

		;
	}

	/**
	 * @SWG\Post(
	 *   path="/ws-my-post",
	 *   summary="Get the posts by the user",
	 *   tags={"Posts"},
	 *   description="Fetch posts by a particular user",
	 *   operationId="",
	 *   produces={"application/json"},
	 *   @SWG\Parameter(
	 *       name="type",
	 *       in="formData",
	 *       description="(Options - 0:ALL , 1:qrious, 2:article, 3:event, 4:meetup, 5:job, 6:education.)",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Parameter(
	 *       name="user_id",
	 *       in="formData",
	 *       description="user ID of the user whose posts are being fetched",
	 *       required=true,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="page_number",
	 *       in="formData",
	 *       description="page_number starts from 0 contains 10 posts per page by default",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *  @SWG\Parameter(
	 *       name="count",
	 *       in="formData",
	 *       description="limit number of posts per page contains, 10 posts per page by default",
	 *       required=false,
	 *       type="integer",
	 *       @SWG\Items(type="integer")
	 *   ),
	 *   @SWG\Response(response="200", description="Success. JSON is returned and post_status 0 means drafted and 1 means posted. <- Behavior vice versa changed in Bug 599")
	 * )
	 **/
	public function wsMyPost() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$no=isset($request['page_number'])?$request['page_number']:0;
		$nos=isset($request['count'])?$request['count']:10;
		$skp=$no*$nos;
		$type = $request['type'];
		$get_static_id = intval($type);

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		/*         * ***** */
		$arr_qrious = array();
		$arr_events = array();
		$arr_article = array();
		$arr_meetup = array();
		$arr_education = array();
		$arr_jobs = array();
		$people_data = array();
		
		$arr_qrious_count = 0;
		$arr_events_count = 0;
		$arr_article_count = 0;
		$arr_meetup_count = 0;
		$arr_education_count = 0;
		$arr_jobs_count = 0;
		$people_data_count = 0;

		if ($get_static_id == 1 || $get_static_id == 0) {
			$arr_querious_upvtes = array();

			$arr_qrious_count = DB::table('mst_question as e')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->where('e.user_id_fk', $user_id)->count();

			$arr_qrious = DB::table('mst_question as e')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'e.status as post_status')
			->where('e.user_id_fk', $user_id)
			->groupBy('e.id')
			->orderBy('e.updated_at', 'DESC')
			->skip($skp)->take($nos)->get();


			foreach ($arr_qrious as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_qrious[$key]->description = $arr_desc;
				$link = url('/') . '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_qrious[$key]->share_link = $link;
				$arr_questions_answer = DB::table('mst_answers as mc')
					->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
					->where('tec.question_id_fk', $value->id)
					->select('mc.*')
					->get();
				$arr_qrious[$key]->count = count($arr_questions_answer);

				$arr_querious_upvtes = DB::table('trans_question_upvote_downvote')
					->where('question_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_qrious[$key]->upvote_count = count($arr_querious_upvtes);
				$arr_querious_downvote = DB::table('trans_question_upvote_downvote')
					->where('question_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_qrious[$key]->downvote_count = count($arr_querious_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_qrious[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_qrious[$key]->follower_count = count($arr_follower_count);
				if ($value->tags != '') {
					$arr_tag = explode(',', $value->tags);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_qrious[$key]->arr_tag = $tag;
				} else {
					$arr_qrious[$key]->arr_tag = [];
				}
				if(!empty($arr_qrious[$key]->image))
				$arr_qrious[$key]->image=config("feature_pic_url").'question_image/thumb/'.$arr_qrious[$key]->image;
				if(!empty($arr_qrious[$key]->profile_picture))
				$arr_qrious[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_qrious[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 2 || $get_static_id == 0) {

			$arr_article_count = DB::table('mst_article as e')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->where('e.user_id_fk', $user_id)->count();

			$arr_article = DB::table('mst_article as e')
			->join('mst_users as u', 'u.id', '=', 'e.user_id_fk')
			->where('e.user_id_fk', $user_id)
			->groupBy('e.id')
			->orderBy('e.updated_at', 'DESC')
			->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender', 'u.location', 'u.tagline', 'u.profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'e.status as post_status')
			->skip($skp)->take($nos)->get();


			foreach ($arr_article as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_article[$key]->description = $arr_desc;
				$link = url('/') . '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_article[$key]->share_link = $link;
				$article_comment = articleCommentModel::where('article_id_fk', $value->id)->get();
				$arr_article[$key]->count = count($article_comment);
				$arr_article_upvtes = DB::table('trans_article_upvote_downvote')
					->where('article_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_article[$key]->upvote_count = count($arr_article_upvtes);
				$arr_article_downvote = DB::table('trans_article_upvote_downvote')
					->where('article_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_article[$key]->downvote_count = count($arr_article_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_article[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_article[$key]->follower_count = count($arr_follower_count);
				/*                 * *tags*** */
				if ($value->tags != '') {
					$arr_tag = explode(',', $value->tags);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_article[$key]->arr_tags = $tag;
				} else {
					$arr_article[$key]->arr_tags = [];
				}

				if(!empty($arr_article[$key]->image))
				$arr_article[$key]->image=config("feature_pic_url").'article_image/thumb/'.$arr_article[$key]->image;
				if(!empty($arr_article[$key]->profile_picture))
				$arr_article[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_article[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 3 || $get_static_id == 0) {
			$arr_events_count = DB::table('mst_event as e')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')
			->where('e.user_id_fk', $user_id)
			->count();

			$arr_events = DB::table('mst_event as e')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')
			->where('e.user_id_fk', $user_id)
			->orderBy('e.updated_at', 'DESC')
			->groupBy('e.id')
			->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.title as title', 'e.description', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'e.status as post_status')
			->skip($skp)->take($nos)->get();

			foreach ($arr_events as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_events[$key]->description = $arr_desc;
				$link = url('/') . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_events[$key]->share_link = $link;
				$arr_event_comment = DB::table('mst_comments as mc')
					->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
					->where('tec.event_id_fk', $arr_events[$key]->id)
					->select('mc.*')
					->get();
				$arr_events[$key]->count = count($arr_event_comment);
				$arr_event_upvtes = DB::table('trans_event_upvote_downvote')
					->where('event_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_events[$key]->upvote_count = count($arr_event_upvtes);
				$arr_event_downvote = DB::table('trans_event_upvote_downvote')
					->where('event_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_events[$key]->downvote_count = count($arr_event_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_events[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_events[$key]->follower_count = count($arr_follower_count);

				if(!empty($arr_events[$key]->image))
				$arr_events[$key]->image=config("feature_pic_url").'event_image/thumb/'.$arr_events[$key]->image;
				if(!empty($arr_events[$key]->profile_picture))
				$arr_events[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_events[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 4 || $get_static_id == 0) {
			$arr_meetup_count = DB::table('mst_meetup as m')
			->join('mst_users as u', 'm.user_id_fk', '=', 'u.id')
			->where('m.user_id_fk', $user_id)->count();

			$arr_meetup = DB::table('mst_meetup as m')
			->join('mst_users as u', 'm.user_id_fk', '=', 'u.id')
			->where('m.user_id_fk', $user_id)
			->orderBy('m.updated_at', 'DESC')
			->groupBy('m.id')
			->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.tagline', 'm.location', 'm.id', 'm.user_id_fk', 'm.title as title', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', DB::raw('substr(description,1,100) as description'), 'm.updated_at', 'm.created_at', 'm.status as post_status')
			->skip($skp)->take($nos)->get();

			foreach ($arr_meetup as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_meetup[$key]->description = $arr_desc;
				$link = url('/') . '/view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_meetup[$key]->share_link = $link;
				$meetup_comment = commentMeetup::where('meetup_id_fk', $value->id)->get();
				$arr_meetup[$key]->count = count($meetup_comment);

				$arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')
					->where('meetup_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_meetup[$key]->upvote_count = count($arr_meetup);
				$arr_meetup_downvote = DB::table('trans_meetup_upvote_downvote')
					->where('meetup_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_meetup[$key]->downvote_count = count($arr_meetup_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_meetup[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_meetup[$key]->follower_count = count($arr_follower_count);

				if(!empty($arr_meetup[$key]->image))
				$arr_meetup[$key]->image=config("feature_pic_url").'meetup_image/thumb/'.$arr_meetup[$key]->image;
				if(!empty($arr_meetup[$key]->profile_picture))
				$arr_meetup[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_meetup[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 6 || $get_static_id == 0) {
			$arr_education_count = DB::table('mst_education as e')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
			->where('e.user_id_fk', $user_id)->count();

			$arr_education = DB::table('mst_education as e')
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left')
			->where('e.user_id_fk', $user_id)
			->orderBy('e.updated_at', 'DESC')
			->groupBy('e.id')
			->select('e.id', 'e.tag', 'e.user_id_fk', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'u.user_name', 'u.gender', 'u.profile_picture', 'u.id as user_id', 'e.status as post_status')
			->skip($skp)->take($nos)->get();

			foreach ($arr_education as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_education[$key]->description = $arr_desc;
				$link = url('/') . '/view-education/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_education[$key]->share_link = $link;
				$education_comment = educationCommentModel::where('education_id_fk', $value->id)->get();
				$arr_education[$key]->count = count($education_comment);
				$arr_educatio_upvotes = DB::table('trans_education_upvote_downvote')
					->where('education_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_education[$key]->upvote_count = count($arr_educatio_upvotes);
				$arr_education_downvote = DB::table('trans_education_upvote_downvote')
					->where('education_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_education[$key]->downvote_count = count($arr_education_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_education[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_education[$key]->follower_count = count($arr_follower_count);
				if ($value->tag != '') {
					$arr_tag = explode(',', $value->tag);
					$tag = array();

					foreach ($arr_tag as $k => $val1) {
						$tag[$k]['tag_name'] = $val1;
					}
					$arr_education[$key]->arr_tag = $tag;
				} else {
					$arr_education[$key]->arr_tag = [];
				}
				if(!empty($arr_education[$key]->image))
				$arr_education[$key]->image=config("feature_pic_url").'education_image/thumb/'.$arr_education[$key]->image;
				if(!empty($arr_education[$key]->profile_picture))
				$arr_education[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_education[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 5 || $get_static_id == 0) {
			$arr_jobs_count = DB::table('mst_job as e')
			->where('e.user_id_fk', $user_id)
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')->count();

			$arr_jobs = DB::table('mst_job as e')
			->where('e.user_id_fk', $user_id)
			->join('mst_users as u', 'e.user_id_fk', '=', 'u.id')
			->groupBy('e.id')
			->orderBy('e.updated_at', 'DESC')
			->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'e.status as post_status')
			->skip($skp)->take($nos)->get();

			foreach ($arr_jobs as $key => $value) {
				$arr_desc = strip_tags($value->description);
				$arr_jobs[$key]->description = $arr_desc;
				$link = url('/') . '/view-job/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
				$arr_jobs[$key]->share_link = $link;
				$arr_job_upvotes = DB::table('trans_job_upvote_downvote')
					->where('job_id_fk', $value->id)
					->where('status', '0')
					->select('*')
					->get();
				$arr_jobs[$key]->upvote_count = count($arr_job_upvotes);
				$arr_job_downvote = DB::table('trans_job_upvote_downvote')
					->where('job_id_fk', $value->id)
					->where('status', '1')
					->select('*')
					->get();
				$arr_jobs[$key]->downvote_count = count($arr_job_downvote);

				$arr_follower_status = DB::table('trans_follow_user')
					->where('follower_id_fk', $user_id)
					->where('user_id_fk', $value->user_id_fk)
					->select('status')
					->first();
				$arr_jobs[$key]->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';

				$arr_follower_count = DB::table('trans_follow_user')
					->where('user_id_fk', $value->user_id_fk)
					->get();
				$arr_jobs[$key]->follower_count = count($arr_follower_count);

				if(!empty($arr_jobs[$key]->profile_picture))
				$arr_jobs[$key]->profile_picture=config('profile_pic_url').$user_id.'/thumb/'.$arr_jobs[$key]->profile_picture;
				
			}
		}
		if ($get_static_id == 7 || $get_static_id == 0) {
//            foreach ($arr_all_data as $val) {
			////                $all_data[] = $val;
			//                $people_data = DB::table('mst_users as u');
			//                $people_data->leftJoin('trans_group_users as g', 'g.user_id_fk', '=', 'u.id', 'left');
			//                $people_data->whereIn('g.group_id_fk', $all_data);
			//                $people_data->select('u.id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender', 'location');
			//                $people_data->groupBy('u.id');
			//                if (isset($total_records)) {
			//                    $people_data->limit($total_records);
			//                }
			//                $people_data = $people_data->get();
			//            }
		}

		/*         * ****This loop is set key into array for divide into diffrent feeds design */
		$arr_qrious1 = array();
		foreach ($arr_qrious as $key => $qrious_value) {
			$arr_qrious1[] = $qrious_value;
			$arr_qrious1[$key]->type = 'qrious';
		}
		$arr_article1 = array();
		foreach ($arr_article as $key => $article_value) {
			$arr_article1[] = $article_value;
			$arr_article1[$key]->type = 'article';
		}

		$arr_events1 = array();
		foreach ($arr_events as $key => $event_value) {
			$arr_events1[] = $event_value;
			$arr_events1[$key]->type = 'event';
		}
		$arr_meetup1 = array();
		foreach ($arr_meetup as $key => $meetup_value) {
			$arr_meetup1[] = $meetup_value;
			$arr_meetup1[$key]->type = 'meetup';
		}
		$arr_jobs1 = array();

		foreach ($arr_jobs as $key => $job_value) {
			$arr_jobs1[] = $job_value;
			$arr_jobs1[$key]->type = 'job';
		}
		$people_data1 = array();
		foreach ($people_data as $key => $people_value) {
			$people_data1[] = $people_value;
			$people_data1[$key]->type = 'people';
		}
		$arr_education1 = array();
		foreach ($arr_education as $key => $education_value) {
			$arr_education1[] = $education_value;
			$arr_education1[$key]->type = 'education';
		}

		$arr_selected_group_data = array_merge($arr_qrious1, $arr_article1, $arr_events1, $arr_meetup1, $arr_jobs1, $people_data1, $arr_education1);
		if (count($arr_selected_group_data) > 0) {
			$arr_post_data = $arr_selected_group_data;
		} else {
			$arr_post_data = [];
		}
		$arr_group_data['arr_post_data'] = $arr_post_data;
		$array_json_return = array('error_code' => '0', 'msg' => 'success', 'arr_data' => $arr_group_data,'count'=>['qrious_count'=>$arr_qrious_count,'event_count'=>$arr_events_count,'article_count'=>$arr_article_count,'meetup_count'=>$arr_meetup_count,'education_count'=>$arr_education_count,'job_count'=>$arr_jobs_count,'people_count'=>$people_data_count]);
		echo json_encode(
			$array_json_return);
	}

	public function wsDeleteMyPost() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$type = $request['type'];
		$post_id = $request['post_id'];
		$get_static_id = intval($type);

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		if ($user_id != '' && $type != '' && $post_id != '') {
			if ($get_static_id == 1 || $get_static_id == 0) {

				$arr_qrious = DB::table('mst_question as e');
				$arr_qrious->where('e.user_id_fk', $user_id);
				$arr_qrious->where('e.id', $post_id);
				$arr_qrious = $arr_qrious->get();
				if (count($arr_qrious) > 0) {
					$arr_qrious = QuestionModel::where('id', $arr_qrious[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
			if ($get_static_id == 2 || $get_static_id == 0) {

				$arr_article = DB::table('mst_article as e');
				$arr_article->where('e.user_id_fk', $user_id);
				$arr_article->where('e.id', $post_id);
				$arr_article = $arr_article->get();
				if (count($arr_article) > 0) {
					$arr_article = ArticleModel::where('id', $arr_article[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
			if ($get_static_id == 3 || $get_static_id == 0) {
				$arr_events = DB::table('mst_event as e');
				$arr_events->where('e.user_id_fk', $user_id);
				$arr_events->where('e.id', $post_id);
				$arr_events = $arr_events->get();
				if (count($arr_events) > 0) {
					$arr_events = Event::where('id', $arr_events[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
			if ($get_static_id == 4 || $get_static_id == 0) {
				$arr_meetup = DB::table('mst_meetup as m');
				$arr_meetup->where('m.user_id_fk', $user_id);
				$arr_meetup->where('m.id', $post_id);
				$arr_meetup = $arr_meetup->get();
				if (count($arr_meetup) > 0) {
					$arr_meetup = meetup_model::where('id', $arr_meetup[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
			if ($get_static_id == 5 || $get_static_id == 0) {
				$arr_jobs = DB::table('mst_job as e');
				$arr_jobs->where('e.user_id_fk', $user_id);
				$arr_jobs->where('e.id', $post_id);
				$arr_jobs = $arr_jobs->get();
				if (count($arr_jobs) > 0) {
					$arr_jobs = JobModel::where('id', $arr_jobs[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
			if ($get_static_id == 6 || $get_static_id == 0) {
				$arr_education = DB::table('mst_education as e');
				$arr_education->where('e.user_id_fk', $user_id);
				$arr_education->where('e.id', $post_id);
				$arr_education = $arr_education->get();
				if (count($arr_education) > 0) {
					$arr_education = education_model::where('id', $arr_education[0]->id)->delete();
					$array_json_return = array('error_code' => '0', 'msg' => 'success');
				} else {
					$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
				}
			}
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'Invalid');
		}

		echo json_encode(
			$array_json_return);
	}

	public function wsMySavedPost() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$type = $request['type'];
//        $get_static_id = $type;
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		if ($user_id != '' && $type != "") {
			if ($type == "event") {
				$feed_type = "1";
			} elseif ($type == "article") {
				$feed_type = "2";
			} elseif ($type == "job") {
				$feed_type = "4";
			} elseif ($type == "meetup") {
				$feed_type = "0";
			} elseif ($type == "qrious") {
				$feed_type = "3";
			} elseif ($type == "education") {
				$feed_type = "5";
			}

			$arr_post_data = array();
			$arr_saved_post = DB::table('trans_save_user_posts as p');
			$arr_saved_post->where('p.user_id_fk', $user_id);
			$arr_saved_post->where('p.type', $feed_type);
			$arr_saved_post->select('p.post_id_fk');
			$arr_saved_post = $arr_saved_post->get();
			$arr_qrious = array();
			$arr_events = array();
			$arr_article = array();
			$arr_events = array();
			$arr_meetup = array();
			$arr_education = array();
			$arr_jobs = array();
			$people_data = array();

			if (count($arr_saved_post) > 0) {

				foreach ($arr_saved_post as $key => $val) {
					if ($type == "qrious") {
						$arr_querious_upvtes = array();
						$arr_qrious = DB::table('mst_question as e');
						$arr_qrious->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
						$arr_qrious->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
						$arr_qrious->where('e.id', $val->post_id_fk);
						if (isset($request['page'])) {
							$arr_qrious->limit($page);
						}
						$arr_qrious = $arr_qrious->get();
						if (count($arr_qrious) > 0) {
							$arr_desc = strip_tags($arr_qrious[0]->description);
							$arr_qrious[$key]->description = $arr_desc;
							$link = url('/') . '/view-question/' . $preg_replace('/[^A-Za-z0-9\s]/', '', $arr_qrious[0]->title) . '-' . $arr_qrious[0]->id;
							$arr_qrious[$key]->share_link = $link;
							$arr_questions_answer = DB::table('mst_answers as mc')
								->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
								->where('tec.question_id_fk', $arr_qrious[0]->id)
								->select('mc.*')
								->get();
							$arr_qrious[$key]->count = count($arr_questions_answer);

							$arr_querious_upvtes = DB::table('trans_question_upvote_downvote')
								->where('question_id_fk', $arr_qrious[0]->id)
								->where('status', '0')
								->select('*')
								->get();
							$arr_qrious[$key]->upvote_count = count($arr_querious_upvtes);
							$arr_querious_downvote = DB::table('trans_question_upvote_downvote')
								->where('question_id_fk', $arr_qrious[0]->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_qrious[$key]->downvote_count = count($arr_querious_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_qrious[0]->user_id_fk)
								->select('status')
								->first();
							$arr_qrious[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_qrious[0]->user_id_fk)
								->get();
							$arr_qrious[$key]->follower_count = count($arr_follower_count);
							if ($arr_qrious[0]->tags != '') {
								$arr_tag = explode(',', $arr_qrious[0]->tags);
								$tag = array();

								foreach ($arr_tag as $k => $val1) {
									$tag[$k]['tag_name'] = $val1;
								}
								$arr_qrious[$key]->arr_tag = $tag;
							} else {
								$arr_qrious[$key]->arr_tag = [];
							}
//calculating hotness of the article from total upvotes, downvotes and posted date
							$arr_to_return_qrious_hotness = $this->hotness($arr_qrious[$key]->upvote_count, $arr_qrious[$key]->downvote_count, $arr_qrious[0]->created_at);
//assigning key into array with resulted hotness
							$arr_qrious[$key]->hotness = $arr_to_return_qrious_hotness;
							$arr_qrious[$key]->type = 'qrious';
							$arr_post_data[$key] = $arr_qrious;
						}
					}
					if ($type == "article") {

						$arr_article = DB::table('mst_article as e');
						$arr_article->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
						$arr_article->where('e.id', $val->post_id_fk);
						if (isset($page)) {
							$arr_article->limit($page);
						}
						$arr_article->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender', 'u.location', 'u.tagline', 'u.profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');

						$arr_article = $arr_article->get();
						if (count($arr_article) > 0) {
							$arr_desc = strip_tags($arr_article[0]->description);
							$arr_article[$key]->description = $arr_desc;
							$link = url('/') . '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_article[0]->title) . '-' . $arr_article[0]->id;
							$arr_article[$key]->share_link = $link;
							$article_comment = articleCommentModel::where('article_id_fk', $arr_article[0]->id)->get();
							$arr_article[$key]->count = count($article_comment);
							$arr_article_upvtes = DB::table('trans_article_upvote_downvote')
								->where('article_id_fk', $arr_article[0]->id)
								->where('status', '0')
								->select('*')
								->get();
							$arr_article[$key]->upvote_count = count($arr_article_upvtes);
							$arr_article_downvote = DB::table('trans_article_upvote_downvote')
								->where('article_id_fk', $arr_article[0]->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_article[$key]->downvote_count = count($arr_article_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_article[0]->user_id_fk)
								->select('status')
								->first();
							$arr_article[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_article[0]->user_id_fk)
								->get();
							$arr_article[$key]->follower_count = count($arr_follower_count);
							/*                             * *tags*** */
							if ($arr_article[0]->tags != '') {
								$arr_tag = explode(',', $arr_article[0]->tags);
								$tag = array();

								foreach ($arr_tag as $k => $val1) {
									$tag[$k]['tag_name'] = $val1;
								}
								$arr_article[$key]->arr_tags = $tag;
							} else {
								$arr_article[$key]->arr_tags = [];
							}
							$arr_to_return_article_hotness = $this->hotness($arr_article[$key]->upvote_count, $arr_article[$key]->downvote_count, $arr_article[0]->created_at);
//assigning key into array with resulted hotness
							$arr_article[$key]->hotness = $arr_to_return_article_hotness;
							$arr_article[$key]->type = 'article';
							$arr_post_data[$key] = $arr_article;
						}
					}
					if ($type == "event") {
						$arr_events = DB::table('mst_event as e');
						$arr_events->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');
						$arr_events->where('e.id', $val->post_id_fk);
						if (isset($page)) {
							$arr_events->limit($page);
						}
						$arr_events->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'u.gender', 'u.location', 'e.id', 'e.user_id_fk', 'e.title as title', 'e.description', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');

						$arr_events = $arr_events->first();

						if (isset($arr_events)) {
							$arr_desc = strip_tags($arr_events->description);
							$arr_events->description = $arr_desc;
							$link = url('/') . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_events->title) . '-' . $arr_events->id;
							$arr_events->share_link = $link;
							$arr_event_comment = DB::table('mst_comments as mc')
								->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
								->where('tec.event_id_fk', $arr_events->id)
								->select('mc.*')
								->get();
							$arr_events->count = count($arr_event_comment);
							$arr_event_upvtes = DB::table('trans_event_upvote_downvote')
								->where('event_id_fk', $arr_events->id)
								->where('status', '0')
								->select('*')
								->get();
							$arr_events->upvote_count = count($arr_event_upvtes);
							$arr_event_downvote = DB::table('trans_event_upvote_downvote')
								->where('event_id_fk', $arr_events->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_events->downvote_count = count($arr_event_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_events->user_id_fk)
								->select('status')
								->first();
							$arr_events->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_events->user_id_fk)
								->get();
							$arr_events->follower_count = count($arr_follower_count);
							$arr_to_return_events_hotness = $this->hotness($arr_events->upvote_count, $arr_events->downvote_count, $arr_events->created_at);
//assigning key into array with resulted hotness
							$arr_events->hotness = $arr_to_return_events_hotness;
							$arr_events->type = 'event';
//                            $arr_post_data[$key] = $arr_events;
							$arr_post_data[] = $arr_events;
						}
					}
					if ($type == "job") {

						$arr_jobs2 = DB::table('mst_job as e');
						$arr_jobs2->where('e.id', $val->post_id_fk);
						$arr_jobs2->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');

						if (isset($page)) {
							$arr_jobs2->limit($page);
						}
						$arr_jobs2->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender as gender', 'u.profile_picture as profile_picture', 'e.id', 'e.user_id_fk', 'e.tags', 'e.title as title', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at');
//                        $arr_jobs = $arr_jobs2->get();
						$arr_jobs = $arr_jobs2->first();

						if (count($arr_jobs) > 0) {
							$arr_desc = strip_tags($arr_jobs->description);
							$arr_jobs->description = $arr_desc;
							$link = url('/') . '/view-job/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_jobs->title) . '-' . $arr_jobs->id;
							$arr_jobs->share_link = $link;
							$arr_job_upvotes = DB::table('trans_job_upvote_downvote')
								->where('job_id_fk', $arr_jobs->id)
								->where('status', '0')
								->select('*')
								->get();
							$arr_jobs->upvote_count = count($arr_job_upvotes);
							$arr_job_downvote = DB::table('trans_job_upvote_downvote')
								->where('job_id_fk', $arr_jobs->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_jobs->downvote_count = count($arr_job_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_jobs->user_id_fk)
								->select('status')
								->first();
							$arr_jobs->follower_status = isset($arr_follower_status->status) ? $arr_follower_status->status : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_jobs->user_id_fk)
								->get();
							$arr_jobs->follower_count = count($arr_follower_count);
							$arr_to_return_jobs_hotness = $this->hotness($arr_jobs->upvote_count, $arr_jobs->downvote_count, $arr_jobs->created_at);
//assigning key into array with resulted hotness
							$arr_jobs->hotness = $arr_to_return_jobs_hotness;
							$arr_jobs->type = 'job';
							$arr_post_data[] = $arr_jobs;
						}
					}
					if ($type == "education") {
						$arr_education = DB::table('mst_education as e');
						$arr_education->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
						$arr_education->where('e.id', $val->post_id_fk);
						if (isset($page)) {
							$arr_education->limit($page);
						}
						$arr_education->select('e.id', 'e.tag', 'e.user_id_fk', 'e.title as title', 'e.profile_image as image', DB::raw('substr(description,1,100) as description'), 'e.updated_at', 'e.created_at', 'u.user_name', 'u.gender', 'u.profile_picture', 'u.id as user_id');
						$arr_education = $arr_education->get();
						if (count($arr_education) > 0) {
							$arr_desc = strip_tags($arr_education->description);
							$arr_education[$key]->description = $arr_desc;
							$link = url('/') . '/view-education/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_education->title) . '-' . $arr_education->id;
							$arr_education[$key]->share_link = $link;
							$education_comment = educationCommentModel::where('education_id_fk', $arr_education->id)->get();
							$arr_education[$key]->count = count($education_comment);
							$arr_educatio_upvotes = DB::table('trans_education_upvote_downvote')
								->where('education_id_fk', $arr_education->id)
								->where('status', '0')
								->select('*')
								->get();
							$arr_education[$key]->upvote_count = count($arr_educatio_upvotes);
							$arr_education_downvote = DB::table('trans_education_upvote_downvote')
								->where('education_id_fk', $arr_education->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_education[$key]->downvote_count = count($arr_education_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_education->user_id_fk)
								->select('status')
								->first();
							$arr_education[$key]->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_education->user_id_fk)
								->get();
							$arr_education[$key]->follower_count = count($arr_follower_count);
							if ($arr_education->tag != '') {
								$arr_tag = explode(',', $arr_education->tag);
								$tag = array();

								foreach ($arr_tag as $k => $val1) {
									$tag[$k]['tag_name'] = $val1;
								}
								$arr_education[$key]->arr_tag = $tag;
							} else {
								$arr_education[$key]->arr_tag = [];
							}
							$arr_to_return_education_hotness = $this->hotness($arr_education[$key]->upvote_count, $arr_education[$key]->downvote_count, $arr_education->created_at);
//assigning key into array with resulted hotness
							$arr_education[$key]->hotness = $arr_to_return_education_hotness;
							$arr_education[$key]->type = 'education';
							$arr_post_data[$key] = $arr_education;
						}
					}
					if ($type == "meetup") {
						$arr_meetup = DB::table('mst_meetup as m');
						$arr_meetup->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
						$arr_meetup->where('m.id', $val->post_id_fk);
						if (isset($page)) {
							$arr_meetup->limit($page);
						}
						$arr_meetup->select('u.id as user_id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.gender', 'u.tagline', 'm.location', 'm.id', 'm.user_id_fk', 'm.title as title', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', DB::raw('substr(description,1,100) as description'), 'm.updated_at', 'm.created_at');
						$arr_meetup = $arr_meetup->first();
						if (isset($arr_meetup)) {
							$arr_desc = strip_tags($arr_meetup->description);
							$arr_meetup->description = $arr_desc;
							$link = url('/') . '/view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $arr_meetup->title) . '-' . $arr_meetup->id;
							$arr_meetup->share_link = $link;
							$meetup_comment = commentMeetup::where('meetup_id_fk', $arr_meetup->id)->get();
							$arr_meetup->count = count($meetup_comment);

							$arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')
								->where('meetup_id_fk', $arr_meetup->id)
								->where('status', '0')
								->select('*')
								->get();

							$arr_meetup->upvote_count = count($arr_meetup);
							$arr_meetup_downvote = DB::table('trans_meetup_upvote_downvote')
								->where('meetup_id_fk', $arr_meetup->id)
								->where('status', '1')
								->select('*')
								->get();
							$arr_meetup->downvote_count = count($arr_meetup_downvote);

							$arr_follower_status = DB::table('trans_follow_user')
								->where('follower_id_fk', $user_id)
								->where('user_id_fk', $arr_meetup->user_id_fk)
								->select('status')
								->first();
							$arr_meetup->follower_status = (count($arr_follower_status) > 0) ? "1" : '0';

							$arr_follower_count = DB::table('trans_follow_user')
								->where('user_id_fk', $arr_meetup->user_id_fk)
								->get();
							$arr_meetup->follower_count = count($arr_follower_count);
							$arr_to_return_meetup_hotness = $this->hotness($arr_meetup->upvote_count, $arr_meetup->downvote_count, $arr_meetup->created_at);
//assigning key into array with resulted hotness
							$arr_meetup->hotness = $arr_to_return_meetup_hotness;
							$arr_meetup->type = 'meetup';
//                            $arr_post_data[$key] = $arr_meetup;
							$arr_post_data[] = $arr_meetup;
						}
					}

//                    if ($type == "people" || $type == "") {
					//                        $people_data = DB::table('mst_users as u');
					//                        $people_data->leftJoin('trans_group_users as g', 'g.user_id_fk', '=', 'u.id', 'left');
					//                        $people_data->whereIn('g.group_id_fk', $all_data);
					//                        $people_data->select('u.id', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'u.profession', 'u.gender', 'location');
					//                        $people_data->groupBy('u.id');
					//                        if (isset($total_records)) {
					//                            $people_data->limit($total_records);
					//                        }
					//                        $people_data = $people_data->get();
					//                        $arr_post_data[$key] = $people_data;
					//                    }
				}
				if (count($arr_post_data) > 0) {
					$arr_return_data = array('error_code' => '0', 'msg' => 'success', 'arr_data' => $arr_post_data);
				} else {
					$arr_return_data = array('error_code' => '1', 'msg' => 'record not found');
				}
			} else {
				$arr_return_data = array('error_code' => '1', 'msg' => 'record not found');
			}
			echo json_encode(
				$arr_return_data);
		}
	}

	public function wsDeleteMySavedPost() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$type = $request['type'];
		$post_id = $request['post_id'];

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		if ($user_id != '' && $type != '' && $post_id != '') {
			if ($type == "event") {
				$feed_type = "1";
			} elseif ($type == "article") {
				$feed_type = "2";
			} elseif ($type == "job") {
				$feed_type = "4";
			} elseif ($type == "meetup") {
				$feed_type = "0";
			} elseif ($type == "qrious") {
				$feed_type = "3";
			} elseif ($type == "education") {
				$feed_type = "5";
			}
			$arr_saved_post = DB::table('trans_save_user_posts as p');
			$arr_saved_post->where('p.user_id_fk', $user_id);
			$arr_saved_post->where('p.type', $feed_type);
			$arr_saved_post->where('p.post_id_fk', $post_id);
			$arr_saved_post->select('*');
			$arr_saved_post = $arr_saved_post->get();
			if (count($arr_saved_post) > 0) {

				foreach ($arr_saved_post as $val) {
					$arr_saved_post = saveUserPost::where('id', $val->id)->delete();
				}
				$array_json_return = array('error_code' => '0', 'msg' => 'success');
			} else {
				$array_json_return = array('error_code' => '1', 'msg' => 'record not found');
			}
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'Invalid');
		}

		echo json_encode(
			$array_json_return);
	}

	public function wsAcceptRequest() {
		$request = Input::all();
		$from_id = $request['from_id'];
		$user_id = $request['user_id'];

		if ($request['status'] == '0') {
			$accept = DB::table('trans_user_connection as user_connection')->where('to_user_id', $user_id)->where('from_user_id', $from_id)
				->update(['status' => '0']);
			$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'requested');
		} else if ($request['status'] == '1') {

			$accept = DB::table('trans_user_connection as user_connection')->where('to_user_id', $user_id)->where('from_user_id', $from_id)
				->update(['status' => '1']);
			$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'accepted');
		} else if ($request['status'] == '2') {

			$accept = DB::table('trans_user_connection as user_connection')->where('to_user_id', $user_id)->where('from_user_id', $from_id)
				->update(['status' => '2']);
			$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'rejected');
		} else {
			$reject = DB::table('trans_user_connection as user_connection')->where('to_user_id', $user_id)->where('from_user_id', $from_id)
				->update(['status' => '3']);
			$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'blocked');
		}
		echo json_encode(
			$array_json_return);
	}

	public function wsSingleConnectRequest() {
		$request = Input::all();
		$to_user_id = $request['to_user_id'];
		$from_user_id = $request['from_user_id'];

		if ($request['status'] == '0') {
			$reqt = UserConnection::where(['to_user_id' => $to_user_id, 'from_user_id' => $from_user_id])->first();

			if (count($reqt) > 0) {
				$array_json_return = array('error_code' => '1', 'msg' => 'You have already requested');
			} else {

				$connect_data = new UserConnection();
				$connect_data->to_user_id = $to_user_id;
				$connect_data->from_user_id = $from_user_id;
				$connect_data->status = $request['status'];
				$connect_data->save();
				$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'accepted');
			}
		} else if ($request['status'] == '2') {

			$reqt = DB::table('trans_user_connection as user_connection')->where('to_user_id', $to_user_id)->where('from_user_id', $from_user_id)
				->update(['status' => $request['status']]);
			$array_json_return = array('error_code' => '0', 'msg' => 'success', 'status' => 'requested');
		} else {
			$array_json_return = array('error_code' => '1', 'msg' => 'error', 'Invalide');
		}
		echo json_encode(
			$array_json_return);
	}

/**
 * @SWG\Post(
 *   path="/ws-apply-job",
 *   summary="Apply Job",
 *   tags={"job"},
 *   description="Apply to a job",
 *   operationId="apply to job",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="job_id",
 *       in="formData",
 *       description="job id on which user_id is applying",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="resume_id",
 *       in="formData",
 *       description="User's resume id",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="experience",
 *       in="formData",
 *       description="user experience in his field",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 * @SWG\Parameter(
 *       name="qualification",
 *       in="formData",
 *       description="user qualification in his field",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 * @SWG\Parameter(
 *       name="why_we_hire",
 *       in="formData",
 *       description="user experience in his field",
 *       required=true,
 *       type="string",
 *       @SWG\Items(type="string")
 *   ),
 *     @SWG\Response(response="200", description="0:Success msg:applied, 1:Fail  msg: Please select atleast one group to proceed further, 2:fail  msg: your account type should be student or professional to apply to a job.")
 * )
 **/
	public function wsApplyJob() {
		$request = Input::all();
		$user_id = $request['user_id'];
		$job_id = $request['job_id'];
		$experience = $request['experience'];
		$qualification = $request['qualification'];
		$whywehire=$request['why_we_hire'];
		$resume_id=$request['resume_id'];
		
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		
		$arr_user_data = User::find($user_id);

		$get_groups = DB::table('trans_group_users')
		->where('user_id_fk', $user_id)
		->get();
		if (count($get_groups) == 0) {
			return json_encode(['err'=>1,'err_msg'=>'Please select atleast one group to proceed further.']);
			
		}

		if ($arr_user_data['user_type'] == "2" || $arr_user_data['user_type'] == "4") {
			return json_encode(['err'=>2,'err_msg'=>'Your account type should be student or professional to apply to a job.']);
		}

		
				$app_job = new ApplicantToJobModel;
				$app_job->job_id_fk = $job_id;
				$app_job->user_id_fk = $user_id;
				$app_job->application_date = date("Y-m-d H:i:s");
				$app_job->status = '1';
				$app_job->resume_id = $resume_id;
				$app_job->save();


				$job_data = JobModel::find($job_id);

				$message = ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']) . ' ' . ' has applied to ' . ucwords($job_data['title']) . '.';
                $subject = ' has applied to ' . ucwords($job_data['title']) . '.';
				$insert_data = array(
					'notification_from' => $user_id,
					'notification_to' => $job_data['user_id_fk'],
					'notification_type' => "5",
                    'subject' => $subject,
                    'post_id' => $job_id,
					'team_id_fk' => $job_id,
					'message' => trim($message),
					'notification_status' => 'send',
					'created_at' => date('Y-m-d H:i:s'),
				);
				$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);


				/** Send job applier users information to job posted user * */
				$get_job_posted_user_details = DB::table('mst_users')
					->where('id', $job_data['user_id_fk'])
					->select('first_name', 'email', 'last_name')
					->first();


				/* change status to apply in student saved job */
				DB::table('trans_student_saved_job')
					->where('user_id_fk', $user_id)
					->where('job_id_fk', $job_id)
					->where('status', '0')
					->update(['user_id_fk' => $user_id, 'job_id_fk' => $job_id, 'status' => '1']);	

				// $get_job_appier_user_details = DB::table('mst_users as mu')
				// 	->join('mst_job as mj', 'mj.user_id_fk', '=', 'mu.id')
				// 	->select('mu.first_name', 'mu.email', 'mu.last_name', 'mj.title', 'mj.id')
				// 	->first();

				//TODO : Reduce queries here
					\Log::info("TOGETHER");
				$arr = User::find($job_data['user_id_fk']);
					\Log::info($arr);
        		if ($arr->getUserEmailSchedule == '' || $arr->getUserEmailSchedule->job_notification == 1) {					
        			\Log::info("TOGE222THER");
					$commonModel = new CommonModel();
					$data = $commonModel->commonFunction();
					$email_contents = email_templates::where(array('email_template_title' => 'applied-mail-to-user', 'lang_id' => '14'))->first();
					$content = $email_contents['email_template_content'];
					$reserved_arr = array
						("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
						"||SITE_PATH||" => '<a href="' . url('/') . '">Go to site</a>',
						"||NAME||" => $get_job_posted_user_details->first_name . ' ' . $get_job_posted_user_details->last_name,
						"||APPAYER_NAME||" => $arr_user_data->first_name . ' ' . $arr_user_data->last_name,
						'||JOB_NAME||' => $job_data['title'],
					);

					$reserved_words = array();
					$macros_array_detail = array();
					$macros_array_detail = email_template_macros::all();
					$macros_array = array();
					foreach ($macros_array_detail as $row) {
						$macros_array[$row['macros']] = $row['value'];
					}

					$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
					foreach ($reserved_words as $k => $v) {
						$content = str_replace($k, $v, $content);
					}
					$contents_array = array("email_conents" => $content);

					// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.

					Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($contents_array, $data, $email_contents, $get_job_posted_user_details) {

						$message->from('no-reply@atg.world', $data['global']['site_title']);

						$message->to($get_job_posted_user_details->email)->subject($email_contents['email_template_subject']);
					});

				}
				return json_encode(array('err' => 0, 'msg' => 'Applied'));
	

	}

	public function wsConnectUserDetails() {
		$request = Input::all();
		$user_id = $request['user_id'];

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		if ($user_id != "") {
			$arr_user_group = DB::table('trans_group_users')
				->select('group_id_fk')->where('user_id_fk', $user_id)
				->get();
			if (count($arr_user_group) > 0) {

				foreach ($arr_user_group as $key => $val) {
					$arr_group = DB::table('mst_group')
						->select('id', 'group_name', 'icon_img', 'profession', 'cover_img')
						->where('id', $val->group_id_fk)
						->first();
					$arr_temp_grp[$key] = $arr_group;
				}
				$arr_user_group = $arr_group;
			} else {
				$arr_user_group = '';
			}
			$arr_to_return['arr_group_details'] = $arr_temp_grp;
			$arr_u_data = DB::table('mst_users')->select('*')
				->where('id', $user_id)
				->get();
			$arr_user_data = array();
			foreach ($arr_u_data as $object) {
				$arr_user_data = (array) $object;
			}
			$arr_to_return['arr_user_data'] = $arr_user_data;
			$array_json_return = array('error_code' => '1', 'msg' => 'success', "details" => $arr_to_return);
		}
		echo json_encode($array_json_return);
	}

/**
 * @SWG\Get(
 *   path="/ws-get-resume",
 *   summary="get resume",
 *   tags={"job"},
 *   description="get resume",
 *   operationId="apply job",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user whose resume list is to be fetched",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *
 *     @SWG\Response(response="200", description="0:Success, 1:error or empty, return=resumes: contains [id,file_name] ,number of resumes as count")
 * )
 **/
	public function wsGetResume(){
		$req=Input::all();
		if(!isset($req['user_id'])){
			return json_encode(['err'=>2]);
		}
		$user_id=$req['user_id'];
		$rs=UserResumeModel::where('user_id_fk',$user_id)->get();
		$c=count($rs);
		$resArr=array();
		if($c>0){
			foreach($rs as $r){
				$resArr[]=['id'=>$r->resume_id,'file_name'=>config('resume_url').$r->uploaded_path];
			}
			
			return json_encode(['err'=>0,'count'=>$c,'resumes'=>$resArr]);
		}
		return json_encode(['err'=>1]);	
		
	}


	/**
 * @SWG\Post(
 *   path="/ws-upload-resume",
 *   summary="upload resume",
 *   tags={"job"},
 *   description="upload resume",
 *   operationId="apply job",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="formData",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *  @SWG\Parameter(
 *       name="file",
 *       in="formData",
 *       description="Resume file",
 *       required=true,
 *       type="file",
 *       @SWG\Items(type="string")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:file can't be empty, return=resume_id and user_id")
 * )
 **/

	public function wsUploadResume(Request $req){
		$user_id=$req->user_id;
		if($req->hasFile('file')){
			$file = $req->file('file');
			$filename = rand(1,1000).$file->getClientOriginalName();
			$this->upload_resume($user_id,$_FILES['file']['tmp_name'],$filename);
			$rs= new UserResumeModel;
			$rs->user_id_fk=$user_id;
			$rs->uploaded_path=$filename;
			$rs->save();
			return(json_encode(['err'=>0,'msg'=>'successfully uploaded resume','resume_id'=>$rs->id,'user_id'=>$user_id]));

		}
		return(json_encode(['err'=>1,'msg'=>'error in uploading resume maybe file not attached properly']));
		
	}

		/**
 * @SWG\Get(
 *   path="/ws-get-user-followers",
 *   summary="user-follow-details",
 *   tags={"follow"},
 *   description="get user followers list",
 *   operationId="get followers",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="query",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *  @SWG\Parameter(
 *       name="page_number",
 *       in="query",
 *       description="page number starts from 0 and by default it is 0",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="count",
 *       in="query",
 *       description="count of followers per page to be displayed, by default it is 10",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:user_id missing, return:   ['err'=>0,'msg'=>'successful','followers'=>$arr_user_follower,'followers__total_count'=>$arr_user_follower_count]")
 * )
 **/

	public function wsGetUserFollowers(Request $request){
		if(isset($request->user_id))
		$user_id=$request->user_id;
		else
		return json_encode(['err'=>1,'msg'=>'missing user_id']);
		$user_id=$request->user_id;
		$no=isset($request->page_number)?$request->page_number:0;
		$nos=isset($request->count)?$request->count:10;
		$skp=$no*$nos;
		$arr_user_follower_count = DB::table('trans_follow_user as follower')
		->leftjoin('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
		->where('user_id_fk', $user_id)
		->where('status', '1')
		->count();
		$arr_user_follower = DB::table('trans_follow_user as follower')->leftjoin('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
		->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'user.profile_picture', 'user.last_name')
		->where('user_id_fk', $user_id)
		->where('status', '1')
		->skip($skp)->take($nos)->get();

		foreach($arr_user_follower as $key=>$value){
			if(!empty($arr_user_follower[$key]->profile_picture))
			$arr_user_follower[$key]->profile_picture=config('profile_pic_url').$arr_user_follower[$key]->follower_id_fk.'/thumb/'.$arr_user_follower[$key]->profile_picture;

		}

		return json_encode(['err'=>0,'msg'=>'successful','followers'=>$arr_user_follower,'followers__total_count'=>$arr_user_follower_count]);
		
	}
/**
 * @SWG\Get(
 *   path="/ws-get-user-followings",
 *   summary="user-follow-details",
 *   tags={"follow"},
 *   description="get user list to whom given user is followings",
 *   operationId="get user following list",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="query",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *  @SWG\Parameter(
 *       name="page_number",
 *       in="query",
 *       description="page number starts from 0 and by default it is 0",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="count",
 *       in="query",
 *       description="count of followers per page to be displayed, by default it is 10",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:user_id missing, return:  ['err'=>0,'msg'=>'successful','followings'=>$arr_user_following,'followings__total_count'=>$arr_user_following_count]")
 * )
 **/


	public function wsGetUserFollowings(Request $request){
		if(isset($request->user_id))
		$user_id=$request->user_id;
		else
		return json_encode(['err'=>1,'msg'=>'missing user_id']);
		$no=isset($request->page_number)?$request->page_number:0;
		$nos=isset($request->count)?$request->count:10;
		$skp=$no*$nos;

		$arr_user_following_count =DB::table('trans_follow_user as follower')
		->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
		->where('follower_id_fk', $user_id)
		->where('status', '1')
		->count();
		$arr_user_following = DB::table('trans_follow_user as follower')
		->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
		->select('follower.user_id_fk', 'user.first_name', 'user.gender', 'user.last_name', 'user.gender', 'user.profile_picture')
		->where('follower_id_fk', $user_id)
		->where('status', '1')
		->skip($skp)->take($nos)->get();

		foreach($arr_user_following as $key=>$value){
			if(!empty($arr_user_following[$key]->profile_picture))
			$arr_user_following[$key]->profile_picture=config('profile_pic_url').$arr_user_following[$key]->user_id_fk.'/thumb/'.$arr_user_following[$key]->profile_picture;

		}

		return json_encode(['err'=>0,'msg'=>'successful','followings'=>$arr_user_following,'followings__total_count'=>$arr_user_following_count]);
	}


/**
 * @SWG\Get(
 *   path="/ws-get-user-connections",
 *   summary="user-connections-details",
 *   tags={"User"},
 *   description="get user connections list",
 *   operationId="get user connections list",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="query",
 *       description="user id of the user who is logged in",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *  @SWG\Parameter(
 *       name="page_number",
 *       in="query",
 *       description="page number starts from 0 and by default it is 0",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 * @SWG\Parameter(
 *       name="count",
 *       in="query",
 *       description="count of connections per page to be displayed, by default it is 10",
 *       required=false,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *
 *     @SWG\Response(response="200", description="0:Success, 1:user_id missing, return: ['err'=>0,'msg'=>'successful','connections'=>$all_connection,'total_connections'=>$all_connections_count]")
 * )
 **/
	public function wsGetUserConnections(Request $req){
	
			if(isset($req->user_id)){
				$user_id=$req->user_id;
			}
			else{
				return json_encode(['err'=>1,'msg'=>'user id not found']);
			}
			$nos=isset($req->count)?$req->count:10;
			$no=isset($req->page_number)?$req->page_number:0;
			$skp=$no*$nos;
	
			$all_connections_count = DB::table('trans_user_connection')->where('from_user_id', $user_id)->orWhere('to_user_id', $user_id)->where('status', '1')->count();
			$all_connections = DB::table('trans_user_connection as user_connection')
			->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
			->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
			->select('to_user.id as to_user_id','to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.id as from_user_id','from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
			->where('user_connection.from_user_id', $user_id)
			->orWhere('user_connection.to_user_id', $user_id)
			->where('status', '1')
			->skip($skp)->take($nos)->get();
			foreach($all_connections as $key=>$val){
				if(!empty($all_connections[$key]->to_profile_picture))
				$all_connections[$key]->to_profile_picture=config('profile_pic_url').$all_connections[$key]->to_user_id.'/thumb/'.$all_connections[$key]->to_profile_picture;
				if(!empty($all_connections[$key]->from_profile_picture))
				$all_connections[$key]->from_profile_picture=config('profile_pic_url').$all_connections[$key]->from_user_id.'/thumb/'.$all_connections[$key]->from_profile_picture;
				
			}
			return json_encode(['err'=>0,'msg'=>'successful','connections'=>$all_connections,'total_connections'=>$all_connections_count]);
		}
	
/**
 * @SWG\Get(
 *   path="/ws-get-status-of-x-f-y",
 *   summary="check whether x follows y or not",
 *   tags={"User"},
 *   description="check whether x follows y or not",
 *   operationId="check whether x follows y or not",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id_x",
 *       in="query",
 *       description="user id of the user x",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="user_id_y",
 *       in="query",
 *       description="user id of the user y",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *
 *     @SWG\Response(response="200", description="0:Success, return: ['err'=>0,'msg'=>'successful','status'=>$status] status 1 : X follows Y, status 0 : X does not follow Y")
 * )
 **/

	public function wsGetXfollowsYOrNot(Request $request){
		if(isset($request->user_id_x) && isset($request->user_id_y)){
			$user_id_x=$request->user_id_x;
			$user_id_y=$request->user_id_y;
			
		}
		else
		return json_encode(['err'=>1,'msg'=>'missing user_id']);
		
		$chk = DB::table('trans_follow_user as follower')
		->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
		->where('follower_id_fk', $user_id_x)
		->where('user_id_fk',$user_id_y)
		->where('status', '1')
		->count();
		$status=($chk>0)?1:0;

		return json_encode(['err'=>0,'msg'=>'successful','status'=>$status]);
	}

/**
 * @SWG\Get(
 *   path="/ws-follow-unfollow",
 *   summary="follow unfollow a user",
 *   tags={"follow"},
 *   description="follow unfollow a user",
 *   operationId="follow unfollow a user",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 *       name="user_id",
 *       in="query",
 *       description="user id of the user to be followed",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  @SWG\Parameter(
 *       name="follower_id",
 *       in="query",
 *       description="user id of the user who is following",
 *       required=true,
 *       type="integer",
 *       @SWG\Items(type="integer")
 *   ),
 *  
 *
 *   @SWG\Response(response="200", description="0:Success, 1:fail, return: [follow_status->0 means unfollowed and 1 means followed; follower_count : count of people following user_id after the API call ")
 * )
 **/


	public function wsFollowUnfollowUser(Request $request){
		if(isset($request->user_id) && isset($request->follower_id)){
			$user_id=$request->user_id;
			$follower_id=$request->follower_id;
		}
		else
		return json_encode(['err'=>1,'msg'=>'missing user_id or follower_id']);

		$returned_arr = $this->followUnfollowUserfx($user_id,$follower_id);
		echo json_encode(['err'=>0,'follow_status' => $returned_arr['follow_status'], 'follower_count' => $returned_arr['follower_count']]);
	}

    /**
     * @SWG\POST (
     *   path="/ws-get-trending-groups",
     *   summary="Get trending groups",
     *   tags={"group"},
     *   description="Get trending groups",
     *   operationId="Get trending groups",
     *   produces={"application/json"},
     *   @SWG\Parameter (
     *     name="limit",
     *     in="formData",
     *     description="limit number of groups per request",
     *     default=10,
     *     required=false,
     *     type="integer",
     *     @SWG\Items(type="integer")
     *   ),
     *   @SWG\Parameter (
     *     name="offset",
     *     in="formData",
     *     description="number of skipped groups",
     *     default=0,
     *     required=false,
     *     type="integer",
     *     @SWG\Items(type="integer")
     *   ),
     *   @SWG\Response(
     *     response="200",
     *     description="success, array of trending groups"
     *   )
     * )
     **/
    function wsGetTrendingGroups(Request $request)
    {
        return Group::getTrendingGroups($request->get('limit', 10), $request->get('offset', 0));
    }

    /**
     * @SWG\POST (
     *   path="/ws-get-major-groups",
     *   summary="Get major groups",
     *   tags={"group"},
     *   description="Get major groups",
     *   operationId="Get major groups",
     *   produces={"application/json"},
     *   @SWG\Parameter (
     *     name="limit",
     *     in="formData",
     *     description="limit number of groups per request",
     *     default=10,
     *     required=false,
     *     type="integer",
     *     @SWG\Items(type="integer")
     *   ),
     *   @SWG\Parameter (
     *     name="offset",
     *     in="formData",
     *     description="number of skipped groups",
     *     default=0,
     *     required=false,
     *     type="integer",
     *     @SWG\Items(type="integer")
     *   ),
     *   @SWG\Response(
     *     response="200",
     *     description="array of major groups"
     *   )
     * )
     **/
    function wsGetMajorGroups(Request $request)
    {
        return Group::getMajorGroups($request->get('limit', 10), $request->get('offset', 0));
    }

    /**
     * @SWG\POST (
     *   path="/ws-get-group-details-by-id",
     *   summary="Get the details of a group",
     *   tags={"group"},
     *   description="Get group details",
     *   operationId="Get group details",
     *   produces={"application/json"},
     *   @SWG\Parameter (
     *     name="group_id",
     *     in="formData",
     *     description="ID of a group",
     *     required=true,
     *     type="integer",
     *     @SWG\Items(type="integer")
     *   ),
     *   @SWG\Response(
     *     response="200",
     *     description="group details"
     *   )
     * )
     **/
    public function wsGetGroupDetailsById(Request $request)
    {
        if ($request->get('group_id', null) === null) {
            return [
                'error' => true,
                'message' => 'missing group ID'
            ];
        }

        return Group::getDetailsById($request->get('group_id'));
    }
}

