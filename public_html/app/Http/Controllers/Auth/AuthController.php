<?php

namespace App\Http\Controllers\Auth;

use App\QuestionModel;
use App\User;
use App\AdminUser;
use App\Institute;
use App\CommonModel;
use App\email_templates;
use App\email_template_macros;
use App\trans_global_settings;
use App\cms;
use App\group;
use App\GroupUser;
use App\Filter;
use App\UserConnection;
use App\FollowerUser;
use App\RegisterModel;
use Mockery\Exception;
use Validator;
use App\city_model;
use App\state_model;
use App\UserProfileSettings;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
Use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Mail;
use DB;
use Cookie;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Filesystem\Filesystem;
use App\UserProfileImages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\UserEmailSchedule;
use App\Traits\sendsignupmail;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    use sendsignupmail;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => ['getLogout', 'getAdminLogout']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function getLogin() {
        $user = Auth::user();

        return view('Frontend.auth.login', ['name' => 'User Login']);
    }

    protected function getAdminLogin() {

        return view('Backend.login.admin-login');
    }

    public function checkUserEmail() {
        $all = Input::all();
        /* checking  user email already exist or not for add user */
        $arr_user_detail = AdminUser::where('email', $all['user_email'])->first();
        if (empty($arr_user_detail)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function checkEmailExist() {
        $all = Input::all();

        /* checking  user email already exist or not for add user */
        $arr_user_detail = AdminUser::where('email', $all['email'])->first();
        if (empty($arr_user_detail)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function checkPassword() {
        $all = Input::all();
        $email = $all['user_email'];
        $password = $all['password'];
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            echo "true";
        } else {
            echo "false";
        }
    }

    protected function postAdminLogin(Request $request) {
        $user_name = $request->input('user_name', "");
        $password = $request->input('user_password', "");
        if (Auth::attempt(['user_name' => $user_name, 'password' => $password, 'user_type' => '2'])) {
            $user = Auth::User();

            if ($user->role_id != 1) {
                if ($user->user_type == 1) {
                    Auth::logout();
                    Session::flash('error_msg', 'Invalid, if you are trying to access site. Please go to home page.');
                    return redirect()->guest('/auth/admin/login');
                } else if ($user->user_status == 0 && $user->email_verified == 0) {
                    Auth::logout();
                    Session::flash('error_msg', 'Please activate your account by the clicking on link, we sent to your email account.');
                    return redirect()->guest('/auth/admin/login');
                } else if ($user->user_status == 2) {
                    Auth::logout();
                    Session::flash('error_msg', 'Your account has been blocked by admin. Please contact admin for more info.');
                    return redirect()->guest('/auth/admin/login');
                }
            }
            if ($user->role_id != '1') {
                $arr_privileges = DB::table('trans_role_privileges')
                        ->select(array('privilege_id'))
                        ->where('role_id', $user->role_id)
                        ->get();
                foreach ($arr_privileges as $object) {
                    $privileges[] = (array) $object;
                }
                $arr_privileges = $privileges;
                Session::put('user_privileges', $arr_privileges);
            }
            return redirect('admin/home');
        } else {

            Session::flash('error_msg', 'User name or password is invalid. Please try again.');
            return redirect()->guest('/auth/admin/login');
        }
    }

    protected function postUserLogin() {

        $request = Input::all();
        $email = $request['email'];
        $password = $request['password'];
        \Log::info($email.$password);
        if (Auth::attempt(['email' => $email, 'password' => $password])) {

            $user = Auth::User();

            if ($user->user_type == 2) {
                Auth::logout();
                Session::flash('error_msg', 'Invalid, if you are trying to access admin panel. Please go to admin panel login page.');
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Auth::logout();
                $url = '<a href="' . url('/') . '/cms/terms-services">terms</a>';
                Session::flash('error_msg', 'This account has been blocked for violating terms. Please read ' . $url . ' before registering again.');
                return redirect()->guest('/');
            } else if ($user->signup_step == "1") {
                Session :: put('user_account', $user);
                Session::flash('error_msg', 'Please select atleast one group to proceed further.');
                return redirect()->guest('/signup2');
            } else if (isset($user->email_verified) && $user->email_verified == '0') {
                $this->sendEmailVerificationMail($email, $user->toarray());
                Session :: put('user_account', $user);
                \Log::info($user);
                $get_groups = DB::table('trans_group_users')
                        ->where('user_id_fk', $user['id'])
                        ->get();
                // if user's group count is zero, redirect to signup2 to choose groups first
                if (count($get_groups) == 0) {
                    Session::flash('error_msg', 'Please select atleast one group to proceed further.');
                    return redirect()->guest('/signup2');
                }
            }


            if (isset($request['remember'])) {
                $response = new Response('added');
                $array_cookie = array('email' => $email, 'password' => $password);
                setcookie('login_cr', serialize($array_cookie));
            } else {
                setcookie('login_cr', '');
            }

            Session::put('user_account', $user);
            Session::flash('success_msg', 'Thank You for returning. Have fun using ATG.');
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            if (isset($request['redirect_uri']) != '') {
                return redirect($request['redirect_uri']);
            } else {
                return redirect()->guest('/user-dashboard');
            }
        } else {
            Session::flash('error_msg', 'Invalid details, Please try again with valid details.');
            return redirect()->guest('/');
        }
    }

    protected function getRegister() {
        return view('Frontend.signup.signup1', ['name' => 'User Registration']);
    }


    protected function postRegister1(Request $request) {

        $validate_response = Validator::make($request->all(), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'user_email' => 'required|email|unique:mst_users,email',
                    'user_password' => 'required|min:6',
                    'cnf_user_password' => 'required|same:user_password',
                        )
        );

        if ($validate_response->fails()) {
            return redirect(url('/'))->withErrors($validate_response)->withInput();
        } else {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);
            $activation_code = time();
            try {
                $gender_api_data = json_decode(file_get_contents("https://gender-api.com/get?key=wKQrQHjxvbaVyNvucL&name=" . urlencode($request['first_name'])));
                $gender_result = $gender_api_data->gender;
                if ($gender_result == 'male') {
                    $gender = "0";
                } else if ($gender_result == 'female') {
                    $gender = "1";
                } else {
                    $gender = "0";
                }
            } catch (\Exception $e) {
                \Log::alert($e);
                $gender = "0";
            }
            $user = new User();
            $user->first_name = $request['first_name'];
            $user->last_name = $request['last_name'];
            $user->email = $request['user_email'];
            $user->password = $hash_password;
            $user->role_id = '0';
            $user->user_status = '1';
            $user->user_type = '1';
            $user->email_verified = '0';
            $user->signup_step = '2';
            $user->ip_address = $_SERVER['REMOTE_ADDR'];
            $user->activation_code = $activation_code;
            $user->created_at = date("Y-m-d H:i:s");
            $user->gender=$gender;
            $user->save();

            $obj_email = new UserEmailSchedule();
            $obj_email->user_id = $user->id;
            $obj_email->email_schedule = '0';
            $obj_email->day = '';
            $obj_email->date = '';
            $obj_email->event = '1';
            $obj_email->meetup = '1';
            $obj_email->education = '1';
            $obj_email->article = '1';
            $obj_email->qurious = '1';
            $obj_email->post_audience = '1';

            $obj_email->save();

            Session::put('user_account', $user);

            // add referred user with signup user..
            if ($request['r'] != '') {
                $commonModel = new CommonModel();
                $referralUser = $request['r'];
                $fromUser = User::where('user_name', $referralUser)->first();
                if (count($fromUser) > 0) {
                    $to_user_id = $user->id;
                    $arr_from_connection = UserConnection::where('to_user_id', '=', $to_user_id)->where('from_user_id', '=', $fromUser->id)->get();
                    if (count($arr_from_connection) > 0) {
                        $conn_id = $arr_from_connection['0']['id'];
                        UserConnection::find($conn_id)->delete();
                    } else if (count($arr_to_connection = UserConnection::where('to_user_id', '=', $fromUser->id)->where('from_user_id', '=', $to_user_id)->get()) > 0) {
                        $conn_id = $arr_to_connection['0']['id'];
                        UserConnection::find($conn_id)->delete();
                    } else {
                        $userconnection = new UserConnection();
                        $userconnection->to_user_id = $to_user_id;
                        $userconnection->from_user_id = $fromUser->id;
                        $userconnection->status = '1';
                        $userconnection->is_referral = '1';
                        $userconnection->save();

                        $subject = ' is now connected with you.';
                        $message = ucfirst($user->first_name) . ' ' . ucfirst($user->last_name) . ' is now connected with you.';
                        $insert_data = array(
                            'notification_from' => $user->id,
                            'notification_to' => $fromUser->id,
                            'notification_type' => "1",
                            'subject' => $subject,
                            'message' => trim($message),
                            'notification_status' => 'send',
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
                    }
                }
            }

            $this->sendEmailVerificationMail($request['user_email'], $user);

	        $activation_link = '<a style="color:springgreen; text-decoration:underline;" href="' . url('/') . '/again-send-mail"> Click here to receive the e-mail again.</a>';

            Session::flash('success_msg_resend_mail', "Please check your email. We've sent you a link that will allow you to complete your registration. <br> Please CHECK YOUR JUNK/SPAM folder as well for the email. <br> If you still don't receive the email, $activation_link");
            return redirect(url('/'));
        }
    }

    protected function getRegister2() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $user_account = Session::get('user_account');
        /*         * **This is for resend mail to user***** */
        $email = $user_account;
        session::put('email', $email);

        /*         * **end****** */
        $user_id = $user_account['id'];
        if ($user_account['first_name'] == '' && $user_account['user_email'] == '' && $user_account['id'] == '') {
            Session::flash('error_msg', 'Invalid details, Please try again with valid details.');
            return redirect()->guest('/');
        }

        $activation_link = '<a style="color:springgreen; text-decoration:underline;" href="' . url('/') . '/again-send-mail"> Click here to receive the e-mail again.</a>';

        if ($user_account['signup_step'] == '3') {
            $arr_group_user = GroupUser::where('user_id_fk', '=', $user_account['id'])->pluck('group_id_fk');

            if (count($arr_group_user) == 0) {
                $commonModel->updateRow('mst_users', array('signup_step' => '2'), array('id' => $user_account["id"]));
            } else {
                return redirect()->guest('/user-dashboard');
            }
        }

        $position = \Location::get();
        $position = \Location::get($_SERVER['REMOTE_ADDR']);
        $user_location = (($position->cityName != '') ? $position->cityName : '') . (($position->regionName != '') ? ', '.$position->regionName : '') . (($position->countryName != '') ? ', '.$position->countryName : '');
        // Updating IP address and lat/long values during signup so that we can show relevant posts on homepage
        $commonModel->updateRow('mst_users', array('ip_address' => $_SERVER['REMOTE_ADDR'], 'latitude' => $position->latitude, 'longitude' => $position->longitude, 'user_city' => $position->cityName, 'user_state' => $position->regionName, 'location' => $user_location ), array('id' => $user_account["id"]));

        $arr_parent_group = Group::where('parent_id', '0')->get();
        $user_id = $data['user_session']['id'];
        $arr_user_group = DB::table('trans_group_users')
                ->select('group_id_fk', 'status', 'created_at', 'till_date', db::raw('DATEDIFF(till_date,CURDATE()) as days_diff'))
                ->where('user_id_fk', $user_id)
                ->get();
        $data['arr_already_group'] = '';
        foreach ($arr_user_group as $key => $value) {
            $data['arr_user_group'][0][] = $value->group_id_fk;
            $data['arr_user_group'][1][$value->group_id_fk] = $value->days_diff;
            $data['arr_already_group'] .= ',' . $value->group_id_fk;
        }

        $data['header'] = array(
            "title" => 'Select Groups - Signup | ATG',
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.signup.signup2', [
            'name' => 'User Registration',
            'arr_parent_group' => $arr_parent_group,
            'finalData' => $data,
            'trending_groups' => Group::getTrendingGroups(10, 0)
        ]);
    }

    protected function postRegister2() {

        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        $request = Input::all();

        $arr_user_group1 = explode(',', $request['child_groups']);

        $arr_child_group = array();
        if (isset($request['chk_child_grp']) && $request['chk_child_grp'] != '') {
            $arr_child_group = $request['chk_child_grp'];
        }

        if (isset($request['chk_end_grp'])) {
            $arr_end_group = $request['chk_end_grp'];
        } else {
            $arr_end_group = array();
        }


        $arr_user_group = array_unique(array_merge($arr_user_group1, $arr_child_group, $arr_end_group));
        if ($user_account['first_name'] != '' && $user_account['id'] != '') {
            foreach ($arr_user_group as $user_group) {
                if ($user_group != '') {
                    $groupuser = new GroupUser();
                    $groupuser->group_id_fk = $user_group;
                    $groupuser->user_id_fk = $user_account['id'];
                    $groupuser->status = '1';
                    $groupuser->created_at = date("Y-m-d H:i:s");
                    $groupuser->save();
                    /* set default filter */
                    $get_filter = DB::table('trans_user_filter')
                            ->where('user_id_fk', $user_account['id'])
                            ->where('group_id_fk', $user_group)
                            ->get();
                    if (!(count($get_filter) > 0)) {

                        $filter = new Filter;
                        $filter->user_id_fk = $user_account['id'];
                        $filter->qrious = 100;
                        $filter->jobs = 100;
                        $filter->news = 100;
                        $filter->blogs = 100;
                        $filter->visibility = 100;
                        $filter->events = 100;
                        $filter->meetups = 100;
                        $filter->articles = 100;
                        $filter->education = 100;
                        $filter->group_id_fk = $user_group;
                        $filter->save();
                    }
                }
            }
            $commonModel->updateRow('mst_users', array('signup_step' => '3'), array('id' => $user_account["id"]));
            $temail = explode('@', $user_account['email'] );
            $emaildomain    = end( $temail );
            $publicdomains  = array("gmail.com", "hotmail.com", "yahoo.com", "yahoo.co.uk", "email.com", "ymail.com","live.com", "msn.com");
            if(!in_array($emaildomain, $publicdomains))
                return redirect(url('/') . '/signup3');
             else  
                return redirect(url('/') . '/user-dashboard');
            
        } else {
            Session::flash('error_msg', 'Invalid details, Please try again with valid details.');
            return redirect()->guest('/');
        }
    }

    protected function getRegister3(){
        $commonModel    = new CommonModel();
        $user_account   = Session::get('user_account');
        $arr_user_data  = DB::table('mst_users')->where('id', $user_account['id'])->first();

        $data['header'] = ["title" => 'Signup step3',"keywords" => '',"description" => ''];

        return view('Frontend.signup.signup3', ['finalData' => $data , 'arr_user_data' => $arr_user_data]);
    }

    protected function postRegister3()
    {
        $commonModel    = new CommonModel();
        $user_account   = Session::get('user_account');
        $request        = Input::all();
        $usertype       = $request['user_type'];

        if($usertype == 0 || $usertype == "" || $usertype == 2 )
        {
            Session::flash('error_msg', 'Invalid details, Please try again with valid details.');
            return redirect(url('/') . '/signup3');           
        }else
        {
            DB::table('mst_users')->where('id','=',$user_account['id'])->update(['user_type'=>$usertype]);
            Session::flash('success_msg', 'Account type updated successfully . Have fun using ATG.');
            return redirect(url('/') . '/user-dashboard');           
        }
        
    }



    protected function getRegister4() {

        $user_account = Session::get('user_account');
        $user_id = $user_account['id'];
        $arr_group_user = GroupUser::where('user_id_fk', '=', $user_account['id'])->pluck('group_id_fk');
        $arr_all_users = array();
        foreach ($arr_group_user as $group) {
            $arr_user = GroupUser::where('group_id_fk', '=', $group)->where('user_id_fk', '!=', $user_account['id'])->pluck('user_id_fk');
            if (count($arr_user) > 0) {
                array_push($arr_all_users, $arr_user[0]);
            }
        }

        $arr_users = array_unique($arr_all_users);
        $arr_follow_user = array();
        $arr_no_group = array();
        foreach ($arr_users as $user) {
            $arr_no_group = array_count_values($arr_all_users);
            $arr_follow_user[] = User::where('id', '=', $user)->select('first_name', 'last_name', 'id', 'profile_picture')->get();
        }

        $get_connections = DB::table('trans_user_connection')
                ->where('from_user_id', $user_id)
                ->orWhere('to_user_id', $user_id)
                ->where('status', '1')
                ->select('to_user_id', 'from_user_id')
                ->get();

        $condition1 = 'null';
        $temp_array = array();
        $user_ids = array();
        $arr_ids = array();
        if (count($get_connections) > 0) {
            foreach ($get_connections as $key => $value) {
                $user_ids[] = $value->to_user_id;
                $user_ids[] = $value->from_user_id;

                foreach ($user_ids as $ids) {
                    if (!in_array($ids, $arr_ids)) {
                        $arr_ids[] = $ids;
                    }
                }
            }
            $condition1 = implode(',', $arr_ids);
        }
        $user_group = array();
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();

        foreach ($get_groups as $key => $value) {
            $user_group[$key] = $value->group_id_fk;
        }
        $condition = implode(',', $user_group);
        $arr_events = array();
        $arr_events = DB::table('mst_event as e')
                ->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left')
                ->join('mst_users as user', 'user.id', '=', 'e.user_id_fk', 'left')
                ->where('e.status', '1')
                ->whereIn('g.group_id_fk', explode(',', $condition))
                ->orWhereIn('e.user_id_fk', $arr_ids)
                ->groupBy('e.id')
                ->select('e.*', 'e.title as name', 'user.gender', 'user.profile_picture as user_profile_image', 'e.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
                ->take('3')
                ->get();
//        foreach ($arr_events as $key => $event) {
//            $arr_events[$key]->type = 'event';
//        }

        $arr_meetup = array();
        $arr_meetup = DB::table('mst_meetup as e')
                ->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'e.id', 'left')
                ->join('mst_users as user', 'user.id', '=', 'e.user_id_fk', 'left')
                ->where('e.status', '1')
                ->whereIn('g.group_id_fk', explode(',', $condition))
                ->orWhereIn('e.user_id_fk', $arr_ids)
                ->groupBy('e.id')
                ->select('e.*', 'e.title as name', 'user.gender', 'user.profile_picture as user_profile_image', 'e.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
                ->take('3')
                ->get();

//        foreach ($arr_meetup as $key => $event) {
//            $arr_meetup[$key]->type = 'meetup';
//        }
        //dd($arr_events);
//        if (count($arr_events) > 0 && count($arr_meetup) > 0) {
//            $arr_feeds = (object) array_merge((array) $arr_events, (array) $arr_meetup);
//            $arr_random_feeds = $this->shuffle_assoc($arr_feeds);
//            $arr_random_feeds = (object) $arr_random_feeds;
//            foreach ($arr_random_feeds as $item) {
//                foreach ($item as $i) {
//                $arr[]=$i;
//                }
//            }
//            $arr=(object)$arr;
//
//        } else {
//            $arr_random_feeds=array();
//            $arr_random_feeds = (object) $arr_random_feeds;
//        }

        /*         * ****(mamata)**** */
        $arr_events1 = array();
        foreach ($arr_events as $key => $event_value) {
            $arr_events1[] = $event_value;
            $arr_events1[$key]->type = 'event';
        }
        $arr_meetup1 = array();
        foreach ($arr_meetup as $key => $meetup_value) {
            $arr_meetup1[] = $meetup_value;
            $arr_meetup1[$key]->type = 'meetup';
        }
        $arr_feeds = array_merge($arr_events1, $arr_meetup1);
        $arr_random_feeds = $this->shuffle_assoc($arr_feeds);
        $arr_random_feeds = $arr_random_feeds;

        $data['header'] = array(
            "title" => 'Invite Friends',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.signup.signup4', ['name' => 'User Registration', 'arr_follow_user' => $arr_follow_user, 'arr_no_group' => $arr_no_group, 'finalData' => $data, 'arr_feeds' => $arr_random_feeds]);
    }

    function shuffle_assoc($list) {
        if (!is_array($list))
            return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    protected function addFollower() {
        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        $request = Input::all();
        $follower_user_id = $request['follower_user_id'];
        $arr_follower = FollowerUser::where('user_id_fk', '=', $follower_user_id)->where('follower_id_fk', '=', $user_account['id'])->get();

        if ($user_account['first_name'] != '' && $user_account['id'] != '') {
            if (count($arr_follower) > 0) {
                FollowerUser::where('user_id_fk', '=', $follower_user_id)->where('follower_id_fk', '=', $user_account['id'])->delete();
                echo false;
            }

            $followeruser = new FollowerUser();
            $followeruser->user_id_fk = $follower_user_id;
            $followeruser->follower_id_fk = $user_account['id'];
            $followeruser->status = '1';
            $followeruser->created_at = date("Y-m-d H:i:s");
            $followeruser->save();
            $check_exists = DB::table('mst_notifications')
                    ->where('notification_from', $user_account['id'])
                    ->where('notification_to', $follower_user_id)
                    ->where('notification_type', '1')
                    ->get();
            if (count($check_exists) > 0) {
                $delete = DB::table('mst_notifications')
                        ->where('notification_from', $user_account['id'])
                        ->where('notification_to', $follower_user_id)
                        ->where('notification_type', '1')
                        ->delete();
                $subject = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $message = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $follower_user_id,
                    'notification_type' => "1",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            } else {
                $subject = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $message = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $follower_user_id,
                    'notification_type' => "1",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            }
            echo true;
        }
    }

    protected function addUnFollower() {

        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        $request = Input::all();
        $follower_user_id = $request['follower_user_id'];
        if ($request['status'] == '0') {
            $status = '1';
        } else {
            $status = '0';
        }
        $arr_follower = FollowerUser::where('user_id_fk', '=', $follower_user_id)->where('follower_id_fk', '=', $user_account['id'])->get();

        if ($user_account['first_name'] != '' && $user_account['id'] != '') {

            DB::table('trans_follow_user')
                    ->where('user_id_fk', $follower_user_id)
                    ->where('follower_id_fk', $user_account['id'])
                    ->update(['status' => $status]);

            $check_exists = DB::table('mst_notifications')
                    ->where('notification_from', $user_account['id'])
                    ->where('notification_to', $follower_user_id)
                    ->where('notification_type', '1')
                    ->get();
            if (count($check_exists) > 0) {
                $delete = DB::table('mst_notifications')
                        ->where('notification_from', $user_account['id'])
                        ->where('notification_to', $follower_user_id)
                        ->where('notification_type', '1')
                        ->delete();
                $subject = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $message = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $follower_user_id,
                    'notification_type' => "1",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            } else {
                $subject = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $message = ($user_account['first_name']) . ' ' . ($user_account['last_name']) . ' followed you.';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $follower_user_id,
                    'notification_type' => "1",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            }
            echo true;
        }
    }

    protected function addConnection() {
        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        $request = Input::all();
        $to_user_id = $request['to_user_id'];
        $arr_from_connection = UserConnection::where('to_user_id', '=', $to_user_id)->where('from_user_id', '=', $user_account['id'])->get();
        if (count($arr_from_connection) > 0) {
            $conn_id = $arr_from_connection['0']['id'];
            UserConnection::find($conn_id)->delete();
            echo false;
        } else if (count($arr_to_connection = UserConnection::where('to_user_id', '=', $user_account['id'])->where('from_user_id', '=', $to_user_id)->get()) > 0) {
            $conn_id = $arr_to_connection['0']['id'];
            UserConnection::find($conn_id)->delete();
            echo false;
        } else {
            $userconnection = new UserConnection();
            $userconnection->to_user_id = $to_user_id;
            $userconnection->from_user_id = $user_account['id'];
            $userconnection->status = '0';
            $userconnection->save();

            $check_exists = DB::table('mst_notifications')
                    ->where('notification_from', $user_account['id'])
                    ->where('notification_to', $to_user_id)
                    ->where('notification_type', '2')
                    ->get();
            if (count($check_exists) > 0) {
                $delete = DB::table('mst_notifications')
                        ->where('notification_from', $user_account['id'])
                        ->where('notification_to', $to_user_id)
                        ->where('notification_type', '2')
                        ->delete();
                $subject = '  requests to connect with you.';
                $message = ucfirst($user_account['first_name']) . ' ' . ucfirst($user_account['last_name']) . '  requests to connect.';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $to_user_id,
                    'notification_type' => "2",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
                $this->send_mail($to_user_id);
            } else {
                $subject = '  requests to connect with you.';
                $message = ucfirst($user_account['first_name']) . ' ' . ucfirst($user_account['last_name']) . ' requests to connect. ';
                $insert_data = array(
                    'notification_from' => $user_account['id'],
                    'notification_to' => $to_user_id,
                    'notification_type' => "2",
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
                $this->send_mail($to_user_id);
            }

            echo true;
        }
    }

    protected function getLogout() {
        Auth::logout();
        Session::flush();
        Session::flash('success_msg', 'You have logged out successfully');
        return redirect()->guest('/');
    }

    protected function setLogoutTime() {
        $user_account = Session::get('user_account');
        $seconds = strtotime(date('Y-m-d H:i:s')) - strtotime(isset($user_account->last_logout_time) ? $user_account->last_logout_time : 0 );
        if ($seconds > 5) {
            User::where('id', $user_account['id'])
                ->update(['last_login' => date('Y-m-d H:i:s') ]);
        }
        User::where('id', $user_account['id'])
            ->update(['last_logout_time' => date('Y-m-d H:i:s') ]);
    }

    protected function getAdminLogout() {
        Auth::logout();
        Session::flash('admin_logout', 'You have logged out successfully');
        return redirect('/auth/admin/login');
    }

    public function forgotPassword() {
        $site_title = "Forgot Password";
        return view('Backend.login.forgot-password', ['site_title' => $site_title]);
    }

    public function forgotPasswordAction() {
        $all = Input::all();
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $data['global']['site_email'] = $site_email->value;
        $data['global']['site_title'] = $site_title->value;
        /* sending admin credentail on admin account mail on user email */
        $code = rand(9991, 999899);
        $activation_code = time();
        $arr_admin_detail = User::where('email', $all['user_email'])->first();
        $update_data = array('reset_password_code' => $activation_code);
        User::where('email', $all['user_email'])->update($update_data);
        $reset_password_link = '<a href="' . url('/') . '/admin/reset-password/' . base64_encode($activation_code) . '">Click here</a>';
        $lang_id = 14;
        $admin_login_link = '<a href="' . url('/') . '" target="_new">Admin Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        /* setting reserved_words for email content */
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'admin-forgot-password', 'lang_id' => '14'))->first();
        //$email_contents = end($email_contents);
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||USER_NAME||" => $arr_admin_detail['user_name'],
            "||ADMIN_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||ADMIN_EMAIL||" => $arr_admin_detail['email'],
            "||RESET_PASSWORD_LINK||" => $reset_password_link,
            "||ADMIN_LOGIN_LINK||" => $admin_login_link
        );
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);
        // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($arr_admin_detail, $contents_array, $data, $email_contents) {
            $message->from($data['global']['site_email'], $data['global']['site_title']);
            $message->to($arr_admin_detail['email'], $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'])->subject($email_contents['email_template_subject']);
        }); // End Mail
        Session::flash('message', 'Reset details has been sent successfully to your email');
        return redirect('admin/forgot-password');
    }

    public function checkForgotPasswordEmail() {

        $all = Input::all();
        /* checking email is exist or not for forgot password email entry */
        $arr_admin_detail = User::where('email', $all['user_email'])->first();
        if (count($arr_admin_detail) == 0) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function resetPassword($code = '') {

        /* getting admin details if exist from email */
        if ($code != '') {
            $arr_admin_detail = User::where('reset_password_code', (int) base64_decode($code))->first();
            if (count($arr_admin_detail) <= 0) {
                Session::flash('message', 'The entered link is not valid.');
                return redirect('admin/forgot-password');
            }
        }
        $site_title = "Reset Password";
        return view('Backend.login.reset-password', ['site_title' => $site_title, 'code' => $code]);
    }

    public function resetPasswordAction() {
        $all = Input::all();
        $code = (int) base64_decode($all['code']);
        /* getting admin details if exist from email */
        $arr_admin_detail = User::where('reset_password_code', $code)->first();
        if (count($arr_admin_detail) > 0) {
            if ($all['user_password'] != '') {
                /* sending admin credentail on admin account mail on user email */
                $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
                $password = crypt($all['user_password'], '$2y$12$' . $salt);
                if ($code != '' && $code != 0) {
                    $update_data = array('password' => $password);
                    User::where('reset_password_code', $code)->update($update_data);
                    //updating reset code
                    $update_data = array('reset_password_code' => '0');
                    User::where('reset_password_code', $code)->update($update_data);
                    Session::flash('admin_logout', 'Password has been reset successfully.');
                }
                return redirect('auth/admin/login');
            }
        } else {
            Session::flash('message', 'The entered link is not valid.');
            return redirect('admin/forgot-password');
        }
    }

    public function activateUserAccount($activation_code) {

        /* get user details to verify the email address */
        $arr_login_data = User::where('activation_code', $activation_code)->first();

        if (count($arr_login_data)) {
            /* if email already verified */
            if ($arr_login_data['email_verified'] == 1) {
                
                $user = User::find($arr_login_data->id);
                Session::put('user_account', $user);
                return redirect()->to('user-dashboard')->with('success_msg', "Your account is active.");
            } else {
                $Question_objects = QuestionModel::where('user_id_fk', $arr_login_data['id'])->get();

                if(count($Question_objects)>0){
                    foreach ($Question_objects as $key=>$value){
                        $Question_object = QuestionModel::where('id', $value['id'])->first();
                        $Question_object->status = '1';
                        $Question_object->save();
                    }
                }

                /* if email not verified. */
                $update_data = array('email_verified' => '1', 'user_status' => '1');
                User::where('activation_code', $activation_code)->update($update_data);
                $user = User::find($arr_login_data->id);
                Session::put('user_account', $user);
                /* Send thank you email to registerd user */
                $site_email = trans_global_settings::where('id', '1')->first();
                $site_title = trans_global_settings::where('id', '2')->first();

                $macros_array_detail = array();
                $macros_array_detail = email_template_macros::all();

                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }
                $email_contents = email_templates::where(array('email_template_title' => 'thank-you-user', 'lang_id' => '14'))->first();


                $email_contents = $email_contents;
                $content = $email_contents['email_template_content'];

                $name = "";
                $reserved_words = array();
                $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

                $reserved_arr = array(
                    "||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => $site_path,
                    "||USER_NAME||" => $arr_login_data->first_name,
                );

                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);

                foreach ($reserved_words as $k => $v) {
                    $content = str_replace($k, $v, $content);
                }

                /* getting mail subect and mail message using email template title and lang_id and reserved works */
                $contents_array = array("email_conents" => $content);
                $data['site_email'] = $site_email->value;
                $data['site_title'] = $site_title->value;

                // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
                Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $email_contents,$arr_login_data) {
                    $message->from($data['site_email'], $data['site_title']);
                    $message->to($arr_login_data->email)->subject($email_contents['email_template_subject']);
                });
                return redirect()->to('user-dashboard')->with('success_msg', "Congratulations! Your account has been activated successfully.");
            }
        } else {
            /* if any error invalid activation link found account */
            return redirect()->to('user-dashboard')->with('error_msg', "Invalid activation code. Request activation code again.");
            Session::flash('error_msg', 'Invalid activation code.');
        }
        return redirect(url('/') . '/');
    }

    public function checkUsername() {
        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user name already exist or not for edit Admin */
            if (strtolower($all['user_name']) == strtolower($all['old_username'])) {
                echo "true";
            } else {
                $arr_admin_detail = AdminUser::where('user_name', $all['user_name'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user name already exist or not for add admin  */
            $arr_admin_detail = AdminUser::where('user_name', $all['user_name'])->first();
            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function checkUsernameExist() {
        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user name already exist or not for edit Admin */
            if (strtolower($all['user_name']) == strtolower($all['old_username'])) {
                echo "true";
            } else {
                $arr_admin_detail = User::where('user_name', $all['user_name'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user name already exist or not for add admin  */
            $arr_admin_detail = User::where('user_name', $all['user_name'])->first();
            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function checkEmail() {
        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user email already exist or not for edit Admin */
            if (strtolower($all['user_email']) == strtolower($all['old_email'])) {
                echo "true";
            } else {
                $arr_admin_detail = AdminUser::where('email', $all['user_email'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user email already exist or not for add admin  */
            $arr_admin_detail = AdminUser::where('email', $all['user_email'])->first();

            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function checkUseremailExist() {
        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user email already exist or not for edit Admin */
            if (strtolower($all['user_email']) == strtolower($all['old_email'])) {
                echo "true";
            } else {
                $arr_admin_detail = User::where('email', $all['user_email'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user email already exist or not for add admin  */
            $arr_admin_detail = User::where('email', $all['user_email'])->first();

            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function activateAccount($activation_code) {

        /* get user details to verify the email address */
        $arr_login_data = User::where('activation_code', $activation_code)->first();
        if (count($arr_login_data)) {
            /* if email already verified */
            if ($arr_login_data['email_verified'] == 1) {
                Session::flash('warning_msg', 'You have already activated your account. Please login.');
            } else {
                /* if email not verified. */
                $update_data = array('email_verified' => '1', 'user_status' => '1');
                User::where('activation_code', $activation_code)->update($update_data);
                Session::flash('success_msg', 'Congratulation! Your account has been activated successfully. Please login.');
            }
        } else {
            /* if any error invalid activation link found account */
            Session::flash('error_msg', 'Invalid activation code.');
        }
        return redirect(url('/') . '/auth/admin/login');
    }

    public function linkedInConnect() {
        // Change these
        define('API_KEY', '754jrj4q3upgk4'); // live
        define('API_SECRET', 'OhcyYV5dtRB204Kd'); // live
        define('REDIRECT_URI', url('/') . '/user/linkedin-connect');
        define('SCOPE', 'r_basicprofile r_emailaddress');
        // You'll probably use a database
        session_name('linkedin');
        session_start();
        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            $linkedin_error = $_GET['error'] . ': ' . $_GET['error_description'];
            $msg = array('msg_type' => 'warning', 'msg_slogan' => 'Info', 'msg_detail' => $linkedin_error);
            $value = Session::get('key', 'default');
            $this->session->set_userdata('flash_message', $msg);
            Session::flash('msg_warning', $msg);
            return redirect(url('/') . '/user/register');
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if ($_SESSION['state'] == $_GET['state']) {
                // Get token so you can make API calls
                $this->getAccessToken();
            } else {
                // CSRF attack? Or did you mix up your states?
                exit;
            }
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() < $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $_SESSION = array();
            }
            if (empty($_SESSION['access_token'])) {
                // Start authorization process
                $this->getAuthorizationCode();
            }
        }
        $profile_fileds = array(
            'id',
            'firstName',
            'lastName',
            'date-of-birth',
            'email-address'
        );
// Congratulations! You have a valid token. Now fetch your profile
        $user = $this->fetch('GET', '/v1/people/~:(' . implode(',', $profile_fileds) . ')');
        if (!(empty($user))) {
            //check in the database user alrdy registerd or not
            $data = AdminUser::where('email', '=', $user->emailAddress)->get();
            if (count($data) > 0) {
                //if registered redirect to signup page  with error message
                Session::flash('warning_msg', 'You are already registred with this account.please login with your syncampus id.');
                return redirect(url('/') . '/user/register');
            } else {
//                Session::put('linkedin_data', $user);
                Session::flash('linkedin_data', $user);
                Session::flash('success_msg', 'Please complete your profile.');
                return redirect(url('/') . '/user/register');
            }
            //redirect(base_url('/') . $this->session->userdata('current_path'));
        }
    }

    function getAuthorizationCode() {
        $params = array(
            'response_type' => 'code',
            'client_id' => API_KEY,
            'scope' => SCOPE,
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => REDIRECT_URI,
        );
        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);
        // Needed to identify request when it returns to us
        $_SESSION['state'] = $params['state'];
        // Redirect user to authenticate
        header("Location: $url");
        exit;
    }

    function getAccessToken() {
        $params = array(
            'grant_type' => 'authorization_code',
            'client_id' => API_KEY,
            'client_secret' => API_SECRET,
            'code' => $_GET['code'],
            'redirect_uri' => REDIRECT_URI,
        );
        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);
        // Tell streams to make a POST request
        $context = stream_context_create(
                array('http' =>
                    array('method' => 'POST',
                    )
                )
        );
        // Retrieve access token information
        $response = file_get_contents($url, false, $context);
        // Native PHP object, please
        $token = json_decode($response);
        // Store access token and expiration time
        $_SESSION['access_token'] = $token->access_token; // guard this!
        $_SESSION['expires_in'] = $token->expires_in; // relative time (in seconds)
        $_SESSION['expires_at'] = time() + $_SESSION['expires_in']; // absolute time
        return true;
    }

    function fetch($method, $resource, $body = '') {
        $opts = array(
            'http' => array(
                'method' => $method,
                'header' => "Authorization: Bearer " . $_SESSION['access_token'] . "\r\n" . "x-li-format: json\r\n"
            )
        );
        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource;
        $context = stream_context_create($opts);
        // Hocus Pocus
        $response = file_get_contents($url, false, $context);
        // Native PHP object, please
        return json_decode($response);
    }

    function getFbContents($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($this->curl, CURLOPT_URL, $fileName);
        $response = curl_exec($curl);
        curl_close($curl);
//            if (!curl_errno($this->curl)) {
//                $parts = explode('.', $fileName);
//                $ext = array_pop($parts);
//                $return = sfConfig::get('sf_upload_dir') . '/tmp/' . uniqid('fbphoto') . '.' . $ext;
//                file_put_contents($return, $response);
//            }
        return $response;
    }

    public function getParentGroupInner() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $user_account = Session::get('user_account');
        $request = Input::all();
        $parent_id = $request['parent_id'];
        if (isset($request['user_already_groups']))
            $user_already_groups = $request['user_already_groups'];
        $user_groups = array();
        if (isset($user_already_groups))
            $user_groups = explode(',', $user_already_groups);
        $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->get();

        $str = '';
        foreach ($arr_parent_group as $key => $parent) {
            $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
            $str .= '<li onclick = "openNewTab1('. "'" . $parent['group_name'] . "'" .  ');">';
            $str .= '<div style="position:relative" class="dropdown fixter">';
//            if (!in_array($parent['id'], $user_groups)) {
//            $str .= '<a data-toggle="dropdown" title="Double click to open,Single Click to Expand"  class="dropdown-toggle"  onclick="dropDown(' . $parent['id'] . ')" ondblClick="openNewTab1(' . $parent['id'] . ')"  href="javascript:void(0)" ><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'] . '"  onerror="src=' . "'" . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' .' '. $parent['group_name'] . '</a>';
           

		   // chnge    $str .= '<a data-toggle="dropdown" title="Double click to open,Single Click to Expand" ' . (count($arr_child_group) >= 1 ? 'class="dropdown-toggle trigger right-caret child' . $parent['id'] . '"' : 'class= dropdown-toggle child' . $parent['id'] . '') . '   onclick="openNewTab1(' . "'" . $parent['group_name'] . "'" . ');"  href="javascript:void(0)" ><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'] . '"  onerror="src=' . "'" . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' . ' ' . $parent['group_name'] . '</a>';

		   
		   
 $str .= '<a class = "dropdown-toggle child" onclick = "openNewTab1(' . "'" . $parent['group_name'] . "'" . ');" href="javascript:void(0)"> 
 <img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'] . '" 
 onerror="src=' . "'" . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' . ' ' . $parent['group_name'] . '</a>' .(count($arr_child_group) >= 1 ? 
 '<span data-toggle = "dropdown"  class="dropdown-toggle trigger right-caret child' . $parent['id'] . '" > </span>' : '<span class= "dropdown-toggle child' . $parent['id'] . '"> </span>') . ' ';
		   
		   
//                $str .= '<a data-toggle="dropdown" class="dropdown-toggle" onclick="dropDown(' . $parent['id'] . ');" href="get-group-event/' .base64_encode($parent['id']). '" ><i class="icon-1"></i>' . $parent['group_name'] . '</a>';
//            } else {
//                $str .= '<a data-toggle="dropdown"  class="dropdown-toggle" onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ><i class="icon-1"></i>' . $parent['group_name'] . '</a>';
//            }
            $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';
            $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();

            if (count($arr_child_group) > 0) {
                foreach ($arr_child_group as $child_group) {
                    if ($child_group["id"] != '' && $child_group["group_name"] != '') {
                        $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                        $count = 0;
                        foreach ($arr_sub_child_group as $sub_child_group) {
                            if (in_array($sub_child_group["id"], $user_groups)) {
                                $count++;
                            }
                        }
                        if ($count > 0 && $count == count($arr_sub_child_group)) {

                            $str .= '<li> ';
                            $str .= '<a href="javascript:void(0)" title="Click to open" ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');" ><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' . ' ' . $child_group['group_name'] . '</a>';
//                            $str .= '<input type="checkbox" class="chk_child" checked="checked" name="chk_grp_' . $child_group['id'] . '" value="' . $child_group['id'] . '"><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');"><i class="icon-1"></i>' . $child_group['group_name'] . '</a>';
                        } else {
                            $str .= '<li>';

//                            $str .= '<input type="checkbox" class="chk_child" name="chk_grp_' . $child_group['id'] . '" value="'.$child_group['id'].'"><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');"><i class="icon-1"></i>' . $child_group['group_name'] . '</a>';
                            $str .= '<a href="javascript:void(0)" title="Click to open" ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' . ' ' . $child_group['group_name'] . '</a>';
                        }

                        if (count($arr_sub_child_group) > 0) {
                            $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';


                            foreach ($arr_sub_child_group as $sub_child_group) {
//                                if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '' && !in_array($sub_child_group["id"], $user_groups)) {
                                if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                    $sub_count = 0;
                                    if (!in_array($sub_child_group["id"], $user_groups)) {
                                        $sub_count++;
//                                        $str .= '<li><a href="get-group-event/' . base64_encode($sub_child_group['id']) . '" target="_blank"><i class="icon-1"></i>' . $sub_child_group['group_name'] . '.</a></li>';
                                        $str .= '<li><a href="go/' . $sub_child_group['group_name'] . '" target="_blank"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/group_image.png' . "'" . '" />' . ' ' . $sub_child_group['group_name'] . '.</a></li>';
//                                        $str .= '<li><a href="javascript:void(0)"  onClick ="return addGroup(' . $sub_child_group['id'] . ');"><i class="icon-1"></i>' . $sub_child_group['group_name'] . '.</a></li>';
                                    } else {
// in the user group                     $str .= '<li><a href="javascript:void(0)"  onClick ="return showSubChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $sub_child_group['id'] . ');"><input type="checkbox" name="chk_grp'. $sub_child_group['id'].'" checked="checked" />   ' .   $sub_child_group['group_name'] . '</a></li>';
//                                        $str .= '<li><a href="javascript:void(0)"  onClick ="return addGroup(' . $sub_child_group['id'] . ');"><i class="icon-1"></i>' . $sub_child_group['group_name'] . '.</a></li>';
                                    }
                                }
                            }

                            $str .= '</ul>';
                        }

                        $str .= '</li>';
                    }
                }
            } else {
                $str .= '';
            }
            $str .= '</ul>';
            $str .= '</div>';
            $str .= '</li>';
        }
        echo $str;
    }

    public function getParentGroup() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $user_account = Session::get('user_account');
        $request = Input::all();

        $parent_id = $request['parent_id'];
//        if (isset($request['user_already_groups']))
//            $user_already_groups = $request['user_already_groups'];
//        $user_groups = array();
//        if (isset($user_already_groups)){
//            $user_groups = explode(',', $user_already_groups);
//        }
        $user_groups = array();
        if (isset($request['user_already_groups'])) {
            $user_groups = $request['user_already_groups'];
        }

        $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->get();
        //get the sub jointed data

        $str = '';
        foreach ($arr_parent_group as $key => $parent) {
            $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
            $image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
            if (file_exists($image_path) && $parent['icon_img'] != '') {
                $post_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
            } else {
                $post_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
            }
            $str .= '<li>';
            $str .= '<div style="position:relative" class="dropdown fixter">';
            if (!in_array($parent['id'], $user_groups)) {
                $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]"  /><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '" />' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
            } else {
                $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]" checked="checked" /><label for="' . $parent['id'] . '"><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '"/>' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
            }
            $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';



            if (count($arr_child_group) > 0) {
                foreach ($arr_child_group as $child_group) {
                    if ($child_group["id"] != '' && $child_group["group_name"] != '') {

                        $c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                        if (file_exists($c_image_path) && $child_group['icon_img'] != '') {
                            $child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                        } else {
                            $child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                        }

                        $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                        $count = 0;
                        foreach ($arr_child_group as $sub_child_group) {
                            if (in_array($sub_child_group["id"], $user_groups)) {
                                $count++;
                            }
                        }
                        if ($count > 0 && $count == count($arr_child_group)) {

                            $str .= '<li> ';
                            $str .= '<input type="checkbox" class="chk_child" checked="checked" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                        } else {
                            $str .= '<li>';

                            $str .= '<input type="checkbox" class="chk_child" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                        }

                        if (count($arr_sub_child_group) > 0) {
                            $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';
                            foreach ($arr_sub_child_group as $sub_child_group) {

                                $s_c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                if (file_exists($s_c_image_path) && $sub_child_group['icon_img'] != '') {
                                    $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                } else {
                                    $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                                }

                                if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                    $sub_count = 0;
                                    if (!in_array($sub_child_group["id"], $user_groups)) {
                                        $sub_count++;

                                        $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '" name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" /><a href="javascript:void(0)" title="Double click to open"><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '.</a></li>';
                                    } else {
                                        $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '"  name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" checked="checked" /><a href="javascript:void(0)"><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '</a></li>';
                                    }
                                }
                            }

                            $str .= '</ul>';
                        }

                        $str .= '</li>';
                    }
                }
            } else {
                $str .= '';
            }
            $str .= '</ul>';
            $str .= '</div>';
            $str .= '</li>';
        }
        echo $str;
    }

    public function getChildGroup() {
        $request = Input::all();
        $parent_id = $request['parent_id'];
        $arr_child_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->get();
        $str = '';
        $str .= '<ul class="dropdown-menu">';
        foreach ($arr_child_group as $child) {
            $str .= ' <li>';
            $str .= '<a href="javascript:void(0)" onclick="return getChildGroup(' . $child['id'] . ')"  ><i class="icon-1"></i>' . $child['group_name'] . '<input type="checkbox" name="chk_grp" /></a>';
            $str .= '</li>';
        }
        $str .= '</ul>';
        echo $str;
    }

    public function getGroupName() {
        $request = Input::all();
        $search_group = $request['search_group'];
        $arr_child_group = DB::table('mst_group')
                ->where('group_name', 'like', '%' . $search_group . '%')
                ->get();
        return $arr_child_group;
    }

    public function getGroup() {
        $request = Input::all();
        $parent_id = $request['parent_id'];
        $arr_parent = array();
        $arr_group_main = Group::Where('id', '=', $parent_id)->get();
        $arr_group = Group::Where('parent_id', '=', $parent_id)->get();



        $arr_sub_group = array();
        $arr_group1 = array();

        foreach ($arr_group as $key => $value) {
            $arr_parent[] = $value->id;
        }
        if ($arr_parent != '') {
            $arr_parent = array_unique($arr_parent);
        }

        $sub_group = Group::whereIn('parent_id', $arr_parent)->get();

        if (count($sub_group) > 0 && count($arr_group) > 0) {

            $arr_group = $arr_group->merge($sub_group);
        } elseif (count($sub_group) > 0) {
            $arr_group = $sub_group;
        }
        $key = 1;
        foreach ($arr_group as $val) {
            $final[$key] = $val;
            $key++;
        }
        $final[0] = $arr_group_main[0];
        return $final;
    }

    protected function postRegisterFB() {

        $request = Input::all();
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);
        $activation_code = time();
        $user = new User();

        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['user_email'];
        $user->password = $hash_password;
        $user->role_id = '0';
        $user->user_status = '1';
        $user->user_type = '1';
        $user->email_verified = '1';
        $user->ip_address = $_SERVER['REMOTE_ADDR'];
        $user->activation_code = $activation_code;
        $user->created_at = date("Y-m-d H:i:s");
        $user->save();
        Session::put('user_account', $user);

        return redirect(url('/') . '/signup2');
    }

    public function getParentGroup1() {
        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        $request = Input::all();

        $parent_id = $request['parent_id'];

        if (isset($request['user_already_groups']))
            $user_already_groups = $request['user_already_groups'];
        $user_groups = array();
        if (isset($user_already_groups))
            $user_groups = explode(',', $user_already_groups);
        $arr = Group::where('id', '=', $parent_id)->get();



        if ($arr[0]['parent_id'] != 0) {
            /* without main parent */
            $arr = Group::where('id', '=', $parent_id)->get();
            $parent_id = $arr[0]['parent_id'];
            $id = $arr[0]['id'];
            $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->where('id', $id)->get();

            $str = '';
            foreach ($arr_parent_group as $key => $parent) {
                $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
                $str .= '<li>';
                $str .= '<div style="position:relative" class="dropdown fixter">';
                if (!in_array($parent['id'], $user_groups)) {
                    $str .= '<a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $parent['group_name'] . "'" . ');"><i class="icon-1"></i>' . '' . $parent['group_name'] . '</a>';
                } else {
                    $str .= '<a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $parent['group_name'] . "'" . ');" ><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $parent['group_name'] . '</a>';
                }
//            $str .= '<a data-toggle="dropdown" class="dropdown-toggle" onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ><i class="icon-1"></i>' . $parent['group_name'] . '<input type="checkbox" name="chk_grp_' . $parent['id'] . '" /></a></a>';
                $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';

                if (count($arr_child_group) > 0) {
                    foreach ($arr_child_group as $child_group) {
                        if ($child_group["id"] != '' && $child_group["group_name"] != '') {
                            $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                            $count = 0;
                            foreach ($arr_sub_child_group as $sub_child_group) {
                                if (in_array($sub_child_group["id"], $user_groups)) {
                                    $count++;
                                }
                            }
                            if ($count > 0 && $count == count($arr_sub_child_group)) {

                                $str .= '<li> ';
                                $str .= '<a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $child_group['group_name'] . '</a>';
                            } else {
                                $str .= '<li>';

//                            $str .= '<input type="checkbox" class="chk_child" name="chk_grp_' . $child_group['id'] . '" value="'.$child_group['id'].'"><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');"><i class="icon-1"></i>' . $child_group['group_name'] . '</a>';
                                $str .= '<a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $child_group['group_name'] . '</a>';
                            }

                            if (count($arr_sub_child_group) > 0) {
                                $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';


                                foreach ($arr_sub_child_group as $sub_child_group) {
//                                if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '' && !in_array($sub_child_group["id"], $user_groups)) {
                                    if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                        $sub_count = 0;
                                        if (!in_array($sub_child_group["id"], $user_groups)) {
                                            $sub_count++;

                                            $str .= '<li><a href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $sub_child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $sub_child_group['group_name'] . '.</a></li>';
                                        } else {
//                                        $str .= '<li><a href="javascript:void(0)"  onClick ="return showSubChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $sub_child_group['id'] . ');"><input type="checkbox" name="chk_grp'. $sub_child_group['id'].'" checked="checked" />   ' .   $sub_child_group['group_name'] . '</a></li>';
                                            $str .= '<li><a href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $sub_child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $sub_child_group['group_name'] . '.</a></li>';
                                        }
                                    }
                                }

                                $str .= '</ul>';
                            }

                            $str .= '</li>';
                        }
                    }
                } else {
                    $str .= '';
                }
                $str .= '</ul>';
                $str .= '</div>';
                $str .= '</li>';
            }
        } else {


            $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->get();

            /* start add new code */
            $str = '';
            foreach ($arr_parent_group as $key => $parent) {
                $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
                $str .= '<li>';
                $str .= '<div style="position:relative" class="dropdown fixter">';
                if (!in_array($parent['id'], $user_groups)) {
                    $str .= '<a data-toggle="dropdown" ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $parent['group_name'] . "'" . ');"><i class="icon-1"></i>' . '' . $parent['group_name'] . '</a>';
                } else {
                    $str .= '<a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $parent['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $parent['group_name'] . '</a>';
                }
//            $str .= '<a data-toggle="dropdown" class="dropdown-toggle" onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ><i class="icon-1"></i>' . $parent['group_name'] . '<input type="checkbox" name="chk_grp_' . $parent['id'] . '" /></a></a>';
                $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';


                if (count($arr_child_group) > 0) {
                    foreach ($arr_child_group as $child_group) {
                        if ($child_group["id"] != '' && $child_group["group_name"] != '') {
                            $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                            $count = 0;
                            foreach ($arr_sub_child_group as $sub_child_group) {
                                if (in_array($sub_child_group["id"], $user_groups)) {
                                    $count++;
                                }
                            }
                            if ($count > 0 && $count == count($arr_sub_child_group)) {

                                $str .= '<li> ';
                                $str .= '<a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $child_group['group_name'] . '</a>';
                            } else {
                                $str .= '<li>';

//                            $str .= '<input type="checkbox" class="chk_child" name="chk_grp_' . $child_group['id'] . '" value="'.$child_group['id'].'"><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');"><i class="icon-1"></i>' . $child_group['group_name'] . '</a>';
                                $str .= '<a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $child_group['id'] . ');" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $child_group['group_name'] . '</a>';
                            }

                            if (count($arr_sub_child_group) > 0) {
                                $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';


                                foreach ($arr_sub_child_group as $sub_child_group) {
//                                if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '' && !in_array($sub_child_group["id"], $user_groups)) {
                                    if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                        $sub_count = 0;
                                        if (!in_array($sub_child_group["id"], $user_groups)) {
                                            $sub_count++;

                                            $str .= '<li><a href="javascript:void(0)" title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $sub_child_group['group_name'] . "'" . ');"><i class="icon-1"></i>' . $sub_child_group['group_name'] . '.</a></li>';
                                        } else {
//                                        $str .= '<li><a href="javascript:void(0)"  onClick ="return showSubChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . '),addGroup(' . $sub_child_group['id'] . ');"><input type="checkbox" name="chk_grp'. $sub_child_group['id'].'" checked="checked" />   ' .   $sub_child_group['group_name'] . '</a></li>';
                                            $str .= '<li><a href="javascript:void(0)"  title="Double click to open,Single Click to Expand" ondblClick="openNewTab1(' . "'" . $sub_child_group['group_name'] . "'" . ');"><img  src="' . url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'] . '"  onerror="src=' . "'" . '' . url('/') . '/public/assets/Backend/img/image_not_found_thumb.jpg' . "'" . '" />' . '' . $sub_child_group['group_name'] . '.</a></li>';
                                        }
                                    }
                                }

                                $str .= '</ul>';
                            }

                            $str .= '</li>';
                        }
                    }
                } else {
                    $str .= '';
                }
                $str .= '</ul>';
                $str .= '</div>';
                $str .= '</li>';
            }
        }
        echo $str;
    }

    public function getParentGroupDetail() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $user_account = Session::get('user_account');
        $request = Input::all();

        $parent_id = $request['parent_id'];
        if (isset($request['user_already_groups']))
            $user_already_groups = $request['user_already_groups'];
        $user_groups = array();
        if (isset($user_already_groups))
            $user_groups = explode(',', $user_already_groups);
        $arr = Group::where('id', '=', $parent_id)->get();

        if ($arr[0]['parent_id'] != 0) {
            /* without main parent */
            $arr = Group::where('id', '=', $parent_id)->get();
            $parent_id = $arr[0]['parent_id'];
            $id = $arr[0]['id'];
            $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->where('id', $id)->get();

            $str = '';
            foreach ($arr_parent_group as $key => $parent) {
                $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
                $image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
                if (file_exists($image_path) && $parent['icon_img'] != '') {
                    $post_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
                } else {
                    $post_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                }
                $str .= '<li>';
                $str .= '<div style="position:relative" class="dropdown fixter">';
                if (!in_array($parent['id'], $user_groups)) {
                    $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]" checked="checked" /><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '" />' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
                } else {
                    $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]" checked="checked" /><label for="' . $parent['id'] . '"><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '"/>' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
                }
                $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';


                if (count($arr_child_group) > 0) {
                    foreach ($arr_child_group as $child_group) {
                        if ($child_group["id"] != '' && $child_group["group_name"] != '') {

                            $c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                            if (file_exists($c_image_path) && $child_group['icon_img'] != '') {
                                $child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                            } else {
                                $child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                            }

                            $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                            $count = 0;
                            foreach ($arr_sub_child_group as $sub_child_group) {
                                if (in_array($sub_child_group["id"], $user_groups)) {
                                    $count++;
                                }
                            }
                            if ($count > 0 && $count == count($arr_sub_child_group)) {

                                $str .= '<li> ';
                                $str .= '<input type="checkbox" class="chk_child" checked="checked" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                            } else {
                                $str .= '<li>';

                                $str .= '<input type="checkbox" class="chk_child" checked="checked" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                            }

                            if (count($arr_sub_child_group) > 0) {
                                $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';
                                foreach ($arr_sub_child_group as $sub_child_group) {

                                    $s_c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                    if (file_exists($s_c_image_path) && $sub_child_group['icon_img'] != '') {
                                        $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                    } else {
                                        $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                                    }

                                    if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                        $sub_count = 0;
                                        if (!in_array($sub_child_group["id"], $user_groups)) {
                                            $sub_count++;

                                            $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '" name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" checked="checked"/><a href="javascript:void(0)" ><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '.</a></li>';
                                        } else {
                                            $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '"  name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" checked="checked" /><a href="javascript:void(0)"><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '</a></li>';
                                        }
                                    }
                                }

                                $str .= '</ul>';
                            }

                            $str .= '</li>';
                        }
                    }
                } else {
                    $str .= '';
                }
                $str .= '</ul>';
                $str .= '</div>';
                $str .= '</li>';
            }
        } else {

            $arr_parent_group = Group::where('parent_id', '=', $parent_id)->where('parent_id', '!=', 0)->get();
            //get the sub jointed data

            $str = '';
            foreach ($arr_parent_group as $key => $parent) {
                $arr_child_group = Group::where('parent_id', '=', $parent['id'])->where('parent_id', '!=', 0)->get();
                $image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
                if (file_exists($image_path) && $parent['icon_img'] != '') {
                    $post_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $parent['icon_img'];
                } else {
                    $post_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                }
                $str .= '<li>';
                $str .= '<div style="position:relative" class="dropdown fixter">';
                if (!in_array($parent['id'], $user_groups)) {
                    $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]" checked="checked" /><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '" />' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
                } else {
                    $str .= '<input type="checkbox" value="' . $parent['id'] . '" id="' . $parent['id'] . '" class="chk_parent" name="chk_child_grp[]" checked="checked" /><label for="' . $parent['id'] . '"><label for="' . $parent['id'] . '"><img  src="' . $post_image_path . '"/>' . $parent['group_name'] . '</label><a data-toggle="dropdown"  ' . (count($arr_child_group) > 0 ? 'class="dropdown-toggle right-caret"' : 'class="dropdown-toggle"') . ' onclick="dropDown(' . $parent['id'] . ');" href="javascript:void(0)" ></a>';
                }
                $str .= '<ul class="dropdown-menu parent' . $parent['id'] . '">';


                if (count($arr_child_group) > 0) {
                    foreach ($arr_child_group as $child_group) {
                        if ($child_group["id"] != '' && $child_group["group_name"] != '') {

                            $c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                            if (file_exists($c_image_path) && $child_group['icon_img'] != '') {
                                $child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $child_group['icon_img'];
                            } else {
                                $child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                            }

                            $arr_sub_child_group = Group::where('parent_id', '=', $child_group['id'])->where('parent_id', '!=', 0)->get();
                            $count = 0;
                            foreach ($arr_sub_child_group as $sub_child_group) {
                                if (in_array($sub_child_group["id"], $user_groups)) {
                                    $count++;
                                }
                            }
                            if ($count > 0 && $count == count($arr_sub_child_group)) {

                                $str .= '<li> ';
                                $str .= '<input type="checkbox" class="chk_child" checked="checked" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                            } else {
                                $str .= '<li>';

                                $str .= '<input type="checkbox" class="chk_child" checked="checked" id="' . $child_group['id'] . '" name="chk_child_grp[]" value="' . $child_group['id'] . '"><label for="' . $child_group['id'] . '"><img  src="' . $child_image_path . '"/>' . $child_group['group_name'] . '</label><a   href="javascript:void(0)"  ' . (count($arr_sub_child_group) > 0 ? 'class="dropdown-toggle trigger right-caret child' . $child_group['id'] . '"' : 'class=child' . $child_group['id'] . '') . ' onClick ="return showChild(' . $child_group['id'] . ',' . $child_group['parent_id'] . ')"></a>';
                            }

                            if (count($arr_sub_child_group) > 0) {
                                $str .= '<ul class="dropdown-menu sub-menu subchild' . $child_group['id'] . ' subparent' . $child_group['parent_id'] . '">';
                                foreach ($arr_sub_child_group as $sub_child_group) {

                                    $s_c_image_path = $data['absolute_path'] . 'public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                    if (file_exists($s_c_image_path) && $sub_child_group['icon_img'] != '') {
                                        $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_img/icon/thumb/' . $sub_child_group['icon_img'];
                                    } else {
                                        $sub_child_image_path = url('/') . '/public/assets/Backend/img/group_image.png';
                                    }

                                    if ($sub_child_group["id"] != '' && $sub_child_group["group_name"] != '') {
                                        $sub_count = 0;
                                        if (!in_array($sub_child_group["id"], $user_groups)) {
                                            $sub_count++;

                                            $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '" name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" checked="checked"/><a href="javascript:void(0)" ><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '.</a></li>';
                                        } else {
                                            $str .= '<li><input type="checkbox" class="chk_sub_child" id="' . $sub_child_group['id'] . '"  name="chk_end_grp[]" value="' . $sub_child_group['id'] . '" checked="checked" /><a href="javascript:void(0)" ><img  src="' . $sub_child_image_path . '"/>' . $sub_child_group['group_name'] . '</a></li>';
                                        }
                                    }
                                }

                                $str .= '</ul>';
                            }

                            $str .= '</li>';
                        }
                    }
                } else {
                    $str .= '';
                }
                $str .= '</ul>';
                $str .= '</div>';
                $str .= '</li>';
            }
        }
        echo $str;
    }

    public function send_mail($second_user_id) {

        /* ***********Send Email Notification************ */        
        $user = User::find($second_user_id);
        $user_account = Session::get('user_account');

        if ($user->getUserEmailSchedule == '' || $user->getUserEmailSchedule->connection_notification == 1)
        {
            $site_email = trans_global_settings::where('id', '1')->first();
            $site_title = trans_global_settings::where('id', '2')->first();

            /* setting reserved_words for email content */
            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();

            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $email_contents = email_templates::where(array('email_template_title' => 'connection-notification', 'lang_id' => '14'))->first();

            $email_contents = $email_contents;
            $content = $email_contents['email_template_content'];

            $reserved_words = array();
            $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

            $user_acc= DB::table('mst_users')->where('id', $user_account['id'])->get()->first();

            $profile_path=config('profile_pic_url').$user_acc->id.'/thumb/'.$user_acc->profile_picture;
            if(isset($user_acc->profile_picture) && $user_acc->profile_picture!=''){
                if ($user_acc->gender == 0)
                    $profile_path_onerror=config('avatar_male');
                else
                    $profile_path_onerror=config('avatar_female');

                $profile_image_path=$profile_path.'" onerror="'.$profile_path_onerror;
            }
            else {
                if ($user_acc->gender == 0) {
                    $profile_path=config('avatar_male');
                    $profile_image_path=$profile_path;
                }
                else {
                    $profile_path=config('avatar_female');
                    $profile_image_path=$profile_path;
                }
            }

            $reserved_arr = array(
                "||SITE_TITLE||" => stripslashes($site_title->value),
                "||SITE_PATH||" => $site_path,
                "||USER_NAME||" => $user['first_name'],
                "||USER_EMAIL||" => $user['email'],
                "||SECOND_USER_NAME||" => $user_account['first_name']." ".$user_account['last_name'],
                "||SECOND_USER_PROFILE_PATH||" => url('/').'/user-profile/' . (base64_encode($user_account['id'])),
                "||SECOND_USER_PROFILE_IMAGE||" => $profile_image_path,
                "||NOTIFICATION_LINK||" => url('/').'/notification/',
            );

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);

            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }

            /* getting mail subect and mail message using email template title and lang_id and reserved works */
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;


            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $user, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($user['email'], $user['first_name'])->subject($email_contents['email_template_subject']);
            });
        }
    }

}
