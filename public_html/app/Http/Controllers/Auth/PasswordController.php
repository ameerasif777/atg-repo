<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Input;
use App\User;
use App\email_templates;
use App\email_template_macros;
use App\trans_global_settings;
use Session;
use Auth;
use Mail;
use Request;

class PasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */

use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }
    

    public function passwordRecovery() {
        $request = Input::all();       
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        //$arr_institute_detail = Institute::find($institute_id);        
        if ($request['email'] != '') {
            /* get user information to send password detail */
            $arr_password_data = User::where('email', '=', $request['email'])->get();
            $email = $arr_password_data[0]['email'];
            if (count($arr_password_data) > 0) {
                //dd($arr_password_data);
                $code = rand(9991, 999899);
                $activation_code = time();
                $update_data = array('reset_password_code' => $activation_code, 'password_change_security_code' => $code);
                User::where('email', $request['email'])->update($update_data);
                $reset_password_link = '<a href="' . url('/') . '/auth/reset-password/' . base64_encode($activation_code) . '">Click here</a>';
                $email_contents = email_templates::where(array('email_template_title' => 'forgot-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];
                $macros_array_detail = array();
                $macros_array_detail = email_template_macros::all();
                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }
                $reserved_words = array();
                $reserved_arr = array
                    (
                    "||USER_NAME||" => $arr_password_data[0]['first_name'],
                    "||FIRST_NAME||" => $arr_password_data[0]['first_name'],
                    "||LAST_NAME||" => $arr_password_data[0]['last_name'],
                    "||USER_EMAIL||" => $arr_password_data[0]['email'],
                    "||RESET_PASSWORD_LINK||" => $reset_password_link,
                    "||SITE_TITLE||" => stripslashes($site_title->value),
                    "||CODE||" => $code,
                    "||SITE_PATH||" => url('/')
                );
                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                foreach ($reserved_words as $k => $v) {
                    $content = str_replace($k, $v, $content);
                }
                $contents_array = array("email_conents" => $content);
                $data['site_email'] = $site_email->value;
                $data['site_title'] = $site_title->value;

                // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

                Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_password_data, $contents_array, $data, $email_contents) {
                    $message->from($data['site_email'], $data['site_title']);
                    $message->to($email)->subject($email_contents['email_template_subject']);
                }); // End Mail
                Session::flash('message', 'We have sent reset password link on your email' . '<strong> ' . $arr_password_data[0]['email'] . '</strong>.');
                return redirect(url('/') . '/');
            }
        }
    }


    /*
     * Change new user's password
     */

    public function resetPassword($activation_code) {
        $request = Input::all();     
        $activation_code = base64_decode($activation_code);        
        /* cheaking password link expirted or not using reset_password_code; */
        $user_detail = User::where('reset_password_code', '=', $activation_code)->get();        
        if (count($user_detail) == 0) {
            Session::flash('message_error', 'Your reset password link has been expired');
            return redirect(url('/') . '/');
            exit;
        } elseif ($request['new_password'] != '') {           
            // generating the password by using hashing technique and applyng salt on it
            //crypt has method is used. 2y is crypt algorith selector
            //12 is workload factor on core processor.
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['new_password'], '$2y$12$' . $salt);
            $arr_to_update = array(
                'reset_password_code' => '',
                'password' => $hash_password,
            );            
            User::where('reset_password_code', $activation_code)->update($arr_to_update);           
            Session::flash('message', 'Your password has been reset successfully');
            return redirect(url('/') . '/');
            exit;
        }
    }
    public function chkEmailExist() {       
        $all = Input::all();
        /* checking  user email already exist or not  */
        $arr_user_detail = User::where('email', $all['email'])->first();
        if (count($arr_user_detail)) {
            echo "true";
        } else {
            echo "false";
        }
        exit;
        
    }
}
    