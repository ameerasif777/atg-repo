<?php

namespace App\Http\Controllers;

use Mail;
use DB;
use App\AdminUser;
use App\role;
use App\email_templates;
use App\email_template_macros;
use App\global_settings;
use App\trans_global_settings;
use App\Institute;
use App\SocialLink;
use App\UserAcademics;
use App\course;
use App\degree;
use App\UserCertification;
use App\UserAchievements;
use App\UserLanguage;
use App\UserInterest;
use App\UserExperience;
use App\CommonModel;
use App\saveUserPost;
use App\UserEducationalDocuments;
use App\UserProfileSettings;
use App\UserProfileImages;
use Session;
use Illuminate\Filesystem\Filesystem;
use Auth;
use Illuminate\Support\Facades\Input;;
use Request;
use App\UserInstituteFollow;
use App\Traits\UserDetails;

class UserController extends Controller {

    use UserDetails;
    
    public function __construct() {
        $this->middleware('auth', ['except' => ['getAdminLogout']]);
    }

    public function index() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        if ($user->portfolio_flag == '0') {
            return redirect('user/portfolio/add-personal');
        } else {
            return $this->viewPersonal();
        }
    }

    public function viewPersonal() {
        $user = Auth::user();
        $social_data = SocialLink::where('user_id', '=', $user->id)->get();
        if (isset($user->institute_id)) {
            $institute_data = Institute::where('id', '=', $user->institute_id)->select('institute_name')->first();
            if (isset($institute_data->institute_name)) {
                $institute_name = $institute_data->institute_name;
            } else {
                $institute_name = "";
            }
        } else {
            $institute_name = "";
        }
        return view('Frontend.user.portfolio.personal.view-personal', ['arr_user_detail' => $user, 'site_title' => "Portfolio", 'social_data' => $social_data, 'institute_name' => $institute_name]);
    }

    public function addAcademics() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();
        $arr_course_detail = course::all();
        $arr_degree_detail = degree::all();

        $site_title = "Portfolio";

        return view('Frontend.user.portfolio.academics.add-academics', ['arr_institute_detail' => $arr_institute_detail, 'arr_course_detail' => $arr_course_detail, 'arr_degree_detail' => $arr_degree_detail, 'site_title' => "Portfolio"]);
    }

    public function editAcademics($id) {
        $edit_id = base64_decode($id);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();
        $arr_course_detail = course::all();
        $arr_degree_detail = degree::all();

        $arr_academics_details = UserAcademics::where('academic_id', '=', $edit_id)->first();

        $site_title = "Portfolio";

        return view('Frontend.user.portfolio.academics.edit-academics', ['arr_academics_details' => $arr_academics_details, 'arr_institute_detail' => $arr_institute_detail, 'arr_course_detail' => $arr_course_detail, 'arr_degree_detail' => $arr_degree_detail, 'site_title' => "Portfolio"]);
    }

    public function addInterest() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $site_title = "Portfolio";
        $user_type = $user->user_type;
        return view('Frontend.user.portfolio.interest.add-interest', ['site_title' => "Portfolio", 'user_type' => $user_type]);
    }

    public function editInterest($interest_id) {
        $interest_id = base64_decode($interest_id);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_interest_detail = UserInterest::where('intrest_id', '=', $interest_id)->first();
        $arr_expertise_detail = UserExperience::where('user_id', '=', $user->id)->get();


        $site_title = "Portfolio";
        $user_type = $user->user_type;
        return view('Frontend.user.portfolio.interest.edit-interest', ['arr_expertise_detail' => $arr_expertise_detail, 'arr_interest_detail' => $arr_interest_detail, 'site_title' => "Portfolio", 'user_type' => $user_type]);
    }

    public function addCertifications() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();

        $site_title = "Portfolio";

        return view('Frontend.user.portfolio.certifications.add-certification', ['arr_institute_detail' => $arr_institute_detail, 'site_title' => "Portfolio"]);
    }

    public function addDocument() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $site_title = "Portfolio";
        return view('Frontend.user.portfolio.educational_documents.add-documents', ['site_title' => "Portfolio"]);
    }

    public function editCertifications($edit_id) {
        $edit_id = base64_decode($edit_id);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_certification_detail = UserCertification::where('certification_id', '=', $edit_id)->first();
        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();

        $site_title = "Portfolio";


        return view('Frontend.user.portfolio.certifications.edit-certification', ['arr_certification_detail' => $arr_certification_detail, 'arr_institute_detail' => $arr_institute_detail, 'site_title' => "Portfolio"]);
    }

    public function addLanguage() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $site_title = "Portfolio";
        $arr_lanaguage_data = CommonModel::getLanguage();

        return view('Frontend.user.portfolio.language.add-language', ['arr_lanaguage_data' => $arr_lanaguage_data, 'site_title' => "Portfolio"]);
    }

    public function editLanguage($edit_id) {
        $edit_id = base64_decode($edit_id);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $site_title = "Portfolio";
        $arr_lanaguage_data = CommonModel::getLanguage();
        $arr_language_detail = UserLanguage::where('language_id', '=', $edit_id)->first();
        return view('Frontend.user.portfolio.language.edit-language', ['arr_language_detail' => $arr_language_detail, 'site_title' => "Portfolio", 'arr_lanaguage_data' => $arr_lanaguage_data]);
    }

    public function addAchievements() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $site_title = "Portfolio";

        return view('Frontend.user.portfolio.achievements.add-achievements', [ 'site_title' => "Portfolio"]);
    }

    public function editAchievements($edit_id) {
        $edit_id = base64_decode($edit_id);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_achievements_detail = UserAchievements::where('achievement_id', '=', $edit_id)->first();
        $site_title = "Portfolio";

        return view('Frontend.user.portfolio.achievements.edit-achievements', ['arr_achievements_detail' => $arr_achievements_detail, 'site_title' => "Portfolio"]);
    }

    public function viewAcademics() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_academics_details = UserAcademics::get_all_academics($user->id);
        return view('Frontend.user.portfolio.academics.view-academics', ['arr_academics_details' => $arr_academics_details, 'site_title' => "Portfolio"]);
    }

    public function viewDocument() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_documents_details = UserEducationalDocuments::where('user_id', '=', $user->id)->get();
        return view('Frontend.user.portfolio.educational_documents.view-documents', ['arr_documents_details' => $arr_documents_details, 'site_title' => "Portfolio"]);
    }

    public function viewInterest() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_interest_details = UserInterest::where('user_id', '=', $user->id)->get();
        $arr_experience_details = UserExperience::where('user_id', '=', $user->id)->get();
        return view('Frontend.user.portfolio.interest.view-interest', ['arr_experience_details' => $arr_experience_details, 'arr_interest_details' => $arr_interest_details, 'site_title' => "Portfolio", 'user_type' => $user->user_type]);
    }

    public function viewAchievements() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_achievements_detail = UserAchievements::where('user_id', '=', $user->id)->get();
        return view('Frontend.user.portfolio.achievements.view-achievements', ['arr_achievements_detail' => $arr_achievements_detail, 'site_title' => "Portfolio"]);
    }

    public function viewCertifications() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_certification_details = UserCertification::get_all_certification($user->id);

        return view('Frontend.user.portfolio.certifications.view-certification', ['arr_certification_details' => $arr_certification_details, 'site_title' => "Portfolio"]);
    }

    public function viewProfileSettings() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }
        $arr_profile_settings = UserProfileSettings::where('user_id', '=', $user->id)->first();

        return view('Frontend.user.portfolio.profile_settings.view-profile-settings', ['arr_profile_settings' => $arr_profile_settings, 'site_title' => "Profile Settings"]);
    }

    public function viewLanguage() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }


        $arr_language_details = UserLanguage::where('user_id_fk', '=', $user->id)->get();

        return view('Frontend.user.portfolio.language.view-language', ['arr_language_details' => $arr_language_details, 'site_title' => "Portfolio"]);
    }

    public function viewWall() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }
        $arr_user_detail = $user;
        $arr_condition = ['user_id' => $user->id, 'profile_current_status' => '1'];

        $res_profile_pic = UserProfileImages::where($arr_condition)->select('profile_pic')->first();

        if (!isset($res_profile_pic->profile_pic)) {
            $profile_pic = "";
        } else {
            $profile_pic = $res_profile_pic->profile_pic;
        }

        $profile_pic = "";
        $arr_user_detail['profile_pic'] = $profile_pic;
        return view('Frontend.user.wall.wall', ['site_title' => "Home", 'arr_user_detail' => $arr_user_detail]);
    }

    public function redirectHome() {
        return redirect('user/home');
    }

    public function savePersonal() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;
        $profile = new UserProfileImages();
        if (Input::hasFile('profile_pic')) {
            $destinationPath = public_path() . "/assets/Frontend/user/profile_pics/$user_detail->id";
            $obj = new Filesystem();
            if (!$obj->exists($destinationPath)) {
                $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
            }
            try {
                $extension = Input::file('profile_pic')->getClientOriginalExtension(); // getting image extension
                $fileName = Input::file('profile_pic')->getClientOriginalName(); // renameing image
                $image = \Image::make(\Input::file('profile_pic'));
                // save original
                $image->save($destinationPath . '/' . $fileName);
                $profile->profile_pic = $fileName;
                $profile->profile_current_status = '1';
                //resize
                $image->resize(300, 200);
                $destinationPath = public_path() . "/assets/Frontend/user/profile_pics/$user_detail->id/thumb";
                $obj = new Filesystem();
                if (!$obj->exists($destinationPath)) {
                    $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                }
                // save resized
                $image->save($destinationPath . '/' . 'thumb_' . $fileName);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }

        if (Input::hasFile('cover_pic')) {
            $destinationPath = public_path() . "/assets/Frontend/user/cover_pics/$user_detail->id";
            $obj = new Filesystem();
            if (!$obj->exists($destinationPath)) {
                $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
            }
            try {
                $extension = Input::file('cover_pic')->getClientOriginalExtension(); // getting image extension
                $fileName = Input::file('cover_pic')->getClientOriginalName(); // renameing image
                $image = \Image::make(\Input::file('cover_pic'));
                // save original
                $image->save($destinationPath . '/' . $fileName);
                $profile->cover_pic = $fileName;
                $profile->cover_current_status = '1';
                //resize
                $image->resize(300, 200);
                $destinationPath = public_path() . "/assets/Frontend/user/cover_pics/$user_detail->id/thumb";
                $obj = new Filesystem();
                if (!$obj->exists($destinationPath)) {
                    $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                }
                // save resized
                $image->save($destinationPath . '/' . 'thumb_' . $fileName);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
        $profile->user_id = $user_detail->id;
        $profile->save();

        $arr_request_data = array(
            'gender' => Input::get('gender'),
            'user_birth_date' => Input::get('dob'),
            'current_status' => Input::get('current_status'),
            'current_place' => Input::get('current_place'),
            'home_town' => Input::get('home_town')
        );
        if (Input::has('contact_no')) {
            $arr_request_data['contact_no'] = Input::get('contact_no');
        }
        if (Input::get('current_status') == '1') {
            $arr_request_data['institute_id'] = Input::get('institute_id');
        } else {
            $arr_request_data['company'] = Input::get('company_name');
        }
        $arr_request_data['portfolio_flag'] = '1';

        AdminUser::where('id', $user_id)->update($arr_request_data);

        $arr_social_site_name = Input::get('social_site_name');
        $arr_social_link = Input::get('social_link');

        $arr_social_site_name = array_filter($arr_social_site_name);
        $arr_social_link = array_filter($arr_social_link);
        if ((count($arr_social_site_name) > 0) && (count($arr_social_link) > 0)) {
            $arr_social_site_name = Input::get('social_site_name');
            $arr_social_link = Input::get('social_link');
            $data = array_combine($arr_social_site_name, $arr_social_link);
            foreach ($data as $site => $link) {
                $social_link = new SocialLink();
                $social_link->user_id = $user_id;
                $social_link->social_site_name = $site;
                $social_link->social_link = $link;
                $social_link->save();
            }
        }

        Session::flash('message', 'Your information updated successfully.');

        return redirect('user/portfolio');
    }

    public function updatePersonal() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;

        $arr_request_data = array(
            'gender' => Input::get('gender'),
            'user_birth_date' => Input::get('dob'),
            'current_status' => Input::get('current_status'),
            'current_place' => Input::get('current_place'),
            'home_town' => Input::get('home_town')
        );
        if (Input::has('contact_no')) {
            $arr_request_data['contact_no'] = Input::get('contact_no');
        }
        if (Input::get('current_status') == '1') {
            $arr_request_data['institute_id'] = Input::get('institute_id');
        } else {
            $arr_request_data['company'] = Input::get('company_name');
        }
        $arr_request_data['portfolio_flag'] = '1';

        AdminUser::where('id', $user_id)->update($arr_request_data);
        $affectedRows = SocialLink::where('user_id', '=', $user_id)->delete();

        $arr_social_site_name = Input::get('social_site_name');
        $arr_social_link = Input::get('social_link');

        $arr_social_site_name = array_filter($arr_social_site_name);
        $arr_social_link = array_filter($arr_social_link);
        if ((count($arr_social_site_name) > 0) && (count($arr_social_link) > 0)) {
            $arr_social_site_name = Input::get('social_site_name');
            $arr_social_link = Input::get('social_link');
            $data = array_combine($arr_social_site_name, $arr_social_link);
            foreach ($data as $site => $link) {
                $social_link = new SocialLink();
                $social_link->user_id = $user_id;
                $social_link->social_site_name = $site;
                $social_link->social_link = $link;
                $social_link->save();
            }
        }

        Session::flash('message', 'Your information updated successfully.');

        return redirect('user/portfolio');
    }

    public function saveAcademics() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;
        $duration_to = Input::get('duration_to');
        $academics = new UserAcademics();
        $academics->institute_id_fk = Input::get('institute_id');
        $academics->status = Input::get('status');
        $academics->duration_from = Input::get('duration_from');
        $academics->duration_to = $duration_to;
        $academics->course = Input::get('course');
        $academics->degree = Input::get('degree');
        $academics->grade = Input::get('grade');
        $academics->user_id = $user_id;
        $academics->save();
        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/academics');
    }

    public function updateAcademics() {
        $academic_id = base64_decode(Input::get('academic_id'));
        $duration_to = Input::get('duration_to');
        $arr_data['institute_id_fk'] = Input::get('institute_id');
        $arr_data['status'] = Input::get('status');
        $arr_data['duration_from'] = Input::get('duration_from');
        $arr_data['duration_to'] = $duration_to;
        $arr_data['course'] = Input::get('course');
        $arr_data['degree'] = Input::get('degree');
        $arr_data['grade'] = Input::get('grade');

        UserAcademics::where('academic_id', '=', $academic_id)->update($arr_data);

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/academics');
    }

    public function saveInterest() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;
        $count = UserInterest::where('user_id', '=', $user_id)->count();

        //if count is equal to zero then add record else update
        if ($count === 0) {
            $skill = Input::get('skills');
            $skill = $skill[0];
            $interests_data = Input::get('interests');
            $interests_data = $interests_data[0];
            $user_id = $user_detail->id;

            $interest = new UserInterest();
            $interest->interest = $interests_data;
            $interest->skill = $skill;
            $interest->user_id = $user_id;
            $interest->save();
        } else {
            //getting existing table data and concating with new data
            $skill = Input::get('skills');
            $skill = $skill[0];

            $interests_data = Input::get('interests');
            $interests_data = $interests_data[0];

            $user_id = $user_detail->id;
            $existing_interest_data = UserInterest::where('user_id', $user_id)->first();
            $interests_data = $existing_interest_data->interest . ',' . $interests_data;
            $skill = $existing_interest_data->skill . ',' . $skill;
            $arr_update_data['interest'] = $interests_data;
            $arr_update_data['skill'] = $skill;
            UserInterest::where('user_id', $user_id)->update($arr_update_data);
        }

        if ($user_detail->user_type != '3') {
            $arr_field = Input::get('field');
            $arr_experience = Input::get('experience');

            $arr_experience = array_filter($arr_experience);
            $arr_field = array_filter($arr_field);
            if ((count($arr_field) > 0) && (count($arr_experience) > 0)) {
                $arr_field = Input::get('field');
                $arr_experience = Input::get('experience');
                $data = array_combine($arr_field, $arr_experience);
                foreach ($data as $field => $exp) {
                    $obj = new UserExperience();
                    $obj->user_id = $user_id;
                    $obj->field = $field;
                    $obj->years = $exp;
                    $obj->save();
                }
            }
        }

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/interest');
    }

    public function updateInterest() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;

        $skill = Input::get('skills');
        $skill = $skill[0];

        $interests_data = Input::get('interests');
        $interests_data = $interests_data[0];

        $arr_update_data['interest'] = $interests_data;
        $arr_update_data['skill'] = $skill;
        UserInterest::where('user_id', $user_id)->update($arr_update_data);


        if ($user_detail->user_type != '3') {
            $affectedRows = UserExperience::where('user_id', '=', $user_id)->delete();
            $arr_field = Input::get('field');
            $arr_experience = Input::get('experience');

            $arr_experience = array_filter($arr_experience);
            $arr_field = array_filter($arr_field);
            if ((count($arr_field) > 0) && (count($arr_experience) > 0)) {
                $arr_field = Input::get('field');
                $arr_experience = Input::get('experience');
                $data = array_combine($arr_field, $arr_experience);
                foreach ($data as $field => $exp) {
                    $obj = new UserExperience();
                    $obj->user_id = $user_id;
                    $obj->field = $field;
                    $obj->years = $exp;
                    $obj->save();
                }
            }
        }

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/interest');
    }

    public function saveAchievements() {
        $user_detail = Auth::user();
        $date = Input::get('date');
        $date = date("Y-m-d", strtotime($date));
        $user_id = $user_detail->id;
        $achievements = new UserAchievements();
        $achievements->title = Input::get('title');
        $achievements->status = Input::get('status');

        $achievements->achievement_date = $date;
        $achievements->venue = Input::get('venue');
        $achievements->user_id = $user_id;
        $achievements->save();

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/achievements');
    }

    public function updateAchievements() {
        $achievement_id = base64_decode(Input::get('achievement_id'));
        $date = Input::get('date');
        $date = date("Y-m-d", strtotime($date));

        $arr_achievements['title'] = Input::get('title');
        $arr_achievements['status'] = Input::get('status');

        $arr_achievements['achievement_date'] = $date;
        $arr_achievements['venue'] = Input::get('venue');

        UserAchievements::where('achievement_id', '=', $achievement_id)->update($arr_achievements);

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/achievements');
    }

    public function saveCertifications() {
        $user_detail = Auth::user();

        $user_id = $user_detail->id;
        $duration_to = Input::get('duration_to');
        $certification = new UserCertification();
        $certification->institute_id_fk = Input::get('institute_id');
        $certification->certification_name = Input::get('certification_name');
        $certification->duration_from = Input::get('duration_from');
        $certification->duration_to = $duration_to;
        $certification->grade = Input::get('grade');
        $certification->user_id = $user_id;
        $certification->save();
        Session::flash('message', 'Your information updated successfully.');

        return redirect('user/certifications');
    }

    public function saveProfileSettings() {

        $user_detail = Auth::user();
        $user_id = $user_detail->id;

        $setting['academic_visibility'] = Input::get('academic_visibility');
        $setting['certification_visibility'] = Input::get('certification_visibility');
        $setting['achievement_visibility'] = Input::get('achievement_visibility');
        $setting['interest_visibility'] = Input::get('interest_visibility');
        $setting['language_visibility'] = Input::get('language_visibility');


        UserProfileSettings::where('user_id', '=', $user_id)->update($setting);
        Session::flash('message', 'Your information updated successfully.');

        return redirect('user/portfolio');
    }

    public function saveDocument() {
        $user_detail = Auth::user();

        if (Input::hasFile('edu_Attachment')) {
            $destinationPath = public_path() . "/assets/Frontend/user/educational_documents/$user_detail->id";
            $obj = new Filesystem();
            if (!$obj->exists($destinationPath)) {
                $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
            }
            try {
                $extension = Input::file('edu_Attachment')->getClientOriginalExtension(); // getting image extension
//                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                $fileName = Input::file('edu_Attachment')->getClientOriginalName(); // renameing image
                $image = \Image::make(\Input::file('edu_Attachment'));
                // save original
                $image->save($destinationPath . '/' . $fileName);
                //resize
                //	$image->resize(300, 200);
                // save resized
                //	$image->save($destinationPath . '/thumb/thumb_' . $fileName);
                $document = new UserEducationalDocuments();
                $document->title = Input::get('title');
                $document->document_name = $fileName;
                $document->user_id = $user_detail->id;
                $document->save();
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/educational_documents');
    }

    public function updateCertifications() {

        $user_detail = Auth::user();
        $user_id = $user_detail->id;
        $certification_id = base64_decode(Input::get('certification_id'));
        $duration_to = Input::get('duration_to');
        $arr_certification['institute_id_fk'] = Input::get('institute_id');
        $arr_certification['certification_name'] = Input::get('certification_name');
        $arr_certification['duration_from'] = Input::get('duration_from');
        $arr_certification['duration_to'] = $duration_to;
        $arr_certification['grade'] = Input::get('grade');
        UserCertification::where('certification_id', '=', $certification_id)->update($arr_certification);
        Session::flash('message', 'Your information updated successfully.');

        return redirect('user/certifications');
    }

    public function saveLanguage() {
        $user_detail = Auth::user();
        $user_id = $user_detail->id;
        $language = new UserLanguage();
        $language->language = Input::get('language');
        $language->proficiency = Input::get('proficiency');
        $language->user_id_fk = $user_id;
        $language->save();
        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/language');
    }

    public function updateLanguage() {
        $lang_id = base64_decode(Input::get('language_id'));

        $arr_language['language'] = Input::get('language');
        $arr_language['proficiency'] = Input::get('proficiency');
        UserLanguage::where('language_id', '=', $lang_id)->update($arr_language);

        Session::flash('message', 'Your information updated successfully.');
        return redirect('user/language');
    }

    public function getLang() {
        $key = Input::get('key');
        $data = CommonModel::getLanguage($key);
        $str = '<ul id="country-list">';
        foreach ($data as $lang) {
            $str.='<li onClick="selectLang(\'' . $lang->lang_name . '\');">' . $lang->lang_name . '</li>';
        }
        $str.='</ul>';
        echo $str;
    }

    public function addPersonal() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();
        $site_title = "Portfolio";
        return view('Frontend.user.portfolio.personal.add-personal', ['arr_user_detail' => $user, 'site_title' => $site_title, 'arr_institute_detail' => $arr_institute_detail]);
    }

    public function editPersonal() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('message_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            } else if ($user->user_status == 2) {
                Session::flash('message_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('message_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/');
        }

        $arr_institute_detail = Institute::where('user_status', '=', 1)
                ->where('setup_request', '=', 2)
                ->where('email_verified', '=', 1)
                ->select('id', 'institute_name')
                ->get();
        $arr_social_data = SocialLink::where('user_id', '=', $user->id)->get();
        $site_title = "Portfolio";
        return view('Frontend.user.portfolio.personal.edit-personal', ['arr_social_data' => $arr_social_data, 'arr_user_detail' => $user, 'site_title' => $site_title, 'arr_institute_detail' => $arr_institute_detail]);
    }

    public function viewPortfolio() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/login');
        }
        $user_id = Auth::user()->id;

        if ($user->user_type == '0') {
            $msg = "Please complete your profile";
        } else {
            $msg = '';
        }


        $site_title = "User Portfolio";
        return view('Frontend.user.portfolio', ['arr_user_detail' => $user, 'site_title' => $site_title, 'msg' => $msg]);
    }

    public function deleteAcademics($id) {
        $id = base64_decode($id);
        $affected_row = UserAcademics::where('academic_id', '=', $id)->delete();
        if ($affected_row > 0) {
            Session::flash('message', 'Record deleted succesfully.');
        }
        return redirect(url('/') . '/user/academics');
    }

    public function deleteCertifications($id) {
        $id = base64_decode($id);
        $affected_row = UserCertification::where('certification_id', '=', $id)->delete();
        if ($affected_row > 0) {
            Session::flash('message', 'Record deleted succesfully.');
        }
        return redirect(url('/') . '/user/certifications');
    }

    public function deleteAchievements($id) {
        $id = base64_decode($id);
        $affected_row = UserAchievements::where('achievement_id', '=', $id)->delete();
        if ($affected_row > 0) {
            Session::flash('message', 'Record deleted succesfully.');
        }
        return redirect(url('/') . '/user/achievements');
    }

    public function deleteLanguage($id) {
        $id = base64_decode($id);
        $affected_row = UserLanguage::where('language_id', '=', $id)->delete();
        if ($affected_row > 0) {
            Session::flash('message', 'Record deleted succesfully.');
        }
        return redirect(url('/') . '/user/language');
    }

    public function deleteDocument($id, $document_name) {

        $user = Auth::user();
        $user_id = $user->id;
        $id = base64_decode($id);
        $document_name = base64_decode($document_name);
        $affected_row = UserEducationalDocuments::where('document_id', '=', $id)->delete();

        if (file_exists('public/assets/Frontend/user/educational_documents/' . $user_id . '/' . $document_name)) {
            unlink('public/assets/Frontend/user/educational_documents/' . $user_id . '/' . $document_name);
        }



        if ($affected_row > 0) {
            Session::flash('message', 'Record deleted succesfully.');
        }
        return redirect(url('/') . '/user/educational_documents');
    }

    public function downloadDocument($user_id, $file_name) {

//		dd(app_path().'/public/assets/Frontend/user/educational_documents/97/profile.jpg');
        $dir = base_path() . '/public/assets/Frontend/user/educational_documents/97/' . $file_name;


        if (file_exists($dir)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($dir));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($dir));
            ob_clean();
            flush();
            readfile($dir);
            exit;
        } else
            dd(123);
    }

    public function viewUserInstitutes($institute_type = 'all', $status = 'all', $key = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/login');
        }
        $user_id = Auth::user()->id;

        if (($institute_type == 'all') && ($status == 'you-may-follow')) {
            $arr_user_following_institute = UserInstituteFollow::getUserMayFollowInstitute();
        } else {
            $type = array();
            switch ($institute_type) {
                case "university":
                    $type[0] = '1';
                    break;
                case "college":
                    $type[0] = '2';
                    break;
                case "private_institute":
                    $type[0] = '3';
                    break;
                default:
                    $type = array('1', '2', '3');
            }


            $arr_status = array();
            switch ($status) {
                case "following":
                    $arr_status[0] = '1';
                    break;
                case "joined":
                    $arr_status[0] = '2';
                    break;
                default:
                    $arr_status = array('1', '2');
            }

            $arr_user_following_institute = UserInstituteFollow::getUserFollowingInstitute2($user_id, $key, $type, $arr_status);
        }
        $site_title = "My Institutes";
        $arr_search_criteria = array('institute_type' => $institute_type, 'status' => $status, 'key' => $key);
        return view('Frontend.user.institutes.institute', ['site_title' => $site_title, 'arr_user_following_institute' => $arr_user_following_institute, 'arr_search_criteria' => $arr_search_criteria]);
    }

    public function viewUserMayFollowInstitutes($institute_type = 'all', $key = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/login');
        }
        $user_id = Auth::user()->id;





        $type = array();
        switch ($institute_type) {
            case "university":
                $type[0] = '1';
                break;
            case "college":
                $type[0] = '2';
                break;
            case "private_institute":
                $type[0] = '3';
                break;
            default:
                $type = array('1', '2', '3');
        }





        $arr_user_following_institute = UserInstituteFollow::getUserMayFollowInstitute($key, $type);

        $site_title = "Colleges you may follow";
        $arr_search_criteria = array('institute_type' => $institute_type, 'key' => $key);
        return view('Frontend.user.institutes.user-may-follow-institute', ['site_title' => $site_title, 'arr_user_following_institute' => $arr_user_following_institute, 'arr_search_criteria' => $arr_search_criteria]);
    }

    public function setFollowStatus() {
        $user = Auth::user();
        $institute_id = Input::get('institute_id');
        $status = Input::get('status');
        $obj = new UserInstituteFollow();
        $obj->user_id_fk = $user->id;
        $obj->institute_id_fk = $institute_id;
        $obj->status = $status;
        $obj->save();
        echo '1';
    }

    public function setUnfollowStatus() {
        $user = Auth::user();
        $follow_id = Input::get('follow_id');
        UserInstituteFollow::where('follow_id', '=', $follow_id)->where('user_id_fk', '=', $user->id)->delete();
        echo '1';
    }

//		public function ajaxGetInstitute($institute_type='all',$status='all') {
//		if (Auth::check()) {
//			$user = Auth::user();
//			if ($user->user_status == 0) {
//				Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
//				Auth::logout();
//				return redirect()->guest('/auth/login');
//			} else if ($user->user_status == 2) {
//				Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
//				Auth::logout();
//				return redirect()->guest('/auth/login');
//			}
//		} else {
//			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
//			return redirect()->guest('/auth/login');
//		}
//		$user_id = Auth::user()->id;
//
//		if(($institute_type=='all') && ($status=='you-may-follow'))
//		{
//		$arr_user_following_institute=UserInstituteFollow::getUserMayFollowInstitute();
//
//		}
//		else
//		{
//			$arr_user_following_institute=UserInstituteFollow::getUserFollowingInstitute($user_id);
//		}	
//		$site_title = "My Institutes";
//		return view('Frontend.user.institutes.institute', ['site_title' => $site_title,'arr_user_following_institute'=>$arr_user_following_institute]);
//	}


    public function saveUserPost() {

        $request = Input::all();
        $post_id = $request['post_id'];
        $user_id = $request['user_id'];
        $type = $request['type'];
//        dd($request);
        $get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $post_id)->where('type', $type)->first();

        if (count($get_save_user_post) == 0) {
            $add_post = new saveUserPost();
            $add_post->user_id_fk = $user_id;
            $add_post->post_id_fk = $post_id;
            $add_post->type = $type;
            $add_post->save();
            return 0;
        } else {
            saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $post_id)->where('type', $type)->delete();
            return 1;
        }
    }
    
    public function getAllAbsentUsers()
    {
        $users = array();
        $data = $this->getAllAbsentUsersTrait();
        $arr_email = array();
        if(count($data))
        {
            foreach($data as $value)
            {
                $users[] = $value->id;
            }
            $arr_email = \App\email_templates::with('values')->get(['id', 'email_template_title']);
        }
        echo json_encode(array($users,$arr_email));
    }
    
    
    public function getAllUsers()
    {
        $users = array();
        $data = $this->getAllUsersTrait();
        if(count($data))
        {
            foreach($data as $value)
            {
                $users[] = $value->id;
            }
            $arr_email = \App\email_templates::with('values')->get(['id', 'email_template_title']);
        }
        echo json_encode(array($users,$arr_email));
    }

}
