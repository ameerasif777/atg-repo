<?php

namespace App\Http\Controllers;

use DB;
use App\languages;
use App\global_settings;
use App\email_templates;
use App\email_template_macros;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;;

class EmailTemplateController extends Controller {
    /* construction function used to load all the models used in the controller.	   */

    public function __construct() {
        
    }

    /* function to list all the states */

    public function listEmail() {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        
          /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('1', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */

        $global = \App\global_settings::with('values')->get();
        
        $arr_email = \App\email_templates::with('values')->get();
        $site_title = "Manage Email Templates";
        return view('Backend.email-template.list', ['arr_email_templates' => $arr_email, 'site_title' => $site_title, 'global' => $global]);
    }

    /* function for edi temail template  */

    public function editEmailTemplate($email_template_id = '') {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        
           /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('1', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        
        $edit_template_data = email_templates::with('values')->find($email_template_id);
        $all_macros = email_template_macros::all();
        $site_title = "Edit Email Templates";
        return view('Backend.email-template.edit-template', ['arr_email_template_details' => $edit_template_data, 'title_for_layout' => 'data pawar', 'arr_macros' => $all_macros, 'email_template_id' => $email_template_id, 'site_title' => $site_title]);
    }

    public function updateEmailTemplate() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::all();
        $arr_update_data = array(
            'email_template_subject' => $request['input_subject'],
            'email_template_content' => $request['text_content'],
            'updated_at' => date("Y-m-d H:i:s")
        );
        email_templates::where('id', $request['email_template_hidden_id'])->update($arr_update_data);
        Session::flash('message', 'Email Template details has been updated sucessfully!');
        return redirect('admin/email-template/list');
    }

}
