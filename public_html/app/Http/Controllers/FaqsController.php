<?php

namespace App\Http\Controllers;

use DB;
use App\faqs;
use App\role;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;;
use Request;

class FaqsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $arr_faqs_list = faqs::where('status','=','Active')->get();
        return view('Frontend.faq.faq',['faqs_list'=>$arr_faqs_list]);
    }

    /* function to list all the FAQs */

    public function listFaqs() {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $date_format = 'Y-m-d';
        $arr_faqs_list = faqs::orderBy('id','DESC')->get();
        $date_format = 'Y-m-d';
        $site_title = "Manage FAQs";
        return view('Backend.faqs.list', ['arr_faqs' => $arr_faqs_list, 'site_title' => $site_title, 'date_format' => $date_format]);
    }

    // Add New Faqs Start Here
    public function addFaqs(Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $arr_roles = role::all();
        $site_title = "Add FAQ";
        return view('Backend.faqs.add', ['site_title' => $site_title, 'arr_roles' => $arr_roles]);
    }

    // Add Faqs Action End Here
    public function addFaqsAction() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::get();
        $user = new faqs;
        $user->question = $request['question'];
        $user->answer = $request['answer'];
        $user->lang_id = '14';
        $user->status = 'Active';
        $user->created_on = date("Y-m-d H:i:s");
        $user->created_at = date("Y-m-d H:i:s");
        $user->updated_at = '';
        $user->save();
        /* inserting admin details into the dabase */
        $last_insert_id = $user->id;

        Session::flash('message', 'FAQs has been added successfully.');
        return redirect('admin/faqs/list');
    }

    // Add Faqs Action End Here

    /* function to edit the FAQs */

    public function editFaqs($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $arr_faq_detail = faqs::find(base64_decode($edit_id));
        $arr_roles = role::all();
        $site_title = "Update FAQ";
        $edit_id = $edit_id;
        return view('Backend.faqs.edit', ['arr_faq_detail' => $arr_faq_detail, 'site_title' => $site_title, 'arr_roles' => $arr_roles, 'edit_id' => $edit_id]);
    }

    public function updateFaqs() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::get();
        $edit_id = $request['edit_id'];

        $arr_request_data = array(
            'question' => $request['question'],
            'answer' => $request['answer']
        );
        faqs::where('id', $edit_id)->update($arr_request_data);

        Session::flash('message', 'Faqs has been updated successfully!');
        return redirect(url('/') . '/admin/faqs/list');
    }

    public function deleteFaqs() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $contact_ids = $request['checkbox'];

        if (!empty($contact_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($contact_ids) > 0) {
                foreach ($contact_ids as $val) {

                    faqs::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'FAQs has been deleted successfully.');
        return redirect('admin/faqs/list');
    }

    public function changeStatus() {
        $request = Input::all();
        if ($request['id'] != "") {
            /* updating the user status. */
            $arr_to_update = array("status" => $request['status']);
            /* updating the user status  value into database */
            faqs::where('id', $request['id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Faq status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

}
