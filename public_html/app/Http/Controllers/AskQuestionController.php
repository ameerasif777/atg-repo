<?php

namespace App\Http\Controllers;

use App\Filter;
use App\UserConnection;
use Exception;
use DB;
use Laravel\Socialite\Facades\Socialite;
use Mail;
use App\languages;
use App\global_settings;
use App\JobModel;
use App\answerMeetup;
use App\email_templates;
use App\email_template_macros;
use Illuminate\Http\Request;
use App\CommonModel;
use App\questionCommentModel;
use App\User;
use App\Helpers\GlobalData;
use App\trans_global_settings;
use Illuminate\Filesystem\Filesystem;
use Session;
use App\GroupUser;
use Auth;
use App\Answer;
use Validator;
use App\group;
use Illuminate\Support\Facades\Input;
use App\UserActivity;
use App\QuestionModel;
use App\groupQuestion;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Traits\Post\PostActions;
use App\Traits\sendsignupmail;
use App\promotionModel;
use App\postPromotion;
use App\Traits\commentsTrait;
class AskQuestionController extends Controller {

    use PostActions;
    use sendsignupmail;
    use commentsTrait;
    public $recordPerPage = 25;

    /*     * ******for backend********* */

    public function listQuestion() {

        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('13', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }

        $requestData = Input::all();

        $questionObj = DB::table('mst_question as mq')
                ->join('mst_users as mu', 'mu.id', '=', 'mq.user_id_fk')
                ->select('mq.*', 'mu.First_name', 'mu.last_name')
                ->orderBy('id', 'desc');

        if ($requestData && $requestData['search-qry'] != '') {
            $questionObj = $questionObj->whereRaw("(title like '%" . $requestData['search-qry'] . "%' or description like '%" .
                $requestData['search-qry'] . "%' or tags like '%" . $requestData['search-qry'] . "%')");
        }
        if ($requestData && $requestData['search-status'] != '') {
            $questionObj = $questionObj->where('status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }

        $question = $questionObj->paginate($recordPerPage);


        foreach ($question as $key => $value) {
            $arr_questions_answer = DB::table('mst_answers as mc')
                    ->join('trans_question_answers as tbc', 'mc.id', '=', 'tbc.answer_id_fk', 'left')
                    ->where('tbc.question_id_fk', $question[$key]->id)
                    ->select('mc.*', 'tbc.*')
                    ->get();
            $question_report = DB::table('mst_make_report')
                    ->where('question_id', $value->id)
                    ->get();
            $question[$key]->report_count = count($question_report);

            $question[$key]->count = count($arr_questions_answer);
        }

        $site_title = 'Manage Qrious';
        return view('Backend.ask-question.list', [
            'arr_question' => $question,
            'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
    }

    public function ViewBackendQuestion($view_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('13', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }

        $commonModel = new CommonModel();
        $arr_question = DB::table('mst_question as mq')
                ->join('mst_users as mu', 'mu.id', '=', 'mq.user_id_fk')
                ->select('mq.*', 'mu.First_name', 'mu.last_name')
                ->where('mq.id', base64_decode($view_id))
                ->get();
        $get_groups = DB::table('trans_question_group as t')
                ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                ->where('question_id_fk', base64_decode($view_id))
                ->select('mg.group_name', 'mg.id')
                ->get();

        $site_title = 'View Qrious Details';
        return view('Backend.ask-question.view', ['arr_question' => $arr_question, 'get_groups' => $get_groups, 'site_title' => $site_title]);
    }

    public function editQuestion($edit_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('13', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        $request = Input::all();
        if ((count($request) > 0)) {

            if ($request['title'] != '' && $request['description'] != '') {
                $question_object = QuestionModel::find(base64_decode($edit_id));
                $question_object->title = $request['title'];
                $question_object->description = $request['description'];
                $question_object->tags = $request['tags'];
                $question_object->status = $request['status'];

                if (isset($request['question_pic']) != '') {
                    if (Input::hasFile('question_pic')) {
                        $image_upload_success = $this->UploadCoverImagefx(\Input::file('question_pic'), 'qrious');
                        if ($image_upload_success["return_code"] == 0) {
                            $question_object->profile_image = trim($image_upload_success["msg"]);
                        } 
                        else if ($image_upload_success["return_code"] == 1) {
                            Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                            return redirect('/mypost');
                        }
                        $question_object->cover_type=0;
                    }
                }
                else{
                
                    $question_object->profile_image=$request['cover_url'];
                    $question_object->cover_type=$request['cover_type'];
                }

                $question_object->save();
                $lastInsertId = $question_object['id'];

                $get_groups = DB::table('trans_question_group as t')
                        ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                        ->where('t.question_id_fk', $question_object['id'])
                        ->select('mg.group_name', 'mg.id')
                        ->get();
                $arrQuestionGroup= Array();
                foreach ($get_groups as $key => $object) {
                    $arrQuestionGroup[] = (array) $object;
                }

                $question = DB::table('mst_question')
                        ->join('mst_users', 'mst_users.id', '=', 'mst_question.user_id_fk')
                        ->select('mst_question.*', 'mst_users.First_name', 'mst_users.last_name')
                        ->where('mst_question.id', base64_decode($edit_id))
                        ->get();
                
                $user_id = $question['0']->user_id_fk;

                $arr_users_group = DB::table('trans_group_users as t')
                        ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                        ->where('t.user_id_fk', $user_id)
                        ->groupBy('g.id')
                        ->select('g.group_name', 'g.id')
                        ->get();
                $arrUserGroup= Array();
                foreach ($arr_users_group as $key => $object) {
                    $arrUserGroup[] = (array) $object;
                }
                $arrUserGroups= Array();
                foreach ($arrUserGroup as $arrUserGP) {
                    $arrUserGroups[] = $arrUserGP['id'];
                }

                $alreadyGroup = DB::table('trans_question_group')->where('question_id_fk', $question_object['id'])->get();
                $alreadyGroups = Array();
                foreach ($alreadyGroup as $gp) {
                    $alreadyGroups[] = $gp->group_id_fk;
                }

                $delete = groupQuestion::where('question_id_fk', $lastInsertId)->delete();

                /*                 * *****for group********* */
                if (isset($request['sub_groups']) && $request['sub_groups'] != '') {
                    $post_link = url('/').'/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $question['0']->title) . '-' . $question['0']->id;

                    foreach ($arrQuestionGroup as $value) {
                        if(!in_array($value['id'], $request['sub_groups'])){
                            $groupName=$value['group_name'];
                            $groupLink=url('/').'/go/'.$value['group_name'];
                            $this->send_mail_by_adminfx($user_id, 'post-group-remove', $question['0']->title, $post_link, 'question', $groupName, $groupLink);
                        }
                    }

                    foreach ($request['sub_groups'] as $value) {
                        $question_group = new groupQuestion;
                        $question_group->group_id_fk = $value;
                        $question_group->question_id_fk = $lastInsertId;
                        $question_group->save();

                        if(!in_array($value, $arrUserGroups) && !in_array($value, $alreadyGroups)){
                            $groupAdd=DB::table('mst_group')->where('id', $value)->first();
                            $groupName=$groupAdd->group_name;
                            $groupLink=url('/').'/go/'.$groupAdd->group_name;
                            $this->send_mail_by_adminfx($user_id, 'post-group-add', $question['0']->title, $post_link, 'question', $groupName, $groupLink);
                        }

                    }
                } else {
                    $post_link = url('/').'/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $question['0']->title) . '-' . $question['0']->id;

                    foreach ($alreadyGroup as $removeGP) {
                        $groupRemove=DB::table('mst_group')->where('id', $removeGP->group_id_fk)->first();
                        $groupName=$groupRemove->group_name;
                        $groupLink=url('/').'/go/'.$groupRemove->group_name;
                        $this->send_mail_by_adminfx($user_id, 'post-group-remove', $question['0']->title, $post_link, 'question', $groupName, $groupLink);
                    }

                    $lastInsertId = $question_object['id'];
                    $delete = groupQuestion::where('question_id_fk', $lastInsertId)->delete();
                }
                /*                 * ************* */
                Session::flash('message', 'Question details has been updated successfully.');
                return redirect()->guest('admin/question/list');
            }
        } else {
            $commonModel = new CommonModel();
            $question = DB::table('mst_question as mq')
                    ->join('mst_users as mu', 'mu.id', '=', 'mq.user_id_fk')
                    ->select('mq.*', 'mu.First_name', 'mu.last_name')
                    ->where('mq.id', base64_decode($edit_id))
                    ->get();

            $user_id = $question['0']->user_id_fk;

            if (count($question) > 0) {
                /*                 * **for group**** */
                foreach ($question as $key => $value) {
                    $get_groups = DB::table('trans_question_group as t')
                            ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                            ->where('t.question_id_fk', base64_decode($edit_id))
                            ->select('mg.group_name', 'mg.id')
                            ->get();


                    /*$arr_users_group = DB::table('trans_group_users as t')
                            ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                            ->where('t.user_id_fk', $user_id)
                            ->groupBy('g.id')
                            ->select('g.group_name', 'g.id')
                            ->get();*/

                    $arr_users_group=Group::select('group_name', 'id')->get();
                }
                $arr = array();
                foreach ($get_groups as $key => $object) {
                    $arr[] = (array) $object;
                }

                $arr1 = array();

                foreach ($arr as $value) {
                    $arr1[] = $value['id'];
                }
                /*                 * ******************** */
            }
            $site_title = 'Edit Qrious Details';
            return view('Backend.ask-question.edit', ['arr_question' => $question, 'site_title' => $site_title, 'arr_selected_groups' => $arr1, 'user_group_data' => $arr_users_group, 'arr_selected_groups1' => $arr]);
        }
    }

    public function deleteQuestion() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $question_ids = $request['checkbox'];
        $is_mail_send = isset($request['send-mail']) ? $request['send-mail'] : '';
        if (!empty($question_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($question_ids) > 0) {

                foreach ($question_ids as $val) {
                    $queData = QuestionModel::where('id', $val)->get();
                    foreach ($queData as $que) {
                        DB::table('trans_question_answers')->where('question_id_fk', '=', $que->id)->delete();
                        DB::table('trans_question_group')->where('question_id_fk', '=', $que->id)->delete();
                        DB::table('trans_question_upvote_downvote')->where('question_id_fk', '=', $que->id)->delete();

                        if (isset($is_mail_send) && $is_mail_send == 'on') {

                            $commonModel = new CommonModel();
                            $data = $commonModel->commonFunction();
                            $email_contents = email_templates::where(array('email_template_title' => 'abuse-report-to-user', 'lang_id' => '14'))->first();
                            $content = $email_contents['email_template_content'];

                            $reserved_words = array();
                            $reserved_arr = array
                                ("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
                                "||SITE_PATH||" => url('/'),
                                "||NAME||" => $que->getQuestionUser[0]->first_name,
                                "||FEED_NAME||" => $que->title,
                            );

                            $macros_array_detail = array();
                            $macros_array_detail = email_template_macros::all();
                            $macros_array = array();
                            foreach ($macros_array_detail as $row) {
                                $macros_array[$row['macros']] = $row['value'];
                            }

                            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                            foreach ($reserved_words as $k => $v) {
                                $content = str_replace($k, $v, $content);
                            }
                            $contents_array = array("email_conents" => $content);


                            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.

                            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $email_contents, $que) {

                                $message->from($data['global']['site_email'], $data['global']['site_title']);

                                $message->to($que->getQuestionUser[0]->email)->subject($email_contents['email_template_subject']);
                            }); // End Mail
                        }



                    }
                    QuestionModel::where('id', $val)->delete();
                }
            }
        }
        Session::flash('success_msg', 'Qrious info has been deleted successfully.');
        return redirect('admin/question/list');
    }

    function listAnswer($question_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('13', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        $question_id = base64_decode($question_id);
        $question = DB::table('mst_question as ma')
                ->join('mst_users as mu', 'mu.id', '=', 'ma.user_id_fk')
                ->where('ma.id', $question_id)
                ->select('ma.*', 'mu.First_name', 'mu.last_name')
                ->orderBy('id', 'desc')
                ->get();
        foreach ($question as $key => $value) {
            $arr_questoin_answer = DB::table('mst_answers as mc')
                    ->join('trans_question_answers as tac', 'mc.id', '=', 'tac.answer_id_fk', 'left')
                    ->join('mst_users as u', 'u.id', '=', 'mc.answered_by', 'left')
                    //->where('tac.status', '1')
                    ->where('tac.question_id_fk', $question[$key]->id)
                    ->select('mc.*', 'u.user_name', 'u.first_name' , 'u.last_name' , 'u.profile_picture')
                    ->get();

            $question[$key]->answer = $arr_questoin_answer;
        }
        $site_title = 'Manage Answers';
        return view('Backend.ask-question.listAnswer', ['arr_question' => $question, 'site_title' => $site_title]);
    }

    function editAnswer($answer_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('13', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }

        $input_data = Input::all();
        if (!(empty($input_data))) {
            $arr_answer = Answer::find($answer_id);
            $arr_answer->answer = $input_data['answer'];
            $arr_answer->status = $input_data['status'];
            $arr_answer->save();
            $questions_answer = questionCommentModel::where('answer_id_fk', $answer_id)
                    ->update(['status' => $input_data['status']]);

            Session::flash('success_msg', 'Your answer has been update successfully.');
            return redirect()->guest('/admin/question/list');
        }

        $answer_id = base64_decode($answer_id);
        $arr_answer = Answer::where('id', $answer_id)->get();
        $questions_answer = questionCommentModel::where('answer_id_fk', $answer_id)->get();
        $site_title = 'Edit Answer';
        return view('Backend.ask-question.editAnswer', ['arr_answer' => $arr_answer, 'site_title' => $site_title]);
    }

    function deleteQuestionsAnswer(Request $request) {

        if (count($request->answer_ids) > 0) {
            foreach ($request->answer_ids as $key => $val) {

                $answer = Answer::find($val);
                $answer->delete();
                $arr_answer = questionCommentModel::where('answer_id_fk', $val)
                        ->delete();
            }
        }
        Session::flash('success_msg', 'Answer has been deleted successfully.');
        return redirect('admin/question/list');
    }

    /*     * ******for frontend*********** */

    public function postQuestion($group_id = '') {
        $user_account = Session::get('user_account');
        if (isset($user_account['email_verified']) && $user_account['email_verified'] == '0') {
            $this->sendEmailVerificationMail($user_account['email'] , $user_account);
            return redirect()->back()->with('warning_msg_permanent', "Verify your email <b>".$user_account['email']."</b> to post on ATG. Check your inbox/spam. <br> <b>".$user_account['email']."</b> not your email? <a style=\"color:white\" href=\"send-feedback\"> Contact us </a>");
        }
        $group_id = base64_decode($group_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            $data['user_session'] = Session::get('user_account');
            $arr_user_data = User::find($data['user_session']['id']);
            $arr_users_group = DB::table('mst_group')
                ->groupBy('id')
                ->select('group_name', 'id')
                ->get();
        }else{
            $data['user_session'] = Session::get('user_account');
            $arr_user_data = User::find($data['user_session']['id']);

            $user_id = $data['user_session']['id'];
            $arr_users_group = DB::table('trans_group_users as t')
                ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                ->where('user_id_fk', $user_id)
                ->groupBy('g.id')
                ->select('g.group_name', 'g.id')
                ->get();
        }

        $data['header'] = array(
            "title" => 'Ask a Question on ATG',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.ask-question.post-question')
                        ->with('title', 'Ask a Question')
                        ->with('arr_users_group', $arr_users_group)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('group_id', $group_id)
                        ->with('finalData', $data);
    }

    public function addPostQuestion() {

        $login = GlobalData::isLoggedIn();
        $request = Input::all();
       
        $datass='';
        if ($login) {
            if($request['posttype']=='0'){
                if($request['socialicons']== 'f'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('facebook')->redirect();
                }else if($request['socialicons']== 'g'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('google')->redirect();
                }else if($request['socialicons']== 't'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('twitter')->redirect();
                }else {
                    $datass = MakePostsPublicController::addPostQuetionsLoggedOutSignUp($request);
                }
            }else{
                if($request['socialicons']== 'f'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('facebook')->redirect();
                }else if($request['socialicons']== 'g'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('google')->redirect();
                }else if($request['socialicons']== 't'){
                    Session::put('mypost', $request);
                    Session::forget('student_fb_data');
                    return Socialite::driver('twitter')->redirect();
                }else{
                    MakePostsPublicController::addPostQuestionsLoggedOutSignIn($request);
                }
            }
        }
  
        $lastInsertId = $this->savePostQuestions($request);

        /*         * *********************** */
        if ($request['btn_post'] == '0') {
            Session::flash('success_msg', 'Qrious has been added successfully.');
            $link = '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;
            return redirect()->guest($link);       
            //return redirect('mypost');
        } else {
            Session::flash('success_msg', 'Qrious has been saved successfully.');
            return redirect('mypost');
        }
    }

    public function frontEditQuestion($edit_id) {

        $question_id = base64_decode($edit_id);
        $request = Input::all();
        if (isset($request['id']) != '') {
            $question_id = $request['id'];
        }
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }

        if (count($request) > 0) {

            $data['user_session'] = Session::get('user_account');
            $Question_object = QuestionModel::find($question_id);
            $Question_object->title = $request['title'];
            $Question_object->description = $request['description'];
            $Question_object->tags = $request['tags'];

            if($Question_object->status == '1')
            {
                $subject = 'updated a question - "' . $request['title'] . '"';

                $message = ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) .' updated qrious - "' . $request['title'] . '"';
                $link = '/view-question/' . $question_id;

                $User_activity = new UserActivity;
                $User_activity->user_id_fk = $data['user_session']['id'];
                $User_activity->activity_id = $question_id;
                $User_activity->activity_title = $subject;
                $User_activity->activity_link = $link;
                $User_activity->activity_table_name = 'mst_question';
                $User_activity->activity_action = 'updated qrious';
                $User_activity->save();
            }
            $Question_object->status = '1';
            if (isset($request['profile_image']) != '') {
                if (Input::hasFile('profile_image')) {
                    $image_upload_success = $this->UploadCoverImagefx(\Input::file('profile_image'), 'qrious');
                    if ($image_upload_success["return_code"] == 0) {
                        $Question_object->profile_image = trim($image_upload_success["msg"]);
                    } 
                    else if ($image_upload_success["return_code"] == 1) {
                        Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                        return redirect('/mypost');
                    }
                    $Question_object->cover_type=0;
                }
            }
            
            else{
                $Question_object->profile_image=$request['cover_url'];
                $Question_object->cover_type=$request['cover_type'];
            }
            $Question_object->save();


            /* update sub groups */
            if (isset($request['sub_groups']) && !(empty($request['sub_groups']))) {
                $lastInsertId = $Question_object['id'];
                $delete = groupQuestion::where('question_id_fk', $lastInsertId)->delete();

                foreach ($request['sub_groups'] as $value) {
                    $question_group = new groupQuestion;
                    $question_group->group_id_fk = $value;
                    $question_group->question_id_fk = $lastInsertId;
                    $question_group->save();
                }
            } else {
                $lastInsertId = $Question_object['id'];
                $delete = groupQuestion::where('question_id_fk', $lastInsertId)->delete();
            }

            /* Ends Here */
            Session::flash('success_msg', 'Qrious details have been updated successfully.');
            $link = '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;
            return redirect()->guest($link);       
            //return redirect('mypost');
        }


        /*         * **** for group********* */
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $condition_to_pass = array('id' => $question_id);
        $arr_question = $commonModel->getRecords('mst_question', '*', $condition_to_pass);
        $user_id = $arr_question['0']->user_id_fk;
        if (count($arr_question) > 0) {
            /**             * ** for group********* */
            foreach ($arr_question as $key => $value) {
                $get_groups = DB::table('trans_question_group as t')
                        ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                        ->where('t.question_id_fk', $question_id)
                        ->select('mg.group_name', 'mg.id')
                        ->get();
                $arr_users_group = DB::table('trans_group_users as t')
                        ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                        ->where('t.user_id_fk', $user_id)
                        ->groupBy('g.id')
                        ->select('g.group_name', 'g.id')
                        ->get();
            }
            $arr = array();
            foreach ($get_groups as $key => $object) {
                $arr[] = (array) $object;
            }

            $arr1 = array();

            foreach ($arr as $value) {
                $arr1[] = $value['id'];
            }
        }

        $condition_to_pass = array('id' => $question_id);
        $arr_question = $commonModel->getRecords('mst_question', '*', $condition_to_pass);
        $arr_user_data = User::find($user_id);
        $site_title = 'Edit Qrious Details';
        $data['header'] = array(
            "title" => 'Edit Qrious Details',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.ask-question.edit')
                        ->with('arr_question', $arr_question)
                        ->with('site_title', $site_title)
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('user_group_data', $arr_users_group)
                        ->with('arr_selected_groups', $arr1);
    }

    public function listFrontQuestion() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');

        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $data['header'] = array(
            "title" => 'Qrious List',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.ask-question.list-question')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('title', 'Qrious');
    }

    public function getAllPostsQuestion() {
        $request = Input::all();
        $page_number = $request['page'];
        $total_records = $page_number * 10;

        $user = Session ::get('user_account');
        $commonModel = new CommonModel();
        $userModel = new User();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        $condition = 'null';
        foreach ($get_groups as $key => $value) {
            $user_group[$key] = $value->group_id_fk;
        }

        $condition = implode(',', $user_group);

        $get_connections = DB::table('trans_user_connection')
                ->where('from_user_id', $user_id)
                ->orWhere('to_user_id', $user_id)
                ->where('status', '1')
                ->select('to_user_id', 'from_user_id')
                ->get();
        $condition1 = 'null';
        if (count($get_connections) > 0) {
            $temp_array = array();
            $user_ids = array();
            foreach ($get_connections as $key => $value) {
                $user_ids[] = $value->to_user_id;
                $user_ids[] = $value->from_user_id;
                $arr_ids = array();
                foreach ($user_ids as $ids) {
                    if (!in_array($ids, $arr_ids)) {
                        $arr_ids[] = $ids;
                    }
                }
            }
            $condition1 = implode(',', $arr_ids);
        }

        $arr_question = DB::table('mst_question as e');
        $arr_question->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
        $arr_question->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id', 'left');
//                $arr_question->WhereIn('e.user_id_fk', explode(',', $condition1));
        $arr_question->WhereIn('g.group_id_fk', explode(',', $condition));
        $arr_question->where(['e.status' => '1']);
        if ($request['search_text'] != '') {
            $arr_question->where('e.title', 'LIKE', '%' . trim($request['search_text']) . '%');
        }
        $arr_question->groupBy('e.id');
        $arr_question->orderBy('e.id', 'DESC');
        $arr_question->select('e.*', 'u.user_name');
        $arr_question = $arr_question->get();
        foreach ($arr_question as $key => $value) {
            $arr_questions_answer = DB::table('mst_answers as mc')
                    ->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
                    ->where('tec.question_id_fk', $arr_question[$key]->id)
                    ->select('mc.*')
                    ->get();
            $arr_question[$key]->count = count($arr_questions_answer);
        }
        if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
            $no = $all['slice_arr_count'];
        } else {
            $no = 0;
        }
        $arr_questions = $arr_question->slice($no, 10);
        $slice_arr_count = $no + 10;
        $data['header'] = array(
            "title" => 'Qrious List',
            "keywords" => '',
            "description" => ''
        );
        $total_count = count($arr_question);
        $arr_user_data = User::find($user_id);
        return view('Frontend.ask-question.all-post')
                        ->with('finalData', $data)
                        ->with('title', 'Qrious List')
                        ->with('questiondata', $arr_questions)
                        ->with('total_count', $total_count)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('slice_arr_count', $slice_arr_count);
    }

    public function listUserQuestion() {

        $commonModel = new CommonModel();

        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $data['header'] = array(
            "title" => 'Qrious List',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.ask-question.user-question')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('title', 'Qrious');
    }

    public function getAllUserPostsQuestion() {
        $request = Input::all();
        $page_number = $request['page'];
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        $arr_question = DB::table('mst_question as e');
        $arr_question->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
        $arr_question->where('user_id_fk', $user_id);
        if ($request['search_text'] != '') {
            $arr_question->where('e.title', 'LIKE', '%' . trim($request['search_text']) . '%');
        }
        $arr_question->groupBy('e.id');
        $arr_question->orderBy('e.id', 'DESC');
        $arr_question->select('e.*', 'u.user_name');
        $arr_question = $arr_question->get();

        foreach ($arr_question as $key => $value) {

            $arr_questions_answer = DB::table('mst_answers as mc')
                    ->join('trans_question_answers as tec', 'mc.id', '=', 'tec.answer_id_fk', 'left')
                    ->where('tec.question_id_fk', $arr_question[$key]->id)
                    ->select('mc.*')
                    ->get();
            $arr_question[$key]->count = count($arr_questions_answer);
        }
        if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
            $no = $all['slice_arr_count'];
        } else {
            $no = 0;
        }
        $arr_questions = $arr_question->slice($no, 10);
        $slice_arr_count = $no + 10;
        $total_count = count($arr_question);
        return view('Frontend.ask-question.all-user-posts')
                        ->with('questiondata', $arr_questions)
                        ->with('absolute_path', $data['absolute_path'])
                        ->with('total_count', $total_count)
                        ->with('slice_arr_count', $slice_arr_count);
    }

    public function viewQuestion($view_id, $response='') {

        $id = explode('-', $view_id);
        if (count($id) > 1) {
            $id = $id[1];
        }
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $arr_question = DB::table('mst_question as ma')
                ->where('id', $id)
                ->select('ma.*')
                ->get();
        if (count($arr_question) > 0) {
            $user_id_fk = $arr_question['0']->user_id_fk;
            $arr_user1_data = User::find($user_id_fk);
            $arr_user_data = User::find($user_id);


            $array_question = DB::table('mst_question as ma')
                    ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                    ->where('ma.id', $id)
                    ->select('ma.*', 'u.user_name', 'u.first_name', 'u.last_name', 'u.gender')
                    ->get();

            foreach ($array_question as $key => $value) {

                $question_answer = DB::table('mst_answers as mc')
                        ->join('trans_question_answers as tac', 'mc.id', '=', 'tac.answer_id_fk', 'left')
                        ->join('mst_users as u', 'u.id', '=', 'mc.answered_by', 'left')
                        ->where('tac.question_id_fk', $array_question[0]->id)
                        ->orderBy('id', 'DESC')
                        ->select('mc.*', 'u.user_name','u.first_name', 'u.last_name', 'u.profile_picture', 'tac.answer_image')
                        ->get();
                if (count($question_answer) > 0) {
                    foreach ($question_answer as $key1 => $value1) {
                        /*                         * **************upvote answer Downvote********************* */
                        $question_answer[$key1]->is_upvotes = DB::table('trans_answer_upvote_downvote as t')
                                ->where('user_id_fk', $user_id)
                                ->where('answer_id_fk', $value1->id)
                                ->select('t.status')
                                ->get();

                        $question_answer[$key1]->total_upvotes = count(DB::table('trans_answer_upvote_downvote')
                                        ->where('answer_id_fk', $value1->id)
                                        ->where('status', '0')
                                        ->select('status')
                                        ->get());

                        $question_answer[$key1]->total_downvotes = count(DB::table('trans_answer_upvote_downvote')
                                        ->where('answer_id_fk', $value1->id)
                                        ->where('status', '1')
                                        ->select('status')
                                        ->get());
                    }
                    $array_question[$key]->answer = $question_answer;
                    $array_question[$key]->count = count($question_answer);
                }
            }
            /**             * ***********Related Events********** */
            $user_group = array();
            $get_group = DB::table('trans_group_users')
                    ->where('user_id_fk', $user_id)
                    ->get();
            foreach ($get_group as $key => $value) {
                $user_group[$key] = $value->group_id_fk;
            }
            $condition = implode(',', $user_group);
            $related_question = DB::table('mst_question as e')
                    ->join('trans_question_group as g', 'g.question_id_fk', '=', 'e.id')
                    ->whereIn('g.group_id_fk', explode(',', $condition))
                    ->groupBy('e.id')
                    ->limit(20)
                    ->select('e.id', 'e.title as title', 'e.profile_image', 'e.created_at', 'e.post_short_description as description')
                    ->get();
            /**             * *********************************** */
            /*             * **************upvote question Downvote********************* */
            $is_upvotes = DB::table('trans_question_upvote_downvote as t')
                    ->where('user_id_fk', $user_id)
                    ->where('question_id_fk', $arr_question[0]->id)
                    ->select('t.status')
                    ->get();


            $total_upvotes = DB::table('trans_question_upvote_downvote')
                    ->where('question_id_fk', $arr_question[0]->id)
                    ->where('status', '0')
                    ->select('status')
                    ->get();

            $total_downvotes = DB::table('trans_question_upvote_downvote')
                    ->where('question_id_fk', $arr_question[0]->id)
                    ->where('status', '1')
                    ->select('status')
                    ->get();

            /*             * ********* */
            $arr_follower = DB::table('trans_follow_user')
                    ->where('user_id_fk', $user_id_fk)
                    ->select(DB::raw('count(follower_id_fk) as follower'))
                    ->get();
            $is_follower = DB::table('trans_follow_user')
                    ->where('user_id_fk', $user_id_fk)
                    ->where('follower_id_fk', $user_id)
                    ->get();

            $is_connect = DB::table('trans_user_connection')
                    ->where('from_user_id', $user_id_fk)
                    ->where('to_user_id', $user_id)
                    ->orWhere('from_user_id', $user_id)
                    ->Where('to_user_id', $user_id_fk)
                    ->get();

            $is_reported = DB::table('mst_make_report')
                    ->where('question_id', $arr_question[0]->id)
                    ->where('user_id_fk', $user_id)
                    ->get();

            /*             * *********for group************ */
            $get_groups = DB::table('trans_question_group as t')
                    ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                    ->where('question_id_fk', $arr_question[0]->id)
                    ->select('mg.group_name', 'mg.id', 'mg.profession')
                    ->get();

            /*             * ** Ends Here ***** */

            $data['header'] = array(
                "title" => $arr_question[0]->title . ' - Question & Answer on ATG',
                "meta_title" => $arr_question[0]->title,
                "meta_keywords" => $arr_question[0]->tags,
                "meta_description" => $arr_question[0]->description
            );
        } else {
            return redirect()->guest('/error');
        }

        $is_postfollower = DB::table('trans_question_follow_post as t')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $arr_question[0]->id)
                ->select('t.status')
                ->get();

        $total_unfollowPost = DB::table('trans_question_follow_post')
                ->where('question_id_fk', $arr_question[0]->id)
                ->where('status', '1')
                ->select('status')
                ->get();
        $total_followPost = DB::table('trans_question_follow_post')
                ->where('question_id_fk', $arr_question[0]->id)
                ->where('status', '0')
                ->select('status')
                ->get();

	$promotion_det = promotionModel::where('status', '1')->get();
        $promotion = array();
        if($arr_user1_data['id'] == $arr_user_data['id'])
        {
            $promotion = postPromotion::where('post_id_fk', $arr_question[0]->id)->where('status', '1')->whereraw('CURDATE() between `promo_start_date` and `promo_end_date`')->get();
        }

        return view('Frontend.ask-question.view', ['is_reported' => $is_reported,
            'promotion' => $promotion,
            'promotion_det' => $promotion_det,
            'response' => $response,            
            'is_upvotes' => $is_upvotes,
            'is_postfollower' => $is_postfollower,
            'total_upvotes' => count($total_upvotes),
            'total_downvotes' => count($total_downvotes),
            'total_followPost' => count($total_followPost),
            'total_unfollowPost' => count($total_unfollowPost),
            'related_question' => $related_question,
            'is_follower' => $is_follower,
            'is_connect' => $is_connect,
            'arr_follower' => $arr_follower,
            'array_question' => $array_question,
            'arr_user_data' => $arr_user_data,
            'arr_user1_data' => $arr_user1_data,
            'finalData' => $data,
            'qriousData' => $arr_question,
            'get_groups' => $get_groups]);
    }

    function notifyFollowingUsers($user_account, $post_id, $last_comment_id) {
        $this -> PostCommentNotificationMailfx($user_account, $post_id, $last_comment_id, 'question');
    }

    function frontCommentquestion() {
       $request=Input::all();
       // dd($request);
       $request['post_question_id']=isset($request['post_question_id'])?$request['post_question_id']:"";
        $resp=json_decode($this->commentfx('question',$request['post_question_id']));
        if($resp->error_code==0)
        {Session::flash('success_msg', 'Comment has been added successfully.');
        return redirect('view-question/' . $request['post_question_id']);
        }
        elseif($resp->error_code==1){
            return redirect(url('view-question/' . $request['post_question_id']))->withErrors($resp->errmsg)->withInput();
        }
        elseif($resp->error_code==2){
            Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
            return redirect('/view-question/-'.$request['post_question_id']);
        }
    }

  
    function followPostQuery() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $user_account = Session::get('user_account');
        $follower_user_id = $request['follower_user_id'];
        //echo '<pre>'; print_r($request); echo '</pre>';exit;

        $question_id = $request['question_id'];
        $status = $request['status'];
        $user_id = $data['user_session']['id'];

        $get_voting_data = DB::table('trans_question_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $question_id)
                ->get();

        $total_downvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '1')
                ->where('article_id_fk', $question_id)
                ->get();
        $total_downvotes = count($total_downvotes);
        $total_upvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '0')
                ->where('article_id_fk', $question_id)
                ->get();
        $total_upvotes = count($total_upvotes);

        $get_data = DB::table('trans_question_follow_post')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $question_id)
                ->get();

        if (count($get_data) == 0) {
            DB::table('trans_question_follow_post')->insert([
                ['user_id_fk' => $user_id, 'question_id_fk' => $question_id, 'status' => $status],
            ]);
        } else {
            $update = DB::table('trans_question_follow_post')
                    ->where('question_id_fk', $question_id)
                    ->where('user_id_fk', $user_id)
                    ->update(['status' => $status]);
        }
        $total_unfollowPost = DB::table('trans_question_follow_post')
                ->where('status', '1')
                ->where('question_id_fk', $question_id)
                ->get();
        $total_unfollowPost = count($total_unfollowPost);
        $total_followPost = DB::table('trans_question_follow_post')
                ->where('status', '0')
                ->where('question_id_fk', $question_id)
                ->get();
        $total_followPost = count($total_followPost);

        $get_question_data = DB::table('mst_question')
                ->where('id', $question_id)
                ->get();

        $notification_from_user_id = $data['user_session']['id'];
        // Following function defined in App\Traits\Post\PostActions
        $this -> UserFollowsPostSendNotificationEmailfx ($commonModel, 'question', $status, $get_question_data, $notification_from_user_id);

        ?>

        <button id="like" name="like" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $question_id ?>, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>

        <button id="unlike" name="attend" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $question_id ?>, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>

        <?php if (isset($status) && $status == '0') { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $question_id ?>, 1);"><i class="fa fa-foursquare"></i> Unfollow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php } else { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $question_id ?>, 0);"><i class="fa fa-foursquare"></i> Follow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php } ?>

        <?php
    }

    public function makeReport($question_id) {
        $question_id = base64_decode($question_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $request = Input::all();
        if (isset($request['report']) && ($request['report'] != '')) {
            $report = $request['report'];
        } else {
            $report = '';
        }

        if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
            $sub_group = $request['sub_group'];
            $group = group::find($sub_group);
            $group_name = $group['group_name'];
        } else {
            $sub_group = '';
            $group_name = '';
        }

        if ($report == '0') {
            $report_name = 'Junk';
        } elseif ($report == '1') {
            $report_name = 'Uninteresting';
        } elseif ($report == '2') {
            $report_name = 'Hatred or Vulgarity';
        } else {
            $report_name = '';
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_question_data = QuestionModel::find($question_id);
        $arr_user_data = User::find($user_id);
        $email = $arr_user_data['email'];
        $contact_email = $data['global']['contact_email'];
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
        $arr_admin_detail = User::where('email', $contact_email)->first();
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||ADMIN_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||PROGRAMME_TYPE||" => 'question',
            "||PROGRAMME_TITLE||" => ucwords($arr_question_data['title']),
            "||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
            "||LOGIN_LINK||" => $admin_login_link
        );
        if ($report_name != '') {
            $reserved_arr['||NAME||'] = ' as ' . $report_name;
        } elseif ($group_name != '') {
            $reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
        }
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);



        //$contents_array = array("email_conents" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']) . ' reported on ' . ucwords($arr_article_data['title']));
        if (count(DB::table('mst_make_report')->where('question_id', $question_id)->where('user_id_fk', $user_id)->get()) > 0) {

            $update = DB::table('mst_make_report')
                    ->where('question_id', $question_id)
                    ->where('user_id_fk', $user_id)
                    ->update(
                    ['report_type' => $report, 'group_id' => $sub_group]
            );


            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        } else {
            $insert = DB::table('mst_make_report')
                    ->insert(
                    ['user_id_fk' => $user_id, 'question_id' => $question_id, 'group_id' => $sub_group, 'report_type' => $report]
            );
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        }
        Session::flash('success_msg', 'Thank you for reporting us. Your report has been sent successfully.');
        echo json_encode(array('msg' => 1));
    }

    public function makeGroupReport($question_id) {
        $question_id = base64_decode($question_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $request = Input::all();
        if (isset($request['report']) && ($request['report'] != '')) {
            $report = $request['report'];
        } else {
            $report = '';
        }

        if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
            $sub_group = $request['sub_group'];
            $group = group::find($sub_group);
            $group_name = $group['group_name'];
        } else {
            $sub_group = '';
            $group_name = '';
        }

        if ($report == '0') {
            $report_name = 'Junk';
        } elseif ($report == '1') {
            $report_name = 'Uninteresting';
        } elseif ($report == '2') {
            $report_name = 'Hatred or Vulgarity';
        } else {
            $report_name = '';
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_question_data = QuestionModel::find($question_id);
        $arr_user_data = User::find($user_id);
        $email = $arr_user_data['email'];
        $contact_email = $data['global']['contact_email'];
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
        $arr_admin_detail = User::where('email', $contact_email)->first();
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||ADMIN_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||PROGRAMME_TYPE||" => 'question',
            "||PROGRAMME_TITLE||" => ucwords($arr_question_data['title']),
            "||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
            "||LOGIN_LINK||" => $admin_login_link
        );
        if ($report_name != '') {
            $reserved_arr['||NAME||'] = ' as ' . $report_name;
        } elseif ($group_name != '') {
            $reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
        }
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);

        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
            $message->from($email, $data['global']['site_title']);
            $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
        }); // End Mail

        Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully.');
        echo json_encode(array('msg' => 1));
    }

    function likeanswer($answer_id, $status) {
        $answer_id = base64_decode($answer_id);
        $status = base64_decode($status);


        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');


        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $answer_data = Answer::find($answer_id);
        $get_data = DB::table('trans_answer_like')
                ->where('user_id_fk', $user_id)
                ->where('answer_id_fk', $answer_id)
                ->get();

        if (count($get_data) > 0) {
            $arr_to_update = array("status" => $status);
            /* updating the user status  value into database */
            DB::table('trans_answer_like')
                    ->where('user_id_fk', $user_id)
                    ->where('answer_id_fk', $answer_id)
                    ->update($arr_to_update);


            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            DB::table('trans_answer_like')->insert([
                ['user_id_fk' => $user_id, 'answer_id_fk' => $answer_id, 'status' => $status],
            ]);


            echo json_encode(array('success' => 1));
        }
    }

    function frontRepost($question_id, $created_at) {
        $question_id = base64_decode($question_id);
        $created_at = base64_decode($created_at);

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = User::find($question_id);

        $get_data = DB::table('mst_question')
                ->where('id', $question_id)
                ->get();
        if (count($get_data) > 0) {
            $arr_to_update = array("created_at" => date("Y-m-d H:i:s"));

            DB::table('mst_question')
                    ->where('id', $question_id)
                    ->update($arr_to_update);
            echo json_encode(array("msg" => "0", "success_message" => "Question has been repost successfully"));
        }
    }

    function upDownVoteQuestion() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $question_id = $request['question_id'];
        $status = $request['status'];
        $user_id = $data['user_session']['id'];

        /*         * ****************for ranking purpose******************** */
        $get_group_ids = DB::table('trans_question_group as t')
                ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                ->where('question_id_fk', $question_id)
                ->select('mg.id')
                ->get();
        /*         * ** Ends Here ***** */


        $get_data = DB::table('trans_question_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $question_id)
                ->get();
        if (count($get_data) == 0) {
            DB::table('trans_question_upvote_downvote')->insert([
                ['user_id_fk' => $user_id, 'question_id_fk' => $question_id, 'status' => $status],
            ]);
            foreach ($get_group_ids as $val) {
                DB::table('vote_statistics')->insert([
                    ['group_id_fk' => $val->id, 'user_id_fk' => $user_id, 'post_id' => $question_id, 'vote' => $status],
                ]);
            }
        } else {
            $update = DB::table('trans_question_upvote_downvote')
                    ->where('question_id_fk', $question_id)
                    ->where('user_id_fk', $user_id)
                    ->update(['status' => $status]);
            foreach ($get_group_ids as $val) {
                $update = DB::table('vote_statistics')
                        ->where(['post_id' => $question_id, 'user_id_fk' => $user_id, 'group_id_fk' => $val->id])
                        ->update(['vote' => $status]);
            }
        }
        $get_data = DB::table('trans_question_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $question_id)
                ->get();
        $total_downvotes = DB::table('trans_question_upvote_downvote')
                ->where('status', '1')
                ->where('question_id_fk', $question_id)
                ->get();
        $total_downvotes = count($total_downvotes);
        $total_upvotes = DB::table('trans_question_upvote_downvote')
                ->where('status', '0')
                ->where('question_id_fk', $question_id)
                ->get();
        $total_upvotes = count($total_upvotes);
        $get_question_data = DB::table('mst_question')
                ->where('id', $question_id)
                ->get();

        $is_postfollower = DB::table('trans_question_follow_post as t')
                ->where('user_id_fk', $user_id)
                ->where('question_id_fk', $question_id)
                ->select('t.status')
                ->get();

        $total_unfollowPost = DB::table('trans_question_follow_post')
                ->where('question_id_fk', $question_id)
                ->where('status', '1')
                ->select('status')
                ->get();
        $total_followPost = DB::table('trans_question_follow_post')
                ->where('question_id_fk', $question_id)
                ->where('status', '0')
                ->select('status')
                ->get();
          $notification_from_user_id = $data['user_session']['id'];
          // Following function defined in App\Traits\UpvoteDownvote
          // Send Notification on ATG, Android, iOS and emails
          $this -> UpvoteDownvoteSendNotificationEmailfx($commonModel, 'question', $status, $get_question_data, $notification_from_user_id);
        ?>

        <button id="like" name="like" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $question_id ?>, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>
        <button id="unlike" name="attend" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $question_id ?>, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>
        <?php if (isset($is_postfollower) && count($is_postfollower) > 0 && $is_postfollower[0]->status == '0') { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
            $is_postfollower[0]->status=='0') ? 'acti-up-dw' : ''; ?>" onclick = "addFollowPost('<?php echo $get_question_data[0]->id ?>', 1, 2);">
            <i class="fa fa-foursquare"></i> Unfollow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php } else { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
            $is_postfollower[0]->status=='1') ? 'acti-up-dw' : ''; ?>" onclick = "addFollowPost('<?php echo $get_question_data[0]->id ?>', 0, 1);">
            <i class="fa fa-foursquare"></i> Follow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php } ?>

        <?php
    }

    public function send_notification($registration_id, $message = '') {
        //using fcm
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => array($registration_id),
            'data' => array("message" => $message)
        );
        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . env('GOOGLE_API_KEY'),
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
//        echo '<pre>';print_r($result);die;
        // Close connection
        curl_close($ch);
        return $result;
//        echo $result;
    }

    function upDownVoteAnswer() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $answer_id = $request['answer_id'];
        $status = $request['status'];
        $user_id = $data['user_session']['id'];

        $get_data = DB::table('trans_answer_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('answer_id_fk', $answer_id)
                ->get();
        if (count($get_data) == 0) {
            DB::table('trans_answer_upvote_downvote')->insert([
                ['user_id_fk' => $user_id, 'answer_id_fk' => $answer_id, 'status' => $status],
            ]);
        } else {
            $update = DB::table('trans_answer_upvote_downvote')
                    ->where('answer_id_fk', $answer_id)
                    ->where('user_id_fk', $user_id)
                    ->update(['status' => $status]);
        }
        echo 'true';
    }

    function chkQuestionTitle() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $question_title = $request['title'];
        $array_question = DB::table('mst_question as ma')
                ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                ->where('ma.title', $question_title)
                ->select('ma.*', 'u.user_name')
                ->get();
        if (count($array_question) > 0) {
            echo 'false';
            die;
        } else {
            echo 'true';
            die;
        }
    }

    function chkQuestionTitleE() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $title = $request['title'];
        $old_title = $request['old_title'];
        $array_question = DB::table('mst_question as ma')
                ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                ->where('ma.title', $title)
                ->select('ma.*', 'u.user_name')
                ->get();
        if (count($array_question) > 0) {
            if ($old_title == $title) {
                echo 'true';
                die;
            } else {
                echo 'false';
                die;
            }
        } else {

            echo 'true';
            die;
        }
    }

    public function savePostQuestions($request){
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
       
        $data['user_session'] = Session::get('user_account');

        if (!(empty($request))) {
            if (isset($request['posttype']) && $request['posttype'] == '0' && isset($request['socialicons']) && $request['socialicons']=='') {
                $status = '0';
            }
            if ($request['btn_post'] == '0') {
                $status = '1';
            } else {
                $status = '0';
            }
            /* ************************** */
            $random_no = rand(1000, 99999);
            $user_id = $data['user_session']['id'];
            $Question_object = QuestionModel::where('id', $request['id'])->first();

            if (count($Question_object) > 0) {
                $Question_object->user_id_fk = $user_id;
                $Question_object->title = $request['title'];
                $Question_object->description = $request['description'];
                $Question_object->tags = $request['tags'];
                $Question_object->status = $status;
            } else {
                $Question_object = new QuestionModel;
                $Question_object->user_id_fk = $user_id;
                $Question_object->title = $request['title'];
                $Question_object->description = $request['description'];
                $Question_object->tags = $request['tags'];
                $Question_object->status = $status;
            }
            if (Input::hasFile('question_pic')) {
                $image_upload_success = $this->UploadCoverImagefx(\Input::file('question_pic'), 'qrious');
                if ($image_upload_success["return_code"] == 0) {
                    $Question_object->profile_image = trim($image_upload_success["msg"]);
                } 
                else if ($image_upload_success["return_code"] == 1) {
                    Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                    return redirect('/mypost');
                }
                
                $Question_object->cover_type=0;
            }
            else{
                
                $Question_object->profile_image=$request['cover_url'];
                $Question_object->cover_type=$request['cover_type'];
            }
            $Question_object->save();
            $lastInsertId = $Question_object['id'];
            /***for insert group into group table**/
            if (isset($request['sub_groups']) && $request['sub_groups'] != '') {
                DB::table('trans_question_group')->where('question_id_fk', $lastInsertId)->delete();
                foreach ($request['sub_groups'] as $value) {
                    $Question_group = new groupQuestion;
                    $Question_group->group_id_fk = $value;
                    $Question_group->question_id_fk = $lastInsertId;
                    $Question_group->save();
                }
            }

            if ($request['btn_post'] == '0') {
                $subject = /*ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) .*/' posted Qrious - "' . $request['title'] . '"';
                $link = '/view-question' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']);
                $message = ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) .' posted Qrious - "' . $request['title'] . '"';
                $link = '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;
                $insert_data = array(
                    'notification_from' => $data['user_session']['id'],
                    'notification_type' => '7',
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                    'link' => $link,
                );

                $User_activity = new UserActivity;
                $User_activity->user_id_fk = $data['user_session']['id'];
                $User_activity->activity_id = $lastInsertId;
                $User_activity->activity_title = $subject;
                $User_activity->activity_link = $link;
                $User_activity->activity_table_name = 'mst_question';
                $User_activity->activity_action = 'posted qrious';
                $User_activity->save();
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            }
        }

        $get_data = DB::table('trans_question_follow_post')
                ->where('user_id_fk', $data['user_session']['id'])
                ->where('question_id_fk', $lastInsertId)
                ->get();
        if (count($get_data) == 0) {
            DB::table('trans_question_follow_post')->insert([
                ['user_id_fk' => $data['user_session']['id'], 'question_id_fk' => $lastInsertId, 'status' => '0'],
            ]);
        }

        if (isset($request['auto_save_flag']) == "true") {
            return $lastInsertId;
        }
        return $lastInsertId;;
    }

    public static function saveUsersGroups($request,$user){
        $user_group=$request['sub_groups'][0];
        $grouplist=array();
        $grouplist[]=$user_group;
        $groups=MakePostsPublicController::getGroupChild($user_group,$grouplist);

        foreach ($groups as $k=>$v){
            if ($v != '') {
                $groupuser = new GroupUser();
                $groupuser->group_id_fk = $v;
                $groupuser->user_id_fk = $user->id;
                $groupuser->status = '1';
                $groupuser->created_at = date("Y-m-d H:i:s");
                $groupuser->save();
                /* set default filter */
                $get_filter = DB::table('trans_user_filter')
                    ->where('user_id_fk', $user->id)
                    ->where('group_id_fk', $v)
                    ->get();
                if (!(count($get_filter) > 0)) {

                    $filter = new Filter();
                    $filter->user_id_fk = $user->id;
                    $filter->qrious = 100;
                    $filter->jobs = 100;
                    $filter->news = 100;
                    $filter->blogs = 100;
                    $filter->visibility = 100;
                    $filter->events = 100;
                    $filter->meetups = 100;
                    $filter->articles = 100;
                    $filter->education = 100;
                    $filter->group_id_fk = $v;
                    $filter->save();
                }
            }
        }
    }


}
