<?php

namespace App\Http\Controllers;

use Exception;
use DB;
use Mail;
use App\languages;
use App\global_settings;
use App\JobModel;
use App\groupArticle;
use App\commentMeetup;
use App\email_templates;
use App\email_template_macros;
use Illuminate\Http\Request;
use App\CommonModel;
use App\articleCommentModel;
use App\User;
use App\Helpers\GlobalData;
use App\trans_global_settings;
use Illuminate\Filesystem\Filesystem;
use Session;
use App\GroupUser;
use App\FollowerUser;
use Auth;
use App\Comment;
use Validator;
use App\group;
use Illuminate\Support\Facades\Input;
use App\ArticleModel;
use App\UserActivity;
use App\trans_ArticleModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Traits\Post\PostActions;
use App\Traits\sendsignupmail;
use App\postPromotion;
use App\promotionModel;
use App\Traits\commentsTrait;


class ArticleController extends Controller {
    use PostActions;
    use sendsignupmail;
    use commentsTrait;
    
    public $recordPerPage = 25;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listarticle() {

        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('11', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }

        $requestData = Input::all();
        $articleObj = DB::table('mst_article')
                ->join('mst_users', 'mst_users.id', '=', 'mst_article.user_id_fk')
                ->select('mst_article.*', 'mst_users.First_name', 'mst_users.last_name')
                ->orderBy('id', 'desc');
        if ($requestData && $requestData['search-qry'] != '') {
            $articleObj = $articleObj->whereRaw("(title like '%" . $requestData['search-qry'] . "%' or description like '%" .
                    $requestData['search-qry'] . "%' or tags like '%" . $requestData['search-qry'] . "%')");
        }
        if ($requestData && $requestData['search-status'] != '') {
            $articleObj = $articleObj->where('status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }
        $article = $articleObj->paginate($recordPerPage);

        foreach ($article as $key => $value) {
            $arr_article_comment = DB::table('mst_comments as mc')
                    ->join('trans_article_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
                    ->where('tbc.article_id_fk', $article[$key]->id)
                    ->select('mc.*', 'tbc.*')
                    ->get();
            $article_report = DB::table('mst_make_report')
                    ->where('article_id', $value->id)
                    ->get();
            $article[$key]->report_count = count($article_report);

            $article[$key]->count = count($arr_article_comment);
        }

        $site_title = 'Manage Articles';
        return view('Backend.article.list', [
            'arr_article' => $article, 'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
    }

    public function makeReport($article_id) {
        $article_id = base64_decode($article_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $request = Input::all();
        if (isset($request['report']) && (!empty($request['report']))) {
            $report = $request['report'];
        } else {
            $report = '';
        }

        if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
            $sub_group = $request['sub_group'];
            $group = group::find($sub_group);
            $group_name = $group['group_name'];
        } else {
            $sub_group = '';
            $group_name = '';
        }

        if ($report == '0') {
            $report_name = 'Junk';
        } elseif ($report == '1') {
            $report_name = 'Uninteresting';
        } elseif ($report == '2') {
            $report_name = 'Hatred or Vulgarity';
        } else {
            $report_name = 'spam';
        }
        /*         * **Notification*** */
        $get_article_data = DB::table('mst_article')
                ->where('id', $article_id)
                ->get();
        $subject = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . ' ' . strtoupper($group_name) . '"';
        $link = 'view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_article_data[0]->title) . '-' . $get_article_data[0]->id;
        $message = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . ' ' . strtoupper($group_name) . '"';
        $link = 'view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_article_data[0]->title) . '-' . $get_article_data[0]->id;
// Commenting below code where notifications are sent to the user
        /*        $insert_data = array(
          //                        'notification_from' => $user_account['id'],
          'notification_to' => $get_article_data[0]->user_id_fk,
          'notification_type' => "6",
          'subject' => $subject,
          'message' => trim($message),
          'notification_status' => 'send',
          'created_at' => date('Y-m-d H:i:s'),
          'link' => $link,
          );
          $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
          /*         * ******push Notification******** */
        $arr_to_user = User::where('id', '=', $get_article_data[0]->user_id_fk)->get();
        if ($arr_to_user[0]->device_name == "1") { //ios
            $this->ios_notificaton($arr_to_user[0]->registration_id);
        } else { //android
            $this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_article_data[0]->user_id_fk, 'article_id' => $article_id, 'flag' => 'Report')));
        }
        /*         * end*** */
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_article_data = ArticleModel::find($article_id);
        $arr_user_data = User::find($user_id);
        $email = $arr_user_data['email'];
        $contact_email = $data['global']['contact_email'];
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
        $arr_admin_detail = User::where('email', $contact_email)->first();
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||ADMIN_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||PROGRAMME_TYPE||" => 'article',
            "||PROGRAMME_TITLE||" => ucwords($arr_article_data['title']),
            "||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
            "||LOGIN_LINK||" => $admin_login_link
        );
        if ($report_name != '') {
            $reserved_arr['||NAME||'] = ' as ' . $report_name;
        } elseif ($group_name != '') {
            $reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
        }
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);



        //$contents_array = array("email_conents" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']) . ' reported on ' . ucwords($arr_article_data['title']));
        if (count(DB::table('mst_make_report')->where('article_id', $article_id)->where('user_id_fk', $user_id)->get()) > 0) {

            $update = DB::table('mst_make_report')
                    ->where('article_id', $article_id)
                    ->where('user_id_fk', $user_id)
                    ->update(
                    ['report_type' => $report, 'group_id' => $sub_group]
            );


            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        } else {
            $insert = DB::table('mst_make_report')
                    ->insert(
                    ['user_id_fk' => $user_id, 'article_id' => $article_id, 'group_id' => $sub_group, 'report_type' => $report]
            );
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        }
        Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully.');
        echo json_encode(array('msg' => 1));
    }

    public function makeGroupReport($article_id) {
        $article_id = base64_decode($article_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $request = Input::all();
        if (isset($request['report']) && $request['report'] != '') {
            $report = $request['report'];
        } else {
            $report = '';
        }

        if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
            $sub_group = $request['sub_group'];
            $group = group::find($sub_group);
            $group_name = $group['group_name'];
        } else {
            $sub_group = '';
            $group_name = '';
        }
        if ($report == '0') {
            $report_name = 'Junk';
        } elseif ($report == '1') {
            $report_name = 'Uninteresting';
        } elseif ($report == '2') {
            $report_name = 'Hatred or Vulgarity';
        } else {
            $report_name = 'Does not belong in';
        }
        /*         * **Notification*** */
        $get_article_data = DB::table('mst_article')
                ->where('id', $article_id)
                ->get();
        $subject = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . '"' . $group_name;
        $link = 'view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_article_data[0]->title) . '-' . $get_article_data[0]->id;
        $message = 'Your article "' . $get_article_data[0]->title . '" has been reported as "' . $report_name . '"' . $group_name;
        $link = 'view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_article_data[0]->title) . '-' . $get_article_data[0]->id;
// Commenting below code where notifications are sent to the user
        /*       $insert_data = array(
          //                        'notification_from' => $user_account['id'],
          'notification_to' => $get_article_data[0]->user_id_fk,
          'notification_type' => "6",
          'subject' => $subject,
          'message' => trim($message),
          'notification_status' => 'send',
          'created_at' => date('Y-m-d H:i:s'),
          'link' => $link,
          );
          $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
          /*         * ******push Notification******** */
        $arr_to_user = User::where('id', '=', $get_article_data[0]->user_id_fk)->get();
        if ($arr_to_user[0]->device_name == "1") { //ios
            $this->ios_notificaton($arr_to_user[0]->registration_id);
        } else { //android
            $this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_article_data[0]->user_id_fk, 'article_id' => $article_id, 'flag' => 'ReportWithGroup')));
        }
        /*         * end*** */
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_article_data = ArticleModel::find($article_id);
        $arr_user_data = User::find($user_id);
        $email = $arr_user_data['email'];
        $contact_email = $data['global']['contact_email'];
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
        $arr_admin_detail = User::where('email', $contact_email)->first();
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||ADMIN_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||PROGRAMME_TYPE||" => 'article',
            "||PROGRAMME_TITLE||" => ucwords($arr_article_data['title']),
            "||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
            "||LOGIN_LINK||" => $admin_login_link
        );
        if ($report_name != '') {
            $reserved_arr['||NAME||'] = ' as ' . $report_name;
        } elseif ($group_name != '') {
            $reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
        }
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);

        if (count(DB::table('mst_make_report')->where('article_id', $article_id)->where('user_id_fk', $user_id)->get()) > 0) {

            $update = DB::table('mst_make_report')
                    ->where('article_id', $article_id)
                    ->where('user_id_fk', $user_id)
                    ->update(
                    ['report_type' => $report, 'group_id' => $sub_group]
            );


            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        } else {

            $insert = DB::table('mst_make_report')
                    ->insert(
                    ['user_id_fk' => $user_id, 'article_id' => $article_id, 'group_id' => $sub_group, 'report_type' => $report]
            );

            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
            }); // End Mail
        }
        Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully.');
        echo json_encode(array('msg' => 1));
    }

    public function deletearticle() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $article_ids = $request['checkbox'];
        $is_mail_send = isset($request['send-mail']) ? $request['send-mail'] : '';
        if (!empty($article_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($article_ids) > 0) {

                foreach ($article_ids as $val) {
                    $articleData = ArticleModel::where('id', $val)->get();
                    foreach ($articleData as $art) {
                        DB::table('trans_article')->where('article_id_fk', '=', $art->id)->delete();
                        DB::table('trans_article_comments')->where('article_id_fk', '=', $art->id)->delete();
                        DB::table('trans_article_group')->where('article_id_fk', '=', $art->id)->delete();
                        DB::table('trans_article_upvote_downvote')->where('article_id_fk', '=', $art->id)->delete();


                        if (isset($is_mail_send) && $is_mail_send == 'on') {

                            $commonModel = new CommonModel();
                            $data = $commonModel->commonFunction();
                            $email_contents = email_templates::where(array('email_template_title' => 'abuse-report-to-user', 'lang_id' => '14'))->first();
                            $content = $email_contents['email_template_content'];

                            $reserved_words = array();
                            $reserved_arr = array
                                ("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
                                "||SITE_PATH||" => url('/'),
                                "||NAME||" => $art->getArticleUser[0]->first_name,
                                "||FEED_NAME||" => $art->title,
                            );

                            $macros_array_detail = array();
                            $macros_array_detail = email_template_macros::all();
                            $macros_array = array();
                            foreach ($macros_array_detail as $row) {
                                $macros_array[$row['macros']] = $row['value'];
                            }

                            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                            foreach ($reserved_words as $k => $v) {
                                $content = str_replace($k, $v, $content);
                            }
                            $contents_array = array("email_conents" => $content);


                            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.

                            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $email_contents, $art) {

                                $message->from($data['global']['site_email'],$data['global']['site_title']);

                                $message->to($art->getArticleUser[0]->email)->subject($email_contents['email_template_subject']);
                            }); // End Mail
                        }



                    }
                    ArticleModel::where('id', $val)->delete();
                }
            }
        }
        Session::flash('success_msg', 'Article info has been deleted successfully.');
        return redirect('admin/article/article-list');
    }

    public function editarticle($edit_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('11', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        $request = Input::all();
        if ((count($request) > 0)) {

                $article_object = ArticleModel::find(base64_decode($edit_id));
                $article_object->title = $request['title'];
//                $article_object->post_short_description = $request['post_short_description'];
                $article_object->description = $request['description'];
                $article_object->tags = $request['tags'];
                $article_object->status = $request['status'];

                if (isset($request['article_pic']) && !empty($request['article_pic'])) {
                    if (Input::hasFile('article_pic')) {
                        $image_upload_success = $this->UploadCoverImagefx(Input::file('article_pic'), 'article');
                       if ($image_upload_success["return_code"] == 0) {
                           $article_object->profile_image = trim($image_upload_success["msg"]);
                       } 
                       else if ($image_upload_success["return_code"] == 1) {
                           Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                           return redirect('/mypost');
                       }
       
                       $article_object->cover_type=0;
                   }
                }
                   else {
                    //Article already has a cover image
                        if (isset($request['last_profile_pic']) && (!empty($request['last_profile_pic']))) {
                            $article_object->profile_image = trim($request['last_profile_pic']);
                        } else {
                            $article_object->profile_image = $request['cover_url'];
                            $article_object->cover_type = $request['cover_type'];
                            
                        }
                    }

                $article_object->save();
                $lastInsertId = $article_object['id'];

                $get_groups = DB::table('trans_article_group as t')
                            ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                            ->where('t.article_id_fk', $lastInsertId)
                            ->select('mg.group_name', 'mg.id')
                            ->get();
                $arrArticleGroup= Array();
                foreach ($get_groups as $key => $object) {
                    $arrArticleGroup[] = (array) $object;
                }

                $article = DB::table('mst_article')
                        ->join('mst_users', 'mst_users.id', '=', 'mst_article.user_id_fk')
                        ->select('mst_article.*', 'mst_users.First_name', 'mst_users.last_name')
                        ->where('mst_article.id', base64_decode($edit_id))
                        ->get();

                $user_id = $article['0']->user_id_fk;
                $arr_users_group = DB::table('trans_group_users as t')
                        ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                        ->where('t.user_id_fk', $user_id)
                        ->groupBy('g.id')
                        ->select('g.group_name', 'g.id')
                        ->get();
                $arrUserGroup= Array();
                foreach ($arr_users_group as $key => $object) {
                    $arrUserGroup[] = (array) $object;
                }
                $arrUserGroups= Array();
                foreach ($arrUserGroup as $arrUserGP) {
                    $arrUserGroups[] = $arrUserGP['id'];
                }

                $alreadyGroup = DB::table('trans_article_group')->where('article_id_fk', $lastInsertId)->get();
                $alreadyGroups = Array();
                foreach ($alreadyGroup as $gp) {
                    $alreadyGroups[] = $gp->group_id_fk;
                }
                
                $delete = groupArticle::where('article_id_fk', $lastInsertId)->delete();

                /*                 * *****for group********* */
                if (isset($request['sub_groups']) && $request['sub_groups'] != '') {

                    $post_link = url('/').'/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $article['0']->title) . '-' . $article['0']->id;
                    
                    foreach ($arrArticleGroup as $value) {
                        if(!in_array($value['id'], $request['sub_groups'])){
                            $groupName=$value['group_name'];
                            $groupLink=url('/').'/go/'.$value['group_name'];
                            $this->send_mail_by_adminfx($user_id, 'post-group-remove', $article['0']->title, $post_link, 'article', $groupName, $groupLink);
                        }
                    }

                    foreach ($request['sub_groups'] as $value) {
                        $article_group = new groupArticle;
                        $article_group->group_id_fk = $value;
                        $article_group->article_id_fk = $lastInsertId;
                        $article_group->save();
                        if(!in_array($value, $arrUserGroups) && !in_array($value, $alreadyGroups)){
                            $groupAdd=DB::table('mst_group')->where('id', $value)->first();
                            $groupName=$groupAdd->group_name;
                            $groupLink=url('/').'/go/'.$groupAdd->group_name;
                            $this->send_mail_by_adminfx($user_id, 'post-group-add', $article['0']->title, $post_link, 'article', $groupName, $groupLink);
                        }

                    }

                } else {

                    $post_link = url('/').'/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $article['0']->title) . '-' . $article['0']->id;

                    foreach ($alreadyGroup as $removeGP) {
                        $groupRemove=DB::table('mst_group')->where('id', $removeGP->group_id_fk)->first();
                        $groupName=$groupRemove->group_name;
                        $groupLink=url('/').'/go/'.$groupRemove->group_name;
                        $this->send_mail_by_adminfx($user_id, 'post-group-remove', $article['0']->title, $post_link, 'article', $groupName, $groupLink);
                    }

                    $lastInsertId = $article_object['id'];
                    $delete = groupArticle::where('article_id_fk', $lastInsertId)->delete();
                }
                /*                 * ************* */
                Session::flash('message', 'Article details has been updated successfully.');
                return redirect()->guest('admin/article/article-list');
        } else {
            $commonModel = new CommonModel();
            $article = DB::table('mst_article')
                    ->join('mst_users', 'mst_users.id', '=', 'mst_article.user_id_fk')
                    ->select('mst_article.*', 'mst_users.First_name', 'mst_users.last_name')
                    ->where('mst_article.id', base64_decode($edit_id))
                    ->get();

            $user_id = $article['0']->user_id_fk;

            if (count($article) > 0) {

                /*                 * *** for group********* */
                foreach ($article as $key => $value) {
                    $get_groups = DB::table('trans_article_group as t')
                            ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                            ->where('t.article_id_fk', base64_decode($edit_id))
                            ->select('mg.group_name', 'mg.id')
                            ->get();


                    /*$arr_users_group = DB::table('trans_group_users as t')
                            ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                            ->where('t.user_id_fk', $user_id)
                            ->groupBy('g.id')
                            ->select('g.group_name', 'g.id')
                            ->get();*/
                    $arr_users_group=Group::select('group_name', 'id')->get();
                }
                $arr = array();
                foreach ($get_groups as $key => $object) {
                    $arr[] = (array) $object;
                }

                $arr1 = array();

                foreach ($arr as $value) {
                    $arr1[] = $value['id'];
                }
                /*                 * ******************** */
            }
            $site_title = 'Edit Article Details';
            return view('Backend.article.edit', ['arr_article' => $article, 'site_title' => $site_title, 'arr_selected_groups' => $arr1, 'user_group_data' => $arr_users_group, 'arr_selected_groups1' => $arr]);
        }
    }

    public function ViewBackendArticle($view_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('11', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        $commonModel = new CommonModel();
        $arr_article = DB::table('mst_article')
                ->join('mst_users', 'mst_users.id', '=', 'mst_article.user_id_fk')
                ->select('mst_article.*', 'mst_users.First_name', 'mst_users.last_name')
                ->where('mst_article.id', base64_decode($view_id))
                ->get();
        $get_groups = DB::table('trans_article_group as t')
                ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                ->where('article_id_fk', base64_decode($view_id))
                ->groupBy('mg.id')
                ->select('mg.group_name', 'mg.id')
                ->get();

        $site_title = 'View Article Details';
        return view('Backend.article.view', ['arr_article' => $arr_article, 'get_groups' => $get_groups, 'site_title' => $site_title]);
    }

    function listComment($article_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('11', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        $article_id = base64_decode($article_id);
        $article = DB::table('mst_article as ma')
                ->join('mst_users as mu', 'mu.id', '=', 'ma.user_id_fk')
                ->where('ma.id', $article_id)
                ->select('ma.*', 'mu.First_name', 'mu.last_name')
                ->orderBy('id', 'desc')
                ->get();
        foreach ($article as $key => $value) {
            $arr_article_comment = DB::table('mst_comments as mc')
                    ->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
                    ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                    //->where('tac.status', '1')
                    ->where('tac.article_id_fk', $article[$key]->id)
                    ->select('mc.*', 'u.user_name', 'u.profile_picture')
                    ->get();

            $article[$key]->comment = $arr_article_comment;
        }
        $site_title = 'Manage Article';
        return view('Backend.article.listComment', ['arr_article' => $article, 'site_title' => $site_title]);
    }

    function editComment($comment_id) {
        $commonModel = new CommonModel();
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('11', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }

        $input_data = Input::all();
        if (!(empty($input_data))) {
            $arr_comment = Comment::find($comment_id);
            $arr_comment->comment = $input_data['comment'];
            $arr_comment->status = $input_data['status'];
            $arr_comment->save();
            $article_comment = articleCommentModel::where('comment_id_fk', $comment_id)
                    ->update(['status' => $input_data['status']]);

            Session::flash('success_msg', 'Your comment has been update successfully.');
            return redirect()->guest('/admin/article/article-list');
        }

        $comment_id = base64_decode($comment_id);
        $arr_comment = Comment::where('id', $comment_id)->get();
        $article_comment = articleCommentModel::where('comment_id_fk', $comment_id)->get();
        $site_title = 'Edit Comment';
        return view('Backend.article.editComment', ['arr_comment' => $arr_comment, 'site_title' => $site_title]);
    }

    function deleteArticleComments(Request $request) {

        if (count($request->comment_ids) > 0) {
            foreach ($request->comment_ids as $key => $val) {

                $comment = Comment::find($val);
                $comment->delete();
                $arr_comment = articleCommentModel::where('comment_id_fk', $val)
                        ->delete();
            }
        }
        Session::flash('success_msg', 'Comment has been deleted successfully.');
        return redirect('admin/article/article-list');
    }

    public function postArticle(Request $request, $group_id = '') {
        $user_account = Session::get('user_account');
        if (isset($user_account['email_verified']) && $user_account['email_verified'] == '0') {
            $this->sendEmailVerificationMail($user_account['email'] , $user_account);
            return redirect()->back()->with('warning_msg_permanent', "Verify your email <b>".$user_account['email']."</b> to post on ATG. Check your inbox/spam. <br> <b>".$user_account['email']."</b> not your email? <a style=\"color:white\" href=\"send-feedback\"> Contact us </a>");
        }

        $group_id = base64_decode($group_id);
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }

        $data['header'] = array(
            "title" => 'Post an Article, share your story or views on ATG',
            "keywords" => 'Post an Article, share your story or views on ATG',
            "description" => 'Post an Article, share your story or views on ATG'
        );
        $user_id = $data['user_session']['id'];
        $arr_users_group = DB::table('trans_group_users as t')
                ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                ->where('user_id_fk', $user_id)
                ->groupBy('g.id')
                ->select('g.group_name', 'g.id')
                ->get();
        $arr_user_data = User::find($data['user_session']['id']);
        return view('Frontend.article.post-article')
                        ->with('title', 'Article')
                        ->with('arr_users_group', $arr_users_group)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('group_id', $group_id)
                        ->with('finalData', $data);
    }

    public function addPostArticle() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        } 
        $request = Input::all();

        $data['user_session'] = Session::get('user_account');
        if (!(empty($request))) {
            $status='0';
            if ($request['btn_post'] == '0') {
                $status = '1';
            } else {
                $status = '0';
            }

            $article_id = $request['id'];
            $Article = ArticleModel::where('id', $article_id)->first();
            $random_no = rand(100, 2147483647);
            if (isset($Article) && count($Article) > 0) {
                if(isset($Article->status) && $Article->status == '1'){
                    return 'already posted this article';
                }
                $Article->title = $request['title'];
                $Article->description = $request['description'];
                $Article->tags = $request['tags'];
                $Article->status = $status;
            } else {
                $user_id = $data['user_session']['id'];
                $Article = new ArticleModel;
                $Article->user_id_fk = $user_id;
                $Article->title = $request['title'];
                $Article->description = $request['description'];
                $Article->tags = $request['tags'];
                $Article->status = $status;
            }

            if (\Input::hasFile('article_pic')) {
                 $image_upload_success = $this->UploadCoverImagefx(\Input::file('article_pic'), 'article');
                
                if ($image_upload_success["return_code"] == 0) {
                    $Article->profile_image = $image_upload_success["msg"];
                } 
                else if ($image_upload_success["return_code"] == 1) {
                    Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                    return redirect('/mypost');
                }

                $Article->cover_type=0;
            }
            else{
                $Article->profile_image=$request['cover_url'];
                $Article->cover_type=$request['cover_type'];
            }
            
            $Article->save();
            $lastInsertId = $Article['id'];

            /*             * ******for group********* */
            $group_data = DB::table('trans_article_group')->where('article_id_fk', '=', $article_id)->delete();
            /*             * *for insert group into group table* */
            if (isset($request['sub_groups']) && $request['sub_groups'] != '') {
                foreach ($request['sub_groups'] as $value) {
                    $article_group = new groupArticle;
                    $article_group->group_id_fk = $value;
                    $article_group->article_id_fk = $lastInsertId;
                    $article_group->save();
                }
            }
            if ($request['btn_post'] == '0') {

                /*                 * ***for send notification****** */
                $subject = /*ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) . */' posted an article - "' . $request['title'] . '"';
                $link = '/view-article' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']);
                $message = ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) . ' posted an article - "' . $request['title'] . '"';
                $link = '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;

                        $User_activity = new UserActivity;
                        $User_activity->user_id_fk = $data['user_session']['id'];
                        $User_activity->activity_id = $Article->id;
                        $User_activity->activity_title = $subject;
                        $User_activity->activity_link = $link;
                        $User_activity->activity_table_name = 'mst_article';
                        $User_activity->activity_action = 'posted article';
                        $User_activity->save();


                $insert_data = array(
                    'notification_from' => $data['user_session']['id'],
                    'notification_type' => '7',
                    'subject' => $subject,
                    'message' => trim($message),
                    'notification_status' => 'send',
                    'created_at' => date('Y-m-d H:i:s'),
                    'link' => $link,
                );
                $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
            }
        }
        if (isset($request['auto_save_flag']) == "true") {
            return $Article->id;
        }

        $get_data = DB::table('trans_article_follow_post')
                ->where('user_id_fk', $data['user_session']['id'])
                ->where('article_id_fk', $lastInsertId)
                ->get();
        if (count($get_data) == 0) {
            DB::table('trans_article_follow_post')->insert([
                ['user_id_fk' => $data['user_session']['id'], 'article_id_fk' => $lastInsertId, 'status' => '0'],
            ]);
        }
        /*         * *********************** */
        if ($request['btn_post'] == '1') {
            Session::flash('success_msg', 'Article has been saved successfully.');
            return redirect('mypost');
        } else {
            Session::flash('success_msg', 'Article has been added successfully.');
            $link = '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;
            return redirect()->guest($link);       
            //return redirect('mypost');
        }
    }

    public function listFrontArticle() {
        $user = Session ::get('user_account');
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $data['header'] = array(
            "title" => 'Article Gallery',
        );
        return view('Frontend.article.list-article')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('title', 'Profile');
    }

    public function getArticleList() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $all = Input::all();

        $page_number = $all['page'];
        $total_records = $page_number * 10;
        $user_id = $data['user_session']['id'];

        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        foreach ($get_groups as $key => $value) {
            $user_group[$key] = $value->group_id_fk;
        }
        $condition = implode(',', $user_group);
        $get_connections = DB::table('trans_user_connection')
                ->where('from_user_id', $user_id)
                ->orWhere('to_user_id', $user_id)
                ->where('status', '1')
                ->select('to_user_id', 'from_user_id')
                ->get();
        $condition1 = 'null';
        if (count($get_connections) > 0) {
            $temp_array = array();
            $user_ids = array();
            foreach ($get_connections as $key => $value) {
                $user_ids[] = $value->to_user_id;
                $user_ids[] = $value->from_user_id;
                $arr_ids = array();
                foreach ($user_ids as $ids) {
                    if (!in_array($ids, $arr_ids)) {
                        $arr_ids[] = $ids;
                    }
                }
            }
            $condition1 = implode(',', $arr_ids);
        }
        $arr_article = DB::table('mst_article as e');
        $arr_article->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
        $arr_article->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id', 'left');
        $arr_article->where(['e.status' => '1']);
//                ->WhereIn('e.user_id_fk', explode(',', $condition1))
        $arr_article->whereIn('g.group_id_fk', $user_group);
        if ($all['search_text'] != '') {
            $arr_article->where('e.title', 'LIKE', '%' . trim($all['search_text']) . '%');
        }

        $arr_article->groupBy('e.id');
        $arr_article->orderBy('e.id', 'DESC');
        $arr_article->select('e.*', 'u.user_name', 'u.profile_picture as profile_picture');
        $arr_article = $arr_article->get();

        foreach ($arr_article as $key => $value) {

            $arr_article_comment = DB::table('mst_comments as mc')
                    ->join('trans_article_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
                    ->where('tec.article_id_fk', $arr_article[$key]->id)
                    ->where('mc.parent_id', '==', '0')
                    ->select('mc.*')
                    ->get();
            $arr_article[$key]->count = count($arr_article_comment);
        }

        if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
            $no = $all['slice_arr_count'];
        } else {
            $no = 0;
        }
        $arr_articles = $arr_article->slice($no, 10);
        $slice_arr_count = $no + 10;

        $total_count = count($arr_article);
        $arr_user_data = User::find($user_id);
        return view('Frontend.article.all-posts')->with('arr_all_records', $arr_articles)->with('absolute_path', $data['absolute_path'])->with('total_count', $total_count)->with('slice_arr_count', $slice_arr_count);
    }

    public function getUserArticle() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $all = Input::all();

        $page_number = $all['page'];
        $total_records = $page_number * 10;

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        $condition = '';
        $user_group = array();
        if (count($get_groups) > 0) {
            foreach ($get_groups as $key => $value) {
                $user_group[$key] = $value->group_id_fk;
            }
            $condition = implode(',', $user_group);
        }
        $arr_article = DB::table('mst_article as e');
        $arr_article->join('mst_users as u', 'e.user_id_fk', '=', 'u.id', 'left');
        $arr_article->where('user_id_fk', $user_id);
        if ($all['search_text'] != '') {
            $arr_article->where('e.title', 'LIKE', '%' . trim($all['search_text']) . '%');
        }
        $arr_article->groupBy('e.id');
        $arr_article->orderBy('e.id', 'DESC');
        $arr_article->select('e.*', 'u.user_name', 'u.profile_picture as profile_picture');
        $arr_article = $arr_article->get();

        foreach ($arr_article as $key => $value) {

            $arr_article_comment = DB::table('mst_comments as mc')
                    ->join('trans_article_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
                    ->where('tec.article_id_fk', $arr_article[$key]->id)
                    ->select('mc.*')
                    ->get();
            $arr_article[$key]->count = count($arr_article_comment);
        }
        if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
            $no = $all['slice_arr_count'];
        } else {
            $no = 0;
        }
        $arr_articles = $arr_article->slice($no, 10);
        $slice_arr_count = $no + 10;
        $total_count = count($arr_article);
        $arr_user_data = User::find($user_id);
        return view('Frontend.article.all-user-posts')->with('arr_all_records', $arr_articles)->with('absolute_path', $data['absolute_path'])->with('total_count', $total_count)->with('arr_user_data', $arr_user_data)->with('slice_arr_count', $slice_arr_count);
    }

    public function viewArticle($view_id, $response='') {

        $article_id = "$view_id";
        $id = explode('-', $article_id);
        if (count($id) > 1) {
            $article_id = $id[1];
        }

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_article = DB::table('mst_article as ma')
                ->where('id', $article_id)
                ->select('ma.*')
                ->get();
        if (count($arr_article) > 0) {
            $user_id_fk = $arr_article[0]->user_id_fk;
            $arr_user1_data = User::find($user_id_fk);
            $arr_user_data = User::find($user_id);

            $array_article = DB::table('mst_article as ma')
                    ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                    ->where('ma.id', $article_id)
                    ->select('ma.*', 'u.user_name')
                    ->get();
            $article_id = $array_article[0]->id;

            foreach ($array_article as $key => $value) {
                $article_comment = DB::table('mst_comments as mc')
                        ->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
                        ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                        ->where('tac.article_id_fk', $arr_article[0]->id)
                        ->where('mc.parent_id', '=', '0')
                        ->where('mc.r_parent_id', '=', '0')
                        ->orderBy('id', 'DESC')
                        ->select('mc.*', 'u.user_name', 'u.profile_picture', 'u.first_name', 'u.last_name', 'tac.comment_image')
                        ->get();

                /* Fetching all the comment level 1 */
                if (count($article_comment) > 0) {
                    $array_article[$key]->comment = $article_comment;
                    $array_article[$key]->count = count($article_comment);
                    foreach ($array_article[$key]->comment as $k => $replies) {
                        $article_replies = DB::table('mst_comments as mc')
                                ->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
                                ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                                ->where('tac.article_id_fk', $arr_article[0]->id)
                                ->where('mc.parent_id', $replies->id)
                                ->orderBy('id', 'DESC')
                                ->select('mc.*', 'u.user_name', 'u.profile_picture', 'tac.comment_image')
                                ->get();

                        if (count($article_replies) > 0) {
                            $replies_count = 0;
                            foreach ($article_replies as $rep) {
                                if ($rep->r_parent_id == "0" && $rep->parent_id == $replies->id) {
                                    $array_article[$key]->comment[$k]->replies[] = $rep;
                                    $replies_count++;
                                }
                            }
                            $array_article[$key]->comment[$k]->r_count = $replies_count;
                            /* Fetching all the comment replies level 2 */
                            $article_repliess = DB::table('mst_comments as mc')
                                    ->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
                                    ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                                    ->where('tac.article_id_fk', $arr_article[0]->id)
                                    ->where('mc.parent_id', $replies->id)
                                    ->where('mc.r_parent_id', '!=', '0')
                                    ->orderBy('id', 'DESC')
                                    ->select('mc.*', 'u.user_name', 'u.profile_picture', 'tac.comment_image')
                                    ->get();

                            /* Fetching replies reply level 3 */
                            foreach ($article_replies as $ks => $repliesR) {
                                $article_replies_replies = DB::table('mst_comments as mc')
                                        ->join('trans_article_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
                                        ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                                        ->where('tac.article_id_fk', $arr_article[0]->id)
                                        ->where('mc.parent_id', $replies->id)
                                        ->where('mc.r_parent_id', '!=', '0')
                                        ->where('mc.r_parent_id', $repliesR->r_parent_id)
                                        ->orderBy('id', 'DESC')
                                        ->select('mc.*', 'u.user_name', 'u.profile_picture', 'tac.comment_image')
                                        ->get();

                                if (count($article_replies_replies) > 0) {
                                    foreach ($article_replies_replies as $vals) {
                                        $array_article[$key]->comment[$k]->replies[$ks]->replied[] = $article_replies_replies;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            /*             * **************upvote Downvote********************* */
            $is_upvotes = DB::table('trans_article_upvote_downvote as t')
                    ->where('user_id_fk', $user_id)
                    ->where('article_id_fk', $article_id)
                    ->select('t.status')
                    ->get();

            $is_postfollower = DB::table('trans_article_follow_post as t')
                    ->where('user_id_fk', $user_id)
                    ->where('article_id_fk', $article_id)
                    ->select('t.status')
                    ->get();

            $total_upvotes = DB::table('trans_article_upvote_downvote')
                    ->where('article_id_fk', $article_id)
                    ->where('status', '0')
                    ->select('status')
                    ->get();

            $total_downvotes = DB::table('trans_article_upvote_downvote')
                    ->where('article_id_fk', $article_id)
                    ->where('status', '1')
                    ->select('status')
                    ->get();

            $total_unfollowPost = DB::table('trans_article_follow_post')
                    ->where('article_id_fk', $article_id)
                    ->where('status', '1')
                    ->select('status')
                    ->get();
            $total_followPost = DB::table('trans_article_follow_post')
                    ->where('article_id_fk', $article_id)
                    ->where('status', '0')
                    ->select('status')
                    ->get();

            //dd($total_downvotes);

            /*             * ********* */
            $arr_follower = DB::table('trans_follow_user')
                    ->where('user_id_fk', $user_id_fk)
                    ->select(DB::raw('count(follower_id_fk) as follower'))
                    ->get();
            $is_follower = DB::table('trans_follow_user')
                    ->where('user_id_fk', $user_id_fk)
                    ->where('follower_id_fk', $user_id)
                    ->get();

            $is_connect = DB::table('trans_user_connection')
                    ->where('from_user_id', $user_id_fk)
                    ->where('to_user_id', $user_id)
                    ->orWhere('from_user_id', $user_id)
                    ->Where('to_user_id', $user_id_fk)
                    ->get();

            $is_reported = DB::table('mst_make_report')
                    ->where('article_id', $article_id)
                    ->where('user_id_fk', $user_id)
                    ->get();

            /*             * ********for group*********** */
            /*             * **for not login**** */

            if (!isset($user_id)) {

                $arr_article = DB::table('mst_article as ma')
                        ->where('id', $article_id)
                        ->select('ma.*')
                        ->get();
                $user_id = $arr_article[0]->user_id_fk;
            }
            /*             * **end for not login**** */
            /*             * ***********Related Events********** */

            /* Get all groups of logged in user */
            $get_group = DB::table('trans_group_users')
                    ->where('user_id_fk', $user_id)
                    ->get();

            foreach ($get_group as $key => $value) {
                $user_group[$key] = $value->group_id_fk;
            }
            if (isset($user_group)) {
                $condition = implode(',', $user_group);
            } else {
                $condition = '';
            }
            $related_article = DB::table('mst_article as e')
                    ->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id')
                    ->whereIn('g.group_id_fk', explode(',', $condition))
                    ->groupBy('e.id')
                    ->where('e.id', '!=', $article_id)
                    ->limit(20)
                    ->orderBy('e.id', 'DESC')
                    ->select('e.*')
                    ->get();
            /*             * ************************************ */
            $get_groups = DB::table('trans_article_group as t')
                    ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                    ->where('article_id_fk', $article_id)
                    ->select('mg.group_name', 'mg.id', 'mg.profession')
                    ->get();
            /*             * ** Ends Here ***** */

            $data['header'] = array(
                "title" => $arr_article[0]->title . ' - Article | ATG',
                "meta_title" => $arr_article[0]->title . ' - Article | ATG',
                "meta_keywords" => $arr_article[0]->tags,
                "meta_description" => $arr_article[0]->description
            );
        } else {
            return redirect()->guest('/error');
        }

        $promotion_det = promotionModel::where('status', '1')->get();
        $promotion = array();
        if($arr_user1_data['id'] == $arr_user_data['id'])
        {
            $promotion = postPromotion::where('post_id_fk', $article_id)->where('status', '1')->whereraw('CURDATE() between `promo_start_date` and `promo_end_date`')->get();
        }

        return view('Frontend.article.view', ['is_reported' => $is_reported,
            'promotion' => $promotion,
            'promotion_det' => $promotion_det,
            'response' => $response,
	    'is_upvotes' => $is_upvotes,
            'is_postfollower' => $is_postfollower,
            'total_upvotes' => count($total_upvotes),
            'total_downvotes' => count($total_downvotes),
            'total_followPost' => count($total_followPost),
            'total_unfollowPost' => count($total_unfollowPost),
            'related_article' => $related_article,
            'is_follower' => $is_follower,
            'is_connect' => $is_connect,
            'arr_follower' => $arr_follower,
            'arr_article' => $array_article,
            'arr_user_data' => $arr_user_data,
            'arr_user1_data' => $arr_user1_data,
            'finalData' => $data,
            'articleData' => $arr_article,
            'get_groups' => $get_groups]);
    }

    public function frontEditPostArticle($edit_id) {

        $article_id = base64_decode($edit_id);
        $request = Input::all();
        if (isset($request['id']) != '') {
            $article_id = $request['id'];
        }

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }

        $arr_user_data = User::find($data['user_session']['id']);

        if (count($request) > 0) {
            if ($request['title'] != '') {

                $article_object = ArticleModel::find($article_id);
                $article_object->title = $request['title'];
                if($article_object->status == '1')
                {

                    $subject = 'updated an article - "' . $request['title'] . '"';

                    $message = ucfirst($data['user_session']['first_name']) . ' ' . ucfirst($data['user_session']['last_name']) .' updated an article - "' . $request['title'] . '"';
                    $link = '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $article_id;

                    $User_activity = new UserActivity;
                    $User_activity->user_id_fk = $data['user_session']['id'];
                    $User_activity->activity_id = $article_id;
                    $User_activity->activity_title = $subject;
                    $User_activity->activity_link = $link;
                    $User_activity->activity_table_name = 'mst_article';
                    $User_activity->activity_action = 'updated article';
                    $User_activity->save();
                }
                $article_object->status = '1';
                $article_object->description = $request['description'];
                $article_object->tags = $request['tags'];

                if (isset($request['article_pic']) && !empty($request['article_pic'])) {
                    if (Input::hasFile('article_pic')) {
                        $image_upload_success = $this->UploadCoverImagefx(Input::file('article_pic'), 'article');
                       if ($image_upload_success["return_code"] == 0) {
                           $article_object->profile_image = trim($image_upload_success["msg"]);
                       } 
                       else if ($image_upload_success["return_code"] == 1) {
                           Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                           return redirect('/mypost');
                       }
       
                       $article_object->cover_type=0;
                   }
                }
                   else {
                    //Article already has a cover image
                        if (isset($request['last_profile_pic']) && (!empty($request['last_profile_pic']))) {
                            $article_object->profile_image = trim($request['last_profile_pic']);
                        } else {
                            $article_object->profile_image = $request['cover_url'];
                            $article_object->cover_type = $request['cover_type'];
                            
                        }
                    }
                
                $article_object->save();
                $lastInsertId = $article_object['id'];
                $delete = groupArticle::where('article_id_fk', $lastInsertId)->delete();

                /*                 * ******for group********* */
                if (isset($request['sub_groups']) && $request['sub_groups'] != '') {
                    foreach ($request['sub_groups'] as $value) {
                        $article_group = new groupArticle;
                        $article_group->group_id_fk = $value;
                        $article_group->article_id_fk = $lastInsertId;
                        $article_group->save();
                    }
                } else {
                    $lastInsertId = $article_object['id'];
                    $delete = groupArticle::where('article_id_fk', $lastInsertId)->delete();
                }

                //Session::flash('success_msg', 'Article details has been updated successfully.');
                return redirect()->guest('/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $article_object['title']) . '-' . $lastInsertId);
            }
        }
        /*         * **** for group********* */
        $user_id = $data['user_session']['id'];
        $condition_to_pass = array('id' => $article_id);
        $arr_article = $commonModel->getRecords('mst_article', '*', $condition_to_pass);

        foreach ($arr_article as $key => $value) {
            $get_groups = DB::table('trans_article_group as t')
                    ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                    ->where('article_id_fk', $article_id)
                    ->select('mg.group_name', 'mg.id')
                    ->get();
            $arr_users_group = DB::table('trans_group_users as t')
                    ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
                    ->where('user_id_fk', $user_id)
                    ->groupBy('g.id')
                    ->select('g.group_name', 'g.id')
                    ->get();
        }

        $arr = array();
        foreach ($get_groups as $key => $object) {
            $arr[] = (array) $object;
        }

        $arr1 = array();
        foreach ($arr as $value) {
            $arr1[] = $value['id'];
        }

        if (isset($request['auto_save_flag']) == "true") {
            $article = DB::table('mst_article')->where('id', $article_id)->get();
            $article = end($article);
            $article = (array) $article[0];
            return $article;
        }

        $data['header'] = array(
            "title" => 'Article Edit',
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.article.edit-article')
                        ->with('arr_article', $arr_article)
                        ->with('user_group_data', $arr_users_group)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('finalData', $data)
                        ->with('arr_selected_groups', $arr1);
    }

    function frontCommentArticle(){
        $request=Input::all();
        if(!isset($request) || !isset($request['post_article_id']))
        return redirect('/user-dashboard');
        $resp=json_decode($this->commentfx('article',$request['post_article_id']));


        if($resp->error_code==0)
        {Session::flash('success_msg', 'Comment has been added successfully.');
        return redirect('view-article/' . $request['post_article_id']);
        }
        elseif($resp->error_code==1){
            return redirect(url('view-article/' . base64_encode($request['post_article_id'])))->withErrors($resp->errmsg)->withInput();
        }
        elseif($resp->error_code==2){
            Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
            return redirect('/view-article/-'.$request['post_article_id']);
        }
    }


    function frontRepost($article_id, $created_at) {
        $article_id = base64_decode($article_id);
        $created_at = base64_decode($created_at);

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = User::find($article_id);

        $get_data = DB::table('mst_article')
                ->where('id', $article_id)
                ->get();
        if (count($get_data) > 0) {
            $arr_to_update = array("created_at" => date("Y-m-d H:i:s"));

            DB::table('mst_article')
                    ->where('id', $article_id)
                    ->update($arr_to_update);


            echo json_encode(array("error" => "0", "error_message" => "Article has been repost successfully"));
        }
    }

    function upDownVoteArticle() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $article_id = $request['article_id'];
        $status = $request['status'];
        $user_id = $data['user_session']['id'];


        /*         * ****************for ranking purpose******************** */
        $get_group_ids = DB::table('trans_article_group as t')
                ->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
                ->where('article_id_fk', $article_id)
                ->select('mg.id')
                ->get();

        $get_data = DB::table('trans_article_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('article_id_fk', $article_id)
                ->get();
        if (count($get_data) == 0) {

            DB::table('trans_article_upvote_downvote')->insert([
                ['user_id_fk' => $user_id, 'article_id_fk' => $article_id, 'status' => $status],
            ]);
            foreach ($get_group_ids as $val) {
                DB::table('vote_statistics')->insert([
                    ['group_id_fk' => $val->id, 'user_id_fk' => $user_id, 'post_id' => $article_id, 'vote' => $status],
                ]);
            }
        } else {

            $update = DB::table('trans_article_upvote_downvote')
                    ->where('article_id_fk', $article_id)
                    ->where('user_id_fk', $user_id)
                    ->update(['status' => $status]);
            foreach ($get_group_ids as $val) {
                $update = DB::table('vote_statistics')
                        ->where(['post_id' => $article_id, 'user_id_fk' => $user_id, 'group_id_fk' => $val->id])
                        ->update(['vote' => $status]);
            }
        }
        $get_data = DB::table('trans_article_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('article_id_fk', $article_id)
                ->get();
        $total_downvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '1')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_downvotes = count($total_downvotes);
        $total_upvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '0')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_upvotes = count($total_upvotes);
        $get_article_data = DB::table('mst_article')
                ->where('id', $article_id)
                ->get();

        $is_postfollower = DB::table('trans_article_follow_post as t')
                ->where('user_id_fk', $user_id)
                ->where('article_id_fk', $article_id)
                ->select('t.status')
                ->get();

        $total_unfollowPost = DB::table('trans_article_follow_post')
                ->where('article_id_fk', $article_id)
                ->where('status', '1')
                ->select('status')
                ->get();
        $total_followPost = DB::table('trans_article_follow_post')
                ->where('article_id_fk', $article_id)
                ->where('status', '0')
                ->select('status')
                ->get();


        $notification_from_user_id = $data['user_session']['id'];
        // Following function defined in App\Traits\UpvoteDownvote
        // Send Notification on ATG, Android, iOS and emails
        $this -> UpvoteDownvoteSendNotificationEmailfx($commonModel, 'article', $status, $get_article_data, $notification_from_user_id);
        ?>

        <button id="like" name="like" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $article_id ?>, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>
        <button id="unlike" name="attend" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $article_id ?>, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>
        <?php if (isset($is_postfollower) && count($is_postfollower) > 0 && $is_postfollower[0]->status == '0') { ?>
            <button id="followPost" name="followPost" class="btn <?php
                    echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
                    $is_postfollower[0]->status == '0') ? 'acti-up-dw' : '';
                    ?>" onclick = "addFollowPost('<?php echo $get_article_data[0]->id ?>', 1, 2);">
                <i class="fa fa-foursquare"></i> Unfollow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php } else { ?>
            <button id="followPost" name="followPost" class="btn <?php
            echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
            $is_postfollower[0]->status == '1') ? 'acti-up-dw' : '';
            ?>" onclick = "addFollowPost('<?php echo $get_article_data[0]->id ?>', 0, 1);">
                <i class="fa fa-foursquare"></i> Follow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php } ?>
        <?php
    }

    public function send_notification($registration_id, $message = '') {
        //using fcm
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => array($registration_id),
            'data' => array("message" => $message)
        );
        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . env('GOOGLE_API_KEY'),
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
//        echo '<pre>';print_r($result);die;
        // Close connection
        curl_close($ch);
        return $result;
//        echo $result;
    }

    function followPostArticle() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $user_account = Session::get('user_account');
        $follower_user_id = $request['follower_user_id'];
        //echo '<pre>'; print_r($request); echo '</pre>';exit;

        $article_id = $request['article_id'];
        $status = $request['status'];
        $user_id = $data['user_session']['id'];

        $get_voting_data = DB::table('trans_article_upvote_downvote')
                ->where('user_id_fk', $user_id)
                ->where('article_id_fk', $article_id)
                ->get();

        $total_downvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '1')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_downvotes = count($total_downvotes);
        $total_upvotes = DB::table('trans_article_upvote_downvote')
                ->where('status', '0')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_upvotes = count($total_upvotes);

        $get_data = DB::table('trans_article_follow_post')
                ->where('user_id_fk', $user_id)
                ->where('article_id_fk', $article_id)
                ->get();
        //echo '<pre>'; print_r($request); echo '</pre>'; exit;
        if (count($get_data) == 0) {
            DB::table('trans_article_follow_post')->insert([
                ['user_id_fk' => $user_id, 'article_id_fk' => $article_id, 'status' => $status],
            ]);
        } else {
            $update = DB::table('trans_article_follow_post')
                    ->where('article_id_fk', $article_id)
                    ->where('user_id_fk', $user_id)
                    ->update(['status' => $status]);
        }
        $total_unfollowPost = DB::table('trans_article_follow_post')
                ->where('status', '1')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_unfollowPost = count($total_unfollowPost);
        $total_followPost = DB::table('trans_article_follow_post')
                ->where('status', '0')
                ->where('article_id_fk', $article_id)
                ->get();
        $total_followPost = count($total_followPost);

        $get_article_data = DB::table('mst_article')
                ->where('id', $article_id)
                ->get();

        $notification_from_user_id = $data['user_session']['id'];
        // Following function defined in App\Traits\Post\PostActions
        $this -> UserFollowsPostSendNotificationEmailfx ($commonModel, 'article', $status, $get_article_data, $notification_from_user_id);

        ?>

        <button id="like" name="like" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $article_id ?>, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>

        <button id="unlike" name="attend" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $article_id ?>, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>

            <?php if (isset($status) && $status == '0') { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $article_id ?>, 1);"><i class="fa fa-foursquare"></i> Unfollow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php } else { ?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $article_id ?>, 0);"><i class="fa fa-foursquare"></i> Follow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php } ?>

        <?php
    }

    function chkArticleTitle() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();

        $article_title = $request['title'];
        $array_article = DB::table('mst_article as ma')
                ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                ->where('ma.title', $article_title)
                ->select('ma.*', 'u.user_name')
                ->get();

        if (count($array_article) > 0) {
            echo 'false';
            die;
        } else {
            echo 'true';
            die;
        }
    }

    function chkTitleAutosave() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $title = $request['title'];
        $id = $request['id'];
        $table = $request['table'];
        $array_article = DB::table("$table as ma")
                ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                ->where('ma.id', '!=', $id)
                ->where('ma.title', $title)
                ->select('ma.*', 'u.user_name')
                ->get();

        if (count($array_article) > 0) {
            echo 'false';
            die;
        } else {
            echo 'true';
            die;
        }
    }

    function chkArticleTitleE() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $request = Input::all();
        $title = $request['title'];
        $old_title = $request['old_title'];
        echo $old_title;
        echo $title;



        if ($old_title == $title) {
            echo 'true';
            die;
        } else {
            $array_education = DB::table('mst_article as ma')
                    ->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
                    ->where('ma.title', $title)
                    ->select('ma.*', 'u.user_name')
                    ->get();
            if (count($array_education) > 0) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }

}
