<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Mail;
use DB;
use App\AdminUser;
use App\BlackList;
use App\FollowerUser;
use App\role;
use App\Filter;
use App\email_templates;
use App\email_template_macros;
use App\global_settings;
use App\trans_global_settings;
use App\SocialLink;
use App\CommonModel;
use App\UserProfileSettings;
use App\UserProfileImages;
use App\User;
use App\GroupUser;
use Session;
use App\Event;
use App\group;
use App\Visitor;
use App\meetup_model;
use App\UserActivity;
use Illuminate\Filesystem\Filesystem;
use App\Helpers\FlashMessage;
use App\Helpers\GlobalData;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Request;
use Share;
use App\UserConnection;
use App\UserEmailSchedule;
use App\Traits\UserDetails;
use App\Traits\sendsignupmail;
use App\Traits\paymentDetails;
use App\Traits\EventDetails;
use App\Traits\sendMail;
use Illuminate\Support\Facades\Storage;
use App\Traits\s3upload;
use App\Traits\pushtoweb;

class UserAccountController extends Controller {
    
    use UserDetails;
    use sendsignupmail;
    use paymentDetails;
    use EventDetails;
    use sendMail;
    use s3upload;
    use pushtoweb;

    public function sendScheduledEmail() {

        $users = UserEmailSchedule::get();
        $user_groups = array();
        $array_article = array();
        $array_event = array();
        $array_meetup = array();
        $array_education = array();
        $array_qrious = array();
        
        foreach ($users as $key => $user) {
          try {    
         
            $get_groups = DB::table('trans_group_users')
                    ->where('user_id_fk', $user->user_id)
                    ->select('group_id_fk')
                    ->get();
        
            if ($user->article == 1) {
        
                foreach ($get_groups as $grp) {
                    $arr_article = DB::table('mst_article as e');
                    $arr_article->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id', 'left');
                    $arr_article->where('g.group_id_fk', $grp->group_id_fk);
                    $arr_article = $arr_article->first();
                    $array_article[] = $arr_article;
                }
            }
        
            if ($user->event == 1) {
                foreach ($get_groups as $grp) {
                    $get_events = DB::table('mst_event as e');
                    $get_events->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
                    $get_events->where('g.group_id_fk', $grp->group_id_fk);
                    $get_events->where('status', '1');
                    $get_events = $get_events->first();
                    $array_event[] = $get_events;
                }
            }
            if ($user->meetup == 1) {

                foreach ($get_groups as $grp) {
                    $get_meetups = DB::table('mst_meetup as m');
                    $get_meetups->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
                    $get_meetups->where('g.group_id_fk', $grp->group_id_fk);
                    $get_meetups->where('status', '0');
                    /*                     * **show only posted record not saved****** */
                    $get_meetups = $get_meetups->first();
                    $array_meetup[] = $get_meetups;
                }
            }
            if ($user->education == 1) {

                foreach ($get_groups as $grp) {

                    $arr_education = DB::table('mst_education as e');
                    $arr_education->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left');
                    $arr_education->where('g.group_id_fk', $grp->group_id_fk);
                    $arr_education->where('status', '1');
                    $arr_education = $arr_education->first();
                    $array_education[] = $arr_education;
                }
            }

            if ($user->qurious == 1) {
                foreach ($get_groups as $grp) {
                    $get_qrious = DB::table('mst_question as q');
                    $get_qrious->join('trans_question_group as g', 'g.question_id_fk', '=', 'q.id', 'left');
                    $get_qrious->where('g.group_id_fk', $grp->group_id_fk);
                    $get_qrious->where('status', '0');
                    $get_qrious->select('g.group_id_fk', 'q.id', 'q.title as title', 'q.status', 'q.updated_at', 'q.created_at', 'q.description');
                    $get_qrious = $get_qrious->first();
                    $array_qrious[] = $get_qrious;
                }
            }


            $temp_array_event = array();
            foreach ($array_event as $key => $records) {
                if (isset($records)) {
                    $temp_array_event[] = $records;
                } else {
                    continue;
                }
            }
            $temp_array_qrious = array();
            foreach ($array_qrious as $key => $records0) {
                if (isset($records0)) {
                    $temp_array_qrious[] = $records0;
                } else {
                    continue;
                }
            }
            $temp_array_meetup = array();
            foreach ($array_meetup as $key => $records1) {
                if (isset($records1)) {
                    $temp_array_meetup[] = $records1;
                } else {
                    continue;
                }
            }
            $temp_array_education = array();
            foreach ($array_education as $key => $records2) {
                if (isset($records2)) {
                    $temp_array_education[] = $records2;
                } else {
                    continue;
                }
            }

            $array_event = $this->unique_multidim_array($temp_array_event, 'id');
            $array_meetup = $this->unique_multidim_array($temp_array_meetup, 'id');
            $array_qrious = $this->unique_multidim_array($temp_array_qrious, 'id');
            $array_education = $this->unique_multidim_array($temp_array_education, 'id');

            $arr_all_atricle_data = array();

            if (count($array_article) > 0) {
                foreach ($array_article as $key => $article) {
                    $arr_all_atricle_data[$key]['data'] = $article;
                    if (isset($article->id) && $article->id != '') {

                        //calculating total upvotes for the article
                        $arr_article_upvotes = DB::table('trans_article_upvote_downvote')
                                ->where('article_id_fk', $article->id)
                                ->where('status', '0')
                                ->select(DB::raw('COUNT(article_id_fk) as upvote_count'))
                                ->get();

                        $arr_all_atricle_data[$key]['upvote_count'] = isset($arr_article_upvotes[0]->upvote_count) ? $arr_article_upvotes[0]->upvote_count : '0';

                        //calculating total downvotes for the article
                        $arr_article_downvotes = DB::table('trans_article_upvote_downvote')
                                ->where('article_id_fk', $article->id)
                                ->where('status', '1')
                                ->select(DB::raw('COUNT(article_id_fk) as downvote_count'))
                                ->get();
                        $arr_all_atricle_data[$key]['downvote_count'] = isset($arr_article_downvotes[0]->downvote_count) ? $arr_article_downvotes[0]->downvote_count : '0';

                        //calculating hotne of the article from total upvotes, downvotes and posted date
                        $arr_to_return_article_hotness = $this->hotness($arr_article_upvotes[0]->upvote_count, $arr_article_downvotes[0]->downvote_count);

                        //assigning key into array with resulted hotness
                        $arr_all_atricle_data[$key]['hotness'] = $arr_to_return_article_hotness;

                        //assigning type to a array
                        $arr_all_atricle_data[$key]['type'] = 'Article';
                        $arr_all_atricle_data[$key]['url'] = url('/') . '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $article->title) . '-' . $article->id;
                    }
                }
            }


            $arr_all_event_data = array();
            if (count($array_event) > 0) {
                foreach ($array_event as $key => $events) {

                    //assign all data to a veriable
                    $arr_all_event_data[$key]['data'] = $events;
                    if (isset($events->id) && $events->id != '') {
                        //calculating total upvotes for the event
                        $arr_event_upvotes = DB::table('trans_event_upvote_downvote')
                                ->where('event_id_fk', $events->id)
                                ->where('status', '0')
                                ->select(DB::raw('COUNT(event_id_fk) as upvote_count'))
                                ->get();
                        $arr_all_event_data[$key]['upvote_count'] = $arr_event_upvotes[0]->upvote_count;


                        //calculating total downvotes for the event
                        $arr_event_downvotes = DB::table('trans_event_upvote_downvote')
                                ->where('event_id_fk', $events->id)
                                ->where('status', '1')
                                ->select(DB::raw('COUNT(event_id_fk) as downvote_count'))
                                ->get();
                        $arr_all_event_data[$key]['downvote_count'] = $arr_event_downvotes[0]->downvote_count;

                        //calculating hotness of the blog from total upvotes, downvotes and posted date
                        $arr_to_return_event_hotness = $this->hotness($arr_event_upvotes[0]->upvote_count, $arr_event_downvotes[0]->downvote_count, $events->created_at);

                        //assigning key into array with resulted hotness
                        $arr_all_event_data[$key]['hotness'] = $arr_to_return_event_hotness;

                        //assigning type to a array
                        $arr_all_event_data[$key]['type'] = 'Event';
                        $arr_all_event_data[$key]['url'] = url("/") . '/view-event/' .preg_replace('/[^A-Za-z0-9\s]/', '',  $events->title)  .'-'. $events->id;
                    }
                }
            }
            $arr_all_meetup_data = array();
            if (count($array_meetup) > 0) {
                foreach ($array_meetup as $key => $meetups) {

                    //assign all data to a veriable
                    $arr_all_meetup_data[$key]['data'] = $meetups;

                    //calculating total upvotes for the meetup
                    $arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(meetup_id_fk) as upvote_count'))
                            ->get();

                    $arr_all_meetup_data[$key]['upvote_count'] = isset($arr_meetup_upvotes[$arr_meetup_upvotes[0]->upvote_count]) ? $arr_meetup_upvotes[0]->upvote_count : '0';




                    //calculating total downvotes for the meetup
                    $arr_meetup_downvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(meetup_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_meetup_data[$key]['downvote_count'] = isset($arr_meetup_downvotes[0]->downvote_count) ? $arr_meetup_downvotes[0]->downvote_count : '0';

                    //calculating hotness of the blog from total upvotes, downvotes and posted date
                    $arr_to_return_meetup_hotness = $this->hotness($arr_meetup_upvotes[0]->upvote_count, $arr_meetup_downvotes[0]->downvote_count, $meetups->created_at);

                    //assigning key into array with resulted hotness
                    $arr_all_meetup_data[$key]['hotness'] = $arr_to_return_meetup_hotness;

                    //assigning type to a array
                    $arr_all_meetup_data[$key]['type'] = 'Meetup';
                    $arr_all_meetup_data[$key]['url'] = url("/") . '/view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $meetups->title) . '-' . $meetups->id;
                }
            }

        $arr_all_qrious_data = array();

        if (count($array_qrious) > 0) {
            foreach ($array_qrious as $key => $qrious) {

                //assign all data to a veriable
                $arr_all_qrious_data[$key]['data'] = $qrious;

                //calculating total upvotes for the qrious
                $arr_qrious_upvotes = DB::table('trans_question_upvote_downvote')
                        ->where('question_id_fk', $qrious->id)
                        ->where('status', '0')
                        ->select(DB::raw('COUNT(question_id_fk) as upvote_count'))
                        ->get();
                $arr_all_qrious_data[$key]['upvote_count'] = isset($arr_qrious_upvotes[0]->upvote_count) ? $arr_qrious_upvotes[0]->upvote_count : '0';



                //calculating total downvotes for the qrious
                $arr_qrious_downvotes = DB::table('trans_question_upvote_downvote')
                        ->where('question_id_fk', $qrious->id)
                        ->where('status', '1')
                        ->select(DB::raw('COUNT(question_id_fk) as downvote_count'))
                        ->get();
                $arr_all_qrious_data[$key]['downvote_count'] = isset($arr_qrious_downvotes[0]->downvote_count) ? $arr_qrious_downvotes[0]->downvote_count : '0';

                //calculating hotness of the blog from total upvotes, downvotes and posted date
                $arr_to_return_qrious_hotness = $this->hotness($arr_qrious_upvotes[0]->upvote_count, $arr_qrious_downvotes[0]->downvote_count, $qrious->created_at);

                //assigning key into array with resulted hotness
                $arr_all_qrious_data[$key]['hotness'] = $arr_to_return_qrious_hotness;

                //assigning type to a array
                $arr_all_qrious_data[$key]['type'] = 'Qrious';
                $arr_all_qrious_data[$key]['url'] = url("/") . '/view-question/' . preg_replace('/[^A-Za-z0-9\s]/', '', isset($qrious->title) ? $qrious->title : 'No question title' ) . '-' . $qrious->id;
            }
        }

        $arr_all_education_data = array();
        if (count($array_education) > 0) {
            foreach ($array_education as $key => $education) {

                //assign all data to a veriable
                $arr_all_education_data[$key]['data'] = $education;

                $arr_education_upvotes = DB::table('trans_education_upvote_downvote')
                        ->where('education_id_fk', $education->id)
                        ->where('status', '0')
                        ->select(DB::raw('COUNT(education_id_fk) as upvote_count'))
                        ->get();
                $arr_all_education_data[$key]['upvote_count'] = isset($arr_education_upvotes[0]->upvote_count) ? $arr_education_upvotes[0]->upvote_count : '0';

                //calculating total downvotes for the education
                $arr_education_downvotes = DB::table('trans_education_upvote_downvote')
                        ->where('education_id_fk', $education->id)
                        ->where('status', '1')
                        ->select(DB::raw('COUNT(education_id_fk) as downvote_count'))
                        ->get();
                $arr_all_education_data[$key]['downvote_count'] = isset($arr_education_downvotes[0]->downvote_count) ? $arr_education_downvotes[0]->downvote_count : '0';



                //calculating hotne of the education from total upvotes, downvotes and posted date
                $arr_to_return_education_hotness = $this->hotness($arr_education_upvotes[0]->upvote_count, $arr_education_downvotes[0]->downvote_count, $education->created_at);
                $arr_all_education_data[$key]['hotness'] = $arr_to_return_education_hotness;

                //assigning type to a array
                $arr_all_education_data[$key]['type'] = 'Education';
                $arr_all_education_data[$key]['url'] = url("/") . '/view-education/' . preg_replace('/[^A-Za-z0-9\s]/', '', isset($education->title) ? $education->title :'Title not available') . '-' . $education->id;
            }
        }


        $arr_all_records = array();

        $arr_all_records = array_merge($arr_all_atricle_data, $arr_all_event_data, $arr_all_meetup_data, $arr_all_qrious_data, $arr_all_education_data);


        $merged_hotness = array();
        foreach ($arr_all_records as $key => $records) {
            if (isset($records['data'])) {
                $merged_hotness[] = $records;
            } else {
                continue;
            }
        }
        $sort_arr = array();

        if (count($merged_hotness) > 0) {
            $sort_arr = $this->sksort($merged_hotness, 'hotness', false);
        }

         
        if (count($sort_arr) > 0) {

            $type0 = isset($sort_arr[0]['type']) ? $sort_arr[0]['type'] : '';
            $title0 = isset($sort_arr[0]['data']->title) ? $sort_arr[0]['data']->title : '';
            $url0 = isset($sort_arr[0]['url']) ? '<a href="' . $sort_arr[0]['url'] . '">View this ' . $sort_arr[0]['type'] . '</a>' : '';
            $type1 = isset($sort_arr[1]['type']) ? $sort_arr[1]['type'] : '';
            $title1 = isset($sort_arr[1]['data']->title) ? $sort_arr[1]['data']->title : '';
            $url1 = isset($sort_arr[1]['url']) ? '<a href="' . $sort_arr[1]['url'] . '">View this ' . $sort_arr[1]['type'] . '</a>' : '';
            $type2 = isset($sort_arr[2]['type']) ? $sort_arr[2]['type'] : '';
            $title2 = isset($sort_arr[2]['data']->title) ? $sort_arr[2]['data']->title : '';
            $url2 = isset($sort_arr[2]['url']) ? '<a href="' . $sort_arr[2]['url'] . '">View this ' . $sort_arr[2]['type'] . '</a>' : '';
            $type3 = isset($sort_arr[3]['type']) ? $sort_arr[3]['type'] : '';
            $title3 = isset($sort_arr[3]['data']->title) ? $sort_arr[3]['data']->title : '';
            $url3 = isset($sort_arr[3]['url']) ? '<a href="' . $sort_arr[3]['url'] . '">View this ' . $sort_arr[3]['type'] . '</a>' : '';
            $type4 = isset($sort_arr[4]['type']) ? $sort_arr[4]['type'] : '';
            $title4 = isset($sort_arr[4]['data']->title) ? $sort_arr[4]['data']->title : '';
            $url4 = isset($sort_arr[4]['url']) ? '<a href="' . $sort_arr[4]['url'] . '">View this ' . $sort_arr[4]['type'] . '</a>' : '';
            $commonModel = new CommonModel();
            $data = $commonModel->commonFunction();
            $reserved_arr = array
                ("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
                "||SITE_PATH||" => '<a href="' . url('/') . '">Go to site</a>',
                "||NAME||" => $user->getUser[0]->first_name . ' ' . $user->getUser[0]->last_name,
                "||TYPE0||" => $type0,
                "||TITLE0||" => $title0,
                "||URL0||" => $url0,
                "||TYPE1||" => $type1,
                "||TITLE1||" => $title1,
                "||URL1||" => $url1,
                "||TYPE2||" => $type2,
                "||TITLE2||" => $title2,
                "||URL2||" => $url2,
                "||TYPE3||" => $type3,
                "||TITLE3||" => $title3,
                "||URL3||" => $url3,
                "||TYPE4||" => $type4,
                "||TITLE4||" => $title4,
                "||URL4||" =>$url4,
            );
          
            if ($user->email_schedule == 0) {
                
                $this->send_email_to_user($reserved_arr, $user);
                echo $user->getUser[0]->email.'=>Email Sent'.'</br>';
                
            } elseif ($user->email_schedule == 1) {

                $today_day = date("D");
                if ($user->day == $today_day) {
                    $this->send_email_to_user($reserved_arr, $user);
                    echo $user->getUser[0]->email.'=>Email Sent'.'</br>';
                    
                } else {
                    echo $user->getUser[0]->email.'Email Sent only user set day.'.'</br>';
                }
            } else {

                $today_day = date("d");
                if ($user->date == $today_day) {
                    $this->send_email_to_user($reserved_arr, $user);
                    echo $user->getUser[0]->email.'=>Email Sent'.'</br>';
                } else {
                    echo $user->getUser[0]->email.'Email Sent only user set day.'.'</br>';
                }
            }
        }
        }catch (\Exception $e){
            \Log::alert($e);
        }
            
        }die;
        
    }

    public function send_email_to_user($reserved_arr = array(), $user = array()) {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $email_contents = email_templates::where(array('email_template_title' => 'upvoted-feeds-email-to-user', 'lang_id' => '14'))->first();
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }

        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        $contents_array = array("email_conents" => $content);


        // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        try {
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $email_contents, $user) {

                $message->from($data['global']['site_email'], $data['global']['site_title']);

                $message->to($user->getUser[0]->email)->subject($email_contents['email_template_subject']);
            });
        } catch (\Exception $e){
            \Log::alert($e);
        }
    }

    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val->$key, $key_array)) {
                $key_array[$i] = $val->$key;
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function sksort(&$array, $subkey = "id", $sort_ascending = false) {
        if (count($array))
            $temp_array[key($array)] = array_shift($array);

        foreach ($array as $key => $val) {
            $offset = 0;
            $found = false;
            foreach ($temp_array as $tmp_key => $tmp_val) {
                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                    $temp_array = array_merge((array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset)
                    );
                    $found = true;
                }
                $offset++;
            }
            if (!$found)
                $temp_array = array_merge($temp_array, array($key => $val));
        }

        if ($sort_ascending)
            $array = array_reverse($temp_array);
        else
            $array = $temp_array;
        return $array;
    }

    /*
     * Get user's profile information
     */

    public function userDashboard() {
        $user = Session ::get('user_account');

        $commonModel = new CommonModel();
        $userModel = new User();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        // Get the groups joined by the user
        $user_id = $data['user_session']['id'];
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        // if user's group count is zero, redirect to signup2 to choose groups first
        if (count($get_groups) == 0) {
            Session::flash('error_msg', 'Please select atleast one group to proceed further.');
            return redirect()->guest('/signup2');
        }
        $lang_id = 14;
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        /* Differciate date for get record for perticuler month */

        $today_date = date("Y-m-d");
        $first_date = date("Y-m-01");
        $date1 = date_create($today_date);
        $date2 = date_create($first_date);
        $diff = date_diff($date1, $date2);
        $remain_days = $diff->days;
        $today_with_minus_days = date('Y-m-d', strtotime("-$remain_days days"));

        /* Ends here */

        /* notification count */
        $arr_user_data = $userModel->find($user_id);
        $arr_user_data = $arr_user_data;


        $get_events = DB::table('mst_event as e')
                ->join('trans_event_rsvp_status as r', 'r.event_id_fk', '=', 'e.id')
                ->where('r.user_id_fk', $user_id)
                ->where('r.status', 1)
                ->where('e.start_date', '>', $today_with_minus_days)
                ->where('e.end_date', '<', $today_date)
                ->select('e.start_date')
                ->groupBy('e.id')
                ->get();


        $get_e = array();
        if (count($get_events) > 0) {
            foreach ($get_events as $object) {
                $get_e[] = (array) $object;
            }
        }

        $get_meetups = DB::table('mst_meetup as m')
                ->join('trans_meetup_rsvp_status as r', 'r.meetup_id_fk', '=', 'm.id')
                ->where('r.user_id_fk', $user_id)
                ->where('m.start_date', '>', $today_with_minus_days)
                ->where('m.start_date', '<', $today_date)
                ->where('r.status', 1)
                ->select('m.start_date')
                ->groupBy('m.id')
                ->get();


        $get_m = array();
        if (count($get_meetups) > 0) {
            foreach ($get_meetups as $object) {
                $get_m[] = (array) $object;
            }
        }

        $arr = array();
        $arr = array_merge($get_e, $get_m);
        $upcoming_going_event_count = count($arr);
        if ($upcoming_going_event_count > 0) {
            Session::put('going_count', $upcoming_going_event_count);
        } else {
            Session::forget('going_count');
        }
        /* Ends here */
        $data['header'] = array(
            "title" => 'My Home | ATG',
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.user.dashboard')
                        ->with('title', 'Profile')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data);
    }

    /** Calculates the difference between upvotes and downvotes
    Used by hotness function */
    public function vote_diff($up = 0, $down = 0) {
        return ($up - $down);
    }

    /**
    Hotness algorithm takes the no of upvotes, downvotes and CREATED_DATE
    of any post and returns a hotness number
    medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9
    */
    public function hotness($upvotes = 0, $downvotes = 0, $posted_date = 0) {

        $s = $this->vote_diff($upvotes, $downvotes);    

        if ($s > 0) {
            $y = 1;
        } elseif ($s < 0) {
            $y = -1;
        } else {
            $y = 0;
        }

        if (abs($s >= 1)) {
            $z = abs($s);
        } else {
            $z = 1;
        }

        /** 
        Addresses the corner case where posts do not have a posted date
        TODO : Give CREATED_AT in database for such posts
        Verify that none of the posts after 26.04.17 have CREATE_AT=0 
        */
        if ($posted_date == 0) { 
            $posted_date = '2016-09-29 12:08:53';
        }
    
        /** \Log::info($posted_date);
        \Log::info($epoch_updated);
        \Log::info(strtotime($posted_date)); */

        $epoch_updated = strtotime($posted_date);
        //$epoch_updated = Carbon::parse($posted_date)->format('U');
        $order = log10(max(abs($s), 1));
        $second = $epoch_updated - 1134028003 ;
        return round($y * 20 * $order + $second / 45000, 7);
    }

    public function getAllUsersForFollower() {
        $all = Input::all();
        $page_number = $all['page'];
        $total_records = $page_number;
        $user_account = Session::get('user_account');

        $arr_follow_user = DB::table('trans_follow_user as tfu')
                ->join('mst_users as mu', 'mu.id', '=', 'tfu.user_id_fk')
                ->where('tfu.follower_id_fk', $user_account['id'])
                ->select('mu.first_name', 'mu.gender', 'mu.last_name', 'mu.id', 'mu.profile_picture', 'tfu.status')
                ->orderBy('mu.profile_picture', 'DESC')
                ->limit($total_records)
                ->get();
        $arr_total_follow_user = DB::table('trans_follow_user as tfu')
                ->join('mst_users as mu', 'mu.id', '=', 'tfu.user_id_fk')
                ->where('tfu.follower_id_fk', $user_account['id'])
                ->select('mu.first_name', 'mu.gender', 'mu.last_name', 'mu.id', 'mu.profile_picture', 'tfu.status')
                ->get();

        if (count($arr_follow_user) > 0) {
            return (array('error' => '0', 'response' => $arr_follow_user, 'total_record' => count($arr_total_follow_user)));
            exit;
        }
        return (array('error' => '1'));
        exit;
    }

    public function getAllUsersForConnection() {
        $all = Input::all();
        $page_number = $all['page'];
        $total_records = $page_number;
        $user_account = Session::get('user_account');
        $arr_group_user = GroupUser::where('user_id_fk', '=', $user_account['id'])->groupBy('group_id_fk')->pluck('group_id_fk');

        $arr_all_connected_users = array();
        foreach ($arr_group_user as $group) {
            $arr_user = GroupUser::where('group_id_fk', '=', $group)
                            ->where('user_id_fk', '!=', $user_account['id'])
                            ->groupBy('user_id_fk')->pluck('user_id_fk');
            if (count($arr_user) > 0) {
                foreach ($arr_user as $user) {
                    array_push($arr_all_connected_users, $user);
                }
            }
        }
        $arr_con_users = array_unique($arr_all_connected_users);

        $arr_final_conn_user = array();

        if (count($arr_con_users) > 0) {
            $array = DB::table('mst_users')
                    ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                    ->where('id', '!=', $user_account['id'])
                    ->whereIn('id', $arr_con_users)
                    ->groupBy('id')
                    ->orderBy('profile_picture', 'DESC')
                    ->limit($total_records)
                    ->get();
            $total_array_count = DB::table('mst_users')
                    ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                    ->where('id', '!=', $user_account['id'])
                    ->whereIn('id', $arr_con_users)
                    ->groupBy('id')
                    ->orderBy('first_name', 'ASC')
                    ->get();

            return (array('error' => '0', 'response' => $array, 'total_records' => count($total_array_count)));
            exit;
        }
        return (array('error' => '1'));
        exit;
    }

    public function bkgetAllUsersForConnection() {
        $all = Input::all();
        $page_number = $all['page'];
        $total_records = $page_number;
        $user_account = Session::get('user_account');
        $arr_group_user = GroupUser::where('user_id_fk', '=', $user_account['id'])->groupBy('group_id_fk')->pluck('group_id_fk');

        $arr_all_connected_users = array();
        foreach ($arr_group_user as $group) {
            $arr_user = GroupUser::where('group_id_fk', '=', $group)
                            ->where('user_id_fk', '!=', $user_account['id'])
                            ->groupBy('user_id_fk')->pluck('user_id_fk');
            if (count($arr_user) > 0) {
                foreach ($arr_user as $user) {
                    array_push($arr_all_connected_users, $user);
                }
            }
        }
        $arr_con_users = array_unique($arr_all_connected_users);

        $arr_final_conn_user = array();
        $i = 1;
        if (count($arr_con_users) > 0) {
            foreach ($arr_con_users as $user) {
                if ($i > $total_records) {
                    break;
                }
                $array = DB::table('mst_users')
                        ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                        ->where('id', '!=', $user_account['id'])
                        ->where('id', '=', $user)
                        ->groupBy('id')
                        ->orderBy('first_name', 'ASC')
                        ->get();
                $total_array = DB::table('mst_users')
                        ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                        ->whereNotIn('id', $arr_all_connected_users)
                        ->where('id', '!=', $user_account['id'])
                        ->get();
                foreach ($array as $object) {
                    $arr_final_conn_user[] = (array) $object;
                }
                foreach ($total_array as $object) {
                    $total_array_count_user[] = (array) $object;
                }
                $i++;
            }

            return (array('error' => '0', 'response' => $arr_final_conn_user, 'total_records' => count($total_array_count_user)));
            exit;
        }
        return (array('error' => '1'));
        exit;
    }

    public function getAllUserTagging() {
        $all = Input::all();
        $user_account = Session::get('user_account');
        $user_id = $user_account['id'];

        $queryOne = DB::table('trans_user_connection as user_connection')->select(\DB::raw('to_user_id as id'))
                ->where('user_connection.status', '1')
                ->where('user_connection.from_user_id', $user_id);
        $results = DB::table('trans_user_connection as user_connection')->select(\DB::raw('from_user_id as id'))
                ->where('user_connection.status', '1')
                ->where('user_connection.to_user_id', $user_id)
                ->union($queryOne)
                ->get();

        $connectedUsers = [];
        if (count($results) > 0) {
            foreach ($results as $cUser) {
                $connectedUsers[] = $cUser->id;
            }
        }
        $array = [];
        if (count($connectedUsers) > 0) {
            $array = DB::table('mst_users')
                    ->select('id', \DB::raw('concat(first_name, " ", last_name) as name, "contact" as type'))
                    ->whereRaw(" concat(first_name, ' ', last_name) like '%" . $all['q'] . "%'")
                    ->where('id', '!=', $user_id)
                    ->whereIn('id', $connectedUsers)
                    ->orderBy('first_name', 'ASC')
                    ->limit(15)
                    ->get();
        }

        return $array;
        exit;
    }

    public function getAllUsers() {

        $all = Input::all();

        $page_number = $all['page'];

        $total_records = $page_number;
        $user_account = Session::get('user_account');
        $arr_group_user = GroupUser::where('user_id_fk', '=', $user_account['id'])->pluck('group_id_fk');
        $arr_connected_user = DB::table('trans_user_connection')
                ->where('from_user_id', $user_account['id'])
                ->orwhere('to_user_id', $user_account['id'])
                ->get();


        $arr_user_ids = array();
        if (count($arr_connected_user) > 0) {
            foreach ($arr_connected_user as $conected_users) {
                $arr_user_ids[] = $conected_users->to_user_id;
                $arr_user_ids[] = $conected_users->from_user_id;
            }
        }
        $connection = implode(',', $arr_user_ids);
        $arr_all_connected_users = array();
        foreach ($arr_group_user as $group) {
            $arr_user = GroupUser::where('group_id_fk', '=', $group)->where('user_id_fk', '!=', $user_account['id'])->whereNotIn('user_id_fk', explode(',', $connection))->groupBy('user_id_fk')->pluck('user_id_fk');
            if (count($arr_user) > 0) {
                foreach ($arr_user as $user) {
                    array_push($arr_all_connected_users, $user);
                }
            }
        }
        $arr_con_users = array_unique($arr_all_connected_users);
        $arr_final_conn_user = array();
        if (count($arr_con_users) > 0) {
            $array = DB::table('mst_users')
                    ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                    ->whereNotIn('id', $arr_all_connected_users)
                    ->whereNotIn('id', $arr_user_ids)
                    ->where('id', '!=', $user_account['id'])
                    ->where('user_type', '!=', '4')
                    ->where('user_type', '!=', '2')
                    ->orderBy('first_name', 'ASC')
                    ->limit($total_records)
                    ->get();
            $total_array = DB::table('mst_users')
                    ->select('first_name', 'gender', 'last_name', 'id', 'profile_picture')
                    ->whereNotIn('id', $arr_all_connected_users)
                    ->where('id', '!=', $user_account['id'])
                    ->get();

            foreach ($array as $object) {
                $arr_final_conn_user[] = (array) $object;
            }
            foreach ($total_array as $object) {
                $total_array_count_user[] = (array) $object;
            }

            return (array('error' => '0', 'response' => $arr_final_conn_user, 'total_records' => count($total_array_count_user)));
            exit;
        }
        return (array('error' => '1'));
        exit;
    }


    public function updateUserLocation() {
        /* Update User Location based on input from the browser, else from IP */
        $all = Input::all();
        $latitude = (isset($all['latitude']) ? $all['latitude'] : '');
        $longitude = (isset($all['longitude']) ? $all['longitude'] : ''); 
        $agent = (isset($all['agent']) ? $all['agent'] : '');
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        if ($latitude == '' && $longitude == '') {
    	    try {
    	        $ip = Request::ip();
    	        $data = \Location::get($ip);
        		$latitude = $data->latitude;
        		$longitude = $data->longitude;
                $agent = $_SERVER['HTTP_USER_AGENT'];
    	    } catch (\Exception $e) {
	           \Log::alert($e);
    	    }
        }
        $this -> UpdateUserLocationfx($user_id, 'web', $latitude, $longitude, $agent);
    }


    public function getAllPosts() {
        $all = Input::all();
        $page_number = $all['page'];
        $session_id = $all['session_id'];
        $arr_article_id = array();
        $arr_event_id = array();
        $arr_meetup_id = array();
        $arr_qrious_id = array();
        $arr_job_id = array();
        $arr_education_id = array();
        $filter_feeds_id = $all['filter_feed_id'];
        $filter_feeds_array[0] = $filter_feeds_id;

        if (isset($all['ids_and_types'])) {
            $ids_and_types = $all['ids_and_types'];
            foreach ($ids_and_types as $data) {
                if ($data['types'] == 'Article') {
                    $arr_article_id[] = $data['ids'];
                }
                if ($data['types'] == 'Event') {
                    $arr_event_id[] = $data['ids'];
                }
                if ($data['types'] == 'Meetup') {
                    $arr_meetup_id[] = $data['ids'];
                }
                if ($data['types'] == 'job') {
                    $arr_job_id[] = $data['ids'];
                }
                if ($data['types'] == 'Qrious') {
                    $arr_qrious_id[] = $data['ids'];
                }
                if ($data['types'] == 'Education') {
                    $arr_education_id[] = $data['ids'];
                }
            }
        }

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $user = User::find($user_id);
        
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();

        $user_group = array();
        if (count($get_groups) > 0) {
            foreach ($get_groups as $key => $value) {
                $user_group[] = $value->group_id_fk;
            }
            $group = implode(',', $user_group);
        }

        /*         * *******for mobile user********** */
        $get_filter = DB::table('trans_user_filter')
                ->where('user_id_fk', $user_id)
                ->whereIn('group_id_fk', $user_group)
                ->get();       

        if (count($get_filter) > 0) {
            foreach ($get_filter as $key => $value) {
                $filtered_group[] = $value->group_id_fk;
            }
        }

        if (count($get_filter) > 0) {

            $get_groups = DB::table('trans_group_users')
                    ->where('user_id_fk', $user_id)
                    ->get();

            $user_group = array();
            if (count($get_groups) > 0) {
                foreach ($get_groups as $key => $value) {
                    $user_group[] = $value->group_id_fk;
                }
                $group = implode(',', $user_group);
            }

        
        $arr_all_record = $this->HomePagePostsAlgofx($user_id, 'web', $session_id, $page_number, $filter_feeds_array, '0', $user->location, $user->latitude, $user->longitude);

        }
        return view('Frontend.user.all-posts')->with('arr_all_records', $arr_all_record)->with('absolute_path', $data['absolute_path'])->with('total_count', count($arr_all_record))->with('slice_arr_count',count($arr_all_record));
    }


    public function userTrendingPost() {
        $user = Session ::get('user_account');

        $commonModel = new CommonModel();
        $userModel = new User();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        $lang_id = 14;
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        /* Differciate date for get record for perticuler month */

        $today_date = date("Y-m-d");
        $first_date = date("Y-m-01");
        $date1 = date_create($today_date);
        $date2 = date_create($first_date);
        $diff = date_diff($date1, $date2);
        $remain_days = $diff->days;
        $today_with_minus_days = date('Y-m-d', strtotime("-$remain_days days"));

        /* Ends here */

        /* notification count */
        $arr_user_data = $userModel->find($user_id);
        $arr_user_data = $arr_user_data;

        $get_events = DB::table('mst_event as e')
                ->join('trans_event_rsvp_status as r', 'r.event_id_fk', '=', 'e.id')
                ->where('r.user_id_fk', $user_id)
                ->where('r.status', 1)
                ->where('e.start_date', '>', $today_with_minus_days)
                ->where('e.end_date', '<', $today_date)
                ->select('e.start_date')
                ->groupBy('e.id')
                ->get();

        $get_e = array();
        if (count($get_events) > 0) {
            foreach ($get_events as $object) {
                $get_e[] = (array) $object;
            }
        }

        $get_meetups = DB::table('mst_meetup as m')
                ->join('trans_meetup_rsvp_status as r', 'r.meetup_id_fk', '=', 'm.id')
                ->where('r.user_id_fk', $user_id)
                ->where('m.start_date', '>', $today_with_minus_days)
                ->where('m.start_date', '<', $today_date)
                ->where('r.status', 1)
                ->select('m.start_date')
                ->groupBy('m.id')
                ->get();

        $get_m = array();
        if (count($get_meetups) > 0) {
            foreach ($get_meetups as $object) {
                $get_m[] = (array) $object;
            }
        }

        $arr = array();
        $arr = array_merge($get_e, $get_m);
        $upcoming_going_event_count = count($arr);
        if ($upcoming_going_event_count > 0) {
            Session::put('going_count', $upcoming_going_event_count);
        } else {
            Session::forget('going_count');
        }
        /* Ends here */
        $data['header'] = array(
            "title" => 'User Trending',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.trending')
                        ->with('title', 'Trending')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getAllTrendingPosts() {
        $all = Input::all();

        $page_number = $all['page'];
        $arr_article_id = array();
        $arr_event_id = array();
        $arr_meetup_id = array();
        $arr_qrious_id = array();
        $arr_job_id = array();
        $arr_education_id = array();
        $filter_feeds_id = $all['filter_feed_id'];

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        $arr_all_atricle_data = array();
        if ($filter_feeds_id == '1' || $filter_feeds_id == '0') {

            $arr_article = DB::table('mst_article as e');
            $arr_article->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id', 'left');
            $arr_article->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');
            $arr_article->groupBy('e.id');
            //            $arr_article->limit($total_records);
            $arr_article->orderBy('e.updated_at', 'DESC');
            $arr_article->select('u.id as user_id', 'u.gender', 'u.profile_picture', 'g.group_id_fk', 'e.id', 'e.title as title', 'e.profile_image as image', 'description', 'e.updated_at', 'e.created_at');
            $arr_article = collect($arr_article->get());

            /*             * **show only posted record not saved****** */
            $arr_article = $arr_article->filter(function ($obj) {
                return $obj->status == 1;
            });
            /**             * ****** */
            if (count($arr_article) > 0) {
                foreach ($arr_article as $key => $article) {

                    //assign all data to a veriable
                    $arr_all_atricle_data[$key] = $article;

                    //calculating total upvotes for the article
                    $arr_article_upvotes = DB::table('trans_article_upvote_downvote')
                            ->where('article_id_fk', $article->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(article_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_atricle_data[$key]->upvote_count = $arr_article_upvotes[0]->upvote_count;


                    $arr_article_user_upvotes = DB::table('trans_article_upvote_downvote')
                            ->where('article_id_fk', $article->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('status')
                            ->get();
                    $arr_all_atricle_data[$key]->user_upvote_count = count($arr_article_user_upvotes);
                    $arr_article_user_downvotes = DB::table('trans_article_upvote_downvote')
                            ->where('article_id_fk', $article->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('status')
                            ->get();
                    $arr_all_atricle_data[$key]->user_downvote_count = count($arr_article_user_downvotes);


                    //calculating total downvotes for the article
                    $arr_article_downvotes = DB::table('trans_article_upvote_downvote')
                            ->where('article_id_fk', $article->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(article_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_atricle_data[$key]->downvote_count = $arr_article_downvotes[0]->downvote_count;

                    //calculating hotne of the article from total upvotes, downvotes and posted date
                    $arr_to_return_article_hotness = $this->hotness($arr_article_upvotes[0]->upvote_count, $arr_article_downvotes[0]->downvote_count, $article->created_at);

                    //assigning key into array with resulted hotness
                    $arr_all_atricle_data[$key]->hotness = $arr_to_return_article_hotness;

                    //assigning type to a array
                    $arr_all_atricle_data[$key]->type = 'Article';
                }
            }
        }

        $arr_all_education_data = array();
        if ($filter_feeds_id == '2' || $filter_feeds_id == '0') {

            $arr_education = DB::table('mst_education as e');
            $arr_education->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left');
            $arr_education->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');

            $arr_education->groupBy('e.id');
            //            $arr_education->limit($total_records);
            $arr_education->orderBy('e.updated_at', 'DESC');
            $arr_education->select('u.id as user_id', 'u.gender', 'u.profile_picture', 'g.group_id_fk', 'e.id', 'e.education_type', 'e.title as title', 'e.profile_image as image', 'e.status', 'e.description', 'e.updated_at', 'e.created_at');
            $arr_education = collect($arr_education->get());

            /*             * **show only posted record not saved****** */
            $arr_education = $arr_education->filter(function ($obj) {
                return $obj->status == 0;
            });

            /*             * ******* */
            if (count($arr_education) > 0) {
                foreach ($arr_education as $key => $education) {

                    //assign all data to a veriable
                    $arr_all_education_data[$key] = $education;

                    $arr_education_upvotes = DB::table('trans_education_upvote_downvote')
                            ->where('education_id_fk', $education->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(education_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_education_data[$key]->upvote_count = $arr_education_upvotes[0]->upvote_count;

                    //calculating total downvotes for the education
                    $arr_education_downvotes = DB::table('trans_education_upvote_downvote')
                            ->where('education_id_fk', $education->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(education_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_education_data[$key]->downvote_count = $arr_education_downvotes[0]->downvote_count;

                    $arr_education_user_upvotes = DB::table('trans_education_upvote_downvote')
                            ->where('education_id_fk', $education->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('status')
                            ->get();
                    $arr_all_education_data[$key]->user_upvote_count = count($arr_education_user_upvotes);

                    $arr_education_user_downvotes = DB::table('trans_education_upvote_downvote')
                            ->where('education_id_fk', $education->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('status')
                            ->get();
                    $arr_all_education_data[$key]->user_downvote_count = count($arr_education_user_downvotes);
                    //
                    //calculating hotne of the education from total upvotes, downvotes and posted date
                    $arr_to_return_education_hotness = $this->hotness($arr_education_upvotes[0]->upvote_count, $arr_education_downvotes[0]->downvote_count, $education->created_at);
                    $arr_all_education_data[$key]->hotness = $arr_to_return_education_hotness;

                    //assigning type to a array
                    $arr_all_education_data[$key]->type = 'Education';
                }
            }
        }
        /*         * ***for jobs***** */
        $arr_all_job_data = array();
        if ($filter_feeds_id == '5' || $filter_feeds_id == '0') {
            $arr_job = DB::table('mst_job as e');
            $arr_job->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id', 'left');
            $arr_job->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');

            $arr_job->where('e.status', '1');
            $arr_job->groupBy('e.id');
            $arr_job->orderBy('e.updated_at', 'DESC');
            //            $arr_job->limit($total_records);            
            $arr_job->select('u.id as user_id', 'u.gender', 'u.profile_picture', 'g.group_id_fk', 'e.id', 'e.job_type', 'e.title as title', 'e.status', 'e.description as description', 'e.updated_at', 'e.created_at');
            $arr_job = collect($arr_job->get());

            /*             * **show only posted record not saved****** */
            $arr_job = $arr_job->filter(function ($obj) {
                return $obj->job_type == 0;
            });

            /*             * ******* */
            if (count($arr_job) > 0) {
                foreach ($arr_job as $key => $job) {

                    //assign all data to a veriable
                    $arr_all_job_data[$key] = $job;

                    $arr_job_upvotes = DB::table('trans_job_upvote_downvote')
                            ->where('job_id_fk', $job->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(job_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_job_data[$key]->upvote_count = $arr_job_upvotes[0]->upvote_count;

                    $arr_job_downvotes = DB::table('trans_job_upvote_downvote')
                            ->where('job_id_fk', $job->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(job_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_job_data[$key]->downvote_count = $arr_job_downvotes[0]->downvote_count;

                    $arr_job_user_upvotes = DB::table('trans_job_upvote_downvote')
                            ->where('job_id_fk', $job->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('status')
                            ->get();
                    $arr_all_job_data[$key]->user_upvote_count = count($arr_job_user_upvotes);

                    $arr_job_user_downvotes = DB::table('trans_job_upvote_downvote')
                            ->where('job_id_fk', $job->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('status')
                            ->get();
                    $arr_all_job_data[$key]->user_downvote_count = count($arr_job_user_downvotes);

                    $arr_to_return_job_hotness = $this->hotness($arr_job_upvotes[0]->upvote_count, $arr_job_downvotes[0]->downvote_count, $job->created_at);

                    // assigning key into array with resulted hotness
                    $arr_all_job_data[$key]->hotness = $arr_to_return_job_hotness;

                    //assigning type to a array
                    $arr_all_job_data[$key]->type = 'Job';
                }
            }
        }


        $arr_all_event_data = array();
        if ($filter_feeds_id == '4' || $filter_feeds_id == '0') {
            $get_events = DB::table('mst_event as e');
            $get_events->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
            $get_events->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');

            $get_events->where('e.status', '1');
            $get_events->orderBy('e.updated_at', 'DESC');
            $get_events->groupBy('e.id');
            //            $get_events->limit($total_records);            
            $get_events->select('u.id as user_id', 'u.gender', 'u.profile_picture as profile_picture', 'g.group_id_fk', 'e.id', 'e.status', 'e.title as title', 'e.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'description', 'e.updated_at', 'e.created_at');

            $get_events = collect($get_events->get());
            $get_events = $get_events->filter(function ($obj) {
                return $obj->status == 0;
            });

            if (count($get_events) > 0) {
                foreach ($get_events as $key => $events) {
                    //assign all data to a veriable
                    $arr_all_event_data[$key] = $events;

                    //calculating total upvotes for the event
                    $arr_event_upvotes = DB::table('trans_event_upvote_downvote')
                            ->where('event_id_fk', $events->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(event_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_event_data[$key]->upvote_count = $arr_event_upvotes[0]->upvote_count;

                    $arr_event_user_upvotes = DB::table('trans_event_upvote_downvote')
                            ->where('event_id_fk', $events->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('status')
                            ->get();
                    $arr_all_event_data[$key]->user_upvote_count = count($arr_event_user_upvotes);

                    $arr_event_user_downvotes = DB::table('trans_event_upvote_downvote')
                            ->where('event_id_fk', $events->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('status')
                            ->get();
                    $arr_all_event_data[$key]->user_downvote_count = count($arr_event_user_downvotes);


                    //calculating total downvotes for the event
                    $arr_event_downvotes = DB::table('trans_event_upvote_downvote')
                            ->where('event_id_fk', $events->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(event_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_event_data[$key]->downvote_count = $arr_event_downvotes[0]->downvote_count;

                    //calculating hotness of the blog from total upvotes, downvotes and posted date
                    $arr_to_return_event_hotness = $this->hotness($arr_event_upvotes[0]->upvote_count, $arr_event_downvotes[0]->downvote_count, $events->created_at);

                    //assigning key into array with resulted hotness
                    $arr_all_event_data[$key]->hotness = $arr_to_return_event_hotness;

                    //assigning type to a array
                    $arr_all_event_data[$key]->type = 'Event';
                }
            }
        }

        /* Ends here */

        $arr_all_meetup_data = array();
        if ($filter_feeds_id == '3' || $filter_feeds_id == '0') {
            $get_meetups = DB::table('mst_meetup as m');
            $get_meetups->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
            $get_meetups->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');

            $get_meetups->where('m.status', '1');
            $get_meetups->orderBy('m.updated_at', 'DESC');
            $get_meetups->groupBy('m.id');
            //            $get_meetups->limit($total_records);            
            $get_meetups->select('u.id as user_id', 'u.gender', 'u.profile_picture as profile_picture', 'g.group_id_fk', 'm.id', 'm.title as title', 'm.status', 'm.profile_image as image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'm.location as location', 'description', 'm.updated_at', 'm.created_at');

            /*             * **show only posted record not saved****** */
            $get_meetups = collect($get_meetups->get());
            $get_meetups = $get_meetups->filter(function ($obj) {
                return $obj->status == 0;
            });

            if (count($get_meetups) > 0) {
                foreach ($get_meetups as $key => $meetups) {

                    //assign all data to a veriable
                    $arr_all_meetup_data[$key] = $meetups;

                    //calculating total upvotes for the meetup
                    $arr_meetup_upvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(meetup_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_meetup_data[$key]->upvote_count = $arr_meetup_upvotes[0]->upvote_count;

                    $arr_meetup_user_upvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('*')
                            ->get();
                    $arr_all_meetup_data[$key]->user_upvote_count = count($arr_meetup_user_upvotes);

                    $arr_meetup_user_downvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('*')
                            ->get();
                    $arr_all_meetup_data[$key]->user_downvote_count = count($arr_meetup_user_downvotes);
                    //calculating total downvotes for the meetup
                    $arr_meetup_downvotes = DB::table('trans_meetup_upvote_downvote')
                            ->where('meetup_id_fk', $meetups->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(meetup_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_meetup_data[$key]->downvote_count = $arr_meetup_downvotes[0]->downvote_count;

                    //calculating hotness of the blog from total upvotes, downvotes and posted date
                    $arr_to_return_meetup_hotness = $this->hotness($arr_meetup_upvotes[0]->upvote_count, $arr_meetup_downvotes[0]->downvote_count, $meetups->created_at);

                    //assigning key into array with resulted hotness
                    $arr_all_meetup_data[$key]->hotness = $arr_to_return_meetup_hotness;

                    //assigning type to a array
                    $arr_all_meetup_data[$key]->type = 'Meetup';
                }
            }
        }

        $arr_all_qrious_data = array();
        if ($filter_feeds_id == '6' || $filter_feeds_id == '0') {
            $get_qrious = DB::table('mst_question as q');
            $get_qrious->join('trans_question_group as g', 'g.question_id_fk', '=', 'q.id', 'left');
            $get_qrious->join('mst_users as u', 'q.user_id_fk', '=', 'u.id');


            $get_qrious->where('q.status', '1');
            $get_qrious->orderBy('q.updated_at', 'DESC');
            $get_qrious->groupBy('q.id');
            //            $get_qrious->limit($total_records);            
            $get_qrious->select('u.id as user_id', 'u.gender', 'q.profile_image as image', 'u.profile_picture as profile_picture', 'g.group_id_fk', 'q.id', 'q.title', 'q.status', 'q.updated_at', 'q.created_at', 'q.description');

            $get_qrious = collect($get_qrious->get());
            $get_qrious = $get_qrious->filter(function ($obj) {
                return $obj->status == 1;
            });

            if (count($get_qrious) > 0) {
                foreach ($get_qrious as $key => $qrious) {

                    //assign all data to a veriable
                    $arr_all_qrious_data[$key] = $qrious;

                    //calculating total upvotes for the qrious
                    $arr_qrious_upvotes = DB::table('trans_question_upvote_downvote')
                            ->where('question_id_fk', $qrious->id)
                            ->where('status', '0')
                            ->select(DB::raw('COUNT(question_id_fk) as upvote_count'))
                            ->get();
                    $arr_all_qrious_data[$key]->upvote_count = $arr_qrious_upvotes[0]->upvote_count;

                    $arr_question_user_upvotes = DB::table('trans_question_upvote_downvote')
                            ->where('question_id_fk', $qrious->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '0')
                            ->select('status')
                            ->get();
                    $arr_all_qrious_data[$key]->user_upvote_count = count($arr_question_user_upvotes);

                    $arr_question_user_downvotes = DB::table('trans_question_upvote_downvote')
                            ->where('question_id_fk', $qrious->id)
                            ->where('user_id_fk', $user_id)
                            ->where('status', '1')
                            ->select('status')
                            ->get();
                    $arr_all_qrious_data[$key]->user_downvote_count = count($arr_question_user_downvotes);

                    //calculating total downvotes for the qrious
                    $arr_qrious_downvotes = DB::table('trans_question_upvote_downvote')
                            ->where('question_id_fk', $qrious->id)
                            ->where('status', '1')
                            ->select(DB::raw('COUNT(question_id_fk) as downvote_count'))
                            ->get();
                    $arr_all_qrious_data[$key]->downvote_count = $arr_qrious_downvotes[0]->downvote_count;

                    //calculating hotness of the blog from total upvotes, downvotes and posted date
                    $arr_to_return_qrious_hotness = $this->hotness($arr_qrious_upvotes[0]->upvote_count, $arr_qrious_downvotes[0]->downvote_count, $qrious->created_at);

                    //assigning key into array with resulted hotness
                    $arr_all_qrious_data[$key]->hotness = $arr_to_return_qrious_hotness;

                    //assigning type to a array
                    $arr_all_qrious_data[$key]->type = 'Qrious';
                }
            }
        }

        //merging all records in single array
        $arr_all_records = array();

        $arr_all_records = array_merge($arr_all_atricle_data, $arr_all_education_data, $arr_all_event_data, $arr_all_meetup_data, $arr_all_job_data, $arr_all_qrious_data);

        //taking all hotness from all arrays
        $merged_hotness = array();
        foreach ($arr_all_records as $key => $records) {
            $merged_hotness[$key] = $records->hotness;
        }
        array_multisort($merged_hotness, SORT_DESC, $arr_all_records);
        $final_array = array();
        foreach ($arr_all_records as $key => $value) {
            $final_array[$key] = $value;
            $description = preg_replace('/<iframe.*?\/iframe>/i', '', $value->description);
            $final_array[$key]->rep_decription = substr(preg_replace('/<img[^>]+\>/i', '', $description), 0, 100);
        }
        if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
            $no = $all['slice_arr_count'];
        } else {
            $no = 0;
        }

        $arr_all_records = collect($arr_all_records);
        $arr_all_record = $arr_all_records->slice($no, 12);
        $slice_arr_count = $no + 12;


        return view('Frontend.user.all-trending-post')->with('arr_all_records', $arr_all_record)->with('absolute_path', $data['absolute_path'])->with('total_count', count($arr_all_records))->with('slice_arr_count', $slice_arr_count);
    }

    public function aasort(&$array, $key) {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va->$key;
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    public function addKey($array, $type) {
        foreach ($array as $key => $value) {
            $array[$key] = $value;
            $array[$key]->type = $type;
        }
        return $array;
    }

    public function profile() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_u_data = DB::table('mst_users as user')
                ->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
                ->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')
                ->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
                ->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')
                ->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')
                ->where('user.id', $user_id)
                ->get();
        $arr_u_follower = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
                ->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'follower.status', 'user.profile_picture', 'user.last_name')
                ->where('status', '1')
                ->where('user_id_fk', $user_id)
                ->get();

        $arr_u_following = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
                ->select('follower.user_id_fk', 'follower.status', 'user.first_name', 'user.gender', 'user.last_name', 'user.gender', 'user.profile_picture')
                ->where('follower_id_fk', $user_id)
                ->where('status', '1')
                ->get();


        $arr_u_group = DB::table('trans_group_users as group_user')
                ->leftjoin('mst_group as group', 'group.id', '=', 'group_user.group_id_fk')
                ->select('group.group_name', 'group.id', 'group_user.user_id_fk')
                ->where('user_id_fk', $user_id)
                ->where('group_user.status', '=', '1')
                ->distinct()
                ->get('group_name');

        $arr_user_group = array();

        $arr_u_cover = DB::table('trans_user_cover_photo')
                ->select('cover_photo', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get('group_name');

        $arr_u_profile_pic = DB::table('trans_user_profile_picture') 
                ->select('profile_picture', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();
        $arr_u_connection = DB::table('trans_user_connection as user_connection')
                ->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
                ->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
                ->select('to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
                ->where('user_connection.from_user_id', $user_id)
                ->orWhere('user_connection.to_user_id', $user_id)
                ->get('group_name');


        $arr_u_notification = DB::table('mst_notifications as n')
                ->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')
                ->select('n.*', 'user.first_name', 'user.last_name', 'user.profile_picture')
                ->where('n.notification_from', $user_id)
                ->where('n.read_status', '0')
//                ->take(5)
                ->orderBy('n.created_at', 'DESC')
                ->get();

        $arr_u_activity = UserActivity::where('user_id_fk',$user_id)->orderBy('id','DESC')->paginate(5);

        $jobcount = DB::table('mst_job')
                ->where('user_id_fk', $user_id)
                ->where('status','1')
                ->count();        

        $arr_user_data = array();
        foreach ($arr_u_data as $object) {
            $arr_user_data = (array) $object;
        }



        $arr_user_cover_photo = array();
        foreach ($arr_u_cover as $object) {
            $arr_user_cover_photo[] = (array) $object;
        }

        $arr_notification = array();
        foreach ($arr_u_notification as $object) {
            $arr_notification[] = (array) $object;
        }

        $arr_user_profile_picture = array();
        foreach ($arr_u_profile_pic as $object) {
            $arr_user_profile_picture[] = (array) $object;
        }



        $arr_user_connection = array();
        foreach ($arr_u_connection as $object) {
            $arr_user_connection[] = (array) $object;
        }

        $arr_user_group = array();
        foreach ($arr_u_group as $object) {
            $arr_user_group[] = (array) $object;
        }
        $arr_follower = array();
        foreach ($arr_u_follower as $object) {
            $arr_follower[] = (array) $object;
        }


        $arr_following = array();
        foreach ($arr_u_following as $object) {
            $arr_following[] = (array) $object;
        }

        $is_connect = DB::table('trans_user_connection')
                ->Where('from_user_id', $user_id)
                ->get();

        $get_user_profile_picture = DB::table('trans_user_profile_picture')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();

        $fullprofileimagepath=json_decode($this->gfp_of_profilepic($user_id));

        $page_title = '';
        if ($arr_user_data['first_name'] != '' || $arr_user_data['last_name'] != '') {
            $page_title = $arr_user_data['first_name'] . ' ' . $arr_user_data['last_name'];
        } else {
            $page_title = 'User Profile';
        }

        $data['header'] = array(
            "title" => $page_title,
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.user.profile.profile')
                        ->with('title', 'Profile')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('arr_follower', $arr_follower)
                        ->with('is_connect', $is_connect)
                        ->with('arr_notification', $arr_notification)
                        ->with('arr_user_cover_photo', $arr_user_cover_photo)
                        ->with('arr_user_profile_picture', $arr_user_profile_picture)
                        ->with('get_user_profile_picture', $get_user_profile_picture)
                        ->with('arr_user_connection', $arr_user_connection)
                        ->with('arr_following', $arr_following)
                        ->with('arr_user_group', $arr_user_group)
                        ->with('jobcount', $jobcount)
                        ->with('arr_activities', $arr_u_activity)
                        ->with('user_id', $user_id)
                        ->with('profile_pic',$fullprofileimagepath);
    }


    public function fullimgpath($user_id){
        return $this->gfp_of_profilepic($user_id);
    }

    public function SingleUserprofile($user_id) {

        if (!is_null($user_id)) {
            $userDetail = \App\User::where('user_name', $user_id)->first();
            $request = Input::all();
            if (isset($request['m'])) {
                $message = 'Great! You can now view or share your profile as atg.world/' . $user_id . '.';
                Session::put('success_msg', $message);
                return redirect(url('/' . $user_id));
            }
            if (is_array($userDetail) && count($userDetail) > 0)
                $user_id = base64_encode($userDetail->id);
        }

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            //return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $session_user_id = $data['user_session']['id'];

        $arr_seesion_u_data = DB::table('mst_users as user')
                ->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
                ->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')
                ->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
                ->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')
                ->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')
                ->where('user.id', $session_user_id)
                ->get();

        $user_id = base64_decode($user_id);


        $arr_u_data = DB::table('mst_users as user')
                ->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
                ->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')
                ->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
                ->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')
                ->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')
                ->where('user.id', $user_id)
                ->get();
        $arr_u_follower = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
                ->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'user.profile_picture', 'user.last_name')
                ->where('user_id_fk', $user_id)
                ->get();

        $arr_u_following = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
                ->select('follower.user_id_fk', 'user.first_name', 'user.gender', 'user.last_name', 'user.profile_picture')
                ->where('follower_id_fk', $user_id)
                ->get();

        $arr_u_group = DB::table('trans_group_users as group_user')
                ->leftjoin('mst_group as group', 'group.id', '=', 'group_user.group_id_fk')
                ->select('group.group_name', 'group.id', 'group_user.user_id_fk')
                ->where('user_id_fk', $user_id)
                ->distinct()
                ->get('group_name');
        $arr_user_group = array();

        $arr_u_cover = DB::table('trans_user_cover_photo')
                ->select('cover_photo', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get('group_name');

        $arr_u_profile_pic = DB::table('trans_user_profile_picture')
                ->select('profile_picture', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();
        $arr_u_connection = DB::table('trans_user_connection as user_connection')
                ->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
                ->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
                ->select('to_user.first_name as to_first_name', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
                ->where('user_connection.status', '1')
                ->where('user_connection.to_user_id', $user_id)
                ->orWhere('user_connection.from_user_id', $user_id)
                ->get('group_name');

        $arr_u_notification = DB::table('mst_notifications as n')
                ->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')
                ->select('n.*', 'user.first_name', 'user.last_name', 'user.profile_picture')
                ->where('n.notification_from', $user_id)
                ->where('n.read_status', '0')
                ->take(5)
                ->orderBy('n.created_at', 'DESC')
                ->get();

        $jobcount = DB::table('mst_job')
                ->where('user_id_fk', $user_id)
                ->where('status','=','1')
                ->whereRaw('application_deadline >= CURDATE()')
                ->count();

        $arr_u_activity = UserActivity::where('user_id_fk',$user_id)->orderBy('id','DESC')->paginate(5);

        $arr_user_data = array();
        foreach ($arr_seesion_u_data as $object) {
            $arr_user_data = (array) $object;
        }

        $arr_public_user_data = array();
        foreach ($arr_u_data as $object) {
            $arr_public_user_data = (array) $object;
        }

        $arr_user_cover_photo = array();
        foreach ($arr_u_cover as $object) {
            $arr_user_cover_photo[] = (array) $object;
        }

        $arr_notification = array();
        foreach ($arr_u_notification as $object) {
            $arr_notification[] = (array) $object;
        }

        $arr_user_profile_picture = array();
        foreach ($arr_u_profile_pic as $object) {
            $arr_user_profile_picture[] = (array) $object;
        }

        $arr_user_connection = array();
        foreach ($arr_u_connection as $object) {
            $arr_user_connection[] = (array) $object;
        }

        $arr_user_group = array();
        foreach ($arr_u_group as $object) {
            $arr_user_group[] = (array) $object;
        }
        $arr_follower = array();
        foreach ($arr_u_follower as $object) {
            $arr_follower[] = (array) $object;
        }

        $arr_following = array();
        foreach ($arr_u_following as $object) {
            $arr_following[] = (array) $object;
        }
        /*         * **********For follow,unfollow,mesage button********** */
        $is_follower = DB::table('trans_follow_user')
                ->where('user_id_fk', $user_id)
                ->where('follower_id_fk', $session_user_id)
                ->get();

        $is_connect1 = DB::table('trans_user_connection');
        $where = "(to_user_id = '$user_id' AND from_user_id = '$session_user_id') OR (to_user_id = '$session_user_id' AND from_user_id = '$user_id')";
        $is_connect1->whereRaw($where);
        $is_connect1 = $is_connect1->get();

        $is_connect = array();
        foreach ($is_connect1 as $object) {
            $is_connect[] = (array) $object;
        }

        $get_user_profile_picture = DB::table('trans_user_profile_picture')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();

        $page_title = '';

        if (isset($arr_public_user_data['first_name']) || isset($arr_public_user_data['last_name'])) {
            if ($arr_public_user_data['first_name'] != '' || $arr_public_user_data['last_name'] != '') {
                $page_title = $arr_public_user_data['first_name'] . ' ' . $arr_public_user_data['last_name'] . ' | ATG';
            } else {
                $page_title = 'User Profile | ATG';
            }
        } else {
            $page_title = 'User Profile';
        }

        $arr_public_user_data = (array) $arr_public_user_data;

        $data['header'] = array(
            "title" => $page_title,
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.user.profile.user-profile')
                        ->with('title', 'Profile')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('arr_public_user_data', $arr_public_user_data)
                        ->with('arr_follower', $arr_follower)
                        ->with('arr_notification', $arr_notification)
                        ->with('arr_user_cover_photo', $arr_user_cover_photo)
                        ->with('arr_user_profile_picture', $arr_user_profile_picture)
                        ->with('arr_user_connection', $arr_user_connection)
                        ->with('arr_following', $arr_following)
                        ->with('arr_user_group', $arr_user_group)
                        ->with('is_connect', $is_connect)
                        ->with('get_user_profile_picture', $get_user_profile_picture)
                        ->with('is_follower', $is_follower)
                        ->with('user_id', $user_id)
                        ->with('jobcount', $jobcount)
                        ->with('arr_activities', $arr_u_activity)
                        ->with('session_user_id', $session_user_id);
    }

    public function removeSocialConnection($social_network) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $session_user_id = $data['user_session']['id'];

        $arr_seesion_u_data = DB::table('mst_users as user')
                ->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
                ->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')
                ->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
                ->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')
                ->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')
                ->where('user.id', $session_user_id)
                ->get();

        switch ($social_network) {
            case 'fb':
                $update_data = array(
                    'fb_id' => '',
                );
                $commonModel->updateRow("mst_users", $update_data, array('id' => $session_user_id));
                $user = User::find($session_user_id);
                return redirect(url('edit-user-bio'));
                break;
            case 'twitter':
                $update_data = array(
                    'twitter_id' => '',
                );
                $commonModel->updateRow("mst_users", $update_data, array('id' => $session_user_id));
                $user = User::find($session_user_id);
                return redirect(url('edit-user-bio'));
                break;
            case 'linkdin':
                $update_data = array(
                    'linkdin_id' => '',
                );
                $commonModel->updateRow("mst_users", $update_data, array('id' => $session_user_id));
                $user = User::find($session_user_id);
                return redirect(url('edit-user-bio'));
                break;
            case 'google':
                $update_data = array(
                    'google_id' => '',
                );
                $commonModel->updateRow("mst_users", $update_data, array('id' => $session_user_id));
                $user = User::find($session_user_id);
                return redirect(url('edit-user-bio'));
                break;
            default : {
                    return redirect(url('edit-user-bio'));
                    break;
                }
        }
        return redirect(url('edit-user-bio'));
    }

    public function userBio() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];

        $arr_u_data = DB::table('mst_users as u')
                ->leftjoin('mst_hair_color as hc', 'hc.id', '=', 'u.hair_id')
                ->leftjoin('mst_eye_color as ec', 'ec.id', '=', 'u.eye_id')
                ->leftjoin('mst_ethnicity_type as et', 'et.id', '=', 'u.ethnicity_id')
                ->leftjoin('mst_body_type as bt', 'bt.id', '=', 'u.body_id')
                ->select('u.*', 'bt.body_type', 'hc.hair_color', 'ec.eye_color', 'et.ethnicity_type')
                ->where('u.id', $user_id)
                ->get();

        $arr_user_data = array();
        foreach ($arr_u_data as $object) {
            $arr_user_data = (array) $object;
        }

        $arr_u_follower = DB::table('trans_follow_user as follower')
                ->leftjoin('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
                ->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'user.profile_picture', 'user.last_name')
                ->where('user_id_fk', $user_id)
                ->get();

        $arr_follower = array();
        foreach ($arr_u_follower as $object) {
            $arr_follower[] = (array) $object;
        }

        $data['header'] = array(
            "title" => 'User Bio',
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.user.profile.bio')
                        ->with('title', 'Profile')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('arr_follower', $arr_follower)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function editUserBio() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        
        
        $transaction_count = $this->getIfTransactionHistory($user_id);
        
        if($arr_user_data->fn_updated != '')
        {
            $fn_updated = date('Y-m-d',strtotime($arr_user_data->fn_updated));
            $cur_date = date('Y-m-d');
            $datetime1 = date_create($fn_updated);
            $datetime2 = date_create($cur_date);
            $interval = date_diff($datetime1, $datetime2);
            $date_diff =  $interval->format('%m');
            if($date_diff >= 6)
            {
                $data['fn_update'] = 'enabled';

                $data['fn_date_diff'] = $date_diff;
            }
            else
            {
                $data['fn_update'] = 'disabled';
                
                $data['fn_date_diff'] = 'You last updated your firstname on '.date('d F, Y',strtotime($arr_user_data->fn_updated)).'. You can not edit it again before '.date('d F, Y', strtotime("+6 months", strtotime($arr_user_data->fn_updated))).'.';
            }
        }
        else
        {
            $data['fn_update'] = 'enabled';
            $data['fn_date_diff'] = 0;
        }  
        if($arr_user_data->ln_updated != '')
        {
            $ln_updated = date('Y-m-d',strtotime($arr_user_data->ln_updated));
            $cur_date = date('Y-m-d');
            $datetime1 = date_create($ln_updated);
            $datetime2 = date_create($cur_date);
            $interval = date_diff($datetime1, $datetime2);
            $date_diff =  $interval->format('%m');
            if($date_diff >= 6)
            {
                $data['ln_update'] = 'enabled';

                $data['ln_date_diff'] = $date_diff;
            }
            else
            {
                $data['ln_update'] = 'disabled';
                
                $data['ln_date_diff'] = 'You last updated your firstname on '.date('d F, Y',strtotime($arr_user_data->ln_updated)).'. You can not edit it again before '.date('d F, Y', strtotime("+6 months", strtotime($arr_user_data->ln_updated))).'.';
            }
        }
        else
        {
            $data['ln_update'] = 'enabled';
            $data['ln_date_diff'] = 0;
        }   

        if($arr_user_data->ut_updated != '')
        {
            $ut_updated = date('Y-m-d',strtotime($arr_user_data->ut_updated));
            $cur_date = date('Y-m-d');
            $datetime1 = date_create($ut_updated);
            $datetime2 = date_create($cur_date);
            $interval = date_diff($datetime1, $datetime2);
            $date_diff =  $interval->format('%m');
            if($date_diff >= 6)
            {
                $data['ut_update'] = 'enabled';

                $data['ut_date_diff'] = $date_diff;
            }
            else
            {
                $data['ut_update'] = 'disabled';
                
                $data['ut_date_diff'] = 'You last updated your firstname on '.date('d F, Y',strtotime($arr_user_data->ut_updated)).'. You can not edit it again before '.date('d F, Y', strtotime("+6 months", strtotime($arr_user_data->ut_updated))).'.';//6-$date_diff;
            }
        }
        else
        {
            $data['ut_update'] = 'enabled';
            $data['ut_date_diff'] = 0;
        } 

        $data['header'] = array(
            'title' => 'Profile Settings',
        );

        return view('Frontend.user.profile.profile-setting', ['transaction_count' => $transaction_count,'finalData' => $data, 'arr_user_data' => $arr_user_data]);
    }

    public function getBio() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
            
        $transaction_count = $this->getIfTransactionHistory($user_id);
        
        $arr_ethnicity_type = DB::table('mst_ethnicity_type')
                ->select(array('id', 'ethnicity_type'))
                ->get();
        $arr_body_type = DB::table('mst_body_type')
                ->select(array('id', 'body_type'))
                ->get();
        $arr_eye_color = DB::table('mst_eye_color')
                ->select(array('id', 'eye_color'))
                ->get();
        $arr_hair_color = DB::table('mst_hair_color')
                ->select(array('id', 'hair_color'))
                ->get();

        $data['header'] = array(
            "title" => 'Account Setting',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.bio-setting')
                        ->with('transaction_count', $transaction_count)
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('arr_ethnicity_type', $arr_ethnicity_type)
                        ->with('arr_body_type', $arr_body_type)
                        ->with('arr_eye_color', $arr_eye_color)
                        ->with('arr_hair_color', $arr_hair_color)
                        ->with('arr_user_data', $arr_user_data);
    }
    
    
    
    

    public function chkEditEmailDuplicate() {
        $commonModel = new CommonModel();
        $all = Input::all();
        if (!empty($all)) {
            if ($all['user_email'] == $all['user_email_old']) {
                echo 'true';
            } else {
                $table_to_pass = 'mst_users';
                $condition_to_pass = array("user_email" => $all['user_email']);
                $arr_login_data = $commonModel->getRecords($table_to_pass, '*', $condition_to_pass);
                if (count($arr_login_data)) {
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
        }
    }

    /*
     * Check username for dupliation 
     */

    public function chkEditUsernameDuplicate() {
        $commonModel = new CommonModel();
        $all = Input::all();
        if (!empty($all)) {
            if ($all['user_name'] == $all['user_name_old']) {
                echo 'true';
            } else {
                $table_to_pass = 'mst_users';
                $fields_to_pass = array('user_id', 'user_name');
                $condition_to_pass = array("user_email" => $all['user_name']);
                $arr_login_data = $commonModel->getRecords($table_to_pass, '*', $condition_to_pass);
                if (count($arr_login_data)) {
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
        }
    }

    /*
     * Change user's account setting
     */

    public function accountSetting() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect('signin');
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $request = Input::all();

        $user_new_username = Input::get('user_name');

        $blacklist = BlackList::get();
        $blacklist_array = array();
        foreach ($blacklist as $k => $val) {
            $blacklist_array[$k] = $val['attributes']['blacklist_name'];
        }
        if (in_array($user_new_username, $blacklist_array)) {
            Session::flash('error_msg', 'You cannot use this word as a Username.');
            return redirect()->back();
        }

        
            
                $user = User::find($user_id);
                if($request['user_type']!= '2')
                {
                    if($user->fn_updated == '')
                    {
                        if($user->first_name != $request['first_name'])
                        {
                            $user->fn_updated = date('Y-m-d H:i:s');
                            $user->first_name = $request['first_name'];
                        }
                    }
                    else
                    {
                        $fn_updated = date('Y-m-d',strtotime($user->fn_updated));
                        $cur_date = date('Y-m-d');
                        $datetime1 = date_create($fn_updated);
                        $datetime2 = date_create($cur_date);
                        $interval = date_diff($datetime1, $datetime2);
                        $date_diff =  $interval->format('%m');
                        if($date_diff >= 6 && $user->first_name != $request['first_name'])
                        {
                            $user->fn_updated = date('Y-m-d H:i:s');
                            $user->first_name = $request['first_name'];
                     
                        }
                    }  

                    if($user->ln_updated == '')
                    {
                        if($user->last_name != $request['last_name'])
                        {
                            $user->ln_updated = date('Y-m-d H:i:s');
                            $user->last_name = $request['last_name'];
                        }
                    }
                    else
                    {
                        $ln_updated = date('Y-m-d',strtotime($user->ln_updated));
                        $cur_date = date('Y-m-d');
                        $datetime1 = date_create($ln_updated);
                        $datetime2 = date_create($cur_date);
                        $interval = date_diff($datetime1, $datetime2);
                        $date_diff =  $interval->format('%m');
                        if($date_diff >= 6 && $user->last_name != $request['last_name'])
                        {
                            $user->ln_updated = date('Y-m-d H:i:s');
                            $user->last_name = $request['last_name'];
                     
                        }
                    }  

                    if($user->ut_updated == '')
                    {
                        if($user->user_type != $request['user_type'])
                        {
                            $user->ut_updated = date('Y-m-d H:i:s');
                            $user->user_type = $request['user_type'];
                        }
                    }
                    else
                    {
                        $ut_updated = date('Y-m-d',strtotime($user->ut_updated));
                        $cur_date = date('Y-m-d');
                        $datetime1 = date_create($ut_updated);
                        $datetime2 = date_create($cur_date);
                        $interval = date_diff($datetime1, $datetime2);
                        $date_diff =  $interval->format('%m');
                        if($date_diff >= 6 && $user->user_type != $request['user_type'])
                        {
                            $user->ut_updated = date('Y-m-d H:i:s');
                            $user->user_type = $request['user_type'];
                     
                        }
                    } 
                }   

                $user->save();
                $update_data = array(
                    'user_name' => $request['user_name'],
                    'tagline' => $request['tagline'],
                    'email' => $request['email'],
                    'profession' => $request['profession'],
                    'about_me' => $request['about_me'],
                    'mob_no' => $request['mob_no'],
                    'phone_no' => $request['phone_no'],
                    'location' => isset($request['location']) ? $request['location'] : '',
                    'is_google_crawalable' => isset($request['is_google_crawalable']) ? $request['is_google_crawalable'] : 0,
                    'gender' => isset($request['gender']) ? $request['gender'] : NULL,
                    'user_birth_date' => isset($request['dob']) ? date('Y-m-d', strtotime($request['dob'])) : '',
                    'website' => isset($request['website']) ? $request['website'] : '',
                    'facebook_page' => isset($request['facebook_page']) ? $request['facebook_page']: '',
                    'twitter_page' => isset($request['twitter_page']) ? $request['twitter_page']: '',
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $commonModel->updateRow("mst_users", $update_data, array('id' => $user_id));
                $user = User::find($user_id);
                Session::put('user_account', $user);
                Session::flash('success_msg', 'Your profile has been updated successfully.');
        
        return redirect()->guest('edit-user-bio');
    }

    
    
    public function editBio() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect('signin');
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $request = Input::all();
        
        
        $update_data = array(
            //'gender' => $request['gender'],
            'height' => $request['height'],
            'weight' => $request['weight'],
            'height_unit' => $request['height_unit'],
            'body_id' => $request['body_type'],
            'hair_id' => $request['hair_color'],
            'eye_id' => $request['eye_color'],
            'ethnicity_id' => $request['ethnicity_type'],
            //'user_birth_date' => $request['dob'],
        );
        $commonModel->updateRow("mst_users", $update_data, array('id' => $user_id));
        Session::flash('success_msg', 'Your profile has been updated successfully.');
        return redirect()->guest('bio-setting');
    }

    /*
     * Check user password for edit
     */

    public function editPasswordSetting() {
        $commonModel = new CommonModel();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $request = Input::all();
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        $hash_password = crypt($request['new_password'], '$2y$12$' . $salt);
        $table_to_pass = 'mst_users';
        $condition_to_pass = array("id" => $user_id);
        $commonModel->updateRow($table_to_pass, array("password" => $hash_password), $condition_to_pass);
        Session::flash('success_msg', 'Your password has been updated successfully.');
        return redirect()->guest('profile');
    }

    public function uploadCoverPhoto() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $file = Input::file('cover_photo');
                if (isset($_POST['coverImagec'])) {
                    $data = $_POST['coverImagec'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $upload_pic_response=$this->upload_pic(1,$user_id,$data);
                    $resp=json_decode($upload_pic_response);
                    $cover_photo = array("cover_photo" => $resp->cover_photo, 'user_id_fk' => $user_id, "created_at" => 'Y-m-d H:i:s');
                    $commonModel->updateRow('mst_users', array('cover_photo' => $resp->cover_photo), array('id' => $user_id));
                    $commonModel->insertRow('trans_user_cover_photo', $cover_photo);
                    echo $upload_pic_response;
                    exit();
                }
    }

    public function uploadProfilePicture() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
       
        if (isset($_POST['imagebase'])) {
            $image = $_POST['imagebase'];
            list($type, $image) = explode(';', $image);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $upload_pic_response=$this->upload_pic(0,$user_id,$image);
            $resp=json_decode($upload_pic_response);
            $profile_picture = array("profile_picture" => $resp->profile_picture, 'user_id_fk' => $user_id, "created_at" => 'Y-m-d H:i:s');
            $commonModel->updateRow('mst_users', array('profile_picture' => $resp->profile_picture), array('id' => $user_id));
            $commonModel->insertRow('trans_user_profile_picture', $profile_picture);
            echo $upload_pic_response;
            
        }
    }

    public function addNewUsername() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $user_new_username = Input::get('u_val');

        //get data from table
        $blacklist = BlackList::get();
        $blacklist_array = array();
        foreach ($blacklist as $k => $val) {
            $blacklist_array[$k] = $val['attributes']['blacklist_name'];
        }

        if (isset($_POST['u_val'])) {
            $check_detail = AdminUser::where('user_name', $user_new_username)->first();
            if (!empty($check_detail) || $check_detail != null) {
                echo json_encode(array("error" => '0', "username" => $user_new_username, 'msg' => 'Username alredy exists'));
                exit();
            } elseif (in_array($user_new_username, $blacklist_array)) {
                echo json_encode(array("error" => '1', "username" => $user_new_username, 'msg' => 'Username is Invalid.'));
                exit();
            } else {
                Session::put('uname_added', '1');
                $commonModel->updateRow('mst_users', array('user_name' => $user_new_username), array('id' => $user_id));
                #print_r('success');
                echo json_encode(array("success" => '2', "username" => $user_new_username, 'msg' => 'Username added successfully.'));
                exit();
            }
        }
    }

    public function uploadGroupIcon() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
                if (isset($_POST['imagebase'])) {
                    $data = $_POST['imagebase'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $upload_response=$this->upload_pic(2,null,$data);
                    $resp=json_decode($upload_response);
                    $icon_img = array("icon_img" => $resp->icon_img, 'id' => $user_id, "created_at" => 'Y-m-d H:i:s');
                    $commonModel->updateRow('mst_group', array('icon_img' => $resp->icon_img), array('id' => $user_id));
                    $commonModel->insertRow('mst_group', $icon_img);
                    // $user = Auth::User();
                    // Session :: put('user_account', $user);
                    echo $upload_response;
                    exit();
                }
        
    }

    function logout() {
        Session::Flush();
        return redirect('')->with('message', FlashMessage::DisplayAlert("You are logged out successfully.!", 'success'));
    }

    public function confirmRequest() {

        $request = Input::all();
        $from_id = $request['from_id'];
        $user_id = $request['user_id'];

        if ($request['status'] == 0) {

            $rejected = DB::table('trans_user_connection')
                ->where('to_user_id', $user_id)
                ->where('from_user_id', $from_id)
                ->delete();

            echo json_encode(array('msg' => '0'));
        } else if ($request['status'] == 1) {

            $accept = UserConnection::
                    where('to_user_id', $user_id)
                    ->where('from_user_id', $from_id)
                    ->update(['status' => '1']);

            echo json_encode(array('msg' => '1'));
        } else {
            $reject = DB::table('trans_user_connection as user_connection')
                    ->where('to_user_id', $user_id)
                    ->where('from_user_id', $from_id)
                    ->update(['status' => '0']);
            echo json_encode(array('msg' => '0'));
        }
        $update_notification = DB::table('mst_notifications')
                ->where('notification_to', $user_id)
                ->where('notification_from', $from_id)
                ->where('read_status', '0')
                ->update([
            'read_status' => '1'
        ]);

        $arr_u_notification = DB::table('mst_notifications')
                ->where('notification_to', $user_id)
                ->where('read_status', '0')
                ->get();
        $get_count_of_unread_notificatons = count($arr_u_notification);
        Session::put('unread_notification_count', $get_count_of_unread_notificatons);
    }

    public function deletedConnection() {

        $request = Input::all();
        $from_id = $request['from_id'];
        $user_id = $request['user_id'];

        $deleted = DB::table('trans_user_connection')
                ->where('to_user_id', $user_id)
                ->where('from_user_id', $from_id)
                ->delete();
        $deleted = DB::table('trans_user_connection')
                ->where('to_user_id', $from_id)
                ->where('from_user_id', $user_id)
                ->delete();
        echo json_encode(array('msg' => '1'));
        exit;
    }

    public function getProfileSetting() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $data['header'] = array(
            'title' => 'Profile Setting',
        );

        return view('Frontend.user.profile.profile-setting', ['finalData' => $data, 'arr_user_data' => $arr_user_data]);
    }

    public function getNotificationSetting() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $transaction_count = $this->getIfTransactionHistory($user_id);
        
        $data['header'] = array(
            'title' => 'Notification Settings',
        );

        return view('Frontend.user.profile.notification-setting', ['transaction_count' => $transaction_count,'finalData' => $data, 'arr_user_data' => $arr_user_data]);
    }

    public function editNotificationSetting() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect('signin');
        }
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];
        $request = Input::all();
        
        // $group_notify = (isset($request['new_group_notification']) ? $request['new_group_notification'] : '0');
        // $job_notification = (isset($request['job_notification']) ? $request['job_notification'] : '0');
        // $update_data = array(
        //     'follower_notification' => $request['follower_notification'],
        //     'connection_notification' => $request['connection_notification'],
        //     'set_group_email' => $group_notify,
        //     'job_notification'=> $job_notification
        // );
        // $commonModel->updateRow("mst_users", $update_data, array('id' => $user_id));

        $follower_notification = isset($request['follower_notification']) ? $request['follower_notification'] : '';
        $connection_notification = isset($request['connection_notification']) ? $request['connection_notification'] : '';
        $set_group_email = isset($request['new_group_notification']) ? $request['new_group_notification'] : '0';
        $job_notification = isset($request['job_notification']) ? $request['job_notification'] : '0';
        $schdule_email_fix = isset($request['schedule_email']) ? $request['schedule_email'] : '0';
        $schedule_day_fix = isset($request['schedule_day']) ? $request['schedule_day'] : '';
        $schedule_date_fix = isset($request['schedule_date']) ? $request['schedule_date'] : '';
        $event = isset($request['event']) ? $request['event'] : '0';
        $meetup = isset($request['meetup']) ? $request['meetup'] : '0';
        $education = isset($request['education']) ? $request['education'] : '0';
        $article = isset($request['article']) ? $request['article'] : '0';
        $qurious = isset($request['qurious']) ? $request['qurious'] : '0';
        $post_audience = isset($request['post_audience']) ? $request['post_audience'] : '1';
        $promotion_noti = isset($request['promotion_notification']) ? $request['promotion_notification'] : '0' ;
        $obj_email = UserEmailSchedule::where('user_id', $user_id)->first();
        
        if (isset($request['schedule_email']) && $request['schedule_email'] == '3') {
            $event = '0';
            $meetup = '0';
            $education = '0';
            $article = '0';
            $qurious = '0';
        }

        if ($event == '0' && $article == '0' && $meetup == '0' && $education == '0' && $qurious == '0') {
            $schdule_email_fix = '3';
        }

        if (count($obj_email) <= 0) {
            $obj_email = new UserEmailSchedule();
        }

        $obj_email->follower_notification = $follower_notification;
        $obj_email->connection_notification = $connection_notification;
        $obj_email->set_group_email = $set_group_email;
        $obj_email->job_notification = $job_notification;
        $obj_email->email_schedule = $schdule_email_fix;
        $obj_email->day = $schedule_day_fix;
        $obj_email->date = $schedule_date_fix;
        $obj_email->event = $event;
        $obj_email->meetup = $meetup;
        $obj_email->education = $education;
        $obj_email->article = $article;
        $obj_email->qurious = $qurious;
        $obj_email->post_audience = $post_audience;
        $obj_email->ATG_promotion_noti = $promotion_noti;
        $obj_email->user_id = $user_id;

        $obj_email->save();
        Session::flash('success_msg', 'Notification settings updated.');
        return redirect()->guest('notification-setting');
    }

    public function getPasswordSetting() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $transaction_count = $this->getIfTransactionHistory($user_id);
        
        $data['header'] = array(
            "title" => 'Account Setting',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.pwd-setting')
                        ->with('transaction_count', $transaction_count)
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getMessages() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $data['header'] = array(
            "title" => 'Messages',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.messages')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getMyBlogs() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);

        $arr_blog = DB::table('mst_blog as b')
                ->join('mst_users as u', 'b.posted_by', '=', 'u.id', 'left')
                ->join('mst_uri_map as um', 'b.id', '=', 'um.rel_id', 'left')
                ->where('b.posted_by', $user_id)
                ->select('b.*', 'u.user_name', 'um.*')
                ->get();
        foreach ($arr_blog as $key => $value) {

            $arr_blog_comment = DB::table('mst_comments as mc')
                    ->join('trans_blog_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
                    ->where('tbc.blog_id_fk', $value->id)
                    ->select('mc.*')
                    ->get();
            $arr_blog[$key]->count = count($arr_blog_comment);
        }

        $data['header'] = array(
            "title" => 'My Blogs',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.my-blog')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('arr_blog', $arr_blog)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getSavedEvents() {

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $arr_events = Event::where('status', '1')
                ->where('user_id_fk', $user_id)
                ->get();

        $data['header'] = array(
            "title" => 'Saved Events',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.saved-events')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('eventdata', $arr_events)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getSavedMeetups() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $arr_meetup = meetup_model::where('status', '1')
                ->where('user_id_fk', $user_id)
                ->get();


        $data['header'] = array(
            "title" => 'Saved Meetups',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.saved-meetups')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('arr_meetup', $arr_meetup)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getCompanySavedJobs() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $saved_job = DB::table('mst_job')
                ->where('user_id_fk', $user_id)
                ->where('job_type', '0')
                ->get();



        $data['header'] = array(
            "title" => 'Saved Jobs',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.saved-jobs-company')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('saved_job', $saved_job)
                        ->with('arr_user_data', $arr_user_data);
    }

    public function getStudentSavedJobs() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $is_save = DB::table('trans_student_saved_job as t')
                ->join('mst_job as m', 'm.id', '=', 't.job_id_fk')
                ->where('t.user_id_fk', $user_id)
                ->where('t.status', '0')
                ->select('t.*', 'm.title')
                ->get();

        $data['header'] = array(
            "title" => 'Saved Jobs',
            "keywords" => '',
            "description" => ''
        );
        return view('Frontend.user.profile.saved-jobs-student')
                        ->with('title', 'Setting')
                        ->with('finalData', $data)
                        ->with('is_save', $is_save)
                        ->with('arr_user_data', $arr_user_data);
    }

    function blockUser() {

        $request = Input::all();
        $status = $request['status'];
        $user_id = $request['user_id'];
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        $data['user_session'] = Session::get('user_account');
        $session_user_id = $data['user_session']['id'];

        $user_id = $request['user_id'];

        if ($status == 3) {

            DB::table('trans_user_connection')
                    ->whereRaw("((from_user_id =" . $session_user_id . " AND to_user_id =" . $user_id . ") OR  (to_user_id =" . $session_user_id . " AND from_user_id =" . $user_id . "))")
                    ->update(['status' => '3', 'block_by' => $session_user_id]);

            Session::flash('success_msg', 'You have successfully blocked This user.');
            echo json_encode(array('msg' => '1'));
        } else {

            DB::table('trans_user_connection')
                    ->whereRaw("((from_user_id =" . $session_user_id . " AND to_user_id =" . $user_id . ") OR  (to_user_id =" . $session_user_id . " AND from_user_id =" . $user_id . "))")
                    ->update(['status' => '0', 'block_by' => '0']);



            Session::flash('success_msg', 'You have successfully unblocked This user.');
            echo json_encode(array('msg' => '1'));
        }
    }

    function userReport($user_id) {

        $user_id = base64_decode($user_id);

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $request = Input::all();
        if (isset($request['report']) && (!empty($request['report']))) {
            $report = $request['report'];
        } else {
            $report = '';
        }

        if ($report == '0') {
            $report_name = 'Report this profile';
        } elseif ($report == '1') {
            $report_name = 'Recover or close this account';
        } elseif ($report == '2') {
            $report_name = 'I want to help';
        } else {
            $report_name = '';
        }
        $data['user_session'] = Session::get('user_account');
        $session_user_id = $data['user_session']['id'];

        $arr_user_data = User::find($session_user_id);
        $arr_user_data1 = User::find($user_id);

        $email = $arr_user_data['email'];
        $contact_email = $data['global']['contact_email'];

        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();


        $arr_admin_detail = User::where('email', $contact_email)->first();
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||ADMIN_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||PROGRAMME_TYPE||" => '"' . ucfirst($arr_user_data1['first_name']) . ' ' . ucfirst($arr_user_data1['last_name']) . '"' . 'profile',
            "||LOGIN_USER_NAME||" => '"' . ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
            "||LOGIN_LINK||" => $admin_login_link
        );

        if ($report_name != '') {
            $reserved_arr['||NAME||'] = ' as ' . $report_name;
        }

        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }

        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $email_contents, $data) {
            $message->from($email, $data['global']['site_title']);
            $message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
        }); // End Mail 

        Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully .');
        echo json_encode(array('msg' => 1));
    }

    public function viewer() {
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        $request = Input::all();
        $flag = (isset($request['flag'])?$request['flag']:'0');
        $start_date = (isset($request['start_date'])?$request['start_date']:'');
        $end_date = (isset($request['end_date'])?$request['end_date']:'');
        $type = $request['type'];
        $ip_address = $_SERVER['REMOTE_ADDR'];

        if ($type == '1' || $type == 'event') {
            $type = '1';
            $type_name = 'event';
        }
        else if ($type == '2' || $type == 'meetup') {
            $type = '2';
            $type_name = 'meetup';
        }
        else if ($type == '3' || $type == 'article') {
            $type = '3';
            $type_name = 'article';
        }
        else if ($type == '4' || $type == 'education') {
            $type = '4';
            $type_name = 'education';
        }
        else if ($type == '5' || $type == 'job') {
            $type = '5';
            $type_name = 'job';
        }
        else if ($type == '6' || $type == 'question' || $type == 'qrious') {
            $type = '6';
            $type_name = 'question';
        }
        
        $post_id = $request[$type_name.'_id'];
        $to_post_visitor = $request['to_'.$type_name.'_visitor'];
        if ($to_post_visitor != $user_id) {
            $get_last_record = DB::table('mst_visitor')
                    ->where('feature_id', $post_id)
                    ->where('type', $type)
                    ->where('visitor_id', $user_id)
                    ->where('flag', $flag)
                    ->select('*')
                    ->get();

            if (count($get_last_record) == 0) {
                DB::table('mst_visitor')->insert(
                        ['visitor_id' => ($user_id ? $user_id : 0), 'feature_id' => $post_id, 'type' => $type, 'visiting_date' => date("Y-m-d H:i:s"), 'flag' => $flag]
                );
            }

            $get_last_record = DB::table('mst_visitor')
                    ->where('feature_id', $post_id)
                    ->where('type', $type)
                    ->where('flag', $flag)
                    ->select('*')
                    ->get();
            return count($get_last_record);
        }
        else
        {
            $get_last_record = DB::table('mst_visitor')
                    ->where('feature_id', $post_id)
                    ->where('type', $type)
                    ->where('visitor_id', '!=',$user_id)
                    ->where('flag', $flag)
                    ->select('*');
            if($start_date != '') 
            {
                $get_last_record->where('visiting_date', '>=', $start_date.' 00:00:00');
            }
            if($end_date != '') 
            {
                $get_last_record->where('visiting_date', '<=', $end_date.' 00:00:00');
            }
                    
            $num = $get_last_record->get();
            echo count($num);
        }

        
    }

    public function againSendMail() {
        $commonModel = new CommonModel();
        $user_account = Session::get('user_account');
        /*         * **********resend mail************** */

        if (isset($user_account) && $user_account['email'] != '') {
            $this->sendEmailVerificationMail($user_account['email'], $user_account);
            return redirect()->back()->with('warning_msg_permanent', "Verification email sent on <b>".$user_account['email']."</b>. Check your inbox/spam. <br> <b>".$user_account['email']."</b> not your email? <a style=\"color:white\" href=\"send-feedback\"> Contact us </a>");
        }
    }

    public function checkPublicUrl() {
        return redirect()->guest('/errors');
    }

    public function getInviteFriends() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $arr_user_data = array();
        $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        
        $transaction_count = $this->getIfTransactionHistory($user_id); 
        
        $data['header'] = array(
            "title" => 'Invite Friends',
            "keywords" => '',
            "description" => ''
        );

        $arr_u_connection = DB::table('trans_user_connection as user_connection')
                ->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
                ->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
                ->select('to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
                ->where('user_connection.from_user_id', $user_id)
                ->where('user_connection.is_referral', '1')
                ->get('group_name');

        $arr_user_connection = array();
        foreach ($arr_u_connection as $object) {
            $arr_user_connection[] = (array) $object;
        }

        return view('Frontend.user.profile.invite-friends')
                        ->with('transaction_count', $transaction_count)
                        ->with('title', 'Invite Friends')
                        ->with('finalData', $data)
                        ->with('arr_user_connection', $arr_user_connection)
                        ->with('arr_user_data', $arr_user_data);
    }

    // verify username 
    public function verify_username() {
        $all = Input::all();

        //get data from table
        $blacklist = BlackList::get();
        $blacklist_array = array();
        foreach ($blacklist as $k => $val) {
            $blacklist_array[$k] = $val['attributes']['blacklist_name'];
        }

        /* checking user name already exist or not for add admin  */
        if ($all['id']) {
            $arr_admin_detail = AdminUser::where('user_name', $all['key'])->where('id', '!=', $all['id'])->first();
        } else {
            $arr_admin_detail = AdminUser::where('user_name', $all['key'])->first();
        }
        if (!empty($arr_admin_detail) || $arr_admin_detail != null) {
            echo '0';
        } elseif (in_array($all['key'], $blacklist_array)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function uploadImage() {
            $all = Input::all();
            $imageuploadfile=Input::file('file');
            $resp=$this->upload_pic(3,null,$imageuploadfile,$all['folder']);
            echo stripslashes($resp);
    }

    public function testprofile() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();

        if ($login) {
            return redirect(url('/'));
        }

        $data['user_session'] = Session::get('user_account');
        if ($data['user_session']['signup_step'] == "2") {
            Session::flash('error_msg', 'Please select atleast one group to proceed further.');
            return redirect()->guest('/signup2');
        }
        $user_id = $data['user_session']['id'];
        $arr_u_data = DB::table('mst_users as user')
                ->leftjoin('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id')
                ->leftjoin('mst_body_type as body', 'body.id', '=', 'user.body_id')
                ->leftjoin('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id')
                ->leftjoin('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id')
                ->select('user.*', 'ethnicity.ethnicity_type as ethnicity_type', 'body.body_type as body_type', 'eye.eye_color as eye_color', 'hair.hair_color as hair_color')
                ->where('user.id', $user_id)
                ->get();
        $arr_u_follower = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.follower_id_fk', '=', 'user.id')
                ->select('follower.follower_id_fk', 'user.first_name', 'user.gender', 'follower.status', 'user.profile_picture', 'user.last_name')
                ->where('status', '0')
                ->where('user_id_fk', $user_id)
                ->get();

        $arr_u_following = DB::table('trans_follow_user as follower')
                ->join('mst_users as user', 'follower.user_id_fk', '=', 'user.id')
                ->select('follower.user_id_fk', 'follower.status', 'user.first_name', 'user.gender', 'user.last_name', 'user.gender', 'user.profile_picture')
                ->where('follower_id_fk', $user_id)
                ->where('status', '0')
                ->get();


        $arr_u_group = DB::table('trans_group_users as group_user')
                ->leftjoin('mst_group as group', 'group.id', '=', 'group_user.group_id_fk')
                ->select('group.group_name', 'group.id', 'group_user.user_id_fk')
                ->where('user_id_fk', $user_id)
                ->where('group_user.status', '=', '1')
                ->distinct()
                ->get('group_name');

        $arr_user_group = array();

        $arr_u_cover = DB::table('trans_user_cover_photo')
                ->select('cover_photo', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get('group_name');

        $arr_u_profile_pic = DB::table('trans_user_profile_picture')
                ->select('profile_picture', 'created_at')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();
        $arr_u_connection = DB::table('trans_user_connection as user_connection')
                ->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
                ->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
                ->select('to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
                ->where('user_connection.from_user_id', $user_id)
                ->orWhere('user_connection.to_user_id', $user_id)
                ->get('group_name');


        $arr_u_notification = DB::table('mst_notifications as n')
                ->leftjoin('mst_users as user', 'n.notification_from', '=', 'user.id')
                ->select('n.*', 'user.first_name', 'user.last_name', 'user.profile_picture')
                ->where('n.notification_from', $user_id)
                ->where('n.read_status', '0')
//                ->take(5)
                ->orderBy('n.created_at', 'DESC')
                ->get();

        $jobcount = DB::table('mst_job')
                ->where('user_id_fk', $user_id)
                ->where('status','1')
                ->count();        

        $arr_user_data = array();
        foreach ($arr_u_data as $object) {
            $arr_user_data = (array) $object;
        }

        $arr_user_cover_photo = array();
        foreach ($arr_u_cover as $object) {
            $arr_user_cover_photo[] = (array) $object;
        }

        $arr_notification = array();
        foreach ($arr_u_notification as $object) {
            $arr_notification[] = (array) $object;
        }

        $arr_user_profile_picture = array();
        foreach ($arr_u_profile_pic as $object) {
            $arr_user_profile_picture[] = (array) $object;
        }

        $arr_user_connection = array();
        foreach ($arr_u_connection as $object) {
            $arr_user_connection[] = (array) $object;
        }

        $arr_user_group = array();
        foreach ($arr_u_group as $object) {
            $arr_user_group[] = (array) $object;
        }
        $arr_follower = array();
        foreach ($arr_u_follower as $object) {
            $arr_follower[] = (array) $object;
        }


        $arr_following = array();
        foreach ($arr_u_following as $object) {
            $arr_following[] = (array) $object;
        }

        $is_connect = DB::table('trans_user_connection')
                ->Where('from_user_id', $user_id)
                ->get();

        $get_user_profile_picture = DB::table('trans_user_profile_picture')
                ->where('user_id_fk', $user_id)
                ->orderBy('id', 'DESC')
                ->get();

        $page_title = '';
        if ($arr_user_data['first_name'] != '' && $arr_user_data['last_name'] != '') {
            $page_title = $arr_user_data['first_name'] . ' ' . $arr_user_data['last_name'];
        } else {
            $page_title = 'User Profile';
        }

        $data['header'] = array(
            "title" => $page_title,
            "keywords" => '',
            "description" => ''
        );

        return view('Frontend.user.profile.testprofile')
                        ->with('title', 'Profile')
                        ->with('finalData', $data)
                        ->with('arr_user_data', $arr_user_data)
                        ->with('arr_follower', $arr_follower)
                        ->with('is_connect', $is_connect)
                        ->with('arr_notification', $arr_notification)
                        ->with('arr_user_cover_photo', $arr_user_cover_photo)
                        ->with('arr_user_profile_picture', $arr_user_profile_picture)
                        ->with('get_user_profile_picture', $get_user_profile_picture)
                        ->with('arr_user_connection', $arr_user_connection)
                        ->with('arr_following', $arr_following)
                        ->with('arr_user_group', $arr_user_group)
                        ->with('jobcount', $jobcount)
                        ->with('user_id', $user_id);
    }

    public function userActivityLoad($id = 0) {
        if($id > 0){
            $user_id = $id;
        }else{
            $data['user_session'] = Session::get('user_account');
            $user_id = $data['user_session']['id'];
        }
        $activities=UserActivity::where('user_id_fk',$user_id)->orderBy('id','DESC')->paginate(5);
        $html='';
        foreach ($activities as $ua) {
            $html.='<li>
                <div class="media main-user-acti">
                    <div class="media-left">
                        <i class="icon-Post"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"> 
                            <span>';
                            $ex=explode('-',$ua->activity_title);
                                if(strlen($ua->activity_title) < 50){
                                $html.= $ua->user['first_name'].' '.$ua->user['last_name'].' '.$ex[0].' - '.'<a class="actlink" href="'.url('/').'/'.$ua->activity_link.'">'.ucwords($ex[1]).'</a>';
                                }else{ 
                                $html.= $ua->user['first_name'] .' '. $ua->user['last_name'] .' '.$ex[0].' - '.'<a class="actlink" href="'.url('/').'/'.$ua->activity_link.'">'.substr(strip_tags($ex[1]), 0, 10).'<br>'.substr(strip_tags($ex[1]), 10, strlen($ex[1])).'</a>';
                                }
                            $html.='    
                            </span>
                        </h4>
                        <div class="posting-day">'.date('M d, Y', strtotime($ua['created_at'])).'</div>
                    </div>
                </div>
            </li>';
        }
        // if(count($activities) < 1){
        //     $html.='<li id="no_more_activity">No More Activity Found</li>';
        // }

        return $html;
    }
    
    public function sendEndPromoEmail()
    {
        $list = $this->getAllPromotedPostsEndedYesterday();
        foreach($list as $key => $value)
        {
            $data = DB::table('mst_visitor')
                    ->whereRaw('DATE(visiting_date) BETWEEN "'.$value['promo_start_date'].'" AND "'.$value['promo_end_date'].'"')
                    ->where('feature_id', $value['post_id_fk'])
                    ->selectRaw('COUNT(IF(flag=0,1,NULL)) AS clicks, COUNT(IF(flag=1,1,NULL)) AS views')
                    ->get();
            if(count($data))
            {
                $this->sendPromotionMailToPromoter('end', $value['user_name'],$value['email'], $value['title'], $value['id'], $value['post_type'], $data[0]->views, $data[0]->clicks);
            }
        }
        
    }
    
    public function sendDailyPromoEmail()
    {
        $list = $this->getAllPromotedPosts();
        foreach($list as $key => $value)
        {
            $data = DB::table('mst_visitor')
                    ->whereRaw('DATE(visiting_date) = "'.DATE('Y-m-d').'"')
                    ->where('feature_id', $value['post_id_fk'])
                    ->selectRaw('COUNT(IF(flag=0,1,NULL)) AS clicks, COUNT(IF(flag=1,1,NULL)) AS views')
                    ->get();
            if(count($data))
            {
                $this->sendPromotionMailToPromoter('daily', $value['user_name'],$value['email'], $value['title'], $value['id'], $value['post_type'], $data[0]->views, $data[0]->clicks);
            }
        }
    }

    public function pushapi($id){
        $req=Input::all();
        $user=User::findOrFail($id);
        $user->updatePushSubscription($req['endpoint'],$req['keys']['p256dh'],$req['keys']['auth']);
        // $user->notify(new \App\Notifications\webpushnotification());
            return json_encode( ['success'=>true]);
    }
    public function webpushmessagetospececificuser($id,Request $request)
    {
       $inp=Input::all();
        $user=User::findOrFail($id);
        
        $user->notify(new \App\Notifications\webpushnotification($inp['title'],$inp['body'],$inp['icon']));
            return json_encode( ['success'=>true]);
    }


  


}

?>
