<?php

namespace App\Http\Controllers;

use Mail;
use DB;
use App\AdminUser;
use App\FollowerUser;
use App\role;
use App\Team;
use App\Filter;
use App\TeamUser;
use App\email_templates;
use App\email_template_macros;
use App\global_settings;
use App\trans_global_settings;
use App\SocialLink;
use App\CommonModel;
use App\UserProfileSettings;
use App\UserProfileImages;
use App\User;
use Session;
use Illuminate\Filesystem\Filesystem;
use App\Helpers\FlashMessage;
use App\Helpers\GlobalData;
use Auth;
use Illuminate\Support\Facades\Input;;
use Request;

class FilterController extends Controller {

    public function __construct() {
        
    }

    public function saveFilterData() {
        $request = Input::all();
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');

        // Get the groups joined by the user
        $user_id = $data['user_session']['id'];
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        // if user's group count is zero, redirect to signup2 to choose groups first
        if (count($get_groups) == 0) {
            Session::flash('error_msg', 'Please select atleast one group to proceed further.');
            return redirect()->guest('/signup2');
        }

        $user_id = $data['user_session']['id'];
        $get_filter = DB::table('trans_user_filter')
                ->where('user_id_fk', $user_id)
                ->where('group_id_fk', $request['group_id'])
                ->get();
        if (count($get_filter) > 0) {
            $update = DB::table('trans_user_filter')
                    ->where('user_id_fk', $user_id)
                    ->where('group_id_fk', $request['group_id'])
                    ->update([
                'user_id_fk' => $user_id,
                'qrious' => $request['qrious'],
                'jobs' => $request['jobs'],
                'events' => $request['events'],
                'meetups' => $request['meetups'],
                'articles' => $request['articles'],
                'education' => $request['education'],
                'group_id_fk' => $request['group_id']
            ]);
        }  else {
            $filter = new Filter;
            $filter->user_id_fk = $user_id;
            $filter->qrious = $request['qrious'];
            $filter->jobs = $request['jobs'];
            $filter->events = $request['events'];
            $filter->meetups = $request['meetups'];
            $filter->articles = $request['articles'];
            $filter->education = $request['education'];
            $filter->group_id_fk = $request['group_id'];
            $filter->save();
        }
       
        Session::put('group_id', $request['group_id']);
        
        return redirect()->guest('/my-group');
    }

    public function getFiltersData() {
        $request = Input::all();
        $group_id = $request['group_id'];
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        $get_all_filter_data = DB::table('trans_user_filter')
                ->where('user_id_fk', $user_id)
                ->where('group_id_fk', $group_id)
                ->get();
        if (count($get_all_filter_data) > 0) {
            echo json_encode(array('error' => '0', 'response' => $get_all_filter_data[0]));
        } else {
            echo json_encode(array('error' => '1'));
        }
    }

}

?>
