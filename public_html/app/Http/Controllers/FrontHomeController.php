<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\global_settings;
use App\Http\Requests;
use App\CommonModel;
use Session;
use Cookie;
use Illuminate\Cookie\CookieJar;
use App\Http\Controllers\Controller;

class FrontHomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username = null) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $user_account = Session::get('user_account');

        if (!empty($user_account) && $user_account != '') {
            return redirect()->guest('/user-dashboard');
        }

        $code = Session::get('user_code');
        $cookie_data = '';
        if (isset($_COOKIE['login_cr'])) {
            $cookie_data = unserialize($_COOKIE['login_cr']);
        }

        return view('Frontend.home.home_new')->with('finalData', $data)->with('user_account', $user_account)->with('cookie_data', $cookie_data);
    }

    public function indexResetPassword($activation_code = '') {
        return view('Frontend.home.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
    }

}
