<?php

namespace App\Http\Controllers;

use App\faqs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class faqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    /* function to list all the FAQs */
    public function listContactUs() {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $date_format = 'Y-m-d';
        $arr_faqs_list = faqs::all();
        echo '<pre>';print_r($arr_faqs_list);die;
        $date_format = 'Y-m-d';
        $site_title = "Manage Contact Us";
        return view('Backend.faqs.list', ['arr_faqs' => $arr_faqs_list, 'site_title' => $site_title, 'date_format' => $date_format]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
