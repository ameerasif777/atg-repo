<?php

namespace App\Http\Controllers;

use DB;
use App\contact_us;
use App\User;
use App\email_templates;
use App\email_template_macros;
use App\trans_global_settings;
use App\CommonModel;
use App\contact_feedback_reply;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;;
use Request;
use Illuminate\Support\Facades\Mail;
use App\Helpers\GlobalData;


class ContactUsController extends Controller {
    /*

      |-----------------------------------------------------------------------
      |---
      | Admin Panel Controller

      |-----------------------------------------------------------------------
      |---
     */

    public function __construct() {
        
    }

    public function index() {
        $request = Input::all();
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        
        if (count($request) > 0) {

            /* insert contact details */
            $email = $request['email'];
            $contact_email = $data['global']['contact_email'];
            $contact = new contact_us;
            $contact->name = $request['first_name'];
//            $contact->subject = $request['subject'];
            $contact->mail_id = $request['email'];
            $contact->message = $request['message'];
            $contact->contact_feedback = $request['contact_feedback'];
            $contact->date = date("Y-m-d H:i:s");
            $contact->reply_status = '0';
            $contents_array = array("email_conents" => $request['message']);
            $contact->save();
          
            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($email, $data, $request) {
                $message->from($email, $data['global']['site_title']);
                $message->to($data['global']['contact_email'], 'Admin');
//                $message->subject($request['subject'], $request['subject']);
                
            }); // End Mail            
            Session::flash('success_msg', 'Your message has been sent successfully !');
            return redirect(url('/') . '/send-feedback');
        }
        $site_title = "Send Feedback ";
        return view('Frontend.contact-us.contact-us', ['arr_address' => $address, 'site_title' => $site_title]);
    }

    public function forgotPasswordAction() {
        $all = Input::all();
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $data['global']['site_email'] = $site_email->value;
        $data['global']['site_title'] = $site_title->value;
        /* sending admin credentail on admin account mail on user email */
        $code = rand(9991, 999899);
        $activation_code = time();
        $arr_admin_detail = User::where('email', $all['email'])->first();
        $update_data = array('reset_password_code' => $activation_code);
        User::where('email', $all['email'])->update($update_data);
        $reset_password_link = '<a href="' . url('/') . '/front/reset-password/' . base64_encode($activation_code) . '">Click here</a>';
        $lang_id = 17;
        $admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
        /* setting reserved_words for email content */
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'user-forgot-password', 'lang_id' => '17'))->first();
        //$email_contents = end($email_contents);

        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($data['global']['site_title']),
            "||SITE_PATH||" => url('/'),
            "||USER_NAME||" => $arr_admin_detail['user_name'],
            "||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
            "||USER_EMAIL||" => $arr_admin_detail['email'],
            "||RESET_PASSWORD_LINK||" => $reset_password_link,
            "||LOGIN_LINK||" => $admin_login_link
        );
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);

       
// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($arr_admin_detail, $contents_array, $data, $email_contents) {
            $message->from($data['global']['site_email'], $data['global']['site_title']);
            $message->to($arr_admin_detail['email'], $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'])->subject($email_contents['email_template_subject']);
        }); // End Mail
        Session::flash('success_msg', 'Reset details has been sent successfully to your email');
        return redirect('/');
    }

    // public function resetPassword($code) {
        
        
    //     /* getting user details if exist from email */
    //     if ($code != '') {
    //         $user=User::where('reset_password_code', base64_decode($code))->first();
    //         if (count($user) <= 0) {
    //             Session::flash('error_msg', 'The entered link is not valid.');
    //             return redirect('/');
    //         }
    //         else{
    //             Session::put('user_account', $user);
    //             return redirect('/pwd-setting');
    //         }
            
    //     }
    //     else{
    //         Session::flash('error_msg', 'The entered link is not valid.');
    //         return redirect('/');
    //     }

    // }

    public function resetPassword($code) {
        
        
        /* getting user details if exist from email */
        if ($code != '') {
            $user=User::where('reset_password_code', base64_decode($code))->first();
            if (count($user) <= 0) {
                Session::flash('error_msg', 'The entered link is not valid.');
                return redirect('/');
            }
            else{
                Session::put('user_account', $user);
                return redirect('/pwd-setting');
            }
            
        }
        else{
            Session::flash('error_msg', 'The entered link is not valid.');
            return redirect('/');
        }

    }

    public function resetPasswordAction() {
        $all = Input::all();
        $code = base64_decode($all['activation_code']);
//        print_R($code); die;
        /* getting admin details if exist from email */
        $arr_admin_detail = User::where('reset_password_code', $code)->first();

        if (count($arr_admin_detail) > 0) {
            if ($all['user_password'] != '') {
                /* sending admin credentail on admin account mail on user email */
                $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
                $password = crypt($all['user_password'], '$2y$12$' . $salt);
                if ($code != '' && $code != 0) {
                    $update_data = array('password' => $password);
                    User::where('reset_password_code', $code)->update($update_data);
                    //updating reset code
                    $update_data = array('reset_password_code' => '0');
                    User::where('reset_password_code', $code)->update($update_data);
                    Session::flash('success_msg', 'Password has been reset successfully.');
                }
                Session::forget('user_code');
                return redirect('/');
            }
        } else {
            Session::flash('error_msg', 'The entered link is not valid.');
            return redirect('/');
        }
    }

    public function getContactUs() {
        $user = Session ::get('user_account');
        $commonModel = new CommonModel();
        $userModel = new User();
        $data = $commonModel->commonFunction();
        $login = GlobalData::isLoggedIn();
        if ($login) {
            return redirect(url('/'));
        }
        $data['user_session'] = Session::get('user_account');
        $lang_id = 14;
        $arr_user_data = array();
         $user_id = $data['user_session']['id'];
        $arr_user_data = User::find($user_id);
        $data['header'] = array(
            "title" => 'Send Feedback ',
            "keywords" => '',
            "description" => ''
        );

        $user=Session::get('user_account');
        $site_title = "Contact Us";
        return view('Frontend.contact-us.contact-us', ['arr_user_data'=>$arr_user_data,'finalData' => $data, 'global' => $data['global'],'user_data' => $user]);
    }

    /* function to list all the ! contact us */

    public function listContactUs() {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
          /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('6', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        $date_format = 'Y-m-d';
        $arr_contact_us_list = contact_us::orderBy('id', 'DESC')->get();
        $date_format = 'Y-m-d';
        $site_title = "Manage Contact Us";
        return view('Backend.contact-us.list', ['arr_contact_us' => $arr_contact_us_list, 'site_title' => $site_title, 'date_format' => $date_format]);
    }

    public function viewContact($edit_id = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        
          /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('6', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        
        
        $date_format = 'Y-m-d';
        $arr_contact_us = contact_us::find(base64_decode($edit_id));
        $arr_contact_us_reply = \App\contact_us::with('values')->find(base64_decode($edit_id));
        $message = '';
        if (isset($arr_contact_us_reply) && count($arr_contact_us_reply) > 0) {
            foreach ($arr_contact_us_reply['values'] as $val) {
                $reply_msg = 'Please see below you replied on user message on ' . ($val['reply_date']) . ':- ' . PHP_EOL . PHP_EOL;
                $message .= $reply_msg . $val['message_body'] . PHP_EOL . PHP_EOL;
            }
        }
        $reply_msg = 'Please see user message on ' . ($arr_contact_us->date) . ':- ' . PHP_EOL . PHP_EOL;
        $message .= $reply_msg . $arr_contact_us->message . PHP_EOL . PHP_EOL;

        $site_title = "Message Detail";
        return view('Backend.contact-us.view', ['arr_contact' => $arr_contact_us, 'site_title' => $site_title, 'date_format' => $date_format, 'message' => $message]);
    }

    public function replyContact($edit_id = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        
          /* privilages */
         if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id',$user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('6', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /*ends here */
        
        $edit_id = $edit_id;
        $arr_contact_us = contact_us::find(base64_decode($edit_id));

        $title = "Reply Message";
        return view('Backend.contact-us.reply', ['arr_contact' => $arr_contact_us, 'site_title' => $title]);
    }

    public function replyAction($edit_id = '') {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::all();
        
        $contact_reply = new \App\contact_us_feedback_reply;
        $contact_reply->contact_id = $request['contact_id'];
        $contact_reply->message_to = $request['to'];
        $contact_reply->message_from_name = $request['from_name'];
        $contact_reply->message_from_email = $request['from_email'];
//        $contact_reply->message_subject = $request['subject'];
        $contact_reply->message_body = $request['message'];
        $contact_reply->reply_date = date("Y-m-d H:i:s");
        $contact_reply->save();
        
        $arr_update_data = array('reply_status' => '1');
        contact_us::where('id', $request['contact_id'])->update($arr_update_data);
         
//        echo "<pre>Doemn";
//        print_r($request);
//        die();
        $contents_array = array("email_conents" => $request['message']);
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        
// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message)use($site_title, $site_email, $request) {
            $message->from($site_email->value, $site_title->value);
            $message->to($request['to'])->subject($request['subject']);
        });
        Session::flash('message', 'Reply message sent successfully!');
        return redirect('admin/contact-us/list');
    }

    public function deleteContactUs() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $contact_ids = $request['checkbox'];

        if (!empty($contact_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($contact_ids) > 0) {
                foreach ($contact_ids as $val) {

                    contact_us::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Contact us info has been deleted successfully.');
        return redirect('admin/contact-us/list');
    }

}
