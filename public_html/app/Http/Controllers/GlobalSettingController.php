<?php

namespace App\Http\Controllers;

use DB;
use App\global_settings;
use App\trans_global_settings;
use Illuminate\Http\Request;
use Session;
use Auth;

class GlobalSettingController extends Controller {

    public function __construct() {
        
    }

    /* function to list all the global setteings parameter */

    public function listGlobalSettings() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
         /* privilages */
         if ($user->role_id != '1') {
           
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            
        }
        /*ends here */
        $globalArr = global_settings::with('values')->get();     
//        echo '<pre>';
//        print_r($globalArr->values());
//        die;
        $site_title = "Manage Global Settings";
        return view('Backend.global-setting.list', ['global_settings' => $globalArr->values(),'site_title'=>$site_title]);
    }

    /* function to edit the global settings parameter */

    public function editGlobalSettings($edit_id = '', Request $request) {
        $value = $request->get('value');
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
         /* privilages */
         if ($user->role_id != '1') {
           
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                 return redirect()->guest('admin/dashboard');
                exit();
            
        }
        /*ends here */
        $globalArr = global_settings::with('values')->find($edit_id);
        $site_title = "Update Global Setting";
        return view('Backend.global-setting.edit', ['global_setting_info' => $globalArr,'site_title'=>$site_title]);
    }

    //updating the global setting values.
    public function postGlobalSettings(Request $request) {
        $edit_id = $request->input('global_name_id');
        if (!empty($edit_id) && $edit_id != 0) {
            if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
            $user_privileges = array();

            $value = $request->input('value', '');
            trans_global_settings::where('global_name_id', $edit_id)
                    ->update(['value' => $value]);
             /* updating the global settings to the file for the default language English */
            Session::flash('message', 'Global setting parameter value has been updated successfully.');
            return redirect('admin/global-settings/list');
        } else {
            return redirect('admin/global-settings/list');
        }
    }

    /* function to edit the global settings parameter */

    public function editParameterLanguage($edit_id = '') {

        $commonModel = new CommonModel();
        $GlobalSettingModel = new GlobalSettingModel();
        $login = GlobalData::isLoggedIn();
        if ($login == "inactive") {
            return redirect('backend/login')->with('message', FlashMessage::DisplayAlert('Your account is currently inactive, Please contact your Administrator for additional details', 'danger'));
        } else if ($login == "blocked") {
            return redirect('backend/login')->with('message', FlashMessage::DisplayAlert('Your account is currently blocked, Please contact your Administrator for additional details', 'danger'));
        } else if ($login == "deleted") {
            return redirect('backend/login')->with('message', FlashMessage::DisplayAlert('Not Authorized to see page content', 'danger'));
        }
        #get global data

        $data = $commonModel->commonFunction();
        if ($data['user_account']['role_id'] != 1) {
            /* an admin which is not super admin not privileges to access global settings */
            /* setting session for displaying notiication message. */
            return redirect('backend/dashboard')->with('message', FlashMessage::DisplayAlert("You doesn't have priviliges to manage global settings!", 'danger'));
        }

        $all = Input::all();
        if ($all['lang_id'] != '') {
            if ($all['value'] != "") {
                if ($all['edit_id'] != '' && $all['lang_id'] != '') {
                    $arr_to_update = array(
                        "value" => ($all['value'])
                    );

                    /* condition to update record	 */
                    $condition_array = array('lang_id' => intval($all['lang_id']), 'global_name_id' => intval(base64_decode($all['edit_id'])));
                    $insert_array = array('lang_id' => intval($all['lang_id']), 'global_name_id' => intval(base64_decode($all['edit_id'])), "value" => ($all['value']));

                    /*                     * * check if record is already exist * */
                    $arr_global_setting = $commonModel->getRecords('trans_global_settings', '', $condition_array);

                    /*                     * * if we does not have record then we will be inserting a new one else we just need to udate the value parameter ** */
                    if (count($arr_global_setting) > 0) {
                        $commonModel->updateRow('trans_global_settings', $arr_to_update, $condition_array);
                    } else {
                        $commonModel->insertRow('trans_global_settings', $insert_array);
                    }

                    /* updating the global setttings parameter value into database */
                    $commonModel->updateRow('trans_global_settings', $arr_to_update, $condition_array);
                    /* updating the global settings Parameeter values to the file according to language. */

                    /* setting session for displaying notiication message. */
                    return redirect('backend/global-settings/list')->with('message', FlashMessage::DisplayAlert("Global setting parameter updated successfully!", 'success'));
                }
            }
        }

        $data['title'] = "Edit Global Settings";
        if (($edit_id != '')) {
            $data['edit_id'] = $edit_id;
            $globalArr = $GlobalSettingModel->getGlobalSettingsById($edit_id);

            /* getting all the active languages from the database */
            $data['arr_languages'] = $commonModel->getAllLanguages();
            return view('admin.global-setting.edit-language-parameter')
                            ->with('title', 'Edit Global Settings')
                            ->with('finalData', $data)
                            ->with('arr_languages', $data['arr_languages'])
                            ->with('arr_global_settings', $globalArr[0]);
        } else {
            /* go to the page not found */
        }
    }

}
