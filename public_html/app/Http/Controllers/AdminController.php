<?php

namespace App\Http\Controllers;

use Mail;
use DB;
use App\AdminUser;
use App\BlackList;
use App\Ethnicity;
use App\Body;
use App\Eye;
use App\Hair;
use App\role;
use App\CommonModel;
use App\email_templates;
use App\email_template_macros;
use App\global_settings;
use App\trans_global_settings;
use Session;
use App\role_privileges;
use App\Institute;
use Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use App\SocialLink;
use App\UserAcademics;
use App\UserCertification;
use App\UserAchievements;
use App\UserInterest;
use App\UserExperience;
use App\UserLanguage;
use App\UserEducationalDocuments;
use App\Traits\sendMail;
use Request;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Admin Panel Controller
      |--------------------------------------------------------------------------
     */

    use sendMail;
    public $recordPerPage = 25;

    public function __construct() {
        $this->middleware('auth', ['except' => ['adminLogin','getAdminLogout','sendEmailAsync']]);
    }

    /**
     * Display listing of the resource
     * 
     * @return Response
     */
    // This action grab user information and dislay it on the admin panel index

    public function adminLogin() {
        return view('Backend.login.admin-login', ["global_values" => $data]);
    }

    public function home() {
        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $site_title = "Welcome to Admin Dashboard";
//        $arr_admin_list = AdminUser::where('user_type','2');        
        $arr_admin_list = DB::table('mst_users')
                ->where('user_type', '2')
                ->select('*')
                ->get();
        $arr_student_list = DB::table('mst_users')
                ->where('user_type', '1')
                ->select('*')
                ->get();
        $arr_professional_list = DB::table('mst_users')
                ->where('user_type', '3')
                ->select('*')
                ->get();
        $arr_company_list = DB::table('mst_users')
                ->where('user_type', '4')
                ->select('*')
                ->get();

        $Tracker = DB::table('mst_users');
        $where = "created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND NOW()";
        $Tracker->whereRaw($where);
//        $Tracker->orWhere('user_type','!=',2);
        $Tracker->select('*');
        $arr_24hr_user = $Tracker->get();

        $Tracker1 = DB::table('mst_users');
        $where = "created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND NOW()";
        $Tracker1->whereRaw($where);
//        $Tracker1->whereOr('user_type','!=','2');
        $Tracker1->select('*');
        $arr_7day_user = $Tracker1->get();

        $Tracker2 = DB::table('mst_users');
        $where = "created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND NOW()";
        $Tracker2->whereRaw($where);
        $Tracker2->select('*');
        $arr_30day_user = $Tracker2->get();

        $Tracker3 = DB::table('mst_users');
        $Tracker3->where('user_type', '!=', '2');
        $Tracker3->select('*');
        $arr_total_user_count = $Tracker3->count();

        $Tracker4 = DB::table('mst_users')->where([
                ['user_type','!=','2'],
                ['email_verified','!=','0'],
            ])->select('*');
        $arr_total_verified_users = $Tracker4->count();

        return view('Backend.dashboard', [
            'site_title' => $site_title, 'arr_24hr_user' => count($arr_24hr_user),
            'arr_7day_user' => count($arr_7day_user), 'arr_30day_user' => count($arr_30day_user),
            'total_user' => $arr_admin_list, 'total_student' => count($arr_student_list),
            'total_professional' => count($arr_professional_list), 'total_company' => count($arr_company_list),
            'total_admin' => count($arr_admin_list), 'role_id' => $user->role_id, 'arr_total_user_count' => $arr_total_user_count,
            'arr_total_verified_users' => $arr_total_verified_users
        ]);
    }

    /* function to list all the admin users */

    public function listAdmin() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != '1') {

            Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
            return redirect()->guest('admin/dashboard');
            exit();
        }
        /* ends here */

        $arr_admin_list = AdminUser::where('user_type', '=', '2')->get();

        $site_title = "Manage Admin Users";
        return view('Backend.admin.list', ['arr_admin_list' => $arr_admin_list, 'site_title' => $site_title]);
    }

    public function listEthnicityType() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $arr_ethnicity_list = Ethnicity::orderBy('id', 'DESC')->get();
        $site_title = "Manage Ethnicity Type";
        return view('Backend.ethnicity.list', ['arr_ethnicity_list' => $arr_ethnicity_list, 'site_title' => $site_title]);
    }

    public function listBodyType() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $arr_body_list = Body::orderBy('id', 'DESC')->get();
        $site_title = "Manage Body Type";
        return view('Backend.body.list', ['arr_body_list' => $arr_body_list, 'site_title' => $site_title]);
    }

    public function listEyeColor() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        }
        /* ends here */
        $arr_eye_list = Eye::orderBy('id', 'DESC')->get();
        $site_title = "Manage Eye Color";
        return view('Backend.eye.list', ['arr_eye_list' => $arr_eye_list, 'site_title' => $site_title]);
    }

    public function listHairColor() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $arr_hair_list = Hair::orderBy('id', 'DESC')->get();
        $site_title = "Manage Hair Color";
        return view('Backend.hair.list', ['arr_hair_list' => $arr_hair_list, 'site_title' => $site_title]);
    }

    public function listSendEmail()
    {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        return view('Backend.send-email.list', [
            
        ]);
    }
    
    public function editEmail()
    {
        $request = Input::all();
        if(!isset($request['email-temp']) || !isset($request['userids']))
        {
            return redirect()->guest('/');
        }
        
        $email_template_id = $request['email-temp'];
        $userids = $request['userids'];
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        
        $edit_template_data = email_templates::with('values')->find($email_template_id);
        $all_macros = email_template_macros::all();
        $site_title = "Edit Email Templates";
        return view('Backend.email-template.edit-template', ['userids'=>$userids, 'parentTemplate'=>'Send Emails', 'arr_email_template_details' => $edit_template_data, 'title_for_layout' => 'data pawar', 'arr_macros' => $all_macros, 'email_template_id' => $email_template_id, 'site_title' => $site_title]);

    }
    
    public function sendEmail()
    {
        $request = Input::all();
        $url = url('/').'/admin/send-emails/send-async';
        $param = array( 'userids' => urlencode($request['userids']), 'email_temp_id' => urlencode($request['email_template_hidden_id']), 'input_subject' => urlencode($request['input_subject']), 'text_content' => urlencode($request['text_content']));
        $post_string = http_build_query($param); 
        $parts = parse_url($url);
        if($parts['scheme'] == 'https')
        {
            $fp = fsockopen('ssl://'.$parts['host'], 443, $errno, $errstr, 30);
        }
        else
        {
            $fp = fsockopen($parts['host'], 80, $errno, $errstr, 30);
        }
        
        
        if(!$fp)
        {
            //echo "Some thing Problem";    
        }
        $out = "POST ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;
        fwrite($fp, $out);
        fclose($fp);
        Log::useDailyFiles(storage_path().'/logs/daily.log');
        Log::info(['Request'=> $url, 'param' => $post_string, 'error' => $errstr]); 
        
        return redirect()->guest('/admin/send-emails/list');
        
    }
    
    
    public function sendEmailAsync()
    {             
        $request = Input::all();       
        $this->sendMailToMultipleUsers(urldecode($request['userids']), urldecode($request['email_temp_id']), urldecode($request['input_subject']), urldecode($request['text_content']));      
    }


    public function listStudentUser() {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $requestData = Input::all();
        $studentObj = AdminUser::where('user_type', '=', '1')->orderBy('id', 'DESC');
        if ($requestData && $requestData['search-qry'] != '') {
            $studentObj = $studentObj->whereRaw("(concat(first_name,' ', last_name) like '%" . $requestData['search-qry'] .
                    "%' or email like '%" . $requestData['search-qry'] . "%')");
        }
        if ($requestData && $requestData['search-status'] != '') {
            $studentObj = $studentObj->where('user_status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }
        $arr_student_list = $studentObj->paginate($recordPerPage);
        $site_title = "Manage Students";
        return view('Backend.student.list', [
            'arr_student_list' => $arr_student_list, 'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
    }

    public function listProfessionalUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        //$arr_professional_list = AdminUser::where('user_type', '=', '3')->orderBy('id', 'DESC')->get();
        $requestData = Input::all();
        $professionalObj = AdminUser::where('user_type', '=', '3')->orderBy('id', 'DESC');
        if ($requestData && $requestData['search-qry'] != '') {
            $professionalObj = $professionalObj->whereRaw("(concat(first_name,' ', last_name) like '%" . $requestData['search-qry'] .
                "%' or email like '%" . $requestData['search-qry'] . "%')");
        }
        if ($requestData && $requestData['search-status'] != '') {
            $professionalObj = $professionalObj->where('user_status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }
        $arr_professional_list = $professionalObj->paginate($recordPerPage);
        $site_title = "Manage Professional Users";
        //return view('Backend.professional.list', ['arr_professional_list' => $arr_professional_list, 'site_title' => $site_title]);
        return view('Backend.professional.list', [
            'arr_professional_list' => $arr_professional_list, 'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
    }

    public function listCompanyUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        //$arr_company_list = AdminUser::where('user_type', '=', '4')->orderBy('id', 'DESC')->get();
        $requestData = Input::all();
        $companyObj = AdminUser::where('user_type', '=', '4')->orderBy('id', 'DESC');
        if ($requestData && $requestData['search-qry'] != '') {
            $companyObj = $companyObj->whereRaw("(concat(first_name,' ', last_name) like '%" . $requestData['search-qry'] .
                "%' or email like '%" . $requestData['search-qry'] . "%')");
        }
        if ($requestData && $requestData['search-status'] != '') {
            $companyObj = $companyObj->where('user_status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }
        $arr_company_list = $companyObj->paginate($recordPerPage);
        $site_title = "Manage Company Users";
        //return view('Backend.company.list', ['arr_company_list' => $arr_company_list, 'site_title' => $site_title]);
        return view('Backend.company.list', [
            'arr_company_list' => $arr_company_list, 'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
    }

    /*     * blacklist actions  ** */

    public function listblacklistnames() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */


        $site_title = "Manage Blacklist Names";
        $blacklist = BlackList::get();

        return view('Backend.blacklist.list', ['blacklist' => $blacklist, 'site_title' => $site_title]);
    }

    public function addNewBlacklistname() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $new_bname = Input::get('blacklist_name');

        if (isset($_POST['blacklist_name'])) {

            $check_detail = BlackList::where('blacklist_name', $new_bname)->first();
            if (!empty($check_detail) || $check_detail != null) {


                echo json_encode(array("error" => '0', "username" => $new_bname, 'msg' => 'Username alredy exists'));
                exit();
            } else {
                $user = new BlackList;
                $user->blacklist_name = $new_bname;
                $user->save();


                echo json_encode(array("success" => '1', "username" => $new_bname, 'msg' => 'Username added successfully.'));
                exit();
            }
        }
    }

    public function deleteBlacklist() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $name_ids = $request['checkbox'];
        if (!empty($name_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($name_ids) > 0) {
                foreach ($name_ids as $val) {

                    BlackList::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Blacklisted Word has been deleted successfully.');
        return redirect('admin/users/list/blacklist');
    }

    public function editBlacklist($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $b_name = BlackList::find(base64_decode($edit_id));
        $site_title = "Update Blacklist Name";
        $edit_id = $edit_id;
        return view('Backend.blacklist.edit', ['b_name' => $b_name, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function updateBlacklist() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $request = Input::all();
        $edit_id = $request['edit_id'];

        $arr_request_data = array(
            'blacklist_name' => $request['user_name']
        );

        BlackList::where('id', $edit_id)->update($arr_request_data);

        Session::flash('message', 'Name has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/blacklist');
    }

    public function listGeneralUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $arr_general_user_list = AdminUser::where('user_type', '=', '6')->orderBy('id', 'DESC')->get();
        $site_title = "Manage General User";


        return view('Backend.general-user.list', ['arr_general_user_list' => $arr_general_user_list, 'site_title' => $site_title]);
    }

    public function listRegisteredUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $arr_registered_user_list = AdminUser::where('user_type', '=', '0')->get();
        $site_title = "Manage Registered User";


        return view('Backend.registered-user.list', ['arr_registered_user_list' => $arr_registered_user_list, 'site_title' => $site_title]);
    }

    public function changeStatusEmail() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            /* updating the user status. */
            $arr_to_update = array("email_verified" => $request['user_email_verified']);
            /* updating the user status  value into database */
            AdminUser::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "User record updated successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    /* function to edit the edit admin */

    public function editAdmin($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != '1') {

            Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
            return redirect()->guest('admin/dashboard');
            exit();
        }
        /* ends here */
        $arr_admin_detail = AdminUser::with('values')->find(base64_decode($edit_id));
        $arr_roles = role::all();
        $site_title = "Update Admin User";
        $edit_id = $edit_id;
        return view('Backend.admin.edit', ['arr_admin_detail' => $arr_admin_detail, 'site_title' => $site_title, 'arr_roles' => $arr_roles, 'edit_id' => $edit_id]);
    }

    /* function to add admin user */

    public function addAdmin(Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        /* privilages */
        if ($user->role_id != '1') {

            Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
            return redirect()->guest('admin/dashboard');
            exit();
        }
        /* ends here */
        $arr_roles = role::all();
        $site_title = "Add Admin User";
        return view('Backend.admin.add', ['site_title' => $site_title, 'arr_roles' => $arr_roles]);
    }

    public function updateAdmin() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::all();
        $user_id = $request['edit_id'];

        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_admin_detail = AdminUser::with('values')->find($user_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */
        if ($request['email'] == $request['old_email']) {
            if ($request['user_status'] != "") {
                $status = $request['user_status'];
                $email_verified = '1';
            } else {
                $status = '0';
                $email_verified = '0';
            }
            $activation_code = $arr_admin_detail->activation_code;
        } else {
            if ($arr_admin_detail->role_id != 1) {
                $status = '0';
                $email_verified = '0';
                $activation_code = time();
            } else {
                $status = '1';
                $email_verified = '1';
                $activation_code = time();
            }
        }

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);
            $arr_request_data = array(
                'user_name' => $request['user_name'],
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'password' => $hash_password,
                'gender' => $request['gender'],
                'role_id' => $request['role_id'],
                'user_status' => $request['user_status']
            );
        } else {
            $arr_request_data = array(
                'user_name' => $request['user_name'],
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'gender' => $request['gender'],
                'role_id' => $request['role_id'],
                'user_status' => $request['user_status']
            );
        }
        AdminUser::where('id', $user_id)->update($arr_request_data);

        if ($request['email'] == $request['old_email']) {
            if ($arr_admin_detail['role_id'] != 1) {
                /* sending account updating mail to user */
                $admin_login_link = '<a href="' . url('/') . '/auth/admin/login" target="_new">Log in to ' . url('/') . '/administration</a>';

                $macros_array_detail = array();
                $macros_array_detail = email_template_macros::all();
                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }

                $reserved_words = array();
                if (isset($hash_password) && $hash_password != '') {
                    $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
                    $content = $email_contents['email_template_content'];

                    $reserved_arr = array
                        ("||SITE_TITLE||" => stripslashes($site_title->value),
                        "||SITE_PATH||" => url('/'),
                        "||USER_NAME||" => $request['user_name'],
                        "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                        "||ADMIN_EMAIL||" => $request['email'],
                        "||PASSWORD||" => $request['user_password'],
                        "||ADMIN_LOGIN_LINK||" => $admin_login_link
                    );
                } else {
                    $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
                    $content = $email_contents['email_template_content'];
                    $reserved_arr = array
                        ("||SITE_TITLE||" => stripslashes($site_title->value),
                        "||SITE_PATH||" => url('/'),
                        "||USER_NAME||" => $request['user_name'],
                        "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                        "||ADMIN_EMAIL||" => $request['email'],
                        "||ADMIN_LOGIN_LINK||" => $admin_login_link
                    );
                }

                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                foreach ($reserved_words as $k => $v) {
                    $content = str_replace($k, $v, $content);
                }
                $contents_array = array("email_conents" => $content);
                $data['site_email'] = $site_email->value;
                $data['site_title'] = $site_title->value;

                // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
                Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $user, $contents_array, $data, $email_contents) {
                    $message->from($data['site_email'], $data['site_title']);
                    $message->to($email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
                }); // End Mail
            }
        } else {

            /* sending account verification link mail to user */
            $lang_id = 14;
            $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['user_name'],
                    "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['user_name'],
                    "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            }

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $user, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
            }); // End Mail
        }

        Session::flash('message', 'Profile has been updated successfully!');
        return redirect(url('/') . '/admin/admin/list');
    }

    public function updateProfile() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::all();
        $user_id = Auth::user()->id;
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_admin_detail = AdminUser::with('values')->find($user_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */
        if ($request['email'] == $request['old_email']) {
            if ($request['user_status'] != "") {
                $status = $request['user_status'];
                $email_verified = '1';
            } else {
                $status = '0';
                $email_verified = '0';
            }
            $activation_code = $arr_admin_detail->activation_code;
        } else {

            if ($arr_admin_detail->role_id != 1) {
                $status = '0';
                $email_verified = '0';
                $activation_code = time();
            } else {
                $status = '1';
                $email_verified = '1';
                $activation_code = time();
            }
        }

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);
            $arr_request_data = array(
                'user_name' => $request['user_name'],
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'password' => $hash_password,
                'gender' => $request['gender'],
                'role_id' => $request['role_id'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        } else {
            $arr_request_data = array(
                'user_name' => $request['user_name'],
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'gender' => $request['gender'],
                'role_id' => $request['role_id'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        }

        AdminUser::where('id', $user_id)->update($arr_request_data);

        if ($request['email'] == $request['old_email']) {
            if ($arr_admin_detail['role_id'] != 1) {
                /* sending account updating mail to user */
                $admin_login_link = '<a href="' . url('/') . '/auth/admin/login" target="_new">Log in to ' . url('/') . 'administration</a>';

                $macros_array_detail = array();
                $macros_array_detail = email_template_macros::all();
                $macros_array = array();
                foreach ($macros_array_detail as $row) {
                    $macros_array[$row['macros']] = $row['value'];
                }

                $reserved_words = array();
                if (isset($hash_password) && $hash_password != '') {
                    $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
                    $content = $email_contents['email_template_content'];

                    $reserved_arr = array
                        ("||SITE_TITLE||" => stripslashes($site_title->value),
                        "||SITE_PATH||" => url('/'),
                        "||USER_NAME||" => $request['user_name'],
                        "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                        "||ADMIN_EMAIL||" => $request['email'],
                        "||PASSWORD||" => $request['user_password'],
                        "||ADMIN_LOGIN_LINK||" => $admin_login_link
                    );
                } else {
                    $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
                    $content = $email_contents['email_template_content'];
                    $reserved_arr = array
                        ("||SITE_TITLE||" => stripslashes($site_title->value),
                        "||SITE_PATH||" => url('/'),
                        "||USER_NAME||" => $request['user_name'],
                        "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                        "||ADMIN_EMAIL||" => $request['email'],
                        "||ADMIN_LOGIN_LINK||" => $admin_login_link
                    );
                }

                $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
                foreach ($reserved_words as $k => $v) {
                    $content = str_replace($k, $v, $content);
                }
                $contents_array = array("email_conents" => $content);
                $data['site_email'] = $site_email->value;
                $data['site_title'] = $site_title->value;

                // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
                Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $user, $contents_array, $data, $email_contents) {
                    $message->from($data['site_email'], $data['site_title']);
                    $message->to($email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
                }); // End Mail
            }
        } else {

            /* sending account verification link mail to user */
            $lang_id = 14;
            $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
            $content = $email_contents['email_template_content'];

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['user_name'],
                    "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['user_name'],
                    "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            }


            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $user, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
            }); // End Mail
            Session::flash('admin_logout', 'Your account is inactivated for email verification. Account activation link has been sent on updated email address.');
            Auth::logout();
            return redirect()->guest('/auth/admin/login');
        }

        return redirect(url('/') . '/admin/admin/profile');
    }

    public function addAdminAction() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $request = Input::all();
        $activation_code = time();
        $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
        $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);
        $user = new AdminUser;
        $user->user_name = $request['user_name'];
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->password = $hash_password;
        $user->gender = $request['gender'];
        $user->role_id = $request['role_id'];
        $user->user_type = '2';
        $user->user_status = '0';
        $user->email_verified = '0';
        $user->ip_address = $_SERVER['REMOTE_ADDR'];
        $user->activation_code = $activation_code;
        $user->created_at = date("Y-m-d H:i:s");
        $user->save();
        /* inserting admin details into the dabase */
        $last_insert_id = $user->id;
        /* Activation link  */
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';
        /* setting reserved_words for email content */
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();
        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'admin-added', 'lang_id' => '14'))->first();

        //$email_contents = end($email_contents);
        $content = $email_contents['email_template_content'];

        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($site_title->value),
            "||SITE_PATH||" => $site_path,
            "||USER_NAME||" => $request['user_name'],
            "||ADMIN_NAME||" => $request['first_name'] . ' ' . $request['last_name'],
            "||ADMIN_EMAIL||" => $request['email'],
            "||PASSWORD||" => $request['user_password'],
            "||ADMIN_ACTIVATION_LINK||" => $activation_link
        );
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);

        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }

        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);
        $data['site_email'] = $site_email->value;
        $data['site_title'] = $site_title->value;
        // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($user, $contents_array, $data, $email_contents) {
            $message->from($data['site_email'], $data['site_title']);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
        }); // End Mail
        Session::flash('message', 'Admin added successfully and verification email sent to email address!');
        return redirect('admin/admin/list');
    }

    /* function to check existancs of user name */

    public function checkAdminUsername() {

        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user name already exist or not for edit Admin */
            if (strtolower($all['user_name']) == strtolower($all['old_username'])) {
                echo "true";
            } else {
                $arr_admin_detail = AdminUser::where('user_name', $all['user_name'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user name already exist or not for add admin  */
            $arr_admin_detail = AdminUser::where('user_name', $all['user_name'])->first();

            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function deleteUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        $admin = User::where('id', '=', $request['checkbox'])->first();
        $user = new AdminUser;
        $user->first_name = $admin['first_name'];
        $user->last_name = $admin['last_name'];
        $user->email = $admin['email'];
        if (!empty($users_ids)) {
            $site_email = trans_global_settings::where('id', '1')->first();
            $site_title = trans_global_settings::where('id', '2')->first();
            /* setting reserved_words for email content */
            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }
            $email_contents = email_templates::where(array('email_template_title' => 'admin-deleted', 'lang_id' => '14'))->first();

            //$email_contents = end($email_contents);
            $content = $email_contents['email_template_content'];
            $reserved_words = array();
            $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
            $reserved_arr = array(
                "||SITE_TITLE||" => stripslashes($site_title->value),
                "||SITE_PATH||" => $site_path,
                "||ADMIN_NAME||" => $admin['user_name'],
            );
            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);

            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }

            /* getting mail subect and mail message using email template title and lang_id and reserved works */
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;
            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($user, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($user->email, $user->first_name . ' ' . $user->last_name)->subject($email_contents['email_template_subject']);
            }); // End Mail
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
            Session::flash('message', 'User info has been deleted successfully.');
            return redirect('admin/admin/list');
        }
    }

    public function deleteBodyType() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    Body::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Body info has been deleted successfully.');
        return redirect('admin/bio/list/body');
    }

    public function deleteEyeColor() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    Eye::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Eye info has been deleted successfully.');
        return redirect('admin/bio/list/eye');
    }

    public function deleteHairColor() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    Hair::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Hair info has been deleted successfully.');
        return redirect('admin/bio/list/hair');
    }

    public function deleteEthnicityType() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    Ethnicity::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Ethnicity info has been deleted successfully.');
        return redirect('admin/bio/list/ethnicity');
    }

    public function deleteStudentUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'User info has been deleted successfully.');
        return redirect('admin/users/list/student');
    }

    public function deleteProfessionalUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Account has been deleted successfully.');
        return redirect('admin/users/list/professional');
    }

    public function deleteCompanyUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Account  has been deleted successfully.');
        return redirect('admin/users/list/company');
    }

    public function deleteGeneralUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'General user info has been deleted successfully.');
        return redirect('admin/users/list/general_user');
    }

    public function deleteRegisteredUser() {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $users_ids = $request['checkbox'];
        if (!empty($users_ids)) {
            //now insetring all all privilages for selecte role.
            if (count($users_ids) > 0) {
                foreach ($users_ids as $val) {

                    AdminUser::where('id', $val)->delete();
                }
            }
        }
        Session::flash('message', 'Registered user info has been deleted successfully.');
        return redirect('admin/users/list/registered_user');
    }

    public function adminProfile() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $edit_id = Auth::user()->id;
        $arr_admin_detail = AdminUser::with('values')->find($edit_id);
        $site_title = "Profile";
        return view('Backend.admin.admin-profile', ['arr_admin_detail' => $arr_admin_detail, 'site_title' => $site_title]);
    }

    public function editProfile() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $edit_id = Auth::user()->id;
        $arr_admin_detail = AdminUser::with('values')->find($edit_id);
        $site_title = "Edit Admin User";
        return view('Backend.admin.edit-profile', ['arr_admin_detail' => $arr_admin_detail, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    /* function to check existancs of admin email */

    public function checkAdminEmail() {
        $all = Input::all();
        if (isset($all['type'])) {
            /* checking admin user email already exist or not for edit Admin */
            if (strtolower($all['email']) == strtolower($all['old_email'])) {
                echo "true";
            } else {
                $arr_admin_detail = AdminUser::where('email', $all['email'])->first();
                if (empty($arr_admin_detail)) {
                    echo "true";
                } else {
                    echo "false";
                }
            }
        } else {
            /* checking admin user email already exist or not for add admin  */
            $arr_admin_detail = AdminUser::where('email', $all['email'])->first();

            if (empty($arr_admin_detail)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function changeStatus() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            /* updating the user status. */
            $arr_to_update = array("user_status" => $request['user_status']);
            /* updating the user status  value into database */
            AdminUser::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    public function changeEthnicityStatus() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            $arr_to_update = array("status" => $request['user_status']);
            Ethnicity::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    public function changeBodyStatus() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            $arr_to_update = array("status" => $request['user_status']);
            Body::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    public function changeEyeStatus() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            $arr_to_update = array("status" => $request['user_status']);
            Eye::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    public function changeHairStatus() {
        $request = Input::all();
        if ($request['user_id'] != "") {
            $arr_to_update = array("status" => $request['user_status']);
            Hair::where('id', $request['user_id'])->update($arr_to_update);
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        } else {
            /* if something going wrong providing error message.  */
            echo json_encode(array("error" => "1", "error_message" => "Sorry, your request can not be fulfilled this time. Please try again later"));
        }
    }

    /* function to edit the edit Student */

    public function editEthnicity($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $arr_ethnicity_type = Ethnicity::find(base64_decode($edit_id));
//        $arr_ethnicity_type = DB::table('mst_ethnicity_type')
//                ->select(array('id', 'ethnicity_type'))
//                ->get();

        $site_title = "Update Ethnicity";
        $edit_id = $edit_id;
        return view('Backend.ethnicity.edit', ['arr_ethnicity_type' => $arr_ethnicity_type, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function editBody($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $arr_body_type = Body::find(base64_decode($edit_id));


        $site_title = "Update Body";
        $edit_id = $edit_id;
        return view('Backend.body.edit', ['arr_body_type' => $arr_body_type, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function editEye($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */


        $arr_eye_type = Eye::find(base64_decode($edit_id));


        $site_title = "Update Eye";
        $edit_id = $edit_id;
        return view('Backend.eye.edit', ['arr_eye_type' => $arr_eye_type, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function editHair($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $arr_hair_type = Hair::find(base64_decode($edit_id));


        $site_title = "Update Hair";
        $edit_id = $edit_id;
        return view('Backend.hair.edit', ['arr_hair_type' => $arr_hair_type, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function editStudent($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */


        $arr_student_detail = AdminUser::find(base64_decode($edit_id));

        $arr_ethnicity_type = DB::table('mst_ethnicity_type')
                ->select(array('id', 'ethnicity_type'))
                ->get();
        $arr_body_type = DB::table('mst_body_type')
                ->select(array('id', 'body_type'))
                ->get();
        $arr_eye_color = DB::table('mst_eye_color')
                ->select(array('id', 'eye_color'))
                ->get();
        $arr_hair_color = DB::table('mst_hair_color')
                ->select(array('id', 'hair_color'))
                ->get();
        $site_title = "Update Student";
        $edit_id = $edit_id;

        return view('Backend.student.edit', ['arr_student_detail' => $arr_student_detail, 'site_title' => $site_title, 'edit_id' => $edit_id, 'arr_ethnicity_type' => $arr_ethnicity_type, 'arr_body_type' => $arr_body_type, 'arr_eye_color' => $arr_eye_color, 'arr_hair_color' => $arr_hair_color]);
    }

    public function editProfessional($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $arr_professional_detail = AdminUser::find(base64_decode($edit_id));
        $arr_ethnicity_type = DB::table('mst_ethnicity_type')
                ->select(array('id', 'ethnicity_type'))
                ->get();
        $arr_body_type = DB::table('mst_body_type')
                ->select(array('id', 'body_type'))
                ->get();
        $arr_eye_color = DB::table('mst_eye_color')
                ->select(array('id', 'eye_color'))
                ->get();
        $arr_hair_color = DB::table('mst_hair_color')
                ->select(array('id', 'hair_color'))
                ->get();

        $site_title = "Update Professional User";
        $edit_id = $edit_id;
        return view('Backend.professional.edit', ['arr_professional_detail' => $arr_professional_detail, 'site_title' => $site_title, 'edit_id' => $edit_id, 'arr_ethnicity_type' => $arr_ethnicity_type, 'arr_body_type' => $arr_body_type, 'arr_eye_color' => $arr_eye_color, 'arr_hair_color' => $arr_hair_color]);
    }

    public function editCompany($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */


        $arr_company_detail = AdminUser::find(base64_decode($edit_id));
        $arr_ethnicity_type = DB::table('mst_ethnicity_type')
                ->select(array('id', 'ethnicity_type'))
                ->get();
        $arr_body_type = DB::table('mst_body_type')
                ->select(array('id', 'body_type'))
                ->get();
        $arr_eye_color = DB::table('mst_eye_color')
                ->select(array('id', 'eye_color'))
                ->get();
        $arr_hair_color = DB::table('mst_hair_color')
                ->select(array('id', 'hair_color'))
                ->get();

        $site_title = "Update Company";
        $edit_id = $edit_id;
        return view('Backend.company.edit', ['arr_company_detail' => $arr_company_detail, 'site_title' => $site_title, 'edit_id' => $edit_id, 'arr_ethnicity_type' => $arr_ethnicity_type, 'arr_body_type' => $arr_body_type, 'arr_eye_color' => $arr_eye_color, 'arr_hair_color' => $arr_hair_color]);
    }

    public function editGeneralUser($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $arr_general_user_detail = AdminUser::find(base64_decode($edit_id));

        $site_title = "Update General User";
        $edit_id = $edit_id;
        return view('Backend.general-user.edit', ['arr_general_user_detail' => $arr_general_user_detail, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function editRegisteredUser($edit_id = '', Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $arr_registered_user_detail = AdminUser::find(base64_decode($edit_id));

        $site_title = "Update Registered User";
        $edit_id = $edit_id;
        return view('Backend.registered-user.edit', ['arr_registered_user_detail' => $arr_registered_user_detail, 'site_title' => $site_title, 'edit_id' => $edit_id]);
    }

    public function updateEthnicity() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }



        $request = Input::all();
        $ethnicity_id = $request['edit_id'];
        $arr_request_data = array(
            'ethnicity_type' => $request['bio_detail'],
            'status' => $request['status']
        );

        Ethnicity::where('id', $ethnicity_id)->update($arr_request_data);
        Session::flash('message', 'Ethnicity detail has been updated successfully!');
        return redirect(url('/') . '/admin/bio/list/ethnicity');
    }

    public function updateBody() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }



        $request = Input::all();
        $body_id = $request['edit_id'];
        $arr_request_data = array(
            'body_type' => $request['bio_detail'],
            'status' => $request['status']
        );

        Body::where('id', $body_id)->update($arr_request_data);
        Session::flash('message', 'Body detail has been updated successfully!');
        return redirect(url('/') . '/admin/bio/list/body');
    }

    public function updateEye() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $request = Input::all();
        $eye_id = $request['edit_id'];
        $arr_request_data = array(
            'eye_color' => $request['bio_detail'],
            'status' => $request['status']
        );



        Eye::where('id', $eye_id)->update($arr_request_data);
        Session::flash('message', 'Eye detail has been updated successfully!');
        return redirect(url('/') . '/admin/bio/list/eye');
    }

    public function updateHair() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }



        $request = Input::all();
        $hair_id = $request['edit_id'];
        $arr_request_data = array(
            'hair_color' => $request['bio_detail'],
            'status' => $request['status']
        );

        Hair::where('id', $hair_id)->update($arr_request_data);
        Session::flash('message', 'Hair detail has been updated successfully!');
        return redirect(url('/') . '/admin/bio/list/hair');
    }

    public function updateStudent() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        $request = Input::all();



        $student_id = $request['edit_id'];

        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_student_detail = AdminUser::find($student_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */

        if ($request['user_status'] != "") {
            $status = $request['user_status'];
        } else {
            $status = '0';
        }

        $dob = date('Y-m-d', strtotime($request['dob']));
        $age = date_diff(date_create($dob), date_create('now'))->y;

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);

            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'height_unit' => $request['height_unit'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'password' => $hash_password,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );
        } else {
            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );

            if (isset($request['location'])) {
                $arr_request_data["location"] = $request['location'];
            }

            if (isset($request['latitude'])) {
                $arr_request_data["latitude"] = $request['latitude'];
            }

            if (isset($request['longitude'])) {
                $arr_request_data["longitude"] = $request['longitude'];
            }
        }


        AdminUser::where('id', $student_id)->update($arr_request_data);

        // if ($request['email'] == $request['old_email']) {

        //     /* sending account updating mail to user */
        //     $admin_login_link = '<a href="' . url('/') . '/auth/login" target="_new">Log in to ' . url('/') . '/</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_student_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // } else {

        //     /* sending account verification link mail to user */
        //     $lang_id = 14;
        //     $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_student_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // }

        Session::flash('message', 'Student detail has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/student');
    }

    public function updateProfessional() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        $request = Input::all();

        $professor_id = $request['edit_id'];
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_professor_detail = AdminUser::find($professor_id);
        $email = $request['email'];

        if ($request['user_status'] != "") {
            $status = $request['user_status'];
        } else {
            $status = '0';
        }

        $dob = date('Y-m-d', strtotime($request['dob']));
        $age = date_diff(date_create($dob), date_create('now'))->y;

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);



            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'password' => $hash_password,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );
        } else {

            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );
        }


        AdminUser::where('id', $request['edit_id'])->update($arr_request_data);

        // if ($request['email'] == $request['old_email']) {

        //     /* sending account updating mail to user */
        //     $admin_login_link = '<a href="' . url('/') . '/auth/login" target="_new">Log in to ' . url('/') . '/</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_professor_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // } else {

        //     /* sending account verification link mail to user */
        //     $lang_id = 14;
        //     $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_professor_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // }

        Session::flash('message', 'User detail has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/professional');
    }

    public function updateCompany() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        $request = Input::all();
        $counselor_id = $request['edit_id'];
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_counselor_detail = AdminUser::find($counselor_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */

        if ($request['user_status'] != "") {
            $status = $request['user_status'];
        } else {
            $status = '0';
        }

        $dob = date('Y-m-d', strtotime($request['dob']));
        $age = date_diff(date_create($dob), date_create('now'))->y;

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);



            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'password' => $hash_password,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );
        } else {

            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'user_name' => $request['user_name'],
                'last_name' => $request['last_name'],
                'mob_no' => $request['contact_no'],
                'phone_no' => $request['phone_no'],
                'location' => $request['location'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'about_me' => $request['about_me'],
                'profession' => $request['profession'],
                'height' => $request['height'],
                'weight' => $request['weight'],
                'ethnicity_id' => $request['ethnicity_type'],
                'body_id' => $request['body_type'],
                'hair_id' => $request['hair_color'],
                'eye_id' => $request['eye_color'],
                'user_birth_date' => $dob,
                'user_age' => $age,
                'email' => $request['email'],
                'user_status' => $status,
                'user_type' => $request['user_type']
            );
        }


        AdminUser::where('id', $counselor_id)->update($arr_request_data);

        // if ($request['email'] == $request['old_email']) {

        //     /* sending account updating mail to user */
        //     $admin_login_link = '<a href="' . url('/') . '/auth/login" target="_new">Log in to ' . url('/') . '/</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||CONTACT_NAME||" => $request['first_name'],
        //             "||ADMIN_LOGIN_LINK||" => $admin_login_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_counselor_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // } else {

        //     /* sending account verification link mail to user */
        //     $lang_id = 14;
        //     $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

        //     $macros_array_detail = array();
        //     $macros_array_detail = email_template_macros::all();
        //     $macros_array = array();
        //     foreach ($macros_array_detail as $row) {
        //         $macros_array[$row['macros']] = $row['value'];
        //     }

        //     $reserved_words = array();
        //     if (isset($hash_password) && $hash_password != '') {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||PASSWORD||" => $request['user_password'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     } else {
        //         $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
        //         $content = $email_contents['email_template_content'];

        //         $reserved_words = array();
        //         $reserved_arr = array
        //             ("||SITE_TITLE||" => stripslashes($site_title->value),
        //             "||SITE_PATH||" => url('/'),
        //             "||USER_NAME||" => $request['first_name'],
        //             "||ADMIN_EMAIL||" => $request['email'],
        //             "||ADMIN_ACTIVATION_LINK||" => $activation_link
        //         );
        //     }

        //     $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        //     foreach ($reserved_words as $k => $v) {
        //         $content = str_replace($k, $v, $content);
        //     }
        //     $contents_array = array("email_conents" => $content);
        //     $data['site_email'] = $site_email->value;
        //     $data['site_title'] = $site_title->value;

        //     // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

        //     Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_counselor_detail, $contents_array, $data, $email_contents) {
        //         $message->from($data['site_email'], $data['site_title']);
        //         $message->to($email)->subject($email_contents['email_template_subject']);
        //     }); // End Mail
        // }

        Session::flash('message', 'User detail has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/company');
    }

    public function updateGeneralUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        $request = Input::all();


        $general_user_id = $request['edit_id'];

        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_general_user_detail = AdminUser::find($general_user_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */
        if ($request['email'] == $request['old_email']) {
            if ($request['user_status'] != "") {
                $status = $request['user_status'];
                $email_verified = '1';
            } else {
                $status = '0';
                $email_verified = '0';
            }
            $activation_code = $arr_general_user_detail->activation_code;
        } else {

            $status = '0';
            $email_verified = '0';
            $activation_code = time();
        }

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);

            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'user_birth_date' => $request['dob'],
                'password' => $hash_password,
                'email' => $request['email'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        } else {
            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'user_birth_date' => $request['dob'],
                'email' => $request['email'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        }

        AdminUser::where('id', $general_user_id)->update($arr_request_data);

        if ($request['email'] == $request['old_email']) {

            /* sending account updating mail to user */
            $admin_login_link = '<a href="' . url('/') . '/auth/login" target="_new">Log in to ' . url('/') . '/</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||CONTACT_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_LOGIN_LINK||" => $admin_login_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||CONTACT_NAME||" => $request['first_name'],
                    "||ADMIN_LOGIN_LINK||" => $admin_login_link
                );
            }

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_general_user_detail, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email)->subject($email_contents['email_template_subject']);
            }); // End Mail
        } else {

            /* sending account verification link mail to user */
            $lang_id = 14;
            $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            }

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_general_user_detail, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email)->subject($email_contents['email_template_subject']);
            }); // End Mail
        }

        Session::flash('message', 'General user detail has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/general_user');
    }

    public function updateRegisteredUser() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }


        $request = Input::all();


        $registered_user_id = $request['edit_id'];

        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();
        $arr_registered_user_detail = AdminUser::find($registered_user_id);
        $email = $request['email'];
        /* setting variable to update or add admin record. */
        if ($request['email'] == $request['old_email']) {
            if ($request['user_status'] != "") {
                $status = $request['user_status'];
                $email_verified = '1';
            } else {
                $status = '0';
                $email_verified = '0';
            }
            $activation_code = $arr_registered_user_detail->activation_code;
        } else {

            $status = '0';
            $email_verified = '0';
            $activation_code = time();
        }

        if (isset($request['user_password']) && $request['user_password'] != '') {
            $salt = substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash_password = crypt($request['user_password'], '$2y$12$' . $salt);

            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'password' => $hash_password,
                'email' => $request['email'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        } else {
            $arr_request_data = array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'gender' => $request['gender'],
                'email' => $request['email'],
                'user_status' => $status,
                'email_verified' => $email_verified,
                'activation_code' => $activation_code
            );
        }

        AdminUser::where('id', $registered_user_id)->update($arr_request_data);

        if ($request['email'] == $request['old_email']) {

            /* sending account updating mail to user */
            $admin_login_link = '<a href="' . url('/') . '/auth/login" target="_new">Log in to ' . url('/') . '/</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||CONTACT_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_LOGIN_LINK||" => $admin_login_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||CONTACT_NAME||" => $request['first_name'],
                    "||ADMIN_LOGIN_LINK||" => $admin_login_link
                );
            }

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 
            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_registered_user_detail, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email)->subject($email_contents['email_template_subject']);
            }); // End Mail
        } else {

            /* sending account verification link mail to user */
            $lang_id = 14;
            $activation_link = '<a href="' . url('/') . '/admin/admin/account-activate/' . $activation_code . '">Activate Account</a>';

            $macros_array_detail = array();
            $macros_array_detail = email_template_macros::all();
            $macros_array = array();
            foreach ($macros_array_detail as $row) {
                $macros_array[$row['macros']] = $row['value'];
            }

            $reserved_words = array();
            if (isset($hash_password) && $hash_password != '') {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated-password', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||PASSWORD||" => $request['user_password'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            } else {
                $email_contents = email_templates::where(array('email_template_title' => 'admin-email-updated', 'lang_id' => '14'))->first();
                $content = $email_contents['email_template_content'];

                $reserved_words = array();
                $reserved_arr = array
                    ("||SITE_TITLE||" => stripslashes($site_title->value),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $request['first_name'],
                    "||ADMIN_EMAIL||" => $request['email'],
                    "||ADMIN_ACTIVATION_LINK||" => $activation_link
                );
            }

            $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
            foreach ($reserved_words as $k => $v) {
                $content = str_replace($k, $v, $content);
            }
            $contents_array = array("email_conents" => $content);
            $data['site_email'] = $site_email->value;
            $data['site_title'] = $site_title->value;

            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user. 

            Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($email, $arr_registered_user_detail, $contents_array, $data, $email_contents) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($email)->subject($email_contents['email_template_subject']);
            }); // End Mail
        }

        Session::flash('message', 'Registered user detail has been updated successfully!');
        return redirect(url('/') . '/admin/users/list/registered_user');
    }

    public function addEthnicity(Request $request) {
//        error_reporting(E_ALL);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $global_data['site_title'] = "Add Ethnicity";
        $global = \App\global_settings::with('values')->get();
        $site_title = "Add Ethnicity";
        return view('Backend.ethnicity.add', ['site_title' => $site_title, 'global' => $global]);
    }

    public function addEthnicityPost(Request $request) {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $ethnicity = new Ethnicity;
        $ethnicity->ethnicity_type = $request['bio_detail'];
        $ethnicity->status = $request['status'];


        $ethnicity->save();
        Session::flash('message', 'Ethnicity type has been added successfully.');
        return redirect('admin/bio/list/ethnicity');
    }

    public function addBody(Request $request) {
//        error_reporting(E_ALL);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $global_data['site_title'] = "Add Body";
        $global = \App\global_settings::with('values')->get();
        $site_title = "Add Body";
        return view('Backend.body.add', ['site_title' => $site_title, 'global' => $global]);
    }

    public function addBodyPost(Request $request) {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $body = new Body;
        $body->body_type = $request['bio_detail'];
        $body->status = $request['status'];


        $body->save();
        Session::flash('message', 'Body type  has been added successfully.');
        return redirect('admin/bio/list/body');
    }

    public function addHair(Request $request) {
//        error_reporting(E_ALL);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $global_data['site_title'] = "Add Hair";
        $global = \App\global_settings::with('values')->get();
        $site_title = "Add Hair";
        return view('Backend.hair.add', ['site_title' => $site_title, 'global' => $global]);
    }

    public function addHairPost(Request $request) {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $hair = new Hair;
        $hair->hair_color = $request['bio_detail'];
        $hair->status = $request['status'];


        $hair->save();
        Session::flash('message', 'Hair color has been added successfully.');
        return redirect('admin/bio/list/hair');
    }

    public function addEye(Request $request) {
//        error_reporting(E_ALL);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $global_data['site_title'] = "Add Eye";
        $global = \App\global_settings::with('values')->get();
        $site_title = "Add Eye";
        return view('Backend.eye.add', ['site_title' => $site_title, 'global' => $global]);
    }

    public function addEyePost(Request $request) {
        $request = Input::all();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
        $eye = new Eye;
        $eye->eye_color = $request['bio_detail'];
        $eye->status = $request['status'];


        $eye->save();
        Session::flash('message', 'Eye color has been added successfully.');
        return redirect('admin/bio/list/eye');
    }

    public function viewEthnicity($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $user_id = base64_decode($id);


        $user = DB::table('mst_ethnicity_type as user')
                ->where('user.id', $user_id)
                ->select('user.ethnicity_type as user_name', 'user.status as status')
                ->get();


        return view('Backend.ethnicity.view', ['arr_ethnicity_detail' => $user, 'site_title' => "View Ethnicity Type"]);
    }

    public function viewBody($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $user_id = base64_decode($id);


        $user = DB::table('mst_body_type as user')
                ->where('user.id', $user_id)
                ->select('user.body_type as user_name', 'user.status as status')
                ->get();


        return view('Backend.body.view', ['arr_body_detail' => $user, 'site_title' => "View Body Type"]);
    }

    public function viewHair($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */


        $user_id = base64_decode($id);


        $user = DB::table('mst_hair_color as user')
                ->where('user.id', $user_id)
                ->select('user.hair_color as user_name', 'user.status as status')
                ->get();


        return view('Backend.hair.view', ['arr_hair_detail' => $user, 'site_title' => "View Hair Color "]);
    }

    public function viewEye($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('4', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $user_id = base64_decode($id);


        $user = DB::table('mst_eye_color as user')
                ->where('user.id', $user_id)
                ->select('user.eye_color as user_name', 'user.status as status')
                ->get();


        return view('Backend.eye.view', ['arr_eye_detail' => $user, 'site_title' => "View Eye Color"]);
    }

    public function viewStudent($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */

        $user_id = base64_decode($id);
        $user = DB::table('mst_users as user')
                ->join('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id', 'left')
                ->join('mst_body_type as body', 'body.id', '=', 'user.body_id', 'left')
                ->join('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id', 'left')
                ->join('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id', 'left')
                ->join('trans_group_users as trans', 'user.id', '=', 'trans.user_id_fk', 'left')
                ->join('mst_group as group', 'group.id', '=', 'trans.group_id_fk', 'left')
                ->join('mst_group as parent', 'parent.id', '=', 'group.parent_id', 'left')
                ->join('mst_group as child', 'child.id', '=', 'parent.parent_id', 'left')
                ->join('mst_group as sub_child', 'sub_child.id', '=', 'child.parent_id', 'left')
                ->where('user.id', $user_id)
                ->select('user.id as user_id', 'user.user_name as user_name', 'user.first_name as first_name', 'user.last_name as last_name', 'user.email as email', 'user.gender as gender', 'user.profession as profession','user.about_me as about_me', 'user.location as location', 'user.user_type as type', 'user.user_status as status', 'user.mob_no as mob_no', 'user.phone_no as phone_no', 'body.body_type as body_id', 'ethnicity.ethnicity_type as ethnicity_id', 'eye.eye_color as eye_id', 'hair.hair_color as hair_id', 'user.user_birth_date as dob', 'user.height as height', 'user.weight as weight', 'user.user_age as age', 'trans.group_id_fk as group_id', 'group.group_name as group_name', 'sub_child.group_name as parent_name', 'sub_child.id as parent_id', 'parent.id as sub_child_parent', 'child.id as child_parent')
                ->get();
        $arr_user_data = array();

        foreach ($user as $object) {
            $arr_user_data = (array) $object;
        }

        /* to get group name */
        $total = 0;
        $arr_parent_group = DB::table('mst_group')
                ->select('*', 'group_name')
                ->where('parent_id', '0')
                ->get();

        foreach ($arr_parent_group as $key => $value) {
            $arr_chlid_group = DB::table('mst_group')
                    ->select('*', 'group_name')
                    ->where('parent_id', $value->id)
                    ->get();
            foreach ($arr_chlid_group as $key1 => $value1) {
                $arr_chlid1_group = DB::table('mst_group')
                        ->select('*', 'group_name')
                        ->where('parent_id', $value1->id)
                        ->get();
                foreach ($arr_chlid1_group as $key2 => $value2) {
                    $arr_chlid2_group = DB::table('mst_group')
                            ->select('*', 'group_name')
                            ->where('parent_id', $value2->id)
                            ->get();

                    foreach ($arr_chlid2_group as $key3 => $value3) {
                        
                    }
                    $arr_chlid1_group[$key2]->chlid_2 = $arr_chlid2_group;
                }
                $arr_chlid_group[$key1]->chlid_1 = $arr_chlid1_group;
            }
            $arr_parent_group[$key]->chlid = $arr_chlid_group;
        }
        $data['arr_parent_group'] = $arr_parent_group;
        $arr_user_group = DB::table('trans_group_users')
                ->select('group_id_fk', 'status', 'created_at', 'till_date', db::raw('DATEDIFF(till_date,CURDATE()) as days_diff'))
                ->where('user_id_fk', $user_id)
                ->get();
        $data['arr_already_group'] = '';
        foreach ($arr_user_group as $key => $value) {
            $data['arr_user_group'][0][] = $value->group_id_fk;
            $data['arr_user_group'][1][$value->group_id_fk] = $value->days_diff;
            $data['arr_already_group'] .= ',' . $value->group_id_fk;
        }

        $get_connections = $this->getUserReferredConnections($user_id);
        $data['get_latest_user_agents'] =  $this->getUserLatestUserAgents($user_id);

        $data['user_connections'] = $get_connections;
        return view('Backend.student.view', ['arr_user_detail' => $user, 'finalData' => $data, 'site_title' => "View Student User"]);
    }

    public function viewProfessional($id) {

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $arr_user_data = array();
        $user_id = base64_decode($id);
        $user = DB::table('mst_users as user')
                ->join('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id', 'left')
                ->join('mst_body_type as body', 'body.id', '=', 'user.body_id', 'left')
                ->join('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id', 'left')
                ->join('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id', 'left')
                ->join('trans_group_users as trans', 'user.id', '=', 'trans.user_id_fk', 'left')
                ->join('mst_group as group', 'group.id', '=', 'trans.group_id_fk', 'left')
                ->join('mst_group as parent', 'parent.id', '=', 'group.parent_id', 'left')
                ->join('mst_group as child', 'child.id', '=', 'parent.parent_id', 'left')
                ->join('mst_group as sub_child', 'sub_child.id', '=', 'child.parent_id', 'left')
                ->where('user.id', $user_id)
                ->select('user.id as user_id', 'user.phone_no as phone_no', 'user.user_name as user_name', 'user.first_name as first_name', 'user.last_name as last_name', 'user.email as email', 'user.gender as gender', 'user.profession as profession', 'user.about_me as about_me','user.location as location', 'user.user_type as type', 'user.user_status as status', 'user.mob_no as mob_no', 'body.body_type as body_id', 'ethnicity.ethnicity_type as ethnicity_id', 'eye.eye_color as eye_id', 'hair.hair_color as hair_id', 'user.user_birth_date as dob', 'user.height as height', 'user.weight as weight', 'user.user_age as age', 'trans.group_id_fk as group_id', 'group.group_name as group_name', 'sub_child.group_name as parent_name', 'sub_child.id as parent_id', 'parent.id as sub_child_parent', 'child.id as child_parent')
                ->get();
        $arr_user_data = array();

        foreach ($user as $object) {
            $arr_user_data = (array) $object;
        }

        /* to get group name */
        $total = 0;
        $arr_parent_group = DB::table('mst_group')
                ->select('*', 'group_name')
                ->where('parent_id', '0')
                ->get();

        foreach ($arr_parent_group as $key => $value) {
            $arr_chlid_group = DB::table('mst_group')
                    ->select('*', 'group_name')
                    ->where('parent_id', $value->id)
                    ->get();
            foreach ($arr_chlid_group as $key1 => $value1) {
                $arr_chlid1_group = DB::table('mst_group')
                        ->select('*', 'group_name')
                        ->where('parent_id', $value1->id)
                        ->get();
                foreach ($arr_chlid1_group as $key2 => $value2) {
                    $arr_chlid2_group = DB::table('mst_group')
                            ->select('*', 'group_name')
                            ->where('parent_id', $value2->id)
                            ->get();

                    foreach ($arr_chlid2_group as $key3 => $value3) {
                        
                    }
                    $arr_chlid1_group[$key2]->chlid_2 = $arr_chlid2_group;
                }
                $arr_chlid_group[$key1]->chlid_1 = $arr_chlid1_group;
            }
            $arr_parent_group[$key]->chlid = $arr_chlid_group;
        }
        $data['arr_parent_group'] = $arr_parent_group;
        $arr_user_group = DB::table('trans_group_users')
                ->select('group_id_fk', 'status', 'created_at', 'till_date', db::raw('DATEDIFF(till_date,CURDATE()) as days_diff'))
                ->where('user_id_fk', $user_id)
                ->get();
        $data['arr_already_group'] = '';
        foreach ($arr_user_group as $key => $value) {
            $data['arr_user_group'][0][] = $value->group_id_fk;
            $data['arr_user_group'][1][$value->group_id_fk] = $value->days_diff;
            $data['arr_already_group'] .= ',' . $value->group_id_fk;
        }

        $get_connections = $this->getUserReferredConnections($user_id);
        $data['get_latest_user_agents']  = $this->getUserLatestUserAgents($user_id);

        $data['user_connections'] = $get_connections;
        return view('Backend.professional.view', ['arr_professional_detail' => $user, 'finalData' => $data, 'site_title' => "View User"]);
    }

    public function viewCompany($id) {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        if ($user->role_id != 1) {
            $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($arr_privileges) > 0) {
                foreach ($arr_privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            $arr_login_admin_privileges = $user_privileges;
            if (in_array('3', $arr_login_admin_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }
        /* ends here */
        $user_id = base64_decode($id);
        $user = DB::table('mst_users as user')
                ->join('mst_ethnicity_type as ethnicity', 'ethnicity.id', '=', 'user.ethnicity_id', 'left')
                ->join('mst_body_type as body', 'body.id', '=', 'user.body_id', 'left')
                ->join('mst_eye_color as eye', 'eye.id', '=', 'user.eye_id', 'left')
                ->join('mst_hair_color as hair', 'hair.id', '=', 'user.hair_id', 'left')
                ->join('trans_group_users as trans', 'user.id', '=', 'trans.user_id_fk', 'left')
                ->join('mst_group as group', 'group.id', '=', 'trans.group_id_fk', 'left')
                ->join('mst_group as parent', 'parent.id', '=', 'group.parent_id', 'left')
                ->join('mst_group as child', 'child.id', '=', 'parent.parent_id', 'left')
                ->join('mst_group as sub_child', 'sub_child.id', '=', 'child.parent_id', 'left')
                ->where('user.id', $user_id)
                ->select('user.id as user_id', 'user.phone_no as phone_no', 'user.user_name as user_name', 'user.first_name as first_name', 'user.last_name as last_name', 'user.email as email', 'user.gender as gender', 'user.profession as profession','user.about_me as about_me', 'user.location as location', 'user.user_type as type', 'user.user_status as status', 'user.mob_no as mob_no', 'body.body_type as body_id', 'ethnicity.ethnicity_type as ethnicity_id', 'eye.eye_color as eye_id', 'hair.hair_color as hair_id', 'user.user_birth_date as dob', 'user.height as height', 'user.weight as weight', 'user.user_age as age', 'trans.group_id_fk as group_id', 'group.group_name as group_name', 'sub_child.group_name as parent_name', 'sub_child.id as parent_id', 'parent.id as sub_child_parent', 'child.id as child_parent')
                ->get();


        $arr_user_data = array();

        foreach ($user as $object) {
            $arr_user_data = (array) $object;
        }

        /* to get group name */
        $total = 0;
        $arr_parent_group = DB::table('mst_group')
                ->select('*', 'group_name')
                ->where('parent_id', '0')
                ->get();

        foreach ($arr_parent_group as $key => $value) {
            $arr_chlid_group = DB::table('mst_group')
                    ->select('*', 'group_name')
                    ->where('parent_id', $value->id)
                    ->get();
            foreach ($arr_chlid_group as $key1 => $value1) {
                $arr_chlid1_group = DB::table('mst_group')
                        ->select('*', 'group_name')
                        ->where('parent_id', $value1->id)
                        ->get();
                foreach ($arr_chlid1_group as $key2 => $value2) {
                    $arr_chlid2_group = DB::table('mst_group')
                            ->select('*', 'group_name')
                            ->where('parent_id', $value2->id)
                            ->get();

                    foreach ($arr_chlid2_group as $key3 => $value3) {
                        
                    }
                    $arr_chlid1_group[$key2]->chlid_2 = $arr_chlid2_group;
                }
                $arr_chlid_group[$key1]->chlid_1 = $arr_chlid1_group;
            }
            $arr_parent_group[$key]->chlid = $arr_chlid_group;
        }
        $data['arr_parent_group'] = $arr_parent_group;
        $arr_user_group = DB::table('trans_group_users')
                ->select('group_id_fk', 'status', 'created_at', 'till_date', db::raw('DATEDIFF(till_date,CURDATE()) as days_diff'))
                ->where('user_id_fk', $user_id)
                ->get();
        $data['arr_already_group'] = '';
        foreach ($arr_user_group as $key => $value) {
            $data['arr_user_group'][0][] = $value->group_id_fk;
            $data['arr_user_group'][1][$value->group_id_fk] = $value->days_diff;
            $data['arr_already_group'] .= ',' . $value->group_id_fk;
        }

        $data['user_connections'] = $this->getUserReferredConnections($user_id);
        $data['get_latest_user_agents'] = $this->getUserLatestUserAgents($user_id);

        
        return view('Backend.company.view', ['arr_company_detail' => $user, 'data' => $data, 'site_title' => "View User"]);
    }

    public function downloadAllUser() {
        $userTypeQry = "CASE p1036_mst_users.user_type WHEN 1 THEN 'Student' WHEN 2 THEN 'Admin' WHEN 3 THEN 'Professional' WHEN 4 THEN 'Company' END AS user_type";
        $list = User::select('id', 'first_name', 'last_name', 'email', 'email_verified', \DB::raw($userTypeQry))
                        ->where('user_type', '!=', 2)->get()->toArray();

        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
            , 'Content-type' => 'text/csv'
            , 'Content-Disposition' => 'attachment; filename=users_' . date('dmYHis') . '.csv'
            , 'Expires' => '0'
            , 'Pragma' => 'public'
        ];

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list) {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Response::stream($callback, 200, $headers);
    }

    public function getUserReferredConnections($user_id) {
        $get_connections = DB::table('trans_user_connection as user_connection')
                ->join('mst_users as from_user', 'from_user.id', '=', 'user_connection.from_user_id')
                ->join('mst_users as to_user', 'to_user.id', '=', 'user_connection.to_user_id')
                ->select('to_user.id as to_id', 'to_user.first_name as to_first_name', 'to_user.gender as to_gender', 'to_user.last_name as to_last_name', 'to_user.email_verified as to_email_verified', 'to_user.ip_address as to_ip', 'to_user.location as to_loc', 'to_user.latitude as to_lat', 'to_user.longitude as to_long', 'to_user.created_at as to_created_at', 'to_user.profile_picture as to_profile_picture', 'from_user.first_name as from_first_name', 'from_user.gender as from_gender', 'from_user.last_name as from_last_name', 'from_user.email_verified as from_email_verified', 'from_user.ip_address as from_ip', 'from_user.location as from_loc', 'from_user.latitude as from_lat', 'from_user.longitude as from_long', 'from_user.created_at as from_created_at', 'from_user.profile_picture as from_profile_picture', 'user_connection.*')
                ->where('user_connection.from_user_id', $user_id)
                ->where('user_connection.is_referral', '1')
                ->orderBy('user_connection.id', 'DESC')
                ->get('group_name');
        return $get_connections;
    }
    public function getUserLatestUserAgents($user_id) {
        $get_agents = DB::table('trans_user_location_history')
                ->select('*')
                ->where('user_id', $user_id)
                ->limit(20)
                ->get();
        return $get_agents;
    }


    public function webPushNotification()
    {
        # code...

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                Auth::logout();
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }

        /* privilages */
        $user_privileges=[];
        if ($user->role_id != 1) {
            $privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
            if (count($privileges) > 0) {
                foreach ($privileges as $privilege) {
                    $user_privileges[] = $privilege->privilege_id;
                }
            }
            if (in_array('3', $user_privileges) == False) {
                Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
                return redirect()->guest('admin/dashboard');
                exit();
            }
        }


        return view('Backend.push-notify.view', ['site_title' => "Send Push Notification"]);


    }

    public function webpushtoall(Request $request)
    {

        $user = Auth::user();
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->user_status == 0) {
                Session::flash('login_error', 'Your account is inactive now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_status == 2) {
                Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
                Auth::logout();
                return redirect()->guest('/auth/admin/login');
            } else if ($user->user_type != 2) {
                Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
                return redirect()->guest('/');
            }
        } else {
            Session::flash('login_error', 'Your session has been expired or you are not logged in.');
            return redirect()->guest('/auth/admin/login');
        }
       $inp=Input::all();
       $inp['icon']=isset($inp['icon'])?$inp['icon']:"";
        $users=User::all();
        foreach ($users as $user) {
            # code...
            $user->notify(new \App\Notifications\webpushnotification($inp['title'],$inp['body'],$inp['icon']));
        }
        // $user->notify(new \App\Notifications\webpushnotification($inp['title'],$inp['body'],$inp['icon']));
            return redirect('/admin/pushnotify');
    }


}
