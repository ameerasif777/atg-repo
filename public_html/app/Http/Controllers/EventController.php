<?php 
namespace App\Http\Controllers;
use App\agendaEventSessionTime;
use App\Comment;
use App\commentEvent;
use App\CommonModel;
use App\email_templates;
use App\email_template_macros;
use App\Event;
use App\eventAgenda;
use App\eventCommentModel;
use App\eventCost;
use App\EventDays;
use App\eventGroup;
use App\eventGuest;
use App\eventRSVPCost;
use App\group;
use App\groupEvent;
use App\GroupUser;
use App\Helpers\GlobalData;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SitemapController;
use App\ImsModel;
use App\ImsRepliesModel;
use App\privileges;
use App\RegionalPostUrlModel;
use App\saveUserPost;
use App\Traits\Post\PostActions;
use App\User;
use App\UserActivity;
use Auth;
use DB;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
//use Socialite;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use Session;
use App\Traits\sendsignupmail;
use App\promotionModel;
use App\Traits\EventDetails;
use App\postPromotion;
use App\Traits\commentsTrait;
class EventController extends Controller {
	use PostActions;
	use sendsignupmail;
	use EventDetails;
	use commentsTrait;

	public $recordPerPage = 25;

	public function listEvent() {

		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}
		/* privilages */
		if ($user->role_id != 1) {
			$arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
			if (count($arr_privileges) > 0) {
				foreach ($arr_privileges as $privilege) {
					$user_privileges[] = $privilege->privilege_id;
				}
			}
			$arr_login_admin_privileges = $user_privileges;
			if (in_array('9', $arr_login_admin_privileges) == False) {
				Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
				return redirect()->guest('admin/dashboard');
				exit();
			}
		}

        $requestData = Input::all();
        $get_report = array();
		$arr_all_events = array();
		$eventObj = Event::leftjoin('mst_users as u', 'u.id', '=', 'user_id_fk')->select(['mst_event.*', 'u.first_name'])->orderBy('mst_event.id', 'DESC');

        if ($requestData && $requestData['search-qry'] != '') {
            $eventObj = $eventObj->whereRaw("(title like '%" . $requestData['search-qry'] . "%' or description like '%" .
                $requestData['search-qry'] . "')");
        }

        if ($requestData && $requestData['search-status'] != '') {
            $articleObj = $eventObj->where('status', $requestData['search-status']);
        }
        if ($requestData && $requestData['rpp'] != '') {
            $recordPerPage = $requestData['rpp'];
        } else {
            $recordPerPage = $this->recordPerPage;
        }
        $arr_events = $eventObj->paginate($recordPerPage);

		//dd($arr_events);
		foreach ($arr_events as $key => $value) {
			$arr_event_comment = DB::table('mst_comments as mc')
				->join('trans_event_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
				->where('tbc.event_id_fk', $arr_events[$key]->id)
				->select('mc.*', 'tbc.*')
				->get();

            $event_report = DB::table('mst_make_report')
				->where('event_id', $value['id'])
				->get();
            $arr_events[$key]->count = count($arr_event_comment);

            $arr_events[$key]->report_count = count($event_report);
		}

		$site_title = 'Manage Events';
		return view('Backend.event.list', [
            'arr_events' => $arr_events,
            'site_title' => $site_title,
            'sstatus' => $requestData ? $requestData['search-status'] : '',
            'sqry' => $requestData ? $requestData['search-qry'] : '',
            'recordPerPage' => $recordPerPage
        ]);
	}

	public function addEvent() {

		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}
		/* privilages */
		if ($user->role_id != 1) {
			$arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
			if (count($arr_privileges) > 0) {
				foreach ($arr_privileges as $privilege) {
					$user_privileges[] = $privilege->privilege_id;
				}
			}
			$arr_login_admin_privileges = $user_privileges;
			if (in_array('9', $arr_login_admin_privileges) == False) {
				Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
				return redirect()->guest('admin/dashboard');
				exit();
			}
		}
		$role_privileges = array();
		$privileges_ids = array();
		$role_privileges = DB::table('trans_role_privileges')
			->select(array('privilege_id'))
			->get();
		if (count($role_privileges) > 0) {

			foreach ($role_privileges as $values) {
				$privileges_ids[] = $values->privilege_id;
			}
		}

		$arr_privileges = \App\privileges::all();

		$global_data['site_title'] = "Add Event";
		return view('Backend.event.add', ['role_privileges' => $privileges_ids, 'global_values' => $global_data, 'arr_privileges' => $arr_privileges]);
	}

	public function addEventRecord() {
		$user = Session::get('user_account');
		$commonModel = new CommonModel();
		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}
		/* privilages */
		if ($user->role_id != 1) {
			$arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
			if (count($arr_privileges) > 0) {
				foreach ($arr_privileges as $privilege) {
					$user_privileges[] = $privilege->privilege_id;
				}
			}
			$arr_login_admin_privileges = $user_privileges;
			if (in_array('9', $arr_login_admin_privileges) == False) {
				Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
				return redirect()->guest('admin/dashboard');
				exit();
			}
		}
		$userModel = new User();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();

		if ($login) {
			return redirect(url('/'));
		}

		$user_id = $user->id;

		$Event = new Event;
		$Event->user_id_fk = $user_id;
		$Event->title = $request['title'];
		$Event->venue = $request['venue'];
		$Event->contact_name = $request['contact_name'];
		$Event->contact_number = $request['contact_number'];
		$Event->location = $request['location'];
		$Event->email_address = $request['email_address'];
		$Event->start_date = date('Y-m-d', strtotime($request['start_date']));
		$Event->start_time = $request['start_time'];
		$Event->end_date = date('Y-m-d', strtotime($request['end_date']));
		$Event->end_time = $request['end_time'];
		$Event->agenda = $request['agenda'];
		$Event->description = $request['description'];
		$Event->tag = $request['tag'];
		$Event->website = $request['website'];
		$Event->terms_condition = $request['terms_condition'];
		$Event->age_group = $request['age_group'];
		$Event->status = '1';
		if (Input::hasFile('profile_pic')) {
			$resp=json_decode($this->upload_feature_pic(Input::file('profile_pic')),'event','Backend');
			$Event->profile_image=$resp->msg;
		}
		$Event->save();

		Session::flash('success_msg', 'Event has been added successfully.');
		return redirect('admin/events');
	}

	public function deleteEvents(Request $request) {

		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}

		$roles_ids = $request->input('checkbox', '');
		$is_mail_send = isset($request['send-mail']) ? $request['send-mail'] : '';
		if (!empty($roles_ids)) {
			$arr_users = array();
			//now insetring all all privilages for selecte role.
			if (count($roles_ids) > 0) {
				foreach ($roles_ids as $val) {
					$eventData = Event::where('id', $val)->get();
					foreach ($eventData as $event) {
						DB::table('mst_event_agenda')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('mst_event_cost')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('mst_event_guest')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_group')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_like')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_recurring_days')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_rsvp_accept')->where('post_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_rsvp_status')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_upvote_downvote')->where('event_id_fk', '=', $event->id)->delete();
						DB::table('trans_event_rsvp_status')->where('event_id_fk', '=', $event->id)->delete();

						if (isset($is_mail_send) && $is_mail_send == 'on') {

							$commonModel = new CommonModel();
							$data = $commonModel->commonFunction();
							$email_contents = email_templates::where(array('email_template_title' => 'abuse-report-to-user', 'lang_id' => '14'))->first();
							$content = $email_contents['email_template_content'];

							$reserved_words = array();
							$reserved_arr = array
								("||SITE_TITLE||" => stripslashes($data['global']['site_title']),
								"||SITE_PATH||" => url('/'),
								"||NAME||" => $event->getEventUser[0]->first_name,
								"||FEED_NAME||" => $event->title,
							);

							$macros_array_detail = array();
							$macros_array_detail = email_template_macros::all();
							$macros_array = array();
							foreach ($macros_array_detail as $row) {
								$macros_array[$row['macros']] = $row['value'];
							}

							$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
							foreach ($reserved_words as $k => $v) {
								$content = str_replace($k, $v, $content);
							}
							$contents_array = array("email_conents" => $content);

							// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.

							Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($contents_array, $data, $email_contents, $event) {

								$message->from($data['global']['site_email'], $data['global']['site_title']);
								$message->to($event->getEventUser[0]->email)->subject($email_contents['email_template_subject']);
							}); // End Mail
						}
					}
					Event::where('id', $val)->delete();
				}
			}
		}
		Session::flash('success_msg', 'Event has been deleted successfully.');
		return redirect('admin/events');
	}

	public function editPostEvent($edit_id) {
//        error_reporting(E_ALL);
		//        ini_set('display_errors', 'on');
		$event_id = base64_decode($edit_id);
		$request = Input::all();
		if (count($request) > 0) {

			$event_object = Event::find(base64_decode($edit_id));
			$event_object->title = $request['title'];
			$event_object->venue = $request['venue'];
			$event_object->contact_name = $request['contact_name'];
			$event_object->contact_number = $request['contact_number'];
			$event_object->latitude = $request['latitude'];
			$event_object->longitude = $request['longitude'];
			$event_object->location = $request['location'];
			$event_object->email_address = $request['email_address'];
			$event_object->start_date = date('Y-m-d', strtotime($request['start_dt']));
			$event_object->start_time = $request['start_tm'];
			$event_object->end_date = date('Y-m-d', strtotime($request['end_date']));
			$event_object->end_time = $request['end_time'];
			$event_object->description = $request['description'];
			$event_object->terms_condition = $request['terms_condition'];
			$event_object->website = $request['website'];
			$event_object->tag = $request['tag'];
			$event_object->min_age = $request['min_age'];
			$event_object->max_age = $request['max_age'];
			$event_object->status = '1';

			if (Input::hasFile('profile_pic')) {
				$image_upload_success = $this->UploadCoverImagefx(Input::file('profile_pic'), 'event');
			   	if ($image_upload_success["return_code"] == 0) {
					$event_object->profile_image = trim($image_upload_success["msg"]);
			 	} 
			   	else if ($image_upload_success["return_code"] == 1) {
					Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
					return redirect('/mypost');
			   	}
	
				$event_object->cover_type=0;
		   	}
		   	else{
				$event_object->profile_image=$request['cover_url'];
			   	$event_object->cover_type=$request['cover_type'];
		   	}
			$event_object->save();

			/* Updating Meetup Cost  */
			$event_cost_data = DB::table('mst_event_cost')->where('event_id_fk', base64_decode($edit_id))->get();
			//delete all agenda information
			DB::table('mst_event_agenda')
				->where('event_id_fk', base64_decode($edit_id))
				->delete();
			$meetup_agenda = new eventAgenda;
			$final_array = array();
			/* Add Agenda */
			$arr_raw_data = array();
			$arr_insert_data = array();
			if (isset($request['start_date'][0]) && count($request) > 0 && (!empty($request['start_date'][0]))) {
				foreach ($request['start_date'] as $key => $start_date) {
					$arr_raw_data[$key] = $start_date;
					foreach ($arr_raw_data as $key2 => $data) {
						$arr_insert_data[$key]["date"] = $data;
					}
				}
				foreach ($request['start_time'] as $key => $start_time) {
					$arr_raw_data[$key] = $start_time;
					foreach ($arr_raw_data as $key2 => $data) {
						$arr_insert_data[$key]["time"] = $start_time;
					}
				}
				if (isset($request['session_0']) && (!empty($request['session_0']))) {
					foreach ($arr_insert_data as $k => $v) {
						$final_array[$k] = $v;
						foreach ($request['session_' . $k] as $key3 => $session) {
							$final_array[$k]['session'][$key3] = $session;
						}
						foreach ($request['time_' . $k] as $key4 => $time) {
							$final_array[$k]['ses_time'][$key4] = $time;
						}
					}
				}
			}

			if (count($final_array) > 0) {
				foreach ($final_array as $agenda) {
					$meetup_agenda = new eventAgenda;
					$meetup_agenda->event_id_fk = base64_decode($edit_id);
					$meetup_agenda->start_date = date('Y-m-d', strtotime($agenda['date']));
					$meetup_agenda->start_time = $agenda['time'];
					$meetup_agenda->save();
					$agenda_id = $meetup_agenda['id'];
					foreach ($agenda['session'] as $key1 => $session) {
						$ses_time = new agendaEventSessionTime;
						$ses_time->agenda_id_fk = $agenda_id;
						$ses_time->session = $session;
						$ses_time->time = $agenda['ses_time'][$key1];
						$ses_time->save();
					}
				}
			}

			/* Updating Guest */
			$event_guest_data = DB::table('mst_event_guest')->where('event_id_fk', base64_decode($edit_id))->get();

			if (count($event_guest_data) > 0) {
				DB::table('mst_event_guest')->where('event_id_fk', base64_decode($edit_id))->delete();
			}

			/* update sub groups */
			$get_groups = DB::table('trans_event_group as t')
				->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
				->where('t.event_id_fk', $event_object['id'])
				->select('mg.group_name', 'mg.id')
				->get();
			$arrEventGroup = Array();
			foreach ($get_groups as $key => $object) {
				$arrEventGroup[] = (array) $object;
			}

			$event = DB::table('mst_event')
				->join('mst_users', 'mst_users.id', '=', 'mst_event.user_id_fk')
				->select('mst_event.*', 'mst_users.First_name', 'mst_users.last_name')
				->where('mst_event.id', base64_decode($edit_id))
				->get();

			$user_id = $event['0']->user_id_fk;

			$arr_users_group = DB::table('trans_group_users as t')
				->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
				->where('t.user_id_fk', $user_id)
				->groupBy('g.id')
				->select('g.group_name', 'g.id')
				->get();
			$arrUserGroup = Array();
			foreach ($arr_users_group as $key => $object) {
				$arrUserGroup[] = (array) $object;
			}
			$arrUserGroups = Array();
			foreach ($arrUserGroup as $arrUserGP) {
				$arrUserGroups[] = $arrUserGP['id'];
			}

			$alreadyGroup = DB::table('trans_event_group')->where('event_id_fk', $event_object['id'])->get();
			$alreadyGroups = Array();
			foreach ($alreadyGroup as $gp) {
				$alreadyGroups[] = $gp->group_id_fk;
			}

			if (isset($request['sub_groups']) && !(empty($request['sub_groups']))) {
				$lastInsertId = $event_object['id'];
				$delete = EventGroup::where('event_id_fk', $lastInsertId)->delete();

				$post_link = url('/') . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $event['0']->title) . '-' . $event['0']->id;

				foreach ($arrEventGroup as $value) {
					if (!in_array($value['id'], $request['sub_groups'])) {
						$groupName = $value['group_name'];
						$groupLink = url('/') . '/go/' . $value['group_name'];
						$this->send_mail_by_adminfx($user_id, 'post-group-remove', $event['0']->title, $post_link, 'event', $groupName, $groupLink);
					}
				}

				foreach ($request['sub_groups'] as $value) {
					$event_group = new EventGroup;
					$event_group->group_id_fk = $value;
					$event_group->event_id_fk = $lastInsertId;
					$event_group->save();
					if (!in_array($value, $arrUserGroups) && !in_array($value, $alreadyGroups)) {
						$groupAdd = DB::table('mst_group')->where('id', $value)->first();
						$groupName = $groupAdd->group_name;
						$groupLink = url('/') . '/go/' . $groupAdd->group_name;
						$this->send_mail_by_adminfx($user_id, 'post-group-add', $event['0']->title, $post_link, 'event', $groupName, $groupLink);
					}

				}
			} else {

				$post_link = url('/') . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $event['0']->title) . '-' . $event['0']->id;

				foreach ($alreadyGroup as $removeGP) {
					$groupRemove = DB::table('mst_group')->where('id', $removeGP->group_id_fk)->first();
					$groupName = $groupRemove->group_name;
					$groupLink = url('/') . '/go/' . $groupRemove->group_name;
					$this->send_mail_by_adminfx($user_id, 'post-group-remove', $event['0']->title, $post_link, 'event', $groupName, $groupLink);
				}

				$lastInsertId = $event_object['id'];
				$delete = EventGroup::where('event_id_fk', $lastInsertId)->delete();
			}

			/* Ends Here */
			Session::flash('success_msg', 'Event details has been updated successfully.');
			return redirect()->guest('/admin/events');
		}

		/*         * **** for group********* */
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];

		$commonModel = new CommonModel();
		$condition_to_pass = array('id' => $event_id);
		$arr_event = $commonModel->getRecords('mst_event', '*', $condition_to_pass);
		$user_id = $arr_event['0']->user_id_fk;

		if (count($arr_event) > 0) {
			/**             * ** for group********* */
			foreach ($arr_event as $key => $value) {
				$get_groups = DB::table('trans_event_group as t')
					->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
					->where('t.event_id_fk', base64_decode($edit_id))
					->select('mg.group_name', 'mg.id')
					->get();

				$arr_users_group = Group::select('group_name', 'id')->get();
			}
			$arr = array();
			foreach ($get_groups as $key => $object) {
				$arr[] = (array) $object;
			}

			$arr1 = array();

			foreach ($arr as $value) {
				$arr1[] = $value['id'];
			}
		}
		$arr_cost = DB::table('mst_event_cost')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();

		$arr_agenda = DB::table('mst_event_agenda as ag')
			->join('mst_event as m', 'm.id', '=', 'ag.event_id_fk')
			->where('ag.event_id_fk', $event_id)
			->select('ag.*', 'm.agenda')
			->get();

		$arr_event_agenda_details = array();
		if (count($arr_agenda) > 0) {
			foreach ($arr_agenda as $key => $agenda) {
				$arr_event_agenda_details[$key] = $agenda;

				$arr_agenda_session_details = DB::table('trans_event_agenda_session_time')
					->where('agenda_id_fk', $agenda->id)
					->select('*')
					->get();

				$arr_event_agenda_details[$key]->session_details = $arr_agenda_session_details;
			}
		}
		/*         * *** For List Of Guest ****** */
		$arr_event_guest = DB::table('mst_event_guest')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();
		/*         * ***** Ends Here ******* */
		$commonModel = new CommonModel();
		$condition_to_pass = array('id' => $event_id);
		$arr_event = $commonModel->getRecords('mst_event', '*', $condition_to_pass);
		$site_title = 'Edit Event Details';

		return view('Backend.event.edit')
			->with('arr_event_details', $arr_event)
			->with('site_title', $site_title)
			->with('arr_event_guest', $arr_event_guest)
			->with('arr_event_agenda', $arr_event_agenda_details)
			->with('user_group_data', $arr_users_group)
			->with('arr_selected_groups', $arr1)
			->with('arr_selected_groups1', $arr);
	}

	public function viewEditPostEvent($view_id) {

		$event_id = base64_decode($view_id);

		/*         * **** for group********* */
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$commonModel = new CommonModel();
		$condition_to_pass = array('id' => $event_id);
		$arr_event = $commonModel->getRecords('mst_event', '*', $condition_to_pass);
		$get_groups = DB::table('trans_event_group as t')
			->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
			->where('event_id_fk', $event_id)
			->select('mg.group_name', 'mg.id')
			->get();
		/*         * ***** For Cost ******* */
		$arr_cost = DB::table('mst_event_cost')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();
		/*         * **** For List Of Guest ****** */
		$arr_event_guest = DB::table('mst_event_guest')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();

		/*         * **** Ends Here ******* */
		/*         * ***agenda and session***** */
		$arr_agenda = DB::table('mst_event_agenda as ag')
			->join('mst_event as m', 'm.id', '=', 'ag.event_id_fk')
			->where('ag.event_id_fk', $event_id)
			->select('ag.*', 'm.agenda')
			->get();

		$arr_event_agenda_details = array();
		if (count($arr_agenda) > 0) {
			foreach ($arr_agenda as $key => $agenda) {
				$arr_event_agenda_details[$key] = $agenda;

				$arr_agenda_session_details = DB::table('trans_event_agenda_session_time')
					->where('agenda_id_fk', $agenda->id)
					->select('*')
					->get();

				$arr_event_agenda_details[$key]->session_details = $arr_agenda_session_details;
			}
		}
		/*         * *****end agenda********* */

		$commonModel = new CommonModel();
		$condition_to_pass = array('id' => $event_id);
		$arr_event = $commonModel->getRecords('mst_event', '*', $condition_to_pass);
		$site_title = 'View Event Details';

		return view('Backend.event.view')
			->with('arr_event_details', $arr_event)
			->with('site_title', $site_title)
			->with('arr_event_guest', $arr_event_guest)
			->with('arr_event_agenda', $arr_event_agenda_details)
			->with('get_groups', $get_groups)
			->with('arr_cost', $arr_cost);
	}

	function listComment($event_id) {
		$event_id = base64_decode($event_id);
		$commonModel = new CommonModel();
		$user = Auth::user();
		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contact admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_type != 2) {
				Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
				return redirect()->guest('/');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}
		/* privilages */
		if ($user->role_id != 1) {
			$arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
			if (count($arr_privileges) > 0) {
				foreach ($arr_privileges as $privilege) {
					$user_privileges[] = $privilege->privilege_id;
				}
			}
			$arr_login_admin_privileges = $user_privileges;
			if (in_array('9', $arr_login_admin_privileges) == False) {
				Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
				return redirect()->guest('admin/dashboard');
				exit();
			}
		}

		$event = DB::table('mst_event as ma')
			->join('mst_users as mu', 'mu.id', '=', 'ma.user_id_fk')
			->where('ma.id', $event_id)
			->select('ma.*', 'mu.First_name', 'mu.last_name')
			->orderBy('id', 'desc')
			->get();
		foreach ($event as $key => $value) {
			$arr_event_comment = DB::table('mst_comments as mc')
				->join('trans_event_comments as tac', 'mc.id', '=', 'tac.comment_id_fk', 'left')
				->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
				->where('tac.event_id_fk', $event[$key]->id)
				->select('mc.*', 'u.user_name', 'u.profile_picture')
				->get();

			$event[$key]->comment = $arr_event_comment;
		}
		$site_title = 'Manage Event Comment';
		return view('Backend.event.listComment', ['arr_event' => $event, 'site_title' => $site_title]);
	}

	function editComment($comment_id) {
		$commonModel = new CommonModel();
		$user = Auth::user();
		if (Auth::check()) {
			$user = Auth::user();
			if ($user->user_status == 0) {
				Session::flash('login_error', 'Your account is inactive now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_status == 2) {
				Session::flash('login_error', 'Your account is blocked now, Please contat to admin.');
				Auth::logout();
				return redirect()->guest('/auth/admin/login');
			} else if ($user->user_type != 2) {
				Session::flash('error_msg', 'Sorry, you don\'t have privilege to use this page.');
				return redirect()->guest('/');
			}
		} else {
			Session::flash('login_error', 'Your session has been expired or you are not logged in.');
			return redirect()->guest('/auth/admin/login');
		}
		/* privilages */
		if ($user->role_id != 1) {
			$arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user->role_id)->get();
			if (count($arr_privileges) > 0) {
				foreach ($arr_privileges as $privilege) {
					$user_privileges[] = $privilege->privilege_id;
				}
			}
			$arr_login_admin_privileges = $user_privileges;
			if (in_array('9', $arr_login_admin_privileges) == False) {
				Session::flash('message', 'You do not have sufficient priviliges to manage this user!');
				return redirect()->guest('admin/dashboard');
				exit();
			}
		}

		$input_data = Input::all();

		if (!(empty($input_data))) {
			$arr_comment = Comment::find($comment_id);
			$arr_comment->comment = $input_data['comment'];
			$arr_comment->status = $input_data['status'];
			$arr_comment->save();
			$event_comment = eventCommentModel::where('comment_id_fk', $comment_id)
				->update(['status' => $input_data['status']]);

			Session::flash('success_msg', 'Your comment has been update successfully.');
			return redirect()->guest('/admin/events');
		}

		$comment_id = base64_decode($comment_id);
		$arr_comment = Comment::where('id', $comment_id)->get();
		$event_comment = eventCommentModel::where('comment_id_fk', $comment_id)->get();
		$site_title = 'Edit Comment';
		return view('Backend.event.editComment', ['arr_comment' => $arr_comment, 'site_title' => $site_title]);
	}

	function deleteEventComments(Request $request) {

		if (count($request->comment_ids) > 0) {
			foreach ($request->comment_ids as $key => $val) {

				$comment = Comment::find($val);
				$comment->delete();
				$arr_comment = eventCommentModel::where('comment_id_fk', $val)
					->delete();
			}
		}
		Session::flash('success_msg', 'Comment has been deleted successfully.');
		return redirect('admin/event/list');
	}

	/* Frontend::Edit Post Event */

	public function frontEditPostEvent($edit_id) {

		$event_id = base64_decode($edit_id);
		$request = Input::all();
		// echo '<pre>';
		// print_r($request); exit;
		if (isset($request['event_id']) != '') {
			$event_id = $request['event_id'];
		}

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$arr_user_data = User::find($data['user_session']['id']);
		$user_id = $data['user_session']['id'];
		if (count($request) > 0) {

			if ($request['title'] != '') {
				$event_object = Event::find($event_id);
				$event_object->title = $request['title'];
				$event_object->venue = $request['venue'];
				$event_object->contact_name = $request['contact_name'];
				$event_object->contact_number = $request['contact_number'];
				$event_object->email_address = $request['email_address'];
				$event_object->start_date = date('Y-m-d', strtotime($request['start_dt']));
				$event_object->start_time = $request['start_tm'];
				$event_object->location = $request['location'];
				$event_object->end_date = date('Y-m-d', strtotime($request['end_date']));
				$event_object->end_time = $request['end_time'];
				$event_object->latitude = $request['latitude'];
				$event_object->longitude = $request['longitude'];
				$event_object->description = $request['description'];
				$event_object->tag = $request['tag'];
				$eventstatus = $event_object->status;
				if (isset($request['btn_post']) != '') {
					if ($eventstatus == '1') {
						$user = Session::get('user_account');
						$subject = 'updated an event - "' . $request['title'] . '"';
						$message = ucfirst($user['first_name']) . ' ' . ucfirst($user['last_name']) . ' updated an event - "' . $request['title'] . '"';
						$link = '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $event_object->id;

						$User_activity = new UserActivity;
						$User_activity->user_id_fk = $user['id'];
						$User_activity->activity_id = $event_object->id;
						$User_activity->activity_title = $subject;
						$User_activity->activity_link = $link;
						$User_activity->activity_table_name = 'mst_event';
						$User_activity->activity_action = 'updated event';
						$User_activity->save();
					}
					$event_object->status = '1';
				}
				if (array_key_exists('cost_checkbox', $request) && isset($request['cost_checkbox']) && $request['cost_checkbox'] == 'on') {
					if (isset($request['currency']) && isset($request['cost'])) {
						$event_object->currency = $request['currency'];
						$event_object->cost = $request['cost'];
					}
				} else if (array_key_exists('free_checkbox', $request) && isset($request['free_checkbox']) && $request['free_checkbox'] == 'on') {
					$event_object->currency = '';
					$event_object->cost = '0';
				} else {
					$event_object->currency = '';
					$event_object->cost = '';
				}
				if (Input::hasFile('profile_pic')) {
					$image_upload_success = $this->UploadCoverImagefx(Input::file('profile_pic'), 'event');
					if ($image_upload_success["return_code"] == 0) {
						$event_object->profile_image = trim($image_upload_success["msg"]);
					} 
					else if ($image_upload_success["return_code"] == 1) {
						Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
						return redirect('/mypost');
					}
		
					$event_object->cover_type=0;
				}
				else {
					if(isset($request['last_profile_pic']) && !empty($request['last_profile_pic']))
						$event_object->profile_image = $request['last_profile_pic'];
					else{
						$event_object->profile_image=$request['cover_url'];
						$event_object->cover_type=$request['cover_type'];
					}
				}
				$event_object->save();

				/* Updating Event Cost  */

				/*$event_cost_data = DB::table('mst_event_cost')->where('event_id_fk', $event_id)->get();
				if (count($event_cost_data) > 0) {
					DB::table('mst_event_cost')->where('event_id_fk', $event_id)->delete();
				}*/

				/* Add cost */
				if (isset($request['cost_checkbox']) && $request['cost_checkbox'] == 'on') {
					if ($request['advanced_opt_category'][0] != '') {
						foreach ($request['advanced_opt_category'] as $key => $value) {
							$event_cost = new eventCost;
							$event_cost->event_id_fk = $event_id;
							$event_cost->category = $value;
							$event_cost->description = $request['advanced_opt_description'][$key];
							$event_cost->cost = $request['advanced_opt_cost'][$key];
							$event_cost->currency = $request['advanced_opt_currency'][$key];
							$event_cost->last_date = date('Y-m-d', strtotime($request['advanced_opt_last_date'][$key]));
							$event_cost->save();
						}
					}
				}

				DB::table('trans_event_recurring_days')->where('event_id_fk', $event_id)->delete();
				if ($event_object->end_date != '1970-01-01' && $event_object->end_date != '') {
					if (isset($request['requaring_day']) && $request['requaring_day'] != '') {
						foreach ($request['requaring_day'] as $days) {
							$event_days = new EventDays;
							$event_days->event_id_fk = $event_id;
							$event_days->recurring_day = $days;
							$event_days->save();
						}
					}
				}

				/* Updating Guest */
				$event_guest_data = DB::table('mst_event_guest')->where('event_id_fk', $event_id)->get();

				if (count($event_guest_data) > 0) {
					DB::table('mst_event_guest')->where('event_id_fk', $event_id)->delete();
				}

				//Adding guest records
				$arr_guest = array();
				if ($request['guest_name'][0] != '') {
					$random_no = rand(1000, 9999);
					foreach ($request['guest_name'] as $key => $value) {
						$event_guest = new eventGuest;
						$event_guest->event_id_fk = $event_id;
						$event_guest->name = $value;
						$event_guest->designation = $request['designation'][$key];
						if (isset($request['profile_image'][$key])) {
							if (Input::hasFile('profile_image')) {
								$profile_image = $request['profile_image'][$key];
								$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image";
								$random_no = rand(1000, 9999);
								if (Input::hasFile('profile_image')) {
									$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image";
									$obj = new Filesystem();
									if (!$obj->exists($destinationPath)) {
										$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
									}
									try {
										$extension = $profile_image->getClientOriginalExtension();
										$fileName = $random_no . '.' . $extension;
										$image = \Image::make($profile_image);
										$image->save($destinationPath . '/' . $fileName);
										$event_guest->profile_image = $fileName;
										$image->resize(300, null, function ($constraint) {
											$constraint->aspectRatio();
										});
										$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image/thumb";
										$obj = new Filesystem();
										if (!$obj->exists($destinationPath)) {
											$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
										}
										$image->save($destinationPath . '/' . $fileName);
									} catch (Exception $e) {
										Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
										return redirect('/mypost');
									}
								}
							}
						} else {
							if (isset($request['last_guest_profile_pic'][$key])) {
								$event_guest->profile_image = $request['last_guest_profile_pic'][$key];
							}
						}
						if ($value != '') {
							$event_guest->save();
						}
					}
				}

				/* update sub groups */
				if (isset($request['sub_groups']) && !(empty($request['sub_groups']))) {
					$lastInsertId = $event_object['id'];
					$delete = EventGroup::where('event_id_fk', $lastInsertId)->delete();

					foreach ($request['sub_groups'] as $value) {
						$event_group = new EventGroup;
						$event_group->group_id_fk = $value;
						$event_group->event_id_fk = $lastInsertId;
						$event_group->save();
					}
				} else {
					$lastInsertId = $event_object['id'];
					$delete = EventGroup::where('event_id_fk', $lastInsertId)->delete();
				}

				//delete all agenda information

				$event_agenda = new eventAgenda;
				$final_array = array();
				/* Add Agenda */
				$arr_raw_data = array();
				$arr_insert_data = array();

				if ($request['start_date'][0] != '' && count($request) > 0) {
					foreach ($request['start_date'] as $key => $start_date) {
						$arr_raw_data[$key] = $start_date;
						foreach ($arr_raw_data as $key2 => $data) {
							$arr_insert_data[$key]["date"] = $data;
						}
					}
					foreach ($request['start_time'] as $key => $start_time) {
						$arr_raw_data[$key] = $start_time;
						foreach ($arr_raw_data as $key2 => $data) {
							$arr_insert_data[$key]["time"] = $start_time;
						}
					}

					foreach ($arr_insert_data as $k => $v) {
						if (isset($request['session_' . $k])) {
							$final_array[$k] = $v;
							foreach ($request['session_' . $k] as $key3 => $session) {
								$final_array[$k]['session'][$key3] = $session;
							}
							foreach ($request['time_' . $k] as $key4 => $time) {
								$final_array[$k]['ses_time'][$key4] = $time;
							}
						}
					}
				}
				if (count($final_array) > 0) {
					DB::table('mst_event_agenda')->where('event_id_fk', $event_id)->delete();
					foreach ($final_array as $agenda) {
						$event_agenda = new eventAgenda;
						$event_agenda->event_id_fk = $event_id;
						$event_agenda->start_date = date('Y-m-d', strtotime($agenda['date']));
						$event_agenda->start_time = $agenda['time'];
						$event_agenda->save();
						$agenda_id = $event_agenda['id'];

						foreach ($agenda['session'] as $key1 => $session) {
							$ses_time = new agendaEventSessionTime;
							$ses_time->agenda_id_fk = $agenda_id;
							$ses_time->session = $session;
							$ses_time->time = $agenda['ses_time'][$key1];
							$ses_time->save();
						}
					}
				}
				if ($request['auto_save_flag'] == "true") {
					return $lastInsertId;
				}
				Session::flash('success_msg', 'Event details has been updated successfully.');
				$link = '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $event_id;
                                return redirect()->guest($link);
                                //return redirect()->guest('/mypost');
			}
		}

		/*         * **** for group********* */
		$user_id = $data['user_session']['id'];
		$commonModel = new CommonModel();
		$condition_to_pass = array('id' => $event_id);
		$arr_event = $commonModel->getRecords('mst_event', '*', $condition_to_pass);

		foreach ($arr_event as $key => $value) {
			$get_groups = DB::table('trans_event_group as t')
				->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
				->where('event_id_fk', $event_id)
				->select('mg.group_name', 'mg.id')
				->get();
			$arr_users_group = DB::table('trans_group_users as t')
				->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
				->where('user_id_fk', $user_id)
				->groupBy('g.id')
				->select('g.group_name', 'g.id')
				->get();
		}

		$arr = array();
		foreach ($get_groups as $key => $object) {
			$arr[] = (array) $object;
		}

		$arr1 = array();

		foreach ($arr as $value) {
			$arr1[] = $value['id'];
		}
		/*         * ******************** */

		/*         * ****** For Cost ******* */

		$arr_cost_detail = array();
		$arr_cost_detail = DB::table('mst_event_cost')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();

		$arr_agenda = DB::table('mst_event_agenda as ag')
			->join('mst_event as m', 'm.id', '=', 'ag.event_id_fk')
			->where('ag.event_id_fk', $event_id)
			->select('ag.*', 'm.agenda')
			->get();

		$arr_event_agenda_details = array();
		if (count($arr_agenda) > 0) {
			foreach ($arr_agenda as $key => $agenda) {
				$arr_event_agenda_details[$key] = $agenda;
				$arr_agenda_session_details = DB::table('trans_event_agenda_session_time')
					->where('agenda_id_fk', $agenda->id)
					->select('*')
					->get();
				$arr_event_agenda_details[$key]->session_details = $arr_agenda_session_details;
			}
		}

		/*         * **** For List Of Guest ****** */

		$arr_event_guest = DB::table('mst_event_guest')
			->where('event_id_fk', $event_id)
			->select('*')
			->get();
		$arr_event_recurring_days = array();
		$arr_event_recurring_days = DB::table('trans_event_recurring_days')
			->where('event_id_fk', $event_id)
			->select('recurring_day')
			->get();
		$arr_selected_days = array();
		if (count($arr_event_recurring_days) > 0) {
			foreach ($arr_event_recurring_days as $days) {
				$arr_selected_days[] = $days->recurring_day;
			}
		}
		/*         * ***** Ends Here ******* */
		/* Ends Here */

		$data['header'] = array(
			"title" => 'Event Edit',
			"keywords" => '',
			"description" => '',
		);

		return view('Frontend.event.edit')
			->with('arr_event_details', $arr_event)
			->with('user_group_data', $arr_users_group)
			->with('arr_user_data', $arr_user_data)
			->with('finalData', $data)
			->with('arr_event_guest', $arr_event_guest)
			->with('arr_cost_detail', $arr_cost_detail)
			->with('arr_event_agenda', $arr_event_agenda_details)
			->with('arr_selected_groups', $arr1)
			->with('arr_event_recurring_days', $arr_selected_days);
	}

	/*
		      Frontend::Start Alls Frontend  Functions
	*/

	public function postEvent($group_id = '') {
		
		$user_account = Session::get('user_account');
        if (isset($user_account['email_verified']) && $user_account['email_verified'] == '0') {
            $this->sendEmailVerificationMail($user_account['email'] , $user_account);
            return redirect()->back()->with('warning_msg_permanent', "Verify your email <b>".$user_account['email']."</b> to post on ATG. Check your inbox/spam. <br> <b>".$user_account['email']."</b> not your email? <a style=\"color:white\" href=\"send-feedback\"> Contact us </a>");
        }

		$group_id = base64_decode($group_id);
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}

		$data['user_session'] = Session::get('user_account');

		$user_id = $data['user_session']['id'];
		$arr_users_group = DB::table('trans_group_users as t')
			->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
			->where('user_id_fk', $user_id)
			->groupBy('g.id')
			->select('g.group_name', 'g.id')
			->get();
		$data['header'] = array(
			"title" => 'Post & Share your event on ATG',
			"keywords" => '',
			"description" => '',
		);
		$arr_user_data = User::find($data['user_session']['id']);
		return view('Frontend.event.post-event')
			->with('title', 'Event')
			->with('arr_users_group', $arr_users_group)
			->with('arr_user_data', $arr_user_data)
			->with('group_id', $group_id)
			->with('finalData', $data);
	}

	public function addPostEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();

		if ($login) {
			return redirect(url('/'));
		}

		$user = Session::get('user_account');
		$request = Input::all();
		if ($request['btn_post'] == '0') {
			$status = '1';
		} else {
			$status = '0';
		}
		$Event = Event::where('id', $request['id'])->first();
		$user_id = $user['id'];

                if (count($Event) == 0) {
                    $Event = new Event;
                    $Event->user_id_fk = $user_id;
                }
		
                $Event->title = $request['title'];
                $Event->venue = $request['venue'];
                $Event->contact_name = $request['contact_name'];
                $Event->contact_number = $request['contact_number'];
                $Event->latitude = $request['latitude'];
                $Event->longitude = $request['longitude'];
                $Event->location = $request['location'];
                $Event->email_address = $request['email_address'];
                $Event->start_date = date('Y-m-d', strtotime($request['start_dt']));
                $Event->start_time = $request['start_tm'];
                $Event->end_date = date('Y-m-d', strtotime($request['end_date']));
                $Event->end_time = $request['end_time'];
                $Event->description = $request['description'];
                $Event->tag = $request['tag'];
                $Event->status = $status;
                $Event->onlineTicket = ($request['onlineflag'] == '1')?'1':'0';
		

		if (array_key_exists('cost_checkbox', $request) && isset($request['cost_checkbox']) && $request['cost_checkbox'] == 'on') {
			if (isset($request['currency']) && isset($request['cost'])) {
				$Event->currency = $request['currency'];
				$Event->cost = $request['cost'];
			}
		} else if (array_key_exists('free_checkbox', $request) && isset($request['free_checkbox']) && $request['free_checkbox'] == 'on') {
			$Event->currency = '';
			$Event->cost = '0';
		} else {
			$Event->currency = '';
			$Event->cost = '';
		}

		if (Input::hasFile('profile_pic')) {
			$image_upload_success = $this->UploadCoverImagefx(Input::file('profile_pic'), 'event');
		   if ($image_upload_success["return_code"] == 0) {
			   $Event->profile_image = trim($image_upload_success["msg"]);
		   } 
		   else if ($image_upload_success["return_code"] == 1) {
			   Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
			   return redirect('/mypost');
		   }

		   $Event->cover_type=0;
	   }
	   else{
		   $Event->profile_image=$request['cover_url'];
		   $Event->cover_type=$request['cover_type'];
	   }


		$Event->save();
		$lastInsertId = $Event['id'];

		/* Add cost */
		if (isset($request['cost_checkbox']) && $request['cost_checkbox'] == 'on') {
			if ($request['advanced_opt_category'][0] != '') {
				DB::table('mst_event_cost')->where('event_id_fk', $lastInsertId)->delete();
				foreach ($request['advanced_opt_category'] as $key => $value) {
					$event_cost = new eventCost;
					$event_cost->event_id_fk = $lastInsertId;
					$event_cost->category = $value;
					// $event_cost->description = $request['advanced_opt_description'][$key];
					$event_cost->cost = $request['advanced_opt_cost'][$key];
					$event_cost->currency = $request['advanced_opt_currency'][$key];
					$event_cost->last_date = date('Y-m-d', strtotime($request['advanced_opt_last_date'][$key]));
					$event_cost->save();
				}
			}
		}
		if (isset($request['requaring_day']) && $request['requaring_day'] != '') {
			foreach ($request['requaring_day'] as $days) {
				$event_days = new EventDays;
				$event_days->event_id_fk = $lastInsertId;
				$event_days->recurring_day = $days;
				$event_days->save();
			}
		}

		/*         * ***************** */
		$arr_raw_data = array();
		$arr_insert_data = array();

		if ($request['start_date'][0] != '' && count($request) > 0) {
			foreach ($request['start_date'] as $key => $start_date) {
				$arr_raw_data[$key] = $start_date;
				foreach ($arr_raw_data as $key2 => $data) {
					$arr_insert_data[$key]["date"] = $data;
				}
				foreach ($request['session_' . $key] as $session) {
					$arr_insert_data[$key]["session"] = $session;
				}
				foreach ($request['time_' . $key] as $time) {
					$arr_insert_data[$key]["time"] = $time;
				}
			}
		}

		/*         * **********for agenda************** */
		$arr_raw_data = array();
		$arr_insert_data = array();
		$final_array = array();

//        if ($request['start_date'][0] != '' && count($request) > 0) {
		foreach ($request['start_date'] as $key => $start_date) {
			$arr_raw_data[$key] = $start_date;
			foreach ($arr_raw_data as $key2 => $data) {
				$arr_insert_data[$key]["date"] = $data;
			}
		}

		foreach ($request['start_time'] as $key => $start_time) {
			$arr_raw_data[$key] = $start_time;
			foreach ($arr_raw_data as $key2 => $data) {
				$arr_insert_data[$key]["time"] = $start_time;
			}
		}

		foreach ($arr_insert_data as $k => $v) {
			$final_array[$k] = $v;
			foreach ($request['session_' . $k] as $key3 => $session) {
				$final_array[$k]['session'][$key3] = $session;
			}
			foreach ($request['time_' . $k] as $key4 => $time) {
				$final_array[$k]['ses_time'][$key4] = $time;
			}
		}

		if (count($final_array) > 0) {
			DB::table('mst_event_agenda')->where('event_id_fk', $lastInsertId)->delete();
			foreach ($final_array as $agenda) {
				$event_agenda = new eventAgenda;
				$event_agenda->event_id_fk = $lastInsertId;
				$event_agenda->start_date = date('Y-m-d', strtotime($agenda['date']));
				$event_agenda->start_time = $agenda['time'];
				$event_agenda->save();
				$agenda_id = $event_agenda['id'];
				foreach ($agenda['session'] as $key1 => $session) {
					$ses_time = new agendaEventSessionTime;
					$ses_time->agenda_id_fk = $agenda_id;
					$ses_time->session = $session;
					$ses_time->time = $agenda['ses_time'][$key1];
					$ses_time->save();
				}
			}
		}
//        }

		/*         * ******for group********* */
		if (isset($request['sub_groups']) && $request['sub_groups'] != '') {
			DB::table('trans_event_group')->where('event_id_fk', $lastInsertId)->delete();
			foreach ($request['sub_groups'] as $value) {
				$event_group = new groupEvent;
				$event_group->group_id_fk = $value;
				$event_group->event_id_fk = $lastInsertId;
				$event_group->save();
			}
		}

		if ($request['btn_post'] == '0') {
			$subject = /*ucfirst($user['first_name']) . ' ' . ucfirst($user['last_name']) .*/' posted an event - "' . $request['title'] . '"';
			$message = $subject;
			$link = '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;

			$User_activity = new UserActivity;
			$User_activity->user_id_fk = $user['id'];
			$User_activity->activity_id = $Event->id;
			$User_activity->activity_title = $subject;
			$User_activity->activity_link = $link;
			$User_activity->activity_table_name = 'mst_event';
			$User_activity->activity_action = 'posted event';
			$User_activity->save();

			$insert_data = array(
				'notification_from' => $user['id'],
				'notification_type' => '7',
				'subject' => $subject,
				'message' => trim($message),
				'notification_status' => 'send',
				'created_at' => date('Y-m-d H:i:s'),
				'link' => $link,
			);
			$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
		}

		if ($request['auto_save_flag'] == "true") {
			return $lastInsertId;
		}

		/*         * ****** for guest********** */
		if (count($request['guest_name']) > 0 && $request['guest_name'][0] != '') {
			$arr_guest = array();
			foreach ($request['guest_name'] as $key => $value) {

				foreach ($request['designation'] as $key2 => $val1) {
					foreach ($request['profile_image'] as $key3 => $val2) {
						if ($key2 == $key && $key3 == $key) {
							$name = $arr_guest[$key]['name'] = $value;
							$des = $arr_guest[$key]['designation'] = $val1;
							$profile_image = $arr_guest[$key]['profile_image'] = $val2;
							$event_guest = new eventGuest;
							$event_guest->event_id_fk = $lastInsertId;
							$event_guest->name = $name;
							$event_guest->designation = $des;

							$random_no = rand(1000, 9999);
							if (Input::hasFile('profile_image')) {
								$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image";

								$obj = new Filesystem();
								if (!$obj->exists($destinationPath)) {

									$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
								}
								try {
									$extension = $profile_image->getClientOriginalExtension();
									$fileName = $random_no . '.' . $extension;
									$image = \Image::make($profile_image);
									$image->save($destinationPath . '/' . $fileName);
									$event_guest->profile_image = $fileName;
									$image->resize(300, null, function ($constraint) {
										$constraint->aspectRatio();
									});
									$destinationPath = public_path() . "/assets/Frontend/images/event_guest_image/thumb";
									$obj = new Filesystem();
									if (!$obj->exists($destinationPath)) {
										$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
									}
									$image->save($destinationPath . '/' . $fileName);
								} catch (Exception $e) {
									Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later');
									return redirect('/mypost');
								}
							}
							$event_guest->save();
						}
					}
				}
			}
		}
		/*         * * Ends Here ***** */

		$get_data = DB::table('trans_event_follow_post')
			->where('user_id_fk', $user['id'])
			->where('event_id_fk', $lastInsertId)
			->get();
		if (count($get_data) == 0) {
			DB::table('trans_event_follow_post')->insert([
				['user_id_fk' => $user['id'], 'event_id_fk' => $lastInsertId, 'status' => '1'],
			]);
		}

		/* create url for SEO */
		if (isset($request['sub_groups'])) {
			$result = array_map('strrev', explode(',', strrev($request['venue'])));
			$country = ucwords(trim($result[0]));
			$city = ucwords(trim($request['location']));

			foreach ($request['sub_groups'] as $key => $val) {
				$groupdata = Group::where('id', $val)->get();

				$existingurlcountry = RegionalPostUrlModel::where('city_country', $country)
					->where('group', $groupdata[0]['group_name'])
					->where('posts', 'Events')->get();
				if (count($existingurlcountry) <= 0) {
					$addurl = new RegionalPostUrlModel();
					$addurl->posts = 'Events';
					$addurl->group = $groupdata[0]['group_name'];
					$addurl->city_country = $country;
					$addurl->url = 'go/' . $groupdata[0]['group_name'] . '/Events/CO/' . $country;
					$addurl->save();
				}

				$existingurlcity = RegionalPostUrlModel::where('city_country', $city)
					->where('group', $groupdata[0]['group_name'])
					->where('posts', 'Events')->get();
				if (count($existingurlcity) <= 0) {
					$addurl = new RegionalPostUrlModel();
					$addurl->posts = 'Events';
					$addurl->group = $groupdata[0]['group_name'];
					$addurl->city_country = $city;
					$addurl->url = 'go/' . $groupdata[0]['group_name'] . '/Events/CI/' . $city;
					$addurl->save();
				}
			}
			$sitemap = new SitemapController();
			$sitemap->index();
		}

              
		/*         * *********************** */
		if ($request['btn_post'] == '1') {
			Session::flash('success_msg', 'Event has been saved successfully.');
			return redirect('mypost');
		} else {
			Session::flash('success_msg', 'Event has been added successfully.');
			$link = '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $request['title']) . '-' . $lastInsertId;
                        return redirect()->guest($link);       
                        //return redirect('mypost');
		}
	}

	public function listPostEvent() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();

		if ($login) {
			return redirect(url('/'));
		}
		$data['header'] = array(
			"title" => 'Event Gallery',
			"keywords" => '',
			"description" => '',
		);
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_user_data = User::find($user_id);

		return view('Frontend.event.list')
			->with('finalData', $data)
			->with('arr_user_data', $arr_user_data)
			->with('title', 'Event');
	}

	public function getEventList() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$all = Input::all();
		$search_text = $all['search_text'];

		// Get the groups joined by the user
		$user_id = $data['user_session']['id'];
		$get_groups = DB::table('trans_group_users')
			->where('user_id_fk', $user_id)
			->get();
		// if user's group count is zero, redirect to signup2 to choose groups first
		if (count($get_groups) == 0) {
			Session::flash('error_msg', 'Please select atleast one group to proceed further.');
			return redirect()->guest('/signup2');
		}

		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$data['header'] = array(
			"title" => 'List Events',
			"keywords" => '',
			"description" => '',
		);

		$get_connections = DB::table('trans_user_connection')
			->where('from_user_id', $user_id)
			->orWhere('to_user_id', $user_id)
			->where('status', '0')
			->select('to_user_id', 'from_user_id')
			->get();
		$condition1 = 'null';
		$temp_array = array();
		$user_ids = array();
		$arr_ids = array();
		if (count($get_connections) > 0) {
			foreach ($get_connections as $key => $value) {
				$user_ids[] = $value->to_user_id;
				$user_ids[] = $value->from_user_id;

				foreach ($user_ids as $ids) {
					if (!in_array($ids, $arr_ids)) {
						$arr_ids[] = $ids;
					}
				}
			}
			$condition1 = implode(',', $arr_ids);
		}

		$get_groups = DB::table('trans_group_users')
			->where('user_id_fk', $user_id)
			->get();
		foreach ($get_groups as $key => $value) {
			$user_group[$key] = $value->group_id_fk;
		}
		$condition = implode(',', $user_group);
		$arr_events = DB::table('mst_event as e');
		$arr_events->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
		if ($search_text != '') {
			$arr_events->where('e.title', 'LIKE', '%' . trim($search_text) . '%');
		}
		$arr_events->where('e.status', '=', '1');
		$arr_events->whereIn('g.group_id_fk', $user_group);
		$arr_events->groupBy('e.id');
		$arr_events->orderBy('e.updated_at', 'DESC');

		$arr_events->select('e.*', 'e.title as name', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description', 'e.profile_image as profile_picture');
		$arr_events = $arr_events->get();

		foreach ($arr_events as $key => $value) {

			$arr_event_comment = DB::table('mst_comments as mc')
				->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
				->where('tec.event_id_fk', $arr_events[$key]->id)
				->select('mc.*')
				->get();
			$arr_events[$key]->count = count($arr_event_comment);
		}
		if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
			$no = $all['slice_arr_count'];
		} else {
			$no = 0;
		}
		$arr_event = $arr_events->slice($no, 10);
		$slice_arr_count = $no + 10;

		$total_count = count($arr_events);
		$arr_user_data = User::find($user_id);
		return view('Frontend.event.all-posts')->with('arr_all_records', $arr_event)->with('absolute_path', $data['absolute_path'])->with('total_count', $total_count)->with('arr_user_data', $arr_user_data)->with('slice_arr_count', $slice_arr_count);
	}

	public function listUserEvent() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();

		if ($login) {
			return redirect(url('/'));
		}
		$data['header'] = array(
			"title" => 'My Event',
			"keywords" => '',
			"description" => '',
		);
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_user_data = User::find($user_id);

		return view('Frontend.event.user-event')
			->with('finalData', $data)
			->with('arr_user_data', $arr_user_data)
			->with('title', 'Event');
	}

	public function getUserEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$all = Input::all();
		$page_number = $all['page'];
		$search_text = $all['search_text'];
		$total_records = $page_number * 10;

		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$data['header'] = array(
			"title" => 'List Events',
			"keywords" => '',
			"description" => '',
		);

		$arr_events = DB::table('mst_event as e');
		$arr_events->where('user_id_fk', $user_id);
		if ($search_text != '') {
			$arr_events->where('e.title', 'LIKE', '%' . trim($search_text) . '%');
		}
		$arr_events->groupBy('e.id');
		$arr_events->orderBy('e.id', 'DESC');
		$arr_events->select('e.*', 'e.title as name', 'e.profile_image as profile_picture', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description');
		$arr_events = $arr_events->get();

		foreach ($arr_events as $key => $value) {

			$arr_event_comment = DB::table('mst_comments as mc')
				->join('trans_event_comments as tec', 'mc.id', '=', 'tec.comment_id_fk', 'left')
				->where('tec.event_id_fk', $arr_events[$key]->id)
				->select('mc.*')
				->get();
			$arr_events[$key]->count = count($arr_event_comment);
		}
		if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
			$no = $all['slice_arr_count'];
		} else {
			$no = 0;
		}
//        echo $no;die;
		//        echo '<pre>';
		//                print_r($arr_events);die;
		$arr_event = $arr_events->slice($no, 10);
//        echo '<pre>';
		//                print_r($arr_event);die;
		$slice_arr_count = $no + 10;
		$arr_user_data = User::find($user_id);
		$total_count = count($arr_events);
		return view('Frontend.event.all-user-posts')->with('arr_all_records', $arr_event)->with('absolute_path', $data['absolute_path'])->with('total_count', $total_count)->with('arr_user_data', $arr_user_data)->with('slice_arr_count', $slice_arr_count);
	}

	public function viewEvent($view_id, $response='') {
		$event_id = $view_id;
		$id = explode('-', $event_id);
		if (count($id) > 1) {
			$event_id = $id[1];
		}
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];

		$arr_events = DB::table('mst_event')
			->where('id', $event_id)
			->select('mst_event.*')
			->get();

                $ticketsnum = array();
		if($arr_events[0]->onlineTicket == '1') {
                    $ticketsnum = $this->getTicketsBooked($user_id, $event_id, 'event'); // To get number of tickets booked for each cost id
                }

		if (count($arr_events) > 0) {
			foreach ($arr_events as $key => $value) {
				$arr_event_comment = DB::table('mst_comments as mc')
					->join('trans_event_comments as tbc', 'mc.id', '=', 'tbc.comment_id_fk', 'left')
					->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
					->where('tbc.status', '1')
					->where('tbc.event_id_fk', $arr_events[0]->id)
					->orderBy('id', 'DESC')
					->select('mc.*', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture')
					->get();
				$arr_events[$key]->comment = $arr_event_comment;
				$arr_events[$key]->count = count($arr_event_comment);
			}
			/*             * *************follow*********************** */
			$event_id = $arr_events[0]->id;
			$arr_event_data = Event::find($event_id);
			$user_id_fk = $arr_event_data['user_id_fk'];
			$arr_user1_data = User::find($user_id_fk);

			$arr_follower = DB::table('trans_follow_user')
				->where('user_id_fk', $user_id_fk)
				->select(DB::raw('count(follower_id_fk) as follower'))
				->get();

			/*             * **************upvote Downvote********************* */

			$is_upvotes = DB::table('trans_event_upvote_downvote')
				->where('event_id_fk', $event_id)
				->where('user_id_fk', $user_id)
				->select('status')
				->get();
			$total_upvotes = DB::table('trans_event_upvote_downvote')
				->where('event_id_fk', $event_id)
				->where('status', '0')
				->select('status')
				->get();

			$total_downvotes = DB::table('trans_event_upvote_downvote')
				->where('event_id_fk', $event_id)
				->where('status', '1')
				->select('status')
				->get();

			$is_follower = DB::table('trans_follow_user')
				->where('user_id_fk', $user_id_fk)
				->where('follower_id_fk', $user_id)
				->get();

			$is_connect = DB::table('trans_user_connection')
				->where('from_user_id', $user_id_fk)
				->where('to_user_id', $user_id)
				->orWhere('from_user_id', $user_id)
				->Where('to_user_id', $user_id_fk)
				->get();

			$is_reported = DB::table('mst_make_report')
				->where('event_id', $event_id)
				->where('user_id_fk', $user_id)
				->get();

			/*             * ************* AUDIENCE ************** */
			$audience = DB::table('mst_visitor')
				->where('feature_id', $event_id)
				->where('type', '1')
				->select(DB::raw('count(type) as audience_count'))
				->get();

			/*             * ************Related Events********** */

			/* details of connection and audiance */
			$audience_details = DB::table('mst_visitor as mv')
				->join('mst_users as mu','mu.id', '=', 'mv.visitor_id')
				->join('trans_event_rsvp_status as ters','ters.user_id_fk','=', 'mu.id')
				->join('mst_event_cost as mec', 'ters.cost_id_fk','=','mec.id')
				->where('feature_id', $event_id)
				->where('type', '1')
				->where('ters.event_id_fk', $event_id)
				->orderBy('ters.updated_at','DESC')
				->select('mu.first_name', 'mu.id', 'mu.profile_picture', 'mu.gender', 'mu.profession', 'mu.last_name', 'mu.email', 'mu.location' , 'mu.mob_no','mec.currency', 'mec.cost')
				->get();

			$connection_details = DB::table('trans_user_connection as tuc')
				->join('mst_users as mu', 'mu.id', '=', 'tuc.to_user_id')
				->where('tuc.from_user_id', $user_id_fk)
				->where('tuc.to_user_id', $user_id)
				->orWhere('tuc.from_user_id', $user_id)
				->Where('tuc.to_user_id', $user_id_fk)
				->select('mu.first_name', 'mu.id', 'mu.profile_picture', 'mu.gender', 'mu.profession', 'mu.last_name')
				->get();
			/* ends here */

			$user_group = array();
			$get_groups = DB::table('trans_group_users')
				->where('user_id_fk', $user_id)
				->get();
			foreach ($get_groups as $key => $value) {
				$user_group[$key] = $value->group_id_fk;
			}
			$condition = implode(',', $user_group);
			$related_events = DB::table('mst_event as e')
				->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id')
				->whereIn('g.group_id_fk', explode(',', $condition))
				->groupBy('e.id')
				->orderBy('e.id', 'DESC')
				->limit(20)
				->select('e.id', 'e.title as name', 'e.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
				->get();
			/*             * ************************************ */
			/*             * ****** For Agenda Session and Time ******** */
			$arr_agenda = DB::table('mst_event_agenda')
				->where('event_id_fk', $event_id)
				->select('*')
				->get();

			$arr_event_agenda_details = array();
			if (count($arr_agenda) > 0) {
				foreach ($arr_agenda as $key => $agenda) {
					$arr_event_agenda_details[$key] = $agenda;

					$arr_agenda_session_details = DB::table('trans_event_agenda_session_time')
						->where('agenda_id_fk', $agenda->id)
						->select('*')
						->get();

					$arr_event_agenda_details[$key]->session_details = $arr_agenda_session_details;
				}
			}

			/*             * ***** Ends Agenda Session and Time ****** */
			/*             * ****** For Cost ******* */
			$arr_cost_detail = array();
			$arr_cost_detail = DB::table('mst_event_cost')
				->where('event_id_fk', $event_id)
				->select('*')
				->get();
			/*             * ****** Ends Cost ********* */
			/*             * **** For List Of Guest ****** */

			$arr_event_guest = DB::table('mst_event_guest')
				->where('event_id_fk', $event_id)
				->select('*')
				->get();

			/*             * ***** Ends Here ******* */
			/*             * *********for group************ */
			$get_groups = DB::table('trans_event_group as t')
				->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
				->where('event_id_fk', $event_id)
				->select('mg.group_name', 'mg.id', 'mg.profession')
				->get();
			$event_group = array();
			foreach ($get_groups as $key => $value) {
				$event_group[$key] = $value->id;
			}

			/*             * ************* */
			/*             * * Comment Count *** */
			$arr_user_group = array_unique($user_group);
			$arr_event_group = array_unique($event_group);
			$missed_group = array_diff($arr_event_group, $arr_user_group);
			$m = implode(' ', $missed_group);
			/*             * ****** cost rsvp********* */

			$get_cost = DB::table('trans_event_rsvp_status')
				->where('event_id_fk', $event_id)
				->where('user_id_fk', $user_id)
				->select('*')
				->get();

			/*             * ** Ends Here ***** */

			/* Get record for save post */

			$get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $arr_events[0]->id)->where('type', '1')->first();

			$arr_user_data = User::find($user_id);
			$data['header'] = array(
				"title" => $arr_events[0]->title . ' - Event | ATG',
				"meta_title" => $arr_events[0]->title,
				"meta_keywords" => $arr_events[0]->tag,
				"meta_description" => $arr_events[0]->description,
			);
		} else {
			return redirect()->guest('/error');
		}

		$is_postfollower = DB::table('trans_event_follow_post as t')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->select('t.status')
			->get();

		$total_unfollowPost = DB::table('trans_event_follow_post')
			->where('event_id_fk', $event_id)
			->where('status', '1')
			->select('status')
			->get();
		$total_followPost = DB::table('trans_event_follow_post')
			->where('event_id_fk', $event_id)
			->where('status', '0')
			->select('status')
			->get();

                $promotion_det = promotionModel::where('status', '1')->get();
                $promotion = array();
                if($arr_user1_data['id'] == $arr_user_data['id'])
                {
                    $promotion = postPromotion::where('post_id_fk', $event_id)->where('status', '1')->whereraw('CURDATE() between `promo_start_date` and `promo_end_date`')->get();
                }
                
		return view('Frontend.event.details', ['is_reported' => $is_reported,
                        'promotion' => $promotion,
                        'promotion_det' => $promotion_det,
                        'response' => $response,
			'ticketsnum' => $ticketsnum,
			'get_cost' => $get_cost,
			'event_id' => $event_id,
			'missed_group' => $m,
			'finalData' => $data,
			'eventData' => $arr_events,
			'audience_event' => $audience,
			'arr_user_data' => $arr_user_data,
			'arr_user1_data' => $arr_user1_data,
			'arr_follower' => $arr_follower,
			'audience_details' => $audience_details,
			'connection_details' => $connection_details,
			'arr_event_data' => $arr_event_data,
			'is_connect' => $is_connect,
			'is_upvotes' => $is_upvotes,
			'is_postfollower' => $is_postfollower,
			'is_follower' => $is_follower,
			'total_upvotes' => count($total_upvotes),
			'total_downvotes' => count($total_downvotes),
			'total_followPost' => count($total_followPost),
			'total_unfollowPost' => count($total_unfollowPost),
			'related_event' => $related_events,
			'arr_event_agenda_details' => $arr_event_agenda_details,
			'arr_cost_detail' => $arr_cost_detail,
			'arr_event_guest' => $arr_event_guest,
			'get_groups' => $get_groups,
			'get_save_user_post' => $get_save_user_post,
			'arr_user_data' => $arr_user_data]);
	}

	function notifyFollowingUsers($user_account, $post_id, $last_comment_id) {
		$this->PostCommentNotificationMailfx($user_account, $post_id, $last_comment_id, 'event');
	}

	function upDownVoteEvent() {

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}
		$data['user_session'] = Session::get('user_account');
		$request = Input::all();
		$event_id = $request['id'];
		$status = $request['status'];
		$user_id = $data['user_session']['id'];

		/*         * ****************for ranking purpose******************** */
		$get_group_ids = DB::table('trans_event_group as t')
			->join('mst_group as mg', 'mg.id', '=', 't.group_id_fk', 'left')
			->where('event_id_fk', $event_id)
			->select('mg.id')
			->get();

		/*         * ** Ends Here ***** */

		$get_data = DB::table('trans_event_upvote_downvote')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->get();

		if (count($get_data) == 0) {
//            dd('dfds');
			DB::table('trans_event_upvote_downvote')->insert([
				['user_id_fk' => $user_id, 'event_id_fk' => $event_id, 'status' => $status],
			]);
			foreach ($get_group_ids as $val) {
				DB::table('vote_statistics')->insert([
					['group_id_fk' => $val->id, 'user_id_fk' => $user_id, 'post_id' => $event_id, 'vote' => $status],
				]);
			}
		} else {
			$update = DB::table('trans_event_upvote_downvote')
				->where('event_id_fk', $event_id)
				->where('user_id_fk', $user_id)
				->update(['status' => $status]);
			foreach ($get_group_ids as $val) {
				$update = DB::table('vote_statistics')
					->where(['post_id' => $event_id, 'user_id_fk' => $user_id, 'group_id_fk' => $val->id])
					->update(['vote' => $status]);
			}
		}
		$get_data = DB::table('trans_event_upvote_downvote')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->get();
		$total_downvotes = DB::table('trans_event_upvote_downvote')
			->where('status', '1')
			->where('event_id_fk', $event_id)
			->get();
		$total_downvotes = count($total_downvotes);
		$total_upvotes = DB::table('trans_event_upvote_downvote')
			->where('status', '0')
			->where('event_id_fk', $event_id)
			->get();
		$total_upvotes = count($total_upvotes);
		$get_event_data = DB::table('mst_event')
			->where('id', $event_id)
			->get();

		$get_save_user_post = saveUserPost::where('user_id_fk', $user_id)->where('post_id_fk', $event_id)->where('type', '0')->first();

		$is_postfollower = DB::table('trans_event_follow_post as t')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->select('t.status')
			->get();

		$total_unfollowPost = DB::table('trans_event_follow_post')
			->where('event_id_fk', $event_id)
			->where('status', '1')
			->select('status')
			->get();
		$total_followPost = DB::table('trans_event_follow_post')
			->where('event_id_fk', $event_id)
			->where('status', '0')
			->select('status')
			->get();

		$notification_from_user_id = $data['user_session']['id'];
		// Following function defined in App\Traits\UpvoteDownvoteEvents
		// Send Notification on ATG, Android, iOS and emails
		$this->UpvoteDownvoteSendNotificationEmailfx($commonModel, 'event', $status, $get_event_data, $notification_from_user_id);

		?>
        <button id="like" name="like" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $event_id ?>, 0,<?php echo count($get_save_user_post) ?>);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>
        <button id="unlike" name="attend" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $event_id ?>, 1, <?php echo count($get_save_user_post) ?>);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>
        <?php if (isset($is_postfollower) && count($is_postfollower) > 0 && $is_postfollower[0]->status == '0') {
			?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
				$is_postfollower[0]->status == '0') ? 'acti-up-dw' : ''; ?>" onclick = "addFollowPost('<?php echo $get_event_data[0]->id ?>', 1, 2);">
            <i class="fa fa-foursquare"></i> Unfollow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php } else {
			?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($is_postfollower) && count($is_postfollower) > 0 &&
				$is_postfollower[0]->status == '1') ? 'acti-up-dw' : ''; ?>" onclick = "addFollowPost('<?php echo $get_event_data[0]->id ?>', 0, 1);">
            <i class="fa fa-foursquare"></i> Follow <span class="btn-counters"><?php echo (isset($total_followPost)) ? count($total_followPost) : 0; ?></span></button>
        <?php }?>
        <?php

	}

	public function send_notification($registration_id, $message = '') {
		//using fcm
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => array($registration_id),
			'data' => array("message" => $message),
		);
		$fields = json_encode($fields);
		$headers = array(
			'Authorization: key=' . env('GOOGLE_API_KEY'),
			'Content-Type: application/json',
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$result = curl_exec($ch);
//        echo '<pre>';print_r($result);die;
		// Close connection
		curl_close($ch);
		return $result;
//        echo $result;
	}

	public function commentEvent() {
		$request=Input::all();
        $resp=json_decode($this->commentfx('event',$request['post_event_id']));

        if($resp->error_code==0)
        {Session::flash('success_msg', 'Comment has been added successfully.');
        return redirect('view-event/' . $request['post_event_id']);
        }
        elseif($resp->error_code==1){
            return redirect(url('view-event/' . base64_encode($request['post_event_id'])))->withErrors($resp->errmsg)->withInput();
        }
        elseif($resp->error_code==2){
            Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
            return redirect('/view-event/-'.$request['post_event_id']);
		}
	}

	function followPostEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}
		$data['user_session'] = Session::get('user_account');
		$request = Input::all();
		$user_account = Session::get('user_account');
		$follower_user_id = $request['follower_user_id'];
		//echo '<pre>'; print_r($request); echo '</pre>';exit;

		$event_id = $request['event_id'];
		$status = $request['status'];
		$user_id = $data['user_session']['id'];

		$get_voting_data = DB::table('trans_event_upvote_downvote')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->get();

		$total_downvotes = DB::table('trans_article_upvote_downvote')
			->where('status', '1')
			->where('article_id_fk', $event_id)
			->get();
		$total_downvotes = count($total_downvotes);
		$total_upvotes = DB::table('trans_article_upvote_downvote')
			->where('status', '0')
			->where('article_id_fk', $event_id)
			->get();
		$total_upvotes = count($total_upvotes);

		$get_data = DB::table('trans_event_follow_post')
			->where('user_id_fk', $user_id)
			->where('event_id_fk', $event_id)
			->get();
		//echo '<pre>'; print_r($request); echo '</pre>'; exit;
		if (count($get_data) == 0) {
			DB::table('trans_event_follow_post')->insert([
				['user_id_fk' => $user_id, 'event_id_fk' => $event_id, 'status' => $status],
			]);
		} else {
			$update = DB::table('trans_event_follow_post')
				->where('event_id_fk', $event_id)
				->where('user_id_fk', $user_id)
				->update(['status' => $status]);
		}
		$total_unfollowPost = DB::table('trans_event_follow_post')
			->where('status', '1')
			->where('event_id_fk', $event_id)
			->get();
		$total_unfollowPost = count($total_unfollowPost);
		$total_followPost = DB::table('trans_event_follow_post')
			->where('status', '0')
			->where('event_id_fk', $event_id)
			->get();
		$total_followPost = count($total_followPost);

		$get_event_data = DB::table('mst_event')
			->where('id', $event_id)
			->get();

		$get_save_user_post = saveUserPost::where('user_id_fk', $user_account['id'])
			->where('post_id_fk', $event_id)
			->where('type', '1')->first();

		$notification_from_user_id = $data['user_session']['id'];
		// Following function defined in App\Traits\Post\PostActions
		$this->UserFollowsPostSendNotificationEmailfx($commonModel, 'event', $status, $get_event_data, $notification_from_user_id);
		?>

        <button id="like" name="like" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '0') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $event_id ?>, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters"><?php echo (isset($total_upvotes)) ? $total_upvotes : '0'; ?></span></button>

        <button id="unlike" name="attend" class="btn <?php echo (isset($get_voting_data) && count($get_voting_data) > 0 && $get_voting_data[0]->status == '1') ? 'acti-up-dw disabled' : ''; ?>" onclick="upvoteDownvote(<?php echo $event_id ?>, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters"><?php echo (isset($total_downvotes)) ? $total_downvotes : '0'; ?></span></button>

        <?php if (isset($status) && $status == '0') {?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '1') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $event_id ?>, 1);"><i class="fa fa-foursquare"></i> Unfollow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php } else {?>
            <button id="followPost" name="followPost" class="btn <?php echo (isset($get_data) && count($get_data) > 0 && $get_data[0]->status == '0') ? 'acti-up-dw' : ''; ?>"  onclick="addFollowPost(<?php echo $event_id ?>, 0);"><i class="fa fa-foursquare"></i> Follow<span class="btn-counters follow_post_count"><?php echo (isset($total_followPost)) ? $total_followPost : '0'; ?></span></button>
        <?php }?>

        <?php
}

	public function searchEventAjax() {
		$request = Input::all();
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}

		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];

		$get_groups = DB::table('trans_group_users')
			->where('user_id_fk', $user_id)
			->get();
		foreach ($get_groups as $key => $value) {
			$user_group[$key] = $value->group_id_fk;
		}

		$condition = implode(',', $user_group);
		$keyword = $request['search_input_text'];

		$arr_event = DB::table('mst_event as e')
			->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id')
			->where('status', '1')
			->where('e.title', 'LIKE', '%' . $keyword . '%')
			->whereIn('g.group_id_fk', explode(',', $condition))
			->groupBy('e.id')
			->select('e.*', 'e.title as name', 'e.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
			->get();

		foreach ($arr_event as $key => $value) {

			$event_comment = commentEvent::where('event_id_fk', $value->id)->get();
			$arr_event[$key]->count = count($event_comment);
		}
		echo json_encode(array('arr_event' => $arr_event));
	}

	public function saveListEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}

		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_user_data = User::find($user_id);

		$data['header'] = array(
			"title" => 'Saved Event',
			"keywords" => '',
			"description" => '',
		);

		return view('Frontend.event.savelist')
			->with('finalData', $data)
			->with('arr_user_data', $arr_user_data);
	}

	public function getSaveEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$all = Input::all();

		$page_number = $all['page'];
		$search_text = $all['search_text'];
		$total_records = $page_number * 4;

		$arr_event = DB::table('mst_event as e');
		$arr_event->join('trans_save_user_posts as sp', 'e.id', '=', 'sp.post_id_fk');
		$arr_event->where('sp.user_id_fk', $user_id);
		$arr_event->where('sp.type', '1');
		if ($search_text != '') {
			$arr_event->where('e.title', 'LIKE', '%' . trim($search_text) . '%');
		}
		$arr_event->select('e.id as id', 'e.*', 'e.profile_image as profile_picture');
		$arr_event = $arr_event->get();

		if (isset($all['slice_arr_count']) && $all['slice_arr_count'] != '') {
			$no = $all['slice_arr_count'];
		} else {
			$no = 0;
		}
		$arr_events = $arr_event->slice($no, 10);
		$slice_arr_count = $no + 10;

		$total_count = count($arr_event);
		$arr_user_data = User::find($user_id);
		return view('Frontend.event.all-saved-posts')->with('arr_all_records', $arr_events)->with('absolute_path', $data['absolute_path'])->with('total_count', $total_count)->with('arr_user_data', $arr_user_data)->with('slice_arr_count', $slice_arr_count);
	}

	public function savePostEvent() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$request = Input::all();
		$event_save = Event::find($request['post_event_id']);
		$event_save->status = '1';
		$event_save->save();
		Session::flash('message', 'Event has been successfully posted.');
		return redirect('list-event');
	}

	function ajaxCostEvent() {
		$request = Input::all();
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$post_id = $request['post_id'];

		if (isset($request['group_id'])) {
			$group_id = $request['group_id'];
			$check_group_id = GroupUser::where('group_id_fk', $group_id)->where('user_id_fk', $user_id)->get();
			if (count($check_group_id) > 0) {

			} else {
				$groupuser = new GroupUser();
				$groupuser->group_id_fk = $group_id;
				$groupuser->user_id_fk = $user_id;
				$groupuser->status = '0';
				$groupuser->created_at = date("Y-m-d H:i:s");
				$groupuser->save();
			}
			$arr_event_rsvp_exist = DB::table('trans_event_rsvp_accept')
				->where('event_cost_id_fk', $event_id)
				->where('post_id_fk', $post_id)
				->where('user_id_fk', $user_id)
				->select('*')
				->get();
			$arr_count = count($arr_event_rsvp_exist);

			if ($arr_count > 0) {
				DB::table('trans_event_rsvp_accept')
					->where('event_cost_id_fk', $event_id)
					->where('post_id_fk', $post_id)
					->where('user_id_fk', $user_id)
					->delete();
			} else {
				$event_rsvp_cost = new likeEvent;
				$event_rsvp_cost->event_cost_id_fk = $event_id;
				$event_rsvp_cost->post_id_fk = $post_id;
				$event_rsvp_cost->user_id_fk = $user_id;

				$event_rsvp_cost->save();
			}
		} else {
			$event_id = $request['event_id'];
			$arr_event_rsvp_exist = DB::table('trans_event_rsvp_accept')
				->where('event_cost_id_fk', $event_id)
				->where('post_id_fk', $post_id)
				->where('user_id_fk', $user_id)
				->select('*')
				->get();
			$arr_count = count($arr_event_rsvp_exist);

			if ($arr_count > 0) {
				DB::table('trans_event_rsvp_accept')
					->where('event_cost_id_fk', $event_id)
					->where('post_id_fk', $post_id)
					->where('user_id_fk', $user_id)
					->delete();
			} else {
				$event_rsvp_cost = new eventRSVPCost;
				$event_rsvp_cost->event_cost_id_fk = $event_id;
				$event_rsvp_cost->post_id_fk = $post_id;
				$event_rsvp_cost->user_id_fk = $user_id;

				$event_rsvp_cost->save();
			}
		}
	}

	public function deleteAgenda() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$data['user_session'] = Session::get('user_account');
		$arr_user_data = User::find($data['user_session']['id']);

		$request = Input::all();
		$event_id = $request['event_id'];

		DB::table('mst_event_agenda')
			->where('event_id_fk', $event_id)
			->delete();
		echo json_encode(array('success' => 1));
	}

	public function makeReport($event_id) {
		$event_id = base64_decode($event_id);
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$request = Input::all();

		if (isset($request['report']) && ($request['report'] != '')) {
			$report = $request['report'];
		} else {
			$report = '';
		}

		if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
			$sub_group = $request['sub_group'];
			$group = group::find($sub_group);
			$group_name = $group['group_name'];
		} else {
			$sub_group = '';
			$group_name = '';
		}

		if ($report == '0') {
			$report_name = 'Junk';
		} elseif ($report == '1') {
			$report_name = 'Uninteresting';
		} elseif ($report == '2') {
			$report_name = 'Hatred or Vulgarity';
		} else {
			$report_name = '';
		}
		/*         * ***Notification***** */
		$get_event_data = DB::table('mst_event')
			->where('id', $event_id)
			->get();
		$subject = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . '"';
		$link = 'view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_event_data[0]->title) . '-' . $get_event_data[0]->id;
		$message = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . '"';
		$link = 'view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_event_data[0]->title) . '-' . $get_event_data[0]->id;
// Commenting spam notifications go to the user who posted
		// TODO remove the code later
		/*        $insert_data = array(
	//                        'notification_from' => $user_account['id'],
	            'notification_to' => $get_event_data[0]->user_id_fk,
	            'notification_type' => "6",
	            'subject' => $subject,
	            'message' => trim($message),
	            'notification_status' => 'send',
	            'created_at' => date('Y-m-d H:i:s'),
	            'link' => $link,
	        );
	        $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);

*/
		$arr_to_user = User::where('id', '=', $get_event_data[0]->user_id_fk)->get();
		if ($arr_to_user[0]->device_name == "1") {
			//ios
			$this->ios_notificaton($arr_to_user[0]->registration_id);
		} else {
			//android
			$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_event_data[0]->user_id_fk, 'event_id' => $event_id, 'flag' => 'Report')));
		}

		/*         * ***end***** */
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_event_data = Event::find($event_id);
		$arr_user_data = User::find($user_id);
		$email = $arr_user_data['email'];
		$contact_email = $data['global']['contact_email'];
		$macros_array_detail = array();
		$macros_array_detail = email_template_macros::all();
		$macros_array = array();
		foreach ($macros_array_detail as $row) {
			$macros_array[$row['macros']] = $row['value'];
		}
		$email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
		$arr_admin_detail = User::where('email', $contact_email)->first();
		$admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
		$content = $email_contents['email_template_content'];

		$reserved_words = array();
		$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
		$reserved_arr = array(
			"||SITE_TITLE||" => stripslashes($data['global']['site_title']),
			"||SITE_PATH||" => url('/'),
			"||ADMIN_NAME||" => $arr_admin_detail['user_name'],
			"||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
			"||USER_EMAIL||" => $arr_admin_detail['email'],
			"||PROGRAMME_TYPE||" => 'an event',
			"||PROGRAMME_TITLE||" => ucwords($arr_event_data['title']),
			"||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
			"||LOGIN_LINK||" => $admin_login_link,
		);
		if ($report_name != '') {
			$reserved_arr['||NAME||'] = ' as ' . $report_name;
		} elseif ($group_name != '') {
			$reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
		}

		$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
		foreach ($reserved_words as $k => $v) {
			$content = str_replace($k, $v, $content);
		}
		/* getting mail subect and mail message using email template title and lang_id and reserved works */
		$contents_array = array("email_conents" => $content);

		//$contents_array = array("email_conents" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']) . ' reported on ' . ucwords($arr_event_data['title']));
		if (count(DB::table('mst_make_report')->where('event_id', $event_id)->where('user_id_fk', $user_id)->get()) > 0) {

			$update = DB::table('mst_make_report')
				->where('event_id', $event_id)
				->where('user_id_fk', $user_id)
				->update(
					['report_type' => $report, 'group_id' => $sub_group]
				);

			Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($email, $email_contents, $data) {
				$message->from($email, $data['global']['site_title']);
				$message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
			}); // End Mail
		} else {
			$insert = DB::table('mst_make_report')
				->insert(
					['user_id_fk' => $user_id, 'event_id' => $event_id, 'group_id' => $sub_group, 'report_type' => $report]
				);
			Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($email, $email_contents, $data) {
				$message->from($email, $data['global']['site_title']);
				$message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
			}); // End Mail
		}
		Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully.');
		echo json_encode(array('msg' => 1));
	}

	public function makeGroupReport($event_id) {
		$event_id = base64_decode($event_id);
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$request = Input::all();

		if (isset($request['report']) && ($request['report'] != '')) {
			$report = $request['report'];
		} else {
			$report = '';
		}

		if (isset($request['sub_group']) && (!empty($request['sub_group']))) {
			$sub_group = $request['sub_group'];
			$group = group::find($sub_group);
			$group_name = $group['group_name'];
		} else {
			$sub_group = '';
			$group_name = '';
		}

		if ($report == '0') {
			$report_name = 'Junk';
		} elseif ($report == '1') {
			$report_name = 'Uninteresting';
		} elseif ($report == '2') {
			$report_name = 'Hatred or Vulgarity';
		} else {
			$report_name = 'Does not belong in';
		}
		/*         * ****Notification******** */
		$get_event_data = DB::table('mst_event')
			->where('id', $event_id)
			->get();
		$subject = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . ' ' . strtoupper($group_name) . '"';
		$link = 'view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_event_data[0]->title) . '-' . $get_event_data[0]->id;
		$message = 'Your event "' . $get_event_data[0]->title . '" has been reported as "' . $report_name . ' ' . strtoupper($group_name) . '"';
		$link = 'view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $get_event_data[0]->title) . '-' . $get_event_data[0]->id;
// Commenting spam notifications go to the user who posted
		// TODO remove the code later
		/*          $insert_data = array(
	//                        'notification_from' => $user_account['id'],
	            'notification_to' => $get_event_data[0]->user_id_fk,
	            'notification_type' => "6",
	            'subject' => $subject,
	            'message' => trim($message),
	            'notification_status' => 'send',
	            'created_at' => date('Y-m-d H:i:s'),
	            'link' => $link,
	        );
	        $last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
*/
		$arr_to_user = User::where('id', '=', $get_event_data[0]->user_id_fk)->get();
		if ($arr_to_user[0]->device_name == "1") {
			//ios
			$this->ios_notificaton($arr_to_user[0]->registration_id);
		} else {
			//android
			$this->send_notification($arr_to_user[0]->registration_id, json_encode(array('msg' => $message, 'user_id' => $get_event_data[0]->user_id_fk, 'event_id' => $event_id, 'flag' => 'ReportWithGroup')));
		}
		/*         * ******** */
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_event_data = Event::find($event_id);
		$arr_user_data = User::find($user_id);
		$email = $arr_user_data['email'];
		$contact_email = $data['global']['contact_email'];
		$macros_array_detail = array();
		$macros_array_detail = email_template_macros::all();
		$macros_array = array();
		foreach ($macros_array_detail as $row) {
			$macros_array[$row['macros']] = $row['value'];
		}
		$email_contents = email_templates::where(array('email_template_title' => 'make-report', 'lang_id' => '14'))->first();

//$email_contents = end($email_contents);
		$arr_admin_detail = User::where('email', $contact_email)->first();
		$admin_login_link = '<a href="' . url('/') . '" target="_new">User Login ' . stripslashes($data['global']['site_title']) . ' administration</a>';
		$content = $email_contents['email_template_content'];

		$reserved_words = array();
		$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';
		$reserved_arr = array(
			"||SITE_TITLE||" => stripslashes($data['global']['site_title']),
			"||SITE_PATH||" => url('/'),
			"||ADMIN_NAME||" => $arr_admin_detail['user_name'],
			"||USER_NAME||" => $arr_admin_detail['first_name'] . ' ' . $arr_admin_detail['last_name'],
			"||USER_EMAIL||" => $arr_admin_detail['email'],
			"||PROGRAMME_TYPE||" => 'an event',
			"||PROGRAMME_TITLE||" => ucwords($arr_event_data['title']),
			"||LOGIN_USER_NAME||" => ucfirst($arr_user_data['first_name']) . ' ' . ucfirst($arr_user_data['last_name']),
			"||LOGIN_LINK||" => $admin_login_link,
		);
		if ($report_name != '') {
			$reserved_arr['||NAME||'] = ' as ' . $report_name;
		} elseif ($group_name != '') {
			$reserved_arr['||NAME||'] = ' it does not belong in ' . $group_name . ' group';
		}

		$reserved_words = array_replace_recursive($macros_array, $reserved_arr);
		foreach ($reserved_words as $k => $v) {
			$content = str_replace($k, $v, $content);
		}
		/* getting mail subect and mail message using email template title and lang_id and reserved works */
		$contents_array = array("email_conents" => $content);

		Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($email, $email_contents, $data) {
			$message->from($email, $data['global']['site_title']);
			$message->to($data['global']['contact_email'], 'Admin')->subject($email_contents['email_template_subject']);
		}); // End Mail
		Session::flash('success_msg', 'Thank you for reporting us.Your report has been sent successfully.');
		echo json_encode(array('msg' => 1));
	}

	function frontRepost($event_id, $created_at) {
		$event_id = base64_decode($event_id);
		$created_at = base64_decode($created_at);

		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}
		$data['user_session'] = Session::get('user_account');
		$arr_user_data = User::find($event_id);

		$get_data = DB::table('mst_event')
			->where('id', $event_id)
			->get();
		if (count($get_data) > 0) {
			$arr_to_update = array("created_at" => date("Y-m-d H:i:s"));

			DB::table('mst_event')
				->where('id', $event_id)
				->update($arr_to_update);

			echo json_encode(array("error" => "0", "error_message" => "Event has been repost successfully"));
		}
	}

	/*
		      Frontend::End Alls Frontend  Functions
	*/

	public function costRsvp() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();

		$data['user_session'] = Session::get('user_account');

		$user_id = $data['user_session']['id'];
		$request = Input::all();
		$cost_id = $request['cost_id'];
		$event_id = $request['event_id'];
		$status = $request['status'];

		$event_details = DB::table('mst_event')
		// ->join('mst_users', 'mst_users.id', '=', 'trans_events.user_id_fk')
			->where('mst_event.id', $event_id)
			->first();

		$organizer = DB::table('mst_users')
			->where('id', $event_details->user_id_fk)
			->first();

		$user_rspv = User::where('id',$user_id)->first();
		

		/************************** Send RSVP Mail to user ********************************/

		if ($status == 1) {

            $title = 'ATG.WORLD - Your Event\'s RSVP Yes notification';
			$email_contents = email_templates::where(array('email_template_title' => 'rsvp-yes-notification', 'lang_id' => '14'))->first();
			$content = $email_contents['email_template_content'];

			$reserved_arr = array(
				"||Name||" => ucwords($event_details->contact_name),
				"||User||" => ucwords($user_rspv->first_name . ' ' . $user_rspv->last_name),
				"||user_profile||" => url('/') . "/user-profile/" . base64_encode($user_rspv->id),
				"||Email||" => $user_rspv->email,
				"||Username||" => $user_rspv->user_name,
				"||Mobile||" => $user_rspv->mob_no,
				"||event_name||" => $event_details->title,
				"||event_link||" => url('/') . "/view-event/" . $event_details->title . '-' . $event_details->id,
				"||SITE_PATH||" => url('/'),
				"||profile_pic||" => url('/') . "/public/assets/Frontend/user/profile_pics/".$user_id."/thumb/" . $user_rspv->profile_picture,
			);

			if (empty($user_rspv['email'])) {
				$reserved_arr["||Email||"] = "No email";
			}

			if (empty($user_rspv['mob_no'])) {
				$reserved_arr["||Mobile||"] = "No mobile no.";
			}

			foreach ($reserved_arr as $k => $v) {
				$content = str_replace($k, $v, $content);
			}

			$content2 = '<div class="container" align="center">
                            <div class="row">
                                 <div class="col-sm-offset-2 col-sm-8"><a href="http://www.atg.world/"><img alt="ATG.WORLD Logo" width="200px" src="http://www.atg.world/assets/Frontend/img/logo-in.png"  class="img-responsive"  /></a></div>
                            </div>
                            <div class="row">
                                <p style="text-align:center">Dear ' . $reserved_arr["||Name||"] . ',</p>
                                <p style="text-align:center"><a href="' . $reserved_arr["||user_profile||"] . '">' . $reserved_arr["||User||"] . '</a> has confirmed RSVP Yes to your event <a href="' . $reserved_arr["||event_link||"] . '">' . $reserved_arr["||event_name||"] . '</a></p>
                                <h4 style="text-align:center"><strong>User Details</strong></h4>
                                <p style="text-align:center"> Username - ' . $reserved_arr["||Username||"] . ' </p>
                                <p style="text-align:center">Email - ' . $reserved_arr["||Email||"] . '</p>
                                <p style="text-align:center">Mobile - ' . $reserved_arr["||Mobile||"] . '</p>
                                <p style="text-align:center">&nbsp;</p>
                                <p style="text-align:center">Thank you,</p>
                                <p style="text-align:center">ACROSS THE GLOBE (ATG)</p>
                                <p style="text-align:center">' . $reserved_arr["||SITE_PATH||"] . '</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            </div>
                        </div>';

			$ims_data = ImsModel::where([['user_id_fk', '=', $user_rspv->id], ['title', '=', $title]])->first();
			$replies_object = new ImsRepliesModel();
			$replies_object->message_type = '0';

			if (!$ims_data) {
				$ims_object = new ImsModel();
				$ims_object->user_id_fk = $user_rspv->id;
				$ims_object->title = $title;
				$ims_object->save();
				$ims_data= $ims_object;
			}

			$check = ImsRepliesModel::where([['message_id_fk', '=', $ims_data->id], ['message_from', '=', $user_rspv->id], ['message_to', '=', $organizer->id]])->first();

			if ($check) {
				$replies_object->message_type = '1';
			}

			$replies_object->message_id_fk = $ims_data->id;
			$replies_object->message_from = $user_rspv->id;
			$replies_object->message_to = $organizer->id;
			$replies_object->message = $content2;
			$replies_object->attachment_name = '';
			$replies_object->is_attachment = '0';
			$replies_object->save();

			/* getting mail subect and mail message using email template title and lang_id and reserved works */
			$contents_array = array("email_conents" => $content);

			Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($event_details, $email_contents, $data) {
				$message->from($data['global']['contact_email'], $data['global']['site_title']);
				$message->to($event_details->email_address, ucwords($event_details->contact_name))->subject($email_contents['email_template_subject']);
			});

            /******************************** End Mail*********************************** */
		}


		$result = $this->fillRsvpOrBuyDetails($cost_id,$status, 'event','rsvp');
		if($result == 'update')
        {
            echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
        }
        elseif($result == 'insert')
        {
            echo json_encode(array('success' => 1));
        }



        /*$get_data = DB::table('trans_event_rsvp_status')
            ->where('event_id_fk', $event_id)
            ->where('user_id_fk', $user_id)
            ->get();

		if (count($get_data) > 0) {
			$cost_id_fk = $get_data[0]->cost_id_fk;
			if ($cost_id_fk == $cost_id) {
				if (count($get_data) > 0) {
					$arr_to_update = array("status" => $status);

					DB::table('trans_event_rsvp_status')
						->where('event_id_fk', $event_id)
						->where('cost_id_fk', $cost_id)
						->where('user_id_fk', $user_id)
						->update($arr_to_update);
					echo json_encode(array("error" => "0", "error_message" => "Status has been changed successfully"));
				} else {
					DB::table('trans_event_rsvp_status')->insert([
						['event_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
					]);

					echo json_encode(array('success' => 1));
				}
			} else {
				DB::table('trans_event_rsvp_status')
					->where('event_id_fk', $event_id)
					->where('cost_id_fk', $cost_id_fk)
					->where('user_id_fk', $user_id)
					->delete();
				$add_new_cost = DB::table('trans_event_rsvp_status')->insert([
					['event_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
				]);
				echo json_encode(array('success' => 1));
			}
		} else {
			DB::table('trans_event_rsvp_status')->insert([
				['event_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")],
			]);
			}
        */

	}



	public function uploadContentImage() {
		$request = Input::all();
		if (Input::hasFile('content_img')) {
			$random_no = rand(1000, 9999);
			$destinationPath = public_path() . "/assets/Frontend/images/content_images";
			$obj = new Filesystem();
			if (!$obj->exists($destinationPath)) {
				$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
			}
			try {
				$extension = Input::file('content_img')->getClientOriginalExtension(); // getting image extension
				//                $fileName = Input::file('content_img')->getClientOriginalName(); // renameing image
				$fileName = $random_no . '.' . $extension; // renameing image

				$image = \Image::make(\Input::file('content_img'));

				// save original
				$image->save($destinationPath . '/' . $fileName);
				//resize
				if ($request['image_height'] != '' && $request['image_width'] != '') {
					$image->resize($request['image_width'], $request['image_height']);
				}
				$destinationPath = public_path() . "/assets/Frontend/images/content_images/";
				$obj = new Filesystem();
				if (!$obj->exists($destinationPath)) {
					$result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
				}
				$image->save($destinationPath . '/' . $fileName);
				return array('file_path' => url('/') . '/public/assets/Frontend/images/content_images/' . $fileName, 'file_name' => $fileName);
			} catch (Exception $e) {
				dd($e->getMessage());
			}
		} else {
			return 'false';
		}
	}

	function getUserGoingEventCount() {
		$month = Input::get('month');
		$today_date = date("Y-$month-01");
		$end_date = date("Y-$month-31");
		/* going count of meetup and event of user */
		$data['user_session'] = Session::get('user_account');
		$user_id = $data['user_session']['id'];
		$arr_on_going_event = array();
		$get_on_going_event = DB::table('trans_event_rsvp_status as r')
			->join('mst_event as e', 'e.id', '=', 'r.event_id_fk')
			->where('r.user_id_fk', $user_id)
			->where('r.status', '1')
			->whereBetween('e.start_date', array($today_date, $end_date))
			->select('e.start_date', 'e.id', 'e.title as name', 'e.profile_image', DB::raw('DAY(start_date) as day'), DB::raw('MONTH(start_date) as month'), DB::raw('YEAR(start_date) as year'), 'e.location as location', 'e.description as description')
			->get();
		$upcoming_going_event_count = count($get_on_going_event);
		echo $upcoming_going_event_count;
		/* ends here */
	}

	function chkEventTitle() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}
		$data['user_session'] = Session::get('user_account');
		$request = Input::all();
		$article_title = $request['title'];
		$array_article = DB::table('mst_event as ma')
			->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
			->where('ma.title', $article_title)
			->select('ma.*', 'u.user_name')
			->get();
		if (count($array_article) > 0) {
			echo 'false';
			die;
		} else {
			echo 'true';
			die;
		}
	}

	function chkEventTitleE() {
		$commonModel = new CommonModel();
		$data = $commonModel->commonFunction();
		$login = GlobalData::isLoggedIn();
		if ($login) {
			return redirect(url('/'));
		}
		$data['user_session'] = Session::get('user_account');
		$request = Input::all();
		$title = $request['title'];
		$old_title = $request['old_title'];
		$array_education = DB::table('mst_event as ma')
			->join('mst_users as u', 'ma.user_id_fk', '=', 'u.id', 'left')
			->where('ma.title', $title)
			->select('ma.*', 'u.user_name')
			->get();
		if (count($array_education) > 0) {
			if ($old_title == $title) {
				echo 'true';
				die;
			} else {
				echo 'false';
				die;
			}
		} else {
			echo 'true';
			die;
		}
	}

}
