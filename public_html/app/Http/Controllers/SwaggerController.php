<?php

namespace App\Http\Controllers;

use File;
use Response;
use Illuminate\Http\Request;

class SwaggerController extends Controller
{
        /**
     * Dump api-docs.json content endpoint.
     *
     * @param string $jsonFile
     *
     * @return \Response
     */
    public function docs($jsonFile = null)
    {
        // return "g";
        $filePath = config('l5-swagger.paths.docs').'/'.
            (! is_null($jsonFile) ? $jsonFile : config('l5-swagger.paths.docs_json', 'api-docs.json'));

        if (! File::exists($filePath)) {
            abort(404, 'Cannot find '.$filePath);
        }

        $content = File::get($filePath);

        return Response::make($content, 200, [
            'Content-Type' => 'application/json',
        ]);
    }
}
