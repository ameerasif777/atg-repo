<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
/*
  |---------------------------------------------------------------------------------------->
  |                              [Start] : Backend-End Rountes.
  |---------------------------------------------------------------------------------------->
 */
/* [Start] : Managet-education-gallery'ge Admin Login. */

$username = Request::path();
$userCount = \App\User::where('user_name', $username)->count();
if ($userCount > 0) {
    Route::get('/{username}', 'UserAccountController@SingleUserprofile');
} else {
    Route::get('auth/admin/login', 'Auth\AuthController@getAdminLogin');
    Route::post('auth/admin/login', 'Auth\AuthController@postAdminLogin');
    Route::get('auth/admin/logout', 'Auth\AuthController@getAdminLogout');
    Route::get('admin/login', 'Auth\AuthController@getAdminLogin');

    Route::get('admin/home', 'AdminController@home');
    Route::get('admin/dashboard', 'AdminController@home');
    Route::get('admin/log-out', 'AdminController@adminLogout');
    Route::get('admin/forgot-password', 'Auth\AuthController@forgotPassword');
    Route::post('admin/forgot-password', 'Auth\AuthController@forgotPasswordAction');
    Route::get('admin/forgot-password-email', 'Auth\AuthController@checkForgotPasswordEmail');
    Route::get('admin/reset-password/{edit_id}', 'Auth\AuthController@resetPassword');
    Route::post('admin/reset-password', 'Auth\AuthController@resetPasswordAction');
    Route::post('admin/admin/account-activate/{activation_code}', 'Auth\AuthController@activateAccount');
    Route::get('admin/admin/account-activate/{activation_code}', 'Auth\AuthController@activateAccount');
    Route::post('user/account-activate/{activation_code}', 'Auth\AuthController@activateUserAccount');
    Route::get('user/account-activate/{activation_code}', 'Auth\AuthController@activateUserAccount');
    Route::get('admin/admin/institute-account-activate/{activation_code}', 'Auth\AuthController@activateInstituteAccount');
    Route::post('admin/admin/institute-account-activate/{activation_code}', 'Auth\AuthController@activateInstituteAccount');
    Route::get('admin/user/change-status-email', 'AdminController@changeStatusEmail');
    Route::get('admin/user/change-status', 'AdminController@changeStatus');
    Route::get('admin/pushnotify', 'AdminController@webPushNotification');
    /* [Start] :: Manage Global Settings. */
    Route::get('admin/global-settings/list', 'GlobalSettingController@listGlobalSettings');
    Route::get('admin/global-settings/edit/{edit_id}', 'GlobalSettingController@editGlobalSettings');
    Route::post('admin/global-settings/edit', 'GlobalSettingController@postGlobalSettings');
    Route::post('admin/global-settings/edit/{edit_id}/{lang_id}', 'GlobalSettingController@editGlobalSettings');
    Route::get('admin/global-settings/edit-parameter-language/{edit_id}', 'GlobalSettingController@editParameterLanguage');
    Route::post('admin/global-settings/edit-parameter-language/{edit_id}', 'GlobalSettingController@editParameterLanguage');
    Route::post('admin/global-settings/get-global-parameter-language', 'GlobalSettingController@getGlobalParameterLanguage');

    /* [Start] :: Manage Roles: */
    Route::get('admin/role/list', 'RoleController@listRole');
    Route::post('admin/role/list', 'RoleController@listRole');
    Route::get('admin/role/edit/{edit_id}', 'RoleController@editRole');
    Route::post('admin/role/edit', 'RoleController@updateRole');
    Route::get('admin/role/add', 'RoleController@addRole');
    Route::post('admin/role/add', 'RoleController@addRolePost');
    Route::post('admin/role/delete', 'RoleController@deleteRole');
    Route::get('admin/role/check-role', 'RoleController@checkRole');


    /* [Start] :: Manage Backend Events: */
    Route::get('admin/events', 'EventController@listEvent');
    Route::get('admin/events/add', 'EventController@addEvent');
    Route::post('admin/events/addRecord', 'EventController@addEventRecord');
    Route::post('admin/event/delete', 'EventController@deleteEvents');

    Route::any('edit-event/{edit_id}', 'EventController@editPostEvent');
    Route::any('admin/event/view-event/{edit_id}', 'EventController@viewEditPostEvent');
    Route::any('admin/event/comment/{event_id}', 'EventController@listComment');
    Route::any('admin/event/editcomment/{comment_id}', 'EventController@editComment');
    Route::post('admin/event/deletecomment', 'EventController@deleteEventComments');

    /* [Start] :: Manage Backend Meetups: */
    Route::get('admin/meetups', 'MeetupController@listMeetup');
    Route::get('admin/meetups/add', 'MeetupController@addMeetup');
    Route::post('admin/meetups/addRecord', 'MeetupController@addMeetupRecord');
    Route::post('admin/meetups/delete', 'MeetupController@deleteMeetups');
    Route::any('edit-meetups/{edit_id}', 'MeetupController@editPostMeetup');
    //Route::any('view-meetup/{edit_id}', 'MeetupController@viewPostMeetup');
    Route::any('admin/meetup/comment/{meetup_id}', 'MeetupController@listComment');
    Route::any('admin/meetup/editcomment/{comment_id}', 'MeetupController@editComment');
    Route::post('admin/meetup/deletecomment', 'MeetupController@deleteMeetupComments');

    /* [Start] :: Manage Frontend Events: */
    Route::any('event', 'EventController@postEvent');
    Route::any('event/{group_id}', 'EventController@postEvent');
    Route::any('post-event', 'EventController@addPostEvent');
    Route::any('list-event', 'EventController@listPostEvent');
    Route::any('get-event-gallery', 'EventController@getEventList');
    Route::any('list-my-event', 'EventController@listUserEvent');
    Route::any('get-user-event', 'EventController@getUserEvent');
    Route::any('front-edit-event/{edit_id}', 'EventController@frontEditPostEvent')->name('editEventFront');
    Route::any('save-list-event', 'EventController@saveListEvent');
    Route::any('get-save-list-event', 'EventController@getSaveEvent');
    Route::any('comment-event', 'EventController@commentEvent');
    Route::any('save-post-event', 'EventController@savePostEvent');
    Route::any('view-event/{view_id}/{response?}', 'EventController@viewEvent');
    Route::get('event-detail/{post_id}', function($post_id){
       return Redirect("/view-event/$post_id", 301);
    })->where('restOfURL', '.*');

    Route::any('save-edit-event/{edit_id}', 'EventController@frontEditPostEvent');
    Route::post('up-down-vote-event', 'EventController@upDownVoteEvent');
    Route::any('comment-event', 'EventController@commentEvent');
    Route::any('post-cost-event', 'EventController@ajaxCostEvent');
    Route::any('search-event-ajax', 'EventController@searchEventAjax');

    Route::any('event-cost-rsvp', 'EventController@costRsvp');
    Route::any('meetup-cost-rsvp', 'MeetupController@costRsvp');
    Route::any('education-cost-rsvp', 'EducationController@costRsvp');


    /* Manage Ethnicity type */
    Route::get('admin/bio/list/ethnicity', 'AdminController@listEthnicityType');
    Route::post('admin/ethnicity/delete', 'AdminController@deleteEthnicityType');
    Route::get('admin/ethnicity/edit/{edit_id}', 'AdminController@editEthnicity');
    Route::post('admin/ethnicity/edit/{edit_id}', 'AdminController@updateEthnicity');
    Route::get('admin/ethnicity/view/{edit_id}', 'AdminController@viewEthnicity');
    Route::get('admin/ethnicity/change-status', 'AdminController@changeEthnicityStatus');
    Route::get('admin/ethnicity/add-ethnicity', 'AdminController@addEthnicity');
    Route::post('admin/ethnicity/add', 'AdminController@addEthnicityPost');
    /* Manage Body type */
    Route::get('admin/bio/list/body', 'AdminController@listBodyType');
    Route::post('admin/body/delete', 'AdminController@deleteBodyType');
    Route::get('admin/body/edit/{edit_id}', 'AdminController@editBody');
    Route::post('admin/body/edit/{edit_id}', 'AdminController@updateBody');
    Route::get('admin/body/view/{edit_id}', 'AdminController@viewBody');
    Route::get('admin/body/change-status', 'AdminController@changeBodyStatus');
    Route::get('admin/body/add-body', 'AdminController@addBody');
    Route::post('admin/body/add', 'AdminController@addBodyPost');

    /* Manage Eye color */
    Route::get('admin/bio/list/eye', 'AdminController@listEyeColor');
    Route::post('admin/eye/delete', 'AdminController@deleteEyeColor');
    Route::get('admin/eye/edit/{edit_id}', 'AdminController@editEye');
    Route::post('admin/eye/edit/{edit_id}', 'AdminController@updateEye');
    Route::get('admin/eye/view/{edit_id}', 'AdminController@viewEye');
    Route::get('admin/eye/change-status', 'AdminController@changeEyeStatus');
    Route::get('admin/eye/add-eye', 'AdminController@addEye');
    Route::post('admin/eye/add', 'AdminController@addEyePost');

    /* Manage Hair color */
    Route::get('admin/bio/list/hair', 'AdminController@listHairColor');
    Route::post('admin/hair/delete', 'AdminController@deleteHairColor');
    Route::get('admin/hair/edit/{edit_id}', 'AdminController@editHair');
    Route::post('admin/hair/edit/{edit_id}', 'AdminController@updateHair');
    Route::get('admin/hair/view/{edit_id}', 'AdminController@viewHair');
    Route::get('admin/hair/change-status', 'AdminController@changeHairStatus');
    Route::get('admin/hair/add-hair', 'AdminController@addHair');
    Route::post('admin/hair/add', 'AdminController@addHairPost');

    /* Manage Student user */
    Route::get('admin/users/list/student', 'AdminController@listStudentUser');
    Route::post('admin/student/delete', 'AdminController@deleteStudentUser');
    Route::get('admin/student/edit/{edit_id}', 'AdminController@editStudent');
    Route::post('admin/student/edit/{edit_id}', 'AdminController@updateStudent');
    Route::get('admin/student/view/{edit_id}', 'AdminController@viewStudent');

    /* Manage Professional user */
    Route::get('admin/users/list/professional', 'AdminController@listProfessionalUser');
    Route::post('admin/professional/edit/{edit_id}', 'AdminController@updateProfessional');
    Route::post('admin/professional/delete', 'AdminController@deleteProfessionalUser');
    Route::get('admin/professional/edit/{edit_id}', 'AdminController@editProfessional');
    Route::get('admin/professional/view/{edit_id}', 'AdminController@viewProfessional');



    /* Manage Company user */
    Route::get('admin/users/list/company', 'AdminController@listCompanyUser');
    Route::post('admin/company/edit/{edit_id}', 'AdminController@updateCompany');
    Route::post('admin/company/delete', 'AdminController@deleteCompanyUser');
    Route::get('admin/company/edit/{edit_id}', 'AdminController@editCompany');
    Route::get('admin/company/view/{edit_id}', 'AdminController@viewCompany');

    Route::get('admin/users/list/download', 'AdminController@downloadAllUser');


    /* [Start] :: Manage Admin Users. */
    Route::get('admin/admin/profile', 'AdminController@adminProfile');
    Route::get('admin/admin/edit-profile', 'AdminController@editProfile');
    Route::post('admin/admin/edit-profile', 'AdminController@updateProfile');
    Route::get('admin/admin/list', 'AdminController@listAdmin');
    Route::post('admin/admin/list', 'AdminController@deleteUser');
    Route::get('admin/admin/edit/{edit_id}', 'AdminController@editAdmin');
    Route::post('admin/admin/edit/{edit_id}', 'AdminController@updateAdmin');
    Route::get('admin/admin/change-status', 'AdminController@changeStatus');
    Route::get('admin/admin/add', 'AdminController@addAdmin');
    Route::post('admin/admin/add', 'AdminController@addAdminAction');
    Route::get('admin/admin/check-admin-username', 'AdminController@checkAdminUsername');
    Route::get('admin/admin/check-admin-email', 'AdminController@checkAdminEmail');

    /* [Start] :: Manage Contact Us. */
    Route::get('admin/contact-us/list', 'ContactUsController@listContactUs');
    Route::post('admin/contact-us/list', 'ContactUsController@deleteContactUs');
    Route::get('admin/contact-us/view/{edit_id}', 'ContactUsController@viewContact');
    Route::post('admin/contact-us/view/{edit_id}', 'ContactUsController@viewContact');
    Route::get('admin/contact-us/reply/{edit_id}', 'ContactUsController@replyContact');
    Route::post('admin/contact-us/reply/{edit_id}', 'ContactUsController@replyAction');
    /* [Start] :: Manage Email Templates. */
    Route::get('admin/email-template/list', 'EmailTemplateController@listEmail');
    Route::get('admin/edit-email-template/{email_template_id}', 'EmailTemplateController@editEmailTemplate');
    Route::post('admin/edit-email-template/{email_template_id}', 'EmailTemplateController@updateEmailTemplate');
    
    /* [Start] :: Send Emails. */
    Route::get('admin/send-emails/list', 'AdminController@listSendEmail');
    Route::any('admin/send-emails/edit-email', 'AdminController@editEmail');
    Route::any('admin/send-emails/send', 'AdminController@sendEmail');
    Route::post('admin/send-emails/send-async', 'AdminController@sendEmailAsync');

    /* [Start] :: Manage Group. */
    Route::get('admin/group/list', 'GroupController@listGroup');
    Route::post('admin/group/list', 'GroupController@deleteGroup');
    Route::get('admin/group/edit-group/{group_id}', 'GroupController@editGroup');
    Route::post('admin/group/edit-group/{group_id}', 'GroupController@updateGroup');
    Route::get('admin/group/add-group', 'GroupController@addGroup');
    Route::get('admin/group/get-chlid-group', 'GroupController@getChildGroup');
    Route::post('admin/group/add', 'GroupController@addGroupPost');
    Route::get('admin/group/check-group-groupname', 'GroupController@checkGroupName');
    Route::any('join-permanantly/{group_id}', 'GroupController@joinPermanat');
    Route::any('suggest-new-group/{group_id}', 'GroupController@suggestNewGroup');
    Route::get('admin/group/download-csv/{group_id}/{type}', 'GroupController@downloadGroupUsers');
    Route::get('admin/suggested-groups/list', 'GroupController@listSuggestedGroups');
    Route::get('admin/user/group/edit/{user_id}', 'GroupController@editUserGroups');
    Route::post('admin/user/group/edit/{user_id}', 'GroupController@editUserGroupsPost');

    /* admin : Manage Analytics */
    Route::get('admin/analytics/growth', 'AnalyticsController@growthAnalytics');
    Route::get('admin/analytics/access_location', 'AnalyticsController@accessLocationAnalytics')->name('access-location-analytics');
    Route::post('admin/analytics/location', 'AnalyticsController@LocationAnalytics')->name('location-analytics');


    /* admin : Manage Crawler */
    Route::any('admin/scrawler', 'CrawlerController@CrawlURLs');
    Route::get('admin/website_crawl/list', 'CrawlerController@WebsiteList')->name('website_crawl_list');
    Route::any('admin/website_crawl/review', 'CrawlerController@listImportedURL')->name('crawled_posts_review');
    Route::any('admin/scrawler/scrape', 'CrawlerController@APIorScrape')->name('scrape_urls');
    Route::get('admin/website_crawl/add_url', 'CrawlerController@addCrawlUrl')->name('add_url');
    Route::post('admin/website_crawl/post_add_url', 'CrawlerController@postAddCrawlUrl');
    Route::any('admin/website_crawl/call_test_url', 'CrawlerController@callTestUrl');
    Route::post('admin/website_crawl/save_new_website', 'CrawlerController@saveNewWebsite');
    Route::get('admin/website_crawl/get_website_det', 'CrawlerController@getWebsiteDet');
    Route::get('admin/website_crawl/crawlsitemap', 'CrawlerController@crawlSitemap');
    Route::get('admin/website_crawl/parseWebsite', 'CrawlerController@parseWebsite');
    Route::get('admin/website_crawl/autoapprove', 'CrawlerController@automateApproval');
    Route::get('admin/website_crawl/autocrawl', 'CrawlerController@automateCrawling');
    Route::get('admin/searchgroup/{text}/{ref_id}', 'CrawlerController@searchGroupAjax');

    /* [Start] :: Manage CMS . */
    Route::get('admin/cms', 'CmsController@listCMS');
    Route::get('admin/cms/edit-cms/{edit_id}', 'CmsController@editCMS');
    Route::post('admin/cms/edit-cms', 'CmsController@editCMSPost');

    Route::get('cms/{cms_id}', 'CmsController@cmsDetails');

    /* [Start] :: Manage Editor Upload Images . */
    Route::get('admin/editor-image-upload', 'CmsController@editorImageUpload');
    Route::post('admin/editor-image-upload', ['as' => 'admin.editor-image-upload', 'uses' => 'CmsController@editorImageUpload']);

    /* [Start] :: Manage Authentication. */
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postUserLogin');
    Route::post('auth/user-login', 'Auth\AuthController@postUserLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');
    Route::get('get-parent-group', 'Auth\AuthController@getParentGroup');
    Route::get('get-parent-group-details','Auth\AuthController@getParentGroupDetail');
    Route::get('get-child-group', 'Auth\AuthController@getChildGroup');
    Route::get('get-group-name', 'Auth\AuthController@getGroupName');
    Route::get('get-group', 'Auth\AuthController@getGroup');
    Route::get('get-group-detail', 'GroupController@group_datail');
    Route::get('get-group-detail/{group_id}', 'GroupController@group_datail');
    Route::get('get-parent-group1', 'Auth\AuthController@getParentGroup1');

    Route::get('get-group-structure/{group_id}', function($group_id){
       return Redirect("/go/$group_id", 301);
    })->where('restOfURL', '.*');

    Route::get('get-parent-group-inner', 'Auth\AuthController@getParentGroupInner');
    Route::any('get-group-event', 'GroupController@getAllEventoOfController');
    Route::any('get-group-event/{group_id}', 'GroupController@getAllEventoOfController');
    Route::any('go/{group_id}/events', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/articles', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/qrious', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/questions', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/meetups', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/jobs', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/education', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}/connect', 'GroupController@getGroupStructureTabs');
    Route::any('go/{group_id}', 'GroupController@getGroupStructure');
    Route::any('go/{group_id}/{parent_id}', 'GroupController@getGroupStructure');

    //Route::any('get-group-wise-data/{group_id}/{parent_id}/{get_id}', 'GroupController@getGroupStructure');

    Route::any('get-group-wise-data/{group_id}/{get_id}', 'GroupController@getGroupStructure');
    Route::any('update-group-tag-line', 'GroupController@updateGroupTagLine');
    Route::any('list-quious/{group_id}', 'GroupController@listQuious');
    Route::any('list-article/{group_id}', 'GroupController@listArticle');
    Route::any('list-event/{group_id}', 'GroupController@listEvent');
    Route::any('list-meetup/{group_id}', 'GroupController@listMeetup');
    Route::any('job-list/{group_id}', 'GroupController@listJob');
    Route::any('education-list/{group_id}', 'GroupController@listEducation');
    Route::any('join-group/{group_id}/{status}', 'GroupController@joinGroup');
    Route::any('leave-group/{group_id}/{status}', 'GroupController@leaveGroup');

    /*
      |@---------------------------------------------------------------------------------------->
      |                              [Start] : Front-End Rountes.
      |@---------------------------------------------------------------------------------------->
     */

    Route::get('/', 'FrontHomeController@index');
    Route::get('signup1', 'Auth\AuthController@getRegister');
    Route::post('signup1', 'Auth\AuthController@postRegister1');
    /*     * *again mail send** */
    Route::get('again-send-mail', 'UserAccountController@againSendMail');
    Route::post('again-send-mail', 'UserAccountController@againSendMail');
    /*     * *************** */
    Route::get('signup2', 'Auth\AuthController@getRegister2');
    Route::post('signup2', 'Auth\AuthController@postRegister2');
    Route::get('signup3', 'Auth\AuthController@getRegister3');
    Route::post('signup3', 'Auth\AuthController@postRegister3');
    Route::get('signup4', 'Auth\AuthController@getRegister4');
    Route::get('invite-frineds', 'Auth\AuthController@getRegister4');
    Route::get('add-follower', 'Auth\AuthController@addFollower');
    /*     * *for unfollower*** */
    Route::get('add-unfollower', 'Auth\AuthController@addUnFollower');
    /*     * ***** */
    Route::get('add-connection', 'Auth\AuthController@addConnection');
    Route::get('make-connection', 'GroupController@getAllInfo');
    Route::get('confirm', 'UserAccountController@confirmRequest');
    Route::get('block-user', 'UserAccountController@blockUser');
    Route::get('user-report/{user_id}', 'UserAccountController@userReport');
    Route::any('viewer', 'UserAccountController@viewer');
    Route::get('deleted-connection', 'UserAccountController@deletedConnection');
    Route::get('auth/register/check-user-email', 'Auth\AuthController@checkUserEmail');
    Route::get('check-email-exist', 'Auth\AuthController@checkEmailExist');
    Route::post('check-email-exist', 'Auth\AuthController@checkEmailExist');
    Route::post('check-password', 'Auth\AuthController@checkPassword');
    /* User Dashboard & Profile Details */
    Route::get('user-dashboard', 'UserAccountController@userDashboard');
    Route::get('trending', 'UserAccountController@userTrendingPost');
    Route::get('profile', 'UserAccountController@profile');
    Route::get('getfullimagepath/{user_id}', 'UserAccountController@fullimgpath');
    Route::get('user-activity-load', 'UserAccountController@userActivityLoad');
    Route::get('user-profile/{user_id}', 'UserAccountController@SingleUserprofile');
    Route::get('remove-social-connection/{social_network}', 'UserAccountController@removeSocialConnection');
    Route::get('signin', 'Auth\AuthController@signin');
    Route::post('signin', 'Auth\AuthController@postUserLogin');
    Route::post('cover-photo', 'UserAccountController@uploadCoverPhoto');
    Route::post('resetPassword', 'ContactUsController@forgotPasswordAction');
    Route::get('resetPassword', 'ContactUsController@forgotPasswordAction');
    Route::get('front/reset-password/{code}', 'ContactUsController@resetPassword');
    Route::post('front/reset-password', 'ContactUsController@resetPasswordAction');
    Route::get('invite-friends', 'UserAccountController@getInviteFriends');
    Route::post('upload-image', 'UserAccountController@uploadImage');

    /* Frontend Contact us */
    Route::get('send-feedback', 'ContactUsController@getContactUs');
    Route::post('contact-us/send-contact', 'ContactUsController@index');
    Route::get('logout', 'Auth\AuthController@getLogout');
    Route::get('setlogouttime', 'Auth\AuthController@setLogoutTime');
    Route::post('upload-cover-photo', 'UserAccountController@uploadCoverPhoto');
    Route::post('upload-profile-picture', 'UserAccountController@uploadProfilePicture');
    Route::post('upload-group-icon', 'UserAccountController@uploadGroupIcon');
    Route::post('add_new_username', 'UserAccountController@addNewUsername');

    /*
     * Social Media Routes Start Here
     */
    Route::get('linkedin', 'SocialMediaController@redirectToProvider');
    Route::get('linkedin/{type}', 'SocialMediaController@redirectToProvider');
    Route::get('linkedin_callback', 'SocialMediaController@handleProviderCallback');
    Route::post('linkedin_callback', 'SocialMediaController@handleProviderCallback');

    Route::get('facebook', 'SocialMediaController@redirectToFacebookProvider');
    Route::get('facebook/{type}', 'SocialMediaController@redirectToFacebookProvider');
    Route::get('facebook_callback', 'SocialMediaController@handleFacebookCallback');
    Route::post('facebook_callback', 'SocialMediaController@handleFacebookCallback');

    Route::get('gmail', 'SocialMediaController@redirectToGmailProvider');
    Route::get('gmail/{type}', 'SocialMediaController@redirectToGmailProvider');
    Route::get('google-callback', 'SocialMediaController@handleGmailCallback');
    Route::post('google-callback', 'SocialMediaController@handleGmailCallback');

    Route::get('twitter', 'SocialMediaController@redirectToTwitterProvider');
    Route::get('twitter/{type}', 'SocialMediaController@redirectToTwitterProvider');
    Route::get('twitter-callback', 'SocialMediaController@handleTwitterCallback');
    Route::post('twitter-callback', 'SocialMediaController@handleTwitterCallback');

    Route::get('facebook/share', 'SocialMediaController@shareReferralLinkWithFacebook');
    Route::get('google/share', 'SocialMediaController@shareReferralLinkWithGooglePlus');
    Route::get('twitter/share', 'SocialMediaController@shareReferralLinkWithTwitter');
    Route::get('linkedin/share', 'SocialMediaController@shareReferralLinkWithLinkedIn');

    Route::get('twt/{job_id}', 'SocialMediaController@shareJobWithTwitterProvider');
    Route::get('gplus/{job_id}', 'SocialMediaController@shareJobWithGooglePlusProvider');
    Route::get('lin/{job_id}', 'SocialMediaController@shareJobWithLinkedInProvider');
    Route::get('fb/{job_id}', 'SocialMediaController@shareJobWithFacebookProvider');

    Route::get('meetup/twt/{job_id}', 'SocialMediaController@shareMeetupWithTwitterProvider');
    Route::get('meetup/gplus/{job_id}', 'SocialMediaController@shareMeetupWithGooglePlusProvider');
    Route::get('meetup/lin/{job_id}', 'SocialMediaController@shareMeetupWithLinkedInProvider');
    Route::get('meetup/fb/{job_id}', 'SocialMediaController@shareMeetupWithFacebookProvider');

    Route::get('blog/twt/{blog_id}', 'SocialMediaController@shareBlogWithTwitterProvider');
    Route::get('blog/gplus/{blog_id}', 'SocialMediaController@shareBlogWithGooglePlusProvider');
    Route::get('blog/lin/{blog_id}', 'SocialMediaController@shareBlogWithLinkedInProvider');
    Route::get('blog/fb/{blog_id}', 'SocialMediaController@shareBlogWithFacebookProvider');


    Route::get('article/twt/{article_id}', 'SocialMediaController@shareArticleWithTwitterProvider');
    Route::get('article/gplus/{article_id}', 'SocialMediaController@shareArticleWithGooglePlusProvider');
    Route::get('article/lin/{article_id}', 'SocialMediaController@shareArticleWithLinkedInProvider');
    Route::get('article/fb/{article_id}', 'SocialMediaController@shareArticleWithFacebookProvider');

    Route::get('question/twt/{question_id}', 'SocialMediaController@shareQriousWithTwitterProvider');
    Route::get('question/gplus/{question_id}', 'SocialMediaController@shareQriousWithGooglePlusProvider');
    Route::get('question/lin/{question_id}', 'SocialMediaController@shareQriousWithLinkedInProvider');
    Route::get('question/fb/{question_id}', 'SocialMediaController@shareQriousWithFacebookProvider');

    Route::get('education/twt/{education_id}', 'SocialMediaController@shareEducationWithTwitterProvider');
    Route::get('education/gplus/{education_id}', 'SocialMediaController@shareEducationWithGooglePlusProvider');
    Route::get('education/lin/{education_id}', 'SocialMediaController@shareEducationWithLinkedInProvider');
    Route::get('education/fb/{education_id}', 'SocialMediaController@shareEducationWithFacebookProvider');

    Route::get('event/twt/{job_id}', 'SocialMediaController@shareEventWithTwitterProvider');
    Route::get('event/gplus/{job_id}', 'SocialMediaController@shareEventWithGooglePlusProvider');
    Route::get('event/lin/{job_id}', 'SocialMediaController@shareEventWithLinkedInProvider');
    Route::get('event/fb/{job_id}', 'SocialMediaController@shareEventWithFacebookProvider');

    /*
     * Social Media Routes End Here
     */
    Route::post('account-setting', 'UserAccountController@accountSetting');
    Route::get('edit-user-bio', 'UserAccountController@editUserBio');
    Route::get('profile-setting', 'UserAccountController@getProfileSetting');
    Route::get('bio-setting', 'UserAccountController@getBio');
    Route::post('edit-notification-setting', 'UserAccountController@editNotificationSetting');
    Route::get('user-bio', 'UserAccountController@userBio');
    Route::post('edit-bio', 'UserAccountController@editBio');
    Route::post('edit-password-setting', 'UserAccountController@editPasswordSetting');
    Route::get('pwd-setting', 'UserAccountController@getPasswordSetting');
    Route::get('trans-history', 'PaymentController@getTransHistory');
    Route::get('trans-history-ajax/{page}/{limit}', 'PaymentController@getTransHistoryAjax');
    Route::get('trans-history/dwld-invoice/{id}', 'PaymentController@downloadTransactionInvoice');
    Route::get('messages', 'UserAccountController@getMessages');
    Route::get('my-blogs', 'UserAccountController@getMyBlogs');
    Route::get('saved-events', 'UserAccountController@getSavedEvents');
    Route::get('saved-meetups', 'UserAccountController@getSavedMeetups');
    Route::get('saved-job', 'UserAccountController@getCompanySavedJobs');
    Route::get('saved-jobs', 'UserAccountController@getStudentSavedJobs');
    Route::get('notification-setting', 'UserAccountController@getNotificationSetting');


    Route::get('check-user-name', 'Auth\AuthController@checkUsernameExist');
    Route::get('check-user-email', 'Auth\AuthController@checkUseremailExist');
    Route::get('my-group', 'GroupController@myGroup');
    Route::post('explore-group', 'GroupController@postExploreGroup');
    Route::Post('get-parent-group-edit', 'Auth\AuthController@getParentGroupForUpdate');


    /* [Start] :: Manage Job. */
    Route::any('jobs-applied', 'JobController@appliedJob');
    Route::any('post-job', 'JobController@postJob');
    Route::any('post-job/{group_id}', 'JobController@postJob');

//Route::get('post-job', 'JobController@postJob');
    Route::get('job/edit/{edit_id}', 'JobController@editUserJob');
    Route::any('job/post/{edit_id}', 'JobController@postUserJob');
    Route::get('view-job-applicants/{job_id}', 'JobController@viewApplicants')->name('view-job-applicants');
    Route::get('job/view/resume/{job_id}/{user_id}', 'JobController@viewResume');
    Route::post('job/edit/{edit_id}', 'JobController@editUserJob');
    Route::get('edit-job/{edit_id}', 'JobController@getEditJob')->name('editJobFront');
    Route::get('view-job/{job_id}/{response?}', 'JobController@ViewJob')->name('view-job');
    Route::get('view/{job_id}', function($job_id){
       return Redirect("/view-job/$job_id", 301);
    })->where('restOfURL', '.*');

    Route::any('job-list', 'JobController@listUserJob');
    Route::get('posted-job-list', 'JobController@listPostedJob');
    Route::any('job/delete/{job_id}', 'JobController@deleteUserJob');
    Route::post('applicant/delete', 'JobController@deleteApplicant');
    Route::get('apply-job-status/{job_id}', 'JobController@jobApplyStatus')->name('apply-job-status');
    Route::post('apply-job', 'JobController@applyJob');
    Route::post('create-job-alert', 'JobController@jobAlert');
    Route::post('upload-resumes', 'JobController@manageResumes');
    Route::get('delete-resume/{resume_id}', 'JobController@deleteResume');
    Route::get('delete-saved-job/{job_id}', 'JobController@deleteSavedJob');
    Route::get('resume-count', 'JobController@resumeCount');
    Route::get('view-resume/{resume_name}', 'JobController@view');
    Route::get('like-job/{job_id}/{status}', 'JobController@likeJob');
    Route::get('make-follower', 'JobController@addFollower');
    Route::get('remove-follower', 'JobController@addFollower');
    Route::get('save-user-job/{job_id}/{status}', 'JobController@saveUserJob');
    Route::get('job-alert', 'JobController@alert');
    Route::get('manage-resumes', 'JobController@getManageResumes');
    Route::get('manage-job-alert', 'JobController@getManageJobAlert');
    Route::any('save-list-job', 'JobController@saveListJob');
    Route::any('list-job', 'JobController@listFrontJob');


    /* backend job routes */
    Route::get('admin/job-list', 'JobController@listJob');
    Route::any('admin/job/edit/{edit_id}', 'JobController@editJob');
    Route::any('admin/job/edit/{edit_id}', 'JobController@editJob');
    Route::any('admin/job/view/{job_id}', 'JobController@viewJobDetails');
    Route::get('admin/job/change-status', 'JobController@changeStatus');
    Route::post('admin/job/delete', 'JobController@deleteJob');


    /* backend team routes */
    Route::get('admin/team-list', 'TeamController@listTeam');
    Route::any('admin/team/edit/{edit_id}', 'TeamController@editBackendTeam');
//Route::any('admin/team/edit/{edit_id}', 'TeamController@editBackendTeam');

    Route::any('admin/team/view/{team_id}', 'TeamController@viewTeamDetails');
    Route::get('admin/team/change-status', 'TeamController@changeStatus');
    Route::post('admin/team/delete', 'TeamController@deleteBackendTeam');
    /* [Start] :: Manage Backend Meetup */

    Route::get('admin/meetup/meetup-list', 'MeetupController@listmeetup');
    Route::post('admin/meetup/delete', 'MeetupController@deletemeetup');
    Route::get('admin/meetup/edit/{edit_id}', 'MeetupController@editmeetup');
    Route::post('admin/meetup/edit/{edit_id}', 'MeetupController@editmeetup');
    Route::any('admin/meetup/view/{view_id}', 'MeetupController@viewMeetup');

    /* [Start] :: Manage Frontend Meetups: */
    Route::get('meetup', 'MeetupController@postMeetup');
    Route::any('meetup/{group_id}', 'MeetupController@postMeetup');
    Route::any('post-meetup', 'MeetupController@addPostMeetup');
    Route::any('list-meetup', 'MeetupController@listFrontMeetup');
    Route::any('get-meetup-gallery', 'MeetupController@getListMeetup');
    Route::any('list-my-meetup', 'MeetupController@listUserMeetup');
    Route::any('get-my-meetup', 'MeetupController@getUserMeetup');
    Route::any('save-post-meetup', 'MeetupController@savePostMeetup');
    Route::any('save-list-meetup', 'MeetupController@saveListMeetup');
    Route::any('get-save-meetup-list', 'MeetupController@getSaveMeetup');

    Route::any('search-meetup-ajax', 'MeetupController@searchMeetupAjax');
    Route::any('comment-meetup', 'MeetupController@commentMeetup');
    Route::any('view-meetup/{edit_id}/{response?}', 'MeetupController@viewFrontMeetup');
    Route::any('edit-save-meetup/{edit_id}', 'MeetupController@frontEditPostMeetup');
    Route::any('up-down-vote-meetup', 'MeetupController@upDownVoteMeetup');
    Route::any('front-edit-meetup/{edit_id}', 'MeetupController@frontEditPostMeetup')->name('editMeetupFront');
    Route::any('post-cost-meetup', 'MeetupController@ajaxCostMeetup');
    Route::any('remove-meetup-agenda', 'MeetupController@deleteAgenda');
    Route::any('remove-event-agenda', 'EventController@deleteAgenda');
    Route::any('comment-job', 'JobController@commentJob');

    /* [Start] :: Manage Backend education */
    Route::get('admin/education/list', 'EducationController@listEducation');
    Route::post('admin/education/delete', 'EducationController@deleteEducation');
    Route::any('admin/education/edit/{edit_id}', 'EducationController@editEducation');
    Route::get('admin/education/view/{view_id}', 'EducationController@ViewBackendEducation');
    Route::any('admin/education/comment/{education_id}', 'EducationController@listComment');
    Route::any('admin/education/editcomment/{comment_id}', 'EducationController@editComment');
    Route::post('admin/education/deletecomment', 'EducationController@deleteEducationComments');
    /*     * *** front education******** */
    Route::get('education', 'EducationController@postEducation');
    Route::any('education/{group_id}', 'EducationController@postEducation');
    Route::any('post-education', 'EducationController@addPostEducation');
    Route::any('list-education', 'EducationController@listFrontEducation');
    Route::any('view-education/{view_id}/{response?}', 'EducationController@viewFrontEducation')->name('viewEducation');
    Route::get('education-view/{post_id}', function($post_id){
       return Redirect("/view-education/$post_id", 301);
    })->where('restOfURL', '.*');

    Route::any('list-my-education', 'EducationController@listUserEducation');
    Route::any('get-user-education', 'EducationController@getUserEducation');
    Route::any('front-edit-education/{edit_id}', 'EducationController@frontEditPostEducation')->name('editEducationFront');
    Route::any('up-down-vote-education', 'EducationController@upDownVoteEducation');
    Route::any('comment-education', 'EducationController@commentEducation');
    /*     * ********************** */

    /* Calender Functionality */
    Route::get('calendar', 'CalendarController@getCalendar');
    Route::get('calendar-ajax', 'CalendarController@getCalendarAjax');
    Route::get('calendar-notification', 'CalendarController@getCalendarNotification');
    Route::get('get-events/{val}', 'CalendarController@getEvents');
    Route::get('get-meetups/{val}', 'CalendarController@getMeetups');
    Route::get('view_meetup/{event_id}', 'CalendarController@viewMeetups');

    Route::any('meetup-recurring-dates-data', 'CalendarController@meetupsDates');
    Route::any('event-recurring-dates-data', 'CalendarController@eventDates');

    Route::get('view_event/{event_id}', 'CalendarController@viewEvent');
    Route::get('event-user-attend/{event_id}/{status}', 'CalendarController@mayBeAttendEvent');
    Route::get('meetup-user-attend/{event_id}/{status}', 'CalendarController@mayBeAttendMeetup');


    /* [Start] :: Manage Backend article */
    Route::get('admin/article/article-list', 'ArticleController@listarticle');
    Route::post('admin/article/delete', 'ArticleController@deletearticle');
    Route::get('admin/article/edit/{edit_id}', 'ArticleController@editarticle');
    Route::post('admin/article/edit/{edit_id}', 'ArticleController@editarticle');
    Route::get('admin/article/view/{view_id}', 'ArticleController@ViewBackendArticle');
    Route::any('admin/article/comment/{article_id}', 'ArticleController@listComment');
    Route::any('admin/article/editcomment/{comment_id}', 'ArticleController@editComment');
    Route::post('admin/article/deletecomment', 'ArticleController@deleteArticleComments');
//Front
    Route::get('article', 'ArticleController@postArticle');
    Route::any('article/{group_id}', 'ArticleController@postArticle');
    Route::any('post-article', 'ArticleController@addPostArticle');
    Route::any('list-article', 'ArticleController@listFrontArticle');
    Route::any('get-article-gallery', 'ArticleController@getArticleList');
    Route::any('get-article-user-data', 'ArticleController@getUserArticle');
    Route::any('list-my-article', 'ArticleController@listUserArticle');
    Route::any('front-edit-article/{edit_id}', 'ArticleController@frontEditPostArticle')->name('editArticleFront');
    Route::any('view-article/{view_id}/{response?}', 'ArticleController@viewArticle');
    Route::any('article-comment', 'ArticleController@frontCommentArticle');
    Route::post('up-down-vote-article', 'ArticleController@upDownVoteArticle');
    Route::any('up-down-vote-job', 'JobController@upDownVoteJob');

    Route::post('follow-post-article', 'ArticleController@followPostArticle');
    Route::post('follow-post-education', 'EducationController@followPostEducation');
    Route::post('follow-post-meetup', 'MeetupController@followPostMeetup');
    Route::post('follow-post-queries', 'AskQuestionController@followPostQuery');
    Route::post('follow-post-event', 'EventController@followPostEvent');
    Route::post('follow-post-job', 'JobController@followPostJob');


    /* [End]:: Manage Backend article */
    /*     * ** manage front question*** */
    Route::any('question', 'AskQuestionController@postQuestion');
    Route::any('question/{group_name}', 'AskQuestionController@postQuestion');
    Route::any('post-question', 'AskQuestionController@addPostQuestion');
    Route::any('list-question', 'AskQuestionController@listFrontQuestion');
    Route::any('list-my-question', 'AskQuestionController@listUserQuestion');
    Route::any('front-edit-question/{edit_id}', 'AskQuestionController@frontEditQuestion')->name('editQriousFront');
    Route::any('view-question/{view_id}/{response?}', 'AskQuestionController@viewQuestion');
    Route::get('view-qrious/{post_id}', function($post_id){
       return Redirect("/view-question/$post_id", 301);
    })->where('restOfURL', '.*');
    Route::any('question-answer', 'AskQuestionController@frontCommentquestion');
//Route::get('like-answer/{answer_id}/{status}','AskQuestionController@likeanswer');
    Route::any('up-down-vote-question', 'AskQuestionController@upDownVoteQuestion');
    Route::post('up-down-vote-answer', 'AskQuestionController@upDownVoteAnswer');

    /*     * ** manage backend question*** */
    Route::get('admin/question/list', 'AskQuestionController@listQuestion');
    Route::post('admin/question/delete', 'AskQuestionController@deleteQuestion');
    Route::get('admin/question/edit/{edit_id}', 'AskQuestionController@editQuestion');
    Route::post('admin/question/edit/{edit_id}', 'AskQuestionController@editQuestion');
    Route::get('admin/question/view/{view_id}', 'AskQuestionController@ViewBackendQuestion');
    Route::any('admin/questions/answer/{question_id}', 'AskQuestionController@listAnswer');
    Route::any('admin/question/editanswer/{answer_id}', 'AskQuestionController@editAnswer');
    Route::post('admin/question/deleteanswer', 'AskQuestionController@deleteQuestionsAnswer');

    /*     * ******manage backend ranking and level********** */
    Route::get('admin/level/list', 'RankingController@listLevel');
    Route::any('admin/level/add', 'RankingController@addLevel');
    Route::any('admin/level/edit/{edit_id}', 'RankingController@editLevel');
    Route::post('admin/level/delete', 'RankingController@deleteLevel');
    Route::any('admin/level/add-check-level', 'RankingController@addCheckLevel');
    Route::any('admin/level/add-check-name', 'RankingController@addCheckName');
    Route::any('admin/level/add-check-min-value', 'RankingController@addCheckMinValue');
    Route::any('admin/level/add-check-max-value', 'RankingController@addCheckMaxValue');

    /* [Backed]::Blog */
    Route::get('admin/blog/list', 'BlogController@listBlogs');
    Route::any('admin/blog/comment/{blog_id}', 'BlogController@listComment');
    Route::any('admin/blog/editcomment/{comment_id}', 'BlogController@editComment');
    Route::any('admin/blog/edit/{blog_id}', 'BlogController@editBlog');
    Route::post('admin/blog/delete', 'BlogController@deleteBlogs');
    Route::post('admin/blog/deletecomment', 'BlogController@deleteBlogsComments');
    Route::get('admin/blog/view/{view_id}', 'BlogController@ViewBackendBlog');

    /* [Front]::Blog */

    Route::any('blog-post', 'BlogController@frontPostBlog');
    Route::any('blog-list', 'BlogController@frontlistBlog');
    Route::any('list-my-blog', 'BlogController@listUserBlog');
    Route::any('blog-detail/{blog_id}', 'BlogController@frontDetailBlog');
    Route::any('blog-comment', 'BlogController@frontCommentBlog');
    Route::post('up-down-vote-blog', 'BlogController@upDownVoteBlog');


    /* Ends here */

    /* Manage Team */
    Route::get('team', 'TeamController@getTeam');
    Route::any('create-team', 'TeamController@createTeam');
    Route::get('my-teams', 'TeamController@yourTeam');
    Route::get('team-details/{team_id}', 'TeamController@teamDetails');
    Route::get('team-detail/{team_id}', 'TeamController@teamDetail');
    Route::get('delete-team/{team_id}', 'TeamController@deleteTeam');
    Route::get('remove-member', 'TeamController@removeMember');
    Route::get('edit-team/{team_id}', 'TeamController@editTeam');
    Route::post('edit-team/{team_id}', 'TeamController@editTeam');
    Route::get('member-detail/{member_id}', 'TeamController@memberDetail');
    Route::get('confirm-team', 'TeamController@confirmRequest');
    Route::get('leave-team', 'TeamController@leaveTeam');

    /* manage Notification  */

    Route::get('notification', 'NotificationController@getNotification');
    Route::post('change-read-notification-status', 'NotificationController@changeReadNotificationStatus');
    Route::post('get-notification-details', 'NotificationController@getNotificationDetails');
    Route::any('change-notification-status', 'NotificationController@changeNotificationStatus');




    /* manage filter */
    Route::post('filter', 'FilterController@saveFilterData');
    Route::post('update-filters', 'FilterController@getFiltersData');

    /* chrone  */
    Route::any('chrone', 'MeetupController@Crone');

    /* Make Report */
    Route::any('make-event-report/{event_id}', 'EventController@makeReport');
    Route::any('make-meetup-report/{meetup_id}', 'MeetupController@makeReport');
    Route::any('make-article-report/{article_id}', 'ArticleController@makeReport');
    Route::any('make-blog-report/{blog_id}', 'BlogController@makeReport');
    Route::any('make-job-report/{job_id}', 'JobController@makeReport');
    Route::any('make-question-report/{question_id}', 'AskQuestionController@makeReport');
    Route::any('make-education-report/{education_id}', 'EducationController@makeReport');


    /* Make Posted In Report */
    Route::any('make-event-group-report/{event_id}', 'EventController@makeGroupReport');
    Route::any('make-meetup-group-report/{meetup_id}', 'MeetupController@makeGroupReport');
    Route::any('make-article-group-report/{article_id}', 'ArticleController@makeGroupReport');
    Route::any('make-blog-group-report/{blog_id}', 'BlogController@makeGroupReport');
    Route::any('make-job-group-report/{job_id}', 'JobController@makeGroupReport');
    Route::any('make-education-group-report/{education_id}', 'EducationController@makeGroupReport');
    Route::any('make-question-group-report/{question_id}', 'AskQuestionController@makeGroupReport');
    /*     * ***for repost*** */

    Route::any('front-question-repost/{question_id}/{created_at}', 'AskQuestionController@frontRepost');
    Route::any('front-article-repost/{article_id}/{created_at}', 'ArticleController@frontRepost');
    Route::any('front-event-repost/{event_id}/{created_at}', 'EventController@frontRepost');
    Route::any('front-meetup-repost/{user_id}/{created_at}', 'MeetupController@frontRepost');
    Route::any('front-blog-repost/{blog_id}/{created_at}', 'BlogController@frontRepost');
    Route::any('front-education-repost/{education_id}/{created_at}', 'EducationController@frontRepost');
    Route::any('front-job-repost/{job_id}/{created_at}', 'JobController@frontRepost');

    /* Front end search routes */

    Route::get('search-result', 'SearchController@getAllRecord');
    Route::get('search-result-with-tag', 'SearchController@getAllRecord');
    Route::any('post-search', 'SearchController@viewSearchReuslt');
    //Route::any('post-search/{search_id}', 'SearchController@viewSearchReuslt');
    //Route::any('post-search/{search_id}/{tags}', 'SearchController@viewSearchReuslt');
    Route::any('search', 'SearchController@viewSearchAReuslt');
    Route::any('post-search-ajax/{search_id}/{tags}', 'SearchController@viewSearchAReuslt');

    Route::any('cron-update-user-location-history-table', 'CronController@UpdateUserLocationHistoryTable');
    Route::any('cron-send-audience-count-email', 'CronController@SendPostAudienceCountEmails');
    Route::any('cron-delete-old-queue-jobs', 'CronController@DeleteOldQueueJobs');
    Route::any('cron-update-user-last-login', 'CronController@UpdateUserLastLoginUsingLocationHistoryTable');
    Route::any('cron-send-email-verification-reminder-email', 'CronController@sendEmailVerificationReminderEmail');

    Route::post('update-user-location', 'UserAccountController@updateUserLocation');
    Route::post('get-all-post', 'UserAccountController@getAllPosts');
    Route::post('get-all-trending-post', 'UserAccountController@getAllTrendingPosts');
    Route::post('get-all-user-connection', 'UserAccountController@getAllUsersForConnection');
    Route::post('get-all-user-follower', 'UserAccountController@getAllUsersForFollower');
    Route::post('get-all-user', 'UserAccountController@getAllUsers');
    Route::post('get-all-education-post', 'EducationController@getAllEucationPosts');


    Route::post('get-all-user-post-question', 'AskQuestionController@getAllUserPostsQuestion');
    Route::post('get-all-post-question', 'AskQuestionController@getAllPostsQuestion');
    Route::post('get-all-user-post-job', 'JobController@getAllUserPostsJob');
    Route::post('get-all-post-job', 'JobController@getAllPostsJob');
    Route::post('get-all-save-job', 'JobController@getAllSaveJob');

    Route::post('get-all-user', 'UserAccountController@getAllUsers');
    Route::get('get-username-tagging','UserAccountController@getAllUserTagging');

    Route::get('message', 'ImsController@messageWindow');
    Route::post('compose-message-modal', 'ImsController@composeMessageModal');
    Route::post('get-message', 'ImsController@getMessage');
    Route::post('compose-message', 'ImsController@composeMessage');
    Route::post('delete-message', 'ImsController@deleteMessage');
    Route::post('unread-message-counter', 'ImsController@unreadMessageCounter');

    Route::any('upload-content-image', 'EventController@uploadContentImage');

    /* Group connection */
    Route::any('connect-list/{group_id}', 'GroupController@listUserConnect');


    Route::any('save-user-post', 'UserController@saveUserPost');
    /* calendar count fot going user event and meetup */
    Route::any('get-user-going-event-count', 'EventController@getUserGoingEventCount');
    Route::any('get-user-going-meetup-count', 'MeetupController@getUserGoingMeetupCount');
    Route::any('get-month-meetup-count', 'CalendarController@getMonthMeetupCount');
    Route::any('get-month-event-count', 'CalendarController@getMonthEventCount');

    /* Ends here */
    Route::any('check-article-title', 'ArticleController@chkArticleTitle');
    Route::any('check-title-autosave', 'ArticleController@chkTitleAutosave');
    Route::any('check-article-title-edit', 'ArticleController@chkArticleTitleE');
    Route::any('check-question-title', 'AskQuestionController@chkQuestionTitle');
    Route::any('check-question-title-edit', 'AskQuestionController@chkQuestionTitleE');
    Route::any('check-education-title', 'EducationController@chkEducTitle');
    Route::any('check-education-title-edit', 'EducationController@chkEducTitleE');
    Route::any('check-event-title', 'EventController@chkEventTitle');
    Route::any('check-event-title-edit', 'EventController@chkEventTitleE');
    Route::any('check-meetup-title', 'MeetupController@chkMeetUpTitle');
    Route::any('check-meetup-title-edit', 'MeetupController@chkMeetUpTitleE');
    Route::any('check-job-title', 'JobController@chkJobTitle');
    Route::any('check-job-title-edit', 'JobController@chkJobTitleE');
    Route::any('remember-send-checkbox', 'ImsController@setCheckboxsession');

    /*     * **mantis issue 350**** */
    Route::any('public', 'UserAccountController@checkPublicUrl');

    /*     * ******slider banner(for app)********** */
    Route::get('admin/slider-banner/list-sliders-banners', 'SliderBanner@index');
    Route::post('admin/slider-banner/delete-sliders-banners', 'SliderBanner@deleteSliderBanner');
    Route::get('admin/slider-banner/add-slider-banner', 'SliderBanner@AddSliderBanner');
    Route::post('admin/slider-banner/add-slider-banner', 'SliderBanner@PostSliderBanner');
    Route::get('admin/slider-banner/edit-slider-banner/{slider_id}', 'SliderBanner@EditSliderBanner');
    Route::post('admin/slider-banner/edit-slider-banner/{slider_id}', 'SliderBanner@UpdateSliderBanner');

    #For meet-up api redirect routes
    Route::any('get-meetup-access','EventController@getMeetupEvents');
    Route::get('get-meetup-events','EventController@fetchMeetupEvents');

    /**Schedule email to user **/
    Route::any('send-email-to-users','UserAccountController@sendScheduledEmail');


    /*     * *******web services************* */
    Route::group(['middleware' => ['api.authorization']], function () {
        Route::any('ws-register-user', 'WebServices@wsRegistration');
        Route::any('ws-selected-group', 'WebServices@wsSelectedGroup');
        Route::any('ws-login-user', 'WebServices@wsLogin');
        Route::any('ws-resend-mail', 'WebServices@wsResendMail');
        Route::any('ws-forgot-password', 'WebServices@wsForgotPassword');
        Route::any('ws-change-password', 'WebServices@wsChangePassword');
        Route::any('ws-account-setting', 'WebServices@wsAccountSetting');
        Route::any('ws-update-user-location', 'WebServices@wsUpdateLocation');
        Route::any('ws-edit-account-setting', 'WebServices@wsEditAcountSetting');

        Route::any('ws-group', 'WebServices@wsGroup');
        Route::any('ws-sub-group', 'WebServices@wsChildGroup');
        Route::any('ws-get-group-wise-records', 'WebServices@wsGetGroupWiseRecords');
        Route::any('ws-get-trending-groups', 'WebServices@wsGetTrendingGroups');
        Route::any('ws-get-major-groups', 'WebServices@wsGetMajorGroups');
        Route::any('ws-get-group-details-by-id', 'WebServices@wsGetGroupDetailsById');

        Route::any('ws-dashboard', 'WebServices@wsDashboard');
        Route::any('ws-feed-detail', 'WebServices@wsFeedDetail');
        Route::any('ws-audience', 'WebServices@wsAudience');


        Route::any('ws-slider-banner', 'WebServices@wsSliderBanner');
        Route::any('ws-upvote-downvote', 'WebServices@wsUpvoteDownvote');
        Route::any('ws-report-abuse', 'WebServices@wsReport');
        //Route::any('ws-rsvp-event', 'WebServices@wsRsvpEvent');

        Route::any('ws-post-article-step-one','WebServices@wsPostArticleStepOne');
        Route::any('ws-post-article-step-two','WebServices@wsPostArticleStepTwo');

        Route::any('ws-post-event-step-one', 'WebServices@wsPostEventStepOne');
        Route::any('ws-post-event-step-two', 'WebServices@wsPostEventStepTwo');
        Route::any('ws-post-event-step-three', 'WebServices@wsPostEventStepThree');

        Route::any('ws-post-meetup-step-one', 'WebServices@wsPostMeetupStepOne');
        Route::any('ws-post-meetup-step-two', 'WebServices@wsPostMeetupStepTwo');
        Route::any('ws-post-meetup-step-three', 'WebServices@wsPostMeetupStepThree');

        Route::any('ws-post-qrious-step-one','WebServices@wsPostQriousStepOne');
        Route::any('ws-post-qrious-step-two','WebServices@wsPostQriousStepTwo');

        Route::any('ws-post-education-step-one','WebServices@wsPostEducationStepOne');
        Route::any('ws-post-education-step-two','WebServices@wsPostEducationStepTwo');

        Route::any('ws-post-job-step-one','WebServices@wsPostJobStepOne');
        Route::any('ws-post-job-step-two','WebServices@wsPostJobStepTwo');

        Route::any('ws-gallary-feeds','WebServices@wsGallaryFeeds');
        Route::any('ws-my-feeds','WebServices@wsMyFeeds');
        Route::any('ws-save-feeds','WebServices@wsSavedFeeds');

        Route::any('ws-add-follower','WebServices@wsFollowUnfollow');
        Route::any('ws-add-connect','WebServices@wsConnectDisconnect');
        Route::any('ws-rsvp','WebServices@wsRsvp');
        Route::any('ws-social-media','WebServices@wsSocialMedia');

        Route::any('ws-calender-list','WebServices@wsCalenderList');
        Route::any('ws-date-wise-calender-list','WebServices@wsDateWiseCalenderList');

        Route::any('ws-calender-myintrest-dates','WebServices@wsCalenderMyintrestDates');
        Route::any('ws-calender-my-intrest','WebServices@wsCalenderMyIntrest');

        Route::any('ws-profile','WebServices@wsProfile');
        Route::any('ws-upload_cover-photo','WebServices@wsUploadCoverPhoto');
        Route::any('ws-profile-info','WebServices@wsProfileInfo');
        Route::any('ws-upload-guest-images','WebServices@wsUploadGuestImages');

        Route::any('ws-comment','WebServices@wsComment');
        Route::any('ws-reply-comment','WebServices@wsReplyComment');
        Route::any('ws-get-reply-comment','WebServices@wsGetReplyComment');

        Route::any('ws-get-notification-list','WebServices@wsGetNotifications');
        Route::any('ws-mark-notifications-read','WebServices@wsMarkNotificationsRead');
        Route::any('ws-search','WebServices@wsSearch');
        Route::any('ws-list-comment-replies','WebServices@listCommentReplies');
        Route::any('ws-explorer-groups','WebServices@wsExplorerGroup');
        Route::any('ws-single-group-details','WebServices@wsSingleGroupDetails');
        Route::any('ws-single-group-post-list','WebServices@wsSingleGroupPosttList');
        Route::any('ws-single-group-details','WebServices@wsSingleGroupDetails');
        Route::any('ws-connect-user','WebServices@wsConnectUser');

        Route::any('ws-join-leave-group','WebServices@wsJoinLeaveGroup');
        Route::any('ws-my-groups','WebServices@wsMyGroups');
        Route::any('ws-group-edit-tagline','WebServices@wsGroupEditTagline');
        Route::any('ws-sub-niche-groups','WebServices@wsSubNicheGroups');
        Route::any('ws-my-post','WebServices@wsMyPost');
        Route::any('ws-delete-my-post','WebServices@wsDeleteMyPost');
        Route::any('ws-my-saved-post','WebServices@wsMySavedPost');
        Route::any('ws-delete-my-saved-post','WebServices@wsDeleteMySavedPost');
        Route::any('ws-accept-request','WebServices@wsAcceptRequest');
        Route::any('ws-single-connect-request','WebServices@wsSingleConnectRequest');
        Route::any('ws-apply-job','WebServices@wsApplyJob');
        Route::any('ws-get-resume','WebServices@wsGetResume');
        Route::any('ws-upload-resume','WebServices@wsUploadResume');
        Route::any('ws-get-user-followers','WebServices@wsGetUserFollowers');
        Route::any('ws-get-user-followings','WebServices@wsGetUserFollowings');
        Route::any('ws-get-user-connections','WebServices@wsGetUserConnections');

        Route::any('ws-get-status-of-x-f-y','WebServices@wsGetXfollowsYOrNot');
        Route::any('ws-follow-unfollow','WebServices@wsFollowUnfollowUser');
        //webview for post description
        Route::any('view-{feature_name}/{view_id}/wv/description', 'WebServices@wsPostDescription');

        Route::any('send-notify','WebServices@send_notification');
        Route::any('ws-connect-user-details','WebServices@wsConnectUserDetails');
    });

Route::any('facebook-data','EventController@facebookData');
Route::any('facebook-redirect-uri','EventController@fbRedirectURI');
Route::any('get-facebook-event','EventController@fetchFacebookEvents');


/* Manage blacklist word */
Route::get('admin/users/list/blacklist', 'AdminController@listblacklistnames');
Route::post('add_blacklist_name', 'AdminController@addNewBlacklistname');
Route::post('admin/blacklist/delete', 'AdminController@deleteBlacklist');
Route::get('admin/blacklist/edit/{edit_id}', 'AdminController@editBlacklist');
Route::post('admin/blacklist/update/{edit_id}', 'AdminController@updateBlacklist');
Route::post('verify_username', 'UserAccountController@verify_username');

Route::any('mypost', 'MyPostController@mypostall');
Route::post('get-my-post', 'MyPostController@getmypost');
}

/* User Public Profile */
Route::get('user-profile/{user_id}', 'UserAccountController@SingleUserprofile');
Route::get('user-activity-load/{id?}', 'UserAccountController@userActivityLoad');


Route::get('go/{group_id}/{post_name}/CO/{country}', 'RegionalPostUrlController@getPostsCountryUrl');
Route::post('go/{group_id}/{post_name}/CO/{country}', 'RegionalPostUrlController@postPostsCountryUrl');

Route::get('go/{group_id}/{post_name}/CI/{city}', 'RegionalPostUrlController@getPostsCityUrl');
Route::post('go/{group_id}/{post_name}/CI/{city}', 'RegionalPostUrlController@postPostsCityUrl');


Route::any('sitemap', 'SitemapController@index');
Route::any('sitemap/{post_name}', 'SitemapController@createPostnameSitemap');
Route::get('sitemap/{post_name}/{c}/CO', 'SitemapController@createPostnameCountrySitemap');
Route::get('sitemap/{post_name}/{c}/CI', 'SitemapController@createPostnameCitySitemap');


Route::any(config('l5-swagger.routes.docs').'/{jsonFile?}',[
    'as' => 'l5-swagger.docs',
    'middleware' => config('l5-swagger.routes.middleware.docs', []),
    'uses' => 'SwaggerController@docs',
]);

Route::any('get-parent-group2', 'GroupController@getParentGroup2');
Route::any('get-parent-group3', 'GroupController@getParentGroup3');

Route::any('getPromotionUserInGroup', 'GroupController@getPromotionUserInGroup');
Route::any('getAllDraftedUsers', 'MyPostController@getAllDraftUsers');
Route::any('getAllAbsentUsers', 'UserController@getAllAbsentUsers');
Route::any('getAllUsers', 'UserController@getAllUsers');

/* Payment Gateway Request & Response */
Route::any('order/{type}/{id}/{tnum}', 'PaymentController@viewBillSummary');
Route::any('order/confirm/{type}/{cost_id}/{cost}/{tcount}', 'PaymentController@confirmBill');
Route::post('order/promo', 'PaymentController@viewPromoBillSummary');
Route::any('order/promo/confirm/{type}/{post_id}/{cost}/{dur}', 'PaymentController@confirmPromoBill');
Route::any('order/response', 'PaymentController@paymentResponse');
Route::any('payment/{tid}/{orderid}/{tcost}', 'PaymentController@paymentRequest');


Route::any('send-end-promo-mail', 'UserAccountController@sendEndPromoEmail');
Route::any('send-daily-promo-mail', 'UserAccountController@sendDailyPromoEmail');

Route::post('ajax/get-major-groups', 'GroupController@getMajorGroups');
Route::post('ajax/search-groups-by-name', 'GroupController@searchGroupsByName');
Route::any('ajax/get-group-details-by-id', 'GroupController@getGroupDetailsById');
Route::post('push-subscription/{id}','UserAccountController@pushapi');
Route::post('admin/post-message/{id}','UserAccountController@webpushmessagetospececificuser');

Route::post('admin/backend-push-notify','AdminController@webpushtoall');