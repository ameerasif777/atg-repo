<?php

namespace App\Model;

use DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Auth;

class RegisterModel extends Model {

    public function getUserInformation($table, $fields = '', $condition_username = '', $condition_email = '') {
        $str_sql = '';
        if (is_array($fields)) { /* $fields passed as array */
            $str_sql.=implode(",", $fields);
        } elseif ($fields != "") { /* $fields passed as string */
            $str_sql .= $fields;
        } else {
            $str_sql .= '*';  /* $fields passed blank */
        }
        //$this->db->select($str_sql, FALSE);
        $str_query = DB::table($table)
                ->select($str_sql);
        if (is_array($condition_username)) { /* $condition passed as array */
            if (count($condition_username) > 0) {
                foreach ($condition_username as $field_name => $field_value) {
                    if ($field_name != '' && $field_value != '') {
                        $str_query->where($field_name, $field_value);
                    }
                }
            }
        } else if ($condition_username != "") { /* $condition passed as string */
            $str_query->where($condition_username);
        }
        if (is_array($condition_email)) {
            if (count($condition_email) > 0) {
                foreach ($condition_email as $field_name => $field_value) {
                    if ($field_name != '' && $field_value != '') {
                        $str_query->orwhere($field_name, $field_value);
                    }
                }
            }
        } else if ($condition_email != "") {
            $str_query->orwhere($condition_email);
        }

        $result = $str_query->get();
        return $result;
    }

    public function getSkills($search_val, $already_added) {
        $str_query = DB::table('mst_skills')
                ->select('*');
        if ($search_val != '') {
            $str_query->where('skill_name', 'like', '%' . $search_val . '%');
        }
        if (count($already_added) > 0) {
            $str_query->whereNotIn('skill_name', $already_added);
        }
        $result = $str_query->get();
        return $result;
    }

}
