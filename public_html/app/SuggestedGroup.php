<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestedGroup extends Model
{
    protected $table = 'suggested_groups';
    protected $fillable = ['name'];
}