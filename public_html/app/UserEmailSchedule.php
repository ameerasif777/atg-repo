<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEmailSchedule extends Model
{
    protected $table = 'user_email_schedules';
    protected $fillable = array('*');
    
    function getUser(){
        return $this->belongsTO('App\User','id','user_id');
    }
    
    
}
