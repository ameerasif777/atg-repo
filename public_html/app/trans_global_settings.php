<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trans_global_settings extends Model {

    protected $table = 'trans_global_settings';
    protected $fillable = array('global_name_id','value');

}
