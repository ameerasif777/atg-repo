<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class email_templates extends Model
{
    protected $table = 'mst_email_templates';
    protected $fillable = array('email_template_title','email_template_subject','email_template_content','lang_id','created_by');
    public function values()
    {
        return $this->hasMany('App\languages','id','lang_id'); 
    }
}
