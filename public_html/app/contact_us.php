<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact_us extends Model {

    protected $table = 'mst_contact_us';
    protected $fillable = array('name','message_to');

    public function values() {
        return $this->hasMany('App\contact_us_feedback_reply', 'contact_id');
    }

}
