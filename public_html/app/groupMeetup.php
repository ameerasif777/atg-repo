<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupMeetup extends Model
{
    public $table = "trans_meetup_group";
    public $fillable =array('meetup_group_id','meetup_id_fk','group_id_fk');
    
}
