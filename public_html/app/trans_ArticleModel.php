<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trans_ArticleModel extends Model
{
    protected $table = 'trans_article';
    protected $fillable = array('id','article_id_fk','group_id_fk','created_at','updated_at');
}
