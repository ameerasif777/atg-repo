<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class languages extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mst_languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lang_name', 'status'];

}
