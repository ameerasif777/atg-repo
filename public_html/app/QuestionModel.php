<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model
{
    protected $table = 'mst_question';
    protected $fillable = array('id','user_id_fk','title','description','tags','created_at','profile_image');
    
    function getQuestionUser(){
        return $this->hasMany('App\User','id','user_id_fk');
    }
    
    
}
