<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Session;
//use App\DB;
use Illuminate\Database\Eloquent\Model;


class Group extends Model {

    protected $table = 'mst_group';
    protected $fillable = array('group_name', 'parent_id', 'status', 'group_description', 'profession');

    public function lastSevenDaysUsers() {
        $userTypeQry = "CASE p1036_mst_users.user_type WHEN 1 THEN 'Student' WHEN 2 THEN 'Admin' WHEN 3 THEN 'Professional' WHEN 4 THEN 'Company' END AS user_type";
        return $this->select('mst_users.id', 'mst_users.first_name', 'mst_users.last_name', 'mst_users.email', \DB::raw($userTypeQry))
                        ->join('trans_group_users', 'trans_group_users.group_id_fk', '=', 'mst_group.id')
                        ->join('mst_users', 'trans_group_users.user_id_fk', '=', 'mst_users.id')
                        ->where('mst_group.id', $this->id)
                        ->whereRaw("(DATE(p1036_trans_group_users.created_at) <= CURDATE() AND DATE(p1036_trans_group_users.created_at) >= ADDDATE(CURDATE(), '-7 day'))");
    }

    public function lastThirtyDaysUsers() {
        $userTypeQry = "CASE p1036_mst_users.user_type WHEN 1 THEN 'Student' WHEN 2 THEN 'Admin' WHEN 3 THEN 'Professional' WHEN 4 THEN 'Company' END AS user_type";
        return $this->select('mst_users.id', 'mst_users.first_name', 'mst_users.last_name', 'mst_users.email', \DB::raw($userTypeQry))
                        ->join('trans_group_users', 'trans_group_users.group_id_fk', '=', 'mst_group.id')
                        ->join('mst_users', 'trans_group_users.user_id_fk', '=', 'mst_users.id')
                        ->where('mst_group.id', $this->id)
                        ->whereRaw("(DATE(p1036_trans_group_users.created_at) <= CURDATE() AND DATE(p1036_trans_group_users.created_at) >= ADDDATE(CURDATE(), '-30 day'))");
    }

    public function users() {
        $userTypeQry = "CASE p1036_mst_users.user_type WHEN 1 THEN 'Student' WHEN 2 THEN 'Admin' WHEN 3 THEN 'Professional' WHEN 4 THEN 'Company' END AS user_type";
        return $this->select('mst_users.id', 'mst_users.first_name', 'mst_users.last_name', 'mst_users.email', \DB::raw($userTypeQry))
                        ->join('trans_group_users', 'trans_group_users.group_id_fk', '=', 'mst_group.id')
                        ->join('mst_users', 'trans_group_users.user_id_fk', '=', 'mst_users.id')
                        ->where('mst_group.id', $this->id);
    }

    public function getChild()
    {
//        $user_session = Session::get('user_account');    
//          $user_id=$user_session['id'];
////          print_R($user_id);die;
//        $arr_users_group = DB::table('trans_group_users as t')
//                            ->join('mst_group as g', 'g.id', '=', 't.group_id_fk', 'left')
//                            ->where('t.user_id_fk', $user_id)
//                            ->select('g.group_name', 'g.id')
//                            ->get();
//        print_R($arr_users_group);die;
        return $this->hasMany('App\group','parent_id','id');
    }
    
      public function getUpVote()
    {
          
          $user_session = Session::get('user_account');    
          $user_id=$user_session['id']; 
           
          return $this->hasMany('App\VoteStatistic','group_id_fk','id')->where(['vote'=>'0','user_id_fk'=>$user_id]);
    }
      public function getDownVote()
    {
          $user_session = Session::get('user_account');    
          $user_id=$user_session['id']; 
          
        return $this->hasMany('App\VoteStatistic','group_id_fk','id')->where(['vote'=>'1','user_id_fk'=>$user_id]);
    }

    // loads only direct children - 1 level
    public function childGroup()
    {
        return $this->hasMany('App\group', 'parent_id', 'id')
                    ->orderBy('group_name');
    }

    // recursive, loads all descendants
    public function childGroups()
    {
        return $this->childGroup()->with('childGroups');
    }

    // loads only direct parent - 1 level
    public function parentGroup()
    {
        return $this->belongsTo('App\group', 'parent_id', 'id');
    }

    // recursive, loads all ascendants
    public function parentGroups()
    {
        return $this->parentGroup()->with('parentGroups');
    }

    public static function getDetailsById($group_id = null)
    {
        if ($group_id === null) return $group_id;

        return Group::where('id', $group_id)
                    ->with('childGroups:id,parent_id,group_name,icon_img')
                    ->first(['id', 'parent_id', 'group_name', 'icon_img']);
    }

    // returns all major groups, sorted by popularity
    public static function getMajorGroups($limit = 10, $offset = 0)
    {
        $query = DB::table('mst_group')
                 ->leftJoin('trans_group_users', 'mst_group.id', '=', 'trans_group_users.group_id_fk')
                 ->select(DB::raw('count(*) as count, p1036_mst_group.id as id, parent_id, icon_img, cover_img, group_name'))
                 ->groupBy('id')
                 ->where('parent_id', 0)
                 ->orderBy('count', 'desc');
        if ($limit >=0 && $offset >= 0) {
            $query->offset($offset)->limit($limit);
        }
        return $query->get();
    }
    
    public static function getTrendingGroups($limit = 10, $offset = 0)
    {
        return DB::table('trans_group_users')
                 ->join('mst_group', 'trans_group_users.group_id_fk', '=', 'mst_group.id')
                 ->select(DB::raw('count(*) as count, group_id_fk as id, icon_img, cover_img, group_name, parent_id'))
                 ->groupBy('group_id_fk')
                 ->orderBy('count', 'desc')
                 ->offset($offset)
                 ->limit($limit)
                 ->get();
    }
}
