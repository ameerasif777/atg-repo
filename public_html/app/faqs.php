<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faqs extends Model
{
    protected $table = 'mst_faqs';
    protected $fillable = array('question,answer,lang_id,status');
//    public function values() {
//        return $this->hasMany('App\faq_categories','id');    
//    }
}
