<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role_privileges extends Model
{
    protected $table = 'trans_role_privileges';
    protected $fillable = array('privilege_id', 'role_id');
}
