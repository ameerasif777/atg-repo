<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupQuestion extends Model
{
    protected $table = 'trans_question_group';
    protected $fillable = array('question_group_id','group_id_fk','question_id_fk');
}
