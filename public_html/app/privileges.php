<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class privileges extends Model
{
    protected $table = 'mst_privileges';
    protected $fillable = array('privilege_name');
}
