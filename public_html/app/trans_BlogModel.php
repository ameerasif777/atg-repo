<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trans_BlogModel extends Model
{
   protected $table = 'trans_blogs';
    protected $fillable = array('id','blog_id_fk','group_id_fk','created_at','updated_at');
}
