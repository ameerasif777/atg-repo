<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class commentMeetup extends Model
{
    public $table = 'trans_meetup_comments';
    public $field = array('meetup_id_fk','commented_by','commented_user_id','comment','comment_on');
}
