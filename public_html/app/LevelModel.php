<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelModel extends Model
{
    protected $table = 'mst_level';
    protected $fillable = array('id','min_value','max_value','level','created_at');
}
