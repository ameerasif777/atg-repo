<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionalPostUrlModel extends Model
{
    protected $table = 'mst_group_posts_regional_url_seo';
    protected $fillable = array('id','posts','group','city_country','url','status','created_at','updated_at');

}
