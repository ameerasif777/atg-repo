<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleModel extends Model
{
    protected $table = 'mst_article';
    protected $fillable = array('id','user_id_fk','title','description','tags','created_at','profile_image');
    
    function getArticleUser(){
        return $this->hasMany('App\User','id','user_id_fk');
    }
    
}
