<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    protected $table = 'trans_group_users';
    protected $fillable = array('user_id_fk','group_id_fk','status','created_at');

    function group()
    {
        return $this->belongsTo(Group::class, 'group_id_fk', 'id');
    }
}
