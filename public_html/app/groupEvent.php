<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupEvent extends Model
{
    protected $table = 'trans_event_group';
    protected $fillable = array('event_group_id','group_id_fk','event_id_fk');
}
