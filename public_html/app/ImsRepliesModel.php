<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImsRepliesModel extends Model {

    protected $table = 'trans_ims_replies';
    protected $fillable = array('id', 'message_id_fk', 'message_from', 'message_to', 'message', 'message_type', 'is_read', 'is_attachment', 'attachment_name', 'deleted_by_sender', 'deleted_by_reciever');
}
