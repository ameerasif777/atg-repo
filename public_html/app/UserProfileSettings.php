<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfileSettings extends Model
{
	protected $table="trans_profile_settings";
}
