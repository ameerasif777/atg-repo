<?php
namespace App\Helpers;
class FlashMessage {

    public static function DisplayAlert($message, $type) {

        if ($type == 'success') {
            $message_slogan = 'Success! ';
        }
        if ($type == 'info') {
            $message_slogan = 'Note! ';
        }
        if ($type == 'warning') {
            $message_slogan = 'Warning! ';
        }
        if ($type == 'danger') {
            $message_slogan = 'Error! ';
        }
        return "<div class='alert alert-" . $type . "'>
                <button type='button' class='close alert-close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <strong>" . $message_slogan . "</strong> " . $message . "
            </div>";
        
    }

// End the DisplayAlert Function 
}

// End FlashMessage Class