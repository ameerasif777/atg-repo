<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class commentBlog extends Model
{
    public $table = 'trans_blog_comments';
    public $field = array('*');
}
