<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamUser extends Model {

    protected $table = 'trans_team_user_connection';

}
