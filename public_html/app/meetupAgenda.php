<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class meetupAgenda extends Model
{
    public $table = 'mst_meetup_agenda';
}
