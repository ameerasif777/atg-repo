<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $table = 'mst_comments';
    public $field = array('commented_by','parent_id','commented_user_id','comment','comment_on');
}
