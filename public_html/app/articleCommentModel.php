<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articleCommentModel extends Model
{
    public $table="trans_article_comments";
    public $field = array('*');
}
