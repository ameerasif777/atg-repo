<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfileImages extends Model
{
	protected $table='trans_user_profile_images';
}
