<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionCommentModel extends Model
{
    public $table="trans_question_answers";
    public $field = array('*');
}
