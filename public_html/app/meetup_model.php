<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class meetup_model extends Model {

    protected $table = 'mst_meetup';
    protected $fillable = array('id', 'title', 'venue', 'contact_name', 'contact_number', 'email_address', 'meetup_day', 'meetup_time', 'agenda', 'description', 'guest', 'cost', 'last_date', 'min_age', 'max_age');
    
    
    function getMeetupUser(){
        return $this->hasMany('App\User','id','user_id_fk');
    }
    
}

class meetupDays extends Model {

    protected $table = 'trans_meetup_recurring_days';
    protected $fillable = array('recurring_day_id', 'recurring_day', 'meetup_id_fk');
}

?>
