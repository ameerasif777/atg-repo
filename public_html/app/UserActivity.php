<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table = 'mst_user_activity';

    public function user(){

		return $this->belongsTo('App\User', 'user_id_fk');
	}


}
