<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

class webpushnotification extends Notification
{
    use Queueable;

    public $title,$body,$icon;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($title,$body,$icon=null)
    {
        //
        $this->title=$title;
        $this->body=$body;
        $this->icon=$icon;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toWebPush($notifiable,$notification)
    {
        $time=\Carbon\Carbon::now();
        $webpushmessage= new WebPushMessage;
        // return WebPushMessage::class()
                   $webpushmessage ->title($this->title);
                   $webpushmessage->icon($this->icon);
                   $webpushmessage ->body($this->body);
        return $webpushmessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
