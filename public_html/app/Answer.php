<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $table = 'mst_answers';
    public $field = array('answered_by','answerted_user_id','answer','answer_on');
}
