<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConnection extends Model {

    protected $table = 'trans_user_connection';
    protected $fillable = array('to_user_id','from_user_id','status','created_at','updated_at');

    public function from_user() {
        return $this->belongsTo('\App\User', 'from_user_id');
    }

    public function to_user() {
        return $this->belongsTo('\App\User', 'to_user_id');
    }

}
