<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class commentEvent extends Model
{
    public $table = 'trans_event_comments';
    public $field = array('event_id_fk','commented_by','commented_user_id','comment','comment_on');
}
