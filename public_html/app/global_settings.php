<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class global_settings extends Model {
    protected $table = 'mst_global_settings';
    protected $fillable = array('name','value');
    public function values() {
        return $this->hasMany('App\trans_global_settings', 'global_name_id');
    }

}
