<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobModel extends Model
{
    protected $table = 'mst_job';
    protected $fillable = array('id','user_id_fk','job_title','job_location', 'latitude', 'longitude', 'job_desscription','year_of_experience','preferred_qualification','role','skill','tags','external_link_to_apply','funcational_area','employment_type','application_deadline','salary_range','phone_number','email_address','website','status', 'cmp_name');
    
    function getJobUser(){
       
        return $this->hasMany('App\User','id','user_id_fk');
    }

    function user()
    {
        return $this->belongsTo('App\User', 'user_id_fk');
    }

    function reported()
    {
        return $this->hasMany('App\ReportedPosts', 'job_id', 'id');
    }
}
