<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postPromotion extends Model
{
     protected $table = 'trans_post_promotion';
     protected $fillable = array('post_id_fk','post_type','promo_start_date','promo_end_date','amount','status');

}
