<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    protected $table = 'mst_event';
    protected $fillable = array('id', 'title', 'venue', 'contact_name', 'contact_number', 'email_address', 'day', 'event_time', 'end_date','agenda', 'description', 'guest', 'cost', 'last_date', 'age_group');
    
function getEventUser(){
    return $this->hasMany('App\User','id','user_id_fk');
}
    
    
    
}

class EventDays extends Model {

    protected $table = 'trans_event_recurring_days';
    protected $fillable = array('recurring_day_id', 'recurring_day', 'event_id_fk');
}


?>
