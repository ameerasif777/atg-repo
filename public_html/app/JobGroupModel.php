<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGroupModel extends Model
{
    protected $table = 'trans_job_group';
    protected $fillable = array('job_group_id','group_id_fk','job_id_fk');
}
