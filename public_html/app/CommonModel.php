<?php

namespace App;

use DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Log;

class CommonModel extends Model {
    #function for get Record from respective table

    public function getRecords($table, $fields = '', $condition = '', $order_by = '', $limit = '', $debug = 0, $offset = '', $group_by = '') {
        $str_sql = '';
        if (is_array($fields)) { /* $fields passed as array */
            $str_sql.=implode(",", $fields);
        } elseif ($fields != "") { /* $fields passed as string */
            $str_sql .= $fields;
        } else {
            $str_sql .= '*';  /* $fields passed blank */
        }
        //$this->db->select($str_sql, FALSE);
        $str_query = DB::table($table)
                ->select($fields);
        if (is_array($condition)) { /* $condition passed as array */
            if (count($condition) > 0) {
                foreach ($condition as $field_name => $field_value) {
                    if ($field_name != '' && $field_value != '') {
                        $str_query->where($field_name, $field_value);
                    }
                }
            }
        } else if ($condition != "") { /* $condition passed as string */
            $str_query->where($condition);
        }
        if ($limit != '' && $offset != '') {
            $str_query->take($limit)
                    ->skip($offset);
        }
        if ($group_by != '') {
            $str_query->groupBy($group_by);
        }
        if (is_array($order_by)) {
            $str_query->orderBy($order_by[0], $order_by[1]);  /* $order_by is not blank */
        } else if ($order_by != "") {
            $str_query->orderBy($order_by);  /* $order_by is not blank */
        }
        $result = $str_query->get();
        return $result;
    }

    public function getRecordss($table, $fields = '', $where_field_name = '', $where_field_value = '', $order_by = '', $group_by = '', $offset = '', $limit = '') {
        $str_query = '';
        $str_query = DB::table($table)
                ->select($fields);
        if ($where_field_name != '') {
            $str_query->where($where_field_name, '=', $where_field_value);
        }
        if ($order_by != '') {
            $str_query->orderBy($order_by);
        }
        if ($group_by != '') {
            $str_query->groupBy($group_by);
        }

        if ($limit != '' && $offset != '') {
            $str_query->take($limit)
                    ->skip($offset);
        }

        $result = $str_query->get();
        return $result;
    }

    public function updateRow($table, $update_data, $condition) {
        $result = DB::table($table)
                ->where($condition)
                ->update($update_data);
        return $result;
    }

    public function insertRow($table, $insert_data) {
        $result = DB::table($table)
                ->insert($insert_data);
        $last_id = DB::getPdo()->lastInsertId();
        return $last_id;
    }

    public function deleteRow($table, $condtion) {
        $result = DB::table($table)
                ->where($condtion)
                ->delete();
        $last_id = DB::getPdo()->lastInsertId();
        return $last_id;
    }

    public function deleteRows($table, $condtion, $field_name) {
        if (is_array($condtion)) {
            DB::table($table)->whereIn($field_name, $condtion)->delete();
        }
        return;
    }

    public function getCount($table, $fields = '', $condition = '') {
        $str_sql = '';
        $str_sql = 'count(' . $fields . ') as cnt';
        //$this->db->select($str_sql, FALSE);
        $str_query = DB::table($table)
                ->select(DB::raw($str_sql));
        if (is_array($condition)) { /* $condition passed as array */
            if (count($condition) > 0) {
                foreach ($condition as $field_name => $field_value) {
                    if ($field_name != '' && $field_value != '') {
                        $str_query->where($field_name, $field_value);
                    }
                }
            }
        } else if ($condition != "") { /* $condition passed as string */
            $str_query->where($condition);
        }

        $result = $str_query->get();
        return $result;
    }

    public function commonFunction() {
        #checking user loged in or not if not redirecting to login        
        //get Area of studies data.
        $data['absolute_path'] = $this->absolutePath();
        $data['global'] = $this->getGlobalSettings();
        $data['unread_notification_count'] = $this->getUnreadNotificationCount();
        $data['user_session'] = Session::get('user_account');
        
        $arr_user_data = User::find($data['user_session']['id']);
        $user_id = $data['user_session']['id'];
        /* Get my group */
        $get_groups = DB::table('trans_group_users')
                ->where('user_id_fk', $user_id)
                ->get();
        $condition = '';
        $arr_list_group = array();
        $arr_groups = array();
        if (count($get_groups) > 0) {
            foreach ($get_groups as $key => $value) {
                $user_group[$key] = $value->group_id_fk;
            }

            $condition = implode(',', $user_group);
        }
        if ($condition != '') {
            $arr_groups = DB::table('mst_group')
                    ->whereIn('id', explode(',', $condition))
                    ->select('id', 'group_name')
                    ->get();
            $data['globle_group_arr'] = $arr_groups;
        }
        return($data);
    }

    public function getGlobalSettings() {
        $global = array();
        $lang_id = (Session::get('land_id') ? Session::get('land_id') : 14);
        $global = array();
        $result = DB::table('mst_global_settings')
                ->leftJoin('trans_global_settings', 'trans_global_settings.global_name_id', '=', 'mst_global_settings.id')
                ->where('trans_global_settings.lang_id', '=', $lang_id)
                ->get();
        foreach ($result as $row) {
            $global[$row->name] = $row->value;
        }
        return $global;
    }

    #function to get absolute path for project

    public function absolutePath($path = '') {
        $abs_path = str_replace('system/', $path, base_path());
        //Add a trailing slash if it doesn't exist.
        $abs_path = preg_replace("#([^/])/*$#", "\\1/", $abs_path);
        return $abs_path;
    }

    public function getUnreadNotificationCount() {
        $data['user_session'] = Session::get('user_account');
        if (isset($data['user_session']['id'])) {
            $user_id = $data['user_session']['id'];
            $arr_u_notification = DB::table('mst_notifications')
                    ->where('notification_to', $user_id)
                    ->where('read_status', '0')
                    ->where('delete_status', '0')
                    ->get();
            $get_count_of_unread_notificatons = count($arr_u_notification);
            Session::put('unread_notification_count', $get_count_of_unread_notificatons);
        }
    }

    public static function quickRandom($length = 16) {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function isLoggedIn() {
        #checking user loged in or not if not redirecting to login 
        $user_details = Session::get('user_account');
        $user_id = $user_details['id'];
        $user = User::find($user_id);
        if (isset($user->id) && $user->id != "") {
            if ($user->user_status == 0) {
                Auth::logout();
                Session::flush();
                $msg ='We are unable to reach your inbox at <b>' . $user->email . '<b>. Your account is temporarily blocked. Contact us on <a href="https://www.facebook.com/ATGW0rld/">Facebook</a> or <a href="https://www.twitter.com/ATGW0rld/">Twitter</a> and we will unblock your account.'; 
                Session::put('error_msg', $msg, 'danger');
                return "inactive";
            } elseif ($user->user_status == 2) {
                Auth::logout();
                Session::flush();
                $url = '<a href="' . url('/') . '/cms/terms-services"> Terms</a>';
                Session::put('error_msg', 'Your Account has been Blocked. Please read our ' . $url, 'danger');
                return "blocked";
            }
        } else {
            Auth::logout();
            Session::flush();
            return "deleted";
        }
    }

    public function getEmailTemplateInfo($template_title, $lang_id, $reserved_words) {

        $lang_id = (Session::get('land_id') ? Session::get('land_id') : 17);
        // gather information for database table
        $template_data = CommonModel::getRecords('mst_email_templates', '*', array("email_template_title" => $template_title, "lang_id" => $lang_id));

        $content = $template_data[0]->email_template_content;
        $subject = $template_data[0]->email_template_subject;

        // replace reserved words if any
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        return array("subject" => $subject, "content" => $content);
    }

    public function getprivileges($role_id) {
        $user_privileges = array();
        $arr_privileges = DB::table('trans_role_privileges')->select('privilege_id')
                ->where('trans_role_privileges.role_id', '=', $role_id)
                ->get();
        if (count($arr_privileges) > 0) {
            foreach ($arr_privileges as $privilege) {
                $user_privileges[] = $privilege->privilege_id;
            }
        }

        return $user_privileges;
    }

    public function loadStates($countr_id) {
        
    }

    public function getAllLanguages() {
        $result = DB::table('mst_languages as role')
                ->where('status', '=', 'A')
                ->where('lang_id', '!=', 17)
                ->get();
        return $result;
    }

}
