<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowerUser extends Model
{
    protected $table = 'trans_follow_user';
    protected $fillable = array('user_id_fk','follower_id_fk','status','created_at','updated_at');
}
