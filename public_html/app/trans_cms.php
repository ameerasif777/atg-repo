<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trans_cms extends Model {

    protected $table = 'trans_cms';
    protected $fillable = array('cms_id_fk', 'page_title', 'page_content', 'page_seo_title_lang', 'page_meta_keyword', 'page_meta_description', 'lang_id', 'created_at', 'updated_at');

}
