<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class meetupCommentModel extends Model
{
    public $table ="trans_meetup_comments";
}
