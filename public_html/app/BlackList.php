<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mst_blacklist_username';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  
}