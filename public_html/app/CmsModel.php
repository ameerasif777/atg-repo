<?php


namespace App;

use DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class CmsModel extends Model {

    function getCmsPageDetailsFront($condition) {
        $result = DB::table('mst_cms as A')
                ->leftjoin('trans_cms as B', 'A.id', '=', 'B.id')
                ->select('A.*', 'B.*')
                ->where($condition)
                ->get();
        return $result;
    }
//
//    public function getCmsList($lang_id) {
//        $result = DB::table('mst_cms')
//                ->leftjoin('trans_cms', 'trans_cms.id', '=', 'mst_cms.id')
//                ->leftjoin('mst_languages', 'mst_languages.lang_id', '=', 'trans_cms.lang_id')
//                ->where('trans_cms.lang_id', '=', $lang_id)
//                ->get();
//        return $result;
//    }
//
//    function getCmsById($id, $lang_id) {
//        $result = DB::table('mst_cms')
//                ->leftjoin('trans_cms', 'trans_cms.id', '=', 'mst_cms.id')
//                ->where('mst_cms.id', '=', $id)
//                ->where('trans_cms.lang_id', '=', $lang_id)
//                ->get();
//        return $result;
//    }
//
//    public function updteCmsContent($cms_data_array, $id, $lang_id) {
//        $result = DB::table('trans_cms')
//                ->where('id', '=', $id)
//                ->where('lang_id', '=', $lang_id)
//                ->update($cms_data_array);
//        return $result;
//    }

}
