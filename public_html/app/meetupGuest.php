<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class meetupGuest extends Model
{
    public $table = 'mst_meetup_guest';
}
