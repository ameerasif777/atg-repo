<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groupArticle extends Model
{
    protected $table = 'trans_article_group';
    protected $fillable = array('article_group_id','group_id_fk','article_id_fk');
}
