<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class email_template_macros extends Model
{
    protected $table = 'mst_email_template_macros';
    protected $fillable = ['macros','value'];
}
