<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class education_model extends Model {

    protected $table = 'mst_education';
    protected $fillable = array('id', 'title','description','cost','currency');
    
    function getEducationUser(){
        return $this->hasMany('App\User','id','user_id_fk');
    }
    
}

class groupEducation extends Model {
    
    public $table = "trans_education_group";
    public $fillable =array('education_group_id','education_id_fk','group_id_fk');
}
class educationCost extends Model
{
    public $table="mst_education_cost";
}
class commentEducation extends Model
{
    public $table = 'trans_education_comments';
    public $field = array('education_id_fk','commented_by','commented_user_id','comment','comment_on');
}
