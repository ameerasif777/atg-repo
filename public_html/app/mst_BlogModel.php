<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mst_BlogModel extends Model
{
     protected $table = 'mst_blogs';
    protected $fillable = array('id','user_id_fk','title','comment','tags','created_at','updated_at');

}
