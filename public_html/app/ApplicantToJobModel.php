<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantToJobModel extends Model
{
    protected $table = 'trans_user_applicant_to_job';
    protected $fillable = array('applicant_id','user_id_fk','job_id_fk');
}
