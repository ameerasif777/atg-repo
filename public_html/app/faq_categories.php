<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faq_categories extends Model
{
    protected $table ='mst_faq_categories';
    protected $fillable = array('title');
}
