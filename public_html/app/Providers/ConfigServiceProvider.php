<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Register the App Services.
     *
     */
   Public function register()
           {
               Config([
                   'services.twitter.redirect'=>url('/').'/twitter-callback',
                   'services.google.redirect'=>url('/').'/google-callback',
                   'services.facebook.redirect'=>url('/').'/facebook_callback',
                   'services.linkedin.redirect'=>url('/').'/linkedin_callback',
               ]);
           }

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot()
    {

        //
    }
}
