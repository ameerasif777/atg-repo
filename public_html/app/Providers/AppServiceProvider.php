<?php
//use View;
namespace App\Providers;

use App\global_settings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        $data['absolute_path'] = $this->absolutePath();
        $global = DB::table('mst_global_settings as global')
                ->join('trans_global_settings as trans_global', 'global.id', '=', 'trans_global.global_name_id')
                ->select(array('global.name', 'trans_global.value'))
                ->get();

        $global_values = array();
        if (count($global) > 0) {
            foreach ($global as $data_val) {
                $global_values[$data_val->name] = $data_val->value;
            }
        }

        view::share('global_values', $global_values);
        $awss3bucketurl='https://s3.ap-south-1.amazonaws.com/atg-test-s3';
        \Config::set('awss3bucketurl',$awss3bucketurl);
        \Config::set('profile_pic_url', $awss3bucketurl.'/assets/Frontend/user/profile_pics/');
        \Config::set('avatar_male',  $awss3bucketurl.'/assets/Frontend/img/avtar.png');
        \Config::set('avatar_female',  $awss3bucketurl.'/assets/Frontend/img/avtar_female.png');
        \Config::set('profile_path', 'assets/Frontend/user/profile_pics/');
        \Config::set('feature_pic_url',$awss3bucketurl.'/assets/Frontend/images/');
        \Config::set('cover_image_url',$awss3bucketurl.'/assets/Frontend/user/cover_photo/');
        \Config::set('feature_image_path', 'assets/Frontend/images/');
        \Config::set('cover_path', 'assets/Frontend/user/cover_photo/');
        \Config::set('img',$awss3bucketurl.'/assets/Frontend/img/');
        \Config::set('backend_img',$awss3bucketurl.'/assets/Backend/img/');
        \Config::set('img_path','/assets/Frontend/img/');
        \Config::set('backend_img_path','/assets/Backend/img/');
        \Config::set('resume', 'assets/Frontend/user/resume/');
        \Config::set('resume_url',$awss3bucketurl.'/assets/Frontend/user/resume/');
        \Config::set('ims_attachment',$awss3bucketurl.'/assets/Frontend/ims-attachment/');
        \Config::set('backend_group_path','/assets/Backend/img/group_img/');
        \Config::set('backend_group_url',$awss3bucketurl.'/assets/Backend/img/group_img/');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
       $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
	   $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
       $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    }

    public function absolutePath($path = '') {
        $abs_path = str_replace('system/', $path, base_path());
        //Add a trailing slash if it doesn't exist.
        $abs_path = preg_replace("#([^/])/*$#", "\\1/", $abs_path);
        return $abs_path;
    }

}
