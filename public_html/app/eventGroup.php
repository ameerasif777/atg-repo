<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGroup extends Model {

    protected $table = 'trans_event_group';

}
