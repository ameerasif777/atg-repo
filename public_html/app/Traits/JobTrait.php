<?php

namespace App\Traits;
use App\CommonModel;
use DB;
use Session;
use App\group;
use App\ApplicantToJobModel;
use App\Helpers\GlobalData;
use App\Http\Controllers\SitemapController;
use App\JobGroupModel;
use App\JobModel;
use App\RegionalPostUrlModel;
use App\saveUserPost;
use App\User;
use App\UserActivity;
use Auth;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mail;

trait JobTrait
{
    public function postJobfx($jobDetailArr, $user_id, $status, $data='')
    {
		$job_object = JobModel::where('id', $jobDetailArr['id'])->first();
		if (count($job_object) <= 0) {
            $job_object = new JobModel;
		}

        $job_object->title = $jobDetailArr['title'];
        $job_object->cmp_name = $jobDetailArr['cmp_name'];
        $job_object->job_location = $jobDetailArr['location'];
        $job_object->latitude = $jobDetailArr['latitude'];
        $job_object->longitude = $jobDetailArr['longitude'];
        $job_object->user_id_fk = $user_id;
        $job_object->description = $jobDetailArr['description'];
        $job_object->min = $jobDetailArr['min'];
        $job_object->preferred_qualification = $jobDetailArr['qualification'];
        $job_object->tags = $jobDetailArr['tags'];
        $job_object->external_link_to_apply = $jobDetailArr['link'];
        $job_object->employment_type = (isset($jobDetailArr['employment_type']) ? $jobDetailArr['employment_type'] : '');
        $application_deadline = ($jobDetailArr['end_date'] ? $jobDetailArr['end_date'] : date('Y-m-d', strtotime("+30 days")));
        $job_object->application_deadline = $application_deadline;
        $job_object->website = (isset($jobDetailArr['website']) ? $jobDetailArr['website'] : '');
        $job_object->status = $status;
		$job_object->save();
		$lastInsertId = $job_object['id'];

		return $lastInsertId;
	}

    public function createSEOfx($jobDetailArr)
    {
    	/* This fx creates SEO URLs for groups by city and country names */
		if (isset($jobDetailArr['sub_groups'])) {
			if (isset($jobDetailArr['location'])) {
				$result = array_map('strrev', explode(',', strrev($jobDetailArr['location'])));
				$city = ucwords(trim($result[0]));

				foreach ($jobDetailArr['sub_groups'] as $key => $val) {
					$groupdata = Group::where('id', $val)->get();

					$existingurlcity = RegionalPostUrlModel::where('city_country', $city)
						->where('group', $groupdata[0]['group_name'])
						->where('posts', 'Jobs')->get();
					if (count($existingurlcity) <= 0) {
						$addurl = new RegionalPostUrlModel();
						$addurl->posts = 'Jobs';
						$addurl->group = $groupdata[0]['group_name'];
						$addurl->city_country = $city;
						$addurl->url = 'go/' . $groupdata[0]['group_name'] . '/Jobs/CI/' . $city;
						$addurl->save();
					}
				}
				$sitemap = new SitemapController();
				$sitemap->index();
			}
		}
	}

    public function createUserActivityRecordfx($jobDetailArr, $jobID, $user_id)
    {	
    	/* Create a row in UserActivity table that user posted this job */

    	$user_details = $arr_user = User::find($user_id);

		$subject = 'posted a job - "' . $jobDetailArr['title'] . '"';
		$message = $user_details['first_name'] . ' ' . $user_details['last_name'] . ' posted a job - "' . $jobDetailArr['title'] . '"';
		$link = '/view-job/' . $jobID;

		$User_activity = new UserActivity;
		$User_activity->user_id_fk = $user_id;
		$User_activity->activity_id = $jobID;
		$User_activity->activity_title = $subject;
		$User_activity->activity_link = $link;
		$User_activity->activity_table_name = 'mst_job';
		$User_activity->activity_action = 'posted job';
		$User_activity->save();
		
    }
}