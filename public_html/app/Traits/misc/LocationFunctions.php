<?php namespace App\Traits\misc;

use DB;
use App\CommonModel;

trait LocationFunctions {

	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::                                                                         :*/
	/*::  This routine calculates the calls the Google API to get latitude &     :*/
	/*::  longitude of a given location. For additional accuracy, pass as        :*/
	/*::  much information about the place as possible.						     :*/
	/*::                                                                         :*/
	/*::  Example :                                                              :*/
	/*::    Paris will return different lat/long than Paris,US                   :*/
	/*::    Aurangabad will return different lat/long than Aurangabad, Bihar     :*/
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	public function getLatLongOfLocation($location_str) {

		try {

			$client = new \GuzzleHttp\Client();

			$url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $location_str . "&key=" . env("G_MAPS_KEY");
			$res = $client->get($url);

			if ($res->getStatusCode() == 200) {
				$json_res = json_decode($res->getBody(), true);

				if ($json_res['status'] == "ZERO_RESULTS") {
					return $arrayName = array('status' => 0);
				} else {
					return $arrayName = array(
						'status' => 1,
						'lat' => $json_res['results'][0]['geometry']['location']['lat'],
						'lon' => $json_res['results'][0]['geometry']['location']['lng']
						 );
				}

			} else {
				throw new \Exception("Google API did not return 200");
			}

		} catch (\Exception $e) {
			\Log::alert("GOOGLE API is failing while we were trying to get latitude & longitude from location name");
			\Log::alert($e->getMessage());
			return $arrayName = array('status' => 0);
		}


	}
}
