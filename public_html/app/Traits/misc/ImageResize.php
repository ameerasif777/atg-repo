<?php 
namespace App\Traits\misc;

use DB;
use App\CommonModel;
use Illuminate\Filesystem\Filesystem;
use Intervention\Image\Facades\Image as Image;

trait ImageResize {
    
    public function createThumbnail($feature, $imageurl)
    {
        if($feature != '' && $imageurl!= '')
        {
            $random_no = rand(1000, 9999999);
            $destinationPath = public_path() . "/assets/Frontend/images/".$feature."_image";
            //$destinationPath = public_path() . "\\assets\\Frontend\\images\\".$feature."_image";
            $obj = new Filesystem();
            if (!$obj->exists($destinationPath)) {
                    $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
            }
            try {
                    $extension = pathinfo($imageurl, PATHINFO_EXTENSION);
                    if($extension != '')
                    {
                        $extension = explode('?', $extension)[0];
                    }
                    else
                    {
                        throw new \Exception("No Extension available");;
                    }
                    //$extension = Input::file('profile_pic')->getClientOriginalExtension(); // getting image extension
                    $date_min_sec = date('msuY');
                    $fileName = 'atg-world-' . $date_min_sec . $random_no . '.' . $extension; // renameing image

                    $image = \Image::make($imageurl);

                    // save original
                    $image->save($destinationPath . '/' . $fileName);
                    //resize
                    $image->resize(300, null, function ($constraint) {
                            $constraint->aspectRatio();
                    });

                    $destinationPath = public_path() . "/assets/Frontend/images/".$feature."_image/thumb/";
                    $obj = new Filesystem();
                    if (!$obj->exists($destinationPath)) {
                            $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                    }
                    // save resized
                    $image->save($destinationPath . '/' . $fileName);
                    return $fileName;
            } catch (Exception $e) {
                    Session::flash('error_msg', 'Something went wrong! Please try a different image or retry later.');
                    return '';
            }
        }
    }
}

