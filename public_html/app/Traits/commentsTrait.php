<?php

namespace App\Traits;

use Auth;
use App\CommonModel;
use App\ArticleModel;
use App\Event;
use App\meetup_model;
use App\education_model;
use App\JobModel;
use App\Answer;
use Validator;
use App\User;
use App\questionCommentModel;
use App\QuestionModel;
use App\UserActivity;
use App\articleCommentModel;
use App\eventCommentModel;
use App\meetupCommentModel;
use App\jobCommentModel;
use App\educationCommentModel;
// use App\Traits\Post\PostActions;
use Illuminate\Support\Facades\Input;
use Illuminate\Filesystem\Filesystem;
use App\Comment;
use Mail;
use DB;
use Exception;
use Session;
/**
 * 
 */
trait commentsTrait
{
    
    public function commentfx($type,$postid,$user_id=NULL){

        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        if(isset($user_id) && $user_id!=NULL){
            $abtuser=User::find($user_id);
            $data['user_session']=['id'=>$user_id,'first_name'=>$abtuser->first_name, 'last_name'=>$abtuser->last_name];
        } else {
            $data['user_session'] = Session::get('user_account')->toArray();
        }

        $request = Input::all();
        if ($type=="question" || $type=="qrious") {
            if (!(empty($request))) {
                /* answer is for web and comment is for API */
                $request['answer']=isset($request['answer'])?$request['answer']:$request['comment'];

                // $validate_response = Validator::make($request, array(
                //             'answer' => 'required'
                //     )
                // );
                // if ($validate_response->fails()) {
                //     return json_encode(['error_code' => 1, 'msg' => 'Invalid Arguments', 'errmsg' => $validate_response]);
                // }


                /** ************************* */
                $answer = new Answer();
                $answer->answered_by = $data['user_session']['id'];
                $answer->answer = $request['answer'];
                $answer->answer_on = date("Y/m/d");
                $answer->status = '1';
                $answer->save();
                $last_answer_id = $answer->id;
                $updatedComment = $request['answer'];

                // update answer and send notifications to tagged users.
                if(isset($request['mention-ids'])){
                    $userArray = User::select(\DB::raw('concat(first_name, " ", last_name) as name'), 'id')
                                    ->whereIn('id', explode(',', rtrim($request['mention-ids'], ',')))
                                    ->get()->toArray();

                    $filteredUsers = [];
                    if (count($userArray) > 0) {
                        foreach ($userArray as $userName) {
                            if (substr_count($request['answer'], $userName['name']) > 0) {
                                $filteredUsers[] = $userName['id'];
                                $uProfileLink = '<a href="' . url('/user-profile/' . base64_encode($userName['id'])) . '">' . $userName['name'] . '</a>';
                                $updatedComment = str_replace($userName['name'], $uProfileLink, $updatedComment);
                            }
                        }
                    }
                    // notify filtered connected users..
                    if (count($filteredUsers) > 0) {
                        foreach ($filteredUsers as $fuser) {
                            $this->NotifyTaggedUsersfx($data['user_session'], $fuser, $postid, $last_answer_id,$type);
                        }
                    }
                }
                // filter @ contained usernames..
                $answerArr = explode(' ', $request['answer']);
    
                $taggedUsernames = [];
                if (count($answerArr) > 0) {
                    foreach ($answerArr as $answer) {
                        if (substr_count($answer, '@') > 0) {
                            $taggedUsernames[] = ltrim($answer,'@');
                        }
                    }
                    // notify tagged usernames which are not in connections..
                    if (count($taggedUsernames) > 0) {
                        foreach ($taggedUsernames as $tagUser) {
                            $userInfo = DB::table('mst_users')->select(\DB::raw('concat(first_name, " ", last_name) as name'), 'id')
                                    ->where('user_name', $tagUser)->first();
                            if (count($userInfo) > 0) {
                                $uProfileLink = '<a href="' . url('/user-profile/' . base64_encode($userInfo->id)) . '">' . $userInfo->name . '</a>';
                                $updatedComment = str_replace($userInfo->name, $uProfileLink, $updatedComment);
                                $this->NotifyTaggedUsersfx($data['user_session'], $userInfo->id, $request['post_question_id'], $last_answer_id,$type);
                            }
                        }
                    }
                }
    
                $commentObj = Answer::find($last_answer_id);
                $commentObj->answer = $updatedComment;
                $commentObj->save();
    
                $question_answer = new questionCommentModel();
                $question_answer->answered_by = $data['user_session']['id'];
                $question_answer->answer_id_fk = $last_answer_id;
                $question_answer->question_id_fk = $postid;
                $question_answer->status = '1';
                if (isset($request['answer_pic']) != '') {
    
                    if (Input::hasFile('answer_pic')) {
    
                        $destinationPath = public_path() . "/assets/Frontend/images/question_answer_images/";
                        $obj = new Filesystem();
                        if (!$obj->exists($destinationPath)) {
                            $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                        }
                        try {
                            $extension = Input::file('answer_pic')->getClientOriginalExtension(); // getting image extension
                            $fileName = rand(); // renameing image
    
                            $image = \Image::make(\Input::file('answer_pic'));
    
                            // save original
                            $image->save($destinationPath . '/' . $fileName);
                            $question_answer->answer_image = $fileName;
                            //resize
                            $image->resize(300, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                            $destinationPath = public_path() . "/assets/Frontend/images/question_answer_images/thumb";
                            $obj = new Filesystem();
                            if (!$obj->exists($destinationPath)) {
                                $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                            }
                            // save resized
                            $image->save($destinationPath . '/' . $fileName);
                        } catch (Exception $e) {
                        return json_encode(['error_code' => 2, 'msg' => 'Invalid Image', 'errmsg' => 'Please try a different image or retry later']);
                        }
                    }
                }
                $question_answer->save();
    
                if($question_answer->id != ''){
                    $question = QuestionModel::find($postid);
                    $subject = 'given an answer to the question - "' . $question->title . '"';
                    $link = '/view-question/'. $postid.'#'.$last_answer_id;
    
                    $User_activity = new UserActivity;
                    $User_activity->user_id_fk = $data['user_session']['id'];
                    $User_activity->activity_id = $postid;
                    $User_activity->activity_title = $subject;
                    $User_activity->activity_link = $link;
                    $User_activity->activity_table_name = 'trans_question';
                    $User_activity->activity_action = 'commented question';
                    $User_activity->save();
                }

                // notify following users.
                $this->PostCommentNotificationMailfx($data['user_session'], $postid, $last_answer_id, 'question', $data['user_session']['id']);
                // Session::flash('success_msg', 'Your answer has been added.');
                // return redirect('view-question/' . $request['post_question_id']);
            }
        }
        else{

            if (isset($request['comment']) && $request['comment'] == "" && $request['reply'] != "") {
                $request['comment'] = $request['reply'];
            }
            if (!(empty($request))) {
                /*             * **server validation** */
                if (isset($request['reply']) && $request['reply'] != "") {
                    $validate_response = Validator::make($request, array(
                                'reply' => 'required'
                                    )
                    );
                    $comments = $request['reply'];
                } else {
                    $validate_response = Validator::make($request, array(
                                'comment' => 'required'
                                    )
                    );
                    $comments = $request['comment'];
                }
                if ($validate_response->fails()) {
                    return json_encode(['error_code' => 1, 'msg' => 'Invalid Arguements', 'errmsg' => $validate_response]);
                }
                /*             * ************************* */
                $comment = new Comment();
                $comment->commented_by = $data['user_session']['id'];
                $comment->comment = $comments;
                if (isset($request['parent_id']) && $request['parent_id'] != "") {
                    $comment->parent_id = $request['parent_id'];
                }
                if (isset($request['r_parent_id']) && $request['r_parent_id'] != "") {
                    $comment->r_parent_id = $request['r_parent_id'];
                }
                $comment->comment_on = date("Y/m/d");
                $comment->status = '1';
                $comment->save();
                $last_comment_id = $comment->id;
                $updatedComment = $comments;

                // update comment and send notifications to tagged users
                $userArray=[];
                if(isset($request['mention-ids'])){
                $userArray = User::select(\DB::raw('concat(first_name, " ", last_name) as name'), 'id')
                                ->whereIn('id', explode(',', rtrim($request['mention-ids'], ',')))
                                ->get()->toArray();
                }
                $filteredUsers = [];
                if (count($userArray) > 0) {
                    foreach ($userArray as $userName) {
                        if (isset($request['reply']) && $request['reply'] != "") {
                            if (substr_count($request['reply'], $userName['name']) > 0) {
                                $filteredUsers[] = $userName['id'];
                                $uProfileLink = '<a href="' . url('/user-profile/' . base64_encode($userName['id'])) . '">' . $userName['name'] . '</a>';
                                $updatedComment = str_replace($userName['name'], $uProfileLink, $updatedComment);
                            }
                        } else {
                            if (substr_count($request['comment'], $userName['name']) > 0) {
                                $filteredUsers[] = $userName['id'];
                                $uProfileLink = '<a href="' . url('/user-profile/' . base64_encode($userName['id'])) . '">' . $userName['name'] . '</a>';
                                $updatedComment = str_replace($userName['name'], $uProfileLink, $updatedComment);
                            }
                        }
                    }
                }
                // notify filtered connected users..
                if (count($filteredUsers) > 0) {
                    foreach ($filteredUsers as $fuser) {
                        $this->NotifyTaggedUsersfx($data['user_session'], $fuser, $postid, $last_comment_id,$type);
                    }
                }
                // filter @ contained usernames..
                if (isset($request['reply']) && $request['reply'] != "") {
                    $commentArr = explode(' ', $request['reply']);
                } else {
                    $commentArr = explode(' ', $request['comment']);
                }
                $taggedUsernames = [];
                if (count($commentArr) > 0) {
                    foreach ($commentArr as $comment) {
                        if (substr_count($comment, '@') > 0) {
                            $taggedUsernames[] = $comment;
                        }
                    }
                    // notify tagged usernames which are not in connections..
                    if (count($taggedUsernames) > 0) {
                        foreach ($taggedUsernames as $tagUser) {
                            $userInfo = DB::table('mst_users')->select(\DB::raw('concat(first_name, " ", last_name) as name'), 'id')
                                            ->where('user_name', ltrim($tagUser, '@'))->first();
                            if (count($userInfo) > 0) {
                                $uProfileLink = '<a href="' . url('/user-profile/' . base64_encode($userInfo->id)) . '">' . $userInfo->name . '</a>';
                                $updatedComment = str_replace($userInfo->name, $uProfileLink, $updatedComment);
                                $this->NotifyTaggedUsersfx($data['user_session'], $userInfo->id, $postid, $last_comment_id, $type);
                            }
                        }
                    }
                }

                $commentObj = Comment::find($last_comment_id);
                $commentObj->comment = $updatedComment;
                $commentObj->save();

                if($type=="article")
                {
                    $type_comment = new articleCommentModel();
                    $type_comment->article_id_fk= $postid;
                }
                elseif($type=='event')
                {
                    $type_comment = new eventCommentModel();
                    $type_comment->event_id_fk= $postid;
                }
                elseif($type=='education'){
                    $type_comment = new educationCommentModel();
                    $type_comment->education_id_fk = $postid;
                }
                elseif($type=='meetup')
                {    
                    $type_comment = new meetupCommentModel();
                    $type_comment->meetup_id_fk = $postid;
                }
                elseif($type=='job')
                {   
                    $type_comment= new jobCommentModel();
                    $type_comment->job_id_fk = $postid;
                }
                
                $type_comment->commented_by = $data['user_session']['id'];
                $type_comment->comment_id_fk = $last_comment_id;
                $type_comment->status = '1';
                if (isset($request['commnet_pic']) != '' || $type=='article') {

                    if (Input::hasFile('commnet_pic')) {
    
                        $destinationPath = public_path()."/assets/Frontend/images/article_comment_images/";
                        $obj = new Filesystem();
                        if (!$obj->exists($destinationPath)) {
                            $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                        }
                        try {
                            $extension = Input::file('commnet_pic')->getClientOriginalExtension(); // getting image extension
    //                        $fileName = Input::file('commnet_pic')->getClientOriginalName(); // renameing image
                            $fileName = rand(); // renameing image
    
                            $image = \Image::make(\Input::file('commnet_pic'));
    
                            // save original
                            $image->save($destinationPath . '/' . $fileName);

                            $type_comment->comment_image = $fileName;
                            //resize
                            $image->resize(300, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                            $destinationPath = public_path() . "/assets/Frontend/images/article_comment_images/thumb";
                            
                            
                            if (!$obj->exists($destinationPath)) {
                                $result = $obj->makeDirectory($destinationPath, intval(0777), true, true);
                            }
                            // save resized
                            $image->save($destinationPath . '/' . $fileName);
                        } catch (Exception $e) {
                        return json_encode(['error_code' => 2, 'msg' => 'Invalid Image', 'errmsg' => 'Please try a different image or retry later']);
                        }
                    }

                }
                $type_comment->save();

                $this->PostCommentNotificationMailfx($data['user_session'], $postid, $last_comment_id, $type); //notifyfollowing

                if($type_comment->id != ''){

                    if($type=="article")
                    {
                        $post = ArticleModel::find($postid);
                    }
                    elseif($type=='event')
                    {
                        $post = Event::find($postid);
                    }
                    elseif($type=='education'){
                        $post = education_model::find($postid);
                    }
                    elseif($type=='meetup')
                    {    
                        $post = meetup_model::find($postid);
                    }
                    elseif($type=='job')
                    {   
                        $post = JobModel::find($postid);
                    }
                            
                    $subject = 'commented on '.$type.' - "' . $post->title . '"';
                    $link = '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $post->title) . '-' . $postid.'#'.$last_comment_id;

                    if($type=="education"){
                        $acttablename='trans_'.$type;
                    }
                    else{
                        $acttablename='mst_'.$type;
                    }
                    $acttablename='mst_'.$type;
                    $User_activity = new UserActivity;
                    $User_activity->user_id_fk = $data['user_session']['id'];
                    $User_activity->activity_id = $postid;
                    $User_activity->activity_title = $subject;
                    $User_activity->activity_link = $link;
                    $User_activity->activity_table_name = $acttablename;
                    $User_activity->activity_action = 'commented '.$type;
                    $User_activity->save();
                }

            
                    
            
            }
            
        }
        if ($type=='qrious' || $type=='question') { $type='question';
            $allcomments = DB::table('mst_answers as ma')
                ->join('trans_'.$type.'_answers as ttc', 'ma.id', '=', 'ttc.answer_id_fk', 'left')
                ->join('mst_users as u', 'u.id', '=', 'ma.answered_by', 'left')
                ->where('ttc.'.$type.'_id_fk', $postid)
                ->orderBy('id', 'DESC')
                ->select('ma.*', 'u.user_name', 'u.profile_picture')
                ->get();
        } else {
            $allcomments = DB::table('mst_comments as mc')
                ->join('trans_'.$type.'_comments as ttc', 'mc.id', '=', 'ttc.comment_id_fk', 'left')
                ->join('mst_users as u', 'u.id', '=', 'mc.commented_by', 'left')
                ->where('ttc.'.$type.'_id_fk',$postid)
                ->orderBy('id', 'DESC')
                ->select('mc.*', 'u.user_name', 'u.profile_picture')
                ->get();
        }

        return json_encode(['error_code'=>0, 'msg'=>'success','comment_count'=>count($allcomments),'comments'=>$allcomments]);
    }
    

  
}
