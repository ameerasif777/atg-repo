<?php namespace App\Traits;
/**
 * User: Tanima Gupta
 * Date: 13/1/2018
 * Time: 5:35 PM
 */
ini_set('max_execution_time', 300); 
use DB;
use App\CommonModel;
use Illuminate\Support\Facades\Mail;
use App\email_templates;
use App\email_template_macros;
use App\ImsModel;
use App\ImsRepliesModel;
use App\User;
use Illuminate\Support\Facades\Log;


trait sendMail {

    public function sendMailToMultipleUsers($userids, $email_temp_id, $email_temp_subj, $email_temp_content)
    {
       // $userids = "1819,1820,1827,1851,455,1967,2061,2273,2274,2277,2280,2281,2282,2286,2287,2288,2289,2291,2292,2293";
        Log::useDailyFiles(storage_path().'/logs/mail-daily.log');
        $userids = explode(',',$userids);
        $user_arr = User::whereIN('id',$userids)->get(['id','user_name', 'email','first_name','last_name']);
        $email = "hello@atg.world";
        foreach ($user_arr as $key => $value) {
            if ($value['email']) {
                $reserved_arr = array(
                    "||SITE_TITLE||" => stripslashes("ATG.WORLD"),
                    "||SITE_PATH||" => url('/'),
                    "||USER_NAME||" => $value['user_name'] ,
                    "||USER_EMAIL||" => $value['email'],
                    "||FIRST_NAME||" => $value['first_name'],
                    "||LAST_NAME||" => $value['last_name']
                );
                
                $content = $email_temp_content;
                $subject = $email_temp_subj;
                foreach ($reserved_arr as $k => $v) {
                    $content = str_replace($k, $v, $content);
                    $subject = str_replace($k, $v, $subject);
            
                }

                $contents_array = array("email_conents" => $content);

                //Log::info(['Request'=>'Group Email', 'UserID' => $value['id'],'email_template_subject' => $subject, 'email_content' => $contents_array]);
                
                try {
                    Mail::send('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($email, $subject, $value) {
                                $message->from($email, "ATG.WORLD");
                                $message->to($value['email'], $value['user_name'])->subject($subject);
                    });        
                    \Log::info("Promotional Email sent to ". $value['email']); 
                } catch (\Exception $e) {
                    \Log::alert("PROMOTIONAL EMAIL NOT SENT to " . $value['email']);
                    \Log::info($e);
                }
            }
            sleep(1);   //Important - AWS has sending limits per second
        }
    }

    /*
     * Thankyou mail to user for purchasing ticket
     */
    public function sendMailToUser($name,$email, $tcount, $title, $id, $cost, $tcost, $tax) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $email_contents = email_templates::where(array('email_template_title' => 'ticket-purchase-confirmation', 'lang_id' => '14'))->first();
        $mailcontent=$email_contents['email_template_content'];
        $reserved_arr = array(
            "||Name||" => ucwords($name),
            "||value||" => $tcount,
            "||cost||" => $cost,
            "||cost1||" => $cost*$tcount,
            "||currency||" => '&#x20B9;',
            "||gst||" =>  $tax,
            "||totalcost||" => $tcost + $tax,
            "||event_name||" => $title,
            "||event_link||" => url('/') . "/event-detail/" . $title . '-' . $id,
            "||SITE_PATH||" => url('/'),
        );

        foreach ($reserved_arr as $k => $v) {
            $mailcontent = str_replace($k, $v, $mailcontent);
        }

        $mailcontent_array = array("email_conents" => $mailcontent);

        Mail::queue('Frontend.email-contents.email-contents', $mailcontent_array, function($message)use($name, $email, $data, $email_contents) {
            $message->from($data['global']['contact_email'], $data['global']['site_title']);
            $message->to($email, ucwords($name))->subject($email_contents['email_template_subject']);
        });


        return 1;
    }


    /*
     * Ticket purchase notification to organizer
     */
    public function sendMailToOrganizer($org_name,$email, $user_name, $tcount, $category, $user_email, $user_mob_no, $title, $eventid, $user_id, $profile_picture, $org_userid) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $email_contents = email_templates::where(array('email_template_title' => 'ticket-purchase-org-notification', 'lang_id' => '14'))->first();
        $mailcontent=$email_contents['email_template_content'];
        $reserved_arr = array(
            "||Name||" => ucwords($org_name),
            "||User||" => ucwords($user_name),
            "||value||" => $tcount,
            "||category||" => $category,
            "||user_profile||" => url('/') . "/user-profile/" . base64_encode($user_id),
            "||Email||" => $user_email,
            "||Username||" => $user_name,
            "||Mobile||" => $user_mob_no,
            "||event_name||" => $title,
            "||event_link||" => url('/') . "/view-".$category."/" . $title . '-' . $eventid,
            "||SITE_PATH||" => url('/'),
            "||profile_pic||" => url('/') . "/public/assets/Frontend/user/profile_pics/1999/thumb/" . $profile_picture,
        );

        if (empty($user_email)) {
            $reserved_arr["||Email||"] = "No email";
        }

        if (empty($user_mob_no)) {
            $reserved_arr["||Mobile||"] = "No mobile no.";
        }

        foreach ($reserved_arr as $k => $v) {
            $mailcontent = str_replace($k, $v, $mailcontent);
        }

        $mailcontent_array = array("email_conents" => $mailcontent);

        Mail::queue('Frontend.email-contents.email-contents', $mailcontent_array, function($message)use($org_name, $email, $data, $email_contents) {
            $message->from($data['global']['contact_email'], $data['global']['site_title']);
            $message->to($email, ucwords($org_name))->subject($email_contents['email_template_subject']);
        });


        $this->addNotification($user_id, $email_contents['email_template_subject'], $org_userid, $mailcontent);

        return 1;
    }


    /*
     * To add notification to organizers profile
     */
    public function addNotification($user_id, $title, $org_userid, $mailcontent)
    {
        $ims_data = ImsModel::where([['user_id_fk', '=', $user_id], ['title', '=', $title]])->first();
        $replies_object = new ImsRepliesModel();
        $replies_object->message_type = '0';
        $id = '';
        if (!$ims_data) {
            $ims_object = new ImsModel();
            $ims_object->user_id_fk = $user_id;
            $ims_object->title = $title;
            $ims_object->save();
            $id = $ims_object->id;
        }
        else
        {
            $id = $ims_data->id;
        }

        $check = ImsRepliesModel::where([['message_id_fk', '=', $id], ['message_from', '=', $user_id], ['message_to', '=', $org_userid]])->first();

        if ($check) {
            $replies_object->message_type = '1';
        }

        $replies_object->message_id_fk = $id;
        $replies_object->message_from = $user_id;
        $replies_object->message_to = $org_userid;
        $replies_object->message = $mailcontent;
        $replies_object->attachment_name = '';
        $replies_object->is_attachment = '0';
        $replies_object->save();
    }
    
    
    /*
     * Send Promotion Information to Promoter
     */
    public function sendPromotionMailToPromoter( $tag, $name,$email, $title, $id, $type, $views, $clicks) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        if($tag == 'end')
        {
            $email_temp = 'end-promotion-data-email';
        }
        else 
        {
            $email_temp = 'daily-promotion-data-email';
        }
                
        $email_contents = email_templates::where(array('email_template_title' => $email_temp, 'lang_id' => '14'))->first();
        $mailcontent=$email_contents['email_template_content'];
        $reserved_arr = array(
            "||Name||" => ucwords($name),
            "||POST_NAME||" => $title,
            "||VIEWS||" => $views,
            "||CLICKS||" => $clicks,
            "||TOTAL_COUNT||" => intval($clicks) + intval($views),
            "||POST_LINK||" => url('/') . "/view-".$type."/" . $title . '-' . $id,
            "||SITE_PATH||" => url('/'),
            "||SITE_TITLE||" => 'ATG.WORLD'
        );

        foreach ($reserved_arr as $k => $v) {
            $mailcontent = str_replace($k, $v, $mailcontent);
            $email_contents['email_template_subject'] = str_replace($k, $v, $email_contents['email_template_subject']);
        }

        $mailcontent_array = array("email_conents" => $mailcontent);

        Mail::queue('Frontend.email-contents.email-contents', $mailcontent_array, function($message)use($name, $email, $data, $email_contents) {
            $message->from($data['global']['contact_email'], $data['global']['site_title']);
            $message->to($email, ucwords($name))->subject($email_contents['email_template_subject']);
        });


        return 1;
    }
}



