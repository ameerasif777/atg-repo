<?php namespace App\Traits;

use DB;

trait MarkAllNotificationsAsRead {

    public function MarkAllNotificationsAsReadfx($user_id) {
        $update_notification = DB::table('mst_notifications')
                ->where('notification_to', $user_id)
                ->where('read_status', '0')
                ->update(['read_status' => '1']);
        return $update_notification;
    }

}
