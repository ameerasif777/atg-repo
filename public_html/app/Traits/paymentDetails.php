<?php
/**
 * User: tanima
 * Date: 16/1/2018
 * Time: 2:30 PM
 */

namespace App\Traits;
use App\CommonModel;
use DB;
use Session;
use App\postPromotion;

Trait paymentDetails
{
    public function getIfTransactionHistory($userid)
    {
        $list = DB::table('bank_transactions')
                ->where('status', '1')
                ->where('user_id_fk', $userid)
                ->orwhere('org_user_id_fk', $userid)
                ->selectraw('count(*) as tcount')->get();
        $tcount = $list[0]->tcount;
        return $tcount;
    }
    
    public function getTransactionHistory($userid, $page, $limit)
    {
        
        $list = DB::table('bank_transactions')
                ->where('status', '1')
                ->where('user_id_fk', $userid)
                ->orwhere('org_user_id_fk', $userid)
                ->selectraw('SQL_CALC_FOUND_ROWS *')
                ->orderby('created_at', 'DESC')
                ->offset($page*$limit)
                ->limit($limit)
                ->get();
     
        $total_count = DB::select('select FOUND_ROWS() as tcount');
        $data = array();
        foreach($list as $key => $value)
        {
            $res = $this->getTransactionDetails($value->id_fk, $value->category, $value->trans_type);
            $eventname = $res[0]->title;
            $data[$key]['date'] = date_format(date_create($value->created_at), 'dS F Y');
            if($value->trans_type == 'buy-ticket')
            {
                if($value->user_id_fk == $userid)
                {
                    $data[$key]['summary'] = "Purchased ".$value->ticketCount." tickets for ".$value->category." <b>".$eventname."</b>";                         
                }
                elseif($value->org_user_id_fk == $userid)
                {
                    $data[$key]['summary'] = "Sold ".$value->ticketCount." tickets for ".$value->category." <b>".$eventname."</b>";
                }
            }
            else if($value->trans_type == 'promotion')
            {
                $data[$key]['summary'] = "Paid for promotion of ".$value->category." <b>".$eventname."</b>";                         
                
            }
            $data[$key]['dwld_link'] = url('/').'/trans-history/dwld-invoice/'.$value->transId;   
            $data[$key]['amount'] = $value->amount;
            $data[$key]['total_count'] = $total_count[0]->tcount;
                          
        }
        
        return $data;
    }
    
    public function getTransactionDetails($id_fk, $category, $trans_type='buy-ticket')
    {           
        $list = array();
        if($trans_type == 'buy-ticket' && ($category == 'event' || $category == 'meetup' || $category == 'education')) {
            $table1 = 'mst_' . $category;
            $table2 = 'trans_' . $category . '_rsvp_status';
            $col = $category . '_id_fk';

            $list = DB::table($table2 . ' as a')
                ->join($table1 . ' as b', 'a.' . $col, '=', 'b.id')
                ->where('a.id', $id_fk)
                ->select('b.*', 'a.*', 'b.id as eventid', 'b.user_id_fk as org_userid', 'a.user_id_fk as userid')
                ->get();
        }   
        else if($trans_type == 'promotion')
        {
            $table1 = 'mst_' . $category;
            $list = DB::table('trans_post_promotion as a')
                    ->join($table1.' as b', 'a.post_id_fk', '=', 'b.id')
                    ->where('a.id', $id_fk)
                    ->select('b.*')
                    ->get();
        }
        return $list;
    }
}