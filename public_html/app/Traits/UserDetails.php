<?php namespace App\Traits;

use App\CommonModel;
use DB;
use Illuminate\Support\Facades\Cache;
use App\postPromotion;
use App\FollowerUser;
use App\email_templates;
use App\email_template_macros;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\trans_global_settings;

trait UserDetails {

	public function followUnfollowUserfx ($user_id, $follower_id) {
        $commonModel = new CommonModel();
		$arr_follower = FollowerUser::where('user_id_fk', '=', $user_id)->where('follower_id_fk', '=', $follower_id)->get();
		$arr = User::find($user_id);
        $arr_f_user = User::find($follower_id);
		if (count($arr_follower) > 0) {
			FollowerUser::where('user_id_fk', '=', $user_id)->where('follower_id_fk', '=', $follower_id)->delete();
			$arr_follower = DB::table('trans_follow_user')
				->where('user_id_fk', $user_id)
				->count();
			return array('follow_status' => 0, 'follower_count' => $arr_follower);
		} else {
			$followeruser = new FollowerUser();
			$followeruser->user_id_fk = $user_id;
			$followeruser->follower_id_fk = $follower_id;
			$followeruser->status = '1';
			$followeruser->created_at = date("Y-m-d H:i:s");
			$followeruser->save();
			$subject = ' followed you.';
			$message = ucfirst($arr_f_user->first_name) . ' ' . ucfirst($arr_f_user->last_name) . ' followed you.';
			$activity_subject = 'follows';
			$activity_message =ucfirst($arr_f_user->first_name) . ' ' . ucfirst($arr_f_user->last_name) . ' follows ' . ucfirst($arr->first_name) . ' ' . ucfirst($arr->last_name) . '.';
			$insert_data = array(
				'notification_from' => $follower_id,
				'notification_to' => $user_id,
				'notification_type' => "1",
				'subject' => $subject,
				'message' => trim($message),
				'notification_status' => 'send',
				'created_at' => date('Y-m-d H:i:s'),
			);
			$last_insert_id = $commonModel->insertRow("mst_notifications", $insert_data);
			$arr_follower = DB::table('trans_follow_user')
				->where('user_id_fk', $user_id)
				->count();

	        if ($arr->getUserEmailSchedule == '' || $arr->getUserEmailSchedule->follower_notification == 1) {
	        	$this->send_follow_mail($user_id, $follower_id);
	        }

			return array('follow_status' => 1, 'follower_count' => $arr_follower);
		}
    }

	public function send_follow_mail($followed_user_id, $follower_id) {

		/* ***********Send Email Notification************ */
		$user = DB::table('mst_users')->where('id', $followed_user_id)->get()->first();

		$site_email = trans_global_settings::where('id', '1')->first();
		$site_title = trans_global_settings::where('id', '2')->first();
		/* setting reserved_words for email content */
		$macros_array_detail = array();
		$macros_array_detail = email_template_macros::all();

		$macros_array = array();
		foreach ($macros_array_detail as $row) {
			$macros_array[$row['macros']] = $row['value'];
		}

		$email_contents = email_templates::where(array('email_template_title' => 'following-notification', 'lang_id' => '14'))->first();

		$email_contents = $email_contents;
		$content = $email_contents['email_template_content'];

		$reserved_words = array();
		$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

		$user_acc = DB::table('mst_users')->where('id', $follower_id)->get()->first();

		$profile_path = url('/') . '/public/assets/Frontend/user/profile_pics/' . $user_acc->id . '/thumb/' . $user_acc->profile_picture;
		if (isset($user_acc->profile_picture) && $user_acc->profile_picture != '') {
			if ($user_acc->gender == 0) {
				$profile_path_onerror = url('/') . '/public/assets/Frontend/img/avtar.png';
			} else {
				$profile_path_onerror = url('/') . '/public/assets/Frontend/img/avtar_female.png';
			}

			$profile_image_path = $profile_path . '" onerror="' . $profile_path_onerror;
		} else {
			if ($user_acc->gender == 0) {
				$profile_path = url('/') . '/public/assets/Frontend/img/avtar.png';
				$profile_image_path = $profile_path;
			} else {
				$profile_path = url('/') . '/public/assets/Frontend/img/avtar_female.png';
				$profile_image_path = $profile_path;
			}
		}

		$reserved_arr = array(
			"||SITE_TITLE||" => stripslashes($site_title->value),
			"||SITE_PATH||" => $site_path,
			"||USER_NAME||" => $user->first_name,
			"||USER_EMAIL||" => $user->email,
			"||SECOND_USER_NAME||" => $user_acc->first_name . " " . $user_acc->last_name,
			"||SECOND_USER_PROFILE_PATH||" => url('/') . '/user-profile/' . (base64_encode($follower_id)),
			"||SECOND_USER_PROFILE_IMAGE||" => $profile_image_path,
			"||NOTIFICATION_LINK||" => url('/') . '/notification/',
		);

		$reserved_words = array_replace_recursive($macros_array, $reserved_arr);

		foreach ($reserved_words as $k => $v) {
			$content = str_replace($k, $v, $content);
		}

		/* getting mail subect and mail message using email template title and lang_id and reserved works */
		$contents_array = array("email_conents" => $content);
		$data['site_email'] = $site_email->value;
		$data['site_title'] = $site_title->value;

		// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
		Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($contents_array, $data, $user, $email_contents) {
			$message->from($data['site_email'], $data['site_title']);
			$message->to($user->email, $user->first_name)->subject($email_contents['email_template_subject']);
		});
		
	}

	public function UpdateUserLocationfx($user_id, $platform, $latitude, $longitude, $agent= '', $cityName = '', $countryName = '', $regionName = '', $countryCode = '', $regionCode = '', $token = '') {
		$commonModel = new CommonModel();
		$update_data = array('cityName' => $cityName,
			'platform' => $platform,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'countryName' => $countryName,
			'regionName' => $regionName,
			'countryCode' => $countryCode,
			'regionCode' => $regionCode,
			'user_id' => $user_id,
			'agent' => $agent,
			'token' => $token
		);
		if ($latitude != 0 && $longitude != 0) {
			$commonModel->insertRow("trans_user_location_history", $update_data);
			return 1;
		}
		return 0;
	}

	public function HomePagePostsAlgofx($user_id, $platform, $session_id, $page, $filter_feeds_arr, $no_of_posts = '0', $location = '', $latitude = '', $longitude = '') {
		$commonModel = new CommonModel();

		$get_connections = DB::table('trans_user_connection')
			->where('from_user_id', $user_id)
			->orWhere('to_user_id', $user_id)
			->where('status', '1')
			->select('to_user_id', 'from_user_id')
			->get();

		$get_followers = DB::table('trans_follow_user')
			->where('follower_id_fk', $user_id)
			->where('status', '1')
			->get();

		$condition1 = 'NULL';
		$condition2 = 'NULL';

		if (count($get_connections) > 0) {
			$temp_array = array();
			$user_ids = array();
			foreach ($get_connections as $key => $value) {
				$user_ids[] = $value->to_user_id;
				$user_ids[] = $value->from_user_id;
				$arr_ids = array();
				foreach ($user_ids as $ids) {
					if (!in_array($ids, $arr_ids)) {
						$arr_ids[] = $ids;
					}
				}
			}
			$condition1 = implode(',', $arr_ids);
		}

		if (count($get_followers) > 0) {
			$temp_array = array();
			$user_ids = array();
			foreach ($get_followers as $key => $value) {
				$user_ids[] = $value->user_id_fk;
				$arr_ids = array();
				foreach ($user_ids as $ids) {
					if (!in_array($ids, $arr_ids)) {
						$arr_ids[] = $ids;
					}
				}
			}
			$condition2 = implode(',', $arr_ids);
		}

		$get_groups = DB::table('trans_group_users')
			->where('user_id_fk', $user_id)
			->get();

		$user_group = array();
		if (count($get_groups) > 0) {
			foreach ($get_groups as $key => $value) {
				$user_group[] = $value->group_id_fk;
			}
			$group = implode(',', $user_group);
		}

		$get_filter = DB::table('trans_user_filter')
			->where('user_id_fk', $user_id)
			->whereIn('group_id_fk', $user_group)
			->get();

		if (count($get_filter) > 0) {
			foreach ($get_filter as $key => $value) {
				$filtered_group[] = $value->group_id_fk;
			}
		}
		//Returning more posts on web platform and less on the app
		if ($no_of_posts == 0) {
			if ($platform == 'web') {
				$no_of_posts = 20;
			} else if ($platform == 'android' || $platform == 'ios' || $platform == 'app') {
				$no_of_posts = 10;
			}

		}

		// If page is not first page, we retrieve the position from cache, else we start over
		// $start_hour_of_posts_to_fetch is the created_at datetime of post from where we start fetching
		if ($page != '1' && Cache::has('homepage' . $platform . $user_id . $session_id)) {
			$start_hour_of_posts_to_fetch = Cache::get('homepage' . $platform . $user_id . $session_id);
		} else {
			$start_hour_of_posts_to_fetch = 0;
		}

		$time_interval = 96; //time diff b/w created_at of posts

		$arr_data_final = array();
        $lng_min = 0;  
        $lng_max = 0;
        $lat_min = 0;
        $lat_max = 0;
        $job_lng_min = 0;
        $job_lng_max = 0;
        $job_lat_min = 0;
        $job_lat_max = 0;

        
        if($latitude != '' && $longitude != '')   
        {
            $radius = 100; // in miles
            $lng_min = ($longitude - $radius / abs(cos(deg2rad($latitude)) * 69)); // 76.195308469925
            $lng_max = ($longitude + $radius / abs(cos(deg2rad($latitude)) * 69)); //77.843817530075
            $lat_min = ($latitude - ($radius / 69)); //27.736194318841
            $lat_max = ($latitude + ($radius / 69)); //29.185469681159

            $radius = 200; // in miles
            $job_lng_min = ($longitude - $radius / abs(cos(deg2rad($latitude)) * 69)); // 76.195308469925
            $job_lng_max = ($longitude + $radius / abs(cos(deg2rad($latitude)) * 69)); //77.843817530075
            $job_lat_min = ($latitude - ($radius / 69)); //27.736194318841
            $job_lat_max = ($latitude + ($radius / 69)); //29.185469681159
        }

		while (count($arr_data_final) < $no_of_posts) {
			$arr_all_article_data = array();
			$arr_all_education_data = array();
			$arr_all_job_data = array();
			$arr_all_event_data = array();
			$arr_all_meetup_data = array();
			$arr_all_qrious_data = array();

			if ($start_hour_of_posts_to_fetch > 3000) {
				break;
			}

			if (in_array("1", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$arr_article = DB::table('mst_article as e');
				$arr_article->join('trans_article_group as g', 'g.article_id_fk', '=', 'e.id', 'left');
				$arr_article->join('trans_follow_user as f', 'f.user_id_fk', '=', 'e.user_id_fk', 'left');
				$arr_article->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');

				$whereAndOrRange1 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_e`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";

				$arr_article->whereRaw($whereAndOrRange1);
				$arr_article->whereRaw("`p1036_e`.`status` = '1'");
				$arr_article->whereRaw("`p1036_e`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$arr_article->whereRaw("`p1036_e`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");

				$arr_article->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'e.id', DB::raw("CAST(`p1036_e`.`status` AS CHAR(50)) AS e_status"),
					'e.title as title', 'e.profile_image as image', 'e.cover_type', 'description', 'e.updated_at', 'e.created_at', DB::raw("'' AS `longitude`, '' AS `latitude`"),
					DB::raw("'Article' AS `type`"), DB::raw('(SELECT COUNT(article_id_fk)  FROM `p1036_trans_article_upvote_downvote`
                        WHERE `article_id_fk` = `p1036_e`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_article_upvote_downvote` WHERE
                        `article_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_article_upvote_downvote` WHERE
                        `article_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(article_id_fk) FROM `p1036_trans_article_upvote_downvote`
                        WHERE `article_id_fk` = `p1036_e`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(article_id_fk)  FROM `p1036_trans_article_upvote_downvote`
                        WHERE `article_id_fk` = `p1036_e`.`id` AND `status` = "0"),
                        (SELECT COUNT(article_id_fk) FROM `p1036_trans_article_upvote_downvote`
                        WHERE `article_id_fk` = `p1036_e`.`id` AND `status` = "1"),
                        `p1036_e`.`created_at`) AS hotness')
				);

				$arr_article->groupBy('e.id');
				$arr_article->orderBy('hotness', 'DESC');
				$arr_article = collect($arr_article->get());
				$arr_all_article_data = $arr_article->toArray();
			}

			if (in_array("2", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$arr_education = DB::table('mst_education as e');
				$arr_education->join('trans_education_group as g', 'g.education_id_fk', '=', 'e.id', 'left');
				$arr_education->join('trans_follow_user as f', 'f.user_id_fk', '=', 'e.user_id_fk', 'left');
				$arr_education->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');

				$whereAndOrRange1 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_e`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";

				$arr_education->whereRaw($whereAndOrRange1);
				$arr_education->whereRaw('`p1036_e`.`status` = "1"');
				$arr_education->whereRaw("`p1036_e`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$arr_education->whereRaw("`p1036_e`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");

				$arr_education->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'e.id', DB::raw('CAST(education_type AS CHAR(50)) AS e_type'),
					'e.title as title', 'e.profile_image as image', 'e.cover_type', 'e.description', 'e.updated_at', 'e.created_at', 'e.onlineTicket', 'e.longitude', 'e.latitude',
					DB::raw("'Education' AS `type`"), DB::raw('(SELECT COUNT(education_id_fk)  FROM `p1036_trans_education_upvote_downvote`
                        WHERE `education_id_fk` = `p1036_e`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_education_upvote_downvote` WHERE
                        `education_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_education_upvote_downvote` WHERE
                        `education_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(education_id_fk) FROM `p1036_trans_education_upvote_downvote`
                        WHERE `education_id_fk` = `p1036_e`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(education_id_fk)  FROM `p1036_trans_education_upvote_downvote`
                        WHERE `education_id_fk` = `p1036_e`.`id` AND `status` = "0"),
                        (SELECT COUNT(education_id_fk) FROM `p1036_trans_education_upvote_downvote`
                        WHERE `education_id_fk` = `p1036_e`.`id` AND `status` = "1"),
                        `p1036_e`.`created_at`) AS hotness')
				);
				$arr_education->groupBy('e.id');
				$arr_education->orderBy('hotness', 'DESC');
				$arr_education = collect($arr_education->get());
				$arr_all_education_data = $arr_education->toArray();
			}

			/*             * ***for jobs***** */
			if (in_array("5", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$arr_job = DB::table('mst_job as e');
				$arr_job->join('trans_job_group as g', 'g.job_id_fk', '=', 'e.id', 'left');
				$arr_job->join('trans_follow_user as f', 'f.user_id_fk', '=', 'e.user_id_fk', 'left');
				$arr_job->join('mst_users as u', 'u.id', '=', 'e.user_id_fk');

				$whereAndOrRange1 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_e`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";

				$arr_job->whereRaw($whereAndOrRange1);
				$arr_job->whereRaw('`p1036_e`.`status` = "1"');
				$arr_job->whereRaw("`p1036_e`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$arr_job->whereRaw("`p1036_e`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");
				$arr_job->whereRaw("`p1036_e`. application_deadline > DATE_SUB(NOW(), INTERVAL 12 HOUR)");
				$arr_job->where('e.longitude', '>', $job_lng_min);
				$arr_job->where('e.longitude', '<', $job_lng_max);
				$arr_job->where('e.latitude', '>', $job_lat_min);
				$arr_job->where('e.latitude', '<', $job_lat_max);

				$arr_job->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture', 'e.id',
					'e.title as title', 'e.profile_image as image', 'e.description as description', 'e.updated_at', 'e.created_at',
					'e.application_deadline', 'e.job_location', DB::raw("'' AS `longitude`, '' AS `latitude`"),
					DB::raw("'Job' AS `type`"), DB::raw('(SELECT COUNT(job_id_fk)  FROM `p1036_trans_job_upvote_downvote`
                        WHERE `job_id_fk` = `p1036_e`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_job_upvote_downvote` WHERE
                        `job_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_job_upvote_downvote` WHERE
                        `job_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(job_id_fk) FROM `p1036_trans_job_upvote_downvote`
                        WHERE `job_id_fk` = `p1036_e`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(job_id_fk)  FROM `p1036_trans_job_upvote_downvote`
                        WHERE `job_id_fk` = `p1036_e`.`id` AND `status` = "0"),
                        (SELECT COUNT(job_id_fk) FROM `p1036_trans_job_upvote_downvote`
                        WHERE `job_id_fk` = `p1036_e`.`id` AND `status` = "1"),
                        `p1036_e`.`created_at`) AS hotness')
				);
				$arr_job->groupBy('e.id');
				$arr_job->orderBy('hotness', 'DESC');
				$arr_job = collect($arr_job->get());
				$arr_all_job_data = $arr_job->toArray();

			}

			if (in_array("4", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$get_events = DB::table('mst_event as e');
				$get_events->join('trans_event_group as g', 'g.event_id_fk', '=', 'e.id', 'left');
				$get_events->join('trans_follow_user as f', 'f.user_id_fk', '=', 'e.user_id_fk', 'left');
				$get_events->join('mst_users as u', 'e.user_id_fk', '=', 'u.id');

				$whereAndOrRange3 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_e`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";
				$get_events->whereRaw($whereAndOrRange3);
				$get_events->whereRaw("`p1036_e`.`status` = '1'");
				$get_events->whereRaw("`p1036_e`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$get_events->whereRaw("`p1036_e`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");
				$get_events->whereRaw("`p1036_e`. start_date > DATE_SUB(NOW(), INTERVAL 24 HOUR)");
				$get_events->where('e.longitude', '>', $lng_min);
				$get_events->where('e.longitude', '<', $lng_max);
				$get_events->where('e.latitude', '>', $lat_min);
				$get_events->where('e.latitude', '<', $lat_max);

				$get_events->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'e.id', DB::raw("'' AS e_type"), 'e.title as title',
					'e.profile_image as image', 'e.cover_type', 'e.location as location', 'description', 'e.updated_at', 'e.created_at', 'e.start_date', 'e.start_time', DB::raw("'Event' AS `type`"),'e.onlineTicket',  'e.longitude', 'e.latitude',
					DB::raw('(SELECT COUNT(event_id_fk)  FROM `p1036_trans_event_upvote_downvote`
                        WHERE `event_id_fk` = `p1036_e`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_event_upvote_downvote` WHERE
                        `event_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_event_upvote_downvote` WHERE
                        `event_id_fk` = `p1036_e`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(event_id_fk) FROM `p1036_trans_event_upvote_downvote`
                        WHERE `event_id_fk` = `p1036_e`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(event_id_fk)  FROM `p1036_trans_event_upvote_downvote`
                        WHERE `event_id_fk` = `p1036_e`.`id` AND `status` = "0"),
                        (SELECT COUNT(event_id_fk) FROM `p1036_trans_event_upvote_downvote`
                        WHERE `event_id_fk` = `p1036_e`.`id` AND `status` = "1"),
                        `p1036_e`.`created_at`) AS hotness')
				);
				$get_events->orderBy('hotness', 'DESC');
				$get_events->groupBy('e.id');
				$get_events = collect($get_events->get());
				$arr_all_event_data = $get_events->toArray();

			}

			if (in_array("3", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$get_meetups = DB::table('mst_meetup as m');
				$get_meetups->join('trans_meetup_group as g', 'g.meetup_id_fk', '=', 'm.id', 'left');
				$get_meetups->join('trans_follow_user as f', 'f.user_id_fk', '=', 'm.user_id_fk', 'left');
				$get_meetups->join('mst_users as u', 'm.user_id_fk', '=', 'u.id');
				$whereAndOrRange4 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_m`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";
				$get_meetups->whereRaw($whereAndOrRange4);
				$get_meetups->whereRaw("`p1036_m`.`status` = '1'");
				$get_meetups->whereRaw("`p1036_m`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$get_meetups->whereRaw("`p1036_m`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");
				$get_meetups->whereRaw("`p1036_m`. start_date > DATE_SUB(NOW(), INTERVAL 24 HOUR)");
				$get_meetups->where('m.longitude', '>', $lng_min);
				$get_meetups->where('m.longitude', '<', $lng_max);
				$get_meetups->where('m.latitude', '>', $lat_min);
				$get_meetups->where('m.latitude', '<', $lat_max);

				$get_meetups->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'm.id',
					DB::raw("'' AS e_type"), 'm.title as title', 'm.profile_image as image', 'm.cover_type', 'm.location as location', 'description','m.onlineTicket',  'm.longitude', 'm.latitude',
					'm.updated_at', 'm.created_at', 'm.start_date', 'm.start_time', DB::raw("'Meetup' AS `type`"),
					DB::raw('(SELECT COUNT(meetup_id_fk)  FROM `p1036_trans_meetup_upvote_downvote`
                        WHERE `meetup_id_fk` = `p1036_m`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_meetup_upvote_downvote` WHERE
                        `meetup_id_fk` = `p1036_m`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_meetup_upvote_downvote` WHERE
                        `meetup_id_fk` = `p1036_m`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(meetup_id_fk) FROM `p1036_trans_meetup_upvote_downvote`
                        WHERE `meetup_id_fk` = `p1036_m`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(meetup_id_fk)  FROM `p1036_trans_meetup_upvote_downvote`
                        WHERE `meetup_id_fk` = `p1036_m`.`id` AND `status` = "0"),
                        (SELECT COUNT(meetup_id_fk) FROM `p1036_trans_meetup_upvote_downvote`
                        WHERE `meetup_id_fk` = `p1036_m`.`id` AND `status` = "1"),
                        `p1036_m`.`created_at`) AS hotness')
				);
				$get_meetups->orderBy('hotness', 'DESC');
				$get_meetups->groupBy('m.id');
				$get_meetups = collect($get_meetups->get());
				$arr_all_meetup_data = $get_meetups->toArray();
			}

			if (in_array("6", $filter_feeds_arr, TRUE) || $filter_feeds_arr[0] == 0) {
				$get_qrious = DB::table('mst_question as q');
				$get_qrious->join('trans_question_group as g', 'g.question_id_fk', '=', 'q.id', 'left');
				$get_qrious->join('trans_follow_user as f', 'f.user_id_fk', '=', 'q.user_id_fk', 'left');
				$get_qrious->join('mst_users as u', 'q.user_id_fk', '=', 'u.id');
				$whereAndOrRange5 = "(`p1036_g`.`group_id_fk` in ($group) or `p1036_q`.`user_id_fk` in ($condition1) or `p1036_f`.`user_id_fk` in ($condition2))";
				$get_qrious->whereRaw($whereAndOrRange5);
				$get_qrious->whereRaw("`p1036_q`.`status` = '1'");
				$get_qrious->whereRaw("`p1036_q`.created_at <= DATE_SUB(NOW(), INTERVAL " . $start_hour_of_posts_to_fetch . " HOUR)");
				$get_qrious->whereRaw("`p1036_q`.created_at > DATE_SUB(NOW(), INTERVAL " . ($start_hour_of_posts_to_fetch + $time_interval) . " HOUR)");

				$get_qrious->select(
					'u.id as user_id', 'u.gender', 'u.user_name', 'u.first_name', 'u.last_name', 'u.profile_picture as profile_picture', 'q.id',
					DB::raw("CAST(`p1036_q`.`status` AS CHAR(50) CHARACTER SET utf8) AS e_type"),
					'q.title', 'q.profile_image as image', 'q.cover_type', 'q.description', 'q.updated_at', 'q.created_at', DB::raw("'' AS `longitude`, '' AS `latitude`"),
					DB::raw("'Qrious' AS `type`"), DB::raw('(SELECT COUNT(question_id_fk)  FROM `p1036_trans_question_upvote_downvote`
                        WHERE `question_id_fk` = `p1036_q`.`id` AND `status` = "0") AS upvote_count'),
					DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_question_upvote_downvote` WHERE
                        `question_id_fk` = `p1036_q`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "0")
                        AS user_upvote_count'), DB::raw('(SELECT COUNT(`status`) FROM `p1036_trans_question_upvote_downvote` WHERE
                        `question_id_fk` = `p1036_q`.`id` AND `user_id_fk` = ' . $user_id . ' AND `status` = "1")
                        AS user_downvote_count'), DB::raw('(SELECT COUNT(question_id_fk) FROM `p1036_trans_question_upvote_downvote`
                        WHERE `question_id_fk` = `p1036_q`.`id` AND `status` = "1")  AS downvote_count'),
					DB::raw('get_hotness((SELECT COUNT(question_id_fk)  FROM `p1036_trans_question_upvote_downvote`
                        WHERE `question_id_fk` = `p1036_q`.`id` AND `status` = "0"),
                        (SELECT COUNT(question_id_fk) FROM `p1036_trans_question_upvote_downvote`
                        WHERE `question_id_fk` = `p1036_q`.`id` AND `status` = "1"),
                        `p1036_q`.`created_at`) AS hotness')
				);
				$get_qrious->orderBy('hotness', 'DESC');
				$get_qrious->groupBy('q.id');
				$get_qrious = collect($get_qrious->get());
				$arr_all_qrious_data = $get_qrious->toArray();
			}

			$start_hour_of_posts_to_fetch += $time_interval;
			//Once posts are fetched, we update the counter in cache for 240 minutes
			Cache::put('homepage' . $platform . $user_id . $session_id, $start_hour_of_posts_to_fetch, 240);
                        
			$arr_data_final = array_merge($arr_data_final, $arr_all_article_data, $arr_all_education_data, $arr_all_event_data, $arr_all_meetup_data, $arr_all_job_data, $arr_all_qrious_data);
		
                    
        }

			$merged_hotness = array();
                foreach ($arr_data_final as $key => $records) {
                    $merged_hotness[$key] = (isset($records->hotness)?$records->hotness:'');
                    $a = $records->longitude;
                    $b = $records->latitude;
                    
                    if($a > $lng_min && $a < $lng_max && $b > $lat_min && $b < $lat_max) 
                    {
                        $arr_data_final[$key]->nearby = '1';
                    }
                    else {
                        $arr_data_final[$key]->nearby = '0';
                    }
                }   
                array_multisort($merged_hotness, SORT_DESC, $arr_data_final);
                
                
                /*********Merge with PROMOTED POSTS***************/
                if ($page != '1' && Cache::has('promote' . $platform . $user_id . $session_id)) {
                        $promote_offset = Cache::get('promote' . $platform . $user_id . $session_id);
                } else {
                        $promote_offset = 0;
                }
                if($page == '1'){$promote_offset = 0;}
                $type = '';
                if (in_array("1", $filter_feeds_arr, TRUE))
                {
                    $type = 'article';
                }
                else if (in_array("2", $filter_feeds_arr, TRUE))
                {
                    $type = 'education';
                }
                else if (in_array("3", $filter_feeds_arr, TRUE))
                {
                    $type = 'meetup';
                }
                else if (in_array("4", $filter_feeds_arr, TRUE))
                {
                    $type = 'event';
                }
                else if (in_array("5", $filter_feeds_arr, TRUE))
                {
                    $type = 'job';
                }
                else if (in_array("6", $filter_feeds_arr, TRUE))
                {
                    $type = 'question';
                }
                $this->mergeWithPromotedPosts($user_id, $arr_data_final, $promote_offset, $type);
                Cache::put('promote' . $platform . $user_id . $session_id, $promote_offset, 240);
                /*********END Merge with PROMOTED POSTS***************/       

		
		$final_array = array();
		foreach ($arr_data_final as $key => $value) {
			$final_array[$key] = $value;
			$description = preg_replace('/<iframe.*?\/iframe>/i', '', $value->description);
			$final_array[$key]->rep_decription = substr(preg_replace('/<img[^>]+\>/i', '', $description), 0, 100);
		}

		$arr_data_final = collect($arr_data_final);
		return $arr_data_final;
	}
        
        /********Get all users who have all their posts as drafted/unpublished***********/
        public function getAllDraftUsersTrait()
        {
            $query = "SELECT DISTINCT user_id_fk FROM (SELECT 'event', user_id_fk, SUM(CAST(CAST(`status` AS CHAR) AS SIGNED)) AS draft, COUNT(*) AS total FROM ".DB::getTablePrefix()."mst_event WHERE status = '0' OR status = '1' GROUP BY user_id_fk HAVING draft = total
                    UNION
                    SELECT 'meetup', user_id_fk, SUM(CAST(CAST(`status` AS CHAR) AS SIGNED)) AS draft, COUNT(*) AS total FROM ".DB::getTablePrefix()."mst_meetup WHERE status = '0' OR status = '1' GROUP BY user_id_fk HAVING draft = total
                    UNION
                    SELECT 'edu', user_id_fk, SUM(CAST(CAST(`status` AS CHAR) AS SIGNED)) AS draft, COUNT(*) AS total FROM ".DB::getTablePrefix()."mst_education WHERE status = '0' OR status = '1' GROUP BY user_id_fk HAVING draft = total
                    UNION
                    SELECT 'article', user_id_fk, SUM(CAST(CAST(`status` AS CHAR) AS SIGNED)) AS draft, COUNT(*) AS total FROM ".DB::getTablePrefix()."mst_article WHERE status = '0' OR status = '1' GROUP BY user_id_fk HAVING draft = total
                    UNION
                    SELECT 'quest', user_id_fk, SUM(CAST(CAST(`status` AS CHAR) AS SIGNED)) AS draft, COUNT(*) AS total FROM ".DB::getTablePrefix()."mst_question WHERE status = '0' OR status = '1' GROUP BY user_id_fk HAVING draft = total) tb";
        
            $all_users = DB::select(DB::raw($query));
            return $all_users;
        }
        
        
        /********Get all users who have not logged in since 10 days***********/
        public function getAllAbsentUsersTrait()
        {
            $query = "SELECT id FROM ".DB::getTablePrefix()."mst_users WHERE DATEDIFF(CURDATE(), DATE(last_login)) >= 10 AND email_verified='1' AND email != '' ";       
            $all_users = DB::select(DB::raw($query));
            return $all_users;
        }

        
        /********************Get all users********************/
        public function getAllUsersTrait()
        {
            $query = "SELECT id FROM ".DB::getTablePrefix()."mst_users WHERE email_verified='1' AND email != '' ";       
            $all_users = DB::select(DB::raw($query));
            return $all_users;
        }
        
        
        function mergeWithPromotedPosts($user_id, &$arr_data_final, &$promote_offset, $type)
        {
            $position = 3; // INSERT POST AT THIS POSITION
            $length = intval(count($arr_data_final)/$position); 
            if($length == 0)
            {
               $length = '18446744073709551615';
            }
            $promoted_posts = $this->getAllPromotedPostsHomepage($user_id, $promote_offset, $length, $type);
            
            if(count($arr_data_final))
            {
                $i = $position;
                foreach($promoted_posts as $key => $records)
                {
                    array_splice($arr_data_final, $i, 0, array((object)$records));
                    $i+=$position;
                }
            }
            else
            {
                foreach($promoted_posts as $key => $records)
                {
                    $arr_data_final[] = (object)$records;
                }
            }
            $promote_offset = $promote_offset + count($promoted_posts);
        }
    
        function getAllPromotedPostsHomepage($user_id, $offset='0', $length='0', $type = '')
        {
            if($type != '')
            {
               $list = postPromotion::where('status', '1')->where('post_type', $type)->whereRaw('CURDATE() between `promo_start_date` and `promo_end_date`')->orderby('id', 'desc')->take($length)->skip($offset)->get(['post_id_fk', 'post_type'])->toArray();            
            }
            else {
                $list = postPromotion::where('status', '1')->whereRaw('CURDATE() between `promo_start_date` and `promo_end_date`')->orderby('id', 'desc')->take($length)->skip($offset)->get(['post_id_fk', 'post_type'])->toArray();            
            }
            
            foreach($list as $key=>$value)
            {
                $type = $value['post_type'];
                //DB::setFetchMode(\PDO::FETCH_ASSOC);
                $table = 'mst_'.$type;
                $id = $value['post_id_fk'];

//                if($type == 'job')
//                {
//                    $slect = DB::raw("'' as image");
//                }else
//                {
//                    $slect = "tb.profile_image as image";
//                }
                
                if($type == 'event' || $type == 'meetup' || $type == 'education')
                {
                    $slect = $slect.", tb.onlineTicket";
                }
                
                $event_det = DB::table($table.' as tb')
                        ->join('mst_users as u', 'tb.user_id_fk', '=', 'u.id')
                        ->select('u.id as user_id', 'u.profile_picture as profile_picture', 'u.*', DB::raw("'' as hotness"), 'tb.profile_image as image','tb.*', $slect)
                        ->selectRaw('(SELECT COUNT(`status`) FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.id AND `user_id_fk` = ' . $user_id . ' AND `status` = "0") AS user_upvote_count')
                        ->selectRaw('(SELECT COUNT(`status`) FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.id AND `user_id_fk` = ' . $user_id . ' AND `status` = "1") AS user_downvote_count')
                        ->selectRaw('(SELECT COUNT('.$type.'_id_fk) FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.id AND `status` = "0") AS upvote_count')
                        ->selectRaw('(SELECT COUNT('.$type.'_id_fk) FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.id AND `status` = "1") AS downvote_count')
                        //->selectRaw('get_hotness((SELECT COUNT('.$type.'_id_fk)  FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.d` AND `status` = "0"), (SELECT COUNT('.$type.'_id_fk) FROM `p1036_trans_'.$type.'_upvote_downvote` WHERE '.$type.'_id_fk = p1036_tb.id AND `status` = "1"), `p1036_tb.created_at`) AS hotness')
                        ->selectRaw("'' as hotness")
                        ->selectRaw("'' as e_type")
                        ->where('tb.id', $id)->get()->all();


			if(!empty($event_det)){
                if($type == 'question')
                {
                    $event_det[0]->type = 'Qrious';
                }
                else
                {
                    $event_det[0]->type = ucfirst($type);
                }
                $event_det[0]->promoted = 'promoted';
				$list[$key] = array_merge($list[$key], (array)$event_det[0]);
			}
            }
            //DB::setFetchMode(\PDO::FETCH_CLASS);
            return $list;
        }
}
