<?php namespace App\Traits\Post;

use Exception;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\email_templates;
use App\email_template_macros;
use App\Event;
use App\CommonModel;
use App\Comment;
use App\User;
use App\group;
use App\global_settings;
use App\trans_global_settings;
use App\Helpers\GlobalData;
use App\privileges;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Session;
use App\FollowerUser;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\groupEvent;
use App\UserActivity;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image as Image;
use GuzzleHttp\Client;

trait MailUpvoteDownvoteFollowCommentTag {
	/* This trait sends mails to user when
		             * Your post is upvoted
		             * Your post is downvoted
		             * Your post is followed
		             * Some one comments on your post
		             * Some one comments on the post you are following
		             * Some one tags in a comment
		Arguments expected:
			* poster_user_id = original poster (array)
			* type = 
			* post_name = Title of Post
			* post_link = URL
			* post_type = article | job | qrious | education , etc
			* notification_from_user_id = pass in optionally commenter's info (for API)
	*/

	public function send_mail($poster_user_id, $type, $post_name, $post_link, $post_type, $notification_from_user_id = '') {
		/* ***********Send Email Notification************ */
		$user = DB::table('mst_users')->where('id', $poster_user_id)->get()->first();
		$user_account = Session::get('user_account');
		$site_email = trans_global_settings::where('id', '1')->first();
		$site_title = trans_global_settings::where('id', '2')->first();

		/* setting reserved_words for email content */
		$macros_array_detail = array();
		$macros_array_detail = email_template_macros::all();

		$macros_array = array();
		foreach ($macros_array_detail as $row) {
			$macros_array[$row['macros']] = $row['value'];
		}

		if ($type == 'upvote') {
			$email_contents = email_templates::where(array('email_template_title' => 'post-upvote-notification', 'lang_id' => '14'))->first();
		} else if ($type == 'downvote') {
			$email_contents = email_templates::where(array('email_template_title' => 'post-downvote-notification', 'lang_id' => '14'))->first();
		} else if ($type == 'follow') {
			$email_contents = email_templates::where(array('email_template_title' => 'post-follow-notification', 'lang_id' => '14'))->first();
		} else if ($type == 'comment') {
			$email_contents = email_templates::where(array('email_template_title' => 'post-comment-notification', 'lang_id' => '14'))->first();
		} else if ($type == 'comment-tag') {
			$email_contents = email_templates::where(array('email_template_title' => 'post-comment-tag-notification', 'lang_id' => '14'))->first();
                } else if($type=='answer') {
                    $email_contents = email_templates::where(array('email_template_title' => 'post-answer-notification-to-post-user', 'lang_id' => '14'))->first();
                }

		$email_contents = $email_contents;
		$content = $email_contents['email_template_content'];

		$reserved_words = array();
		$site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

		if ($user_account != '') {
			$user_acc = DB::table('mst_users')->where('id', $user_account['id'])->get()->first();
		} else {
			$user_acc = DB::table('mst_users')->where('id', $notification_from_user_id)->get()->first();
        }

		$profile_path = url('/') . '/public/assets/Frontend/user/profile_pics/' . $user_acc->id . '/thumb/' . $user_acc->profile_picture;
		if (isset($user_acc->profile_picture) && $user_acc->profile_picture != '') {
			if ($user_acc->gender == 0) {
				$profile_path_onerror = url('/') . '/public/assets/Frontend/img/avtar.png';
			} else {
				$profile_path_onerror = url('/') . '/public/assets/Frontend/img/avtar_female.png';
			}

			$profile_image_path = $profile_path . '" onerror="' . $profile_path_onerror;
		} else {
			if ($user_acc['gender'] == 0) {
				$profile_path = url('/') . '/public/assets/Frontend/img/avtar.png';
				$profile_image_path = $profile_path;
			} else {
				$profile_path = url('/') . '/public/assets/Frontend/img/avtar_female.png';
				$profile_image_path = $profile_path;
			}
		}

		$reserved_arr = array(
			"||SITE_TITLE||" => stripslashes($site_title->value),
			"||SITE_PATH||" => $site_path,
			"||USER_NAME||" => $user->first_name,
			"||USER_EMAIL||" => $user->email,
			"||SECOND_USER_NAME||" => $user_acc->first_name . " " . $user_acc->last_name,
			"||SECOND_USER_PROFILE_PATH||" => url('/') . '/user-profile/' . (base64_encode($user_acc->id)),
			"||SECOND_USER_PROFILE_IMAGE||" => $profile_image_path,
			"||NOTIFICATION_LINK||" => url('/') . '/notification/',
			"||POST_TYPE||" => $post_type,
			"||POST_NAME||" => $post_name,
			"||POST_LINK||" => $post_link,
		);

		$reserved_words = array_replace_recursive($macros_array, $reserved_arr);

		foreach ($reserved_words as $k => $v) {
			$content = str_replace($k, $v, $content);
		}

		/* getting mail subect and mail message using email template title and lang_id and reserved works */
		$contents_array = array("email_conents" => $content);
		$data['site_email'] = $site_email->value;
		$data['site_title'] = $site_title->value;

		// Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
		Mail::queue('Frontend.email-contents.email-contents', $contents_array, function ($message) use ($contents_array, $data, $user, $email_contents) {
			$message->from($data['site_email'], $data['site_title']);
			$message->to($user->email, $user->first_name)->subject($email_contents['email_template_subject']);
		});
	
	}

    public function mukeshfx($add, $rem){
        $str='<h1>add='.$add.' <br>rem='.$rem.'</h1>';
        return $str;
    }

    public function send_mail_by_adminfx($second_user_id, $type, $post_name, $post_link, $post_type, $group_name='', $group_link='') {

        /* ***********Send Email Notification************ */
        $user = DB::table('mst_users')->where('id', $second_user_id)->get()->first();

        $site_email = trans_global_settings::where('id', '1')->first();

        $site_title = trans_global_settings::where('id', '2')->first();
        /* setting reserved_words for email content */
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();

        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }

        if($type=='post-group-remove')
            $email_contents = email_templates::where(array('email_template_title' => 'post-group-remove-notification', 'lang_id' => '14'))->first();
        else if($type=='post-group-add')
            $email_contents = email_templates::where(array('email_template_title' => 'post-group-add-notification', 'lang_id' => '14'))->first();


        $email_contents = $email_contents;
        $content = $email_contents['email_template_content'];
        
        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($site_title->value),
            "||SITE_PATH||" => $site_path,
            "||USER_NAME||" => $user->first_name,
            "||USER_EMAIL||" => $user->email,
            "||POST_TYPE||" => $post_type,
            "||POST_NAME||" => $post_name,
            "||POST_LINK||" => $post_link,
            "||GROUP_NAME||" => $group_name,
            "||GROUP_LINK||" => $group_link,
        );
        
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);

        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
        }
        
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);
        $data['site_email'] = $site_email->value;
        $data['site_title'] = $site_title->value;

        
        // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
        Mail::queue('Frontend.email-contents.email-contents', $contents_array, function($message) use($contents_array, $data, $user, $email_contents) {
            $message->from($data['site_email'], $data['site_title']);
            $message->to($user->email, $user->first_name)->subject($email_contents['email_template_subject']);
        });   
        
    }
}
