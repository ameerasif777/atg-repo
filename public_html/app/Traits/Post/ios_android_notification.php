<?php namespace App\Traits\Post;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Mail;
use DB;
use Cookie;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Filesystem\Filesystem;
use App\UserProfileImages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;

trait ios_android_notification {

	public function send_notification($registration_id, $message = '') {
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => array($registration_id),
			'data' => array("message" => $message),
		);
		$fields = json_encode($fields);
		$headers = array(
			'Authorization: key=AIzaSyD_3Vkt_NtklQGQ9CwEDSbPXcsOGKIQh_k',
			'Content-Type: application/json',
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$result = curl_exec($ch);
//        echo '<pre>';print_r($result);die;
		// Close connection
		curl_close($ch);
		return $result;
//        echo $result;
	}

//ios_notificaton
	function ios_notificaton($registrationId, $message) {
		$fcmApiKey = 'AAAAo_NpfIQ:APA91bF3zwOpc8skjySGSN5xDyu_sJ28cFgkp2UsKkt02zl-f4cNkJQHp9qwfgrDdVFAKe5SZbR8aV9zxTOf45DYMx25M-Z6s6lgXmkLQ1UgT8dY6zYZkLCl74FjRUwcCZBSC4Jc43zZ'; //App API Key(This is google cloud messaging api key not web api key)
		$url = 'https://fcm.googleapis.com/fcm/send'; //Google URL
		$title = "atgworld";
		$notification = array('title' => $title, 'text' => $message, 'flag' => '1', 'order_id' => '12466564');

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('to' => $registrationId, 'notification' => $notification, 'priority' => 'high');

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);

		$headers = array(
			'Authorization: key=' . $fcmApiKey,
			'Content-Type: application/json',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		// Execute post
		$result = curl_exec($ch);
		// if ($result === FALSE) {
		//     die('Curl failed: ' . curl_error($ch));
		// }
		// Close connection
		curl_close($ch);
		echo $result;
		$array_json_return = array("success" => "1");
		return json_encode($array_json_return);
//        $array_json_return = array("error_code" => '0', "msg" => "Success", "Response" => $result);
		//        return json_encode($array_json_return);
	}

	//test ios notification
	function testIOSNotificaton() {
		$ios_fcmApiKey = '';
//        $ios_fcmApiKey = $data['global']['ios_push_notification_server_key'];
		$fcmApiKey = 'AAAAo_NpfIQ:APA91bF3zwOpc8skjySGSN5xDyu_sJ28cFgkp2UsKkt02zl-f4cNkJQHp9qwfgrDdVFAKe5SZbR8aV9zxTOf45DYMx25M-Z6s6lgXmkLQ1UgT8dY6zYZkLCl74FjRUwcCZBSC4Jc43zZ'; //App API Key(This is google cloud messaging api key not web api key)
		$fcmApiKey = $fcmApiKey; //App API Key(This is google cloud messaging api key not web api key)
		$url = 'https://fcm.googleapis.com/fcm/send'; //Google URL
		//Fcm Device ids array
		$registrationId = "cyXWlk2ZVxg:APA91bGaVkppfKMPjljlr1i0KePUYg0fin0c__SUd6Knn_QquVZO4qQLAXByNp1HG5DhovspNh9JxLW6S-m2HwctpwfpM7J9-PsnH9dmP61iIBWvzCH6IxTfdYc8qYPfxQwgnUYp8__N";
//        $registrationId = "e8a2cGvjgsE:APA91bEcrswvTgzx1P2IjPeo4kpuAoHeY1aWhIULjPg0S4fbn4JGEILhMb9cYJGA5wWwWrdEaie4hgwFs3ZaakF2mIHlVDi2hNUEZbUOfxt7P6kSzlci1d3sMz82MlFUJDebhHkjIpAR";
		// prepare the bundle
		$title = "UPGLEE";
		//Body of the Notification.
		//        $body = "Bear island knows no king but the king in the north, whose name is stark.";
		$message = "Bear island knows no king but the king in the north, whose name is stark.";
		//Creating the notification array.
		$notification = array('title' => $title, 'text' => $message, 'flag' => '1', 'order_id' => '12466564');

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('to' => $registrationId, 'notification' => $notification, 'priority' => 'high');

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);

		$headers = array(
			'Authorization: key=' . $fcmApiKey,
			'Content-Type: application/json',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		// Execute post
		$result = curl_exec($ch);
		// if ($result === FALSE) {
		//     die('Curl failed: ' . curl_error($ch));
		// }
		// Close connection
		curl_close($ch);
		echo $result;
		die;
		$array_json_return = array("error_code" => '0', "msg" => "Success", "Response" => $result);
		return json_encode($array_json_return);
	}

}
