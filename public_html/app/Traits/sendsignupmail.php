<?php namespace App\Traits;

use DB;
use App\CommonModel;
use Illuminate\Support\Facades\Mail;
use App\email_templates;
use App\email_template_macros;
use Illuminate\Support\Facades\Cache;
use App\trans_global_settings;

trait sendsignupmail {

    public function sendmailsocialsignup($name,$mode,$email) {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $email_contents = email_templates::where(array('email_template_title' => 'send_mail_signup_social', 'lang_id' => '14'))->first();
        $mailcontent=$email_contents['email_template_content'];
        $reserved_arr = array(
            "||Name||" => ucwords($name),
            "||Email||" => $email,
            "||Mode||" => $mode,
            "||SITE_PATH||"=>url('/'),
        );      

        foreach ($reserved_arr as $k => $v) {
            $mailcontent = str_replace($k, $v, $mailcontent);
        }

        $mailcontent_array = array("email_conents" => $mailcontent);

        Mail::queue('Frontend.email-contents.email-contents', $mailcontent_array, function($message)use($name, $email, $data, $email_contents) {
            $message->from($data['global']['contact_email'], $data['global']['site_title']);
            $message->to($email, ucwords($name))->subject($email_contents['email_template_subject']);
        });
        return 1;
    }

    public function sendEmailVerificationMail($email , $user_account) {

        $activation_code = time() . md5(rand(0,1000));

        if (Cache::has('email_activation' . $email)) {
            $activation_code = Cache::get('email_activation' . $email);
        } else {
            $xyz = Cache::put('email_activation' . $email, $activation_code, 14400);
        }

        DB::table('mst_users')
                ->where('email', $email)
                ->update([
                    'activation_code' => $activation_code,
        ]);

        /* Activation link  */
        $site_email = trans_global_settings::where('id', '1')->first();
        $site_title = trans_global_settings::where('id', '2')->first();

        $activation_link = '<a href="' . url('/') . '/user/account-activate/' . $activation_code . '">Activate Account</a>';

        /* setting reserved_words for email content */
        $macros_array_detail = array();
        $macros_array_detail = email_template_macros::all();

        $macros_array = array();
        foreach ($macros_array_detail as $row) {
            $macros_array[$row['macros']] = $row['value'];
        }
        $email_contents = email_templates::where(array('email_template_title' => 'registration-successful', 'lang_id' => '14'))->first();

        $email_contents = $email_contents;
        $content = $email_contents['email_template_content'];

        $name = "";
        $reserved_words = array();
        $site_path = '<a href="' . url('/') . '">' . url('/') . '</a>';

        $reserved_arr = array(
            "||SITE_TITLE||" => stripslashes($site_title->value),
            "||SITE_PATH||" => $site_path,
            "||USER_NAME||" => $user_account['first_name'],
            "||USER_EMAIL||" => $user_account['email'],
            "||ADMIN_NAME||" => $name,
            "||VERIFY_MY_EMAIL||" => $activation_link
        );
        $reserved_words = array_replace_recursive($macros_array, $reserved_arr);
        foreach ($reserved_words as $k => $v) {
            $content = str_replace($k, $v, $content);
            $email_contents['email_template_subject'] = str_replace($k, $v, $email_contents['email_template_subject']);
        }
        /* getting mail subect and mail message using email template title and lang_id and reserved works */
        $contents_array = array("email_conents" => $content);
        $data = array();
        $data['site_email'] = $site_email->value;
        $data['site_title'] = $site_title->value;
        $user = $user_account['email'];
        try {
            // Send a mail to the user. This will plug the datavalues into the reminder email template and mail the user.
            Mail::send('Frontend.email-contents.email-contents', $contents_array, function($message) use($user, $contents_array, $data, $email_contents, $name) {
                $message->from($data['site_email'], $data['site_title']);
                $message->to($user, $name)->subject($email_contents['email_template_subject']);
            });
            \Log::debug("Verification Email sent to " . $user_account['email']);
        } catch (\Exception $e) {
            \Log::alert("SIGNUP VERIFICATION EMAIL NOT SENT to " . $user_account['email']);
            \Log::info($e);
        }

        return '0';
    }

}