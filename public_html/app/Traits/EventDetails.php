<?php

namespace App\Traits;
use App\CommonModel;
use DB;
use Session;
use App\postPromotion;

trait EventDetails
{
    public function getEventDetails($type, $id)
    {
        $commonModel = new CommonModel();
        $table1 = '';
        if($type == 'event' || $type == 'meetup' || $type == 'education' || $type == 'article' || $type == 'job' || $type == 'question') {
            $table1 = 'mst_' . $type;

            $list = DB::table($table1 . ' as b')
                ->where('b.id', $id)
                ->select('b.*')
                ->get();
            if(isset($list[0]) && !isset($list[0]->profile_image))
            {
                $list[0]->profile_image = '';
            }

            return $list;
        }
    }


    public function getEventCostDetails($type, $cost_id)
    {
        $commonModel = new CommonModel();
        $table1 = '';
        $table2 = '';
        if($type == 'event' || $type == 'meetup' || $type == 'education') {
            $table1 = 'mst_' . $type . '_cost';
            $table2 = 'mst_' . $type;

            $list = DB::table($table1 . ' as a')
                ->join($table2 . ' as b', 'a.'.$type.'_id_fk', '=', 'b.id')
                ->where('a.id', $cost_id)
                ->select('a.id as cost_id', 'b.id as event_id', 'b.*', 'a.cost as ccost', 'a.currency as ccurrency', 'a.description as cost_desc')
                ->get();

            return $list;
        }
    }
    
    public function fillRsvpOrBuyDetails($cost_id, $status, $type, $category)
    {
        $commonModel = new CommonModel();
        $data = $commonModel->commonFunction();
        $data['user_session'] = Session::get('user_account');
        $user_id = $data['user_session']['id'];

        if($type == 'event' || $type == 'meetup' || $type == 'education') {
            $table1 = 'mst_' . $type . '_cost';
            $table2 = 'trans_' . $type . '_rsvp_status';

            $list = DB::table($table1 . ' as a')
                ->where('a.id', $cost_id)
                ->select('a.' . $type . '_id_fk as event_id')
                ->get();

            $event_id = $list[0]->event_id;

            if ($category == 'buy') {

                $get_data = DB::table($table2)
                    ->where($type . '_id_fk', $event_id)
                    ->where('user_id_fk', $user_id)
                    ->where('cost_id_fk', $cost_id)
                    ->where('buy', '1')
                    ->first();
                if (count($get_data) == 0) {
                    $insertid = DB::table($table2)->insertGetId([$type . '_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => '0', 'created_at' => date("Y-m-d H:i:s"), 'buy' => '1']);
                } else {
                    $insertid = $get_data->id;
                }

                return $insertid;
            }
            elseif($category == 'rsvp')
            {
                $get_data = DB::table($table2)
                    ->where($type . '_id_fk', $event_id)
                    ->where('user_id_fk', $user_id)
                    ->where('cost_id_fk', $cost_id)
                    ->where('buy', '0')
                    ->get();

                if(count($get_data)>0) {
                    DB::table($table2)
                        ->where($type . '_id_fk', $event_id)
                        ->where('cost_id_fk', $cost_id)
                        ->where('user_id_fk', $user_id)
                        ->where('buy', '0')
                        ->update("status", $status);
                    return 'update';
                }
                else {
                    DB::table($table2)->insert([[$type . '_id_fk' => $event_id, 'cost_id_fk' => $cost_id, 'user_id_fk' => $user_id, 'status' => $status, 'created_at' => date("Y-m-d H:i:s")]]);
                    return 'insert';
                }
            }
        }
    }

    /*
     * To get number of tickets booked for each cost id of an event.
     */
    public function getTicketsBooked($user_id, $event_id, $type)
    {
        $list = array();
        if($type == 'event' || $type == 'meetup' || $type == 'education') {
            $table1 = 'trans_' . $type . '_rsvp_status';
            $col = $type.'_id_fk';

            $list = DB::table($table1.' as a')->join('bank_transactions as b', 'a.id', '=', 'b.id_fk')
                ->where('a.user_id_fk', $user_id)
                ->where('buy', '1')
                ->where('b.status', '1')
                ->where('a.'.$col, $event_id)
                ->select(DB::raw('cost_id_fk, SUM(ticketCount) as tcount'))
                ->groupby('a.cost_id_fk')
                ->get();
        }
        return $list;

    }
    
    function getAllPromotedPosts()
    {
        $list = postPromotion::where('status', '1')->whereRaw('CURDATE() between `promo_start_date` and `promo_end_date`')->orderby('id', 'desc')->get(['post_id_fk', 'post_type', 'promo_start_date', 'promo_end_date'])->toArray();            

        foreach($list AS $key=>$value)
        {
            $type = $value['post_type'];
            //DB::setFetchMode(\PDO::FETCH_ASSOC);
            $table = 'mst_'.$type;
            $id = $value['post_id_fk'];

            $event_det = DB::table($table.' as tb')
                    ->join('mst_users AS u', 'u.id', '=', 'tb.user_id_fk')
                    ->select('tb.id', 'tb.title', 'u.user_name', 'u.email')
                    ->where('tb.id', $id)->get()->all();

            $list[$key] = array_merge($list[$key], (array)$event_det[0]);
        }
        //DB::setFetchMode(\PDO::FETCH_CLASS);
        return $list;
    }
    
    
    function getAllPromotedPostsEndedYesterday()
    {
        $list = postPromotion::where('status', '1')->whereRaw('promo_end_date = SUBDATE(CURDATE(), INTERVAL 1 DAY)')->orderby('id', 'desc')->get(['post_id_fk', 'post_type', 'promo_start_date', 'promo_end_date'])->toArray();            

        foreach($list AS $key=>$value)
        {
            $type = $value['post_type'];
            //DB::setFetchMode(\PDO::FETCH_ASSOC);
            $table = 'mst_'.$type;
            $id = $value['post_id_fk'];

            $event_det = DB::table($table.' as tb')
                    ->join('mst_users AS u', 'u.id', '=', 'tb.user_id_fk')
                    ->select('tb.id', 'tb.title', 'u.user_name', 'u.email')
                    ->where('tb.id', $id)->get()->all();

            $list[$key] = array_merge($list[$key], (array)$event_det[0]);
        }
        //DB::setFetchMode(\PDO::FETCH_CLASS);
        return $list;
    }
}