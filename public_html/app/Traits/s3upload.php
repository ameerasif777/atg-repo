<?php namespace App\Traits;
use Illuminate\Support\Facades\Cache;
use Mail;
use DB;
use App\User;

use Auth;
use Illuminate\Support\Facades\Input;

use App\Traits\sendMail;
use Illuminate\Support\Facades\Storage;
/**
 * 
 */
trait s3upload
{

    private function createfilename($t,$extension='png'){
        $rno = rand(100, 2147483647);
        $dt = date('msuY');
        $filename = $t.'-'.$dt.$rno. '.'.$extension;
        return $filename;
    }

    private function store_file($destinationPath,$filetoupload,$filename,$size=null,$destinationPathresized){
        $image = \Image::make($filetoupload);
        $image=$image->response();    
        Storage::put($destinationPath . '/' . $filename, $image->original);

        if($size!=null){
            $image = \Image::make($filetoupload);
            $image->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
            });

            $image=$image->response();    
            Storage::put($destinationPathresized . '/' . $filename, $image->original);
        }
        return 1;
    }
    private function createfolder($destinationPath){
        if (!Storage::exists($destinationPath)) {
            $result = Storage::makeDirectory($destinationPath,0777,true,true);
            return $result;
        }
        return 1;
    }
    public function upload_pic($type, $user_id=null,$filetoupload,$folder_path=null,$side='Frontend'){ //types-> 0: profile pic, 1: cover Image, 2:group image icon, 3: uploadimage for features on drag and drop.

        if($type==0){
            $destinationPath = "/assets/$side/user/profile_pics/$user_id";
            $destinationPathresized="/assets/$side/user/profile_pics/$user_id/thumb";
            $this->createfolder($destinationPath);
            $this->createfolder($destinationPathresized);
            try {
                if ($filetoupload) {
                    $filename=$this->createfilename('profile');
                    $this->store_file($destinationPath,$filetoupload,$filename,300,$destinationPathresized);                
                    return json_encode(array("error" => '0', "profile_picture" => $filename));
                    
                }
            } catch (Exception $e) {
                return json_encode(array("error" => '1'));
                
            }

        } 

        if($type==1){
            $destinationPath = "/assets/$side/user/cover_photo/$user_id";
            $destinationPathresized = "/assets/$side/user/cover_photo/$user_id/thumb";
            $this->createfolder($destinationPath);
            $this->createfolder($destinationPathresized);
            try {
                if ($filetoupload) {
                    $filename=$this->createfilename('profile_cover');
                    $this->store_file($destinationPath,$filetoupload,$filename,600,$destinationPathresized);                     
                    return json_encode(array("error" => '0', "cover_photo" => $filename));
                    
                }
            } catch (Exception $e) {
                return json_encode(array("error" => '1'));
                
            }

        }
         
        if($type==2){
            $destinationPath = "/assets/Backend/img/group_img/icon";
            $destinationPathresized = "/assets/Backend/img/group_img/icon/thumb";
            $this->createfolder($destinationPath);
            $this->createfolder($destinationPathresized);
            try {
                if ($filetoupload) {
                    $filename=$this->createfilename('group_icon');
                    $this->store_file($destinationPath,$filetoupload,$filename,50,$destinationPathresized);                     
                    return json_encode(array("error" => '0', "icon_img" => $filename));
                    
                }
            } catch (Exception $e) {
                return json_encode(array("error" => '1'));
                
            }

        }

        if($type==3){
            $destinationPath = "/assets/$side/images/" . $folder_path;
            $destinationPathresized = "/assets/$side/images/" . $folder_path.'/thumb';
            $this->createfolder($destinationPath);
            $this->createfolder($destinationPathresized);
            try {
                if ($filetoupload) {
                    $filename=$this->createfilename('feature_inside');
                    $this->store_file($destinationPath,$filetoupload,$filename,300,$destinationPathresized);  
                    return json_encode(array("error" => '0', "link" => config('awss3bucketurl').$destinationPath.'/'.$filename));
                    
                }
            } catch (Exception $e) {
                return http_response_code(404);
                
            }

        }
        if($type==4){
            $destinationPath = "/assets/Backend/img/group_img/cover";
            $destinationPathresized = "/assets/Backend/img/group_img/cover/thumb";
            $this->createfolder($destinationPath);
            $this->createfolder($destinationPathresized);
            try {
                if ($filetoupload) {
                    $filename=$this->createfilename('group_icon');
                    $this->store_file($destinationPath,$filetoupload,$filename,300,$destinationPathresized);                     
                    return json_encode(array("error" => '0', "cover_img" => $filename));
                    
                }
            } catch (Exception $e) {
                return json_encode(array("error" => '1'));
                
            }

        }

        
        

    }



    public function upload_feature_pic($feature_pic,$feature,$side='Frontend'){  //upload pic for article, meetup, qrious, event, job, education
        $destinationPath ="/assets/$side/images/". $feature . '_image';
        $destinationPathresized = "/assets/$side/images/". $feature . "_image/thumb";
        $this->createfolder($destinationPath);
        $this->createfolder($destinationPathresized);
        try {
            $extension = $feature_pic->getClientOriginalExtension(); // getting image extension
            if ($extension == '')
                $extension = 'jpg';
                $filename=$this->createfilename($feature,$extension);
                $this->store_file($destinationPath,$feature_pic,$filename,300,$destinationPathresized);  
                return (array("return_code" => 0, "msg" => $filename));
        } catch (\Exception $e) {
                return (array("return_code" => 1, "msg" => "Image could not be processed"));     
        }
        
    }

    public function upload_resume($user_id,$file,$filename,$side='Frontend'){
            $destinationPath = "/assets/$side/user/resume/$user_id";
            try{
                $this->createfolder($destinationPath);
                Storage::putFileAs($destinationPath,$file,$filename);
                return true;
            }
            catch(Exception $e){
                return false;
            }
            
    }
    public function upload_attachment($file,$filename,$side='Frontend'){
        $destinationPath = "/assets/$side/ims-attachment";  
        try{
            $this->createfolder($destinationPath);
            Storage::putFileAs($destinationPath,$file,$filename);
            return true;
        }
        catch(Exception $e){
            return false;
        }
        
}
    public function delete_file($fileloacation){
        if(Storage::exists($fileloacation)){
            return Storage::delete($fileloacation);
        }
        return false;
    }


    public function gfp_of_profilepic($user_id=""){
        if($user_id!=""){
            $user_profile_pic=User::where('id',$user_id)->select('profile_picture')->first();
            $user_profile_pic=$user_profile_pic->profile_picture;
            $prepath=config('profile_pic_url');
            if(!empty($user_profile_pic)){
                $respond=json_encode(["error"=>0,"msg"=>"success","thumb_path"=>$prepath.$user_id."/thumb/".$user_profile_pic,"path"=>$prepath.$user_id."/".$user_profile_pic]);
            }
            else
                $respond=json_encode(["error"=>1,"path"=>"","thumb_path"=>"","msg"=>"user profile picture missing"]);

            return $respond;
            


        }
        return json_encode(['error'=>2,'msg'=>"user id missing","path"=>$prepath]);
    }

    
    
}
