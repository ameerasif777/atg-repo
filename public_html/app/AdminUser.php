<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\role;

class AdminUser extends Model {

    protected $table = 'mst_users';

    public function values() {
        return $this->hasMany('App\role', 'id', 'role_id');
    }

}
