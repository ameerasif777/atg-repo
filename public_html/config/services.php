<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'linkedin' => [
        'client_id' => '814aybq5mkw19j',
        'client_secret' => 'lh5GB0l0GqSoMU6C',
    ],
 
    'facebook' => [
        'client_id' => '906595209472982',
        'client_secret' => '67139f9310a5d9fb3916b2e1588982b1',
       // 'redirect' => url('/').'/facebook_callback',
    ],
    
    'google' => [
        'client_id' => '735709866114-lds3n4epn5f37ralh3k63netegt8r9nn.apps.googleusercontent.com',
        'client_secret' => 'atdbIpVEcrPyuXpXwj34FcXz',
        //'redirect' => url('/').'/google-callback',
    ],
     
    'twitter' => [
        'client_id' => 'rzs1QufU4AtdGlRx5ESTCwO7a',
        'client_secret' => 'rTSXLk9J15tjMbbhFAMw67WY9pQkd4eHTUuGySUahLjsF7jyn4',
        //'redirect' => url('/').'/twitter-callback',
    ],

];
