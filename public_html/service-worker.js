self.addEventListener('install',function(params) {
    console.log('sw install');
})

self.addEventListener('activate',function(params) {
    console.log('sw activate');
    
})
self.addEventListener('push', function(res) {
  console.log('push')
    var body;
    var title;
    if (res.data) {
      body = JSON.parse(res.data.text());
    } else {
      body = 'Something might gone wrong, Please report this if you continously see this';
    }

    title=body.title;  
    var options = {
      body: body.body,
      icon: body.icon,
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1
      },
      actions: [
        {action: 'explore', title: 'Visit',
          },
        {action: 'close', title: 'Close',
          },
      ]
    };
    res.waitUntil(
      self.registration.showNotification(title, options)
    );
  });