$(document).ready(function () {
	
	$('#frm_achievement_details').validate({
		errorElement: "div",
		rules: {
			title: {
				required: true,
			},
			date: {
				required: true,
			},
			venue: {
				required: true,
			}

		},
		messages: {
			
			title: {
				required: "Please enter title."
			},
			date: {
				required: "Please enter date."
			},
			venue: {
				required: "Please enter venue."
			}
			

		},
		submitHandler: function (form) {

				form.submit();

		}

	});
});