$(document).ready(function () {
	
	$('#frm_academic_details').validate({
		errorElement: "div",
		rules: {
			institute_id: {
				required: true,
			},
			duration_from: {
				required: true,
			},
			duration_to: {
				required: true,
			},
			degree: {
				required: true,
			},
			course: {
				required: true,
			},
			grade: {
				required: true,
			}

		},
		messages: {
			
			institute_id: {
				required: "Please select institute or university."
			},
			duration_from: {
				required: "Please select duration(from)."
			},
			duration_to: {
				required: "Please select duration(to)."
			},
			degree: {
				required: "Please select degree."
			},
			course: {
				required: "Please select course."
			},
			grade: {
				required: "Please enter grade."
			}


		},
		submitHandler: function (form) {

				form.submit();

		}

	});
});