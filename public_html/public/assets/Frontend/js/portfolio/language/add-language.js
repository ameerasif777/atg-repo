$(document).ready(function () {
	
	$('#frm_langauage_details').validate({
		errorElement: "div",
		rules: {
			language: {
				required: true,
			},
			proficiency: {
				required: true,
			}

		},
		messages: {
			
			language: {
				required: "Please enter language."
			},
			proficiency: {
				required: "Please select proficiency."
			}


		},
		submitHandler: function (form) {

				form.submit();

		}

	});
});