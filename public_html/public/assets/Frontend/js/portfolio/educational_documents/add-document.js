$(document).ready(function () {
	
	$('#frm_document_details').validate({
		errorElement: "div",
		rules: {
			title: {
				required: true,
			},
			file: {
				required: true,
			}

		},
		messages: {
			
			title: {
				required: "Please enter title."
			},
			file: {
				required: "Please select file."
			}


		},
		submitHandler: function (form) {

				form.submit();

		}

	});
});