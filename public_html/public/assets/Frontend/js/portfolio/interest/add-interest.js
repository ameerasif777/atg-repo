function removeSocial(div_id)
{
	$("#id" + div_id).remove();
}
var social_link_counter = 1;
function appendSkills()
{
	var hash = $('#ms3').val();
	var curr = $('#skills').val();
	var existingTags = ($('#skills').val()).split(",");
	var isHasTagFound = false;
	if (curr.length > 0) {
		jQuery(existingTags).each(function (indx, ele) {
			if (ele.toLowerCase() == hash.toLowerCase())
			{
				isHasTagFound = true;
			}

		});
	}
	if (hash != '' && !isHasTagFound) {
		var id = "'" + hash + "'";
		var apnd = '<ul class ="hash-tag" id="' + hash + '">';
		apnd += '<li><a href="javascript:void(0);">' + hash + ' &nbsp;<span class = "glyphicon glyphicon-remove text-danger" onclick="deletetag(' + id + ')"> </span>';
		apnd += '</a></li></ul>';
		$('#append_hashtag').append(apnd);
		$('#ms3').val('');
		var hash_value = $('#skills').val();
		if (hash_value != '') {
			var v = hash_value + ',' + hash;
		} else {
			var v = hash;
		}
		$('#skills').val(v);
	} else if (isHasTagFound)
	{
		alert("This skill is already added.");
		$('#ms3').val('');
	}
	else {
		alert('Please enter a skill.');
	}
	$('#ms3').val('');
}

function deletetag(tag) {
	$('#' + tag).remove();
	var all_tag = $('#skills').val();
	var arr_tag = all_tag.split(',')
	var str = '';
	var cnt = 0;
	$(arr_tag).each(function (index, value) {
		if (value != tag) {
			if (cnt == 0) {
				str += '' + value;
			} else {
				str += ',' + value;
			}
			cnt++;
		}
	});
	$('#skills').val('');
	$('#skills').val(str);
}


function appendInterest()
{
	var hash = $('#ms4').val();
	var curr = $('#interests').val();
	var existingTags = ($('#interests').val()).split(",");
	var isHasTagFound = false;
	if (curr.length > 0) {
		jQuery(existingTags).each(function (indx, ele) {
			if (ele.toLowerCase() == hash.toLowerCase())
			{
				isHasTagFound = true;
			}

		});
	}
	if (hash != '' && !isHasTagFound) {
		var id = "'" + hash + "'";
		var apnd = '<ul class ="hash-tag" id="' + hash + '">';
		apnd += '<li><a href="javascript:void(0);">' + hash + ' &nbsp;<span class = "glyphicon glyphicon-remove text-danger" onclick="deletetagInterest(' + id + ')"> </span>';
		apnd += '</a></li></ul>';
		$('#append_hashtag_interest').append(apnd);
		$('#ms4').val('');
		var hash_value = $('#interests').val();
		if (hash_value != '') {
			var v = hash_value + ',' + hash;
		} else {
			var v = hash;
		}
		$('#interests').val(v);
	} else if (isHasTagFound)
	{
		alert("This interest is already added.");
		$('#ms4').val('');
	}
	else {
		alert('Please enter an interest.');
	}
	$('#ms4').val('');
}

function deletetagInterest(tag) {
	$('#' + tag).remove();
	var all_tag = $('#interests').val();
	var arr_tag = all_tag.split(',')
	var str = '';
	var cnt = 0;
	$(arr_tag).each(function (index, value) {
		if (value != tag) {
			if (cnt == 0) {
				str += '' + value;
			} else {
				str += ',' + value;
			}
			cnt++;
		}
	});
	$('#interests').val('');
	$('#interests').val(str);
}

$(function () {
	$("#add_social_section").click(function () {
		var str = '<div id="id' + social_link_counter + '">';
		str += '<div class="form-group clearfix" >';
		str += '					<label class="control-label col-md-3 col-sm-3 col-xs-12">Field</label>';
		str += '					<div class="col-md-8 col-sm-8 col-xs-12">';
		str += '						<input type="text" class="form-control field" value="" name="field[]">';
		str += '					</div>';
		str += '				</div>';
		str += '				<div class="form-group clearfix">';
		str += '					<label class="control-label col-md-3 col-sm-3 col-xs-12">Experience</label> ';
		str += '					<div class="col-md-8 col-sm-8 col-xs-12">';
		str += '						<input type="text" class="form-control experience" value="" name="experience[]">';
		str += '					</div>';
		str += '<button type="button" onclick="removeSocial(' + social_link_counter + ')">Remove </button>';
		str += '				</div>';
		str += '				</div>';
		$("#social_div").append(str);
		social_link_counter++;
	});
});

function chkSkills()
{
	if ($.trim($('#skills').val()) != '')
		return true;
	else
		return false;
}
function chkInterests()
{
	if ($.trim($('#interests').val()) != '')
		return true;
	else
		return false;
}
$(document).ready(function () {

	$('#frm_interest_details').validate({
		errorElement: "div",
		rules: {
			institute_id: {
				required: true,
			},
			duration_from: {
				required: true,
			},
			duration_to: {
				required: true,
			},
			degree: {
				required: true,
			},
			course: {
				required: true,
			},
			grade: {
				required: true,
			}

		},
		messages: {
			institute_id: {
				required: "Please select institute or university."
			},
			duration_from: {
				required: "Please select duration(from)."
			},
			duration_to: {
				required: "Please select duration(to)."
			},
			degree: {
				required: "Please select degree."
			},
			course: {
				required: "Please select course."
			},
			grade: {
				required: "Please select grade."
			}


		},
		submitHandler: function (form) {

			if (!chkSkills())
			{
				alert('Please enter skill');
			} else if (!chkInterests())
			{
				alert('Please enter interest');
			} else
			{
				var isValidWebsites = validateWebSitesOnPage();
				if (isValidWebsites)
				{
					form.submit();
				}
			}

		}

	});
});



function validateWebSitesOnPage()
{
	$(".text-danger").remove();
	var isValid = true;
	jQuery(".field").each(function (index, element) {

		var isValidText = validateField(element);
		if (!isValidText)
			isValid = isValidText;
	});
	jQuery(".experience").each(function (index, element) {

		var isValidName = validateExperience(element);
		if (!isValidName)
		{
			isValid = isValidName;
		} 
	});

	return isValid;
}


function validateField(obj)
{

	var chkValid = true;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter field.</div>');
		jQuery(objError).insertAfter(obj);
	}

	return chkValid;
}
function validateExperience(obj)
{
	var chkValid = true;
	var re = /^[0-9]/;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter experience.</div>');
		jQuery(objError).insertAfter(obj);
	}else if (!re.test(jQuery(obj).val()))
	{
		console.log(123);
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter experience in numbers only.</div>');
		jQuery(objError).insertAfter(obj);
	}

	return chkValid;
}



function validateUrl(obj)
{
	var chkValid = true;
	var re = /^[0-9a-z_A-Z.@\s]+$/;
	if (!re.test(jQuery(obj).val()))
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter Link.</div>');
		jQuery(objError).insertAfter(obj);
	}
	return  chkValid;
}

