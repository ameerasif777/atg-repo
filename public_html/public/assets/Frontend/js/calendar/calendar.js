// Variable
var thisDate = 1;
var today = new Date();
var todaysDay = today.getDay() + 1;
var todaysDate = today.getDate();
var todaysMonth = today.getMonth() + 1;
var todaysYear = today.getFullYear();

var firstDate;
var firstDay;
var lastDate;
var numbDays;
var numevents = 0;
var daycounter = 0;
var calendarString = "";

var monthNum_full = todaysMonth;
var yearNum_full = todaysYear;
var monthNum_compact = todaysMonth;
var yearNum_compact = todaysYear;

var atg_events = [];
var order_num = 0;

// Config variable
var wordDay;
var date_start;

function getShortText(text, num) {
    if (text) {
        // Get num of word
        var textArray = text.split(" ");
        if (textArray.length > num) {
            return textArray.slice(0, num).join(" ") + " ...";
        }
        return text;
    }
    return "";
}

function sortEventsByDate(a, b) {
    if (a.date < b.date) {
        return -1;
    } else if (a.date > b.date) {
        return 1;
    } else {
        return 0;
    }
}

function sortEventsByUpcoming(a, b) {
    var today_date = new Date(todaysYear, todaysMonth - 1, todaysDate);
    if (Math.abs(a.date - today_date.getTime()) < Math.abs(b.date - today_date.getTime())) {
        return -1;
    } else if (Math.abs(a.date - today_date.getTime()) > Math.abs(b.date - today_date.getTime())) {
        return 1;
    } else {
        return 0;
    }
}

function getEventsByTime(type) {
    var events = [];
    var today_date = new Date(todaysYear, todaysMonth - 1, todaysDate);
    for (var i = 0; i < atg_events.length; i++) {
        if (type == 'upcoming') {
            if (atg_events[i].date >= today_date.getTime()) {
                events.push(atg_events[i]);
            }
        } else {
            if (atg_events[i].date < today_date.getTime()) {
                events.push(atg_events[i]);
            }
        }
    }
    return events;
}

// Change month or year on calendar
function changedate(btn, layout) {
    if (btn == "prevyr") {
        eval("yearNum_" + layout + "--;");
    } else if (btn == "nextyr") {
        eval("yearNum_" + layout + "++;");
    } else if (btn == "prevmo") {
        eval("monthNum_" + layout + "--;");
    } else if (btn == "nextmo") {
        eval("monthNum_" + layout + "++;");
    } else if (btn == "current") {
        eval("monthNum_" + layout + " = todaysMonth;");
        eval("yearNum_" + layout + " = todaysYear;");
    }

    if (monthNum_full == 0) {
        monthNum_full = 12;
        yearNum_full--;
    } else if (monthNum_full == 13) {
        monthNum_full = 1;
        yearNum_full++
    }

    if (monthNum_compact == 0) {
        monthNum_compact = 12;
        yearNum_compact--;
    } else if (monthNum_compact == 13) {
        monthNum_compact = 1;
        yearNum_compact++
    }

    // Get first day and number days of month
    eval("firstDate = new Date(yearNum_" + layout + ", monthNum_" + layout + " - 1, 1);");
    if (date_start == 'sunday') {
        firstDay = firstDate.getDay() + 1;
    } else {
        firstDay = firstDate.getDay();
    }
    eval("lastDate = new Date(yearNum_" + layout + ", monthNum_" + layout + ", 0);");
    numbDays = lastDate.getDate();

    // Create calendar
    eval("createCalendar(layout, firstDay, numbDays, monthNum_" + layout + ", yearNum_" + layout + ");");

    return;
}

// Create calendar
function createCalendar(layout, firstDay, numbDays, monthNum, yearNum) {
    calendarString = '';
    daycounter = 0;

    calendarString += '<table class=\"calendar-table table table-bordered\">';
    calendarString += '<tbody>';
    calendarString += '<tr>';
    if (layout == 'full') {
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'prevyr\', \'full\')\">« <span class="btn-change-date">' + prev_year + '<\/span><\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'prevmo\', \'full\')\">« <span class="btn-change-date" onClick="changeMonthData('+(monthNum - 1)+')">' + prev_month + '<\/span><\/span><\/td>';
        calendarString += '<td class=\"calendar-title\" colspan=\"3\"><span><i class=\"fa fa-calendar-o\"><\/i>' + wordMonth[monthNum - 1] + '&nbsp;&nbsp;' + yearNum + '<\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'nextmo\', \'full\')\"><span class="btn-change-date" onClick="changeMonthData('+(monthNum + 1)+')">' + next_month + '<\/span> »<\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'nextyr\', \'full\')\"><span class="btn-change-date">' + next_year + '<\/span> »<\/span><\/td>';
    } else {
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'prevyr\', \'compact\')\">«<\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'prevmo\', \'compact\')\">«<\/span><\/td>';
        calendarString += '<td class=\"calendar-title\" colspan=\"3\"><span>' + wordMonth[monthNum - 1] + '&nbsp;&nbsp;' + yearNum + '<\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'nextmo\', \'compact\')\">»<\/span><\/td>';
        calendarString += '<td class=\"calendar-btn\"><span onClick=\"changedate(\'nextyr\', \'compact\')\">»<\/span><\/td>';
    }
    calendarString += '<\/tr>';

    calendarString += '<tr class="active">';
    for (var m = 0; m < wordDay.length; m++) {
        calendarString += '<th>' + wordDay[m].substring(0, 3) + '<\/th>';
    }
    calendarString += '<\/tr>';

    thisDate == 1;

    for (var i = 1; i <= 6; i++) {
        var k = (i - 1) * 7 + 1;
        if (k < (firstDay + numbDays)) {
            calendarString += '<tr>';
            for (var x = 1; x <= 7; x++) {
                daycounter = (thisDate - firstDay) + 1;
                thisDate++;
                if ((daycounter > numbDays) || (daycounter < 1)) {
                    calendarString += '<td class=\"calendar-day-blank\">&nbsp;<\/td>';
                } else {
                    // Saturday or Sunday
                    if (date_start == 'sunday') {
                        if ((x == 1) || (x == 7)) {
                            daycounter_s = '<span class=\"calendar-day-weekend\">' + daycounter + '</span>';
                        } else {
                            daycounter_s = daycounter;
                        }
                    } else {
                        if ((x == 6) || (x == 7)) {
                            daycounter_s = '<span class=\"calendar-day-weekend\">' + daycounter + '</span>';
                        } else {
                            daycounter_s = daycounter;
                        }
                    }

                    if ((todaysDate == daycounter) && (todaysMonth == monthNum)) {
                        calendarString += '<td class=\"calendar-day-normal calendar-day-today\">';
                    } else {
                        calendarString += '<td class=\"calendar-day-normal\">';
                    }
                    if (checkEvents(daycounter, monthNum, yearNum)) {
                        if (layout == 'full') {
                            calendarString += '<div class=\"calendar-day-event\">';
                        } else {
                            calendarString += '<div class=\"calendar-day-event\" onmouseover=\"showTooltip(0, \'compact\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ', this)\" onmouseout=\"clearTooltip(\'compact\', this)\" onclick=\"showEventDetail(0, \'compact\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ')\">';
                        }
                        calendarString += daycounter_s;

                        // Get events of day
                        if (layout == 'full') {
                            var events = getEvents(daycounter, monthNum, yearNum);
                            for (var t = 0; t < events.length; t++) {
                                if (typeof events[t] != "undefined") {
                                    var color = events[t].color ? events[t].color : 1;
                                    var event_class = "one-day";
                                    if (events[t].first_day && !events[t].last_day) {
                                        event_class = "first-day";
                                    } else if (events[t].last_day && !events[t].first_day) {
                                        event_class = "last-day";
                                    } else if (!events[t].first_day && !events[t].last_day) {
                                        event_class = "middle-day";
                                    }

                                    calendarString += '<div class=\"calendar-event-name ' + event_class + ' color-' + color + '\" id=\"' + events[t].id + '\" onmouseover=\"showTooltip(' + events[t].id + ', \'full\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ', this)\" onmouseout=\"clearTooltip(\'full\', this)\" onclick=\"showEventDetail(' + events[t].id + ', \'full\', ' + daycounter + ', ' + monthNum + ', ' + yearNum + ')\"><span class="event-name">' + getShortText(events[t].name, 2) + '</span><\/div>';
                                } else {
                                    var event_fake;
                                    if (typeof events[t + 1] != "undefined") {
                                        if (typeof atg_events[events[t + 1].id - 1] != "undefined") {
                                            event_fake = getShortText(atg_events[events[t + 1].id - 1].name, 2);
                                        } else {
                                            event_fake = "no-name";
                                        }
                                    } else {
                                        event_fake = "no-name";
                                    }
                                    calendarString += '<div class=\"calendar-event-name no-name\">' + event_fake + '</div>';
                                }
                            }
                        } else {
                            calendarString += '<span class="calendar-event-mark"></span>';
                        }

                        // Tooltip
                        calendarString += '<div class=\"atg-event-tooltip\"><\/div>';
                        calendarString += '<\/div>';
                    } else {
                        calendarString += daycounter_s;
                    }
                    calendarString += '<\/td>';
                }
            }
            calendarString += '<\/tr>';
        }
    }
    calendarString += '</tbody>';
    calendarString += '</table>';

    if (layout == 'full') {
        jQuery('.atg-calendar-full').html(calendarString);
    } else {
        jQuery('.atg-calendar-compact').html(calendarString);
    }
    thisDate = 1;
}

// Check day has events or not
function checkEvents(day, month, year) {
    numevents = 0;
    var date_check = new Date(year, Number(month) - 1, day);
    for (var i = 0; i < atg_events.length; i++) {
        var start_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, atg_events[i].day);
        var end_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, Number(atg_events[i].day) + Number(atg_events[i].duration) - 1);
        if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
            numevents++;
        }
    }

    if (numevents == 0) {
        return false;
    } else {
        return true;
    }
}

function getOrderNumber(id, day, month, year) {
    var date_check = new Date(year, Number(month) - 1, day);
    var events = [];
    for (var i = 0; i < atg_events.length; i++) {
        var start_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, atg_events[i].day);
        var end_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, Number(atg_events[i].day) + Number(atg_events[i].duration) - 1);
        if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
            var first_day = (start_date.getTime() == date_check.getTime()) ? true : false;
            var event = {id: atg_events[i].id, name: atg_events[i].name, day: atg_events[i].day, month: atg_events[i].month, year: atg_events[i].year, first_day: first_day};
            events.push(event);
        }
    }

    if (events.length) {
        if (events[0].id == id) {
            var num = order_num;
            order_num = 0;
            return num;
        } else {
            order_num++;
            for (var j = 0; j < events.length; j++) {
                if (events[j].id == id) {
                    return getOrderNumber(events[j - 1].id, events[j - 1].day, events[j - 1].month, events[j - 1].year);
                }
            }

        }
    }

    return 0;
}

// Get events of day
function getEvents(day, month, year) {
    var n = 0;
    var date_check = new Date(year, Number(month) - 1, day);
    var events = [];
    for (var i = 0; i < atg_events.length; i++) {
        var start_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, atg_events[i].day);
        var end_date = new Date(atg_events[i].year, Number(atg_events[i].month) - 1, Number(atg_events[i].day) + Number(atg_events[i].duration) - 1);
        if ((start_date.getTime() <= date_check.getTime()) && (date_check.getTime() <= end_date.getTime())) {
            var first_day = (start_date.getTime() == date_check.getTime()) ? true : false;
            var last_day = (end_date.getTime() == date_check.getTime()) ? true : false;
            var event = {id: atg_events[i].id, name: atg_events[i].name, first_day: first_day, last_day: last_day, color: atg_events[i].color};

            if (!first_day) {
                n = getOrderNumber(atg_events[i].id, atg_events[i].day, atg_events[i].month, atg_events[i].year);
            }

            events[n] = event;
            n++;
        }
    }

    return events;
}

// Show tooltip when mouse over
function showTooltip(id, layout, day, month, year, el) {
    if (layout == 'full') {
        if (atg_events[id].image) {
            var event_image = '<img src="' + atg_events[id].image + '" alt="' + atg_events[id].name + '" />';
        } else {
            var event_image = '';
        }
        if (atg_events[id].time) {
            var event_time = '<div class="event-time">' + atg_events[id].time + '</div>';
        } else {
            var event_time = '';
        }

        // Change position of tooltip
        var index = jQuery(el).parent().find('.calendar-event-name').index(el);
        var count = jQuery(el).parent().find('.calendar-event-name').length;
        var bottom = 32 + ((count - index - 1) * 25);
        jQuery(el).parent().find('.atg-event-tooltip').css('bottom', bottom + 'px');

        jQuery(el).parent().find('.atg-event-tooltip').html('<div class="event-tooltip-item">'
                + event_time
                + '<div class="event-name">' + atg_events[id].name + '</div>'
                + '<div class="event-image">' + event_image + '</div>'
                + '<div class="event-intro">' + getShortText(atg_events[id].description, 10) + '</div>'
                + '</div>'
                );
        jQuery(el).parent().find('.atg-event-tooltip').css('opacity', '1');
        jQuery(el).parent().find('.atg-event-tooltip').css('-webkit-transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
        jQuery(el).parent().find('.atg-event-tooltip').css('transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
    } else {
        jQuery(el).find('.atg-event-tooltip').html('');
        var events = getEvents(day, month, year);
        for (var i = 0; i < events.length; i++) {
            if (typeof events[i] != "undefined") {
                if (atg_events[events[i].id].image) {
                    var event_image = '<img src="' + atg_events[events[i].id].image + '" alt="' + atg_events[events[i].id].name + '" />';
                } else {
                    var event_image = '';
                }
                if (atg_events[events[i].id].time) {
                    var event_time = '<div class="event-time">' + atg_events[events[i].id].time + '</div>';
                } else {
                    var event_time = '';
                }

                jQuery(el).find('.atg-event-tooltip').append('<div class="event-tooltip-item">'
                        + event_time
                        + '<div class="event-name">' + atg_events[events[i].id].name + '</div>'
                        + '<div class="event-image">' + event_image + '</div>'
                        + '<div class="event-intro">' + getShortText(atg_events[events[i].id].description, 10) + '</div>'
                        + '</div>'
                        );
            }
        }
        jQuery(el).find('.atg-event-tooltip').css('opacity', '1');
        jQuery(el).find('.atg-event-tooltip').css('-webkit-transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
        jQuery(el).find('.atg-event-tooltip').css('transform', 'translate3d(0,0,0) rotate3d(0,0,0,0)');
    }
}

// Clear tooltip when mouse out
function clearTooltip(layout, el) {
    if (layout == 'full') {
        jQuery(el).parent().find('.atg-event-tooltip').css('opacity', '0');
        jQuery(el).parent().find('.atg-event-tooltip').css('-webkit-transform', 'translate3d(0,-10px,0)');
        jQuery(el).parent().find('.atg-event-tooltip').css('transform', 'translate3d(0,-10px,0)');
    } else {
        jQuery(el).find('.atg-event-tooltip').css('opacity', '0');
        jQuery(el).find('.atg-event-tooltip').css('-webkit-transform', 'translate3d(0,-10px,0)');
        jQuery(el).find('.atg-event-tooltip').css('transform', 'translate3d(0,-10px,0)');
    }
}

// Show event detail
function showEventList(layout, max_events) {
    // Sort event via upcoming
    var upcoming_events = getEventsByTime('upcoming');
    upcoming_events.sort(sortEventsByUpcoming);
    var past_events = getEventsByTime('past');
    past_events.sort(sortEventsByUpcoming);
    var atg_list_events = upcoming_events.concat(past_events);
    atg_list_events = atg_list_events.slice(0, max_events);

    if (layout == 'full') {
        jQuery('.atg-event-list-full').html('');
        for (var i = 0; i < atg_list_events.length; i++) {
            // Start date
            var day = new Date(atg_list_events[i].year, Number(atg_list_events[i].month) - 1, atg_list_events[i].day);
            if (date_start == 'sunday') {
                var event_day = wordDay[day.getDay()];
            } else {
                if (day.getDay() > 0) {
                    var event_day = wordDay[day.getDay() - 1];
                } else {
                    var event_day = wordDay[6];
                }
            }
            var event_date = wordMonth[Number(atg_list_events[i].month) - 1] + ' ' + atg_list_events[i].day + ', ' + atg_list_events[i].year;

            // End date
            var event_end_time = '';
            if (atg_list_events[i].duration > 1) {
                var end_date = new Date(atg_list_events[i].year, Number(atg_list_events[i].month) - 1, Number(atg_list_events[i].day) + Number(atg_list_events[i].duration) - 1);

                if (date_start == 'sunday') {
                    var event_end_day = wordDay[end_date.getDay()];
                } else {
                    if (end_date.getDay() > 0) {
                        var event_end_day = wordDay[end_date.getDay() - 1];
                    } else {
                        var event_end_day = wordDay[6];
                    }
                }
                var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
                event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
            }

            // Event time
            if (atg_list_events[i].time) {
                var event_time = '<i class="fa fa-clock-o"></i>' + atg_list_events[i].time;
            } else {
                var event_time = '';
            }

            // Event image
            if (atg_list_events[i].image) {
                var event_image = '<img src="' + atg_list_events[i].image + '" alt="' + atg_list_events[i].name + '" />';
            } else {
                var event_image = '';
            }

            jQuery('.atg-event-list-full').append('<div class="event-item">'
                    + '<div class="event-item-left pull-left">'
                    + '<div class="event-image link" onclick="showEventDetail(' + atg_list_events[i].id + ', \'full\', 0, 0, 0)">' + event_image + '</div>'
                    + '</div>'
                    + '<div class="event-item-right pull-left">'
                    + '<div class="event-name link" onclick="showEventDetail(' + atg_list_events[i].id + ', \'full\', 0, 0, 0)">' + atg_list_events[i].name + '</div>'
                    + '<div class="event-date"><i class="fa fa-calendar-o"></i>' + event_day + ', ' + event_date + event_end_time + '</div>'
                    + '<div class="event-time">' + event_time + '</div>'
                    + '<div class="event-intro">' + getShortText(atg_list_events[i].description, 25) + '</div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="cleardiv"></div>'
                    );
        }
    } else {
        jQuery('.atg-event-list-compact').html('');
        for (var i = 0; i < atg_list_events.length; i++) {
            // Start date
            var day = new Date(atg_list_events[i].year, Number(atg_list_events[i].month) - 1, atg_list_events[i].day);
            if (date_start == 'sunday') {
                var event_day = wordDay[day.getDay()];
            } else {
                if (day.getDay() > 0) {
                    var event_day = wordDay[day.getDay() - 1];
                } else {
                    var event_day = wordDay[6];
                }
            }
            var event_date = wordMonth[Number(atg_list_events[i].month) - 1] + ' ' + atg_list_events[i].day + ', ' + atg_list_events[i].year;

            // End date
            var event_end_time = '';
            if (atg_list_events[i].duration > 1) {
                var end_date = new Date(atg_list_events[i].year, Number(atg_list_events[i].month) - 1, Number(atg_list_events[i].day) + Number(atg_list_events[i].duration) - 1);

                if (date_start == 'sunday') {
                    var event_end_day = wordDay[end_date.getDay()];
                } else {
                    if (end_date.getDay() > 0) {
                        var event_end_day = wordDay[end_date.getDay() - 1];
                    } else {
                        var event_end_day = wordDay[6];
                    }
                }
                var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
                event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
            }

            // Event time
            if (atg_list_events[i].time) {
                var event_time = '<i class="fa fa-clock-o"></i>' + atg_list_events[i].time;
            } else {
                var event_time = '';
            }

            // Event image
            if (atg_list_events[i].image) {
                var event_image = '<img src="' + atg_list_events[i].image + '" alt="' + atg_list_events[i].name + '" />';
            } else {
                var event_image = '';
            }

            jQuery('.atg-event-list-compact').append('<div class="event-item">'
                    + '<div class="event-image link" onclick="showEventDetail(' + atg_list_events[i].id + ', \'compact\', 0, 0, 0)">' + event_image + '</div>'
                    + '<div class="event-name link" onclick="showEventDetail(' + atg_list_events[i].id + ', \'compact\', 0, 0, 0)">' + atg_list_events[i].name + '</div>'
                    + '<div class="event-date"><i class="fa fa-calendar-o"></i>' + event_day + ', ' + event_date + event_end_time + '</div>'
                    + '<div class="event-time">' + event_time + '</div>'
                    + '<div class="event-intro">' + getShortText(atg_list_events[i].description, 15) + '</div>'
                    + '</div>'
                    + '<div class="cleardiv"></div>'
                    );
        }
    }
}

// Show event detail
function showEventDetail(id, layout, day, month, year) {
    jQuery('.atg-events-calendar.' + layout + ' .back-calendar').show();
    jQuery('.atg-events-calendar.' + layout + ' .atg-calendar').hide();
    jQuery('.atg-events-calendar.' + layout + ' .atg-event-list').hide();
    jQuery('.atg-events-calendar.' + layout + ' .atg-view-event').fadeIn(1500);

    jQuery('.atg-events-calendar.' + layout + ' .list-view').removeClass('active');
    jQuery('.atg-events-calendar.' + layout + ' .calendar-view').removeClass('active');

    if (layout == 'full') {
        // Start date
        var day = new Date(atg_events[id].year, Number(atg_events[id].month) - 1, atg_events[id].day);
        if (date_start == 'sunday') {
            var event_day = wordDay[day.getDay()];
        } else {
            if (day.getDay() > 0) {
                var event_day = wordDay[day.getDay() - 1];
            } else {
                var event_day = wordDay[6];
            }
        }
        var event_date = wordMonth[Number(atg_events[id].month) - 1] + ' ' + atg_events[id].day + ', ' + atg_events[id].year;

        // End date
        var event_end_time = '';
        if (atg_events[id].duration > 1) {
            var end_date = new Date(atg_events[id].year, Number(atg_events[id].month) - 1, Number(atg_events[id].day) + Number(atg_events[id].duration) - 1);

            if (date_start == 'sunday') {
                var event_end_day = wordDay[end_date.getDay()];
            } else {
                if (end_date.getDay() > 0) {
                    var event_end_day = wordDay[end_date.getDay() - 1];
                } else {
                    var event_end_day = wordDay[6];
                }
            }
            var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
            event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
        }

        // Event time
        if (atg_events[id].time) {
            var event_time = '<i class="fa fa-clock-o"></i>' + atg_events[id].time;
        } else {
            var event_time = '';
        }

        // Event image
        if (atg_events[id].image) {
            var event_image = '<img src="' + atg_events[id].image + '" alt="' + atg_events[id].name + '" />';
        } else {
            var event_image = '';
        }

        // Event location
        if (atg_events[id].location) {
            var event_location = '<i class="fa fa-map-marker"></i>' + atg_events[id].location;
        } else {
            var event_location = '';
        }

        // Event description
        if (atg_events[id].description) {
            var event_desc = '<div class="event-desc">' + atg_events[id].description + '</div>';
        } else {
            var event_desc = '';
        }

        var for_more_link = '';
        if (atg_events[id].event_url) {
            for_more_link = '<a href="' + atg_events[id].event_url + '" class="bar-btn back-calendar pull-right"> More Details </a>';
        }

        jQuery('.atg-view-event-full').html('<div class="event-item">'
                + '<div class="event-image">' + event_image + '</div>'
                + '<div class="event-name">' + atg_events[id].name + '</div>'
                + '<div class="event-date"><i class="fa fa-calendar-o"></i>' + event_day + ', ' + event_date + event_end_time + '</div>'
                + '<div class="event-time">' + event_time + '</div>'
                + '<div class="event-location">' + event_location + '</div>'
                + event_desc
                + '<div class="more-details-btn">' + for_more_link + '</div>'
                + '</div>'
                );
    } else {
        jQuery('.atg-view-event-compact').html('');
        if (day && month && year) {
            var events = getEvents(day, month, year);
        } else {
            var events = [{id: id}];
        }
        for (var i = 0; i < events.length; i++) {
            if (typeof events[i] != "undefined") {
                // Start date
                var day = new Date(atg_events[events[i].id].year, Number(atg_events[events[i].id].month) - 1, atg_events[events[i].id].day);
                if (date_start == 'sunday') {
                    var event_day = wordDay[day.getDay()];
                } else {
                    if (day.getDay() > 0) {
                        var event_day = wordDay[day.getDay() - 1];
                    } else {
                        var event_day = wordDay[6];
                    }
                }
                var event_date = wordMonth[Number(atg_events[events[i].id].month) - 1] + ' ' + atg_events[events[i].id].day + ', ' + atg_events[events[i].id].year;

                // End date
                var event_end_time = '';
                if (atg_events[events[i].id].duration > 1) {
                    var end_date = new Date(atg_events[events[i].id].year, Number(atg_events[events[i].id].month) - 1, Number(atg_events[events[i].id].day) + Number(atg_events[events[i].id].duration) - 1);

                    if (date_start == 'sunday') {
                        var event_end_day = wordDay[end_date.getDay()];
                    } else {
                        if (end_date.getDay() > 0) {
                            var event_end_day = wordDay[end_date.getDay() - 1];
                        } else {
                            var event_end_day = wordDay[6];
                        }
                    }
                    var event_end_date = wordMonth[Number(end_date.getMonth())] + ' ' + end_date.getDate() + ', ' + end_date.getFullYear();
                    event_end_time = ' - ' + event_end_day + ', ' + event_end_date;
                }

                // Event time
                if (atg_events[events[i].id].time) {
                    var event_time = '<i class="fa fa-clock-o"></i>' + atg_events[events[i].id].time;
                } else {
                    var event_time = '';
                }

                // Event image
                if (atg_events[events[i].id].image) {
                    var event_image = '<img src="' + atg_events[events[i].id].image + '" alt="' + atg_events[events[i].id].name + '" />';
                } else {
                    var event_image = '';
                }

                // Event location
                if (atg_events[events[i].id].location) {
                    var event_location = '<i class="fa fa-map-marker"></i>' + atg_events[events[i].id].location;
                } else {
                    var event_location = '';
                }

                // Event description
                if (atg_events[events[i].id].description) {
                    var event_desc = '<div class="event-desc">' + atg_events[events[i].id].description + '</div>';
                } else {
                    var event_desc = '';
                }

                jQuery('.atg-view-event-compact').append('<div class="event-item">'
                        + '<div class="event-image">' + event_image + '</div>'
                        + '<div class="event-name">' + atg_events[events[i].id].name + '</div>'
                        + '<div class="event-date"><i class="fa fa-calendar-o"></i>' + event_day + ', ' + event_date + event_end_time + '</div>'
                        + '<div class="event-time">' + event_time + '</div>'
                        + '<div class="event-location">' + event_location + '</div>'
                        + event_desc
                        + '</div>'
                        );
            }
        }
    }
}

jQuery(document).ready(function() {
    // Init calendar full
    if (jQuery('.atg-events-calendar.full').length) {
        jQuery('.atg-events-calendar.full').html('<div class="events-calendar-bar">'
                + '<span class="bar-btn calendar-view active"><i class="fa fa-calendar-o"></i>' + calendar_view + '</span>'
                + '<span class="bar-btn list-view"><i class="fa fa-list"></i>' + list_view + '</span>'
                + '<div class="color-circle-align" ><span class="circle-color-green" data-trigger="hover" data-toggle="popover" data-placement="top" data-content=""></span> Meetup'
                + '<span class="circle-color-blue" data-trigger="hover" data-toggle="popover" data-placement="top" data-btn-change-datecontent=""></span> Event'
                + '<span class="circle-color-orange" data-trigger="hover" data-toggle="popover" data-placement="top" data-content=""></span> Event Im going to'
                + '<span class="circle-color-sky-blue" data-trigger="hover" data-toggle="popover" data-placement="top" data-content=""></span> Meetup  Im going </div><div class="clearfix"></div>'
                + '<span class="bar-btn back-calendar pull-right active"><i class="fa fa-caret-left"></i>' + back + '</span>'
                + '</div>'
                + '<div class="cleardiv"></div>'
                + '<div class="atg-events-calendar-wrap">'
                + '<div class="atg-calendar-full atg-calendar"></div>'
                + '<div class="atg-event-list-full atg-event-list"></div>'
                + '<div class="atg-view-event-full atg-view-event"></div>'
                + '</div>'
                );
    }

    // Init calendar compact
    if (jQuery('.atg-events-calendar.compact').length) {
        jQuery('.atg-events-calendar.compact').html('<div class="events-calendar-bar">'
                + '<span class="bar-btn calendar-view active"><i class="fa fa-calendar-o"></i>' + calendar_view + '</span>'
                + '<span class="bar-btn list-view"><i class="fa fa-list"></i>' + list_view + '</span>'
                + '<span class="bar-btn back-calendar pull-right active"><i class="fa fa-caret-left"></i>' + back + '</span>'
                + '</div>'
                + '<div class="cleardiv"></div>'
                + '<div class="atg-events-calendar-wrap">'
                + '<div class="atg-calendar-compact atg-calendar"></div>'
                + '<div class="atg-event-list-compact atg-event-list"></div>'
                + '<div class="atg-view-event-compact atg-view-event"></div>'
                + '</div>'
                );
    }

    // Show - Hide view
    jQuery('.atg-events-calendar .back-calendar').hide();
    jQuery('.atg-event-list').hide();
    jQuery('.atg-view-event').hide();

    jQuery('.atg-events-calendar').each(function(index) {
        // Hide switch button
        var switch_button = (typeof jQuery(this).attr('data-switch') != "undefined") ? jQuery(this).attr('data-switch') : 'show';
        if (switch_button == 'hide') {
            jQuery(this).find('.calendar-view').hide();
            jQuery(this).find('.list-view').hide();

            // Change css of button back
            jQuery(this).find('.events-calendar-bar').css('position', 'relative');
            jQuery(this).find('.back-calendar').css({"position": "absolute", "margin-top": "15px", "right": "15px"});
            jQuery(this).find('.atg-view-event').css('padding-top', '60px');
        }
    });

    // Set wordDay
    date_start = (typeof jQuery('.atg-events-calendar').attr('data-start') != "undefined") ? jQuery('.atg-events-calendar').attr('data-start') : 'sunday';
    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
    }

    // Get events from json file or ajax php
    var source = (typeof jQuery('.atg-events-calendar').attr('data-source') != "undefined") ? jQuery('.atg-events-calendar').attr('data-source') : 'json';
    if (source == 'json') { // Get events from json file : events/events.json
        var events_json = $('#json_calendar_data').val();
        jQuery.getJSON(events_json, function(data) {
            for (var i = 0; i < data.items.length; i++) {
                var event_date = new Date(data.items[i].year, Number(data.items[i].month) - 1, data.items[i].day);
                data.items[i].date = event_date.getTime();
                atg_events.push(data.items[i]);
            }

            // Sort events by date
            atg_events.sort(sortEventsByDate);

            for (var j = 0; j < atg_events.length; j++) {
                atg_events[j].id = j;
                if (!atg_events[j].duration) {
                    atg_events[j].duration = 1;
                }
            }

            // Create calendar
            changedate('current', 'full');
            changedate('current', 'compact');

            jQuery('.atg-events-calendar').each(function(index) {
                // Initial view
                var initial_view = (typeof jQuery(this).attr('data-view') != "undefined") ? jQuery(this).attr('data-view') : 'calendar';
                if (initial_view == 'list') {
                    jQuery(this).find('.list-view').click();
                }
            });
        });
    } else { // Get events from php file via ajax

        var data_id = '';
        var data_source = (typeof jQuery('.atg-events-calendar').attr('data-index') != "undefined") ? jQuery('.atg-events-calendar').attr('data-index') : '';
        var source_url = 'calendar-ajax';
        var start_date = '';
        var end_date = '';
        if (data_source == 'meetup-recurring-dates') {
            source_url = $('#base_url').val() + '/meetup-recurring-dates-data';
            data_id = $('#meetup_id').val();
            start_date = $('#start_date').val();
            end_date = $('#end_date').val();
        } else if (data_source == 'event-recurring-dates') {
            source_url = $('#base_url').val() + '/event-recurring-dates-data';
            data_id = $('#meetup_id').val();
            start_date = $('#start_date').val();
            end_date = $('#end_date').val();
        }


        jQuery.ajax({
            url: source_url,
            dataType: 'json',
            data: {
                id: data_id,
                start_date: start_date,
                end_date: end_date
            },
            beforeSend: function() {
                jQuery('.atg-calendar').html('<div class="loading"><img src="assets/Frontend/img/LoaderIcon.gif" /></div>');
                
            },
            success: function(data) {
                if (data != '') {
                    $('.atg-events-calendar').css('display','block');
                    for (var i = 0; i < data.length; i++) {
                        var event_date = new Date(data[i].year, Number(data[i].month) - 1, data[i].day);
                        data[i].date = event_date.getTime();
                        atg_events.push(data[i]);
                    }

                    // Sort events by date
                    atg_events.sort(sortEventsByDate);

                    for (var j = 0; j < atg_events.length; j++) {
                        atg_events[j].id = j;
                        if (!atg_events[j].duration) {
                            atg_events[j].duration = 1;
                        }
                    }

                    // Create calendar
                    changedate('current', 'full');
                    changedate('current', 'compact');

                    jQuery('.atg-events-calendar').each(function(index) {
                        // Initial view
                        var initial_view = (typeof jQuery(this).attr('data-view') != "undefined") ? jQuery(this).attr('data-view') : 'calendar';
                        if (initial_view == 'list') {
                            jQuery(this).find('.list-view').click();
                        }
                    });
                }
                else{
                       // Create calendar
                    changedate('current', 'full');
                    changedate('current', 'compact');

                    jQuery('.atg-events-calendar').each(function(index) {
                        // Initial view
                        var initial_view = (typeof jQuery(this).attr('data-view') != "undefined") ? jQuery(this).attr('data-view') : 'calendar';
                        if (initial_view == 'list') {
                            jQuery(this).find('.list-view').click();
                        }
                    });
                }
            }
        });
    }

    // Click - Calendar view btn
    jQuery('.atg-events-calendar .calendar-view').click(function() {
        jQuery(this).parents('.atg-events-calendar').find('.back-calendar').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-event-list').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-view-event').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-calendar').fadeIn(1500);

        jQuery(this).parents('.atg-events-calendar').find('.list-view').removeClass('active');
        jQuery(this).parents('.atg-events-calendar').find('.calendar-view').addClass('active');
    });

    // Click - List view btn
    jQuery('.atg-events-calendar .list-view').click(function() {
        jQuery(this).parents('.atg-events-calendar').find('.back-calendar').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-calendar').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-view-event').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-event-list').fadeIn(1500);

        jQuery(this).parents('.atg-events-calendar').find('.calendar-view').removeClass('active');
        jQuery(this).parents('.atg-events-calendar').find('.list-view').addClass('active');

        var layout = jQuery(this).parents('.atg-events-calendar').attr('class') ? jQuery(this).parents('.atg-events-calendar').attr('class') : 'full';
        var max_events = jQuery(this).parents('.atg-events-calendar').attr('data-events') ? jQuery(this).parents('.atg-events-calendar').attr('data-events') : 1000;
        if (layout.indexOf('full') != -1) {
            showEventList('full', max_events);
        } else {
            showEventList('compact', max_events);
        }
    });

    // Click - Back calendar btn
    jQuery('.atg-events-calendar .back-calendar').click(function() {
        jQuery(this).parents('.atg-events-calendar').find('.back-calendar').hide();
        jQuery(this).parents('.atg-events-calendar').find('.atg-view-event').hide();

        var initial_view = (typeof jQuery(this).parents('.atg-events-calendar').attr('data-view') != "undefined") ? jQuery(this).parents('.atg-events-calendar').attr('data-view') : 'calendar';
        if (initial_view == 'calendar') {
            jQuery(this).parents('.atg-events-calendar').find('.atg-calendar').fadeIn(1500);

            jQuery(this).parents('.atg-events-calendar').find('.list-view').removeClass('active');
            jQuery(this).parents('.atg-events-calendar').find('.calendar-view').addClass('active');
        } else {
            jQuery(this).parents('.atg-events-calendar').find('.atg-event-list').fadeIn(1500);

            jQuery(this).parents('.atg-events-calendar').find('.calendar-view').removeClass('active');
            jQuery(this).parents('.atg-events-calendar').find('.list-view').addClass('active');
        }
    });

});