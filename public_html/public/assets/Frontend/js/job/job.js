jQuery(document).ready(function() {
    /**job form validation**/
    jQuery("#form_post_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true,
                remote: {
                    url: jQuery("#base_url").val() +"/check-job-title",
//                                            type: "get"
                }
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            salary_range: {
                required: true,
                alphanumeric: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            phone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            telephone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            website: {
                website: true,
                required: true,
            },
            site_url: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title.",
                remote: "Job with same name already exist.",
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            min: {
                required: "Please enter education."
            },
            qualification: {
                required: "Please enter education and press tab.",
            },
            skills: {
                required: "Please enter skills and press tab."
            },
            role: {
                required: "Please enter role."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                alphanumeric: "Please enter valid range.",
            },
            phone: {
                required: "Please enter mobile no.",
                digits: "Please enter valid mobile number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                digits: "Please enter valid telephone number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                website: "URL should be in www.atg.world",
                required: "Please enter website URL.",
            },
            site_url: {
                url: "This is the company's URL on ATG. URL should be in http://www.atg.world/your-company-username",
                required: "Please enter your company's URL on ATG. URL should be in http://www.atg.world/your-company-username",
            },
            link: {
                url: "URL should be in http://www.atg.world",
            },
            file: {
                required: "Please attach file.",
            },
        },
        submitHandler: function(form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });
    jQuery("#form_post_job_e").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true,
                remote: {
                    url: jQuery("#base_url").val() +"/check-job-title-edit",
                    data:{'old_title': function(ele) { return $('#old_title').val(); } },
//                                            type: "get"
                }
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            salary_range: {
                required: true,
                alphanumeric: true
            },
//            tags: {
//                required: true
//            },
//            'sub_groups[]': {
//                required: true
//            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            phone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            telephone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            website: {
                website: true,
                required: true,
            },
            site_url: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title.",
                remote: "Job with same name already exist.",
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            min: {
                required: "Please enter education."
            },
            qualification: {
                required: "Please enter education and press tab.",
            },
            skills: {
                required: "Please enter skills and press tab."
            },
            role: {
                required: "Please enter role."
            },
//            tags: {
//                required: "Please enter tags."
//            },
//            'sub_groups[]': {
//                required: "Please select maximum five sub-groups."
//            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                alphanumeric: "Please enter valid range.",
            },
            phone: {
                required: "Please enter mobile no.",
                digits: "Please enter valid mobile number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                digits: "Please enter valid telephone number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                website: "URL should be in format www.atg.world",
                required: "Please enter website URL.",
            },
            site_url: {
                url: "This is the company's URL on ATG. URL should be in http://www.atg.world/your-company-username",
                required: "Please enter your company's URL on ATG. URL should be in http://www.atg.world/your-company-username",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach file.",
            },
        },
        submitHandler: function(form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#form_edit_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true
            },
            location: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            salary_range: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 10
            },
            telephone: {
                required: true,
                number: true,
                minlength: 10
            },
            website: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
            'social[]': {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            min: {
                required: "Please enter experience."
            },
            qualification: {
                required: "Please enter education.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit."
            },
            telephone: {
                required: "Please enter telephone no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit."
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter name of website.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach file."
            },
            'social[]': {
                required: "Please select social site to post job.",
            },
        },
        submitHandler: function(form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });


    jQuery("#form_apply_job").validate({
        ignore: ':hidden',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true,
            },
            location: {
                required: true
            },
            cmp_name: {
                required: true
            },
            experiance: {
                required: true
            },
            qualification: {
                required: true,
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
            },
            salary_range: {
                required: true,
                number: true
            },
            telephone: {
                required: true,
                number: true,
                minlength: 10,
            },
            website: {
                required: true,
                url: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            experiance: {
                required: "Please enter experience."
            },
            qualification: {
                required: "Please enter qualification & enter tab.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter name of website.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach resume.",
            },
        },
        submitHandler: function(form) {
            jQuery("#btn_job_apply").attr('disabled', true);
            form.submit();
        }


    });
    jQuery("#create_job_alert").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            skills: {
                required: true
            }

        },
        messages: {
            skills: {
                required: "Please enter skills."
            },
        },
        submitHandler: function(form) {
            jQuery("#btn_job_post").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#upload_resumes").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            file: {
                required: true,
            }

        },
        messages: {
            file: {
                required: "Please attach resume.",
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: 'resume-count',
                data: '',
                dataType: 'json'
            }).success(function(msg) {
                var resume_count = msg.resume_count[0].count
                if (resume_count == 3) {
                    if (confirm("If you uploading new resume then old resume get removed automatically.Do you want to continue?")) {
                        form.submit();
                    } else {
                        return false;
                    }
                } else {
                    form.submit();
                }
            })
        }
    });
    jQuery.validator.addMethod("specialChars", function(value, element) {
        var regex = new RegExp("^[a-zA-Z0-9@_-],+");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");

    jQuery.validator.addMethod("onlytext", function(value, element, param) {
        var reg = /^[a-zA-Z ]*$/;
        if (reg.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter name only.");
    jQuery.validator.addMethod("website", function(value, element, param) {
        var reg = /[www]?\..*?\.(com|net|org)/;
        if (reg.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter valid website.");

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[0-9$.,RSrs]*$/.test(value);
    }, "Letters, numbers, and underscores only please");
    
    
 
    
});
   function jobList(){
    $.ajax({
        url: $("#base_url").val() +"/list-job",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
            
        }
    });
}
    function jobApply(){
    $.ajax({
        url: $("#base_url").val() +"/jobs-applied",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function userSaveJobList(){
    $.ajax({
        url: $("#base_url").val() +"/save-list-job",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
        }
    });
}
function myJobList(){
    $.ajax({
        url: $("#base_url").val() +"/job-list",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
        }
    });
}
