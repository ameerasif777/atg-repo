// JavaScript Document
$(document).ready(function (e) {

    $("#frm_user_login").validate({
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true,
                specialChars: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please enter syncampus id .",
                specialChars: "Please enter valid syncampus id.",
                email: "Please enter a valid syncampus id address.",
            },
            password: {
                required: "Please enter password.",
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });
    $("#frm_institute_login").validate({
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true,
                specialChars: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please enter an email address.",
                specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
            },
            password: {
                required: "Please enter password.",
            }
        },
        submitHandler: function (form) {
            $("#btn_submit").val('Please wait ...').attr('disabled', 'disabled');
            form.submit();
        }
    });

    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9.@_-]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");


    $("#check_box").css({display: "block", opacity: "0", height: "0", width: "0", "float": "right"});
});