function articleList(){
    $.ajax({
        url: $("#base_url").val() +"/list-article",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
            
        }
    });
}

function myArticleList(){
    $.ajax({
        url: $("#base_url").val() +"/mypost",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            $('.grey-overlay-loader').hide();
        }
    });
}