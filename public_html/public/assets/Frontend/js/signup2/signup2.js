jQuery(document).ready(function () {
    /**job form validation**/
    jQuery("#frm_user_registration2").validate({
        ignore: ':hidden',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            joining_type: {
                required: true,
            },
            
        },
        messages: {
            joining_type: {
                required: "Please select joining type."
            },
            
        },
        submitHandler: function (form) {
            jQuery("#btn_submit").attr('disabled', true);
            form.submit();
        }


    });
    
});