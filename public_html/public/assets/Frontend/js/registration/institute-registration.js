// JavaScript Document
$(document).ready(function (e) {
    $("#frm_institute_registration").validate({
        errorElement: "div",
        rules: {
            institute_name: {
                required: true,
                notNumber: true
            },
            institute_type: {
                required: true,
            },
            institue_address: {
                required: true
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            user_email: {
                required: true,
                email: true,
                specialChars: true,
                remote: {
                    url: jQuery("#base_url").val() + "/institute/register/check-user-email",
                    type: "get"
                }
            },
            contact_person_name: {
                required: true
            },
            contact_number: {
                required: true,
				digits: true,
				minlength:10,
				maxlength:13
            },
            user_password: {
                required: true,
                minlength: 6,
                password_strenth: false
            },
            cnf_user_password: {
                required: true,
                equalTo: '#user_password'
            },
            terms: {
                required: true
            }
        },
        messages: {
            institute_name: {
                required: "Please enter institute name.",
                notNumber: "Please enter valid name."
            },
            institute_type: {
                required: "Please select institute type.",
            },
            institue_address: {
                required: "Please enter institute address."
            },
            state_id: {
                required: "Please select state."
            },
            city_id: {
                required: "Please select city."
            },
            user_email: {
                required: "Please enter an email address.",
                specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
                remote: "Email address already exists."
            },
            contact_person_name:{
                required: "Please enter contact person name.",
            },
            contact_number:{
                required: "Please enter contact number.",
				minlength:"Please enter valid contact number.",
				maxlength:"Please enter valid contact number."
            },
            user_password: {
                required: "Please enter password.",
                minlength: "Please enter atleast 6 characters."
            },
            cnf_user_password: {
                required: "Please enter the confirm password.",
                equalTo: "Password and confirm password does not match."
            },
            terms: {
                required: "Please accept terms and condition."
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });
    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9.@_-]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");
    jQuery.validator.addMethod("notNumber", function (value, element, param) {
        var reg = /[0-9]/;
        if (reg.test(value)) {
            return false;
        } else {
            return true;
        }
    }, "Number is not permitted");
    jQuery.validator.addMethod('chk_username_field', function (value, element, param) {
        if (value.match('^[0-9a-zA-Z-._-]{5,20}$')) {
            return true;
        } else {
            return false;
        }

    }, "");

    jQuery.validator.addMethod("password_strenth", function (value, element) {
        return isPasswordStrong(value, element);
    }, "Password must be of minimum 6 characters.");

    $("#check_box").css({display: "block", opacity: "0", height: "0", width: "0", "float": "right"});
});