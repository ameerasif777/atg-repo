function articleList() {
    $.ajax({
        url: $("#base_url").val() + "/list-article",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Article List";
            $('.grey-overlay-loader').hide();

        }
    });
}

function myArticleList() {
    $.ajax({
        url: $("#base_url").val() + "/list-my-article",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My Article List";
            $('.grey-overlay-loader').hide();
        }
    });
}

function educationList(){
    $.ajax({
        url: $("#base_url").val() +"/list-education",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Eduction List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}

function myEducationList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-education",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My Eduction List";
            $('.grey-overlay-loader').hide();
        }
    });
}


function eventList(){
    $.ajax({
        url: $("#base_url").val() +"/list-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Event List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function userSaveEventList(){
    $.ajax({
        url: $("#base_url").val() +"/save-list-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Save event List";
            $('.grey-overlay-loader').hide();
        }
    });
}
function myEventList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My event List";
            $('.grey-overlay-loader').hide();
        }
    });
}


function meetupList(){
    $.ajax({
        url: $("#base_url").val() +"/list-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Meetup List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function userSaveMeetupList(){
    $.ajax({
        url: $("#base_url").val() +"/save-list-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Save Meetup List";
            $('.grey-overlay-loader').hide();
        }
    });
}
function myMeetupList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My meetup List";
            $('.grey-overlay-loader').hide();
        }
    });
}

function qriousList(){
    $.ajax({
        url: $("#base_url").val() +"/list-question",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Qrious List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function myQriousList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-question",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My qrious List";
            $('.grey-overlay-loader').hide();
        }
    });
}

