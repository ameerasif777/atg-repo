
// JavaScript Document
$(document).ready(function (e) {

    $("#frm_forgot_send").validate({
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true,
                specialChars: true,
                remote: {
                    url: jQuery("#base_url").val() + "/check-email-exist",
                    type: "get"
                }
            },
        },
        messages: {
            email: {
                required: "Please enter an email address.",
                specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
                remote: "This email is not registered with this site.",
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#frm_login").validate({
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true,
 //               specialChars: true,
                remote: {
                    url: jQuery("#base_url").val() + "/check-email-exist",
                    type: "post"
                },
            },
            password: {
                required: true,
                remote: {
                    url: jQuery("#base_url").val() + "/check-password",
                    type: "post",
                    data: {
                        user_email:function(){
                            return $("#email").val();
                        }
                    }
                },
                minlength: 6
            }
        },
        messages: {
            email: {
                required: "Please enter an email address.",
 //               specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
                remote: "This email is not registered with this site.",
            }, password: {
                required: "Please enter password.",
                minlength: "Please enter atleast 6 characters.",
                remote: "Please enter valid password.",
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#frm_reset_password").validate({
        errorElement: "div",
        rules: {
            new_password: {
                required: true,
                minlength: 6,
                password_strenth: false,
            },
            cnf_password: {
                required: true,
                equalTo: '#new_password'
            },
        },
        messages: {
            new_password: {
                required: "Please enter password.",
                minlength: "Please enter atleast 6 characters."
            },
            cnf_password: {
                required: "Please enter the confirm password.",
                equalTo: "Password and confirm password does not match."
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#frm_user_registration1").validate({
        errorElement: "div",
        rules: {
            first_name: {
                required: true,
                notNumber: true
            },
            last_name: {
                required: true,
                notNumber: true
            },
            user_type: {
                required: true,
            },
            user_email: {
                required: true,
                email: true,
//                specialChars: true,
                remote: {
                    url: jQuery("#base_url").val() + "/auth/register/check-user-email",
                    type: "get"
                }
            },
            user_password: {
                required: true,
                minlength: 6,
                password_strenth: false
            },
            cnf_user_password: {
                required: true,
                equalTo: '#user_password'
            },
            professional_headline: {
                required: true,
            },
            head_shot: {
                accept: 'jpg|jpeg|png|gif'
            }
        },
        messages: {
            first_name: {
                required: "Please enter first name.",
                notNumber: "Please enter valid name."
            },
            last_name: {
                required: "Please enter last name.",
                notNumber: "Please enter valid name."
            },
            user_email: {
                required: "Please enter an email address.",
//                specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
                remote: "Email address already exists."
            },
            user_password: {
                required: "Please enter password.",
                minlength: "Please enter atleast 6 characters.",
                password_strenth:"(Password must be of minimum 6 characters)"
            },
            cnf_user_password: {
                required: "Please enter the confirm password.",
                equalTo: "Password and confirm password does not match."
            },
            professional_headline: {
                required: 'Please enter the professinal headline.',
            }, head_shot: {
                accept: 'only jpg|jpeg|png|gif file formats are allowed only.'
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });
    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9.@_-]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");
    jQuery.validator.addMethod("notNumber", function (value, element, param) {
        var reg = /[0-9]/;
        if (reg.test(value)) {
            return false;
        } else {
            return true;
        }
    }, "Number is not permitted");
    jQuery.validator.addMethod('chk_username_field', function (value, element, param) {
        if (value.match('^[0-9a-zA-Z-._-]{5,20}$')) {
            return true;
        } else {
            return false;
        }

    }, "");

    jQuery.validator.addMethod("password_strenth", function (value, element) {
        return isPasswordStrong(value, element);
    }, "(Password must be of minimum 6 characters)");

    $("#check_box").css({display: "block", opacity: "0", height: "0", width: "0", "float": "right"});
});