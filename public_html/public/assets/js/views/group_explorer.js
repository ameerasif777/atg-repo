'use strict';

let isMobile;

window.onload = window.onresize = function () {
    if (window.innerWidth <= 640) {
        $('.group-container').addClass('mobile').removeClass('desktop');
        isMobile = true;

    } else {
        $('.group-container').addClass('desktop').removeClass('mobile');
        isMobile = false;
    }
};

$(document).on('mouseenter', '[data-group-id]', function () {
    if (isMobile) return;

    let $group = $(this),
        group_name = $group.attr('title'),
        group_id = $group.data('group-id'),
        $groupWrapper = $group.parent(),
        $groupContainer = $groupWrapper.parent();

    $groupWrapper.nextAll().remove();
    $groupContainer.append(`<span class="group-loading"><i class="fa fa-spin fa-spinner"></i> Loading niche groups for ${group_name}</span>`);

    $.ajax({
        method:  'POST',
        url: '/ajax/get-group-details-by-id',
        data: {
            group_id: group_id
        },
        success: function (res) {
            $groupWrapper.nextAll().remove();

            if (res.child_groups.length === 0) {
                $groupContainer.append(`<span class="no-group">No niche groups for ${group_name}</span>`);
                return;
            }

            $groupContainer.append(getRightSide(res));
        }
    });
});

function getRightSide(group)
{

    let $wrapper = $(`<div class="group-wrapper"></div>`);

    $.each(group.child_groups, function (index, group) {
        let $content = $(`<div class="group" data-group-id="${group.id}" title="${group.group_name}"></div>`);
            $content.append(`<img class="group-icon" src="`+window.backgrurl+`icon/${group.icon_img}" alt="" onerror=this.src='`+window.backimgnotfoundurl+`group_image.png'>`)
                    .append(`<span class="group-name">${group.group_name}</span>`)
                    .append('<span class="group-left-icon"></span>');

        if (group.child_groups.length > 0) {
            $content.append(`<span class="group-status"> (${group.child_groups.length} niche groups)</span>`)
                .addClass('has-child');
        } else if (window.joined_groups.indexOf(group.id) >= 0) {
            $content.addClass('has-joined');
        }

        $wrapper.append($content);
    });

    return $wrapper;
}

$(document).on('click', '[data-group-id]', function () {
    if (!isMobile) return;

    let $group = $(this),
        group_id = $(this).data('group-id');

    if (! $group.hasClass('has-child')) return;
    if ($group.hasClass('opened')) {
        $group.removeClass('opened');
        $(`[data-parent-group-id=${group_id}]`).remove();
        return;
    }

    $group.append(`<i class="fa fa-spin fa-spinner" style="vertical-align: middle"></i>`);

    $.ajax({
        method:  'POST',
        url: '/ajax/get-group-details-by-id',
        data: {
            group_id: group_id
        },
        success: function (res) {
            let $wrapper = getBottomSide(res);
            $wrapper.css('padding-left', '10px');
            $wrapper.css('margin', '-5px 0 10px 20px');
            $wrapper.css('border-left', '2px solid #53b150');
            $group.addClass('opened').after($wrapper);
            $group.find('.fa.fa-spin').remove();
        }
    });
});

function getBottomSide(group)
{
    let $wrapper = $(`<div data-parent-group-id="${group.id}" class="group-wrapper"></div>`);

    $.each(group.child_groups, function (index, group) {
        let $content = $(`<div class="group" data-group-id="${group.id}" title="${group.group_name}"></div>`);
        $content.append(`<img class="group-icon" src="`+window.backgrurl+`icon/${group.icon_img}" alt="" onerror=this.src='`+window.backimgnotfoundurl+`group_image.png'>`)
                .append(`<span class="group-name">${group.group_name}</span>`)
                .append(`<span class="group-left-icon"></span>`);

        if (group.child_groups.length > 0) {
            $content.append(`<span class="group-status"> (${group.child_groups.length} niche groups)</span>`)
                .addClass('has-child');
        } else if (window.joined_groups.indexOf(group.id) >= 0) {
            $content.addClass('has-joined');
        }

        $wrapper.append($content);
    });

    return $wrapper;
}

$(document).on('click', '.group-left-icon', function () {
    let group = $(this).parent(),
        group_id = group.data('group-id');

    if (group.hasClass('has-child')) return;

    group.append(' <i class="fa fa-spin fa-spinner"></i>');

    if (window.joined_groups.indexOf(group_id) < 0) {
        $.ajax({
            method: 'GET',
            url: `/join-group/${btoa(group_id)}/MQ==`,
            success: function (res) {
                res = JSON.parse(res);
                if (res.success == 1) {
                    group.addClass('has-joined');
                }
                window.joined_groups.push(group_id);
            },
            fail: function (err) {
                console.log(err)
            }
        }).always(function () {
            group.find('.fa-spinner').remove();
        });
    } else {
        $.ajax({
            method: 'GET',
            url: `leave-group/${btoa(group_id)}/${btoa(window.user_id)}`,
            success: function (res) {
                res = JSON.parse(res);
                if (res.success == 1) {
                    group.removeClass('has-joined');
                }
                window.joined_groups.splice(window.joined_groups.indexOf(group_id), 1);
            },
            fail: function (err) {
                console.log(err)
            }
        }).always(function () {
            group.find('.fa-spinner').remove();
        });
    }
});

$(document).on('click', '.group .group-name', function () {
    if ( ! $(this).parent().hasClass('has-child'))  {
        window.open(`/go/${ $(this).parent().attr('title') }`);
    }
});
