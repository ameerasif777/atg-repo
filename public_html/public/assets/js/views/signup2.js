'use strict';

let selectedGroupsInStepOne = [];

$(document).on('click', '.searched-group, .trending-group, .major-group', function() {
    let groupId = $(this).data('group-id');

    if (groupId === undefined || isNaN(groupId)) return;

    if (selectedGroupsInStepOne.indexOf(groupId) >= 0) {
        $('[data-group-id="' + groupId +'"]').removeClass('selected');
        selectedGroupsInStepOne.splice(selectedGroupsInStepOne.indexOf(groupId), 1);

        removeChildGroupsOf(groupId);

    } else if (selectedGroupsInStepTwo().indexOf(groupId) >= 0) {
        $('[data-group-id="' + groupId +'"]').removeClass('selected');

    } else {
        $('[data-group-id="' + groupId +'"]').addClass('selected');
        selectedGroupsInStepOne.push(groupId);

        addChildGroupsOf(groupId);
    }

    updateGroupCounter();
});

$(document).on('click', '.child-group', function () {
    let groupId = $(this).data('group-id');

    if (groupId === undefined || isNaN(groupId)) return;


    if ($(this).hasClass('selected')) {
        $('[data-group-id="' + groupId +'"]').removeClass('selected');
    } else {
        $('[data-group-id="' + groupId +'"]').addClass('selected');
    }

    if (selectedGroupsInStepOne.indexOf(groupId) >= 0) {
        selectedGroupsInStepOne.splice(selectedGroupsInStepOne.indexOf(groupId), 1);
    }

    updateGroupCounter();
});

function updateGroupCounter()
{
    $('#selected-group-count').text(selectedGroupsInStepOne.length);

    $('.child-group-container').each(function(i, element) {
        $(element).find('.selected-child-counter').text($(element).find('.child-group.selected').length)
    });

    console.log(selectedGroupsInStepOne, selectedGroupsInStepTwo());
}

function selectedGroupsInStepTwo() {
    let groups = [];

    $('.child-group.selected').each(function (i, element) {
        groups.push($(element).data('group-id'));
    });

    return groups;
}

$(document).on('click', '#group-search-bar .fa-remove, #search-overlay', function () {
    $('#group-search-bar input').val('').trigger('keyup');
});

$('#group-search-bar input').keyup(function () {
    let group_name = $(this).val().trim(),
        $groupSearchResult = $('#group-search-result');

    if (group_name.length === 0) {
        $('#group-search-bar i').removeClass('fa-remove').addClass('fa-search');
        $('#search-overlay').fadeOut();
        $groupSearchResult.empty().slideUp();
        return;
    }

    $('#group-search-bar i').removeClass('fa-search').addClass('fa-remove');
    $('#search-overlay').fadeIn();

    let groups = searchGroupsByName(group_name);

    if (groups.length === 0) {
        $groupSearchResult.empty().append('<p style="text-align: center">No groups on ATG for ' + group_name +'</p>').slideDown();
        return;
    }

    $groupSearchResult.empty();

    for (let i = 0; i < groups.length; i++) {
        let temp = $(`
            <div class="searched-group" data-group-id="` + groups[i].id + `">
            ` + groups[i].group_name + `
            </div>
        `);

        $groupSearchResult.append(temp);

        if (selectedGroupsInStepOne.indexOf(temp.data('group-id')) >= 0 || selectedGroupsInStepTwo().indexOf(temp.data('group-id')) >= 0) {
            temp.addClass('selected');
        }
    }

    $groupSearchResult.slideDown();
});

function searchGroupsByName(group_name)
{
    let groups = [];

    $.ajax({
        async: false,
        method: 'POST',
        url: 'ajax/search-groups-by-name',
        data: {
            group_name: group_name,
            log_if_not_found: true
        },
        success: function (res) {
            if (Array.isArray(res)) {
                groups = res;
            }
        }
    });

    return groups;
}

function getGroupDetailsByID(group_id)
{
    let group = null;

    $.ajax({
        async: false,
        method: 'POST',
        url: 'ajax/get-group-details-by-id',
        data: {
            group_id: group_id
        },
        success: function (res) {
            group = res
        }
    });

    return group;
}

function addChildGroupsOf(parentGroupId)
{
    let parentGroup = getGroupDetailsByID(parentGroupId);

    if (parentGroup === undefined) return;

    let childGroups = parentGroup.child_groups;

    if (childGroups === undefined || childGroups.length === 0) return;

    if ($('[data-parent-group-id="' + parentGroup.id +'"]').length === 0) {
        let $childGroupContainer = $(`
            <div class="row material-shadow-1 child-group-container" data-parent-group-id="` + parentGroup.id + `">
                <div class="col-xs-12 group-header">
                    ` + `Select Niche Groups in ` + parentGroup.group_name + ` (<span class="selected-child-counter">0</span>)` +`
                </div>
            </div>
        `);

        let $childGroupWrapper = $('<div class="col-xs-12 child-group-wrapper"></div>');

        $.each(childGroups, function (i, childGroup) {
            let $childGroup = $('<div class="child-group selected" data-group-id="' + childGroup.id + '">' + childGroup.group_name + '</div>');

            // if (selectedGroupsInStepOne.indexOf(childGroup.id) >= 0) {
            //     $childGroup.addClass('selected');
            // }

            $('[data-group-id="' + childGroup.id +'"]').addClass('selected');

            $childGroupWrapper.append($childGroup);
        });

        $childGroupContainer.append($childGroupWrapper);
        $('#signup2-step2').append($childGroupContainer);
    }

    $.each(childGroups, function (i, childGroup) {
        if (childGroup.child_groups && childGroup.child_groups.length > 0) {
            addChildGroupsOf(childGroup.id);
        }
    });
}

function removeChildGroupsOf(parentGroupId)
{
    let $parentGroup = $('[data-parent-group-id="' + parentGroupId +'"]');

    if ($parentGroup.length === 0) {
        return;

    } else {
        $parentGroup.remove();
    }

    let parentGroup = getGroupDetailsByID(parentGroupId);

    if (parentGroup === null) return;

    let childGroups = parentGroup.child_groups;

    if (childGroups.length === 0) return;

    $.each(childGroups, function (i, childGroup) {

        $('[data-group-id="' + childGroup.id +'"]').removeClass('selected');

        if (childGroup.child_groups && childGroup.child_groups.length > 0) {
            removeChildGroupsOf(childGroup.id);
        }
    });
}


function gotoStep1()
{
    $('#signup2-step1').show();
    $('#signup2-step2').hide();
}
function gotoStep2()
{
    if (selectedGroupsInStepOne.length === 0) {
        swal({
            type: 'info',
            title: '',
            text: 'Search and join one interest group.',
        });
        return;
    }

    if ($('#signup2-step2').text() === '') {
        gotoStep3();
        return;
    }

    $('#signup2-step1').hide();
    $('#signup2-step2').show();

    let timer = 10; // in seconds

    swal({
        html: 'ATG has some groups further divided into niche groups so you receive only meaningful updates.<br><br>' +
              'Now narrow down your interests by selecting niche interests.<br><br>' +
              'By default, every niche group is selected.',
        confirmButtonText: 'OK (<span id="swal-timer">' + timer + '</span>)',
        animation: false,
        timer: timer * 1000,
        footer: '<a href="https://google.com" target="_blank">Learn More</a>&nbsp;- How groups and niche groups work on ATG',
    }).then((result) => {
        if (result.value) {
            clearInterval(refreshInterval);
        }
    });

    let refreshInterval = setInterval(function () {
        $('#swal-timer').text(--timer);
        if (timer === 0) {
            clearInterval(refreshInterval);
        }
    }, 1000);
}
function gotoStep3()
{
    let selectedGroups = selectedGroupsInStepOne.concat(selectedGroupsInStepTwo()).unique();

    if (selectedGroups.length === 0) {
        swal({
            type: '',
            title: '',
            text: 'Please select at last one niche group.',
            animation: false
        });
        return;
    }

    post_to_url('/signup2', {
        child_groups: selectedGroups.join(',')
    });
}

function post_to_url(path, params, method = 'post')
{
    let form = document.createElement("form");
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for(let key in params) {
        if(params.hasOwnProperty(key)) {
            let hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

Array.prototype.unique = function() {
    let a = this.concat();
    for(let i=0; i<a.length; ++i) {
        for(let j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

/*
 * load Major groups using AJAX
 */

let ajaxMajorGroupOffset = 0;
let ajaxMajorGroupLimit = 10;
let ajaxEndOfMajorGroup = false;

function ajaxLoadMajorGroup()
{
    if (ajaxEndOfMajorGroup === true) return;

    $.ajax({
        method: 'POST',
        url: 'ajax/get-major-groups',
        data: {
            offset: ajaxMajorGroupOffset,
            limit: ajaxMajorGroupLimit,
        }

    }).success(function (groups) {
        if (groups.length < ajaxMajorGroupLimit) {
            ajaxEndOfMajorGroup = true;
            $('#major-group-loading-icon').fadeOut();
        }

        $.each(groups, function (i, group) {
            $('.major-group-wrapper').append(`
                <div class="major-group material-shadow-2" data-group-id="${group.id}" title="${group.group_name}">
                    <img src="public/assets/Backend/img/group_img/icon/${group.icon_img}" alt="" onerror="this.src='public/assets/Backend/img/group_image.png';">
                    <span>${group.group_name}</span>
                </div>
            `);
        });

        resizeGroupIcon();
    }).fail(function (err) {
        // TODO: log
    });

    ajaxMajorGroupOffset += ajaxMajorGroupLimit;
}

$(document).ready(function() {
    ajaxLoadMajorGroup();

    $('#content').scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            ajaxLoadMajorGroup();
        }
    });
});

$(window).resize(resizeGroupIcon);

// auto resize major group icon
function resizeGroupIcon()  {
    if (window.innerWidth >= 350) {
        $('.major-group').css({
            'height': 140,
            'width': 140
        });
    } else {
        $('.major-group').css({
            'height': 130,
            'width': 130
        });
    }
}