// JavaScript Document
$(document).ready(function(e) {
     jQuery.validator.addMethod("specialChars", function( value, element )      {
        var regex = new RegExp("^[a-zA-Z0-9.@_-]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
    }, "please enter a valid value for the field.");
	$("#frm_user_details").validate({
		errorElement: "div",
		errorPlacement: function(label, element) {
			if(element[0].name=="admin_privileges[]")
			{
				label.insertAfter("#pre_div");
			}
			else
			{
				label.insertAfter(element);
			}
		},
		rules: {
			bio_detail:{
				required:true,
                                notNumber:true
			}
			
		},
		messages:{
			bio_detail:{
				required:"Please make valid entry.",
                                notNumber:"Please enter valid word."
			}
			
		},
		 submitHandler: function (form) {
         	 $("#btnSubmit").hide();
                 $('#loding_image').show();
                 form.submit();
        }
	});
	jQuery.validator.addMethod("notNumber", function(value, element, param) {
                       var reg = /[0-9]/;
                       if(reg.test(value)){
                             return false;
                       }else{
                               return true;
                       }
                }, "Number is not permitted");
	jQuery.validator.addMethod('chk_username_field', function(value, element, param) {
		 if ( value.match('^[0-9a-zA-Z-._-]{5,20}$') ) {
			return true;
		} else {
			 return false;
		}
		
	},"");
	
	jQuery.validator.addMethod("password_strenth", function(value, element) {
		return isPasswordStrong(value, element);
	}, "Password must be strong");
	
	$("#check_box").css({display:"block",opacity:"0",height:"0",width:"0","float":"right"});
});