$(document).ready(function(eve) {
    
    $('body').click( function(e) {
        if(e.target.className !== "selectedclass") {
            if(e.target.className !== "custom_placeholder"){
            if(e.target.id !== "groupPanel" && !$('#groupPanel').has(e.target).length)
            {
                $('#groupPanel').hide();
            }}
        }
    });
    
    var ref_id = ''; 
    var request;
    
    $('.group-editor .bootstrap-tagsinput').on('keyup', 'input',function(event) {
        var text = $(this).val();
        var parentid = $(this).parent().parent().attr("id");
        urlid = parentid.split('_')[2];
        if(urlid == undefined)
        {
            urlid = '';
        }
        else
        {
            urlid = '_'+urlid;
        }
        ref_id = Math.floor(Date.now()); 
        
        if(request && request.readystate != 4){
                request.abort();
        }
        if(text != '')
        {           
            request = $.get($('#baseurl').val() + '/admin/searchgroup/'+text+'/'+ref_id, function(msg) {
                var rid = msg['ref_id'];
                var res = msg['res'];
                if(res.length > 0 && rid == ref_id)
                {
                    $('#panelContent'+ urlid).empty();
                    $('#panelContent'+ urlid).append('<ul></ul>');
                    $.each(res, function(key, value){
                        if(value['sub_groups'].length > '0')
                        {
                            var str = '<li><a href="javascript:void(0);" onclick=\'addGroup("' + value['id'] + '", "' + value['group_name'] + '", "' + value['id'] + urlid +'", "' + urlid + '")\' ><div class="col-sm-9">'+ value['group_name'] + '</div></a><div class="col-sm-3"><input type="checkbox"  id="'+ value['id'] + urlid + '" name="" value=\'' + JSON.stringify(value['sub_groups'])+ '\'/>'+ ' +' + value['sub_groups'].length +'</div></li>';
                        }
                        else
                        {
                           var str = '<li><a href="javascript:void(0);" onclick=\'addGroup("' + value['id'] + '", "' + value['group_name'] + '", "","' + urlid + '")\'><div class="col-sm-12">'+ value['group_name'] + '</div></a></li>';                   
                        }
                       $('#panelContent' + urlid + ' ul').append(str); 
                    });
                    $('#groupPanel'+ urlid).show();
                }
                else
                {
                    $('#groupPanel' + urlid).hide();
                    $('#panelContent'+ urlid).empty();
                }
            });
        }
        else
        {
            $('#groupPanel' + urlid).hide();
            $('#panelContent'+ urlid).empty();
        }
    });
    
    $('.text-editor').on('itemRemoved', function(event) {
        var parentid = $(this).attr("id");
        urlid = parentid.split('_')[2];
        if(urlid == undefined)
        {
            urlid = '';
        }
        else
        {
            urlid = '_'+urlid;
        }
        
        var group_ar = $('#group_inp' + urlid).val();
        $("#user_group").val(group_ar.split(','));
      });
});

function addGroup(tagid, tagname, checkboxid, urlid)
{
    var subgrp_ar = [];
    if($('#' + checkboxid).is(':checked'))
    {
        var subgrp_val = $('#' + checkboxid).val();
        subgrp_ar = JSON.parse(subgrp_val);
    }
    $('.bootstrap-tagsinput > input').val('');
    $('#group_inp' + urlid).tagsinput('add', { id: tagid, text: tagname});
    $.each(subgrp_ar, function(key, value){
        $('#group_inp' + urlid).tagsinput('add', { id: value['id'], text: value['group_name']});
    });
    $('#groupPanel' + urlid).hide(); 
    var group_ar = $('#group_inp' + urlid).val();
    $("#user_group").val(group_ar.split(','));
}


function addSuggestedGroup(tag_json, urlid)
{
    tag_array = JSON.parse(tag_json);
    $.each(tag_array, function(key, value){
        $('#group_inp' + urlid).tagsinput('add', { id: value['id'], text: value['group_name']});
    });
    var group_ar = $('#group_inp' + urlid).val();
    $("#user_group").val(group_ar.split(','));
}

function initializeTagsInput()
{
    $('.text-editor').tagsinput({
            itemValue: 'id', 
            itemText: 'text'});
    $('.bootstrap-tagsinput > input').off("focusout");
}
