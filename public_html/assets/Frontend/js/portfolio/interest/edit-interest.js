

function chkSkills()
{
	if ($.trim($('#skills').val()) != '')
		return true;
	else
		return false;
}
function chkInterests()
{
	if ($.trim($('#interests').val()) != '')
		return true;
	else
		return false;
}
$(document).ready(function () {

	$('#frm_interest_details').validate({
		errorElement: "div",
		rules: {
			institute_id: {
				required: true,
			},
			duration_from: {
				required: true,
			},
			duration_to: {
				required: true,
			},
			degree: {
				required: true,
			},
			course: {
				required: true,
			},
			grade: {
				required: true,
			}

		},
		messages: {
			institute_id: {
				required: "Please select institute or university."
			},
			duration_from: {
				required: "Please select duration(from)."
			},
			duration_to: {
				required: "Please select duration(to)."
			},
			degree: {
				required: "Please select degree."
			},
			course: {
				required: "Please select course."
			},
			grade: {
				required: "Please select grade."
			}


		},
		submitHandler: function (form) {

			if (!chkSkills())
			{
				alert('Please enter skill');
			} else if (!chkInterests())
			{
				alert('Please enter interest');
			} else
			{
				var isValidWebsites = validateWebSitesOnPage();
				if (isValidWebsites)
				{
					form.submit();
				}
			}

		}

	});
});



function validateWebSitesOnPage()
{
	$(".text-danger").remove();
	var isValid = true;
	jQuery(".field").each(function (index, element) {

		var isValidText = validateField(element);
		if (!isValidText)
			isValid = isValidText;
	});
	jQuery(".experience").each(function (index, element) {

		var isValidName = validateExperience(element);
		if (!isValidName)
		{
			isValid = isValidName;
		} 
	});

	return isValid;
}


function validateField(obj)
{

	var chkValid = true;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter field.</div>');
		jQuery(objError).insertAfter(obj);
	}

	return chkValid;
}
function validateExperience(obj)
{
	var chkValid = true;
	var re = /^[0-9]/;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter experience.</div>');
		jQuery(objError).insertAfter(obj);
	}else if (!re.test(jQuery(obj).val()))
	{
		console.log(123);
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter experience in numbers only.</div>');
		jQuery(objError).insertAfter(obj);
	}

	return chkValid;
}



function validateUrl(obj)
{
	var chkValid = true;
	var re = /^[0-9a-z_A-Z.@\s]+$/;
	if (!re.test(jQuery(obj).val()))
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter Link.</div>');
		jQuery(objError).insertAfter(obj);
	}
	return  chkValid;
}

