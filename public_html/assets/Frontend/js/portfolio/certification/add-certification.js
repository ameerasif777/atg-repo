$(document).ready(function () {
	
	$('#frm_certification_details').validate({
		errorElement: "div",
		rules: {
			institute_id: {
				required: true,
			},
			duration_from: {
				required: true,
			},
			duration_to: {
				required: true,
			},
			certification_name: {
				required: true,
			},
			grade: {
				required: true,
			}

		},
		messages: {
			
			institute_id: {
				required: "Please select institute or university."
			},
			duration_from: {
				required: "Please select duration(from)."
			},
			duration_to: {
				required: "Please select duration(to)."
			},
			certification_name: {
				required: "Please enter certification name."
			},
			grade: {
				required: "Please enter grade."
			}


		},
		submitHandler: function (form) {

				form.submit();

		}

	});
});