jQuery(document).ready(function () {
    /**contact us form validation**/
    jQuery("#team").validate({
        errorElement: 'div',
        ignore:'',
        errorClass: 'text-danger',
        rules: {
            team_name: {
                required: true
            },
            project_name: {
                required: true
            },
            project_description: {
                required: true,
            },
            "member[]":{
                required:true
            },
             profile_pic: {
                    required: true,
                },
                

        },
        messages: {
            team_name: {
                required: "Please enter team name."
            },
            project_name: {
                required: "Please enter project name."
            },
            project_description: {
                required: "Please enter project description.",
            },
            "member[]":{
              required:"Please enter atleast one member."  
            },
             profile_pic: {
                    required: "Please select team profile image.",
                },
               
           
        },
        submitHandler: function (form) {
            jQuery("#btn_create_team").attr('disabled', true);
            form.submit();
        }


    });

    jQuery.validator.addMethod("onlytext", function (value, element, param) {
        var reg = /^[a-zA-Z ]*$/;
        if (reg.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter name only.");
});