

$(document).ready(function () {
	$('#frm_add_personal_info').validate({
		errorElement: "div",
		rules: {
			dob: {
				required: true,
			},
			profile_pic: {
				accept: "jpg|png|jpeg|JPG|PNG|JPEG",
			},
			cover_pic: {
				accept: "jpg|png|jpeg|JPG|PNG|JPEG",
			},
			institute_id: {
				required: true
			},
			company_name: {
				required: true
			},
			contact_no: {
				required: false,
				digits: true
			},
			current_place: {
				required: true,
			},
			home_town: {
				required: true,
			}

		},
		messages: {
			profile_pic: {
				accept: "Please select jpg,png or jpeg type of image."
			},
			cover_pic: {
				accept: "Please select jpg,png or jpeg type of image."
			},
			dob: {
				required: "Please select date of birth."
			},
			institute_id: {
				required: "Please select institute."
			},
			company_name: {
				required: "Please enter company."
			},
			contact_no: {
				required: "Please enter valid contact no."
			},
			current_place: {
				required: "Please enter your current place."
			},
			home_town: {
				required: "Please enter your home town."
			}


		},
		submitHandler: function (form) {
			var isValidWebsites = validateWebSitesOnPage();
			if (isValidWebsites)
			{
				$(".loader").show();
				form.submit();

				setTimeout(function () {
					$(".loader").fadeOut("slow");
				}, 500);
			}
		}

	});

	jQuery.validator.addMethod("validatePhone", function (value, element, param) {


		if (value.match('^[0-9]+$')) {
			return true;
		} else {
			return false;
		}
	}, "Please enter valid contact no.");
});
function validateWebSitesOnPage()
{
	$(".text-danger").remove();
	var isValid = true;
	jQuery(".social_link").each(function (index, element) {

		var isValidText = validateLink(element);
		if (!isValidText)
			isValid = isValidText;

		if (isValid)
		{
			var isValidUrl = validateUrl(element);
			if (!isValidUrl)
				isValid = isValidUrl;
		}


	});
	jQuery(".site_name").each(function (index, element) {

		var isValidName = validateName(element);
		if (!isValidName)
			isValid = isValidName;
	});
	return isValid;
}


function validateName(obj)
{

	var chkValid = true;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter social site name.</div>');
		jQuery(objError).insertAfter(obj);
	}

	return chkValid;
}
function validateLink(obj)
{
	var chkValid = true;
	if (jQuery.trim(jQuery(obj).val()).length < 1)
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter social link.</div>');
		jQuery(objError).insertAfter(obj);
	}
	return chkValid;
}



function validateUrl(obj)
{
	var chkValid = true;
	var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
	if (!re.test(jQuery(obj).val()))
	{
		chkValid = false;
		var objError = $('<div class="text-danger" generated="true" class="error">Please enter valid link.</div>');
		jQuery(objError).insertAfter(obj);
	}
	return  chkValid;
}
