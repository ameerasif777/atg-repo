function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);
  
    for (var i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
function displayNotification(){
    if('serviceWorker' in navigator){
        var opt={
            body:'success install web push'
        };
        navigator.serviceWorker.ready.then(function(swi){
            swi.showNotification('frow service worker hence cool',opt);
        })
    }
    else{
        return;
    }
    // new Notification('subscribed',opt);
}
function subscribepush(){
    if(!('serviceWorker' in navigator)){
        return;
    }
    var sw;
    navigator.serviceWorker.ready.then(function(swr){
        sw=swr;
        return swr.pushManager.getSubscription();

    }).then(function(sub){
        if(sub===null){
            publicenckey=window.vapid_public_key;
            console.log(publicenckey);
            publickey=urlBase64ToUint8Array(publicenckey);
            return sw.pushManager.subscribe({
                userVisibleOnly:true,
                applicationServerKey:publickey
            })
        }
        else{
            console.log(JSON.stringify(sub));
            return null;
        }
    }).then(function(subs){
        if(subs!==null){
        
            return fetch('/push-subscription/'+window.user_id,
                {
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json',
                        'Accept':'application/json',
                    },
                body:JSON.stringify(subs)
                }
            

            ).then(res=>{
                console.log(res.json())
                displayNotification();
                return res;
            })

        }
    }).catch(e=>console.log(e));
}

function permitNotification(){
    if('Notification' in window){

   
    
        Notification.requestPermission(function(res){
            if(res!=='granted'){
                console.log('not granted',res);
            }
            else{
                console.log('granted',res);
                subscribepush();
                
            }
        })
    }
}
permitNotification();