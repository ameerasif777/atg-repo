// JavaScript Document

$(document).ready(function (e) {

    $("#account_setting").validate({
        errorElement: "div",
        rules: {
            user_name: {
                required: true,
                chk_uname:true,
                remote: {
                    url: jQuery("#base_url").val() + "/check-user-name",
                    type: "get",
                    data: {
                        type: "edit",
                        old_username: jQuery('#old_username').val()
                    }
                }
            },
            first_name: {
                required: true,
                notNumber: true
            },
            last_name: {
                required: true,
                notNumber: true
            },
            user_email: {
                required: true,
                email: true,
                specialChars: true,
                remote: {
                    url: jQuery("#base_url").val() + "/check-user-email",
                    type: "get"
                }
            },
            mob_no: {
                //required: true,
                minlength: 10,
                maxlength: 12,
                notCharacter: true,
                matches:"^[0-9-+()]*$",
            },
            phone_no: {
//                required: true,
                minlength: 10,
                maxlength: 10,
                notCharacter: true,
                matches:"^[0-9-+()]*$",
            },
            location: {
                required: true
            },
            about_me: {
                required: true,
                maxlength: 50
            },
            tagline: {
//                required: true,
                maxlength: 50
            },
            profession: {
                required: true,
                maxlength: 20
            },
            user_type: {
                required: true
            },
            user_password: {
                required: true,
                minlength: 6,
                password_strenth: false
            },
            cnf_user_password: {
                required: true,
                equalTo: '#user_password'
            },
            terms: {
                required: true
            }
        },
        messages: {
            user_name: {
                required: "Please enter username.",
                remote: "Username already exists.",
                chk_uname: "Username field should not contain space."
            },
            first_name: {
                required: "Please enter first name.",
                notNumber: "Please enter valid name."
            },
            last_name: {
                required: "Please enter last name.",
                notNumber: "Please enter valid name."
            },
            user_email: {
                required: "Please enter an email address.",
                specialChars: "Please enter valid email address.",
                email: "Please enter a valid email address.",
                remote: "Email address already exists."
            }, mob_no: {
                //required: "Please enter mobile number.",
                number: "Please enter valid  mobile number.",
                minlength: "Please enter mobile number atleast 10 numbers.",
                maxlength: "Please enter mobile number atmost 12 numbers.",
            },
            phone_no: {
//                required: "Please enter phone number.",
                number: "Please enter valid  phone number.",
                minlength: "Please enter phone number  10 numbers.",
                maxlength: "Please enter phone number 10 numbers.",
            },
            location: {
                required: "Please enter location."
            },
            about_me: {
                required: "Please enter about me.",
                maxlength: 'Only 50  characters are allowed.'
            },
            tagline: {
//                required: "Please enter tagline.",
                maxlength: 'Only 50 characters are allowed.'
            },
            profession: {
                required: "Please enter profession.",
                maxlength: 'Only 20 characters are allowed.'
            },
            user_password: {
                required: "Please enter password.",
                minlength: "Please enter atleast 6 characters."
            },
            cnf_user_password: {
                required: "Please enter the confirm password.",
                equalTo: "Password and confirm password does not match."
            },
            terms: {
                required: "Please accept terms and condition."
            },
            user_type: {
                required: "Please select account type."
            },
            dob: {
                required: "Please select date of birth."
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });

    $("#user_bio").validate({
        errorElement: "div",
        rules: {
            gender: {
                required: true
            },
            height: {
                required: true
            },
            weight: {
                required: true
            },
            body_type: {
                required: true
            },
            hair_color: {
                required: true
            },
            eye_color: {
                required: true
            },
            ethnicity_type: {
                required: true
            }
        },
        messages: {
            gender: {
                required: "Please enter gender."
            },
            height: {
                required: "Please enter height."
            },
            weight: {
                required: "Please enter weight."
            },
            body_type: {
                required: "Please enter an body type."
            },
            hair_color: {
                required: "Please enter hair color."
            },
            eye_color: {
                required: "Please enter eye color."
            },
            ethnicity_type: {
                required: "Please enter ethnicity type."
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });


    $("#edit_notifitcation_setting").validate({
        errorElement: "div",
        rules: {
            follower: {
                required: true
            },
            connection: {
                required: true
            },
            profile_picture_updated: {
                required: true
            },
            cover_photo_updated: {
                required: true
            }
        },
        messages: {follower: {
                required: "Please select follower notification."
            },
            connection: {
                required: "Please select connection notification."
            },
            profile_picture_updated: {
                required: "Please select profile picture notification."
            },
            cover_photo_updated: {
                required: "Please select cover photo notification."
            }
        },
        submitHandler: function (form) {
            //$("#btn_register").hide();
            //$('#loding_image').show();
            form.submit();
        }
    });


    $("#frm_password_setting").validate({
        errorElement: "div",
        rules: {
            new_password: {
                required: true,
                password_strenth: false,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                equalTo:'#new_password'
            }
        },
        messages: {
            new_password: {
                required: "Please enter new password.",
                minlength: "Please enter atleast 6 characters."
            },
            confirm_password: {
                equalTo: "Password must be same as above.",
                required: "Please enter confirm password"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });




    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9.@_-]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");
    
    jQuery.validator.addMethod("chk_uname", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9-._-]*$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "please enter a valid value for the field.");
    
    jQuery.validator.addMethod("notNumber", function (value, element, param) {
        var reg = /[0-9]/;
        if (reg.test(value)) {
            return false;
        } else {
            return true;
        }
    }, "Number is not permitted");
    jQuery.validator.addMethod('chk_username_field', function (value, element, param) {
        if (value.match('^[0-9a-zA-Z-._-]{5,20}$')) {
            return true;
        } else {
            return false;
        }

    }, "");

    jQuery.validator.addMethod("password_strenth", function (value, element) {
        return isPasswordStrong(value, element);
    }, "(Password should be of atleast 6 characters)");

    jQuery.validator.addMethod("notCharacter", function (value, element, param) {
        var reg = /[a-zA-Z]/;
        if (reg.test(value)) {
            return false;
        } else {
            return true;
        }
    }, "Characters is not permitted");

    $("#check_box").css({display: "block", opacity: "0", height: "0", width: "0", "float": "right"});
});