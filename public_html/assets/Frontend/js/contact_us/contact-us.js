jQuery(document).ready(function() {
    /**contact us form validation**/
    jQuery("#form_contact_us").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            first_name: {
                required: true,
                onlytext:true
            },
            
            email: {
                required: true,
                email: true
            },
            contact_feedback: {
                required: true
            },
            message: {
                required: true
            }

        },
        messages: {
            first_name: {
                required:"Please enter name."
            },
           
            email: {
                required: "Please enter your email.",
                email:"Please enter valid email."
            },
            contact_feedback: {
                required: "Please select feedback."
            },
            message: {
                required: "Please enter message."
            }
        },
        submitHandler: function(form) {
            jQuery("#btn_contact_us").attr('disabled',true);            
            form.submit();
        }
        

    });
    
    jQuery.validator.addMethod("onlytext", function(value, element, param) {
                       var reg = /^[a-zA-Z ]*$/;
                       if(reg.test(value)){
                             return true;
                       }else{
                               return false;
                       }
                }, "Please enter name only.");
});