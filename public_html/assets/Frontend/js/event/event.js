function eventList(){
    $.ajax({
        url: $("#base_url").val() +"/list-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Event List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function userSaveEventList(){
    $.ajax({
        url: $("#base_url").val() +"/save-list-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Save event List";
            $('.grey-overlay-loader').hide();
        }
    });
}
function myEventList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-event",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My event List";
            $('.grey-overlay-loader').hide();
        }
    });
}