function educationList(){
    $.ajax({
        url: $("#base_url").val() +"/list-education",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Eduction List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}

function myEducationList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-education",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My Eduction List";
            $('.grey-overlay-loader').hide();
        }
    });
}


