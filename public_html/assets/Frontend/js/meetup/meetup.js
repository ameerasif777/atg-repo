function meetupList(){
    $.ajax({
        url: $("#base_url").val() +"/list-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Meetup List";
            $('.grey-overlay-loader').hide();
            
        }
    });
}
function userSaveMeetupList(){
    $.ajax({
        url: $("#base_url").val() +"/save-list-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "Save Meetup List";
            $('.grey-overlay-loader').hide();
        }
    });
}
function myMeetupList(){
    $.ajax({
        url: $("#base_url").val() +"/list-my-meetup",
        method: 'get',
        success: function(msg) {
            $("body").html(msg);
            document.title = "My meetup List";
            $('.grey-overlay-loader').hide();
        }
    });
}