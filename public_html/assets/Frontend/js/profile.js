

$(document).ready(function() {

    fullSize();
    applyOrientation();

    $(function() {
        $(".dropdown-menu > li > a.trigger").on("click", function(e) {
            var current = $(this).next();
            var grandparent = $(this).parent().parent();
            if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
                $(this).toggleClass('right-caret left-caret');
            grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
            grandparent.find(".sub-menu:visible").not(current).hide();
            current.toggle();
            e.stopPropagation();
        });
        $(".dropdown-menu > li > a:not(.trigger)").on("click", function() {
            var root = $(this).closest('.dropdown');
            root.find('.left-caret').toggleClass('right-caret left-caret');
            root.find('.sub-menu:visible').hide();
        });
    });

    (function($) {
        $(window).load(function() {

            // $("body").mCustomScrollbar({
            //     theme: "minimal"
            // });

        });
    })(jQuery);

    $('.resp-btn').click(function() {
        $('body').toggleClass('resp-open-menu')
        $(this).toggleClass('cross-icon')
    });

    $('.spnter').click(function() {
        $('body').toggleClass('editoer-open')
        $(this).toggleClass('edit-info')
    });
    
    $('.recomended-f').click(function() {
        $('body').toggleClass('dash-in-menu-open')
        $(this).toggleClass('in-menu-info')
    });
    

    $('#osh-slide').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        autoplay: true,
        animateOut: 'slideOutUp',
        animateIn: 'flipInX',
        items: 1,
        dots: false,
        navText: [
            "<i class='icon-Arrow-Left'></i>",
            "<i class='icon-Arrow-Right'></i>"
        ],
    })

    $('#guest-slidess').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })

    $(function() {
        $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
    });

    $('.gallery-left-top a').click(function() {
        $(this).parent().toggleClass('activer').siblings().removeClass('activer');
        $(this).parent().siblings().find("ul").slideUp();
        if ($(this).parent().hasClass('activer')) {
            $(this).parent().find("ul").slideDown();
        } else {
            $(this).parent().find("ul").slideUp();
        }
    });
    

    function moved() {
        alert('in');
        var owl = $(".owl-carousel").data('owlCarousel');
        if (owl.currentItem + 1 === owl.itemsAmount) {
            alert('THE END');
        }
    }

    $(document).ready(function() {
        if ($('html').hasClass('desktop')) {
            new WOW().init();
        }
    });

    $.scrollIt({
        upKey: 38, // key code to navigate to the next section
        downKey: 40, // key code to navigate to the previous section
        easing: 'ease-in-out', // the easing function for animation
        scrollTime: 1500, // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: null, // function(pageIndex) that is called when page is changed
        topOffset: 0           // offste (in px) for fixed top navigation
    });

 });

$(window).load(function() {
    if (window.innerWidth > 1024) {
        var s = skrollr.init();
    }
});

$(window).resize(function() {
    fullSize();
});

function fullSize() {
    var heights = window.innerHeight;
    jQuery(".fullHt").css('min-height', (heights + 0) + "px");
}

function applyOrientation() {
    if (window.innerHeight > window.innerWidth) {
        $("body").addClass("potrait");
        $("body").removeClass("landscape");
    } else {
        $("body").addClass("landscape");
        $("body").removeClass("potrait");
    }
}

var banner_Ht = window.innerHeight - $('header').innerHeight();
$(window).scroll(function() {
    var sticky = $('body'),
            scroll = $(window).scrollTop();

    if (scroll >= 100)
        sticky.addClass('sticky-header');
    else
        sticky.removeClass('sticky-header');
});




/*----------------------------------------------------*/
/*    Accordians FAQ
 /*----------------------------------------------------*/
$('.accordion').on('shown.bs.collapse', function(e) {
    $(e.target).parent().addClass('active_acc');
    $(e.target).prev().find('.switch').removeClass('fa-plus');
    $(e.target).prev().find('.switch').addClass('fa-minus');
});
$('.accordion').on('hidden.bs.collapse', function(e) {
    $(e.target).parent().removeClass('active_acc');
    $(e.target).prev().find('.switch').addClass('fa-plus');
    $(e.target).prev().find('.switch').removeClass('fa-minus');
});
