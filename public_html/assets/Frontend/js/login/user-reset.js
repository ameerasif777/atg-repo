/* login admin Js */
jQuery(document).ready(function(){
	jQuery("#frm_user_reset").validate({
		errorElement:'div',
		rules: {
			user_password: {
				 required: true,
				 minlength: 6,
				 password_strenth: false
			},
			confim_user_password:{
				required: true,
                equalTo:"#main_password"
			}
		},
		messages: {
			user_password:{
				required: "Please enter password.",
          	    minlength: "Please enter atleast 6 characters."
			},
			confim_user_password: {
				required:"Please enter confirm password.",
                equalTo:"Please enter same password as above"
			}
		}
	});
        
        jQuery.validator.addMethod("password_strenth", function(value, element) {
		return isPasswordStrong(value, element);
	}, "Password must be strong");
        
});