jQuery(document).ready(function () {
    /**job form validation**/
    jQuery("#form_post_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true
                //onlytext: true
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true,
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            salary_range: {
                required: true,
                number: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            phone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            telephone: {
                required: true,
                number: true,
                digits: true,
                minlength: 10,
            },
            website: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            min: {
                required: "Please enter experiance."
            },
            qualification: {
                required: "Please enter education.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                digits: "Please enter valid mobile number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                digits: "Please enter valid telephone number.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter website URL.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach file.",
            },
        },
        submitHandler: function (form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#form_edit_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true,
                onlytext: true
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true,
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            salary_range: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
            },
            telephone: {
                required: true,
                number: true,
                minlength: 10,
            },
            website: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
            'social[]': {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            min: {
                required: "Please enter experiance."
            },
            qualification: {
                required: "Please enter education.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter name of website.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach file.",
            },
            'social[]': {
                required: "Please select social site to post job.",
            },
        },
        submitHandler: function (form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#form_post_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true
                //onlytext: true
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            min: {
                required: true
            },
            qualification: {
                required: true,
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            salary_range: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
            },
            telephone: {
                required: true,
                number: true,
                minlength: 10,
            },
            website: {
                url: true,
                required: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            'social[]': {
                required: true,
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            min: {
                required: "Please enter experiance."
            },
            qualification: {
                required: "Please enter education.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter name of website.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach file.",
            },
            'social[]': {
                required: "Please select social network to post.",
            },
        },
        submitHandler: function (form) {
            jQuery("#btn_job_post").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#form_apply_job").validate({
        ignore: ':hidden',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            title: {
                required: true,
                onlytext: true
            },
            location: {
                required: true
            },
            description: {
                required: true
            },
            cmp_name: {
                required: true
            },
            experiance: {
                required: true
            },
            qualification: {
                required: true,
            },
            skills: {
                required: true
            },
            role: {
                required: true
            },
            tags: {
                required: true
            },
            'sub_groups[]': {
                required: true
            },
            employment_type: {
                required: true
            },
            end_date: {
                required: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
            },
            salary_range: {
                required: true,
                number: true
            },
            telephone: {
                required: true,
                number: true,
                minlength: 10,
            },
            website: {
                required: true,
                url: true,
            },
            link: {
                url: true,
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please enter job title."
            },
            cmp_name: {
                required: "Please enter name of your company."
            },
            email: {
                required: "Please enter your email.",
                email: "Please enter valid email."
            },
            location: {
                required: "Please enter location."
            },
            description: {
                required: "Please enter job description."
            },
            experiance: {
                required: "Please enter experiance."
            },
            qualification: {
                required: "Please enter education.",
            },
            skills: {
                required: "Please enter skills."
            },
            role: {
                required: "Please enter role."
            },
            tags: {
                required: "Please enter tags."
            },
            'sub_groups[]': {
                required: "Please select maximum five sub-groups."
            },
            functional_area: {
                required: "Please enter functional area."
            },
            employment_type: {
                required: "Please select employment type."
            },
            end_date: {
                required: "Please enter application deadline."
            },
            salary_range: {
                required: "Please enter salary range.",
                number: "Please enter number only."
            },
            phone: {
                required: "Please enter mobile no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            telephone: {
                required: "Please enter telephone no.",
                number: "Please enter number only.",
                minlength: "Please enter minimum 10 digit.",
            },
            website: {
                url: "URL should be in http://www.example.com",
                required: "Please enter name of website.",
            },
            link: {
                url: "URL should be in http://www.example.com",
            },
            file: {
                required: "Please attach resume.",
            },
        },
        submitHandler: function (form) {
            jQuery("#btn_job_apply").attr('disabled', true);
            form.submit();
        }


    });
    jQuery("#create_job_alert").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            skills: {
                required: true
            }

        },
        messages: {
            skills: {
                required: "Please enter skills."
            },
        },
        submitHandler: function (form) {
            jQuery("#btn_job_post").attr('disabled', true);
            form.submit();
        }


    });

    jQuery("#upload_resumes").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {
            file: {
                required: true,
            }

        },
        messages: {
            file: {
                required: "Please attach resume.",
            },
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'resume-count',
                data: '',
                dataType: 'json'
            }).success(function (msg) {
                var resume_count = msg.resume_count[0].count
                if (resume_count == 3) {
                    if (confirm("If you uploading new resume then old resume get removed automatically.Do you want to continue?")) {
                        form.submit();
                    } else {
                        return false;
                    }
                } else {
                    form.submit();
                }
            })
        }
    });

    jQuery.validator.addMethod("onlytext", function (value, element, param) {
        var reg = /^[a-zA-Z ]*$/;
        if (reg.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter name only.");
});