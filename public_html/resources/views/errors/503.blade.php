<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>P-1036</title>
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css" />
        <link href="css/owl.theme.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="css/animated.css" rel="stylesheet" type="text/css" />
        <link href="css/main.css" rel="stylesheet" type="text/css" />
        <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    </head>
    <body>


<section class="contact-info-sec">
    <div class="container text-center">
        <div class="error-logo">
                <a href="{!!url()!!}"  ><img src="{{ url('/public/assets/Frontend/img/logo-in.png') }}" alt="SITE-LOGO"/></a>
        </div>
        <img src="{{ url('/public/assets/Frontend/img/503.jpg') }}">
        
    </div>
</section>

 <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/scrollIt.min.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/jquery.nicescroll.js"></script>
        <script src="js/wow.js"></script>
        <script src="js/device.min.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>