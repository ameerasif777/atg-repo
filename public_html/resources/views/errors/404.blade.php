<!DOCTYPE html>
           <?php $finalData['header'] = array(
                "title" => 'Across The Globe (ATG) - 404',
                "keywords" => '',
                "description" => ''
            ); ?>
@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')    
<input type="hidden" id="base_url" name="base_url" value="{!! url('/') !!}">



<section class="contact-info-sec">
    <div class="container text-center">
        <div class="error-logo">
        </div>
        <img src="{{ url('/assets/Frontend/img/404-error-page-not-found.jpg') }}">
        <a style="color:#444" href="{!!url('/')!!}"  > <h2>Click here to go to home page </h2></a>
    </div>
</section>
@include('Frontend.includes.footer')
