<!DOCTYPE html>
           <?php $finalData['header'] = array(
                "title" => 'Across The Globe (ATG) - 500',
                "keywords" => '',
                "description" => ''
            ); ?>
@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')    
<input type="hidden" id="base_url" name="base_url" value="{!! url('/') !!}">



<section class="contact-info-sec">
    <div class="container text-center">
        <div class="error-logo">
                <a href="{!!url('/')!!}"  ><img src="{{ url('/public/assets/Frontend/img/logo-in.png') }}" alt="ATG.world Logo"/></a>
        </div>
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td class="hidden-xs hidden-sm 500__image" style="width: 50.0000%;">
                        <img src="{{ url('/public/assets/Frontend/img/500_robot.png') }}"><br>
                    </td>
                    <td class="500__text" style="width: 50.0000%;">
                        <h2> Sorry, you are seeing this. </h2>
                        <h3> Try back again in some time. <a href="{!!url('/send-feedback')!!}"> Please let us know </a> in detail how exactly did you end up visiting this page </h3>
                        <a href="{!!url('/')!!}"  > <h3>Click here to go to home page </h3></a><br>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
@include('Frontend.includes.footer')
