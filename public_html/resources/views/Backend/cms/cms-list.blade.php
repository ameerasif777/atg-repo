@extends('Backend.layouts.default')
@section('content')  
    <section class="content-header">
        <h1>
            CMS Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> <i class="fa fa-fw fa-file-text"></i> Manage CMS Pages</li>

        </ol>
    </section>
    <section class="content">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
         @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr>
                                        <th width="10%">
                                            ID
                                        </th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Page Title">Page Title</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Page Alias">Page Alias</th>

                                        <th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($get_cms_list)) 
                                        @foreach ($get_cms_list as $key => $value) 
                                            <tr>
                                                <td>#{{ $value->id }}</td>
                                                <td>{{ $value->values[0]->page_title }}</td>
                                                <td>{{ $value->page_alias }}</td>
                                                <td class="center">
                                                    <a class="btn btn-info" href="{{ url('/') }}/admin/cms/edit-cms/{{ $value->id }}"><i class="icon-edit icon-white"></i>Edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  @stop
        