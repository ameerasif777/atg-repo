@extends('Backend.layouts.default')
@section('content')  
<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
    <h1> Edit CMS Page </h1>            
    <ol class="breadcrumb">
        <li> <a href="{{ url('/') }}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a>  </li>
        <li> <a href="{{ url('/') }}/admin/cms"> <i class="fa fa-fw fa-file-text"></i>  Manage CMS Pages</a></li>
        <li class="active">Edit CMS Page</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">               
                <form  name="edit_cms_form" id="edit_cms_form" action="{{url('/')}}/admin/cms/edit-cms" method="post">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token()}}"/>
                    <input type="hidden" name="id" id="id" value="{{ $arr_cms_details->id }}">

                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="parametername">CMS Page Title <sup class="mandatory">*</sup></label>
                            <input type="text"   class="form-control" name="cms_page_title" id="cms_page_title"  value="{{ $arr_cms_details->values[0]->page_title }}"/>
                        </div>
                        <div class="form-group">
                            <label for="parametername">CMS Page Content<sup class="mandatory">*</sup></label>
                            <textarea class="form-control" name="cms_content" id="productdescription" >{!! $arr_cms_details->values[0]->page_content !!}</textarea>
                            <label style="display: none;" class="error hidden" id="labelProductError">Please enter CMS contents.</label>
                        </div>
                        <div class="form-group">
                            <label for="parametername">Page SEO Title <sup class="mandatory">*</sup></label>
                            <input type="text"   class="form-control" name="cms_page_seo_title" id="cms_page_seo_title"  value="{{ $arr_cms_details->values[0]->page_seo_title_lang }}"/>

                        </div>
                        <div class="form-group">
                            <label for="parametername">Meta Keywords<sup class="mandatory">*</sup></label>
                            <textarea class="form-control" name="cms_page_meta_keywords" id="cms_page_meta_keywords" >{{ $arr_cms_details->values[0]->page_meta_keyword }} </textarea>

                        </div>
                        <div class="form-group">
                            <label for="parametername">Meta Description<sup class="mandatory">*</sup></label>
                            <textarea class="form-control" name="cms_page_meta_description" id="cms_page_meta_description" >{{ $arr_cms_details->values[0]->page_meta_description }}</textarea>
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" name="submit_button" class="btn btn-primary" value="Save" id="submit_button">Save Changes</button>
                        <input type="hidden" name="cms_id" id="cms_id" value="{{ $arr_cms_details->id }}" >
                        <button type="reset" name="cancel" class="btn" onClick="window.top.location = '{{url('/')}}/admin/cms';">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer')  
<script type="text/javascript" language="javascript">
            /* add admin Js */
            jQuery(document).ready(function() {

    jQuery("#edit_cms_form").validate({
    errorElement: 'div',
            rules: {
            cms_page_title: {
            required: true
            },
                    cms_page_seo_title: {
                    required: true
                    },
                    status: {
                    required: true
                    },
                    cms_page_meta_keywords: {
                    required: true
                    },
                    cms_page_meta_description: {
                    required: true
                    },
                    cms_page_meta_content: {
                    required: true
                    }
            },
            messages: {
            cms_page_seo_title: {
            required: "Please mention page SEO title."
            },
                    cms_page_title: {
                    required: "Please enter cms page title."
                    },
                    status: {
                    required: "Please select cms page status."
                    },
                    cms_page_meta_keywords: {
                    required: "Please mention page meta keywords."
                    },
                    cms_page_meta_description: {
                    required: "Please mention page meta description."
                    },
                    cms_page_meta_content: {
                    required: "Please mention page meta content."
                    }
            },
            submitHandler: function(form) {
            if ((jQuery.trim(jQuery("#cke_1_contents iframe").contents().find("body").html())).length < 12)
            {
            jQuery("#labelProductError").removeClass("hidden");
                    jQuery("#labelProductError").show();
            }
            else {
            jQuery("#labelProductError").addClass("hidden");
                    form.submit();
            }
            }
    });
    });
            CKEDITOR.replace('productdescription', {
            filebrowserBrowseUrl: '{{url('/')}}/admin/editor-image-upload',
                    filebrowserImageUploadUrl : "{{route('admin.editor-image-upload',['_token' => csrf_token() ])}}",
//        filebrowserImageUploadUrl : "'{{url('/')}}/admin/editor-image-upload',['_token' => <?php // echo csrf_token();   ?> ])}}", 
//        filebrowserUploadUrl: '{{url('/')}}/admin/editor-image-upload',
                    uiColor: '#9AB8F3',
                    height: 300,
            });
</script>
@stop