@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Update Hair Color Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/bio/list/hair"><i class="fa fa-fw fa-user"></i> Manage Hair Color</a></li>
        <li class="active">
            Update Hair Color Details
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form name="frm_user_details" id="frm_user_details" role="form"  action="{{url('/')}}/admin/hair/edit/{!! $edit_id !!}" method="POST" >
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                
                                <div class="form-group">
                                    <label for="hair_color">Hair Color <sup class="mandatory">*</sup></label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_hair_type['hair_color']))!!}" name="bio_detail" id="bio_detail"  class="form-control">
                                </div>
                               
                                <div class="form-group">
                                    <label for="status">Change Status<sup class="mandatory">*</sup></label>
                                    <select id="status" name="status" class="form-control">
                                        
                                        <option value="1" @if ($arr_hair_type['status'] == 1)  selected="selected" @endif >Active</option>
                                        <option value="0" @if ($arr_hair_type['status'] == 2)  selected="selected" @endif >Inactive</option>
                                    </select>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                        <input type="hidden" name="edit_id" id="edit_id" value="{!! intval(base64_decode($edit_id)) !!}" />
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('/assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('/assets/Backend/js/bio-manage/add-edit.js')}}"></script>

@stop
