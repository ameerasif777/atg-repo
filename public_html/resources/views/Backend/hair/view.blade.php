@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
<!--    <h1>
        Student Details
    </h1>-->
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/bio/list/hair"><i class="fa fa-fw fa-user"></i> Manage Hair Color's</a></li>
        <li class="active">
            Hair Color Details
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p class="lead"> Hair Color Details</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                             

                            <tr>
                                <th> Name :</th>
                                <td>{{ ucfirst($arr_hair_detail[0]->user_name) }}</td>
                            </tr>
                            
                            <?php if (isset($arr_hair_detail[0]->status) && $arr_hair_detail[0]->status == '0') {
                                ?>
                                <tr>
                                    <th> Status:</th>
                                    <td><?php echo 'Inactive'; ?></td>
                                </tr>
                            <?php } ?>
                                <?php if (isset($arr_hair_detail[0]->status) && $arr_hair_detail[0]->status == '1') {
                                ?>
                                <tr>
                                    <th> Status:</th>
                                    <td><?php echo 'Active'; ?></td>
                                </tr>
                            <?php } ?>
                                <?php if (isset($arr_hair_detail[0]->status) && $arr_hair_detail[0]->status == '2') {
                                ?>
                                <tr>
                                    <th> Status:</th>
                                    <td><?php echo 'Blocked'; ?></td>
                                </tr>
                            <?php } ?>
                           
                        </tbody>



                    </table>
                </div>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('//assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('//assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('//assets/Backend/js/admin-manage/edit-admin.js')}}"></script>

@stop
