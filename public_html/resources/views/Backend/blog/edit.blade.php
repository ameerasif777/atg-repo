@extends('Backend.layouts.default')
@section('content') 
<link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
    <h1>
       Update Blog 
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/blog/list"><i class="fa fa-gear"></i> Manage Blog</a></li>
        <li class="active">Update Blog</li>

    </ol>
    
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form name="frm_edit_blog" role="form"  id="frm_edit_blog" action="{{url('/')}}/admin/blog/edit/{!! $blog_posts->id  !!}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                           
                            <div class="form-group">
                                <label for="parametername">Title<sup class="mandatory">*</sup></label>
                                <input type="text" dir="ltr"  class="form-control" name="title" id="title" value="{!! $blog_posts->title !!}"  />
                            </div>
                            <div class="form-group">
                                <label for="parametername">Post Short Description<sup class="mandatory">*</sup></label>
                                <textarea type="text" dir="ltr"  name="post_short_description" id="post_short_description"  class="form-control"  >{!! $blog_posts->post_short_description !!}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="parametername">Post Description<sup class="mandatory">*</sup></label>
                                <textarea type="text" dir="ltr"  class="form-control ckeditor" name="description"  >{!! $blog_posts->description !!}</textarea>
                                <div class="error hidden" id="labelProductError">Please enter post description</div>
                            </div>
                            <div class="form-group">
                                <label for="parametername" style="display:none">Post Page Url<sup class="mandatory">*</sup></label>
                                <textarea type="text" dir="ltr" style="display:none" readonly  class="form-control" name="page_title"  id="page_title" >{!! $blog_posts->page_title !!}</textarea>

                            </div>
                            <div class="form-group">
                                <label for="parametername">Post Tags</label>                                
                                <input type="text" class="form-control" name ="post_tags" id="post_tags" placeholder="Tag"  value="{!! $blog_posts->post_tags !!}"/>                                
                            <div class="text-danger" for="post_tags" generated="true"></div>
                            </div>
                            
                            <div class="form-group">
                            <label for="">GROUP</label>

                            <select name="sub_groups[]" id="sub_groups[]" multiple="" placeholder="Groups" class="form-control multipleSelect" size="{{count($user_group_data)}}">
                                @foreach($user_group_data as  $value)
                                @if(isset($arr_selected_groups) && in_array($value->id,$arr_selected_groups))
                                <option value="{{$value->id}}" selected="">{{$value->group_name}}</option>
                                @else
                                <option value="{{$value->id}}">{{$value->group_name}}</option>
                                @endif                            
                                @endforeach
                            </select>     
                            <div class="text-danger" for="sub_groups[]" generated="true"></div>
                            </div>
                            <div class="form-group">
                                <label for="parametername">Publish Status</label>
                                <select class="form-control" name="status" id="status" class="">
                                    <option @if($blog_posts["status"] == "1")  selected="selected" @endif  value="1">Unpublished</option>
                                    <option @if($blog_posts["status"] == "0")  selected="selected" @endif value="0">Published</option>
                                    <option @if($blog_posts["status"] == "2")  selected="selected" @endif value="2">Removed</option>
                                </select>
                                
                            </div>
                            
                            <div class="form-group">
                                <label for="parametername">Blog Image<sup class="mandatory">*</sup></label>
                                <input type="file" dir="ltr"  class="" name="blog_image" id="blog_image" onchange="readURL(this);" value="{!! $blog_posts->blog_image !!}" size="80"  autocomplete="off"  /></br>
                                <img class="uploding-img-vie" height="200px"  width="200px" id="blah" src="{{ config('feature_pic_url').'blog_image/'.$blog_posts->blog_image }}" onerror="this.src={{ config('img').'image-not-found2.jpg'}}" width="100%" height="200"/><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                            </div>
                     
                        
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>

                        </div>

                    
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script type="text/javascript">
 $(function () {
                $('#blog_image').checkFileType({
                    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
                    error: function () {
                        $('#blog_image').replaceWith($('#blog_image').val('').clone(true));
                        alert('Please upload only jpg,jpeg,png,gif type file.');
                    }

                });

            });

 function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
    $('#post_tags').tokenfield({
  autocomplete: {

    delay: 100
  },
  showAutocompleteOnFocus: true
});
  $('.multipleSelect').popSelect({
        showTitle: false,
        maxallowed:5,
        autoIncrease: true,
        placeholderText: 'Please select the groups'
    });

  $("#frm_edit_blog").validate({
      ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
   rules:{
       title:{
           required:true,
           
       },
       post_short_description:{
           required:true,
       },
       description:{
           required:true,
       },
//       post_tags:{
//           required:true,
//       },
//       'sub_groups[]': {
//                    required: true,
//                }                   
   },
   messages:{
       title:{
           required:"Please enter blog title.",
       },
       post_short_description:{
           required:"Please enter short description.",
           
       },
       description:{
           required:"Please enter full description.",
           
       },
//       post_tags:{
//           required:"Please enter tag.",
//           
//       },
//       'sub_groups[]': {
//            required: "Please select group.",
//      },
      submitHandler: function(form) {
           $("#btn_submit").hide();
       }
       
   },
   
}); 

  CKEDITOR.replace('description', {
            filebrowserBrowseUrl: '{{url('/')}}/admin/editor-image-upload',
                    filebrowserImageUploadUrl : "{{route('admin.editor-image-upload',['_token' => csrf_token() ])}}",
                    uiColor: '#9AB8F3',
                    height: 300,
            });


</script>

@stop