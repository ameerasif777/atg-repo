
@extends('Backend.layouts.default')
@section('content')  
    <section class="content-header">
        <h1>
            Blog Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-fw fa-eye"></i> Manage Blogs</li>

        </ol>
    </section>
    <section class="content">
         @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
            @endif
        
         @if(Session::has('success_msg'))
            <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
            @endif
        
        <div class="row"> 
            <div class="col-xs-12"> 
                <div class="box">
                    <form name="frm_roles" id="frm_roles" action="{{url('/')}}/admin/blog/delete" method="post">
                        {!! csrf_field() !!}
                        <div class="box-body table-responsive">
                            <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                                <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                          <th> <center>
                                            Select <br>
                                            @if (!empty($blog_posts) && count($blog_posts)>1)
                                            <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                            @endif
                                            </center>
                                          </th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Image</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Title</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Short Description</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Posted On</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Status</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Report</th>
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Action</th>
                                            <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" >Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($blog_posts as $post) 
                                        <tr>
                                            <td>
                                            <center>
                                                        <input name="blog_ids[]" value="{!! $post->id !!}" class="case" type="checkbox">
                                            </center>
                                        </td>
                                        <td width="10%"><img src="{{ config('feature_pic_url').'blog_image/'.$post->blog_image }}" alt="No image found" height="100" onerror=this.src="{{ config('img').'image-not-found2.jpg'}}" width="100"></td>
                                    <td width="10%">{{ucfirst($post->title)}}</td>
                                    <td><p style="width:200px;word-wrap:break-word;">{!! $post->post_short_description !!}</p></td>
                                   
                                    <td>
                                        
                                        {!! $post->created_at !!}
                                    </td>
                                    <td>
                                        @if($post->status == 1)
                                        Unpublished 
                                        @elseif($post->status == 0)
                                        Published 
                                        @endif
                                    </td>
                                    <td>{!! stripslashes($post->report_count) !!}</td>
                                    <td ><a class="btn btn-info" href="{{url('/')}}/admin/blog/edit/{!! base64_encode($post->id) !!}" > <i class="icon-edit icon-white"></i> Edit</a> 
                                        <a class="btn btn-info" title="View Article Details" href="{{url('/')}}/admin/blog/view/{!! base64_encode($post->id); !!}"> <i class="icon-edit icon-white"></i>View</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{url('/')}}/admin/blog/comment/{!! base64_encode($post->id) !!}"  title="Click to view comments posted by users"> <i class="icon-eye-open icon-white"></i> Comments ({!! $post->count !!})</a>
                                    </td>
                                    </tr>
                               @endforeach
                                    </tbody>
                                    <tfoot>
                                    <th colspan="13" > 
                                        @if (!empty($blog_posts)&& count($blog_posts)>0)
                                        <input type="submit" value="Delete Selected" onclick="return deleteConfirm();" class="btn btn-danger" name="btn_delete_all" id="btn_delete_all">
                                        @endif
                                        
                                        
                                    </th>
                                    </tfoot>
                                </table>		
                            </div><!-- /.box-body -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop