
@extends('Backend.layouts.default')
@section('content') 
<!--<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>-->
<!--<link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>-->
<section class="content-header">
    <h1>
        View Blog Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/blog/list"><i class="fa fa-gear"></i> Manage Blog</a></li>
        <li class="active">View Blog Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="col-xs-6">
                
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                             <tr>
                      <th> Title :</th>                    
                      <td>{{ ucfirst($arr_blog[0]->title) }}</td>
                  </tr>
                <tr>
                    <th> Description :</th>                    
                    <td>{{ ucfirst($arr_blog[0]->post_short_description) }}</td>
                </tr>
                     
                <tr>
                    <th> Tags :</th>                    
                    <td>
                        @if($arr_blog[0]->post_tags) != '')
                        {{ ucfirst($arr_blog[0]->post_tags) }}
                    @else
                    --
                    @endif
                    </td>
                </tr>
                 <tr>
                     
                    <th> Posted In :</th> 
                    <td><span>
                        @foreach($get_groups as $key=>$value)
                        @if($value != '')
                        @if($key == 0)
                            {!! $value->group_name !!} 
                            @else
                            ,{!! $value->group_name !!} 
                            @endif
                        @else
                        --
                        @endif
                        @endforeach</span>
                        </td>
                 </tr>
                    <label for="parametername">Blog Image : </label></br>
                    <img src="{{config('feature_pic_url')}}blog_image/{{($arr_blog[0]->blog_image)}}" alt="" height="150px" width="150px "  onerror=this.src="{{ config('img').'image-not-found2.jpg'}}" />
                </div>
           </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
              
        </div>
</form>    
    </div>
</section>        
@stop
@section('footer')  
<!--<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>-->
@stop



