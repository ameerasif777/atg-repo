
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                @if($arr['user_type'] == 1)
                <form name="frm_user_details" id="frm_user_details" role="form"  action="{{url('/')}}/admin/student/edit/{!! $edit_id !!}" method="POST" >
                @endif    
                @if($arr['user_type'] == 3)
                <form name="frm_user_details" id="frm_user_details" role="form"  action="{{url('/')}}/admin/professional/edit/{!! $edit_id !!}" method="POST" >
                @endif    
                @if($arr['user_type'] == 4)
                <form name="frm_user_details" id="frm_user_details" role="form"  action="{{url('/')}}/admin/company/edit/{!! $edit_id !!}" method="POST" >
                @endif    
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="User Name">User Name </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['user_name']))!!}" id="user_name" name="user_name" class="form-control">
                                           <input type="hidden" value="{!! str_replace('"', '&quot;', stripslashes($arr['user_name']))!!}" id="old_username" name="old_username">

                                </div>
                                <div class="form-group">
                                    <label for="First Name">First Name</label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['first_name']))!!}" name="first_name" id="first_name"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Last Name">Last Name </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['last_name']))!!}" name="last_name" id="last_name"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="dob">Birth Date</label>
                                    @if($arr['user_birth_date']!='')
                                    <input type="text" readonly value="{{ date('m/d/Y',strtotime($arr['user_birth_date']))}}" id="dob" name="dob" class="form-control">
                                    @else
                                    <input type="text" readonly value="" id="dob" name="dob" class="form-control">
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email Id <sup class="mandatory">*</sup></label>
                                    <input type="text" value="{!! stripslashes($arr['email']) !!}" name="email" id="email" class="form-control">
                                    <input type="hidden" value="{!!stripslashes($arr['email']) !!}" name="old_email" id="old_email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="location">Location </label>
                                    <input type="text" value="{!! $arr['location']!!}" name="location" id="location"  class="form-control">
                                    <input type="hidden"  name="latitude"   class="form-control" value="{!! $arr['latitude']!!}">
                                    <input type="hidden"  name="longitude"   class="form-control" value="{!! $arr['longitude']!!}">
                                </div>

                                <div class="form-group">
                                    <label for="contact_no"> Mob.No </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['mob_no']))!!}" name="contact_no" id="contact_no"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="phone_no"> Phone No </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['phone_no']))!!}" name="phone_no" id="phone_no"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="about_me"> About Me </label>
                                    <textarea  name="about_me" id="about_me"  class="form-control">{!! str_replace('"', '&quot;', stripslashes($arr['about_me']))!!}</textarea>
                                </div>
                            </div>
                            <div class="row col-md-6">
                                <div class="form-group col-md-4">
                                    <label for="height"> Height </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['height']))!!}" name="height" id="height"  class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="height"> Height Unit </label>
                                    <select  class="form-group" name="height_unit">
                                        <option <?php if ($arr['height_unit'] == '0') { ?>selected="selected"<?php } ?>value="0">Inches</option>
                                        <option <?php if ($arr['height_unit'] == '1') { ?>selected="selected"<?php } ?> value="1">CM</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label for="weight"> Weight </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['weight']))!!}" name="weight" id="weight"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="body_type">Body Type</label>
                                    <?php
                                    if (isset($arr['body_id']) && $arr['body_id'] != 'NULL') {
                                        $val = $arr['body_id'];
                                    } else {
                                        $val = 0;
                                    }
                                    ?>
                                    <select class="form-control" name="body_type" id="body_type">
                                        <option value="0">Select</option>
                                        <?php
                                        foreach ($arr_body_type as $parent) {
                                            ?>
                                            <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->body_type; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="eye_color">Eye Color</label>
                                    <?php
                                    if (isset($arr['eye_id']) && $arr['eye_id'] != 'NULL') {
                                        $val = $arr['eye_id'];
                                    } else {
                                        $val = 0;
                                    }
                                    ?>
                                    <select class="form-control" name="eye_color" id="eye_color">
                                        <option value="0">Select</option>
                                        <?php
                                        foreach ($arr_eye_color as $parent) {
                                            ?>
                                            <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->eye_color; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="hair_color">Hair Color</label>
                                    <?php
                                    if (isset($arr['hair_id']) && $arr['hair_id'] != 'NULL') {
                                        $val = $arr['hair_id'];
                                    } else {
                                        $val = 0;
                                    }
                                    ?>
                                    <select class="form-control" name="hair_color" id="hair_color">
                                        <option value="0">Select</option>
                                        <?php
                                        foreach ($arr_hair_color as $parent) {
                                            ?>
                                            <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->hair_color; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>


                                <div class="form-group">
                                    <label for="country">Ethnicity</label>
                                    <?php
                                    if (isset($arr['ethnicity_id']) && $arr['ethnicity_id'] != 'NULL') {
                                        $val = $arr['ethnicity_id'];
                                    } else {
                                        $val = 0;
                                    }
                                    ?>
                                    <select class="form-control" name="ethnicity_type" id="ehnicity_type">
                                        <option value="0">Select</option>
                                        <?php
                                        foreach ($arr_ethnicity_type as $parent) {
                                            ?>
                                            <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->ethnicity_type; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="profession"> Profession </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr['profession']))!!}" name="profession" id="profession"  class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="Gender">Gender<sup class="mandatory">*</sup></label>
                                    <input  class="form-control" id="gender" type="radio" value="0"  name="gender" @if($arr['gender'] == '0')  checked="checked" @endif >
                                            Male
                                            <input id="gender" type="radio" value="1" name="gender" @if ($arr['gender'] == '1') checked="checked" @endif >
                                            Female
                                </div>

                                @if ($arr['role_id'] != 1) 
                                <div class="form-group">
                                    <label for="status">Change Status<sup class="mandatory">*</sup></label>
                                    <select id="user_status" name="user_status" class="form-control">
                                        @if ($arr['user_status'] == 0)
                                        <option value="">Inactive</option>
                                        @endif
                                        <option value="1" @if ($arr['user_status'] == 1)  selected="selected" @endif >Active</option>
                                        <option value="2" @if ($arr['user_status'] == 2)  selected="selected" @endif >Blocked</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="user_type">Change Type<sup class="mandatory">*</sup></label>
                                    <select id="user_type" name="user_type" class="form-control">
                                        @if ($arr['user_type'] == 2)
                                        <option value="">Admin</option>
                                        @endif
                                        <option value="1" @if ($arr['user_type'] == 1)  selected="selected" @endif >Student</option>
                                        <option value="3" @if ($arr['user_type'] == 3)  selected="selected" @endif >Professional</option>
                                        <option value="4" @if ($arr['user_type'] == 4)  selected="selected" @endif >Company</option>
                                    </select>
                                </div>
                                @else
                                <input type="hidden" name="user_status" id="user_status" value="1" />
                                @endif

                                <div class="form-group">
                                    <label for="Email Id">Change Password</label>
                                    <input type="checkbox" class="form-control hide-show-pass-div" name="change_password" id="change_password">
                                </div>
                                <div id="change_password_div" style="display:none;">
                                    <div class="form-group">
                                        <label for="Password">Password <sup class="mandatory">*</sup></label>
                                        <input type="password" id="user_password" name="user_password" class="form-control">
                                        <div class="password-meter" style="display:none">
                                            <div class="password-meter-message password-meter-message-too-short">Too short</div>
                                            <div class="password-meter-bg">
                                                <div class="password-meter-bar password-meter-too-short"></div>
                                            </div>
                                        </div>
                                        <span> (Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters) </span> </div>
                                    <div class="form-group">
                                        <label for="Confirm Password">Confirm Password<sup class="mandatory">*</sup></label>
                                        <input type="password" value="" name="confirm_password" id="confirm_password" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                        <input type="hidden" name="edit_id" id="edit_id" value="{!! intval(base64_decode($edit_id)) !!}" />
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>