                        <tr>
                            @if (isset($arr[0]->first_name) && $arr[0]->first_name != '')

                            <th style="width:50%">First Name :</th>
                            <td>{{ ucfirst($arr[0]->first_name) }} </td>
                            @endif
                        </tr>
                        <tr>
                            @if (isset($arr[0]->last_name) && $arr[0]->last_name != '') 

                            <th>Last Name  :</th>
                            <td> {{ ucfirst($arr[0]->last_name) }} </td>
                            @endif
                        </tr>

                        <tr>
                            @if(isset($arr[0]->user_name) && $arr[0]->user_name != '') 

                            <th>User Name :</th>
                            <td>{{ $arr[0]->user_name }}</td>
                            @endif
                        </tr>
                        <tr>
                            @if(isset($arr[0]->email) && $arr[0]->email != '') 

                            <th>Email Id:</th>
                            <td>{{ $arr[0]->email }}</td>
                            @endif
                        </tr>
                        @if(isset($arr[0]->gender) && $arr[0]->gender == '0') 

                        <tr>
                            <th>Gender:</th>
                            <td>{{'Male'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->gender) && $arr[0]->gender == '1') 

                        <tr>
                            <th>Gender:</th>
                            <td>{{'Female'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->status) && $arr[0]->status == '0')

                        <tr>
                            <th>User Status:</th>
                            <td>{{'Inactive'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->status) && $arr[0]->status == '1') 

                        <tr>
                            <th>User Status:</th>
                            <td>{{'Active'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->status) && $arr[0]->status == '2')

                        <tr>
                            <th>User Status:</th>
                            <td>{{'Blocked'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->type) && $arr[0]->type == '1') 

                        <tr>
                            <th>User Type:</th>
                            <td>{{'Student'}}</td>
                        </tr>
                        @endif

                        @if(isset($arr[0]->type) && $arr[0]->type == '2')

                        <tr>
                            <th>User Type:</th>
                            <td>{{'Admin'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->type) && $arr[0]->type == '3') 

                        <tr>
                            <th>User Type:</th>
                            <td>{{'Professional'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->type) && $arr[0]->type == '4') 

                        <tr>
                            <th>User Type:</th>
                            <td>{{'Company'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->mob_no) && $arr[0]->mob_no != '') 

                        <tr>
                            <th>Mobile No:</th>
                            <td>{{$arr[0]->mob_no or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->phone_no) && $arr[0]->phone_no != '') 

                        <tr>
                            <th>Phone No:</th>
                            <td>{{$arr[0]->phone_no or '-'}} </td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->height) && $arr[0]->height != '') 

                        <tr>
                            <th>Height:</th>
                            <td>{{$arr[0]->height or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->weight) && $arr[0]->weight != '') 

                        <tr>
                            <th>Weight:</th>
                            <td>{{$arr[0]->weight or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->body_id) && $arr[0]->body_id != '') 

                        <tr>
                            <th>Body Type:</th>
                            <td>{{$arr[0]->body_id  or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->ethnicity_id) && $arr[0]->ethnicity_id != '') 

                        <tr>
                            <th>Ethnicity Type:</th>
                            <td>{{$arr[0]->ethnicity_id or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->eye_id) && $arr[0]->eye_id != '') 

                        <tr>
                            <th>Eye Color:</th>
                            <td>{{$arr[0]->eye_id or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->hair_id) && $arr[0]->hair_id != '') 

                        <tr>
                            <th>Hair Color:</th>
                            <td>{{$arr[0]->hair_id or '-'}}</td>
                        </tr>
                        @endif
                        @if(isset($arr[0]->profession) && $arr[0]->profession != '') 

                        <tr>
                            <th>Profession:</th>
                            <td>{{$arr[0]->profession or '-'}}</td>
                        </tr>
                        @endif

                        @if(isset($arr[0]->about_me) && $arr[0]->about_me != '') 

                        <tr>
                            <th>About Me:</th>
                            <td>{{$arr[0]->about_me or '-'}}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Selected Group</th>
                        </tr>    
                        @if(!empty($finalData['arr_parent_group']))
                            @foreach($finalData['arr_parent_group'] as $key=> $value)                            
                                @if(!empty($value->chlid) && isset($finalData['arr_user_group']) && in_array($value->id,$finalData['arr_user_group'][0]))
                                    <tr> 
                                        <td>{{$value->group_name }}</td>
                                @endif 
                                <ul class="sub-menu sub-li " style="display: block;">
                                    @foreach($value->chlid as $value1)
                                    @if(isset($finalData['arr_user_group']) && in_array($value1->id,$finalData['arr_user_group'][0]))
                                    <tr>
                                        <td></td>  
                                        <td>{{$value1->group_name}}</td>
                                    </tr>
                                    @endif

                                    @foreach($value1->chlid_1 as  $value2)    
                                        @if(isset($finalData['arr_user_group']) && in_array($value2->id,$finalData['arr_user_group'][0]))
                                            <tr>
                                                <td></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;->{{ $value2->group_name}}</td>
                                                @endif
                                            </tr>
                                            <?php $counter = 1; ?>
                                            @foreach($value2->chlid_2 as  $value3)    
                                                @if(isset($finalData['arr_user_group']) && in_array($value3->id,$finalData['arr_user_group'][0]))
                                                    <tr>
                                                        <td></td><td></td><td></td>
                                                        <td>{{$counter++.')'.$value3->group_name}}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </ul>
                            @endforeach
                        @else
                            No Group Selected
                        @endif
