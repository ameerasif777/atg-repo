<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="row" style="padding: 10px">
                <div class="col-md-2">
                    <select id="recordPerPage" name="recordPerPage" class="form-control" onchange="doSearch()">
                        <option value="25" {{ ($record_per_page == 25) ? 'selected' : '' }}>25</option>
                        <option value="50" {{ ($record_per_page == 50) ? 'selected' : '' }}>50</option>
                        <option value="100" {{ ($record_per_page == 100) ? 'selected' : '' }}>100</option>
                        <option value="200" {{ ($record_per_page == 200) ? 'selected' : '' }}>200</option>
                        <option value="500" {{ ($record_per_page == 500) ? 'selected' : '' }}>500</option>
                    </select>
                </div>
                <div class="col-md-2">records per page</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-2">&nbsp;</div>
                        <div class="col-md-1">Search</div>
                        <div class="col-md-4">
                            <input id="search-qry" name="search-qry" value="{{ $search_query }}" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <select id="search-status" name="search-status" class="form-control">
                                <option value="0" {{ ($status == 0) ? 'selected' : '' }}>Unpublished</option>
                                <option value="1" {{ ($status == 1) ? 'selected' : '' }}>Published</option>
                                <option value="" {{ ($status == '') ? 'selected' : '' }}>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input type="button" id="search_btn" name="search_btn" class="btn btn-info" onclick="doSearch()" value="Go">
                        </div>
                    </div>
                </div>
            </div>

            <form name="form_post" role="form" id="form_post" action="{{ url($post_delete_path) }}" method="post">
                {!! csrf_field() !!}

                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                        <table class="table table-bordered table-striped" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th style="text-align: center; width: 5%">
                                    Select <br>
                                    @if (count($posts) > 1)
                                        <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all">
                                    @endif
                                </th>
                                <th style="width: 5%; vertical-align: middle; text-align: center;">Image</th>
                                <th style="width: 5%; vertical-align: middle; text-align: center;">ID</th>
                                <th style="width: 20%; vertical-align: middle; text-align: center;">Title</th>
                                <th style="width: 5%; vertical-align: middle; text-align: center;">Posted By</th>
                                <th style="width: 7%; vertical-align: middle; text-align: center;">Posted On</th>
                                <th style="width: 7%; vertical-align: middle; text-align: center;">Status</th>
                                <th style="width: 7%; vertical-align: middle; text-align: center;">Report</th>
                                <th style="width: 7%; vertical-align: middle; text-align: center;">Action</th>
                                <th style="width: 12%; vertical-align: middle; text-align: center;">{{ (isset($post_type) && $post_type == 'question') ? 'Answers' : 'Comments'}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td style="text-align: center">
                                        <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $post->id !!}" />
                                    </td>
                                    <td>
                                        @if(isset($post->profile_image) && !empty($post->profile_image))
                                            <img src="{{ $post_image_path.trim($post->profile_image)}}"
                                                alt="No image found" onerror="this.src={{ config('img').'image-not-found2.jpg'}}" style="width:80px">
                                        @else
                                            <img src="{{ config('img').'image-not-found2.jpg'}}"
                                                alt="No image found" style="width:80px">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $post->id }} 
                                    </td>
                                    <td>
                                        {{ ucfirst($post->title) }}
                                    </td>

                                    <td>{{ ucfirst($post->First_name) }} ({{ ($post->user_id_fk) }})</td>
                                    <td>{!! $post->created_at !!}</td>
                                    <td>
                                        @if($post->status == 1)
                                            Published
                                        @elseif($post->status == 0)
                                            Unpublished
                                        @endif
                                    </td>
                                    <td>{!! stripslashes($post->report_count) !!}</td>
                                    <td>
                                        <a class="btn btn-info" title="Edit Article Details" href="{!! url($post_edit_path . base64_encode($post->id)) !!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                        <a class="btn btn-info" title="View Article Details" href="{!! url($post_view_path . base64_encode($post->id)) !!}"> <i class="icon-edit icon-white"></i>View</a>
                                    </td>
                                    <td>
                                        @if(isset($post_type) && $post_type == 'question')
                                            <a class="btn btn-info" href="{!! url($post_answer_path . base64_encode($post->id)) !!}"  title="Click to view comments posted by users"> <i class="icon-eye-open icon-white"></i> Answers ({!! $post->count !!})</a>
                                        @else
                                            <a class="btn btn-info" href="{!! url($post_comment_path . base64_encode($post->id)) !!}"  title="Click to view comments posted by users"> <i class="icon-eye-open icon-white"></i> Comments ({!! $post->count !!})</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="10">
                                    @if (count($posts) > 0)
                                        <input type="button" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="deleteModel()" value="Delete Selected">
                                    @endif
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-right">{{ $posts->appends(['search-status' => $status, 'search-qry' => $search_query, 'rpp' => $record_per_page])->links() }}</div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal model-style" id="show-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Article</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete?</p>
                <p><input type="checkbox" id="send-mail" name="send-mail">  Send email to user.</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancel">Cancel</button>
                <button type="button" id="delete">Delete</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function deleteModel() {
        $('#show-modal').show();
    }

    document.getElementById('delete').onclick = function() {
        var is_post_checked = false;

        $('.case').each(function() {
            if (this.checked) {
                is_post_checked = true;
                return true;
            }
        });

        if (is_post_checked) {
            document.getElementById('form_post').submit();
        } else {
            alert('Please select at least one post to delete');
        }
    };

    document.getElementById('cancel').onclick = function () {
        $('#show-modal').hide();
    };

    function doSearch() {
        var queryStr = '?rpp=' + $('#recordPerPage').val() + '&search-qry=' + $('#search-qry').val() + '&search-status=' + $('#search-status').val();
        location.href = location.href.split('?')[0] + queryStr;
    }
</script>