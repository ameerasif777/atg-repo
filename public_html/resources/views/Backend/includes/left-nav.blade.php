<aside class="left-side sidebar-offcanvas">
    <?php
    $user_account = Auth::user();
    ?>
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left info">
                <p>Hello, <?php echo $user_account['first_name'] . " " . $user_account['last_name']; ?></p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Logged In</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li <?php if (Request::segment(1) == 'dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo url('/'); ?>/admin/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php
            if ($user_account->role_id == '1') {
                ?>
                <li <?php if (Request::segment(2) == 'global-settings') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/global-settings/list" >
                        <i class="fa fa-gear"></i> <span>Manage Global Settings</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'admin') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/admin/list">
                        <i class="fa fa-fw fa-user"></i> <span>Manage Admin</span> 
                    </a>
                </li>
                <li class="treeview <?php if (Request::segment(2) == 'users' || Request::segment(2) == 'student' || Request::segment(2) == 'professional' || Request::segment(2) == 'company') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'users') { ?> class="active" <?php } ?>>
                    <a>
                        <i class="fa fa-user"></i>
                        <span>Manage Users</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php if (Request::segment(4) == 'student' || Request::segment(2) == 'student') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/users/list/student"><i class="fa fa-angle-double-right"></i>Student</a></li>
                        <li class="<?php if (Request::segment(4) == 'professional' || Request::segment(2) == 'professional') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/users/list/professional"><i class="fa fa-angle-double-right"></i>Professional</a></li>
                        <li class="<?php if (Request::segment(4) == 'company' || Request::segment(2) == 'company') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/users/list/company"><i class="fa fa-angle-double-right"></i>Company</a></li>
                        <li class="<?php if (Request::segment(4) == 'blacklist username' || Request::segment(2) == 'blacklist username') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/users/list/blacklist"><i class="fa fa-angle-double-right"></i>Blacklist Username</a></li>
                    </ul>
                </li>
                <li class="treeview <?php if (Request::segment(2) == 'bio' || Request::segment(2) == 'ethnicity' || Request::segment(2) == 'body' || Request::segment(2) == 'hair' || Request::segment(2) == 'eye') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'users') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>admin/bio/list">
                        <i class="fa fa-fw fa-user"></i>
                        <span>Manage Bio-Details</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php if (Request::segment(4) == 'ethnicity' || Request::segment(2) == 'ethnicity') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/bio/list/ethnicity"><i class="fa fa-angle-double-right"></i>Ethnicity Type</a></li>
                        <li class="<?php if (Request::segment(4) == 'body' || Request::segment(2) == 'body') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/bio/list/body"><i class="fa fa-angle-double-right"></i>Body Type</a></li>
                        <li class="<?php if (Request::segment(4) == 'hair' || Request::segment(2) == 'hair') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/bio/list/hair"><i class="fa fa-angle-double-right"></i>Hair Color</a></li>
                        <li class="<?php if (Request::segment(4) == 'eye' || Request::segment(2) == 'eye') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/bio/list/eye"><i class="fa fa-angle-double-right"></i>Eye Color</a></li>
                    </ul>
                </li>

                <li <?php if (Request::segment(2) == 'job-list') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/job-list">
                        <i class="fa fa-fw fa-usd"></i> <span>Manage Job</span> 
                    
                    </a>
                </li>
                <li class="treeview {{ (Request::segment(2) === 'group') ? 'active' : '' }}">
                    <a href="javascript:void(0)">
                        <i class="fa fa-fw fa-list"></i>
                        <span>Manage Groups</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('admin/group/list') }}"><i class="fa fa-angle-double-right"></i> Add Groups</a></li>
                        <li><a href=" {{ url('admin/suggested-groups/list') }} "><i class="fa fa-angle-double-right"></i> Suggested Groups</a></li>
                    </ul>
                </li>
                <li <?php if (Request::segment(2) == 'event') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/events" >
                        <i class="fa fa-fw fa-list"></i> <span>Manage Events</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'meetup') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/meetup/meetup-list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Meetup</span> 
                    </a>
                </li>
                
                <li <?php if (Request::segment(2) == 'article') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/article/article-list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Articles</span> 
                    </a>
                </li>
                
               <li <?php if (Request::segment(2) == 'question') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/question/list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Qrious</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'education') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/education/list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Education</span> 
                    </a>
                </li>

                <li  class="treeview <?php if (Request::segment(2) == 'analytics' || Request::segment(2) == 'growth' || Request::segment(2) == 'access_location') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'analytics') { ?> class="active" <?php } ?>>
                    <a style="background-color:SeaGreen">
                        <i class="fa fa-line-chart" style="color:SeaShell "></i>
                        <span style="color:SeaShell ">Analytics</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php if (Request::segment(4) == 'growth' || Request::segment(2) == 'growth') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/analytics/growth"><i class="fa fa-angle-double-right"></i>GROWTH</a></li>
                        <li class="<?php if (Request::segment(4) == 'access_location' || Request::segment(2) == 'access_location') { ?> active<?php } ?>"><a href="{{route('access-location-analytics')}}"><i class="fa fa-angle-double-right"></i>Location Access</a></li>
                    </ul>
                </li>
                <li  class="treeview <?php if (Request::segment(2) == 'website_crawl') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'crawl') { ?> class="active" <?php } ?>>
                    <a>
                        <i class="fa fa-list"></i>
                        <span>Crawl/Import</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php if (Request::segment(3) == 'list' ) { ?> active<?php } ?>" ><a href="{{route('website_crawl_list')}}"><i class="fa fa-angle-double-right"></i>Website List</a></li>
                        <li class="<?php if (Request::segment(3) == 'add_url' ) { ?> active<?php } ?>"><a href="{{route('add_url')}}"><i class="fa fa-angle-double-right"></i>Add Url List</a></li>
                        <li class="<?php if (Request::segment(3) == 'review' ) { ?> active<?php } ?>"><a href="{{route('crawled_posts_review')}}"><i class="fa fa-angle-double-right"></i>Posts Review</a></li>
                    </ul>
                </li>
                <li <?php if (Request::segment(2) == 'level') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/level/list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Level</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'cms') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/cms">
                        <i class="fa fa-fw fa-file-text"></i> <span>Manage CMS</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'email-template') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/email-template/list">
                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Email Templates</span> 
                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'send-emails-list') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/send-emails/list">
                        <i class="fa fa-fw fa-envelope"></i> <span>Send Emails</span> 
                    </a>
                </li> 
                <li <?php if (Request::segment(2) == 'pushnotify') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/pushnotify">
                        <i class="fa fa-fw fa-envelope"></i> <span>Send Push Notification</span> 
                    </a>
                </li>   
                              
                <li <?php if (Request::segment(2) == 'contact-us') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/contact-us/list">
                        <i class="fa fa-fw fa-phone"></i> <span>Manage Contact us</span> 
                    </a>
                </li>
                 <li <?php if (Request::segment(2) == 'slider-banner') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/slider-banner/list-sliders-banners">
                        <i class="fa fa-fw fa-rotate-right"></i> <span>Manage Slider Banner</span> 
                    </a>
                </li>
                 <li <?php if (Request::segment(2) == 'team-list') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/team-list">
                        <i class="fa fa-fw fa-list"></i> <span>Manage Teams</span>

                    </a>
                </li>
                <li <?php if (Request::segment(2) == 'role') { ?> class="active" <?php } ?>>
                    <a href="<?php echo url('/'); ?>/admin/role/list" >
                        <i class="fa fa-gear"></i> <span>Manage admin Roles</span>
                    </a>
                </li>
 
                <?php
            } else {
                $arr_privileges = DB::table('trans_role_privileges')->where('role_id', $user_account->role_id)->get();

                if (count($arr_privileges) > 0) {
                    foreach ($arr_privileges as $privilege) {
                        $user_privileges[] = $privilege->privilege_id;
                    }
                }
                $arr_login_admin_privileges = $user_privileges;

                if (count($arr_login_admin_privileges) > 0) {
                    foreach ($arr_login_admin_privileges as $privilage) {

                        switch ($privilage) {

                            case 1:
                                ?>
                                <li <?php if (Request::segment(2) == 'email-template') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/email-template/list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Email Templates</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                            case 2:
                                ?>
                                <li <?php if (Request::segment(2) == 'cms') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/cms">
                                        <i class="fa fa-fw fa-file-text"></i> <span>Manage CMS</span> 
                                    </a>
                                </li>

                                <?php
                                break;
                            case 3:
                                ?>
                                <li class="treeview <?php if (Request::segment(2) == 'users' || Request::segment(2) == 'student' || Request::segment(2) == 'professional' || Request::segment(2) == 'company') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'users') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>admin/users/list">
                                        <i class="fa fa-fw fa-user"></i>
                                        <span>Manage Users</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">

                                        <li class="<?php if (Request::segment(4) == 'student' || Request::segment(2) == 'student') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/users/list/student"><i class="fa fa-angle-double-right"></i>Student</a></li>

                                        <li class="<?php if (Request::segment(4) == 'professional' || Request::segment(2) == 'professional') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/users/list/professional"><i class="fa fa-angle-double-right"></i>Professional</a></li>

                                        <li class="<?php if (Request::segment(4) == 'company' || Request::segment(2) == 'company') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/users/list/company"><i class="fa fa-angle-double-right"></i>Company</a></li>
                    <!--                        <li><a href="<?php echo url('/'); ?>/admin/users/list/general_user"><i class="fa fa-angle-double-right"></i>General User</a></li>
                                        <li><a href="<?php echo url('/'); ?>/admin/users/list/registered_user"><i class="fa fa-angle-double-right"></i>Registered User</a></li>-->
                                    </ul>
                                </li>
                                <?php
                                break;
                            case 4:
                                ?>
                                <li class="treeview <?php if (Request::segment(2) == 'bio' || Request::segment(2) == 'ethnicity' || Request::segment(2) == 'body' || Request::segment(2) == 'hair' || Request::segment(2) == 'eye') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'users') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>admin/bio/list">
                                        <i class="fa fa-fw fa-user"></i>
                                        <span>Manage Bio-Details</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="<?php if (Request::segment(4) == 'ethnicity' || Request::segment(2) == 'ethnicity') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/bio/list/ethnicity"><i class="fa fa-angle-double-right"></i>Ethnicity Type</a></li>
                                        <li class="<?php if (Request::segment(4) == 'body' || Request::segment(2) == 'body') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/bio/list/body"><i class="fa fa-angle-double-right"></i>Body Type</a></li>
                                        <li class="<?php if (Request::segment(4) == 'hair' || Request::segment(2) == 'hair') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/bio/list/hair"><i class="fa fa-angle-double-right"></i>Hair Color</a></li>
                                        <li class="<?php if (Request::segment(4) == 'eye' || Request::segment(2) == 'eye') { ?> active<?php } ?>"><a href="<?php echo url('/'); ?>/admin/bio/list/eye"><i class="fa fa-angle-double-right"></i>Eye Color</a></li>
                    <!--                        <li><a href="<?php echo url('/'); ?>/admin/users/list/general_user"><i class="fa fa-angle-double-right"></i>General User</a></li>
                                        <li><a href="<?php echo url('/'); ?>/admin/users/list/registered_user"><i class="fa fa-angle-double-right"></i>Registered User</a></li>-->
                                    </ul>
                                </li>
                                <?php
                                break;
                                case 5:
                                ?>
                                <li <?php if (Request::segment(2) == 'group') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/group/list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Groups</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 6:
                                ?>
                                <li <?php if (Request::segment(2) == 'contact-us') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/contact-us/list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Contact Us</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 7:
                                ?>
                                <li <?php if (Request::segment(2) == 'job') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/job-list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Jobs</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 8:
                                ?>
                                <li <?php if (Request::segment(2) == 'team') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/team-list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Teams</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 9:
                                ?>
                                <li <?php if (Request::segment(2) == 'events') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/events">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Events</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 10:
                                ?>
                                <li <?php if (Request::segment(2) == 'meetups') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/meetup/meetup-list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Meetups</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 11:
                                ?>
                                <li <?php if (Request::segment(2) == 'articles') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/article/article-list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Articles</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 12:
                                ?>
                                <li  <?php if (Request::segment(2) == 'slider-banner') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/slider-banner/list-sliders-banners">
                                        <i class="fa fa-fw fa-rotate-right"></i> <span>Manage Slider Banner</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 13:
                                ?>
                                <li <?php if (Request::segment(2) == 'question') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/question/list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Manage Qrious</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 14:
                                ?>
                                <li <?php if (Request::segment(2) == 'level') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/level/list">
                                    <i class="fa fa-fw fa-list"></i> <span>Manage Level</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 15:
                                ?>
                                <li <?php if (Request::segment(2) == 'education') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/education/list">
                                    <i class="fa fa-fw fa-list"></i> <span>Manage Education</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 16:
                                ?>
                                <li  class="treeview <?php if (Request::segment(2) == 'analytics' || Request::segment(2) == 'growth' || Request::segment(2) == 'access_location') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'analytics') { ?> class="active" <?php } ?>>
                                    <a style="background-color:SeaGreen">
                                        <i class="fa fa-line-chart" style="color:SeaShell "></i>
                                        <span style="color:SeaShell ">Analytics</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="<?php if (Request::segment(4) == 'growth' || Request::segment(2) == 'growth') { ?> active<?php } ?>" ><a href="<?php echo url('/'); ?>/admin/analytics/growth"><i class="fa fa-angle-double-right"></i>GROWTH</a></li>
                                        <li class="<?php if (Request::segment(4) == 'access_location' || Request::segment(2) == 'access_location') { ?> active<?php } ?>"><a href="{{route('access-location-analytics')}}"><i class="fa fa-angle-double-right"></i>Location Access</a></li>
                                    </ul>
                                </li>
                                <?php
                                break;
                                case 17:
                                ?>
                                <li <?php if (Request::segment(2) == 'send-emails-list') { ?> class="active" <?php } ?>>
                                    <a href="<?php echo url('/'); ?>/admin/send-emails/list">
                                        <i class="fa fa-fw fa-envelope"></i> <span>Send Emails</span> 
                                    </a>
                                </li>
                                <?php
                                break;
                                case 18:
                                ?>
                                <li  class="treeview <?php if (Request::segment(2) == 'website_crawl') { ?> active<?php } ?>" <?php if (Request::segment(2) == 'crawl') { ?> class="active" <?php } ?>>
                                    <a>
                                        <i class="fa fa-list"></i>
                                        <span>Crawl/Import</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="<?php if (Request::segment(3) == 'list' ) { ?> active<?php } ?>" ><a href="{{route('website_crawl_list')}}"><i class="fa fa-angle-double-right"></i>Website List</a></li>
                                        <li class="<?php if (Request::segment(3) == 'add_url' ) { ?> active<?php } ?>"><a href="{{route('add_url')}}"><i class="fa fa-angle-double-right"></i>Add Url List</a></li>
                                        <li class="<?php if (Request::segment(3) == 'review' ) { ?> active<?php } ?>"><a href="{{route('crawled_posts_review')}}"><i class="fa fa-angle-double-right"></i>Posts Review</a></li>
                                    </ul>
                                </li>
                                <?php
                                break;
                                
                        }
                    }
                }
            }
            ?>
        </ul>
    </section>
</aside>
