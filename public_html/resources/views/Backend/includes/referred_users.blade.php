<div>
    <table style="min-width:1000px" class="table">
        <tr><th> User's most recent user agents to access ATG : </th></tr>
        @foreach($data['get_latest_user_agents'] as $key => $value)
            @if($value->agent != NULL) 
                <tr><td>
                    Accessed ATG in location {{$value->latitude}} , {{$value->longitude}} using agent {{ $value->agent }} on {{ $value->created_at }} using {{ $value->platform}} 
                </td></tr> 
            @endif
        @endforeach
    </table>
</div>

@if(count($data['user_connections']) > 0)
    <div class="responsive-table">
    <table style="min-width:500px" class="table">
        <tbody>
            <tr>
                <th colspan="3">Referred Users - {{ count($data['user_connections']) }} <span style="color:green">({{ count($data['user_connections']->where('to_email_verified', 1)->where('from_email_verified', 1)) }} verified)</span>   ({{ count($data['user_connections']->where('from_long', '!=', 0)->where('from_lat', '!=', 0)->where('to_lat', '!=', 0)->where('to_long', '!=', 0)) }} completed signup) </th>
            </tr>
            <tr>
            @foreach($data['user_connections'] as $key => $value)
                <tr>
                    <td class="" width="25%">
                        @if($arr_user_detail[0]->user_id != $value->from_user_id)
                            @if($value->from_email_verified == '1')
                                <span style="color:green">
                            @else
                                <span style="color:red">
                            @endif
                            {{ $value->from_first_name }} {{ $value->from_last_name }}
                            </span>
                            </td>
                            <td>{{ $value->from_ip }}</td>
                            <td>{{ $value->from_lat }} , {{ $value->from_long }}</td>
                        @elseif($arr_user_detail[0]->user_id != $value->to_user_id)
                            @if($value->to_email_verified == '1')
                                <span style="color:green"><a style="color:green" href ="{{url('/')}}/admin/student/view/{{ base64_encode($value->to_id) }}">
                            @else
                                <span style="color:red"><a style="color:red" href ="{{url('/')}}/admin/student/view/{{ base64_encode($value->to_id) }}">
                            @endif
                            
                                {{ $value->to_first_name }} {{ $value->to_last_name }}
                            </a>
                            </span>
                            </td>
                            <td width="25%">{{ $value->to_ip }}</td>
                            <td width="25%">{{ $value->to_lat }} , {{ $value->to_long }}</td>
                        @endif

                    <td width="25%">
                        {{ date('Y-m-d H:i', strtotime($value->created_at)) }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
@endif