<link rel="stylesheet" href="{{url('/assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('/assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('/assets/Backend/js/admin-manage/edit-admin.js')}}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCEQPzQgG8vrtk61scBeGCkK0KBziuGrB4&libraries=places"></script> 
<script src="{{url('/assets/Backend/js/jquery.geocomplete.min.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script>
$(function() {
    $("#location").geocomplete({
        details: ".geo-details",
        detailsAttribute: "data-geo",
        //  map: ".map_canvas",
    });

    $("#location").bind("geocode:result", function(event, result) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': result.formatted_address}, function(results, status) {
            var location = results[0].geometry.location;
            var lat = location.lat();
            var lng = location.lng();
            $("input[name=latitude]").val(lat);
            $("input[name=longitude]").val(lng);

        });


    });
    $('#dob').datepicker({
        changeMonth: true,
        changeYear: true,
    });
});
</script>