<header class="header">
    <?php
    $user_account = Auth::user();
    ?>
    <a href="<?php echo url('/'); ?>/admin/dashboard" class="logo">
        Across The Globe (ATG)
        <title>{{ $global_values['site_title'] or 'p1008'}}</title>
    </a>
    <input type="hidden" id="base_url" name="base_url" value="{!! url('/')!!}">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span><?php echo $user_account['user_name']; ?><i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a class="btn btn-primary btn-flat" href="<?php echo url('/'); ?>/admin/admin/profile"><?php echo $user_account['first_name'] . " " . $user_account['last_name']; ?></a>
                                <a class="btn btn-primary btn-flat" href="<?php echo url('/'); ?>/admin/admin/edit-profile"> Update</a>
                                <a href="<?php echo url('/'); ?>/auth/admin/logout" class="btn btn-warning btn-flat">Logout</a>
                            </div>
                        </li>

                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">	
    <style>
        .second{
            margin-left:1px;
        }
    </style>		
