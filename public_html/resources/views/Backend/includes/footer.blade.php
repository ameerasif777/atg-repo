<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery-ui-1.10.3.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/bootstrap.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/plugins/datatables/jquery.dataTables.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.validate.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/AdminLTE/app.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/AdminLTE/dashboard.js')}}"></script>  
<script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/select-all-delete.js')}}"></script>      
<input type="hidden" name="body_class" id="body_class" value="skin-black" />
<input type="hidden" name="url" id="url" value="<?php echo url('/'); ?>" />
<script type="text/javascript">
$(document).ready(function(e) {
    jQuery("#msg_close").bind("click", function() {
        $(this).parent().remove();
    });
});
var class_val = sessionStorage.getItem("body_class");
if (class_val != '' && class_val != null)
{
    $("body").removeAttr('class');
    $("body").attr('class', class_val);
}
$(function() {
    $("#example1").dataTable();
});
</script>
</body>
</html>