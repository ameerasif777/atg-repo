<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" - admin panel.">
        <meta name="author" content="" >
        <meta charset="utf-8" />
        <title>@yield('title', (isset($site_title)&& $site_title!='')? $site_title: $global_values['site_title'])</title>
        
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery-2.0.2.min.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery-ui.min.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.timepicker.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.datetimepicker.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/date-en-US.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.ui.timeselect.js')}}"></script>
        
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/font-awesome.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/font-awesome.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/ionicons.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/datatables/dataTables.bootstrap.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/AdminLTE.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/jquery-ui.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/timepicker/bootstrap-timepicker.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/timepicker/timepiker.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/timepicker/bootstrap-timepicker.min.css')}}"/>
        
    </head>
    <body class="skin-black">
