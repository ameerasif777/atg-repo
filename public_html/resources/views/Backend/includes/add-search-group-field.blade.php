
<?php 
 $sid = '';
 if($id != '')
 {
     $sid = '_'.$id;
 }
 
 ?>

<input type="hidden" id="user_group" name="user_group[]" value="" />

<div class="group-editor" id="group_div{{$sid}}"> 
    <input id="group_inp{{$sid}}" type="text"  class="text-editor"   value="" style="display:none;" />  
</div>
<div class="panel panel-default" style="z-index:1; width: 350px; position: absolute; margin: auto; display: none; left: 20%; margin-bottom: 20px;" id="groupPanel{{$sid}}">
    <div class="custom_arrow"></div>
    <div class="panel-heading">
        <h3 class="panel-title">ATG - Select Group</h3>
    </div>
    <div class="panel-body" id="panelContent{{$sid}}">
    
    </div>
</div>



 
<?php 
 echo '<script>'.
    '$(document).ready(function(eve) {'.
        'initializeTagsInput();'.
        'addSuggestedGroup(\''.json_encode($arr_selected_groups).'\', \''.$sid.'\');'.
    '});</script>';


?>

@if($key == '0')
<script language="javascript" type="text/javascript" src="{{url('/public/assets/Backend/js/add-group/search-group.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/public/assets/Backend/js/bootstrap.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/public/assets/Backend/js/bootstrap-tagsinput.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{url('/public/assets/Backend/js/bootstrap3-typeahead.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('/public/assets/Backend/css/bootstrap-tagsinput.css')}}"/>

<style>
    .group-editor {
        overflow: hidden;
        border: 0px solid #c8ccd0;
        background-color: #fff;
        cursor:text;
    }
    
    .group-editor:after {
        clear : both;
    }
    
    .panel-default ul{
        list-style: none;
        padding-left:0px;
    }
    
    .bootstrap-tagsinput{
        width:100%;
    }
</style>

<input type="hidden" id="baseurl" value="{!!url('/')!!}"/>
@endif
