<!--
    <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();"></ul>
-->

<input type="hidden" id="user_group" name="user_group[]" value="" />

<style>
    .selectedclass{
        width: 100%; 
        height: 39px; 
        border: 1px solid #ccc; 
        border-radius: 4px; 
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075); 
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s; 
        background-color: #fff; 
        list-style: none; 
        padding-left: unset;
        cursor: pointer;
    }
    .selectedClassElement{
        display: block;
        float: left;
        margin-left: 10px;
        background-color: #36bf8a;
        color: white;
        padding: 5px;
        margin-top: 3px;
        padding-right: 10px;
        border-color: #d43f3a;
        vertical-align: middle;
        -webkit-transition: 0.25s linear;
        transition: 0.25s linear;
        height: 30px;
    }
    .selectedClassElementRemove{
        float: right;
        font-size: 21px;
        font-weight: bold;
        line-height: 1;
        padding-left: 3px;
        color: white; cursor: pointer;
    }
    .custom_arrow {
        position: absolute;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
        left: 50%;
        margin-left: -5px;
        border-width: 0px 10px 10px;
        border-bottom-color: #000;
        border-bottom: 15px solid #ccc;
        top: -15px;
    }
    .custom_placeholder {
        position: relative;
        top: 9px;
        left: 15px;
        color: #999;
        cursor: pointer;
    }
</style>

<div class="panel panel-default" style="z-index:1; width: 350px; position: absolute; margin: auto; display: none; left: 20%; margin-bottom: 20px;" id="groupPanel">
    <div class="custom_arrow"></div>
    <div class="panel-heading">
        <h3 class="panel-title">ATG - Select Group</h3>
    </div>
    <div class="panel-body" id="panelContent">
    
    </div>
</div>

<?php
    $str='<script>$(document).ready(function(){';
    if(isset($arr_selected_groups1))
    {
        foreach ($arr_selected_groups1 as $value) {
            $str.='addGroup1('.$value['id'].',"'.$value['group_name'].'");';
        }
    }
    $str.='getParentGroup(0,"" ); addPlaceholder(); });</script>';
    echo $str;
?>

<script>
    var group=[];

    $('body').click( function(e) {
        if(e.target.className !== "selectedclass") {
            if(e.target.className !== "custom_placeholder"){
            if(e.target.id !== "groupPanel" && !$('#groupPanel').has(e.target).length)
            {
                $('#groupPanel').hide();
            }}
        }
    });

    function getGroup(parent_id)
    {
        $.get('{!!url("/")!!}/get-parent-group3', {parent_id: parent_id, group:group}, function(msg) {
            if (msg != '') {
                $('#panelContent').html(msg);
            }
        });
    }    

    function getParentGroup(parent_id, group_name='')
    {
        $.get('{!!url("/")!!}/get-parent-group2', {parent_id: parent_id, group:group}, function(msg) {
            if (msg != '') {
                if(parent_id==0)
                    $('.panel-title').html('ATG - Select Group');
                else
                    $('.panel-title').html('<span class="glyphicon glyphicon-arrow-left" style="float:left; margin-right:10px;" onclick="getGroup('+parent_id+')"></span>'+group_name+' - Select Sub-Group');

                $('#panelContent').html(msg);
            }
        });
    }

    function addGroup(id, group_name){
        if(!group.includes(id))
        {
            $.get('{!!url("/")!!}/get-group', {parent_id: id}, function(msg) {
                if (msg != '') {
                    $.each(msg, function(i, e) {
                        if(!group.includes(e.id)){
                            addPlaceholder();
                            group.push(e.id);
                            $('#selected').append('<li class="group'+e.id+' mk selectedClassElement">'+e.group_name+'<span class="selectedClassElementRemove" onclick="removeGroup('+e.id+')">×</span></li>');
                            $("#user_group").val(group);
                            $('option[value="'+e.id+'"').prop('selected',"true");
                            changeHeight();
                        }
                    });
                }
            });
        }
    }

    function addGroup1(id, group_name){
        if(!group.includes(id))
        { 
            group.push(id);
            $('#selected').append('<li class="group'+id+' mk selectedClassElement">'+group_name+'<span class="selectedClassElementRemove" onclick="removeGroup('+id+')">×</span></li>');
            $('option[value="'+id+'"').prop('selected',"true");
            $("#user_group").val(group);
            changeHeight();
        }
    }

    function removeGroup(id){
        $.get('{!!url("/")!!}/get-group', {parent_id: id}, function(msg) {
            if (msg != '') {
                $.each(msg, function(i, e) {
                    var index = group.indexOf(e.id);
                    if(index > -1)
                    {
                        $("#"+e.id).prop('checked',false);
                        group.splice(index, 1);
                        $('.group'+e.id).remove();
                        $("#user_group").val(group);
                        $('option[value="'+e.id+'"').removeAttr('selected');
                        changeHeight();
                        addPlaceholder();
                    }
                });
            }
        });
    }

    function changeHeight(){
      var textWidth=$('#selected').width();
      var tagWidth = 0;
      $.each($('#selected').find('li'), function(i,v){
        tagWidth += $(v).outerWidth()+20;
      });
      var mHeight = Math.floor(tagWidth / textWidth);
      console.log(mHeight);
      $('#selected').height(((mHeight + 1) * 33)+4);
    }

    function addPlaceholder() {
        var fun ="$('#groupPanel').show();";
        var code='<div class="custom_placeholder" onclick="'+fun+'">Please select the groups.</div>';
        if($('#selected').html() === ''){
            $('#selected').html(code);
        }
        else {
            if(code === $('#selected').html())
                $('#selected').html('');
        }
    }
</script>
