@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Add Ethnicity Type
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/bio/list/ethnicity"><i class="fa fa-gear"></i> Manage Ethnicity Types</a></li>
        <li class="active">Ethnicity Type</li>

    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form name="frm_user_details" role="form"  id="frm_user_details" action="{{url('/')}}/admin/ethnicity/add" method="POST" >
                    {!! csrf_field() !!}

                    <div class="box-body">
                        <div class="form-group">
                            <label for="bio_detail">Ethnicity Type <sup class="mandatory">*</sup></label>
                            <input class="form-control" type="text" dir="ltr"   name="bio_detail" id="bio_detail" value=""/>
                        </div>
                        <div class="form-group">
                            <label for="Group Name">Ethnicity Status <sup class="mandatory">*</sup></label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inctive</option>
                            </select>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script src="{{url('/assets/Backend/js/bio-manage/add-edit.js')}}"></script>

@stop
