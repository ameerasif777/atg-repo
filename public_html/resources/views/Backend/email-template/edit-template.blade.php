@extends('Backend.layouts.default')
@section('content')  
<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
    <h1>
        Edit Email Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>       
                @if(isset($parentTemplate) &&  $parentTemplate == 'Send Emails')
                    <li> <a href="{{url('/')}}/admin/send-emails/list"><i class="fa fa-fw fa-envelope"></i>{{$parentTemplate}}</a></li>
                @else
                    <li> <a href="{{url('/')}}/admin/email-template/list"><i class="fa fa-fw fa-envelope"></i>Manage Email Templates</a></li>
                @endif
            
        <li class="active">Edit Email Template</li>
    </ol>
</section>
<section class="content">
    <?php if (Session::has('message')) { ?>
        <?php echo Session::get('message'); ?>
    <?php } ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                @if(isset($parentTemplate) &&  $parentTemplate == 'Send Emails')
                <form name="frm_email_template"  id="frm_email_template" action="{{url('/')}}/admin/send-emails/send" method="POST" >
                @else
                <form name="frm_email_template"  id="frm_email_template" action="{{url('/')}}/admin/edit-email-template/<?php echo(isset($email_template_id)) ? $email_template_id : ''; ?>" method="POST" >
                @endif
                
                @if(isset($parentTemplate) &&  $parentTemplate == 'Send Emails')
                <input type="hidden" name="userids" id="userids" value="{{$userids}}"/>
                @endif
                    {!! csrf_field() !!}
                    <input type="hidden" name="email_template_hidden_id" id="email_template_hidden_id" value="<?php echo (isset($email_template_id)) ? $email_template_id : ''; ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="parametername">Email Template Title<sup class="mandatory">*</sup></label>
                            <input type="text" dir="ltr" disabled="disabled"  class="form-control" name="inputTitle" id="inputTitle"  value="<?php echo ucwords(str_replace("-", " ", $arr_email_template_details['email_template_title'])); ?>"/>

                        </div>
                        <div class="form-group">
                            <label for="parametername">Email Template Subject<sup class="mandatory">*</sup></label>
                            <input type="text" dir="ltr"   class="form-control" name="input_subject" id="input_subject"  value="<?php echo str_replace("\n", "", $arr_email_template_details['email_template_subject']); ?>"/>

                        </div>
                        <div class="form-group">
                            <label for="parametername">Email Template Content<sup class="mandatory">*</sup></label>
                            <textarea class="form-control" class="ckeditor" name="text_content" id="text_content"> <?php echo ($arr_email_template_details['email_template_content']); ?></textarea>
                            <div class="error hidden" id="labelProductError">Please enter the email template content.</div>
                        </div>
                        <div class="form-group">
                            <label for="parametername">Email Template Content Macros</label>
                            <select  class="combobox form-control"  multiple ondblclick="insertText(this)" >
                                <?php
                                foreach ($arr_macros as $macros) {
                                    ?>
                                    <option value="<?php echo $macros['macros']; ?>"><?php echo ucwords(str_replace("_", " ", strtolower(trim($macros['macros'], '||'))) . ":-"); ?><?php echo $macros['macros']; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        @if(isset($parentTemplate) &&  $parentTemplate == 'Send Emails')
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="send" id="btnSubmit" >Send</button>
                        @else
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save Changes" id="btnSubmit">Save Changes</button>
                        @endif
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer') 
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
       
        jQuery("#frm_email_template").validate({
            errorElement: 'label',
            rules: {
                input_subject: {
                    required: true
                },
                text_content: {
                    required: true
                }
            },
            messages: {
                input_subject: {
                    required: "Please enter the email template subject."
                },
                text_content: {
                    required: "Please enter the email template content."
                }
            },
            // set this class to error-labels to indicate valid fields
            submitHandler: function(form) {
                if ((jQuery.trim(jQuery("#cke_1_contents iframe").contents().find("body").html())).length < 12)
                {
                    jQuery("#labelProductError").removeClass("hidden");
                    jQuery("#labelProductError").show();
                }
                else {
                    jQuery("#labelProductError").addClass("hidden");
                    $('#btnSubmit').hide();
                    $('#loding_image').show();
                    form.submit();
                }
            }
        });
        CKEDITOR.replace('text_content',
                {
                    filebrowserUploadUrl: '{{url('/')}}/admin/editor-image-upload'
                });

    });
    function insertText(obj) {
        newtext = obj.value;
        console.log(newtext);
        CKEDITOR.instances['text_content'].insertText(newtext);

    }
    
    function sendEmail()
       {
           var strurl = "{!!url('/')!!}/admin/send-emails/send";
           var param = $("#frm_email_template").serialize();
            $.ajax({
                url: strurl,
                method: 'post',
                data: param,
                success: function (resp) {
                     var k = resp;
                }
            });
        }
</script>
@stop