@extends('Backend.layouts.default')
@section('content')  
    <section class="content-header">
        <h1>
            Email Template Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-fw fa-envelope"></i> Manage  Email Template</li>
        </ol>
    </section>
    <section class="content">0
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="ID">ID </th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Title">Title</th>

                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Subject">Subject</th>
                                        
                                      			
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Created on">Created on</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Updated on">Updated on</th>

                                        <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php $i = 1; ?>
                                    @foreach ($arr_email_templates as $email_template) 
                                        <tr>
                                            <td>#{{ $i }}</td>
                                            <td >{{ ucwords(str_replace("-", " ", $email_template->email_template_title)) }}</td>
                                            <td >{{ $email_template->email_template_subject }}</td>                                            
                                            <td >{{ date($global_values['date_format'],strtotime($email_template->created_at)) }}</td>
                                            <td >{{ date($global_values['date_format'],strtotime($email_template->updated_at)) }}</td>
                                            <td class=""><a class="btn btn-info" href="{{ url('/') }}/admin/edit-email-template/{{ $email_template->id }}" title="Edit Email Template"> <i class="icon-edit icon-white"></i>Edit</a></td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop