
@extends('Backend.layouts.default')
@section('content') 
<!--<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>-->
<!--<link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>-->
<section class="content-header">
    <h1>
        View Article Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/article/article-list"><i class="fa fa-gear"></i> Manage Article</a></li>
        <li class="active">View Article Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
            <div class="col-xs-6">
                
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                  <tr>
                      <th> Title :</th>                   
                      <td>
                      @if($arr_article[0]->title != '')
                      {{ ucfirst($arr_article[0]->title) }}
                      @else
                      --
                      @endif
                      </td>
                  </tr>
                <tr>
                    <th> Description :</th>                    
                    <td>{!! ucfirst($arr_article[0]->description) !!}</td>
                </tr>
                     
                <tr>
                    <th> Tags :</th>                    
                    <td>@if($arr_article[0]->tags != '')
                        {{ ucfirst($arr_article[0]->tags) }}
                    @else
                        ---
                    @endif
                    </td>
                </tr>
                @if(count($get_groups) != '')
                 <tr>
                     
                    <th> Posted In :</th> 
                    <td><span>
                            
                        @foreach($get_groups as $key=>$value)                            
                        @if(count($value) > 0)
                        @if($key == 0)
                            {!! $value->group_name !!} 
                            @else
                            ,{!! $value->group_name !!} 
                            @endif
                       @else
                        --
                        @endif
                        @endforeach
                        
                        </span>
                        </td>
                 </tr>
                  @endif  
                <tr>
                    <label for="parametername">Article Image : </label></br>
                    @if(isset($arr_article[0]->profile_image) && !empty($arr_article[0]->profile_image))
                        <img src="{{ config('feature_pic_url').'article_image/'.trim($arr_article[0]->profile_image)}}" alt="" height="150px" width="150px "  onerror=this.src="{{ config('img').'image-not-found2.jpg'}}" width="100%" height="200"/>
                    @else
                        <img src="{{ config('img').'image-not-found2.jpg'}}" alt="" height="150px" width="150px "  width="100%" height="200"/>

                    @endif
                </tr>
           </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
              
        </div>
</form>    
    </div>
</section>        
@stop
@section('footer')  
<!--<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>-->
@stop



