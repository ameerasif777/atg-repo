@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Edit Comment 
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/article/article-list"><i class="fa fa-gear"></i> Manage Comment</a></li>
        <li class="active">Edit Comment</li>

    </ol>
    
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form name="frm_edit_comment" role="form"  id="frm_edit_comment" action="{{url('/')}}/admin/article/editcomment/{!! $arr_comment[0]->id  !!}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                           
                            <div class="form-group">
                                <label for="parametername">Comment<sup class="mandatory">*</sup></label>
                                <input type="text" dir="ltr"  class="form-control" placeholder="Comment"  name="comment" id="comment" value="{!! $arr_comment[0]->comment !!}"  />
                            </div>
                            

                            <div class="form-group">
                                <label for="parametername">Publish Status</label>
                                <select class="form-control" name="status" id="status" class="">
                                    <option @if($arr_comment[0]->status == "0")  selected="selected" @endif  value="0">Unpublished</option>
                                    <option @if($arr_comment[0]->status == "1")  selected="selected" @endif value="1">Published</option>
                                    <!--<option @if($arr_comment[0]->status == "2")  selected="selected" @endif value="2">Removed</option>-->
                                </select>
                            </div>

                            
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>

                        </div>

                    
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script type="text/javascript">




  $("#frm_edit_comment").validate({
   rules:{
       comment:{
           required:true,
           
       },
       
       status:{
           required:true,
       },
       
   },
   messages:{
       comment:{
           required:"Please enter comment.",
       },
      
       status:{
           required:"Please select status.",
           
       },submitHandler: function(form) {
           $("#btnSubmit").hide();
       }
       
   },
   
}); 



</script>

@stop