@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>{{ $site_title }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{ $site_title }}</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif

    @if(Session::has('success_msg'))
        <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif

    @include('Backend.includes.posts-table', [
        'posts' => $arr_article,
        'post_image_path' => config('feature_pic_url').'article_image/',
        'post_view_path' => '/admin/article/view/',
        'post_edit_path' => '/admin/article/edit/',
        'post_comment_path' => '/admin/article/comment/',
        'post_delete_path' => '/admin/article/delete/',
        'status' => $sstatus,
        'record_per_page' => $recordPerPage,
        'search_query' => $sqry
    ])
</section>
@stop
