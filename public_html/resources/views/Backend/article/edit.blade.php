@extends('Backend.layouts.default')
@section('content') 
<link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
    <h1>
        Edit Article Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/article/article-list"><i class="fa fa-gear"></i> Manage Article</a></li>
        <li class="active">Edit Article Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <form name="form_post_edit_article" id="form_post_edit_article" method="post" action="{{url('/')}}/admin/article/edit/{!!base64_encode($arr_article[0]->id)!!}" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="box box-primary">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title">Title <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{ ucfirst($arr_article[0]->title) }}">
                                </div>
<!--                                <div class="form-group">
                                    <label for="parametername">Post Short Description<sup class="mandatory">*</sup></label>
                                    <textarea type="text" dir="ltr"  name="post_short_description" placeholder="Short Description" id="post_short_description"  class="form-control"  >{!! $arr_article[0]->post_short_description !!}</textarea>
                                </div>-->
                                <div class="form-group">
                                    <label for="parametername">Post Description<sup class="mandatory">*</sup></label>
                                    <textarea type="text" dir="ltr"  class="form-control ckeditor" name="description" id="description" >{!! $arr_article[0]->description !!}</textarea>
                                    <label style="display: none;" class="error hidden" id="labelProductError">Please enter CMS contents.</label>
                                </div>                                            
                                <div class="form-group">
                                    <label for="parametername">Tags : </label>
                                    <input type="text" class="form-control" placeholder="Tags" name="tags" id="tags" value="{{ ucfirst($arr_article[0]->tags) }}" />
                                    <div class="text-danger" for="tags" generated="true"></div>
                                </div>
                                <div class="form-group">
                                    <label for="">GROUP</label>

                                    <select name="sub_groups[]" id="sub_groups[]" multiple="" placeholder="Groups" class="form-control multipleSelect" size="{{count($user_group_data)}}" style="display: none;">
                                        @foreach($user_group_data as  $value)
                                        @if(isset($arr_selected_groups) && in_array($value->id,$arr_selected_groups))
                                        <option value="{{$value->id}}" selected="">{{$value->group_name}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->group_name}}</option>
                                        @endif                            
                                        @endforeach
                                    </select>     
                                    <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();"></ul>
                                    <div class="text-danger" for="sub_groups[]" generated="true"></div>
                                </div>
                                @include('Backend.includes.add-search-group-field', ['arr_selected_groups' => $arr_selected_groups1, 'id' => '' , 'key' => '0'])
                                
                                <div class="form-group">
                                    <label for="parametername">Publish Status</label>
                                    <select class="form-control" name="status" id="status" class="">
                                        <option @if($arr_article[0]->status == "0")  selected="selected" @endif  value="0">Unpublished</option>
                                        <option @if($arr_article[0]->status == "1")  selected="selected" @endif value="1">Published</option>
                                        <option @if($arr_article[0]->status == "2")  selected="selected" @endif value="2">Removed</option>
                                    </select>

                                </div>
                                <label for="sub_groups[]" id="errorElement"></label>
                                <div class="form-group">
                                    <label for="parametername">Image :</label>
                                    <input type="file" name='article_pic' id="article_pic" onchange="readURL(this);" /></br>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" id='btn_article_post' name="btn_article_post" class="btn btn-primary" value="Save" onclick="getDescription();">Save</button>
                                    <img  src="{{config('backend_img')}}ajax-loader.gif" style="display: none;" id="loding_image">
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>        
@stop
@section('footer')  
<!--<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>-->


<script type="text/javascript">

    function getDescription(){
        $('#description').html(jQuery.trim(jQuery('#cke_1_contents iframe').contents().find('body').html().replace(/<[^>]+>/g, '')));
    }

    jQuery(document).ready(function() {


        $("#form_post_edit_article").validate({
            ignore: '',
            errorElement: 'div',
            errorClass: 'text-danger',
            rules: {},
            messages: {},
            submitHandler: function(form) {
                $("#btn_article_post").hide();
                $("#loding_image").show();
                form.submit();
            }

        });
    });

    function readURL(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#tags').tokenfield({
                            autocomplete: {
                                source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                delay: 100
                            },
                            showAutocompleteOnFocus: true
                        });
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }
    }
</script>
<script>
    $('#tags').tokenfield({
        autocomplete: {
            source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    });
    /*$('.multipleSelect').popSelect({
          showTitle: false,
        autoIncrease: true,
        maxAllowed: 5,
        placeholderText: 'Please select the groups',
        position: 'top'
    });*/

</script>
@stop

