<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="admin panel">
        <meta name="author" content="Anuj Tyagi" >
        <title>{{!empty($site_title)?$site_title:'Welcome to p1001 Admin Panel'}}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{url('/assets/Backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/Backend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
         Theme style 
        <link href="{{url('/assets/Backend/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
  
    </head>
    <body style="background-color:#191f26!important;">     
        
        <div class="form-box" id="login-box" style="display: block;">
            @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}<a class="close">&times;</a></p>
            @endif
            @if(Session::has('login_error'))
            <p class="alert alert-danger">{{ Session::get('login_error') }}<a class="close">&times;</a></p>
            @endif
           <div class="header">Admin Reset Password</div>
            <form action="{{url('/')}}/admin/reset-password" method="post" name="frm_admin_reset" id="frm_admin_reset" >
              <input type="hidden" name="code" id='code' value="{{ $code }}">
                {!! csrf_field() !!}
                <div class="body bg-gray">
                    <div class="form-group">
                        <input  class="form-control" autofocus type="password" name="user_password" size="" id="user_password" value="" placeholder="Password" >                    
                    </div><span> (Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters) </span>
                    <div class="form-group">
                        <input  type="password" class="form-control" name="confim_user_password" id="confim_user_password" value="" placeholder="Confirm Password" >      
                    </div> 
                </div>
                <div class="footer">        
                    <button type="submit" class="btn bg-olive btn-block">Reset Now</button>  
                    <p><a href="{{url('/')}}/auth/admin/login">Login</a></p>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <input type="hidden" name="base_url" id='base_url' value="{{ url('/') }}">
            </form>
        </div>
  
        <script src="{{url('/assets/Backend/js/jquery-2.0.2.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{url('/assets/Backend/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="{{url('//assets/Backend/js/jquery.validate.js')}}"></script>
        <link rel="stylesheet" href="{{url('/assets/Backend/css/jquery.validate.password.css')}}"/>
        <script src="{{url('/assets/Backend/js/jquery.validate.password.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/login/admin-reset.js')}}"></script>
    </body>
    <script>
     $(document).ready(function()
     {
         setTimeout(function()
         {
             $("#login-box").show();
             $("#loader_img").hide();
             
         },1000);
         
     });
 </script>
</html>
