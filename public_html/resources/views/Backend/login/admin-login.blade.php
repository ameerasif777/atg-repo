<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="admin panel">
        <meta name="author" content="Anuj Tyagi" >
        <title>  .. </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{url('/assets/Backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- bootstrap 3.0.2 -->
        <link href="{{url('/assets/Backend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{url('/assets/Backend/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />  
    </head>
    <body style="background-color:#191f26!important;">     
        <div id="loader_img" style="text-align: center;">
            <img src="{{url('/assets/Backend/img/loader7.gif')}}" title="Admin">
        </div>
        <div class="form-box" id="login-box" style="display: none;">
            @if(Session::has('admin_logout'))
            <p class="alert alert-info">{{ Session::get('admin_logout') }}<a class="close">&times;</a></p>
            @endif
            @if(Session::has('login_error'))
            <p class="alert alert-danger">{{ Session::get('login_error') }}<a class="close">&times;</a></p>
            @endif          
            @if(Session::has('msg_success'))
            <p class="alert alert-success">{{ Session::get('msg_success') }}<a class="close">&times;</a></p>
            @endif          
            @if(Session::has('msg_warning'))
            <p class="alert alert-warning">{{ Session::get('msg_warning') }}<a class="close">&times;</a></p>
            @endif          
            <div class="header">Admin Sign In</div>
            <form action="{{url('/')}}/auth/admin/login" method="post" name="frm_admin_login" id="frm_admin_login" >
                {!! csrf_field() !!}
                <div class="body bg-gray">
                    <div class="form-group">
                        <input  class="form-control" autofocus type="text" name="user_name" size="" id="user_name" value="" placeholder="Username" >                  
                    </div>                    
                    <div class="form-group">
                        <input  type="password" class="form-control" name="user_password" id="user_password" value="" placeholder="Password" >      
                    </div>          
                </div>
                <div class="footer">        
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn bg-olive btn-block">Sign In</button>  
                    <p><a href="<?php echo url('/'); ?>/admin/forgot-password">I forgot my password</a></p>
                </div>
            </form>
        </div>

        <script src="{{url('/assets/Backend/js/jquery-2.0.2.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{url('/assets/Backend/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.validate.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/login/admin-login.js')}}"></script>
        <!--<script src="{{url('/assets/Backend/js/AdminLTE/app.js')}}" type="text/javascript"></script>-->
        <script>
   $(document).ready(function() {
       setTimeout(function()
       {
           $("#login-box").show();
           $("#loader_img").hide();

       }, 1000);

   });
        </script>
    </body>
</html>
