<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="admin panel">
        <meta name="author" content="Anuj Tyagi" >
        <title>{{!empty($site_title)?$site_title:'Welcome to p1001 Admin Panel'}}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{url('/assets/Backend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/Backend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('/assets/Backend/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />  
    </head>
    <body style="background-color:#191f26!important;">     
        
        <div class="form-box" id="login-box" style="display: block;">
            @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}<a class="close">&times;</a></p>
            @endif
            @if(Session::has('login_error'))
            <p class="alert alert-danger">{{ Session::get('login_error') }}<a class="close">&times;</a></p>
            @endif
           <div class="header">Forgot Password</div>
            <form action="{{url('/')}}/admin/forgot-password" method="post" name="frm_admin_forgot_password" id="frm_admin_forgot_password" >
                {!! csrf_field() !!}
                <div class="body bg-gray">
                    <div class="form-group">
                        <input  class="form-control" type="text" name="user_email" size="" id="user_email" value="" placeholder="Email">     
                    </div>
                </div>
                <div class="footer">        
                    <button type="submit" class="btn bg-olive btn-block">Submit</button>  
                    <p><a href="{{url('/')}}/auth/admin/login">Back To Login</a></p>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <input type="hidden" name="base_url" id='base_url' value="{{ url('/') }}">
            </form>
        </div>
  
        <script src="{{url('/assets/Backend/js/jquery-2.0.2.min.js')}}"></script>
        <script src="{{url('/assets/Backend/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/jquery.validate.js')}}"></script>
        <script language="javascript" type="text/javascript" src="{{url('/assets/Backend/js/login/forgot-password.js')}}"></script>
    </body>
</html>
