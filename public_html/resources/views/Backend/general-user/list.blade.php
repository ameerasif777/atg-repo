
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
	<h1>
		{{$site_title}}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
	</ol>
</section>
<section class="content">
	@if(Session::has('message'))
	<p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
	@endif
	<div class="row"> 
		<div class="col-xs-12">
			<div class="box">
				<form name="frm_general_users" role="form" id="frm_general_users" action="{{url('/')}}/admin/general_user/delete" method="post">
					{!! csrf_field() !!}
					<div class="box-body table-responsive">
						<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
							<table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
								<thead>
									<tr role="row">
										<th> <center>
									Select <br>
									@if (count($arr_general_user_list) > 1)
									<input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
									@endif
								</center></th>
								<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username"> General User Name</th>
								<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Email Verified">Email Verified</th>
								<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Status">Status</th>
								<th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Edit</th>
								<th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action"> View</th>
								</tr>
								</thead>

								<tbody>

									@foreach ($arr_general_user_list as $general_user) 
									<tr>
										<td >

								<center>
									<input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $general_user['id'] !!}" />
								</center>

								</td>
								<td >{{ (isset($general_user['first_name']) && $general_user['first_name'])?stripslashes($general_user['first_name'])." ".stripslashes($general_user['last_name']):'None' }}</td>
								<td >
									<div id="active_div_email{{$general_user['id']}}"  
													@if($general_user['email_verified'] == 1) 
													style="display:inline-block"  
													@else 
													style="display:none;" 
													@endif
													>
													<a class="label label-success" title="Click to Change Status" onClick="changeEmailVerified('{{ $general_user['id']}}', 0);" href="javascript:void(0);" id="status_{{$general_user['id']}}">Verified</a>
									</div>

									<div id="inactive_div_email{{ $general_user['id'] }}" 
											 @if ($general_user['email_verified'] == 0)
											 style="display:inline-block" 
											 @else
											 style="display:none;" 
											 @endif >

											 <a class="label label-danger" title="Click to Change Status" onClick="changeEmailVerified('{{$general_user['id']}}', 1);" href="javascript:void(0);" id="status_{{ $general_user['id'] }}">Not verified</a>
									</div>
								</td>
								<td>

									@if ($general_user['user_status'] == 0)
									<div> <a style="cursor:default;" class="label label-warning" href="javascript:void(0);" id="status_{!! $general_user['id']!!}">Inactive</a> </div>
									@else  
									<div id="active_div{!! $general_user['id']!!}"  @if ($general_user['user_status'] == 1)  style="display:inline-block" @else style="display:none;" @endif >
											 <a class="label label-success" title="Click to Change Status" onClick="changeStatus({!! $general_user['id']!!}, 2);" href="javascript:void(0);" id="status_{!! $general_user['id']!!}">Active</a>
									</div>

									<div id="blocked_div{!! $general_user['id']!!}" @if ($general_user['user_status'] == 2) style="display:inline-block" @else  style="display:none;" @endif >

											 <a class="label label-danger" title="Click to Change Status" onClick="changeStatus({!! $general_user['id']!!}, 1);" href="javascript:void(0);" id="status_{!! $general_user['id']!!}">Blocked</a>
									</div>
									@endif     

								</td>
								
								<td class="">
									<a class="btn btn-info" title="Edit General User Details" href="{{url('/')}}/admin/general_user/edit/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-edit"></i></a>
									 <!--<a class="btn btn-info" title="View Institute Details" href="{{url('/')}}/admin/institute/view/{!! base64_encode($general_user['id']); !!}"> <i class="icon-edit icon-white"></i>View</a>-->
								</td>
								<td class="field-label col-xs-4">
									<a class="btn btn-info" title="View Personal Details" href="{{url('/')}}/admin/general_user/view/personal/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-user"></i></a>
									<a class="btn btn-info" title="View Academics Details" href="{{url('/')}}/admin/general_user/view/academic/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-graduation-cap"></i></a>
									<a class="btn btn-info" title="View Certifications Details" href="{{url('/')}}/admin/general_user/view/certification/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-certificate"></i></a>
									<a class="btn btn-info" title="View Participations & Achievements Details" href="{{url('/')}}/admin/general_user/view/achievement/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-trophy"></i></a>
									<a class="btn btn-info" title="View Interest and Skills Details" href="{{url('/')}}/admin/general_user/view/interest/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-pencil"></i></a>
									<a class="btn btn-info" title="View Language Details" href="{{url('/')}}/admin/general_user/view/language/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-language"></i></a>
									<a class="btn btn-info" title="View Educational Documents Details" href="{{url('/')}}/admin/general_user/view/documents/{!! base64_encode($general_user['id'])!!}"> <i class="fa fa-file-o"></i></a>
								</td>
								@endforeach
								</tbody>

							
								<tfoot>
								<th colspan="9">
									@if (count($arr_general_user_list) > 0) 
									<input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
									@endif
									<!--<a id="add_new_institute" name="add_new_institute" href="{{url('/')}}/admin/institute/add" class="btn btn-primary pull-right" >Add New Institute </a>-->
								</th>
								</tfoot>
		
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

@stop
 <script type="text/javascript">
            function changeStatus(user_id, user_status)
            {
                /* changing the user status*/
                var obj_params = new Object();
                obj_params.user_id = user_id;
                obj_params.user_status = user_status;
                jQuery.get("{{url('/')}}/admin/user/change-status", obj_params, function(msg) {
                    if (msg.error == "1")
                    {
                        alert(msg.error_message);
                    }
                    else
                    {
                        /* togling the bloked and active div of user*/
                        if (user_status == 2)
                        {
                            $("#blocked_div" + user_id).css('display', 'inline-block');
                            $("#active_div" + user_id).css('display', 'none');
                        }
                        else
                        {
                            $("#active_div" + user_id).css('display', 'inline-block');
                            $("#blocked_div" + user_id).css('display', 'none');
                        }
                    }
                }, "json");

            }
            function changeEmailVerified(user_id, user_email_verified)
            {
                /* changing the user status*/
                var obj_params = new Object();
                obj_params.user_id = user_id;
                obj_params.user_email_verified = user_email_verified;
                jQuery.get("{{url('/')}}/admin/user/change-status-email", obj_params, function(msg) {
                    if (msg.error == "1")
                    {
                        alert(msg.error_message);
                    }
                    else
                    {
                        /* togling the bloked and active div of user*/
                        if (user_email_verified == 1)
                        {
                            //location.reload();
                            $("#active_div_email" + user_id).css('display', 'inline-block');
                            $("#inactive_div_email" + user_id).css('display', 'none');
                        }
                        else
                        {
                           // location.reload();
                            $("#inactive_div_email" + user_id).css('display', 'inline-block');
                            $("#active_div_email" + user_id).css('display', 'none');
                        }
                    }
                }, "json");

            }
        </script>