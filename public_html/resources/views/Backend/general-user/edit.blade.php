@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
	<h1>
		Update General User Details
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/general_user"><i class="fa fa-fw fa-user"></i> Manage General User</a></li>
		<li class="active">
			Update General User Details
		</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<form name="frm_user_details" id="frm_user_details" role="form"  action="{{url('/')}}/admin/general_user/edit/{!! $edit_id !!}" method="POST" >
					{{csrf_field() }}
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">


								<div class="form-group">
									<label for="First Name">First Name <sup class="mandatory">*</sup></label>
									<input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_general_user_detail['first_name']))!!}" id="first_name" name="first_name" class="form-control">
								</div>
								<div class="form-group">
									<label for="last_name">Last Name<sup class="mandatory">*</sup></label>
									<input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_general_user_detail['last_name']))!!}" id="last_name" name="last_name" class="form-control">
								</div>
								<div class="form-group">
									<label for="last_name">Birth Date<sup class="mandatory">*</sup></label>
									<input type="text" value="{{ date('m/d/Y',strtotime($arr_general_user_detail['user_birth_date']))}}" id="dob" name="dob" class="form-control">
								</div>
								<div class="form-group">
									<label for="gender">Gender<sup class="mandatory">*</sup></label>
									<div class="radio-inline">
										<label>
											<input type="radio" name="gender" id="gender" checked="" value="1" @if ($arr_general_user_detail['gender'] == 1)  checked @endif> Male
										</label>
									</div>
									<div class="radio-inline">
										<label>
											<input type="radio" name="gender"  id="gender" value="2" @if ($arr_general_user_detail['gender'] == 2)  checked @endif> Female
										</label></div>
								</div>
								<div class="form-group">
									<label for="email">Email Id<sup class="mandatory">*</sup></label>
									<input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_general_user_detail['email']))!!}" id="email" name="email" class="form-control">
												 <input type="hidden" value="{!!stripslashes($arr_general_user_detail['email']) !!}" name="old_email" id="old_email" class="form-control">
								</div>
								<div class="form-group">
									<label for="syncampus_id">Syncampus Id <sup class="mandatory">*</sup></label>
									<input type="text" readonly="" value="{!! str_replace('"', '&quot;', stripslashes($arr_general_user_detail['syncampus_id']))!!}" id="syncampus_id" name="syncampus_id" class="form-control">
								</div>

							</div>
							<div class="col-md-6">



								<div class="form-group">
									<label for="status">Change Status<sup class="mandatory">*</sup></label>
									<select id="user_status" name="user_status" class="form-control">
										@if ($arr_general_user_detail['user_status'] == 0)
										<option value="">Inactive</option>
										@endif
										<option value="1" @if ($arr_general_user_detail['user_status'] == 1)  selected="selected" @endif >Active</option>
										<option value="2" @if ($arr_general_user_detail['user_status'] == 2)  selected="selected" @endif >Blocked</option>
									</select>
								</div>



								<div class="form-group">
									<label for="change_password">Change Password</label>
									<input type="checkbox" class="form-control hide-show-pass-div" name="change_password" id="change_password">
								</div>
								<div id="change_password_div" style="display:none;">
									<div class="form-group">
										<label for="Password">Password <sup class="mandatory">*</sup></label>
										<input type="password" id="user_password" name="user_password" class="form-control">
										<div class="password-meter" style="display:none">
											<div class="password-meter-message password-meter-message-too-short">Too short</div>
											<div class="password-meter-bg">
												<div class="password-meter-bar password-meter-too-short"></div>
											</div>
										</div>
										<span> (Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters) </span> </div>
									<div class="form-group">
										<label for="Confirm Password">Confirm Password<sup class="mandatory">*</sup></label>
										<input type="password" value="" name="confirm_password" id="confirm_password" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="type" id="type" value="1" />
						<button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
						<input type="hidden" name="edit_id" id="edit_id" value="{!! intval(base64_decode($edit_id)) !!}" />
						<img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('/assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('/assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('/assets/Backend/js/admin-manage/edit-admin.js')}}"></script>
<script>
$(document).ready(function () {
	$("#dob").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "1960:2003",
	});
});
</script>
@stop
