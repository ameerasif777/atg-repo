
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                <form name="frm_registered_users" role="form" id="frm_registered_users" action="{{url('/')}}/admin/eye/delete" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>
                                    Select <br>
                                    @if (count($arr_eye_list) > 1)
                                    <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                    @endif
                                </center></th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Eye Color</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Status">Status</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Created on</th>
                                <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                    @foreach ($arr_eye_list as $registered_user) 
                                    <tr>
                                        <td >

                                <center>
                                    <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $registered_user['id'] !!}" />
                                </center>

                                </td>
                                <td >{{ucfirst($registered_user['eye_color'])}}</td>


                                <td>


                                    <div id="active_div{!! $registered_user['id']!!}"  @if ($registered_user['status'] == 1)  style="display:inline-block" @else style="display:none;" @endif >
                                         <a class="label label-success" title="Click to Change Status" onClick="changeStatus({!! $registered_user['id']!!}, 0);" href="javascript:void(0);" id="status_{!! $registered_user['id']!!}">Active</a>
                                    </div>

                                    <div id="blocked_div{!! $registered_user['id']!!}" @if ($registered_user['status'] == 0) style="display:inline-block" @else  style="display:none;" @endif >

                                         <a class="label label-danger" title="Click to Change Status" onClick="changeStatus({!! $registered_user['id']!!}, 1);" href="javascript:void(0);" id="status_{!! $registered_user['id']!!}">Inactive</a>
                                    </div>


                                </td>
                                <td >{!! date($global_values['date_format'], strtotime($registered_user['created_at'])) !!}</td>
                                <td class="">
                                    <a class="btn btn-info" title="Edit Eye Details" href="{{url('/')}}/admin/eye/edit/{!! base64_encode($registered_user['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                    <a class="btn btn-info" title="View Eye Details" href="{{url('/')}}/admin/eye/view/{!! base64_encode($registered_user['id']); !!}"> <i class="icon-edit icon-white"></i>View</a>
                                </td>
                                @endforeach
                                </tbody>


                                <tfoot>
                                    <?php if (count($arr_eye_list) > 0) { ?>
                                  
                                    <th colspan="7">
                                        <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                        <a id="add_new_ethnicity" name="add_new_ethnicity" href="<?php echo url('/'); ?>/admin/eye/add-eye" class="btn btn-primary pull-right" >Add Eye Color</a>
                                    </th>
                                   
                                <?php } else { ?> 
                                  
                                    <th colspan="7">
                                        <a id="add_new_ethnicity" name="add_new_ethnicity" href="<?php echo url('/'); ?>/admin/eye/add-eye" class="btn btn-primary pull-right" >Add Eye Color</a>
                                    </th>
                                   
                                <?php } ?>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop
<script type="text/javascript">
            function changeStatus(user_id, user_status)
            {
            /* changing the user status*/
            var obj_params = new Object();
                    obj_params.user_id = user_id;
                    obj_params.user_status = user_status;
                    jQuery.get("{{url('/')}}/admin/eye/change-status", obj_params, function(msg) {
                    if (msg.error == "1")
                    {
                    alert(msg.error_message);
                    }
                    else
                    {
                    /* togling the bloked and active div of user*/
                    if (user_status == 0)
                    {
                    $("#blocked_div" + user_id).css('display', 'inline-block');
                            $("#active_div" + user_id).css('display', 'none');
                    }
                    else
                    {
                    $("#active_div" + user_id).css('display', 'inline-block');
                            $("#blocked_div" + user_id).css('display', 'none');
                    }
                    }
                    }, "json");
            }
    function changeEmailVerified(user_id, user_email_verified)
    {
    /* changing the user status*/
    var obj_params = new Object();
            obj_params.user_id = user_id;
            obj_params.user_email_verified = user_email_verified;
            jQuery.get("{{url('/')}}/admin/user/change-status-email", obj_params, function(msg) {
            if (msg.error == "1")
            {
            alert(msg.error_message);
            }
            else
            {
            /* togling the bloked and active div of user*/
            if (user_email_verified == 1)
            {
            //location.reload();
            $("#active_div_email" + user_id).css('display', 'inline-block');
                    $("#inactive_div_email" + user_id).css('display', 'none');
            }
            else
            {
            // location.reload();
            $("#inactive_div_email" + user_id).css('display', 'inline-block');
                    $("#active_div_email" + user_id).css('display', 'none');
            }
            }
            }, "json");
    }
</script>