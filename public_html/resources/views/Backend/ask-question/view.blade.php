@extends('Backend.layouts.default')
@section('content') 

<section class="content-header">
    <h1>
        View Qrious Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/question/list"><i class="fa fa-gear"></i> Manage Qrious </a></li>
        <li class="active">View Qrious Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
            <div class="col-xs-6">
                
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                  <tr>
                      <th> QUESTION :</th>                    
                      <td>
                          @if($arr_question[0]->title != '')
                          {{ ucfirst($arr_question[0]->title) }}
                          @else
                          --
                          @endif
                      </td>
                  </tr>

                  <tr>
                      <th> DESCRIPTION :</th>                    
                      <td>
                          @if($arr_question[0]->description != '')
                          {!! ($arr_question[0]->description) !!}
                          @else
                          --
                          @endif
                      </td>
                  </tr>

                <tr>
                    <th> Tags :</th>                    
                    <td>
                        @if($arr_question[0]->tags != '')
                        {{ ucfirst($arr_question[0]->tags) }}
                        @else
                        ---
                        @endif
                    </td>
                </tr>
                @if(count($get_groups) != '')
                 <tr>
                     
                    <th> Posted In :</th> 
                    <td><span>
                        @foreach($get_groups as $key=>$value)    
                        @if($value != '')
                        @if($key == 0)
                            {!! $value->group_name !!} 
                            @else
                            ,{!! $value->group_name !!} 
                            @endif
                        @else
                        ---
                        @endif
                        @endforeach</span>
                        </td>
                 </tr>
                 @endif   
                <tr>
                    <label for="parametername">Qrious Image : </label></br>
                    @if(isset($arr_question[0]->profile_image) && !empty($arr_question[0]->profile_image))
                        <img src="{{ config('feature_pic_url').'qrious_image/'.$arr_question[0]->profile_image}}" alt="" height="150px" width="150px "  onerror=this.src="{{  config('img').'/image-not-found2.jpg'}}" />
                    @else
                        <img src="{{ config('img').'image-not-found2.jpg'}}" alt="" height="150px" width="150px "  width="100%" height="200"/>
                        
                    @endif
                
                </tr>
           </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
              
        </div>
</form>    
    </div>
</section>        
@stop
@section('footer')  
<!--<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>-->
@stop



