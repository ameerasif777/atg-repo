
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        Answer Management
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/question/list"><i class="fa fa-gear"></i> Manage Qrious </a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> Manage Answers</li>

    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif

    <div class="row"> 
        <div class="col-xs-12"> 
            <div class="box">
                <form name="frm_roles" id="frm_roles" action="{{url('/')}}/admin/question/deleteanswer" method="post">
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                        <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                            <thead>
                                @if(count($arr_question[0]->answer) > 0)

                            <th width="5%">
                            <center>
                                Select<br>
                                 @if (!empty($arr_question[0]->answer) && count($arr_question[0]->answer)>1)
                                            <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                            @endif
                            </center>
                            @endif	
                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Comment By">Answered By</th>
                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Comment">Answer</th>
                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Comment on">Answered on</th>
                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Status">Status</th>
                            <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Action</th>
                            </thead>
                            <tbody>

                                @foreach ($arr_question[0]->answer as $answer)

                                <tr>
                                    <td>
                            <center>
                                <input name="answer_ids[]" value="{!! $answer->id !!}" class="case" type="checkbox">
                                
                            </center></td>
                            <td>{!! trim($answer->user_name) !!}
                            </td>
                            <td>{!! $answer->answer !!}
                             </td>
                            <td> {!! $answer->created_at !!} </td>
                            <td>

                                @if($answer->status == "0")
                                Unpublished
                                @elseif($answer->status == "1")
                                Published
                                @elseif($answer->status == "2")
                                Removed
                                @endif

                            </td>
                            <td  style="text-align:center"><a class="btn btn-info" href="{{url('/')}}/admin/question/editanswer/{!! base64_encode($answer->id) !!}"> <i class="icon-edit icon-white"></i> Edit</a></td>
                            </tr>

                            @endforeach
                            </tbody>

                            <tfoot>
                            <th colspan="8">                                
                                        <input type="submit" value="Delete Selected" onclick="return deleteConfirm();" class="btn btn-danger" name="btn_delete_all" id="btn_delete_all">
                                        
                                
                            </th>
                            </tfoot>

                        </table>
                    </div>
                </div>
                <!--[sortable body]--> 
            </div>
        </div>
    </div>
</section>
@stop