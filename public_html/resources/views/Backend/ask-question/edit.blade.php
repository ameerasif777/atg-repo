@extends('Backend.layouts.default')
@section('content') 
<link rel="stylesheet" type="text/css" href="{{url('/assets/Backend/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{url('/assets/Backend/js/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
    <h1>
        Edit Qrious Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/question/list"><i class="fa fa-gear"></i> Manage Qrious </a></li>
        <li class="active">Edit Qrious Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <form name="form_edit_post_question" id="form_edit_post_question" method="post" action="{{url('/')}}/admin/question/edit/{!!base64_encode($arr_question[0]->id)!!}" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="box box-primary">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="question">Qrious <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Question" value="{{ ucfirst($arr_question[0]->title) }}">
                                </div>

                                <div class="form-group">
                                    <label for="parametername">Post Description<sup class="mandatory">*</sup></label>
                                    <textarea type="text" dir="ltr"  class="form-control ckeditor" name="description" id="description" >{!! $arr_question[0]->description !!}</textarea>
                                    @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                                </div>                                            
                                <div class="form-group">
                                    <label for="parametername">Tags : </label>
                                    <input type="text" class="form-control" placeholder="Tags" name="tags" id="tags" value="{{ ucfirst($arr_question[0]->tags) }}" />
                                    <div class="text-danger" for="tags" generated="true"></div>
                                </div>
                                <div class="form-group">
                                    <label for="">GROUP</label>

                                    <select name="sub_groups[]" id="sub_groups[]" multiple="" placeholder="Groups" class="form-control multipleSelect" size="{{count($user_group_data)}}" style="display: none;">
                                        @foreach($user_group_data as  $value)
                                        @if(isset($arr_selected_groups) && in_array($value->id,$arr_selected_groups))
                                        <option value="{{$value->id}}" selected="">{{$value->group_name}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->group_name}}</option>
                                        @endif                            
                                        @endforeach
                                    </select>     
                                    <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();"></ul>
                                    <div class="text-danger" for="sub_groups[]" generated="true"></div>
                                </div>
                                @include('Backend.includes.add-group-field-for-backend-features', ['arr_selected_groups1' => $arr_selected_groups1])

                                <div class="form-group">
                                    <label for="parametername">Publish Status</label>
                                    <select class="form-control" name="status" id="status" class="">
                                        <option @if($arr_question[0]->status == "0")  selected="selected" @endif  value="0">Unpublished</option>
                                        <option @if($arr_question[0]->status == "1")  selected="selected" @endif value="1">Published</option>
                                        <option @if($arr_question[0]->status == "2")  selected="selected" @endif value="2">Removed</option>
                                    </select>

                                </div>
                                <label for="sub_groups[]" id="errorElement"></label>
                                <div class="form-group">
                                    <label for="parametername">Image : </label>
                                    <input type="file" name='question_pic' id="question_pic" onchange="readURL(this);" /></br>
                                    <!--<img class="uploding-img-vie" height="200px"  width="200px" id="blah" src="{{ url('/public/assets/Frontend/images/article_image/thumb/'.$arr_question[0]->profile_image) }}"  alt="" onerror=this.src="{{ url('/assets/Backend/img/image-not-found2.jpg') }}" width=100%" height="200"/><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>-->
                                    <img class="uploding-img-vie" height="200px"  width="200px" id="blah" src="{{ url('/public/assets/Frontend/images/qrious_image/'.$arr_question[0]->profile_image) }}" onerror=this.src="{{ url('/public/assets/Frontend/img/image-not-found2.jpg')}}" width="100%" height="200"/><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" id='btn_article_post' name="btn_article_post" class="btn btn-primary" value="Save" onclick="getDescription();">Save</button>
                                    <img  src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>        
@stop
@section('footer')  
<!--<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>-->


<script type="text/javascript">

    function getDescription(){
        $('#description').html(jQuery.trim(jQuery('#cke_1_contents iframe').contents().find('body').html().replace(/<[^>]+>/g, '')));
    }

    jQuery(document).ready(function() {


        $("#form_edit_post_question").validate({
            ignore: '',
            errorElement: 'div',
            errorClass: 'text-danger',
            rules: {},
            messages: {},
            submitHandler: function(form) {
                $("#btn_article_post").hide();
                $("#loding_image").show();
                form.submit();
            }

        });
    });

    function readURL(input) {
        var ext = input.value.match(/\.(.+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#tags').tokenfield({
                            autocomplete: {
                                source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                delay: 100
                            },
                            showAutocompleteOnFocus: true
                        });
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Only Image allowed !');
                input.value = '';
        }
    }
</script>
<script>
    $('#tags').tokenfield({
        autocomplete: {
            source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    });
    /*$('.multipleSelect').popSelect({
        showTitle: false,
        maxallowed: 5,
        autoIncrease: true,
        placeholderText: 'Please select the groups'
    });*/

</script>
@stop

