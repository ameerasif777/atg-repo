@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>{{ $site_title }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif

    @if(Session::has('success_msg'))
        <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif

    @include('Backend.includes.posts-table', [
        'post_type' => 'question',
        'posts' => $arr_question,
        'post_image_path' => config('feature_pic_url').'qrious_image/',
        'post_view_path' => '/admin/question/view/',
        'post_edit_path' => '/admin/question/edit/',
        'post_answer_path' => '/admin/questions/answer/',
        'post_delete_path' => '/admin/question/delete/',
        'status' => $sstatus,
        'record_per_page' => $recordPerPage,
        'search_query' => $sqry
    ])
</section>
@stop
