@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Edit Answer 
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>        
        <li class="active">Edit Answer</li>

    </ol>
    
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form name="frm_edit_answer" role="form"  id="frm_edit_answer" action="{{url('/')}}/admin/question/editanswer/{!! $arr_answer[0]->id  !!}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                           
                            <div class="form-group">
                                <label for="parametername">Answer<sup class="mandatory">*</sup></label>
                                <input type="text" dir="ltr"  class="form-control" placeholder="Answer"  name="answer" id="answer" value="{!! $arr_answer[0]->answer !!}"  />
                            </div>
                            

                            <div class="form-group">
                                <label for="parametername">Publish Status</label>
                                <select class="form-control" name="status" id="status" class="">
                                    <option @if($arr_answer[0]->status == "0")  selected="selected" @endif  value="0">Unpublished</option>
                                    <option @if($arr_answer[0]->status == "1")  selected="selected" @endif value="1">Published</option>
                                    <!--<option @if($arr_answer[0]->status == "2")  selected="selected" @endif value="2">Removed</option>-->
                                </select>
                            </div>

                            
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>

                        </div>

                    
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script type="text/javascript">




  $("#frm_edit_answer").validate({
   rules:{
       answer:{
           required:true,
           
       },
       
       status:{
           required:true,
       },
       
   },
   messages:{
       answer:{
           required:"Please enter answer.",
       },
      
       status:{
           required:"Please select status.",
           
       },submitHandler: function(form) {
           $("#btnSubmit").hide();
       }
       
   },
   
}); 



</script>

@stop