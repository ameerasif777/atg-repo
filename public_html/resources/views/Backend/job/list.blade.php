@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>{{ $site_title }}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{ $site_title }}</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="row" style="padding: 10px">
                    <div class="col-md-2">
                        <select id="recordPerPage" name="recordPerPage" class="form-control" onchange="doSearch()">
                            <option value="25" {{ ($record_per_page == 25) ? 'selected' : '' }}>25</option>
                            <option value="50" {{ ($record_per_page == 50) ? 'selected' : '' }}>50</option>
                            <option value="100" {{ ($record_per_page == 100) ? 'selected' : '' }}>100</option>
                            <option value="200" {{ ($record_per_page == 200) ? 'selected' : '' }}>200</option>
                            <option value="500" {{ ($record_per_page == 500) ? 'selected' : '' }}>500</option>
                        </select>
                    </div>
                    <div class="col-md-2">records per page</div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">&nbsp;</div>
                            <div class="col-md-1">Search</div>
                            <div class="col-md-4">
                                <input id="search-query" name="search-query" value="{{ $search_query }}" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <select id="search-status" name="search-status" class="form-control">
                                    <option value="" {{ ($search_status == '') ? 'selected' : '' }}>All</option>
                                    <option value="0" {{ ($search_status == '0') ? 'selected' : '' }}>Unpublished</option>
                                    <option value="1" {{ ($search_status == '1') ? 'selected' : '' }}>Published</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="button" id="search_btn" name="search_btn" class="btn btn-info" onclick="doSearch()" value="Go">
                            </div>
                        </div>
                    </div>
                </div>

                <form name="frm_registered_users" role="form" id="frm_registered_users" action="{{ url('admin/job/delete') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                            <table class="table table-bordered table-striped" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th>
                                            Select <br>
                                            @if (count($jobs) > 1)
                                                <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all">
                                            @endif
                                        </th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">ID</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Title</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Job Location</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Posted By</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Posted On</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Status</th>
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Report</th>
                                        {{--<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Year Experience</th>--}}
                                        {{--<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Status">Status</th>--}}
                                        <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($jobs as $job)
                                    <tr>
                                        <td><input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $job['id'] !!}"></td>
                                        <td>{{ $job['id'] }}</td>
                                        <td>{{ucfirst($job['title'])}}</td>
                                        <td>@if($job['job_location'] != ''){!! $job['job_location'] !!}@else - @endif</td>
                                        <td>{{ $job['user']['user_name'] }} ({{ $job['user']['id'] }})</td>
                                        <td>{!! ($job['created_at']) !!}</td>
                                        <td>@if($job['status'] == '1') Published @else Unpublished @endif</td>
                                        <td>{{ count($job['reported']) }}</td>
                                        <td class="">
                                            <a class="btn btn-info" title="Edit Job Details" href="{{url('/')}}/admin/job/edit/{!! base64_encode($job['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                            <a class="btn btn-info" title="View Job Details" href="{{url('/')}}/admin/job/view/{!! base64_encode($job['id']); !!}"> <i class="icon-edit icon-white"></i>View</a>
                                        </td>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <th colspan="9">
                                        @if (count($jobs) > 0)
                                            <input type="button" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="deleteModel();"  value="Delete Selected">
                                        @endif
                                    </th>
                                    </tfoot>
                                </table>
                            </div>
                        <div class="text-right">{{ $jobs->appends(['search-status' => $search_status, 'search-query' => $search_query, 'rpp' => $record_per_page])->links() }}</div>
                    </div>
                        <div class="modal model-style" id="show-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Delete Job</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do you really want to delete?</p>
                                        <p><input type="checkbox" id="send-mail" name="send-mail"> Send email to user.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="cancle">Cancel</button>
                                        <button type="button" id="delete">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
            function changeStatus(id, job_status)
            {
            /* changing the user status*/
            var obj_params = new Object();
                    obj_params.id = id;
                    obj_params.job_status = job_status;
                    jQuery.get("{{url('/')}}/admin/job/change-status", obj_params, function(msg) {
                    if (msg.error == "1")
                    {
                    alert(msg.error_message);
                    }
                    else
                    {
                    /* togling the bloked and active div of user*/
                    if (job_status == 1)
                    {
                    $("#blocked_div" + id).css('display', 'inline-block');
                            $("#active_div" + id).css('display', 'none');
                    }
                    else
                    {
                    $("#active_div" + id).css('display', 'inline-block');
                            $("#blocked_div" + id).css('display', 'none');
                    }
                    }
                    }, "json");
            }
            
            
            function deleteModel()
    {
        $("#show-modal").show();
    }
    document.getElementById("delete").onclick = function () {

        var del_num = 0;
        jQuery('.case').each(function () {
            if (jQuery('.case').is(":checked")) {
                del_num = 1;
            }
        });

        if (!del_num) {
            alert("Please select atleast one record to delete");
            return false;
        } else {
            document.getElementById("frm_registered_users").submit();
        }

    };
    document.getElementById("cancle").onclick = function () {
        $("#show-modal").hide();
    };

    function doSearch() {
        var queryStr = '?rpp=' + $('#recordPerPage').val() + '&search-query=' + $('#search-query').val() + '&search-status=' + $('#search-status').val();
        location.href = location.href.split('?')[0] + queryStr;
    }
</script>
@stop
