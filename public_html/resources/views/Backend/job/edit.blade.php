@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Edit Job Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/job-list"><i class="fa fa-gear"></i> Manage Job List</a></li>
        <li class="active">Edit Job Details</li>

    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <form name="form_post_job" id="form_post_job" method="post" action="{{url('/')}}/admin/job/edit/{!!base64_encode($arr_job_details[0]->id)!!}">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="title">Title <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{ ucfirst($arr_job_details[0]->title) }}">
                    </div>
                    <div class="form-group">
                        <label for="title">Company Name <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="cmp_name" id="cmp_name" placeholder="Company name" value="{{ ucfirst($arr_job_details[0]->cmp_name) }}">
                    </div>
                    <div class="form-group">
                        <label for="location">Location <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="location" id="location" placeholder="Location" value='{{ ucfirst($arr_job_details[0]->job_location) }}'>
                        <input type="hidden"  name="latitude"   class="form-control" value='{{ $arr_job_details[0]->latitude }}'>
                        <input type="hidden"  name="longitude"   class="form-control" value='{{ $arr_job_details[0]->longitude }}'>
                    </div>
                    <div class="form-group">
                        <label for="description">Job Description<sup class="mandatory">*</sup></label>
                        <textarea id="description" name="description" placeholder="Job Description" class="form-control" >{{ ucfirst($arr_job_details[0]->description) }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="experiance">Min Year Of Experience <sup class="mandatory">*</sup></label>
                        <select id='min' name='min' class="form-control">
                            <option disabled selected="">Min-Years</option>
                            @for ($i = 0; $i <= 20; $i++)
                            <option value="{{$i}}" @if($arr_job_details[0]->min == $i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="experiance">MAx Year Of Experience <sup class="mandatory">*</sup></label>
                        <select id='max' name='max' class="form-control">
                            <option disabled="" selected="">Max-Years</option>
                            @for ($i = 0; $i <= 20; $i++)
                            <option value="{{$i}}" @if($arr_job_details[0]->max == $i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="qualification">Education <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="qualification" id="qualification" placeholder="Education" value='{{ ucfirst($arr_job_details[0]->preferred_qualification) }}'>
                        <div for="qualification" generated="true" class="text-danger"></div>
                    </div>
                    
                    <div class="form-group">
                        <label for="role">Roles <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="role" id="role" placeholder="Role" value='{{ ucfirst($arr_job_details[0]->role) }}'>
                    </div>
                    <div class="form-group">
                        <label for="skill">Skills <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="skills" id="skill" placeholder="Skills" value='{{ ucfirst($arr_job_details[0]->skill) }}'>
                        <div class="text-danger" for="skill" generated="true"></div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="tag">Tags <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="tags" id="tags" placeholder="Tags" value='{{ ucfirst($arr_job_details[0]->tags) }}'>
                        <div class="text-danger" for="tags" generated="true"></div>
                    </div>
                    
                    <div class="form-group">
                        <label for="sub_groups">Groups <sup class="mandatory">*</sup></label>
                        <select name="sub_groups[]" class="form-control multipleSelect" multiple select  style="display: none;">
                            @foreach($arr_groups as $key=>$value)
                            <option value="{!! $value->id !!}" @if(in_array($value->id,$arr_selected_groups)) selected @endif >{!! $value->group_name !!}</option>
                            @endforeach
                        </select>
                        <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();"></ul>
                        <div class="text-danger" for="sub_groups[]" generated="true"></div>
                    </div>
                    @include('Backend.includes.add-search-group-field', ['arr_selected_groups' => $arr_selected_groups1, 'id' => '' , 'key' => '0'])

                    <div class="form-group">
                        <label for="link">External Link to Apply </label>
                        <input type="text" class="form-control" name="link" id="link" placeholder="External Link to Apply" value='{{ ucfirst($arr_job_details[0]->external_link_to_apply) }}'>
                    </div>
                    <div class="form-group">
                        <label for="functional_area">Functional Area </label>
                        <input type="text" class="form-control" name="functional_area" id="functional_area" placeholder="Functional Area" value='{{ ucfirst($arr_job_details[0]->functional_area) }}'>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="employment_type">Employment Type :</label>
                            <select class="form-control" name="employment_type" id="employment_type">
                                <option value="1" @if ($arr_job_details[0]->employment_type == 1)  selected="selected" @endif >Full Time</option>
                                <option value="2" @if ($arr_job_details[0]->employment_type == 2)  selected="selected" @endif >Part Time</option>
                                <option value="3" @if ($arr_job_details[0]->employment_type == 3)  selected="selected" @endif >Internship</option>
                                <option value="4" @if ($arr_job_details[0]->employment_type == 4)  selected="selected" @endif >Fellowship</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date">Application Deadline <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="end_date" id="end_date" placeholder="End Date" value='{{ $arr_job_details[0]->application_deadline }}'>
                    </div>
                    <div class="form-group">
                        <label for="salary_range">Salary Range <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="salary_range" id="salary_range" placeholder="Salary Range" value='{{ $arr_job_details[0]->salary_range }}'>
                    </div>
                    <div class="form-group">
                        <label for="phone">Mobile No <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone No" value='{{ ucfirst($arr_job_details[0]->phone_number) }}'>
                    </div>
                    <div class="form-group">
                        <label for="phone">Telephone No <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="telephone" id="telephone" placeholder="Telephone No" value='{{ ucfirst($arr_job_details[0]->telephone_number) }}'>
                    </div>
                    <div class="form-group">
                        <label for="email">Email <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value='{{ $arr_job_details[0]->email_address }}'>
                    </div>
                    <div class="form-group">
                        <label for="website">Website <sup class="mandatory">*</sup></label>
                        <input type="text" class="form-control" name="website" id="website" placeholder="Website" value='{{ $arr_job_details[0]->website }}'>
                    </div>
                    <div class="form-group">
                        <label for="website">About Us </label>
                        <textarea class="form-control" name='about_us' placeholder="Write something about job...">{{ ucfirst($arr_job_details[0]->about_us) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="website">Contact Us</label>
                        <textarea class="form-control" name='contact_us' placeholder="Write something your Contact...">{{ ucfirst($arr_job_details[0]->contact_us) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="website">How to Apply </label>
                        <textarea class="form-control" name='how_to_apply' placeholder="how-to-apply...">{{ ucfirst($arr_job_details[0]->how_to_apply) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="website">Hiring Process </label>
                        <textarea class="form-control" name='hiring_process' placeholder="Write something hiring-process...">{{ ucfirst($arr_job_details[0]->hiring_process) }}</textarea>
                    </div>
<!--                    <div class="form-group">
                        <label for="status">Change Status<sup class="mandatory">*</sup></label>
                        <select id="status" name="status" class="form-control">
                            <option value="0" @if ($arr_job_details[0]->status == 0)  selected="selected" @endif >Active</option>
                            <option value="1" @if ($arr_job_details[0]->status == 1)  selected="selected" @endif >Inactive</option>
                        </select>
                    </div>-->
                    <div class="box-footer">
                        <button type="submit" id='btn_job_post' name="btn_job_post" class="btn btn-primary" value="Save">Save</button>
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        <input type="hidden" name="icon_path" id="path" value="{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/">
                        <input type="hidden" name="cover_path" id="search_path" value="{{url('/')}}/public/assets/Backend/img/group_img/cover/thumb/">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script src="https://maps.googleapis.com/maps/api/js?v=3.23&libraries=places&language=en&key={{ env('G_MAPS_KEY') }}"></script> 
<script src="{{url('/assets/Backend/js/jquery.geocomplete.min.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/ckeditor/ckeditor.js')}}"></script>
<script>
$(document).ready(function () {
    /*$('.multipleSelect').popSelect({
        showTitle: false,
        autoIncrease: true,
        maxAllowed: 5,
        placeholderText: 'Please select the groups',
        position: 'bottom'
    });*/
    
    jQuery("#form_post_job").validate({
        ignore: '',
        errorElement: 'div',
        errorClass: 'text-danger',
        rules: {},
        messages: {},
        submitHandler: function (form) {
            jQuery("#btn_job_save").attr('disabled', true);
            form.submit();
        }


    });
    $('#tags').tokenfield({
        autocomplete: {
            // source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })

    $('#skill').tokenfield({
        autocomplete: {
            // source: ['css3', 'php', 'java', 'javascript', 'jquery', '.net', 'html5', 'laravel', 'codeigniter'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    $('#qualification').tokenfield({
        autocomplete: {
            // source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
})
</script>
<script>
    $(function () {
        $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            //  map: ".map_canvas",
        });

        $("#location").bind("geocode:result", function (event, result) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': result.formatted_address}, function (results, status) {
                var location = results[0].geometry.location;
                var lat = location.lat();
                var lng = location.lng();
                $("input[name=latitude]").val(lat);
                $("input[name=longitude]").val(lng);

            });


        });

        $('#end_date').datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: 0,
            dateFormat: 'yy-mm-dd',
        });
        
         $("#min").change(function(){
        $("#max").html('')
        var i,min_val = $("#min").val()
        min_val=parseInt(min_val) + 1
                    $("#max").append('<option disabled selected>Max-Years</option>');
        for(i=min_val;i<=20;i++){
            $("#max").append('<option value = ' + i + '>'+ i +'</option>');
        }
        
        
    });


    });
</script> 


@stop
