@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <!--    <h1>
            Student Details
        </h1>-->
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/job-list"><i class="fa fa-fw fa-user"></i> Manage Jobs</a></li>
        <li class="active">
            Job Details
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p class="lead"> Job Details</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th> Title :</th>
                                <td>{{ ucfirst($arr_job_details[0]->title) }}</td>
                            </tr>
                            <tr>
                                <th> Company Name :</th>
                                <td>{{ ucfirst($arr_job_details[0]->cmp_name) }}</td>
                            </tr>
                            <tr>
                                <th> Location :</th>
                                <td>{{ ucfirst($arr_job_details[0]->job_location) }}</td>
                            </tr>
                            <tr>
                                <th> Description :</th>
                                <td>{{ ucfirst($arr_job_details[0]->description) }}</td>
                            </tr>
                            <tr>
                                <th> Min Year Of Experience :</th>
                                <td>{{ ucfirst($arr_job_details[0]->min) }}</td>
                            </tr>
                            
                            <tr>
                                <th> Max Year Of Experience :</th>
                                <td>{{ ucfirst($arr_job_details[0]->max) }}</td>
                            </tr>
                            <tr>
                                <th> Preferred Qualification :</th>
                                <td>{{ ucfirst($arr_job_details[0]->preferred_qualification) }}</td>
                            </tr>
                            <tr>
                                <th> Roles :</th>
                                <td>{{ ucfirst($arr_job_details[0]->role) }}</td>
                            </tr>
                            <tr>
                                <th> Skills :</th>
                                <td>{{ ucfirst($arr_job_details[0]->skill) }}</td>
                            </tr>
                            <tr>
                                <th> Tags :</th>
                                <td>{{ ucfirst($arr_job_details[0]->tags) }}</td>
                            </tr>
                            <tr>
                                <th> External Link :</th>
                                <td>{{ ucfirst($arr_job_details[0]->external_link_to_apply) }}</td>
                            </tr>
                            <tr>
                                <th> Functional Area :</th>
                                <td>{{ ucfirst($arr_job_details[0]->functional_area) }}</td>
                            </tr>
                            <tr>
                                <th> Employment Type :</th>
                                <td>
                                    @if($arr_job_details[0]->employment_type == 1)
                                    Full Time
                                    @elseif($arr_job_details[0]->employment_type == 2)    
                                    Part Time
                                    @elseif($arr_job_details[0]->employment_type == 3)
                                    Internship
                                    @else
                                    Fellowship
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Groups</th>
                                <td>
                                    <span>
                            @foreach($arr_selected_groups as $key=>$value)
                            @if($key == 0)
                            {!! $value->group_name !!} 
                            @else
                            ,{!! $value->group_name !!} 
                            @endif
                            
                            @endforeach</span>
                        </td>
                        
                        </tr>
                        @if(($arr_job_details[0]->application_deadline != 0000-00-00 ))
                        <tr>
                            <th> Application Deadline :</th>
                            <td>{{ date('d M Y',strtotime($arr_job_details[0]->application_deadline)) }}</td>
                        </tr>
                        @else
                        <tr>
                            <th> Application Deadline :</th>
                            <td> - - - </td>
                        </tr>
                        @endif
                         @if(($arr_job_details[0]->application_deadline != 0.00 ))
                        <tr>
                            <th> Salary Range :</th>
                            <td>{{ $arr_job_details[0]->salary_range }}</td>
                        </tr>
                        @else
                        <tr>
                            <th> Salary Range :</th>
                            <td> - -</td>
                        </tr>
                        @endif
                        <tr>
                            <th> Mobile Number :</th>
                            <td>{{ ucfirst($arr_job_details[0]->phone_number) }}</td>
                        </tr>
                        <tr>
                            <th> Telephone Number :</th>
                            <td>{{ ucfirst($arr_job_details[0]->telephone_number) }}</td>
                        </tr>
                        <tr>
                            <th> Email Address :</th>
                            <td>{{ $arr_job_details[0]->email_address }}</td>
                        </tr>
                        <tr>
                            <th> Website :</th>
                            <td>{{ $arr_job_details[0]->website }}</td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('//assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('//assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('//assets/Backend/js/admin-manage/edit-admin.js')}}"></script>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>
<script>
$(document).ready(function () {
    $('.multipleSelect').popSelect({
        showTitle: false,
        autoIncrease: true,
        maxAllowed: 5,
        placeholderText: 'Please select the groups',
        position: 'bottom'
    });
    $('#tags').tokenfield({
        autocomplete: {
            // source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    $('#qualification').tokenfield({
        autocomplete: {
            // source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })

    $('#skill').tokenfield({
        autocomplete: {
            // source: ['css3', 'php', 'java', 'javascript', 'jquery', '.net', 'html5', 'laravel', 'codeigniter'],
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
})
</script>

@stop
