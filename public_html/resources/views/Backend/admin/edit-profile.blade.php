@extends('Backend.layouts.default')
@section('content') 
    <section class="content-header">
        <h1>
              Update Admin User Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <!--<li><a href="{{url('/')}}/admin/admin/list"><i class="fa fa-fw fa-user"></i> Manage Admin Users</a></li>-->
            <li class="active">
                   Update Admin User Details
            </li>
        </ol>
    </section>
    <section class="content">
         <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <form name="frm_admin_details" id="frm_admin_details" role="form"  action="{{url('/')}}/admin/admin/edit-profile" method="POST" >
                         {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="User Name">User Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_admin_detail['user_name']))!!}" id="user_name" name="user_name" class="form-control">
                                        <input type="hidden" value="{!! str_replace('"', '&quot;', stripslashes($arr_admin_detail['user_name']))!!}" id="old_username" name="old_username">

                                    </div>
                                    <div class="form-group">
                                        <label for="First Name">First Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_admin_detail['first_name']))!!}" name="first_name" id="first_name"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Last Name">Last Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($arr_admin_detail['last_name']))!!}" name="last_name" id="last_name"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Email Id">Email Id <sup class="mandatory">*</sup></label>
                                        <input type="text" value="{!! stripslashes($arr_admin_detail['email']) !!}" name="email" id="email" class="form-control">
                                        <input type="hidden" value="{!!stripslashes($arr_admin_detail['email']) !!}" name="old_email" id="old_email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Email Id">Change Password</label>
                                        <input type="checkbox" class="form-control hide-show-pass-div" name="change_password" id="change_password">
                                    </div>
                                    <div id="change_password_div" style="display:none;">
                                        <div class="form-group">
                                            <label for="Password">Password <sup class="mandatory">*</sup></label>
                                            <input type="password" id="user_password" name="user_password" class="form-control">
                                            <div class="password-meter" style="display:none">
                                                <div class="password-meter-message password-meter-message-too-short">Too short</div>
                                                <div class="password-meter-bg">
                                                    <div class="password-meter-bar password-meter-too-short"></div>
                                                </div>
                                            </div>
                                            <span> (Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters) </span> </div>

                                        <div class="form-group">
                                            <label for="Confirm Password">Confirm Password<sup class="mandatory">*</sup></label>
                                            <input type="password" value="" name="confirm_password" id="confirm_password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Gender">Gender<sup class="mandatory">*</sup></label>
                                        <input  class="form-control" id="gender" type="radio" value="1"  name="gender" @if($arr_admin_detail['gender'] == 1)  checked="checked" @endif >
                                        Male
                                        <input id="gender" type="radio" value="2" name="gender" @if ($arr_admin_detail['gender'] == 2) checked="checked" @endif >
                                        Female
                                    </div>
                                    <input type="hidden" name="role_id" id="role_id" value="{!! $arr_admin_detail['role_id']!!}" />
                                    <input type="hidden" name="user_status" id="user_status" value="{!! $arr_admin_detail['user_status']!!}" />

                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                            <input type="hidden" name="edit_id" id="edit_id" value="{!! $edit_id !!}" />
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('//assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('//assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('//assets/Backend/js/admin-manage/edit-admin.js')}}"></script>

@stop
