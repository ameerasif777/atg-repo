@extends('Backend.layouts.default')
@section('content') 
    <section class="content-header">
        <h1>
               Admin Profile Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">
                  Admin Profile
            </li>
       </ol>
    </section>
    <section class="content">
         <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <p class="lead">My Profile</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody><tr>
                                    <th style="width:50%">User Name:</th>
                                    <td> {!! (isset($arr_admin_detail['user_name']) && $arr_admin_detail['user_name']!='')? $arr_admin_detail['user_name']:'None' !!}</td>
                                </tr>
                                <tr>
                                    <th>First Name :</th>
                                    <td>{!! (isset($arr_admin_detail['first_name']) && $arr_admin_detail['first_name']!='')? $arr_admin_detail['first_name']:'None' !!} </td>
                                </tr>
                                <tr>
                                    <th>Last Name :</th>
                                    <td>{!! (isset($arr_admin_detail['last_name']) && $arr_admin_detail['last_name']!='')? $arr_admin_detail['last_name']:'None' !!} </td>
                                </tr>
                                <tr>
                                    <th>Email Id:</th>
                                    <td>{!! (isset($arr_admin_detail['email']) && $arr_admin_detail['email']!='')? $arr_admin_detail['email']:'None' !!} </td>
                                </tr>
                                <tr>
                                    <th>Role:</th>
                                    <td> @if ($arr_admin_detail->values->count() > 0)
                                                @foreach ($arr_admin_detail->values as $row)
                                                    {!! ucfirst($row->role_name) !!}<br/>
                                                @endforeach
                                            @else
                                                None
                                            @endif</td>
                                </tr>
                                <tr>
                                    <th>Registered Date:</th>
                                    <td>{!! date($global_values['date_format'], strtotime($arr_admin_detail['created_at'])) !!}</td>
                                </tr>
                                <tr>
                                    <th>Gender:</th>
                                    <td>
                                        
                                        @if ($arr_admin_detail['gender'] == 1)
                                            Male
                                        @elseif ($arr_admin_detail['gender'] == 2)
                                           Female
                                       @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>        
@stop

