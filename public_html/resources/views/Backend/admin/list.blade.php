@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        Manage Admin Users 
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> Manage Admin Users </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
         <div class="col-xs-12">
            <div class="box">
                <form name="frm_admin_users" role="form" id="frm_admin_users" action="{{url('/')}}/admin/admin/list" method="post">
                   {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>
                                    Select <br>
                                    @if (count($arr_admin_list) > 1)
                                        <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                  @endif
                                </center></th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Username</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="First Name">First Name</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Last Nam">Last Name</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Email Id">Email Id</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Role">Role</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Status">Status</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Created on</th>
                                <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                    @foreach ($arr_admin_list as $admin) 
                                        <tr>
                                            <td >
                                        @if ($admin['role_id'] != 1)
                                        <center>
                                            <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $admin['id'] !!}" />
                                        </center>
                                        @endif   
                                    </td>
                                    <td >{{ (isset($admin['user_name']) && $admin['user_name'])?stripslashes($admin['user_name']):'None' }}</td>
                                    <td >{{ (isset($admin['first_name']) && $admin['first_name'])?stripslashes($admin['first_name']):'None' }}</td>
                                    <td >{{ (isset($admin['last_name']) && $admin['last_name'])?stripslashes($admin['last_name']):'None' }}</td>
                                    <td >{{ (isset($admin['email']) && $admin['email'])?stripslashes($admin['email']):'None' }}</td>
                                    <td >
                                     @if ($admin->values->count() > 0)
                                            @foreach ($admin->values as $row)
                                                {!! ucfirst($row->role_name) !!}<br/>
                                            @endforeach
                                        @else
                                            None
                                        @endif
                                    </td>
                                    <td >
                                        @if ($admin['role_id'] != 1)
                                            @if ($admin['user_status'] == 0)
                                                <div> <a style="cursor:default;" class="label label-warning" href="javascript:void(0);" id="status_{!! $admin['id']!!}">Inactive</a> </div>
                                            @else  
                                                <div id="active_div{!! $admin['id']!!}"  @if ($admin['user_status'] == 1)  style="display:inline-block" @else style="display:none;" @endif >
                                                    <a class="label label-success" title="Click to Change Status" onClick="changeStatus({!! $admin['id']!!}, 2);" href="javascript:void(0);" id="status_{!! $admin['id']!!}">Active</a>
                                                </div>

                                                <div id="blocked_div{!! $admin['id']!!}" @if ($admin['user_status'] == 2) style="display:inline-block" @else  style="display:none;" @endif >

                                                    <a class="label label-danger" title="Click to Change Status" onClick="changeStatus({!! $admin['id']!!}, 1);" href="javascript:void(0);" id="status_{!! $admin['id']!!}">Blocked</a>
                                                </div>
                                            @endif     
                                        @else
                                            <div id="active_div"> <a class="label label-success" style="cursor:default;" title="" href="javascript:void(0);" id="status">Active</a> </div>
                                        @endif 
                                    </td>
                                    <td >{!! date($global_values['date_format'], strtotime($admin['created_at'])) !!}</td>
                                    <td class="">
                                        @if ($admin['role_id'] == 1) 
                                            @if($admin['id'] == Auth::user()->id) 
                                                <a class="btn btn-info" title="Edit Admin User Details" href="{{url('/')}}/admin/admin/edit/{!! base64_encode($admin['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                            @endif   
                                       @else
                                            <a class="btn btn-info" title="Edit Admin User Details" href="{{url('/')}}/admin/admin/edit/{!! base64_encode($admin['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                        @endif
                                    </td>
                                    @endforeach
                                </tbody>

                                @if (count($arr_admin_list) >= 1) 
                                    <tfoot>
                                        <th colspan="9">
                                        @if (count($arr_admin_list) > 1) 
                                            <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                        @endif
                                            <a id="add_new_admin" name="add_new_admin" href="{{url('/')}}/admin/admin/add" class="btn btn-primary pull-right" >Add New Admin </a>
                                        </th>
                                    </tfoot>
                                @endif
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    function changeStatus(user_id, user_status)
    {
        /* changing the user status*/
        var obj_params = new Object();
        obj_params.user_id = user_id;
        obj_params.user_status = user_status;
        jQuery.get("{{url('/')}}/admin/admin/change-status", obj_params, function (msg) {
            if (msg.error == "1")
            {
                alert(msg.error_message);
            }
            else
            {
                /* toogling the bloked and active div of user*/
                if (user_status == 2)
                {
                    $("#blocked_div" + user_id).css('display', 'inline-block');
                    $("#active_div" + user_id).css('display', 'none');
                }
                else
                {
                    $("#active_div" + user_id).css('display', 'inline-block');
                    $("#blocked_div" + user_id).css('display', 'none');
                }
            }

        }, "json");

    }
</script>
@stop