@extends('Backend.layouts.default')
@section('content') 
    <section class="content-header">
        <h1>
                Add New Admin
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('/')}}/admin/admin/list"><i class="fa fa-fw fa-user"></i> Manage Admin Users</a></li>
            <li class="active">Add New Admin</li>

        </ol>
    </section>
    <section class="content">
         <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <form name="frm_admin_details" role="form" id="frm_admin_details"  action="{{url('/')}}/admin/admin/add" method="POST" >
                    {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="User Name">User Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="" id="user_name" name="user_name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="First Name">First Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="" name="first_name" id="first_name"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Last Name">Last Name <sup class="mandatory">*</sup></label>
                                        <input type="text" value="" name="last_name" id="last_name"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Email Id">Email Id <sup class="mandatory">*</sup></label>
                                        <input type="text" value="" name="email" id="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Password">Password <sup class="mandatory">*</sup></label>
                                        <input type="password" id="user_password" name="user_password" class="form-control">
                                        <div class="password-meter" style="display:none">
                                            <div class="password-meter-message password-meter-message-too-short">Too short</div>
                                            <div class="password-meter-bg">
                                                <div class="password-meter-bar password-meter-too-short"></div>
                                            </div>
                                        </div>
                                        <span> (Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters) </span> </div>

                                    <div class="form-group">
                                        <label for="Confirm Password">Confirm Password<sup class="mandatory">*</sup></label>
                                        <input type="password" value="" name="confirm_password" id="confirm_password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Gender">Gender<sup class="mandatory">*</sup></label>
                                        <input  class="form-control" id="gender" type="radio" value="1" checked="" name="gender" >
                                        Male
                                        <input id="gender" type="radio" value="2" name="gender">
                                        Female
                                    </div>

                                    <div class="form-group">
                                        <label for="Gender">Choose Admin<sup class="mandatory">*</sup></label>
                                        <select id="role_id" name="role_id" class="form-control">
                                            <option value="">Select Role</option>
                                            
                                            @foreach ($arr_roles as $key => $role) 
                                                @if ($role['id'] != 1) 
                                                    <option value="{!! $role['id'] !!}"> {!! stripslashes($role['role_name']) !!}</option>
                                                @endif
                                            @endforeach    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('/assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('/assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('/assets/Backend/js/admin-manage/add-admin.js')}}"></script>
@stop
