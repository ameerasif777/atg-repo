@extends('Backend.layouts.default')
@section('content')    
<section class="content-header">
    <h1>
        Global Settings Management
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Manage Global Settings</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                        <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_desc">No.</th>
                                    <th class="sorting_asc wid-130" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Parameter Name">Parameter Name</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Parameter Value">Parameter Value</th>
                                    <th  class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Action</th>
                                </tr>
                            </thead>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">                                
                                <?php $i = 1 ?>
                                @if (!empty($global_settings))
                                @foreach ($global_settings as $global_setting) 
                                <tr>
                                    <td width="10%">#{{$i++}}</td>
                                    <td>{{ ucwords (str_replace("_", " ", stripslashes($global_setting->name)))}}</td>
                                    <td>
                                        @if ($global_setting->name == 'date_format')
                                        {{date($global_setting->values[0]->value)}}
                                        @else
                                        {{stripslashes($global_setting->values[0]->value)}}
                                        @endif
                                    </td>
                                    <td class=""><a class="btn btn-info" href="{{url('/')}}/admin/global-settings/edit/{{$global_setting->id}}" title="Edit Global Settings Parameter"> <i class="icon-edit icon-white"></i>Edit</a>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop