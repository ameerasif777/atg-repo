@extends('Backend.layouts.default')
@section('content')    
<section class="content-header">
    <h1>
        Update  Global Settings 
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/global-settings/list"><i class="fa fa-gear"></i> Global Settings</a></li>
        <li class="active">Update Global Setting Parameter</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form role="form" name="frm_edit_global_setting_parameter"  id="frm_edit_global_setting_parameter" action="{{url('/')}}/admin/global-settings/edit" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="global_name_id" id="global_name_id" value="{{ $global_setting_info->values[0]->global_name_id }}" />
                    <input type="hidden" name="edit_id" value="{{ $global_setting_info->id }}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="parametername">Parameter Name</label>
                            <input class="form-control" type="text" dir="ltr" readonly  name="name" id="name" value="{{ ucwords(str_replace("_", " ", $global_setting_info->name)) }}" />
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Parameter Value<sup class="mandatory">*</sup></label>
                                @if ($global_setting_info->name == "date_format")
                                <select name="value" class="form-control" id="value">
                                    <option @if ($global_setting_info->values[0]->value == "Y-m-d") selected="selected" @endif value="Y-m-d">{{ date("Y-m-d") }}</option>
                                    <option @if ($global_setting_info->values[0]->value == "Y/m/d") selected="selected" @endif value="Y/m/d">{{ date("Y/m/d") }}</option>
                                    <option @if ($global_setting_info->values[0]->value == "Y-m-d H:i:s")  selected="selected" @endif value="Y-m-d H:i:s">{{ date("Y-m-d H:i:s")}}</option>
                                    <option @if ($global_setting_info->values[0]->value == "Y/m/d H:i:s") selected="selected" @endif value="Y/m/d H:i:s">{{ date("Y/m/d H:i:s") }}</option>
                                    <option @if ($global_setting_info->values[0]->value == "F j, Y, g:i a") selected="selected" @endif value="F j, Y, g:i a">{{  date("F j, Y, g:i a") }}</option>
                                    <option @if ($global_setting_info->values[0]->value == "m.d.y") selected="selected" @endif value="m.d.y">{{ date("m.d.y") }}</option>
                                </select>
                                @elseif ($global_setting_info->name == "address" || $global_setting_info->name == 'contact_us_message')
                                <textarea name="value" class="form-control" id="value">{{ (($global_setting_info->values[0]->value)) }}</textarea>
                                @else
                                <input type="text" dir="ltr" class="form-control" name="value" id="value" value="{{ (($global_setting_info->values[0]->value)) }}" />
                                @endif
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script src="{{url('/assets/Backend/js/jquery-2.0.2.min.js')}}"></script>
<script type="text/javascript">
        jQuery(document).ready(function(e) {
jQuery("#frm_edit_global_setting_parameter").validate({
errorElement: "div",
        rules: {
        value: {
        required: true
<?php
if ($global_setting_info->name == "site_email") {
    echo ",email:true";
}
if ($global_setting_info->name == "contact_email") {
    echo ",email:true";
}
if ($global_setting_info->name == "per_page_record") {
    echo ",number:true";
}
if ($global_setting_info->name == "phone_no") {
    echo ",number:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/";
}
?>
        }
        },
        messages: {
        value: {
<?php
if ($global_setting_info->name == "site_email") {
    echo 'required:"Please enter an email address."';
    echo ',email:"Please enter a valid email address."';
} elseif ($global_setting_info->name == "contact_email") {
    echo 'required:"Please enter an email address."';
    echo ',email:"Please enter a valid email address."';
} else if ($global_setting_info->name == "site_title") {
    echo 'required:"Please enter a site title."';
} else if ($global_setting_info->name == "per_page_record") {
    echo 'required:"Please enter per page record to display."';
}
if ($global_setting_info->name == "phone_no") {
    echo 'required:"Please enter a number."';
    echo ',number:"please enter only digits."';
}
?>
        }
        }
}
);
        });
</script>
@stop
