
@extends('Backend.layouts.default')
@section('content') 

<section class="content-header">
    <h1>
        Edit Level Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/level/list"><i class="fa fa-gear"></i> Manage Level List</a></li>
        <li class="active">Edit Level Details</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <form name="form_edit_level" id="form_edit_level" method="post" action="{{url('/')}}/admin/level/edit/{!!base64_encode($arr_level[0]->id)!!}">
            <div class="col-md-12">
                <div class="box box-primary">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="min_value">Min Score <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="min_value" id="min_value" placeholder="Min Score" value="{{ $arr_level[0]->min_value }}">                                    
                                    <input type="hidden" class="form-control" name="old_min_value" id="old_min_value" placeholder="Min Score" value="{{ $arr_level[0]->min_value }}">                                    
                                </div>
                                <div class="form-group">
                                    <label for="max_value">Max Score <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="max_value" id="max_value" placeholder="Max Score" value="{{ $arr_level[0]->max_value }}">
                                    <input type="hidden" class="form-control" name="old_max_value" id="old_max_value" placeholder="Max Score" value="{{ $arr_level[0]->max_value }}">
                                </div>
                                <div class="form-group">
                                    <label for="level">Rank <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="level" id="level" placeholder="Rank" value="{{ $arr_level[0]->level }}">
                                    <input type="hidden" class="form-control" name="old_level" id="old_level" placeholder="Rank" value="{{ $arr_level[0]->level }}">
                                </div>
                                 <div class="form-group">
                                    <label for="name">Name <sup class="mandatory">*</sup></label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $arr_level[0]->name }}">
                                    <input type="hidden" class="form-control" name="old_name" id="old_name" placeholder="Name" value="{{ $arr_level[0]->name }}">
                                </div>
                                <div class="box-footer">
                                    <button type="submit" id='btnSubmit' name="btn_submit" class="btn btn-primary" value="Save">Save</button>
                                    <!--<img  src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">-->
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>        
@stop
@section('footer')  
<script type="text/javascript">

    jQuery.validator.addMethod("numbersonly", function(value, element) {
        return this.optional(element) || /^\$?[0-9][0-9\,]*(\.\d{1,2})?$|^\$?[\.]([\d][\d]?)$/.test(value);
    }, "Please enter numbers only.");

    jQuery(document).ready(function() {


        $("#form_edit_level").validate({
            ignore: '',
            errorElement: 'div',
            errorClass: 'text-danger',
            rules: {
                min_value: {
                    required: true,
                    numbersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-min-value",
                        type: "get",
                        data: {
                            type: "edit",
                            old_min_value: jQuery('#old_min_value').val()
                        }
                    },
                },
                max_value: {
                    required: true,
                    numbersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-max-value",
                        type: "get",
                        data: {
                            type: "edit",
                            old_max_value: jQuery('#old_max_value').val()
                        }
                    },
                },
                level: {
                    required: true,
                    numbersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-level",
                        type: "get",
                        data: {
                            type: "edit",
                            old_level: jQuery('#old_level').val()
                        }
                    },
                },
                name: {
                    required: true,
//                    lettersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-name",
                        type: "get",
                        data: {
                            type: "edit",
                            old_name: jQuery('#old_name').val()
                        }
                    },
                }

            },
            messages: {
                min_value: {
                    required: "Please enter minimum score.",
                    remote: "Please do not enter value between privious range."
                },
                max_value: {
                    required: "Please enter maximun score.",
                    remote: "Please do not enter value between privious range."

                },
                level: {
                    required: "Please enter rank.",
                    remote: "Rank already exists."
                },
                name: {
                    required: "Please enter name.",
                    remote: "Name already exists."
                }

            },
            submitHandler: function(form) {
                $("#btnSubmit").show();
                $("#loding_image").show();
                form.submit();
            }

        });
    });
    $('#min_value').on('blur', function() {
        var min_value = parseInt(this.value);
        var max_value = $('#max_value').val();
        if(min_value == ''){
            alert('Please enter min score value');
            this.focus();
        }else if (max_value >= min_value) {
            alert('Min score should be less than max score.');
            $(this).val('');
        }

    })

    $('#max_value').on('blur', function() {
        var max_value = parseInt(this.value);
        var min_value = parseInt($('#min_value').val());
        
        if (min_value >= max_value) {
            alert('Max score should be greater than min score.');
            $(this).val('');
        }
    })
    
    $('#level').on('blur', function() {
        var rank = parseInt(this.value);
        if(rank <= 0){
            alert('Rank should be greater than zero.');
            $(this).val('');
        }
    });
</script>
@stop

