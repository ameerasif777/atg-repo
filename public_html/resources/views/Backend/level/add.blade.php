@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Add New Level
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/level/list"><i class="fa fa-fw fa-user"></i> Manage Level</a></li>
        <li class="active">Add New Level</li>

    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form name="frm_add_level" role="form" id="frm_add_level"  action="{{url('/')}}/admin/level/add" method="POST" >
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="min_value">Min Score <sup class="mandatory">*</sup></label>
                                    <input type="text" value="" id="min_value" name="min_value" placeholder="Min Score" class="form-control">
                                    <input type="hidden" value="" id="old_min_value" name="old_min_value" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="max_value">Max Score<sup class="mandatory">*</sup></label>
                                    <input type="text" value="" name="max_value" id="max_value" placeholder="Max Score" class="form-control">
                                    <input type="hidden" value="" id="old_max_value" name="old_max_value" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="level">Rank <sup class="mandatory">*</sup></label>
                                    <input type="text" value="" name="level" id="level" placeholder="Rank" class="form-control">
                                    <input type="hidden" value="" name="old_level" id="old_level"  class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="name">Name <sup class="mandatory">*</sup></label>
                                    <input type="text" value="" name="name" id="name" placeholder="Name" class="form-control">
                                    <input type="hidden" value="" name="old_name" id="old_name"  class="form-control">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" id="btnSubmit" name="btn_submit" class="btn btn-primary" value="Save" >Save</button>
<!--                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script type="text/javascript">
    jQuery(document).ready(function() {

        jQuery.validator.addMethod("numbersonly", function(value, element) {
            return this.optional(element) || /^\$?[0-9][0-9\,]*(\.\d{1,2})?$|^\$?[\.]([\d][\d]?)$/.test(value);
        }, "Please enter numbers only.");

//        $("frm_add_level").val('');

        $("#frm_add_level").validate({
            ignore: '',
            errorElement: 'div',
            errorClass: 'text-danger',
            rules: {
                min_value: {
                    required: true,
                    numbersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-min-value",
                        type: "get",
                    }
                },
                max_value: {
                    required: true,
                    numbersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-max-value",
                        type: "get",
                    }
                },
                level: {
                    required: true,
                    numbersonly: true,  
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-level",
                        type: "get",
                    }
                },
                name: {
                    required: true,
//                    lettersonly: true,
                    remote: {
                        url: jQuery("#base_url").val() + "/admin/level/add-check-name",
                        type: "get",
                    }
                }
            },
            messages: {
                min_value: {
                    required: "Please enter minimum score.",
                    remote: "Please do not enter value between privious range."
                },
                max_value: {
                    required: "Please enter maximun score.",
                    remote: "Please do not enter value between privious range."
                },
                level: {
                    required: "Please enter rank.",
                    remote: "Rank already exists."
                },
                name: {
                    required: "Please enter name.",
                    remote: "Name already exists."
                }

            },
            submitHandler: function(form) {
                $("#btnSubmit").hide();
                $("#loding_image").show();
                form.submit();
            }

        });
    });

    $('#min_value').on('blur', function() {
        var min_value = parseInt(this.value);
        var max_value = $('#max_value').val();
        if(min_value == ''){
            alert('Please enter min score value');
            $(this).focus();
        }else if (max_value >= min_value) {
            alert('Min score should be less than max score.');
            $(this).val('');
        }

    })

    $('#max_value').on('blur', function() {
        var max_value = parseInt($(this).val());
        var min_value = parseInt($('#min_value').val());
        if (min_value >= max_value) {
            alert('Max score should be greater than min score.');
            $(this).val('');
        }
    })
    
    $('#level').on('blur', function() {
        var rank = this.value;
        if(rank <= 0){
            alert('Rank should be greater than zero.');
            $(this).val('');
        }
    });

    function isvalidated(input, min, max) {
        if (!$.isNumeric(input)) {
            alert("Please enter numbers");
            return false;
        } else {
            var message = (input <= max) ? (input >= min ? "input is valid" : "input must be greater than " + min) : "input must be smaller than " + max;
            alert(message);
        }
    }
</script>
@stop
