
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    @if(Session::has('success_msg'))
    <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                
                <form name="frm_meetup" role="form" id="frm_meetup" action="{{url('/')}}/admin/level/delete" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>
                                    Select <br>
                                    @if (count($arr_level) > 1)
                                    <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                    @endif
                                </center></th>
                                
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title">Min Score</th>                                
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Max Score</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Rank</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Name</th>
                                <!--<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Posted by">Posted By</th>-->
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Created on</th>
                                <!--<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Status</th>-->                                
                                <th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Action">Action</th>
                                
                                </tr>
                                </thead>

                                <tbody>
                                    @if(isset($arr_level))
                                    @foreach($arr_level as $level) 
                                    <tr>
                                 <td >
                                <center>
                                    <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $level->id !!}" />
                                </center>
                                </td>                                
                                <td width="10%">{{ $level->min_value }}</td>
                                <td width="10%">{{ $level->max_value }}</td>
                                <td width="10%">{{ $level->level }}</td>
                                <td width="10%">{{ $level->name }}</td>
                                
                                
                                <!--<td>{!! $level->created_at !!}</td>-->
                                <td>{!! $level->updated_at !!}</td>
              
                                    
                                <td class="">
                                    <a class="btn btn-info" title="Edit Question Details" href="{{url('/')}}/admin/level/edit/{!! base64_encode($level->id)!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                    <!--<a class="btn btn-info" title="View Question Details" href="{{url('/')}}/admin/question/view/{!! base64_encode($level->id); !!}"> <i class="icon-edit icon-white"></i>View</a>-->
                                </td>
                                
                                @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <th colspan="9">
                                     
                                    <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">                                    
                                
                                    <a id="add_new_admin" name="add_new_level" href="{{url('/')}}/admin/level/add" class="btn btn-primary pull-right" >Add New Level </a>
                                </th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop
