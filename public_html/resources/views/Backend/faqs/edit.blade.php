@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Update FAQ
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/faqs/list"><i class="fa fa-fw fa-user"></i> Manage FAQs</a></li>
        <li class="active">
            Update FAQ
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <form name="frm_faq_details" id="frm_faq_details" role="form"  action="{{url('/')}}/admin/faqs/edit/{!! $edit_id !!}" method="POST" >
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Question">Question <sup class="mandatory">*</sup></label>
                            <input type="text" value="{!! $arr_faq_detail['question'] !!}" id="question" name="question" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="Answer">Answer <sup class="mandatory">*</sup></label>
                            <textarea name="answer" id="answer"  class="form-control">{!! $arr_faq_detail['answer'] !!}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                        <input type="hidden" name="edit_id" id="edit_id" value="{!! intval(base64_decode($edit_id)) !!}" />
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script>
     $(document).ready(function() {
                jQuery("#frm_faq_details").validate({
                    errorElement: 'label',
                    rules: {
                        question: {
                            required: true,
                            minlength: 3
                        },
                        answer: {
                            required: true,
                            minlength: 25
                        },
                        lang_id: {
                            required: true
                        },
                    },
                    messages: {
                        lang_id: {
                            required: "Please select language"
                        },
                        question: {
                            required: "Please enter question.",
                            minlength: "Please enter at least 3 characters."
                        },
                        answer: {
                            required: "Please enter answer.",
                            minlength: "Please enter at least 25 characters."
                        }
                    },
                    submitHandler: function(form) {
                        $("#btnSubmit").hide();
                        $('#loding_image').show();
                        form.submit();
                    }
                });
            });
</script>
@stop
