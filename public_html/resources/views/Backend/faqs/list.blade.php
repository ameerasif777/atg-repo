@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        FAQs Management
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-phone"></i> Manage FAQs</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                <form name="frm_faqs" role="form" id="frm_faqs" action="{{url('/')}}/admin/faqs/list" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>

                                    @if (count($arr_faqs) > 0)
                                    <center>
                                        Select <br>
                                        <input type="checkbox" name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                    </center>
                                    @endif
                                </center>
                                </th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Sender Name">Question</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Sender Email">Answer</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Status">Status</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Date">Date</th>
                                <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($arr_faqs as $list)
                                    <tr>
                                        <td ><center>
                                    <input value="{!! $list['id'] !!}" class="case" name="checkbox[]" id="checkbox[]" type="checkbox">
                                </center></td>
                                <td >{!! stripcslashes($list['question'])!!}</td>
                                <td >{!! $list['answer'] !!}</td>
                                <td>
                                    <div id="blocked_div{!! $list['id']!!}" @if ($list['status'] == 'Inactive') style="display:inline-block" @else  style="display:none;" @endif >
                                        <a class="label label-danger" title="Click to Change Status" onClick="changeStatus({!! $list['id']!!}, 'Active');" href="javascript:void(0);" id="status_{!! $list['id']!!}">Inactive</a>
                                    </div>
                                    <div id="active_div{!! $list['id']!!}"  @if ($list['status'] == 'Active')  style="display:inline-block" @else style="display:none;" @endif >
                                        <a class="label label-success" title="Click to Change Status" onClick="changeStatus({!! $list['id']!!}, 'Inactive');" href="javascript:void(0);" id="status_{!! $list['id']!!}">Active</a>
                                    </div>
                                </td>        
                                <td > {!! date($global_values['date_format'], strtotime($list['created_at']))!!}</td>

                                <td class=""><a title="Edit" class="btn btn-info" href="{{url('/')}}/admin/faqs/edit/{!! base64_encode($list['id']) !!}"> <i class="icon-edit icon-white"></i>Edit</a></td>
                                @endforeach
                                </tbody>
                                <tfoot>

                                <th colspan="7">
                                    @if (count($arr_faqs) > 0)
                                    <input type="submit" id="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"   value="Delete Selected">
                                    @endif
                                    <a id="add_new_admin" name="add_new_admin" href="{{url('/')}}/admin/faqs/add" class="btn btn-primary pull-right" >Add New Faq </a>
                                </th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop
<script>
    function changeStatus(id, status)
    {
        /* changing the user status*/
        var obj_params = new Object();
        obj_params.id = id;
        obj_params.status = status;
        jQuery.get("{{url('/')}}/admin/faqs/change-status", obj_params, function (msg) {
            if (msg.error == "1")
            {
                alert(msg.error_message);
            }
            else
            {   
                /* toogling the bloked and active div of user*/
                if (status == 'Inactive')
                {   
                    $("#blocked_div" + id).css('display', 'inline-block');
                    $("#active_div" + id).css('display', 'none');
                }
                else
                {
                    $("#active_div" + id).css('display', 'inline-block');
                    $("#blocked_div" + id).css('display', 'none');
                }
            }

        }, "json");

    }
</script>

