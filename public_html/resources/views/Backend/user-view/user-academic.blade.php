@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
	<h1>
		{{$site_title}}
	</h1>

	@if($user->user_type=='3')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/student"><i class="fa fa-fw fa-user"></i> Manage Student</a></li>
		<li class="active">
			View Student Details
		</li>
	</ol>
	@elseif($user->user_type=='4')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/professor"><i class="fa fa-fw fa-user"></i> Manage Professor</a></li>
		<li class="active">
			View Professor Details
		</li>
	</ol>
	@elseif($user->user_type=='5')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/counselor"><i class="fa fa-fw fa-user"></i> Manage Counselor</a></li>
		<li class="active">
			View Counselor Details
		</li>
	</ol>
	@elseif($user->user_type=='6')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/general_user"><i class="fa fa-fw fa-user"></i> Manage General User</a></li>
		<li class="active">
			View General User Details
		</li>
	</ol>
	@endif
</section>
<section class="content">
	@if(Session::has('message'))
	<p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
	@endif


	<div class="row">
		<div class="col-xs-12">
			@if(count($arr_academics_details)>0)		
			<div class="col-xs-6">
				@foreach($arr_academics_details as $academics)
				<p class="lead">{{$academics->institute_name}}</p>

				<div class="table-responsive">
					<table class="table">
						<tbody>

							<tr>
								<th style="width:50%">Status :</th>
								<td>
									@if($academics->status=='1')Current Student @else Alumni @endif
								</td>
							</tr>
							<tr>
								<th style="width:50%">Duration :</th>
								<td>
									From: {{$academics->duration_from}} to  {{$academics->duration_to}}
								</td>
							</tr>
							<tr>
								<th style="width:50%"> Degree :</th>
								<td>
									{{$academics->degree_name}}
								</td>
							</tr>
							<tr>
								<th style="width:50%"> Course :</th>
								<td>
									{{$academics->course_name}}
								</td>
							</tr>
							<tr>
								<th style="width:50%"> Grade :</th>
								<td>
									{{$academics->grade}}
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				@endforeach
			</div>
			@else
			<p class="lead">No Record Added.</p>
			@endif
		</div>
	</div>
</section>

@stop
