@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
	<h1>
		{{$site_title}}
	</h1>

	@if($arr_user_detail['user_type']=='3')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/student"><i class="fa fa-fw fa-user"></i> Manage Student</a></li>
		<li class="active">
			View Student Details
		</li>
	</ol>
	
	@elseif($arr_user_detail['user_type']=='4')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/professor"><i class="fa fa-fw fa-user"></i> Manage Professor</a></li>
		<li class="active">
			View Professor Details
		</li>
	</ol>
	@elseif($arr_user_detail['user_type']=='5')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/counselor"><i class="fa fa-fw fa-user"></i> Manage Counselor</a></li>
		<li class="active">
			View Counselor Details
		</li>
	</ol>
	@elseif($arr_user_detail['user_type']=='6')
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{url('/')}}/admin/users/list/general_user"><i class="fa fa-fw fa-user"></i> Manage General User</a></li>
		<li class="active">
			View General User Details
		</li>
	</ol>
	@endif
</section>
<section class="content">
	@if(Session::has('message'))
	<p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
	@endif


	<div class="row">
		<div class="col-xs-12">
			<div class="col-xs-6">
				<p class="lead"> User Personal Details</p>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<th style="width:50%">User Name :</th>
								<td>
									{{ucwords($arr_user_detail['first_name'].' '.$arr_user_detail['last_name'])}}
								</td>
							</tr>
							<tr>
								<th>User Type :</th>
								<td> 
									@if($arr_user_detail['user_type']=='3')
									Student
									@elseif($arr_user_detail['user_type']=='4')Professor
									@elseif($arr_user_detail['user_type']=='5')Counselor
									@elseif($arr_user_detail['user_type']=='6')General User
									@endif
								</td>
							</tr>
							<tr>
								<th>Email ID : </th>
								<td> 
									{{$arr_user_detail['email']}}
								</td>
							</tr>
							<tr>
								<th>SynCampus ID: </th>
								<td> 
									{{$arr_user_detail['syncampus_id']}}
								</td>
							</tr>
							<tr>
								<th>DOB:</th>
								<td> 
									 {{$arr_user_detail['user_birth_date']}}
								</td>
							</tr>
							<tr>
								<th>Contact No :</th>
								<td> 
									@if(isset($arr_user_detail['contact_no'])) {{$arr_user_detail['contact_no']}}	@else ---	@endif
								</td>
							</tr>



							<tr>
								<th>Syncampus Id  :</th>
								<td> 
									@if ($arr_user_detail['syncampus_id'] != '') 
									{{ $arr_user_detail['syncampus_id']}}
									@else
									{{ "---"}}
									@endif
								</td>
							</tr>

							<tr>
								<th>Current Status :</th>
								<td> 
									 @if($arr_user_detail['current_status']=='1')Studying In {{$institute_name}}	@else 
						Working In {{$arr_user_detail['company']}}	
						@endif 
								</td>
							</tr>
							<tr>
								<th>Current Place:  </th>
								<td> 
									{{ucwords($arr_user_detail['current_place'])}}
								</td>
							</tr>
							<tr>
								<th>Home Town : </th>
								<td>
									{{ucwords($arr_user_detail['home_town'])}}
								</td>
							</tr>
						</tbody>
					</table>
					@if(count($social_data)>0)
					<p class="lead"> Social Links </p>
					<div class="table-responsive">
					<table class="table">
						<tbody>
							@foreach($social_data as $social)
							<tr>
								<th>{{$social->social_site_name}}</th>
								<td>{{$social->social_link}}</td>
							</tr>
							
							@endforeach
						</tbody>
					</table>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>

@stop
