@extends('Backend.layouts.default')
@section('content')
<style>
    .form-group .control-label{
        text-align: left;
    }    
</style>
<section class="content-header">
    <h1>
    Websites We are crawling
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> Websites we are crawling</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    @if(Session::has('success_msg'))
    <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="row" style="padding: 10px">
                <div class="col-md-2">
                    <select id="recordPerPage" name="recordPerPage" class="form-control" onchange="doSearch()">
                        <option value="25" {{ ($recordPerPage == 25) ? 'selected' : '' }}>25</option>
                        <option value="50" {{ ($recordPerPage == 50) ? 'selected' : '' }}>50</option>
                        <option value="100" {{ ($recordPerPage == 100) ? 'selected' : '' }}>100</option>
                        <option value="200" {{ ($recordPerPage == 200) ? 'selected' : '' }}>200</option>
                        <option value="500" {{ ($recordPerPage == 500) ? 'selected' : '' }}>500</option>
                    </select>
                </div>
                <div class="col-md-2">records per page</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">&nbsp;</div>
                        <div class="col-md-1">Search</div>
                        <div class="col-md-4">
                            <input id="search-qry" name="search-qry" value="{{ $sqry }}" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <input type="button" id="search_btn" name="search_btn" class="btn btn-info" onclick="doSearch()" value="Go">
                        </div>
                    </div>
                </div>
            </div>
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                        <table class="table table-bordered table-striped"  aria-describedby="example1_info" style="width:100%;">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="">Website</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Website URL</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Regex</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Feature</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">API / Scrape</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">API Regex</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">API URL</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($get_website_list))
                                    @foreach ($get_website_list as $value)
                                        <tr>
                                            <td>
                                                {!! $value->website_name !!}
                                            </td>
                                            <td>{!! $value->website_address !!}</td>
                                            <td>{!! $value->regex !!}</td>
                                            <td>{!! $value->feature !!}</td>
                                            <td> @if ( $value->API_available  == 1) Yes @else No @endif / Yes </td>
                                            <td style="word-wrap: break-word; word-break: break-all;" >{!! $value->api_regex !!}</td>
                                            <td style="word-wrap: break-word; word-break: break-all;">{!! $value->api_url !!}</td>
                                            <td>
                                                <input type="hidden" id="inp_{{ $value->id }}" value="{{json_encode($value)}}"/>
                                                <a id="edit_{{ $value->id }}"  name="edit_btn" href="#" class="btn btn-primary pull-right" >Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <th colspan="8">
                                    <a id="add_new_website" name="add_new_website" href="#add_website_div" class="btn btn-primary pull-right" >Add New Website </a>
                                </th>
                            </tfoot>
                        </table>
                    </div>
                </div>
                {{ $get_website_list->appends(['search-qry' => $sqry, 'rpp' => $recordPerPage])->links() }}
            </div>
        </div>
    </div>
    
    <div class="row" id="add_website_div" style="display:none">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper" id="tab1_wrapper">
                        <h3 id="add_title">Add New Website</h3>   
                        <h3 id="edit_title" style="display:none">Edit Website</h3>   
                        <form id="add_website">
                            <div class="form-group">
                              <label for="website">Website:</label>
                              <input id="website" name="website" value=""  class="form-control" placeholder="Website" required/>
                              <input id="website_id" name="website_id" value="" type="hidden"/>
                            </div>
                            <div class="form-group">
                              <label for="website_url">Website URL:</label>
                              <input id="website_url" name="website_url" value=""  class="form-control" placeholder="Website URL" required />
                            </div>
                            <div class="form-group">
                              <label for="regex">Regex:</label>
                              <input id="regex" name="regex" value=""  class="form-control" placeholder="Regex"  required/>
                            </div>
                            <div class="form-group">
                              <label for="feature">Feature:</label>
                              <select id="feature" name="feature" value=""  class="form-control" placeholder="Feature" required>
                                  <option value="" disabled selected>Select</option>
                                  <option value="meetup">Meetup</option>
                                  <option value="event">Event</option>
                                  <option value="article">Article</option>
                                  <option value="education" disabled>Education</option>
                                  <option value="question" disabled>Qrious</option>
                                  <option value="job">Job</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="selected">Group:</label>
                              <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();"></ul>
                              <div class="text-danger" for="sub_groups[]" generated="true"></div>
                              @include('Backend.includes.add-search-group-field', ['arr_selected_groups' => '', 'id' => '', 'key' => '0' ])
                            </div>
                            <div class="form-group">
                              <label for="website_url">API/Scrape:</label>
                              <label class="checkbox-inline"><input type="radio" id="api" name="apiorscrape" value="api" >  API</label>
                              <label class="checkbox-inline"><input type="radio" id="scrape" name="apiorscrape" value="scrape"   >  Scrape</label>
                            </div>
                            <div class="form-group">
                              <label for="api_regex">API Regex:</label>
                              <input id="api_regex" name="api_regex" value=""  class="form-control" placeholder="API Regex" disabled required/>
                            </div>
                            <div class="form-group">
                              <label for="api_url">API URL:</label>
                              <input id="api_url" name="api_url" value=""  class="form-control" placeholder="API URL" disabled required/>
                            </div>
                            <div class="form-group">
                              <label for="req_method">Request Method:</label>
                              <select id="req_method" name="req_method" value=""   disabled required>
                                  <option value="get" selected>GET</option>
                                  <option value="post" >POST</option>
                              </select>
                            </div>
                            <button type="submit" class="btn btn-default" id="tab1_next">Next</button>
                        </form>
                        
                    </div>
                    
                    <div role="grid" class="dataTables_wrapper " id="tab2_wrapper" style="display:none;">
                        <h3 id="api_title" style="display:none;">Add Test Url For API Scraping</h3>
                        <h3 id="scrape_title" style="display:none;">Add Field Map For Fallback Scraping</h3>
                        <form id="test_url_form" class="form-inline">
                        <div id="add_test_url" class="form-inline">
                            <input id="test_url" name="test_url" value=""  class="form-control" placeholder="Add Test URL" style="width:95%"/>
                            &nbsp;<button type="submit" id="test_btn" class="btn btn-default fa fa-arrow-down " ></button>
                            </div> 
                        </form>
                        <br>
                            <div id="keyword_div"  class="form-inline" style="display:none;">
                                <label for="">Keywords:</label>
                                <textarea id="keywords" name="" value="" maxlength="200" rows="6"  class="form-control"  disabled></textarea>           
                            </div>
                    <br>
                            <form id="meetup-form"  class="form-horizontal"  style="display:none;">                               
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="title">Title:</label>
                                    <div class="col-sm-5">
                                        <input id="title" name="title" value=""  class="form-control" placeholder="Title"  required/>
                                    </div>
                                    <div class="col-sm-5">
                                        <span  id="title2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="description">Description:</label>
                                  <div class="col-sm-5">
                                    <input id="description" name="description" value=""  class="form-control" placeholder="Description"  required/>
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="description2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="venue">Venue:</label>
                                  <div class="col-sm-5">
                                  <input id="venue" name="venue" value=""  class="form-control" placeholder="Venue"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="venue2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="contact_name">Contact Name:</label>
                                  <div class="col-sm-5">
                                  <input id="contact_name" name="contact_name" value=""  class="form-control" placeholder="Contact Name"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="contact_name2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="contact_num">Contact Number:</label>
                                  <div class="col-sm-5">
                                  <input id="contact_num" name="contact_num" value=""  class="form-control" placeholder="Contact Number"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="contact_num2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="email_addr">Email Address:</label>
                                  <div class="col-sm-5">
                                  <input id="email_addr" name="email_addr" value=""  class="form-control" placeholder="Email Address"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="email_addr2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="website">Website:</label>
                                  <div class="col-sm-5">
                                  <input id="websiteurl" name="websiteurl" value=""  class="form-control" placeholder="Website"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="websiteurl2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="latitude">Latitude:</label>
                                  <div class="col-sm-5">
                                  <input id="latitude" name="latitude" value=""  class="form-control" placeholder="Latitude"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="latitude2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="longitude">Longitude:</label>
                                  <div class="col-sm-5">
                                  <input id="longitude" name="longitude" value=""  class="form-control" placeholder="Longitude"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="longitude2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="location">Location:</label>
                                  <div class="col-sm-5">
                                  <input id="location" name="location" value=""  class="form-control" placeholder="Location"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="location2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="start_date">Start Date:</label>
                                  <div class="col-sm-5">
                                  <input id="start_date" name="start_date" value=""  class="form-control" placeholder="Start Date"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="start_date2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="start_time">Start Time:</label>
                                  <div class="col-sm-5">
                                  <input id="start_time" name="start_time" value=""  class="form-control" placeholder="Start Time"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="start_time2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="end_date">End Date:</label>
                                  <div class="col-sm-5">
                                  <input id="end_date" name="end_date" value=""  class="form-control" placeholder="End Date"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="end_date2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="end_time">End Time:</label>
                                  <div class="col-sm-5">
                                  <input id="end_time" name="end_time" value=""  class="form-control" placeholder="End Time"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="end_time2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="end_time">Cost:</label>
                                  <div class="col-sm-5">
                                  <input id="cost" name="cost" value=""  class="form-control" placeholder="Cost"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="cost2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="currency">Currency:</label>
                                  <div class="col-sm-5">
                                  <input id="currency" name="currency" value=""  class="form-control" placeholder="Currency"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="currency2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="currency">Tag:</label>
                                  <div class="col-sm-5">
                                  <input id="tag" name="tag" value=""  class="form-control" placeholder="Tag"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="tag2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="profile_image">Profile Image:</label>
                                  <div class="col-sm-5">
                                  <input id="profile_image" name="profile_image" value=""  class="form-control" placeholder="Profile Image"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="profile_image2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="term_condition">Terms & Conditions:</label>
                                  <div class="col-sm-5">
                                  <input id="term_condition" name="term_condition" value=""  class="form-control" placeholder="Terms & Conditions"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="term_condition2" ></span>
                                    </div>
                                </div>
                            <button id="back_btn" type="button" class="btn btn-default back_btn">Back</button>
                            <button type="submit" id="save_btn" class="btn btn-default save_btn" style="float:right" disabled>Save</button>
                        </form>
                            <form id="job-form"  class="form-horizontal"  style="display:none;">
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="title">Title:</label>
                                  <div class="col-sm-5">
                                  <input id="job_title" name="job_title" value=""  class="form-control" placeholder="Title" required />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_title2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="cmp_name">Company Name:</label>
                                  <div class="col-sm-5">
                                  <input id="cmp_name" name="cmp_name" value=""  class="form-control" placeholder="Company Name"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="cmp_name2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="description">Description:</label>
                                  <div class="col-sm-5">
                                    <input id="job_description" name="job_description" value=""  class="form-control" placeholder="Description"  required/>
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_description2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_loc">Job Location:</label>
                                  <div class="col-sm-5">
                                  <input id="job_loc" name="job_loc" value=""  class="form-control" placeholder="Job Location"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_loc2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_longitude">Longitude:</label>
                                  <div class="col-sm-5">
                                  <input id="job_longitude" name="job_longitude" value=""  class="form-control" placeholder="Longitude"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_longitude2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_latitude">Latitude:</label>
                                  <div class="col-sm-5">
                                  <input id="job_latitude" name="job_latitude" value=""  class="form-control" placeholder="Latitude"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_latitude2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_phone_num">Phone Number:</label>
                                  <div class="col-sm-5">
                                  <input id="job_phone_num" name="job_phone_num" value=""  class="form-control" placeholder="Phone Number"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_phone_num2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_email_addr">Email Address:</label>
                                  <div class="col-sm-5">
                                  <input id="job_email_addr" name="job_email_addr" value=""  class="form-control" placeholder="Email Address"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_email_addr2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_url">Url:</label>
                                  <div class="col-sm-5">
                                  <input id="job_url" name="job_url" value=""  class="form-control" placeholder="Url"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_url2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="tags">Tags:</label>
                                  <div class="col-sm-5">
                                  <input id="tags" name="tags" value=""  class="form-control" placeholder="Tags"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="tags2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="pref_qual">Preferred Qualification:</label>
                                  <div class="col-sm-5">
                                  <input id="pref_qual" name="pref_qual" value=""  class="form-control" placeholder="Preferred Qualification"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="pref_qual2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="skill">Skill:</label>
                                  <div class="col-sm-5">
                                  <input id="skill" name="skill" value=""  class="form-control" placeholder="Skill"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="skill2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="website">Website:</label>
                                  <div class="col-sm-5">
                                  <input id="jobwebsiteurl" name="jobwebsiteurl" value=""  class="form-control" placeholder="Website"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="jobwebsiteurl2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="how_to_apply">How To Apply:</label>
                                  <div class="col-sm-5">
                                  <input id="how_to_apply" name="how_to_apply" value=""  class="form-control" placeholder="How To Apply"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="how_to_apply2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="appl_deadline">Application Deadline:</label>
                                  <div class="col-sm-5">
                                  <input id="appl_deadline" name="appl_deadline" value=""  class="form-control" placeholder="Application Deadline"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="appl_deadline2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="job_profile_image">Profile Image:</label>
                                  <div class="col-sm-5">
                                  <input id="job_profile_image" name="job_profile_image" value=""  class="form-control" placeholder="Profile Image"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="job_profile_image2" ></span>
                                    </div>
                                </div>
                            <button id="back_btn" type="button" class="btn btn-default back_btn">Back</button>
                            <button type="submit" id="save_btn" class="btn btn-default save_btn" style="float:right" disabled>Save</button>
                        </form>
                        <form id="article-form"  class="form-horizontal"  style="display:none;">
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="article_title">Title:</label>
                                  <div class="col-sm-5">
                                  <input id="article_title" name="article_title" value=""  class="form-control" placeholder="Title" required />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="article_title2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="article_descr">Description:</label>
                                  <div class="col-sm-5">
                                  <input id="article_description" name="article_description" value=""  class="form-control" placeholder="Description"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="article_description2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="article_tags">Tags:</label>
                                  <div class="col-sm-5">
                                  <input id="article_tags" name="article_tags" value=""  class="form-control" placeholder="Tags"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="article_tags2" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-2" for="article_profile_image">Profile Image:</label>
                                  <div class="col-sm-5">
                                  <input id="article_profile_image" name="article_profile_image" value=""  class="form-control" placeholder="Profile Image"  />
                                  </div>
                                  <div class="col-sm-5">
                                        <span  id="article_profile_image2" ></span>
                                    </div>
                                </div>
                            <button id="back_btn" type="button" class="btn btn-default back_btn">Back</button>
                            <button type="submit" id="save_btn" class="btn btn-default save_btn" style="float:right" disabled>Save</button>
                        </form>     
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
    
    
</section>
<script>
$(document).ready(function()
{
    var keys = [];
    var ajax_msg = '';
    var api_flag = '';
    
    $('#add_website').submit(function(event){
        if(!this.checkValidity())
        {
            event.preventDefault();
        }
        else
        {
            
            $('#tab2_wrapper').show("slide", { direction: "right" }, 200);
            $('#tab1_wrapper').hide();
            api_flag = $('#api').iCheck('update')[0].checked;
            if(api_flag == true)
            {               
                $('#api_title').css('display', 'block');
                $('#scrape_title').css('display', 'none'); 
                $('#add_test_url').css('display', 'block');
                $('#keyword_div').css('display', 'none');
                $("[id$=-form]").css('display', 'none');
            }
            else
            {              
                $('#api_title').css('display', 'none');
                $('#scrape_title').css('display', 'block');
                $('#add_test_url').css('display', 'block');
                $('#keyword_div').css('display', 'none');
                $('.save_btn').removeAttr('disabled');
                //$('#meetup-form,#job-form').css('display', 'none');
                if($('#feature').val() == 'meetup' || $('#feature').val() == 'event')
                {                   
                    $("[id$=-form]").css('display', 'none');
                    $('#meetup-form').css('display', 'block');
                    //$('#job-form').css('display', 'none');
                }
                else if($('#feature').val() == 'job')
                {
                    //$('#meetup-form').css('display', 'none');
                    $("[id$=-form]").css('display', 'none');
                    $('#job-form').css('display', 'block');
                }
                else if($('#feature').val() == 'article')
                {
                    //$('#meetup-form').css('display', 'none');
                    $("[id$=-form]").css('display', 'none');
                    $('#article-form').css('display', 'block');
                }
            }
            event.preventDefault();
        }
    });

    $('[id$=-form]').submit(function(event){
        if(!this.checkValidity())
        {
            event.preventDefault();
        }
        else
        {           
            event.preventDefault();
            var dataString = '';
            dataString = $("#add_website, #" + event.target.id).serialize();
//            if(event.target.id == 'meetup-form')
//            {
//                dataString = $("#add_website, #meetup-form").serialize();
//            }
//            else if(event.target.id == 'job-form')
//            {
//                dataString = $("#add_website, #job-form").serialize();
//            }
            
            $('.save_btn').attr('disabled', true);
            $.ajax({
                url: '{{url("/")}}/admin/website_crawl/save_new_website',
                method: 'post',
                data: dataString,
                success: function (res) {
                    if(res != '0')
                    {
                        $('.save_btn').attr('disabled', false);
                        window.location.reload();
                    }
                }
            });
        }
    });
    
    $('#add_new_website').click(function(event){
        $('#add_title').css('display', 'block');
        $('#edit_title').css('display', 'none');
        $('#test_url').val('');
        $("form span").html('');
        $('form').each(function() { this.reset() });
        $('#website_id ').val('');
        $('#user_group').val('');
        $('#add_website_div').show('medium');
    });
    
    $('.back_btn').click(function(event){
        $('#tab1_wrapper').show("slide", { direction: "left" }, 200);
        $('#tab2_wrapper').hide();
    });
    
    $('input[name="apiorscrape"').on('ifChecked', function(event){
        if(event.target.value == 'api')
        {
            $('#api_regex').removeAttr('disabled');
            $('#api_url').removeAttr('disabled');
            $('#req_method').removeAttr('disabled');
        }
        else
        {
            $('#api_regex').attr('disabled', true);
            $('#api_url').attr('disabled', true);
            $('#req_method').attr('disabled', true);
        }
    });
    
    
    
    
    $('#test_btn').click(function(event){
        callTestUrl();
    });
    
    $('#test_url_form').submit(function(event){
        callTestUrl();
    });
    
    
    $('[id$=-form] input').change(function() {
       var inp_val = $(this).val();
       var id = $(this).attr('id');
       var res_label = $('#'+id+'2');
       var label_text = '';
       
       if(api_flag == true)
       {
            var words = inp_val.split(' ');
            for(var i=0; i<words.length; i++)
            {
                var x = keys.indexOf(words[i]);
                if(x != -1)
                {
                    var regex = /\|\|(.*?)\|\|/g;
                    var z = regex.exec(words[i]);
                    var y = z[1].split('-');
                    if(y.length>1)
                    {
                        var data = ajax_msg[y[0]];
                        for(var j=1; j<y.length; j++)
                        {
                            data = data[y[j]];
                        }
                        label_text = label_text +  data + ' ';
                    }
                    else
                    {
                        label_text = label_text + ajax_msg[y] + ' ';
                    }
                }
                else
                {
                    label_text = label_text + words[i] + ' ';
                }
            }
            res_label.html(label_text);
            //res_label.html(label_text.substring(0, 100) + ' ...');
        }
        else
        {                        
            if($('#test_url').val() != '' && inp_val != '')
            {               
                callAjaxScraping(id, api_flag);
            }
            else
            {
                res_label.html('');
            }
        }
    });
    
    
    $('a[name="edit_btn"').click(function(event){
        var id = event.target.id.split('_')[1];
        var json_data = $('#inp_'+id).val();
        var data = JSON.parse(json_data);
        $('form').each(function() { this.reset() });
        $('#test_url').val('');
        $("form span").html('');
        $('#website_id').val(id);
        $('#website').val(data['website_name']);
        $('#website_url').val(data['website_address']);
        $('#regex').val(data['regex']);
        $('#feature').val(data['feature']);  
        $("[id$=-form]").css('display', 'none');

        group = [];
        $('#selected').html('');
        var groups = data['groups'];
        groups_json = JSON.stringify(groups);
        
        initializeTagsInput();
        $('#group_inp').tagsinput('removeAll');
        addSuggestedGroup(groups_json, '');   
        
        if(data['API_available'] == '1')
        {
            $('#api').attr('checked', true);
            $('#api').iCheck('update');
            $('#api').trigger("ifChecked");
        }
        else
        {
            $('#scrape').attr('checked', true);
            $('#scrape').iCheck('update');
            $('#scrape').trigger("ifChecked");
        }
        $('#api_regex').val(data['api_regex']);
        $('#api_url').val(data['api_url']);
        $('#req_method').val(data['req_method']);
        $('#add_website_div').show('medium');
        $('html, body').animate({ scrollTop: $('#add_website_div').offset().top }, 'medium');
        $('#add_title').css('display', 'none');
        $('#edit_title').css('display', 'block');
        
        var map_json = JSON.parse(data['map_json']);
        if(data['feature'] == 'meetup' || data['feature'] == 'event')
        {
            $('#title').val(map_json['title']);
            $('#venue').val(map_json['venue']);
            $('#start_date').val(map_json['start_date']);
            $('#start_time').val(map_json['start_time']);
            $('#end_date').val(map_json['end_date']);
            $('#end_time').val(map_json['end_time']);
            $('#latitude').val(map_json['latitude']);
            $('#longitude').val(map_json['longitude']);
            $('#location').val(map_json['location']);
            $('#contact_name').val(map_json['contact_name']);
            $('#description').val(map_json['description']);
            $('#profile_image').val(map_json['profile_image']);
            $('#email_addr').val(map_json['email_address']);
            $('#contact_num').val(map_json['contact_number']);
            $('#cost').val(map_json['cost']);
            $('#currency').val(map_json['currency']);
            $('#tag').val(map_json['tag']);
            $('#websiteurl').val(map_json['website']);
            $('#term_condition').val(map_json['terms_condition']);
            $('#meetup-form').css('display', 'block');
            
        }
        else if(data['feature'] == 'job')
        {
            $('#job_title').val(map_json['title']);
            $('#job_description').val(map_json['description']);
            $('#cmp_name').val(map_json['cmp_name']);
            $('#job_loc').val(map_json['job_location']);
            $('#tags').val(map_json['tags']);
            $('#pref_qual').val(map_json['preferred_qualification']);
            $('#skill').val(map_json['skill']);
            $('#jobwebsiteurl').val(map_json['website']);
            $('#how_to_apply').val(map_json['how_to_apply']);
            $('#appl_deadline').val(map_json['application_deadline']);
            $('#job_phone_num').val(map_json['phone_number']);
            $('#job_email_addr').val(map_json['email_address']);
            $('#job_url').val(map_json['url']);
            $('#job_longitude').val(map_json['longitude']);
            $('#job_latitude').val(map_json['latitude']);
            $('#job_profile_image').val(map_json['profile_image']);
            $('#job-form').css('display', 'block');
        }
        else if(data['feature'] == 'article')
        {
            $('#article_title').val(map_json['title']);
            $('#article_description').val(map_json['description']);
            $('#article_tags').val(map_json['tags']);
            $('#article_profile_image').val(map_json['profile_image']);
            $('#article-form').css('display', 'block');
        }
    });
});  

function callTestUrl()
{
    if($('#test_url').val() == '')
        {
            alert('Enter a Test URL first');
        }
        else
        {
            $('#test_btn').attr('disabled', true);
            $('.save_btn').attr('disabled', true);
                      
            var api_flag = $('#api').iCheck('update')[0].checked;
            
            if(api_flag == false)
            {
                callAjaxScraping('', api_flag);
            }
            else if(api_flag == true)
            {
            $.ajax({
                url: '{{url("/")}}/admin/website_crawl/call_test_url',
                method: 'get',
                data: {test_url: $('#test_url').val(), 
                    api_regex: $('#api_regex').val(), 
                    api_url: $('#api_url').val(),
                    req_method: $('#req_method').val(),
                    api_check: api_flag},
                success: function (res) {
                    var status = res['status'];
                    ajax_msg = res['result'];
                    $('#test_btn').removeAttr('disabled');
                    $('.save_btn').removeAttr('disabled');
                    if(status != '200')
                    {
                        if(api_flag == true)
                        {
                            alert('API failed. Status Code: '+  status+ '. Try Fallback Scraping');
                            $('#scrape').attr('checked', true);
                            $('#api').removeAttr('checked');
                            $('#scrape').iCheck('update');
                            $('#api').iCheck('update');
                            $('#scrape').trigger("ifChecked");
                            $('#add_website').trigger('submit');
                        }
                    }
                    else
                    {
                        if(api_flag == true)
                        {
                            keys = [];   
                            parseJsonResult(ajax_msg, '', keys);
                            
//                            $.each(ajax_msg, function( index, value ) { 
//                                var y = typeof value;
//                                if(y === 'object')
//                                {
//                                    $.each(value, function( index1, value1 ) {
//                                        keys.push('||'+index + '-' + index1+'||');
//                                    });
//                                }   
//                                else{
//                                    keys.push('||'+index+'||');
//                                }
//                              });

                            $('#keywords').val(keys);
                            $('#keyword_div').css('display', 'block');
                            if($('#feature').val() == 'meetup' || $('#feature').val() == 'event')
                            {
                                $('#meetup-form').css('display', 'block');
                                $('#meetup-form input').trigger('change');
                            }
                            else if($('#feature').val() == 'job')
                            {
                                $('#job-form').css('display', 'block');
                                $('#job-form input').trigger('change');
                            }
                            else if($('#feature').val() == 'article')
                            {
                                $('#article-form').css('display', 'block');
                            }
                        }
                        else
                        {
                            
                        }
                    }
                    
                },
                error: function (res) {
                    if(api_flag == true)
                        {
                            alert('API failed. Try Fallback Scraping');
                            $('#scrape').attr('checked', true);
                            $('#api').removeAttr('checked');
                            $('#scrape').iCheck('update');
                            $('#api').iCheck('update');
                            $('#scrape').trigger("ifChecked");
                            $('#add_website').trigger('submit');
                        }
                            $('#test_btn').removeAttr('disabled');
                            $('.save_btn').removeAttr('disabled');
                }
            });
        }
        }
}

function callAjaxScraping(id, api_flag)
{
    var formdata = '';
    if(id != '')
    {
        var res_label = $('#'+id+'2');
        var inp_val = $('#'+id).val();
        formdata = id + '=' + encodeURIComponent(inp_val) + '&';
        $('#'+id).prop("readonly", true);
    }
    else
    {
        var form_name = '';
        if($('#meetup-form').is(':visible'))
        {
            form_name = 'meetup-form';
        }
        else if($('#job-form').is(':visible'))
        {
            form_name = 'job-form';
        }
        else if($('#article-form').is(':visible'))
        {
            form_name = 'article-form';
        }
        formdata = $("#"+form_name).serialize();
        $("#"+form_name+" input").prop("readonly", true);
    }
    
    $.ajax({
        url: '{{url("/")}}/admin/website_crawl/call_test_url',
        method: 'get',
        data: {test_url: $('#test_url').val(),            
            api_check: api_flag,
            formdata: formdata},
        success: function (res) {
            if(res == 'TIMED OUT')
            {
                if(id != '')
                {
                    $('#'+id+'2').html(res);
                    $('#' + id).prop("readonly", false);
                }
                else
                {
                    $("#"+form_name+" span").html(res);
                    $("#"+form_name+" input").prop("readonly", false);
                }
            }
            else if(res)
            {
                $.each(res, function(key, value){
                    $('#' + key + '2').html(value);
                    $('#' + key).prop("readonly", false);
                });
            }        
            $('#test_btn').removeAttr('disabled');
            $('.save_btn').removeAttr('disabled');
        },
        error: function (res) {
            $('#test_btn').removeAttr('disabled');
            $('.save_btn').removeAttr('disabled');
        }

    });
}

function parseJsonResult(jsonarray, res, keys)
{
    var data = res; 
    $.each(jsonarray, function( index, value ) { 
        var y = typeof value;
        if(data == '')
        {
            res = index;
        }
        else
        {
            res = data + '-' + index;
        }
        if(y === 'object' && value != null)
        {   
            var flag = Array.isArray(value);
            if(flag == true && value.length == 0)
            {
                keys.push('||'+res+'||');
            }
            else
            {
                parseJsonResult(value, res, keys);
            }
            
        }
        else{
            keys.push('||'+res+'||');

        }
      });   
}

function doSearch() {
    var queryStr = '?rpp=' + $('#recordPerPage').val() + '&search-qry=' + $('#search-qry').val() ;
    location.href = location.href.split('?')[0] + queryStr;
}
</script>
@stop
