@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>
    {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}}</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    @if(Session::has('success_msg'))
    <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="row" style="padding: 10px">
                <div class="col-md-2">
                    <select id="recordPerPage" name="recordPerPage" class="form-control" onchange="doSearch()">
                        <option value="25" {{ ($recordPerPage == 25) ? 'selected' : '' }}>25</option>
                        <option value="50" {{ ($recordPerPage == 50) ? 'selected' : '' }}>50</option>
                        <option value="100" {{ ($recordPerPage == 100) ? 'selected' : '' }}>100</option>
                        <option value="200" {{ ($recordPerPage == 200) ? 'selected' : '' }}>200</option>
                        <option value="500" {{ ($recordPerPage == 500) ? 'selected' : '' }}>500</option>
                    </select>
                </div>
                <div class="col-md-2">records per page</div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">&nbsp;</div>
                        <div class="col-md-1">Search</div>
                        <div class="col-md-4">
                            <input id="search-qry" name="search-qry" value="{{ $sqry }}" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <input type="button" id="search_btn" name="search_btn" class="btn btn-info" onclick="doSearch()" value="Go">
                        </div>
                    </div>
                </div>
            </div>
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline"  id="example1_wrapper">
                        <table class="table table-bordered table-striped "  aria-describedby="example1_info">
                            <thead>
                                <tr role="row">
                                    <th role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"   aria-label=""><input type="checkbox" id="selectall" name=""/></th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="">Url</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Feature</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Feature ID</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Created At</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Role Name">Groups</th>
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-label="Role Name">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($list))
                                    @foreach ($list as $key =>  $value)
                                        
                                        <tr>
                                            <td class="selectinp" ><input type="checkbox" name="selectinp" class="checkbox" value="{{$value->id}}"/></td>
                                            <td width="45%">
                                                <a href="{{$value->url}}">{{$value->url}}</a>
                                            </td>
                                            <td>{!! ucfirst($value->feature) !!}</td>
                                            <td>{!! $value->feature_id_fk !!}</td>
                                            <td>{!! $value->created_at !!}</td>
                                            <td>
                                                <a class="btn btn-info" id="add-group-btn_{{$value->id}}" href="#group-pop_{{$value->id}}" title="Add Group" data-toggle="modal" onclick="" > <i class="icon-edit icon-white"></i>Add Group <span id="group_count_{{$value->id}}" class="span_group_count">@if(count($value->groups)) ({{count($value->groups)}}) @else  (0)  @endif</span></a>
                                                <div id="group-pop_{{$value->id}}" class="group-pop modal fade" role="dialog" style="" tabindex='-1'>
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Add Groups</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            @include('Backend.includes.add-search-group-field', ['arr_selected_groups' => (isset($value->groups)?$value->groups:''), 'id' => $value->id, 'key' => $key ])
                                                          </div>
                                                          <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="btn btn-info" href="javascript:void(0);" onclick="approveUrl('{{$value->id}}');" title="Approve"> <i class="icon-edit icon-white"></i>Approve</a>
                                                <a class="btn btn-info" href="javascript:void(0);" onclick="approveUrl('{{$value->id}}', '0');" title="Reject"> <i class="icon-edit icon-white"></i>Reject</a>
                                            </td>
                                        </tr>
                                        <?php
                                            $str='<script>$(document).ready(function(){';
                                            if(isset($value->groups))
                                            {
                                                $grp_name = array();
                                                foreach ($value->groups as $val) {
                                                    //$str.='addGroup1('.$val->id.',"'.$val->group_name.'", "'.$value->id.'");';
                                                    $grp_name[] = $val->group_name;
                                                }
                                                $grp_name = implode(',', $grp_name);
                                                if($grp_name != '')
                                                {
                                                    $str.=' $("#add-group-btn_'.$value->id.'").prop("title","'.$grp_name.'");';
                                                }
                                            }
                                            $str.=' });</script>';
                                            echo $str;
                                        ?>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>                               
                            </tfoot>
                        </table>
                        <div id="actiondiv" style="display:none">
                            <a class="btn btn-info" id="add-group-btn_0" href="#group-pop_0" title="Add Group" data-toggle="modal" onclick="" > <i class="icon-edit icon-white"></i>Add Group</a>                                       
                            <div id="group-pop_0" class="group-pop modal fade" role="dialog" style="">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Groups</h4>
                                      </div>
                                      <div class="modal-body">
                                        @include('Backend.includes.add-search-group-field', ['arr_selected_groups' => '', 'id' => '0', 'key' => '' ])                                                          
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <a class="btn btn-info" href="javascript:void(0);" onclick="approveUrl('');" title="Approve"> <i class="icon-edit icon-white"></i>Approve</a>
                            <a class="btn btn-info" href="javascript:void(0);" onclick="approveUrl('', '0');" title="Approve"> <i class="icon-edit icon-white"></i>Reject</a>
                            
                        </div>
                    </div>    
                        {{ $list->appends(['search_qry' => $sqry, 'rpp' => $recordPerPage])->links() }}               
                </div>
            </div>
        </div>
    </div>
</section>


<script> 
    var activeurlid = '';
    var last_check_index = '';
    
    $(document).ready(function(){
              
        $('#selectall').on('ifChanged', function(event){ 
            if(event.target.checked == true)
            {
                $('input[name=selectinp]').prop('checked', true);
            }
            else
            {
                $('input[name=selectinp]').prop('checked', false);
            }
            $('input[name=selectinp]').iCheck('update');
            $('input[name=selectinp]').trigger("ifChanged");
        });
        
        $('input[name=selectinp]').on('ifChanged', function(event){ 
            var x = $('input[name=selectinp]:checked').length;
            if(x > 1)
            {
                $('#actiondiv').css('display', 'block');
            }
            else
            {
                $('#actiondiv').css('display', 'none');
            }
            cur_check_index = $('input[name=selectinp]').index($(this));
            if(!(last_check_index === ''))
            {
                $(document).bind('keydown', function (event) {
                    if (event.keyCode == 16) {
                      console.log('shift down<br>');
                    }
                });
                $(document).bind('keyup', function (event) {
                  if (event.keyCode == 16) {
                    console.log('shift up<br>');
                    var flag = $('input[name=selectinp]')[cur_check_index].checked;

                    $('.checkbox').slice(Math.min(cur_check_index,last_check_index)+1, Math.max(cur_check_index,last_check_index)+ 1).prop('checked', flag);
                    $('div.icheckbox_minimal').slice(Math.min(cur_check_index,last_check_index)+1, Math.max(cur_check_index,last_check_index)+ 1).addClass('checked');
                    cur_check_index = '';
                  }
                });

            }
            if (last_check_index === '') {
                last_check_index = cur_check_index;
            }
           
        });
    });
        
    $('.group-pop').on('show.bs.modal', function () {
            var id = $(this).attr('id');
            activeurlid = '_' + id.split('_')[1];
    });
    
    $('.group-pop').on('hide.bs.modal', function () {
        var x = $("#group_inp"+activeurlid).val().split(',');
            if(x == '')
            {
                $('#group_count'+ activeurlid).html('');
            }
            else
            {              
                var grouplength = x.length;
                $('#group_count'+ activeurlid).html('(' + grouplength + ')');
            }
            activeurlid = '';
    });
    
    function approveUrl(id, flag = '1')
    {
        if(id == '')
        {
            var groupids = $("#group_inp_0").val(); 
            if(groupids == '')
            {
                var y = [];
                var x = $('input[name=selectinp]:checked');
                $.each(x, function(xkey, xvalue){
                    var z = {'id' : xvalue.value, 'groups' : $("#group_inp_"+ xvalue.value).val()};
                    y.push(z);
                });
                var y_json = JSON.stringify(y);
            }
            else
            {
                var y = [];
                var x = $('input[name=selectinp]:checked');
                $.each(x, function(xkey, xvalue){
                    var z = {'id' : xvalue.value, 'groups' : $("#group_inp_0").val()};
                    y.push(z);
                });
                var y_json = JSON.stringify(y);
            }
        }
        else
        {
            var y = [];
            var groupids = $("#group_inp_"+id).val();
            var z = {'id' : id, 'groups' : groupids};
            y.push(z);
            var y_json = JSON.stringify(y);
        }     
                                                    
        $.post('{!!url("/")!!}/admin/website_crawl/review', {req_json: y_json, action: flag, rpp: $('#recordPerPage').val(), search_qry: $('#search-qry').val()}, function(msg) {
            //alert(msg);
            location.reload();
        });
    }
    
    function doSearch() {
        var queryStr = '?rpp=' + $('#recordPerPage').val() + '&search_qry=' + $('#search-qry').val() ;
        location.href = location.href.split('?')[0] + queryStr;
    }

    $(window).load(function(){
        //sort elemtns by group_count; so after page_load, posts with positive group_count are towards bottom of table
        var tbody = document.querySelector('tbody');
        $( "tbody > tr" ).each(function( i ) {
            no_of_groups_mapped = $(this).find('span.span_group_count').text();
            if ( no_of_groups_mapped.match(/\d/g)) {
                home = tbody.appendChild($(this).get(0));
            }
        });
    });
</script>
@stop
