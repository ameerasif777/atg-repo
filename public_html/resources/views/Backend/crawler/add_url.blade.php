@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>
    {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}}</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    @if(Session::has('success_msg'))
    <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    *Note: Add Url's separated by RETURN Key
                    <br/>
                    <form action="post_add_url" method="post">
                    <div class="box-body form-group">
                        <label for="comment">Add Url List:</label>
                        <textarea class="form-control"  maxlength="20000" rows="20" id="url_list" name="url_list" required></textarea>                       
                    </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
