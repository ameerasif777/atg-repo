@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Company User Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/users/list/company"><i class="fa fa-fw fa-user"></i> Manage Company Users</a></li>
        <li class="active">
            Company User Details
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p class="lead">User Profile</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody><tr>
                                @if (isset($arr_company_detail[0]->first_name) && $arr_company_detail[0]->first_name != '')
                                    
                                    <th style="width:50%">First Name :</th>
                                    <td>{{ ucfirst($arr_company_detail[0]->first_name) }} </td>
                                @endif
                            </tr>
                            <tr>
                               @if (isset($arr_company_detail[0]->last_name) && $arr_company_detail[0]->last_name != '') 
                                    
                                    <th>Last Name  :</th>
                                    <td> {{ ucfirst($arr_company_detail[0]->last_name) }} </td>
                             @endif
                            </tr>

                            <tr>
                                @if(isset($arr_company_detail[0]->user_name) && $arr_company_detail[0]->user_name != '') 
                                    
                                    <th>User Name :</th>
                                    <td>{{ $arr_company_detail[0]->user_name }}</td>
                                @endif
                            </tr>
                            <tr>
                                @if(isset($arr_company_detail[0]->email) && $arr_company_detail[0]->email != '') 
                                    
                                    <th>Email Id:</th>
                                    <td>{{ $arr_company_detail[0]->email }}</td>
                                @endif
                            </tr>
                            @if(isset($arr_company_detail[0]->gender) && $arr_company_detail[0]->gender == '0') 
                                
                                <tr>
                                    <th>Gender:</th>
                                    <td>{{'Male'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->gender) && $arr_company_detail[0]->gender == '1') 
                                
                                <tr>
                                    <th>Gender:</th>
                            <td>{{'Female'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->status) && $arr_company_detail[0]->status == '0')
                                
                                <tr>
                                    <th>User Status:</th>
                                    <td>{{'Inactive'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->status) && $arr_company_detail[0]->status == '1') 
                                
                                <tr>
                                    <th>User Status:</th>
                                    <td>{{'Active'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->status) && $arr_company_detail[0]->status == '2')
                                
                                <tr>
                                    <th>User Status:</th>
                                    <td>{{'Blocked'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->type) && $arr_company_detail[0]->type == '1') 
                                
                                <tr>
                                    <th>User Type:</th>
                                    <td>{{'Student'}}</td>
                                </tr>
                            @endif

                            @if(isset($arr_company_detail[0]->type) && $arr_company_detail[0]->type == '2')
                                
                                <tr>
                                    <th>User Type:</th>
                                    <td>{{'Admin'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->type) && $arr_company_detail[0]->type == '3') 
                                
                                <tr>
                                    <th>User Type:</th>
                                    <td>{{'Professional'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->type) && $arr_company_detail[0]->type == '4') 
                                
                                <tr>
                                    <th>User Type:</th>
                                    <td>{{'Company'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->mob_no) && $arr_company_detail[0]->mob_no != '') 
                                
                                <tr>
                                    <th>Mobile No:</th>
                                    <td>{{$arr_company_detail[0]->mob_no or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->phone_no) && $arr_company_detail[0]->phone_no != '') 
                                
                                <tr>
                                    <th>Phone No:</th>
                                    <td>{{$arr_company_detail[0]->phone_no or '-'}} </td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->height) && $arr_company_detail[0]->height != '') 
                                
                                <tr>
                                    <th>Height:</th>
                                    <td>{{$arr_company_detail[0]->height or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->weight) && $arr_company_detail[0]->weight != '') 
                                
                                <tr>
                                    <th>Weight:</th>
                                    <td>{{$arr_company_detail[0]->weight or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->body_id) && $arr_company_detail[0]->body_id != '') 
                                
                                <tr>
                                    <th>Body Type:</th>
                                    <td>{{$arr_company_detail[0]->body_id  or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->ethnicity_id) && $arr_company_detail[0]->ethnicity_id != '') 
                                
                                <tr>
                                    <th>Ethnicity Type:</th>
                                    <td>{{$arr_company_detail[0]->ethnicity_id or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->eye_id) && $arr_company_detail[0]->eye_id != '') 
                                
                                <tr>
                                    <th>Eye Color:</th>
                                    <td>{{$arr_company_detail[0]->eye_id or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->hair_id) && $arr_company_detail[0]->hair_id != '') 
                                
                                <tr>
                                    <th>Hair Color:</th>
                                    <td>{{$arr_company_detail[0]->hair_id or '-'}}</td>
                                </tr>
                            @endif
                            @if(isset($arr_company_detail[0]->profession) && $arr_company_detail[0]->profession != '') 
                                
                                <tr>
                                    <th>Profession:</th>
                                    <td>{{$arr_company_detail[0]->profession or '-'}}</td>
                                </tr>
                            @endif

                            @if(isset($arr_company_detail[0]->about_me) && $arr_company_detail[0]->about_me != '') 
                                
                                <tr>
                                    <th>About Me:</th>
                                    <td>{{$arr_company_detail[0]->about_me or '-'}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Selected Group</th>
                            </tr>
                          @if(!empty($finalData['arr_parent_group']))
                            @foreach($finalData['arr_parent_group'] as $key=> $value)                            
                            @if(!empty($value->chlid) && isset($finalData['arr_user_group']) && in_array($value->id,$finalData['arr_user_group'][0]))
                               <tr> 
                                   <td>{{$value->group_name }}</td>
                                 @endif 
                                <ul class="sub-menu sub-li " style="display: block;">
                                @foreach($value->chlid as $value1)
                                    @if(isset($finalData['arr_user_group']) && in_array($value1->id,$finalData['arr_user_group'][0]))
                                    <!--<li class="my_group_li_chlid" >-->
                                    <tr><td></td>  
                                    <td>{{$value1->group_name}}</td>
                                    
                                    </tr>
                                    <!--</li>-->
                                    @endif
                                    <!--<ul>-->
                                    @foreach($value1->chlid_1 as  $value2)    
                                    @if(isset($finalData['arr_user_group']) && in_array($value2->id,$finalData['arr_user_group'][0]))
                                    <!--<li class="my_group_li_chlid" >-->
                                    <tr>
                                    <td></td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;->{{$value2->group_name}}</td>
                                    @endif
                                    <!--</ul>-->
                                    </tr>
                                    <?php $counter = 1;?>
                                    @foreach($value2->chlid_2 as  $value3)    
                                    @if(isset($finalData['arr_user_group']) && in_array($value3->id,$finalData['arr_user_group'][0]))
                                    <!--<li class="my_group_li_chlid" >-->
                                    <tr>
                                    <td></td><td></td><td></td>
                                    <td>{{$counter++.')'.$value3->group_name}}</td>
                                    <!--</li>-->
                                    </tr>
                                    @endif
                               @endforeach
                            @endforeach
                            @endforeach
                                </ul>
                              
                            @endforeach
                            @else
                            <!--No Group Selected-->
                            @endif
                        </tbody>
                         <?php 
                            if($data['arr_parent_group'] !=''){
                            foreach ($data['arr_parent_group'] as $key => $value) {
                                ?>
                                <tr>
                                    <td>{{$value->group_name}}</td>
                                    <?php
                                    if ($value->chlid != '') {
                                        foreach ($value->chlid as $child_group) {
                                            if (!empty($child_group->chlid_1) && $child_group->chlid_1 != '') {
                                                ?>
                                                <td>{{$child_group->group_name}}</td>
                                                <?php
                                                if (!empty($child_group->chlid_1) && $child_group->chlid_1 != '') {
                                                    foreach ($child_group->chlid_1 as $sub_child) {
                                                        if (!empty($sub_child->chlid_2) && $sub_child->chlid_2 != '') {
                                                            ?>
                                                            <td>{{$sub_child->group_name}}</td>
                                                            <?php
                                                            if (!empty($sub_child->chlid_2) && $sub_child->chlid_2 != '') {
                                                                $counter = 1;
                                                                foreach($sub_child->chlid_2 as $sub_sub_child){?>
                                                                        <tr>
                                                                            <td></td><td></td>
                                                                                <td>{{$counter++.')'.$sub_sub_child->group_name}}</td>
                                                                        </tr>
                                                                <?php }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                </tr>
                            <?php }
                            }else{
                                ?>
                                <tr>
                                  <td></td>
                                  <td>No Group selected</td>
                                </tr> 
                                <?php
                            }?>
                    </table>                     
                </div>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('//assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('//assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('//assets/Backend/js/admin-manage/edit-admin.js')}}"></script>

@stop