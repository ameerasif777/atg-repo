@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Update Blacklisted Names
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/users/list/company"><i class="fa fa-fw fa-user"></i> Manage Company Users</a></li>
        <li class="active">
            Update Blacklisted Names
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form name="frm_blacknames" id="frm_blacknames" role="form"  action="{{url('/')}}/admin/blacklist/update/{!! $edit_id !!}" method="POST" >
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Name">Blacklisted Name </label>
                                    <input type="text" value="{!! str_replace('"', '&quot;', stripslashes($b_name['blacklist_name']))!!}" id="user_name" name="user_name" class="form-control">
                                    <input type="hidden" value="{!! str_replace('"', '&quot;', stripslashes($b_name['blacklist_name']))!!}" id="old_username" name="old_username">

                                </div>
                            </div>
                       </div>
            <div class="box-footer">
                <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                <input type="hidden" name="edit_id" id="edit_id" value="{!! intval(base64_decode($edit_id)) !!}" />
                <img src="{{url('/')}}/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
            </div>
           </form>
        </div>
    </div>
</div>
</section>        
@stop
@section('footer')  