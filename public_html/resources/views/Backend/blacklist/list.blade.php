@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>

<section class="content">
    <!--add name-->

    <div style="padding:10px;">
        <div class="row"> 
            <div class="col-md-3">
                <h4><label>ADD Blacklist Name :</label></h4>
            </div>   
            <div class="col-md-6">
                <input type="text" id="new_blacklist_name" class="form-control" name="new_blacklist_name" placeholder="Type Blacklisted Name">
            </div>   
            <div class="col-md-3">
                <button class="" type="button" onclick="save_blacklistname();" name="add_blacklist_name" id="add_blacklist_name">ADD</button>
            </div>
        </div>
    </div>

    <!--add name-->
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                <form name="frm_blacknames" role="form" id="frm_blacknames" action="{{url('/')}}/admin/blacklist/delete" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>
                                    Select <br>
                                    @if (count($blacklist) > 1)
                                    <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                    @endif
                                </center></th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username"> Blacklisted Name</th>
                                <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                    @foreach ($blacklist as $name) 
                                    <tr>
                                        <td >

                                <center>
                                    <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $name['id'] !!}" />
                                </center>

                                </td>
                                <td>{{$name['blacklist_name']}} </td>

                                <td class="">
                                    <a class="btn btn-info" title="Edit User Details" href="{{url('/')}}/admin/blacklist/edit/{!! base64_encode($name['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>

                                </td>
                                @endforeach
                                </tbody>


                                <tfoot>
                                <th colspan="9">
                                    @if (count($blacklist) > 0) 
                                    <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                    @endif
                                    <!--<a id="add_new_institute" name="add_new_institute" href="{{url('/')}}/admin/institute/add" class="btn btn-primary" >Add Blacklist Name</a>-->
                                </th>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop
<script type="text/javascript">
    // add blacklist
    function save_blacklistname() {
        var base_url = jQuery("#base_url").val();
        //alert(base_url);
        var b_name = $('#new_blacklist_name').val();
        //alert(b_name);
        if (b_name == '') {
            alert('Please Enter the value.');
            return false;
        }
        /*if (!(/^\S{3,}$/.test(b_name))) {
         alert('name cannot contain space.');
         return false;
         } 
         if(/^[a-zA-Z0-9 ]*$/.test(b_name) == false) {
         alert('Please do not enter special characters.');
         return false;
         }
         if($.trim(b_name).length < 4){
         alert('Please Enter more than 4 character.');
         return false;
         }*/

        $.ajax({
            url: base_url + '/add_blacklist_name',
            dataType: 'json',
            data: {'blacklist_name': b_name},
            method: 'post',
            success: function (response) {
                console.log(response);
                //alert(response.msg);
                if (response.error == '0') {

                    alert('Username is already exists.Try Another!!');
                    return false;

                } else {

                    alert('Username is added successfully.');
                    $('#new_blacklist_name').val('');
                    location.reload();
                }
            }
        });
    }
</script>
