@extends('Backend.layouts.default')
@section('content')
    <section class="content-header">
        <h1>Suggested Group</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-fw fa-envelope"></i> Manage Group</li>
        </ol>
    </section>
    <section class="content">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
            <?php Session::forget('message'); ?>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th>Name</th>
                                        <th>Count</th>
                                        <th>First searched on</th>
                                        <th>Last searched on</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($suggested_groups as $suggested_group)
                                        <tr>
                                            <td>{{ $suggested_group->name }}</td>
                                            <td>{{ $suggested_group->count }}</td>
                                            <td>{{ $suggested_group->created_at }}</td>
                                            <td>{{ $suggested_group->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop