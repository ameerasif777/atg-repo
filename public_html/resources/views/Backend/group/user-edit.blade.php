@extends('Backend.layouts.default')
@section('content')
    <link rel="stylesheet" href="/public/assets/css/libs/select2.min.css">
    <script src="/public/assets/js/libs/jquery.js"></script>
    <script src="/public/assets/js/libs/select2.min.js"></script>
    <section class="content-header">
        <h1>{{ $site_title }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-fw fa-eye"></i> {{ $site_title }}</li>
        </ol>
    </section>
    <section class="content">
        @if(Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
        @endif

        <form action="javascript:void(0)" method="POST">
            <select style="width: 100%; margin: 0 auto" id="mySelect2" class="js-data-example-ajax" name="groups">
                @foreach($joined_groups as $joined_group)
                    <option value="{{ $joined_group['group']['id'] }}">{{ $joined_group['group']['group_name'] }}</option>
                @endforeach
            </select>
            <br><br>

            <input id="submit" class="btn btn-info" type="submit" name="submit" value="Save">
        </form>
    </section>
    <script>
        $('#mySelect2').select2({
            placeholder: 'Search group name',
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                method: 'POST',
                url: '/ajax/search-groups-by-name',
                data: function (params) {
                    return {
                        group_name: params.term,
                    }
                },
                processResults: function (data) {
                    $.each(data, function(index, value){
                        data[index].text = value.group_name;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
        $('#mySelect2 > option').prop('selected', 'selected');
        $('#mySelect2').trigger('change');

        $('#submit').click(function () {
            post_to_url('/admin/user/group/edit/{{ $user_id }}', {
                'groups': $('#mySelect2').val()
            });
        });

        function post_to_url(path, params, method = 'post')
        {
            let form = document.createElement("form");
            form.setAttribute('method', method);
            form.setAttribute('action', path);
            for(let key in params) {
                if(params.hasOwnProperty(key)) {
                    let hiddenField = document.createElement('input');
                    hiddenField.setAttribute('type', 'hidden');
                    hiddenField.setAttribute('name', key);
                    hiddenField.setAttribute('value', params[key]);
                    form.appendChild(hiddenField);
                }
            }
            document.body.appendChild(form);
            form.submit();
        }
    </script>
@endsection
