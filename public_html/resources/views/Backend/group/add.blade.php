@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Add Group
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/group/list"><i class="fa fa-gear"></i> Manage Group</a></li>
        <li class="active">Add Group</li>

    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <form name="frm_group" role="form"  id="frm_group" action="{{url('/')}}/admin/group/add" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <div class="box-body">

                        <div class="form-group">
                            <label for="country">Parent Group :</label>
                            <select class="form-control" name="parent_group" id="parent_group" onchange="return getChildGroup(this.value);">
                                <option value="0">No Group</option>
                                <?php
                                foreach ($arr_parent_group as $parent) {
                                    ?>
                                    <option value="<?php echo $parent['id']; ?>"><?php echo $parent['group_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" id="child" style="display: none;">
                            <label for="child">Child Group :</label>
                            <select class="form-control" name="child_group" id="child_group" onchange="return getSubChildGroup(this.value);">
                                <option value="">Select</option>
                            </select>
                        </div>

                        <div class="form-group" id="sub_child" style="display: none;">
                            <label for="sub_child">Sub Child Group :</label>
                            <select class="form-control" name="sub_child_group" id="sub_child_group" >
                                <option value="">Select</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Group Name">Group Name <sup class="mandatory">*</sup></label>
                            <input class="form-control" type="text" dir="ltr"  placeholder="Guest Name" name="group_name" id="group_name" value=""/>

                        </div>
                        <div class="form-group">
                            <label for="Group Name">Profession <sup class="mandatory">*</sup></label>
                            <input class="form-control" type="text" dir="ltr"  placeholder="Profession" name="profession" id="profession" value=""/>

                        </div>
                        <div class="form-group">
                            <label for="User Name">Group Name Synonyms</label>
                            <textarea type="text" id="group_description"placeholder="Guest Description" name="group_description" class="form-control" ></textarea>
                        </div>
                        <div class="form-group" id="div_icon_img">
                            <label for="User Name">Icon</label>
                            <input type="file" id="icon_img" name="icon_img" class="form-control"/>


                        </div>                        
                        <div class="form-group" id="div_cover_img">
                            <label for="User Name">Cover Image</label>
                            <input type="file" id="cover_img" name="cover_img" class="form-control"/>

                        </div>                        
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btn_submit">Save</button>
                            <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                            <input type="hidden" name="icon_path" id="path" value="{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/">
                            <input type="hidden" name="cover_path" id="search_path" value="{{url('/')}}/public/assets/Backend/img/group_img/cover/thumb/">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<script src="{{url('/assets/Backend/js/add-group/add-edit-role.js')}}"></script>

<script type="text/javascript">
                                $(document).ready(function() {

                                    $('#icon_img').change(function() {
                                        var ext = this.value.match(/\.(.+)$/)[1];
                                        switch (ext) {
                                            case 'jpg':
                                            case 'jpeg':
                                            case 'png':
                                            case 'gif':
                                            case 'JPG':
                                            case 'JPEG':
                                            case 'PNG':
                                            case 'GIF':
                                                break;
                                            default:
                                                alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                                this.value = '';
                                        }
                                    });
                                    var _URL = window.URL || window.webkitURL;
                                    $("#icon_img").change(function(e) {
                                        var file, img;
                                        if ((file = this.files[0])) {
                                            img = new Image();
                                            img.onload = function() {
//                                console.log(this.width + " " + this.height)
                                                if (this.width < 100 && this.height < 100) {
                                                    alert("Image height and width should be greater than 100*100");
                                                    $("#icon_img").replaceWith($("#icon_img").val('').clone(true));
                                                }
                                                else
                                                {
                                                    readURL(this);
                                                }
                                            };
                                            img.src = _URL.createObjectURL(file);
                                        }
                                    });


                                    $("#cover_img").change(function(e) {

                                        var ext = this.value.match(/\.(.+)$/)[1];
                                        switch (ext) {
                                            case 'jpg':
                                            case 'jpeg':
                                            case 'png':
                                            case 'gif':
                                            case 'JPG':
                                            case 'JPEG':
                                            case 'PNG':
                                            case 'GIF':
                                                break;
                                            default:
                                                alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                                this.value = '';
                                        }
                                        var file, img;
                                        if ((file = this.files[0])) {
                                            img = new Image();
                                            img.onload = function() {
                                                if (this.width < 1300 && this.height < 400) {
                                                    alert("Image height and width should be greater than 1300*400");
                                                    $("#cover_img").replaceWith($("#cover_img").val('').clone(true));
                                                }
                                                else {

                                                    if (this.files && this.files[0]) {
                                                        var reader = new FileReader();

                                                        reader.onload = function(e) {
                                                            $('#preview_cover_img').attr('src', e.target.result);
                                                        };

                                                        reader.readAsDataURL(this.files[0]);
                                                    }

                                                }

                                            };
                                            img.src = _URL.createObjectURL(file);

                                        }
                                    });
                                });
                                function readURL(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            alert('dfg');
                                            $('#preview_icon_img').attr('src', e.target.result);
                                        }

                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }
</script>
<script>
    function getChildGroup(parent_id)
    {
        $.get('{!!url("/")!!}/admin/group/get-chlid-group', {parent_id: parent_id}, function(msg) {
            if (msg != '') {
                $("#child").show();
                $.each(msg, function(index, value) {
                    $('#child_group').append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.group_name));
                });
            } else {
                $("#child").hide();
                $("#sub_child").hide();
//                                            $("#div_cover_img").hide();
//                                            $("#div_icon_img").hide();
                $("#child_group").html("<option value='' >Select</option>");
                $("#sub_child_group").html("<option value=''>Select</option>");
            }
        });
    }

    function getSubChildGroup(parent_id)
    {
        $.get('{!!url("/")!!}/admin/group/get-chlid-group', {parent_id: parent_id}, function(msg) {
            if (msg != '') {

                $("#sub_child").show();
                $.each(msg, function(index, value) {
                    $('#sub_child_group').append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.group_name));
                });
            } else {

                $("#sub_child").hide();
                $("#sub_child_group").html("<option value=''>Select</option>");
            }
        });
    }

</script>

@stop
