@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        Manage Group
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-envelope"></i> Manage Group</li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    <?php Session::forget('message'); ?>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                        <form name="frm_category" id="frm_category" action="<?php echo url('/'); ?>/admin/group/list" method="post">
                            {!! csrf_field() !!}
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th class="col-md-1">
                                            <?php if (count($arr_group) > 0) { ?>
                                    <center>Select <br><input type="checkbox" name="check_all" id="check_all"  class="select_all_button_class" value="select all" /></center>
                                <?php } ?> 
                                </th>
                                <th class="sorting_asc col-md-2" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Category Name">Group Name</th>
                                <th class="sorting_asc col-md-5" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Parameter Value">Group Name Synonyms</th>
                                <th class="sorting_asc col-md-1" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Joined in 7 days</th>
                                <th class="sorting_asc col-md-1" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Joined in 30 days</th>
                                <th class="sorting_asc col-md-1" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Total Members</th>
                                <th  class="sorting_asc col-md-1" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="Action">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <?php foreach ($arr_group as $row) { ?>
                                        <tr>
                                            <td align="left">
                                    <center><input name="checkbox[]" class="case" type="checkbox" id="checkbox" value="<?php echo $row['id']; ?>" /></center>
                                    </td>
                                    <td  align="left"><?php echo stripslashes($row['group_name']); ?></td>
                                    <td  align="left"><?php echo stripslashes(substr($row['group_description'], 0, 500)); ?></td>
                                    <!--<td  align="left"><?php echo $row['category_type']; ?></td>-->
                                    <td  align="left">
                                        <?php if ($row->lastSevenDaysUsers()->count() > 0) { ?>
                                            <a href="<?php echo url('/'); ?>/admin/group/download-csv/<?php echo base64_encode($row['id']); ?>/1"><?php echo $row->lastSevenDaysUsers()->count() ?></a>
                                        <?php } else { ?>
                                            <?php echo $row->lastSevenDaysUsers()->count() ?>
                                        <?php } ?>
                                    </td>
                                    <td  align="left">
                                        <?php if ($row->lastThirtyDaysUsers()->count() > 0) { ?>
                                            <a href="<?php echo url('/'); ?>/admin/group/download-csv/<?php echo base64_encode($row['id']); ?>/2"><?php echo $row->lastThirtyDaysUsers()->count() ?></a>
                                        <?php } else { ?>
                                            <?php echo $row->lastThirtyDaysUsers()->count() ?>
                                        <?php } ?>
                                    </td>
                                    <td  align="left">
                                        <?php if ($row->users()->count() > 0) { ?>
                                            <a href="<?php echo url('/'); ?>/admin/group/download-csv/<?php echo base64_encode($row['id']); ?>/3"><?php echo $row->users()->count() ?></a>
                                        <?php } else { ?>
                                            <?php echo $row->users()->count() ?>
                                        <?php } ?>
                                    </td>
                                    <td  align="left">
                                        <a class="btn btn-info" title="Edit Category" href="<?php echo url('/'); ?>/admin/group/edit-group/<?php echo base64_encode($row['id']); ?>"><i class="icon-edit icon-white"></i>Edit</a>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <?php if (count($arr_group) > 0) { ?>
                                    <tfoot>
                                    <th colspan="7">
                                        <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                        <a id="add_new_category" name="add_new_category" href="<?php echo url('/'); ?>/admin/group/add-group" class="btn btn-primary pull-right" >Add New Group </a>
                                    </th>
                                    </tfoot>
                                <?php } else { ?> 
                                    <tfoot>
                                    <th colspan="7">
                                        <a id="add_new_category" name="add_new_category" href="<?php echo url('/'); ?>/admin/group/add-group" class="btn btn-primary pull-right" >Add New Group </a>
                                    </th>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </form>
                    </div>
                </div>
                <!--[sortable body]--> 

            </div>
        </div>

    </div>
</section>
@stop