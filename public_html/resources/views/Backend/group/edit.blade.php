<?php //   dd($arr_sub_child);
//echo  (!empty($arr_sub_child))?'ss':'';die;   
// echo '<pre>'; print_R($arr_sub_child[0]->parent_id); ?>
@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Edit Group
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/group/list"><i class="fa fa-gear"></i> Manage Group</a></li>
        <li class="active">Edit Group</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <form name="frm_group" role="form"  id="frm_group" action="{{url('/')}}/admin/group/edit-group/<?php echo base64_encode($arr_group[0]->id); ?>" method="POST" enctype="multipart/form-data" >
                
                    {!! csrf_field() !!}
                    <?php
                    
                    if (isset($arr_sub_child)&& isset($arr_sub_child[0]->parent_id) ) {                        
                        $val = $arr_sub_child[0]->parent_id;
                    } elseif (isset($arr_child) && isset($arr_child[0]->parent_id)) {                        
                        $val = $arr_child[0]->parent_id;
                    } elseif (isset($arr_group) && isset($arr_group[0]->parent_id)) {                        
                        $val = $arr_group[0]->parent_id;
                    } else {                        
                        $val = 0;
                    }                    
                    ?>
                    
                    <div class="box-body">
                        <div class="form-group">
                            <label for="country">Parent Group :</label>
                            <select class="form-control" name="parent_group" id="parent_group" onchange="return getChildGroup(this.value);">
                                <option value="0">No Group</option>
                                <?php
                                foreach ($arr_parent_group as $parent) {
                                    ?>
                                    <option <?php if ($parent->id == $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->group_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>

                        </div>
                    </div>
                    <?php
                    if ((count($arr_sub_child) > 0) && (count($arr_child) > 0)) {
                        $title = 'Child Group';
                        $title = 'Sub Child Group';
//                    } elseif (isset($arr_child) && empty($arr_sub_child) {
                    } elseif (isset($arr_child) ) {
                        $title = 'Child Group';
                    }
                    ?>
                    <?php if (count($arr_sub_child) > 0) {
                                                
                        ?>
                        <div class="form-group" id="hide_child">
                            <label for="country"><?php echo (isset($title)) ? $title:''; ?> :</label>
                            <select class="form-control" name="child_group" id="edit_child_group" onchange="return getSubChildGroup(this.value);">
                                <option value="0">No Group</option>
                                <?php
                                foreach ($arr_sub_child as $child) {
                                    ?>
                                    <option <?php if ($child->id === $arr_child[0]->parent_id) { ?>selected="selected"<?php } ?> value="<?php echo $child->id; ?>"><?php echo $child->group_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>

                    <?php if (count($arr_child) > 0) {                                                 
                        ?>
                        <div class="form-group" id="hide_sub_child">
                            <label for="country"><?php echo (isset($title)) ? $title:''; ?> :</label>
                            <select class="form-control" name="sub_child_group" id="edit_sub_child_group" onchange="return getChildGroup(this.value);">
                                <option value="0">No Group</option>
                                <?php
                                foreach ($arr_child as $sub_child) {
                                    ?>
                                    <option <?php if ($sub_child->id === $arr_group[0]->parent_id) { ?>selected="selected"<?php } ?> value="<?php echo $sub_child->id; ?>"><?php echo $sub_child->group_name; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>

                    <div class="form-group" name="child" id="child" style="display: none;">
                        <label for="child">Child Group :</label>
                        <select class="form-control" name="change_child_group" id="child_group" onchange="return getSubChildGroup(this.value);">
                            <?php ?>
                            <option value="">Select</option>
                        </select>
                    </div>

                    <div class="form-group" id="sub_child" style="display: none;">
                        <label for="sub_child">Sub Child Group :</label>
                        <select class="form-control" name="change_sub_child_group" id="sub_child_group" >
                            <option value="">Select</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="group_name">Group Name <sup class="mandatory">*</sup></label>
                        <input class="form-control" type="text" dir="ltr"   name="group_name" id="group_name" value="<?php echo $arr_group[0]->group_name; ?>"/>
                        <input class="form-control" type="hidden" dir="ltr"   name="group_id" id="group_id" value="<?php echo $arr_group[0]->id; ?>"/>
                        <input class="form-control" type="hidden" dir="ltr"   name="old_group_name" id="old_group_name" value="<?php echo $arr_group[0]->group_name; ?>"/>
                    </div>
                    <div class="form-group">
                            <label for="Group Name">Profession <sup class="mandatory">*</sup></label>
                            <input class="form-control" type="text" dir="ltr"  placeholder="Profession" name="profession" id="profession" value="<?php echo $arr_group[0]->profession; ?>"/>

                        </div>

                    <div class="form-group">
                        <label for="group_description">Group Name Synonyms</label>
                        <textarea id="group_description" name="group_description" class="form-control" ><?php echo $arr_group[0]->group_description; ?></textarea>
                    </div>
                    <div class="form-group" id="div_icon_img">
                        <label for="User Name">Icon</label>
                        <input type="file" id="icon_img" name="icon_img"/>
                        <br>
                        <img  name='img_icon_view' id="img_icon_view" src='{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/<?php echo $arr_group[0]->icon_img; ?>' style="height: 212px;width:212px;" onerror="src='{{url('/')}}/public/assets/Backend/img/image_not_found.png'" />
                    </div>

                    <div class="form-group" id="div_cover_img">
                        <label for="User Name">Cover Image</label>
                        <input type="file" id="cover_img" name="cover_img" />
                        <br>
                        <input value="<?php echo $arr_group[0]->cover_img; ?>" name="old_img_cover_view" id="old_img_cover_view"  type="hidden" >
                        <img style="height: 212px;width:212px;" name='img_cover_view'id="img_cover_view" src='{{url('/')}}/public/assets/Backend/img/group_img/cover/thumb/<?php echo $arr_group[0]->cover_img; ?>' onerror="src='{{url('/')}}/public/assets/Backend/img/image_not_found.png'"/>
                    </div>

                    <div class="box-footer">
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        <input type="hidden" name="icon_path" id="path" value="{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/">
                        <input type="hidden" name="cover_path" id="search_path" value="{{url('/')}}/public/assets/Backend/img/group_img/cover/thumb/">
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</section>        
@stop
@section('footer')  
<script src="{{url('/assets/Backend/js/add-group/add-edit-role.js')}}"></script>

<script type="text/javascript">
                            $(document).ready(function() {

                                var _URL = window.URL || window.webkitURL;
                                $("#icon_img").change(function(e) {

                                    var ext = this.value.match(/\.(.+)$/)[1];
                                    switch (ext) {
                                        case 'jpg':
                                        case 'jpeg':
                                        case 'png':
                                        case 'gif':
                                        case 'JPG':
                                        case 'JPEG':
                                        case 'PNG':
                                        case 'GIF':
                                            break;
                                        default:
                                            alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                            this.value = '';
                                    }
                                    var file, img;
                                    if ((file = this.files[0])) {
                                        img = new Image();
                                        img.onload = function() {
                                            if (this.width < 100 && this.height < 100) {
                                                alert("Image height and width should be greater than 100*100");
                                                $("#cover_img").replaceWith($("#cover_img").val('').clone(true));
                                            }
                                        };
                                        img.src = _URL.createObjectURL(file);
                                    }
                                });

                                $("#cover_img").change(function(e) {
                                    var ext = this.value.match(/\.(.+)$/)[1];
                                    switch (ext) {
                                        case 'jpg':
                                        case 'jpeg':
                                        case 'png':
                                        case 'gif':
                                        case 'JPG':
                                        case 'JPEG':
                                        case 'PNG':
                                        case 'GIF':
                                            break;
                                        default:
                                            alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                            this.value = '';
                                    }
                                    var file, img;
                                    if ((file = this.files[0])) {
                                        img = new Image();
                                        img.onload = function() {
                                            if (this.width < 1300 && this.height < 400) {
                                                alert("Image height and width should be greater than 1300*400");
                                                $("#cover_img").replaceWith($("#cover_img").val('').clone(true));
                                            }
                                        };
                                        img.src = _URL.createObjectURL(file);
                                    }
                                });
                            });
</script>
<script>
    function getChildGroup(parent_id)
    {
        $.get('{!!url("/")!!}/admin/group/get-chlid-group', {parent_id: parent_id}, function(msg) {
            
            if (msg != '') {
//                alert('is availabel');
                $("#hide_child").empty();
                $("#hide_sub_child").empty();
                $("#main_child").hide();
//           $("#div_cover_img").hide();
//           $("#div_icon_img").hide();
                $("#child").show();
                $.each(msg, function(index, value) {
                    $('#child_group').append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.group_name));
                });
            } else {
//                alert('is not availabel');  
                 $("#hide_child").empty();
                $("#hide_sub_child").empty();
                $("#child").hide();
                $("#sub_child").hide();
                $("#child_group").html("<option value='' >Select</option>");
                $("#sub_child_group").html("<option value=''>Select</option>");
            }
        });
    }

    function getSubChildGroup(parent_id)
    {
        $.get('{!!url("/")!!}/admin/group/get-chlid-group', {parent_id: parent_id}, function(msg) {
            if (msg != '') {
                $("#sub_child").show();
                $.each(msg, function(index, value) {
                    $('#sub_child_group').append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.group_name));
                });
            } else {
                $("#sub_child").hide();
                $("#sub_child_group").html("<option value=''>Select</option>");
            }
        });
    }


</script>
<script>
    $(document).ready(function() {
        $("#hide_child").show();
        $("#hide_sub_child").show();
    });

</script>
@stop
