@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Student Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/users/list/student"><i class="fa fa-fw fa-user"></i> Manage Student Users</a></li>
        <li class="active">
            Student Details
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-lg-8 col-xl-8 col-md-8">
            <p class="lead">User Profile</p>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                       @include('Backend.includes.user-view',array('arr'=>$arr_user_detail))
                    </tbody>
                </table>

                @include('Backend.includes.referred_users', ['data' => $finalData])
                
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer')  
<link rel="stylesheet" href="{{url('//assets/Backend/css/jquery.validate.password.css')}}"/>
<script src="{{url('//assets/Backend/js/jquery.validate.password.js')}}"></script>
<script src="{{url('//assets/Backend/js/admin-manage/edit-admin.js')}}"></script>
@stop
