
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    @if(Session::has('success_msg'))
    <p class="alert alert-success">{{ Session::get('success_msg') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                
                <form name="frm_question" role="form" id="frm_question" action="{{url('/')}}/admin/slider-banner/delete-sliders-banners" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">									
                            <table class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
                                <thead>
                                    <tr role="row">
                                        <th> <center>
                                    Select <br>
                                    @if (count($arr_sliders) > 1)
                                    <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                    @endif
                                </center></th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Image">Image</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title">Slider Title</th>                                
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username">Description</th>
                                <!--<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Posted by">Posted By</th>-->
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Posted On</th>
                                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Created on">Status</th>                                
                                <th  role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Action">Action</th>                              
                                </tr>
                                </thead>

                                <tbody>

                              @foreach($arr_sliders as $sliders) 
                                    <tr>
                                        <td >
                                <center>
                                    <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $sliders->id !!}" />
                                </center>
                                </td>                               
                                <td width="10%"><img src="{{ url('/public/assets/Frontend/images/slider_banner_image/'.$sliders->slider_image) }}" alt="No image found" height="100" onerror=this.src="{{ url('/public/assets/Frontend/img/image-not-found2.jpg')}}" width="100"></td>
                                <td>{{ucfirst($sliders->slider_title)}}</td>
                                <td><p style="width:200px;word-wrap:break-word;">
                                        @if(strlen($sliders->slider_description) < 50)                                                        
                                                        {{ strip_tags($sliders->slider_description) }}   
                                                        @else                                                        
                                                        {{substr(strip_tags($sliders->slider_description), 0, 50)}}...
                                                        
                                                        @endif
                                    </p>
                                </td>                                
                                <td>{!! $sliders->created_at !!}</td>
                                <td>
                                        @if($sliders->status == 1)
                                        published 
                                        @elseif($sliders->status == 0)
                                        Unpublished 
                                        @endif
                                    </td>                                    
                                <td class="">
                                    <a class="btn btn-info" title="Edit Slider Details" href="{{url('/')}}/admin/slider-banner/edit-slider-banner/{!! base64_encode($sliders->id)!!}"> <i class="icon-edit icon-white"></i>Edit</a>                                   
                                </td>                                
                                @endforeach                                
                                </tbody>           
                                
                                    <tfoot>
                                        <th colspan="9">
                                        @if (count($arr_sliders) > 0) 
                                            <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                        @endif
                                            <a id="add_new_admin" name="add_new_admin" href="{{url('/')}}/admin/slider-banner/add-slider-banner" class="btn btn-primary pull-right" >Add New Admin </a>
                                        </th>
                                    </tfoot>
                                
                                
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop
