@extends('Backend.layouts.default')
@section('content') 

<section class="content-header">
    <h1>
        Add Slider Banner
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/slider-banner/list-sliders-banners"><i class="fa fa-gear"></i> Manage Slider Banner</a></li>
        <li class="active">Add slider Banner</li>

    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <form name="frm_add_slider_banner" role="form"  id="frm_add_slider_banner" action="{{url('/')}}/admin/slider-banner/add-slider-banner" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Slider Name">Slider Name <sup class="mandatory">*</sup></label>
                            <input class="form-control" type="text" dir="ltr"  placeholder="Slider Name" name="slider_title" id="slider_title" value=""/>
                        </div>                        
                        <div class="form-group">
                            <label for="User Name">Slider Description<sup class="mandatory">*</sup></label>
                            <textarea type="text" id="slider_description" placeholder="Slider Description" name="slider_description" class="form-control" ></textarea>
                        </div>
                        </div>
                        <div class="form-group" id="div_cover_img">
                            <label for="User Name">Silder Image</label>
                            <input type="file" id="slider_banner_image" name="slider_banner_image" class="form-control"/>
                        </div>                        
                        <div class="box-footer">
                            <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btn_submit">Save</button>
                           <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        <input type="hidden" name="slider_banner_image_path" id="path" value="{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>        
@stop
@section('footer') 
<script>
       $(document).ready(function() {
    $("#frm_add_slider_banner").validate({
                        errorElement: 'div',
                        rules: {
                            slider_title: {
                                required: true
                            },
                            slider_description: {
                                required: true,                                
                            },                          
                        },
                        messages: {
                            slider_title: {
                                required: "Please enter slider name.",
                            },
                            slider_description: {
                                required: "Please enter slider description.",                                
                            },                            
                        },
                        submitHandler: function(form) {
                            $("btn_submit").hide();
                            form.Submit();
                        }
                });
                /****image validation****/
                                var _URL = window.URL || window.webkitURL;
                                $("#slider_banner_image").change(function(e) {

                                    var ext = this.value.match(/\.(.+)$/)[1];
                                    switch (ext) {
                                        case 'jpg':
                                        case 'jpeg':
                                        case 'png':
                                        case 'gif':
                                        case 'JPG':
                                        case 'JPEG':
                                        case 'PNG':
                                        case 'GIF':
                                            break;
                                        default:
                                            alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                            this.value = '';
                                    }
                                    var file, img;
                                    if ((file = this.files[0])) {
                                        img = new Image();
                                        img.onload = function() {
                                            if (this.width < 1242 && this.height < 2208) {
                                                alert("Image height and width should be greater than 1242*2208");
                                                $("#slider_banner_image").replaceWith($("#slider_banner_image").val('').clone(true));
                                            }
                                        };
                                        img.src = _URL.createObjectURL(file);
                                    }
                                });
                });
</script>
@stop