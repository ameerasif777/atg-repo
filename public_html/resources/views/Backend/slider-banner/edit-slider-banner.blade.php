
@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Edit Slider Banner
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/slider-banner/list-sliders-banners"><i class="fa fa-gear"></i> Manage Slider Banner</a></li>
        <li class="active">Edit Slider Banner</li>

    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <form name="frm_edit_slider_banner" role="form"  id="frm_edit_slider_banner" action="{{url('/')}}/admin/slider-banner/edit-slider-banner/<?php echo base64_encode($silder_array[0]->id); ?>" method="POST" enctype="multipart/form-data" >
                
                    {!! csrf_field() !!}                    
                    <div class="form-group">
                        <label for="slider_title">Slider Banner Name <sup class="mandatory">*</sup></label>
                        <input class="form-control" type="text" dir="ltr"   name="slider_title" id="slider_title" value="<?php echo $silder_array[0]->slider_title; ?>"/>                    
                        <input class="form-control" type="hidden" dir="ltr"   name="slider_id" id="slider_id" value="<?php echo $silder_array[0]->slider_id; ?>"/>                    
                    </div>
                    <div class="form-group">
                        <label for="slider_description">Slider Banner Description<sup class="mandatory">*</sup></label>
                        <textarea id="slider_description" name="slider_description" class="form-control" ><?php echo $silder_array[0]->slider_description; ?></textarea>
                    </div>
                    <div class="form-group">
                                    <label for="parametername">Publish Status</label>
                                    <select class="form-control" name="status" id="status" class="">
                                        <option @if($silder_array[0]->status == "0")  selected="selected" @endif  value="0">Unpublished</option>
                                        <option @if($silder_array[0]->status == "1")  selected="selected" @endif value="1">Published</option>                                        
                                    </select>

                    </div>
                    <div class="form-group" id="div_cover_img">
                        <label for="User Name">Slider Banner Image</label>
                        <input type="file" id="slider_banner_image" name="slider_banner_image" onchange="readURL(this);" />
                        <br>
                        <img class="uploding-img-vie" height="200px"  width="200px" id="blah" src="{{ url('/public/assets/Frontend/images/slider_banner_image/thumb/'.$silder_array[0]->slider_image) }}" onerror=this.src="{{ url('/public/assets/Frontend/img/image-not-found2.jpg')}}" width=100%" height="200"/><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                    </div>
                    <div class="box-footer">
                        <button type="submit" name="btn_submit" class="btn btn-primary" value="Save" id="btnSubmit">Save Changes</button>
                        <img src="{{url('/')}}/public/assets/Backend/img/ajax-loader.gif" style="display: none;" id="loding_image">
                        <input type="hidden" name="slider_banner_image_path" id="path" value="{{url('/')}}/public/assets/Backend/img/group_img/icon/thumb/">
                        
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</section>        
@stop
@section('footer')  
<script src="{{url('/assets/Backend/js/add-group/add-edit-role.js')}}"></script>

<script type="text/javascript">
        $(document).ready(function() {
    $("#frm_edit_slider_banner").validate({
                        errorElement: 'div',
                        rules: {
                            slider_title: {
                                required: true
                            },
                            slider_description: {
                                required: true,                                
                            },                          
                        },
                        messages: {
                            slider_title: {
                                required: "Please enter slider name.",
                            },
                            slider_description: {
                                required: "Please enter slider description.",                                
                            },                            
                        },
                        submitHandler: function(form) {
                            $("btn_submit").hide();
                            form.Submit();
                        }
                });
                /****image validation****/
                                var _URL = window.URL || window.webkitURL;
                                $("#slider_banner_image").change(function(e) {

                                    var ext = this.value.match(/\.(.+)$/)[1];
                                    switch (ext) {
                                        case 'jpg':
                                        case 'jpeg':
                                        case 'png':
                                        case 'gif':
                                        case 'JPG':
                                        case 'JPEG':
                                        case 'PNG':
                                        case 'GIF':
                                            break;
                                        default:
                                            alert('Please upload a file only of type jpg,png,gif,jpeg.');
                                            this.value = '';
                                    }
                                    var file, img;
                                    if ((file = this.files[0])) {
                                        img = new Image();
                                        img.onload = function() {
                                            if (this.width < 1242 && this.height < 2208) {
                                                alert("Image height and width should be greater than 1242*2208");
                                                $("#slider_banner_image").replaceWith($("#slider_banner_image").val('').clone(true));
                                            }
                                        };
                                        img.src = _URL.createObjectURL(file);
                                    }
                                });
                });
                
 function readURL(input) {
        var ext = input.value.match(/\.(.+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {                     
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Only Image allowed !');
                input.value = '';
        }
    }                  
</script>
@stop
