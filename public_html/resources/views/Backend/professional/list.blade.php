
@extends('Backend.layouts.default')
@section('content')  
<section class="content-header">
    <h1>
        {{$site_title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-fw fa-eye"></i> {{$site_title}} </li>
    </ol>
</section>
<section class="content">
    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="row" style="padding: 10px;">
                    <div class="col-md-2">
                        <select id="recordPerPage" name="recordPerPage" class="form-control" onchange="doSearch();">
                            <option value="25" @if($recordPerPage == 25) selected @endif>25</option>
                            <option value="50" @if($recordPerPage == 50) selected @endif>50</option>
                            <option value="100" @if($recordPerPage == 100) selected @endif>100</option>
                            <option value="200" @if($recordPerPage == 200) selected @endif>200</option>
                            <option value="500" @if($recordPerPage == 500) selected @endif>500</option>
                        </select>
                    </div>
                    <div class="col-md-2">records per page</div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">&nbsp;</div>
                            <div class="col-md-1">Search</div>
                            <div class="col-md-4">
                                <input id="search-qry" name="search-qry" value="{{ $sqry }}" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <select id="search-status" name="search-status" class="form-control">
                                    <option value="0" @if($sstatus == 0) selected @endif>Inactive</option>
                                    <option value="1" @if($sstatus == 1) selected @endif>Active</option>
                                    <option value="2" @if($sstatus == 2) selected @endif>Blocked</option>
                                    <option value="" @if($sstatus == '') selected @endif>All</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="button" id="search_btn" name="search_btn" class="btn btn-info" onclick="doSearch();" value="Go">
                            </div>
                        </div>
                    </div>
                </div>
                <form name="frm_registered_users" role="form" id="frm_registered_users" action="{{url('/')}}/admin/professional/delete" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
                            <table class="table table-bordered table-striped" aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th style="text-align: center;">
                                        Select <br>
                                        @if (count($arr_professional_list) > 1)
                                            <input type="checkbox"  name="check_all" id="check_all"  class="select_all_button_class" value="select all" />
                                        @endif
                                    </th>
                                    <th aria-label="Username"> Full Name</th>
                                    <th aria-label="First Name">Email ID</th>
                                    <th aria-label="Email Verified">Email Verified</th>
                                    <th aria-label="Status">Status</th>
                                    <th aria-label="Last Login">Last Login</th>
                                    <th aria-label="Created on">Created on</th>
                                    <th aria-label="Action">Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach ($arr_professional_list as $registered_user)
                                    <tr>
                                        <td style="text-align: center;">
                                            <input name="checkbox[]" class="case" type="checkbox" id="checkbox[]" value="{!! $registered_user['id'] !!}" />
                                        </td>
                                        <td>{{ isset($registered_user['first_name']) && isset($registered_user['last_name'])? stripslashes(ucfirst($registered_user['first_name'])).' '.stripslashes(ucfirst($registered_user['last_name'])):'None' }}</td>
                                        <td >{{$registered_user['email']}}</td>
                                        <td >
                                            <div id="active_div_email{{$registered_user['id']}}"
                                                 @if($registered_user['email_verified'] == 1)
                                                 style="display:inline-block"
                                                 @else
                                                 style="display:none;"
                                                    @endif>
                                                <a class="label label-success" title="Click to Change Status" onClick="changeEmailVerified('{{ $registered_user['id']}}', 0);" href="javascript:void(0);" id="status_{{$registered_user['id']}}">Verified</a>
                                            </div>

                                            <div id="inactive_div_email{{ $registered_user['id'] }}"
                                                 @if ($registered_user['email_verified'] == 0)
                                                 style="display:inline-block"
                                                 @else
                                                 style="display:none;"
                                                    @endif >
                                                <a class="label label-danger" title="Click to Change Status" onClick="changeEmailVerified('{{$registered_user['id']}}', 1);" href="javascript:void(0);" id="status_{{ $registered_user['id'] }}">Not verified</a>
                                            </div>
                                        </td>
                                        <td >

                                            <div id="inactive_div{!! $registered_user['id']!!}"  @if ($registered_user['user_status'] == 0)  style="display:inline-block" @else style="display:none;" @endif >
                                                <a class="label label-warning" title="Click to Change Status" onClick="changeStatus({!! $registered_user['id']!!}, 1);" href="javascript:void(0);" id="status_{!! $registered_user['id']!!}">Inactive</a>
                                            </div>

                                            <div id="active_div{!! $registered_user['id']!!}"  @if ($registered_user['user_status'] == 1)  style="display:inline-block" @else style="display:none;" @endif >
                                                <a class="label label-success" title="Click to Change Status" onClick="changeStatus({!! $registered_user['id']!!}, 2);" href="javascript:void(0);" id="status_{!! $registered_user['id']!!}">Active</a>
                                            </div>

                                            <div id="blocked_div{!! $registered_user['id']!!}" @if ($registered_user['user_status'] == 2) style="display:inline-block" @else  style="display:none;" @endif >

                                                <a class="label label-danger" title="Click to Change Status" onClick="changeStatus({!! $registered_user['id']!!}, 0);" href="javascript:void(0);" id="status_{!! $registered_user['id']!!}">Blocked</a>
                                            </div>

                                        </td>
                                        <td>
                                            @if($registered_user['last_login'] != null || $registered_user['last_logout_time'] != null)
                                                {!! date($global_values['date_format'] . ' H:i:s', strtotime($registered_user['last_login'])) !!}
                                                
                                                @if($registered_user['last_logout_time'] != null)
                                                    -
                                                    {!! date($global_values['date_format'] . ' H:i:s', strtotime($registered_user['last_logout_time'])) !!}
                                                @endif
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td >{!! date($global_values['date_format'], strtotime($registered_user['created_at'])) !!}</td>
                                        <td class="">
                                            <a class="btn btn-info" title="Edit professional Details" href="{{url('/')}}/admin/professional/edit/{!! base64_encode($registered_user['id'])!!}"> <i class="icon-edit icon-white"></i>Edit</a>
                                            <a class="btn btn-info" title="View professional Details" href="{{url('/')}}/admin/professional/view/{!! base64_encode($registered_user['id']); !!}"> <i class="icon-edit icon-white"></i>View</a>
                                            <a class="btn btn-info" href="{!! url('admin/user/group/edit/' . base64_encode($registered_user['id'])) !!}">group</a>
                                        </td>
                                @endforeach

                                </tbody>

                                <tfoot>
                                <th colspan="8">
                                    @if (count($arr_professional_list) > 0)
                                        <input type="submit" id="btn_delete_all" name="btn_delete_all" class="btn btn-danger" onClick="return deleteConfirm();"  value="Delete Selected">
                                @endif
                                <!--<a id="add_new_institute" name="add_new_institute" href="{{url('/')}}/admin/institute/add" class="btn btn-primary pull-right" >Add New Institute </a>-->
                                </th>
                                </tfoot>

                            </table>
                        </div>
                        <div class="text-right">
                            {{ $arr_professional_list->appends(['search-status' => $sstatus, 'search-qry' => $sqry, 'rpp' => $recordPerPage])->links() }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop

<script>
    function doSearch(){
        var queryStr = '?rpp=' + $('#recordPerPage').val() + '&search-qry=' + $('#search-qry').val() + '&search-status=' + $('#search-status').val();
        location.href = "{{url('/')}}/admin/users/list/professional" + queryStr;
    }
    function changeStatus(user_id, user_status) {
        /* changing the user status*/
        var obj_params = new Object();
        obj_params.user_id = user_id;
        obj_params.user_status = user_status;
        jQuery.get("{{url('/')}}/admin/user/change-status", obj_params, function(msg) {
            if (msg.error == "1") {
                alert(msg.error_message);
            } else {
                /* togling the bloked and active div of user*/
                if (user_status == 2) {
                    $("#blocked_div" + user_id).css('display', 'inline-block');
                    $("#active_div" + user_id).css('display', 'none');
                    $("#inactive_div" + user_id).css('display', 'none');
                } else if (user_status == 1){
                    $("#active_div" + user_id).css('display', 'inline-block');
                    $("#blocked_div" + user_id).css('display', 'none');
                    $("#inactive_div" + user_id).css('display', 'none');
                }else if(user_status == 0){
                    $("#active_div" + user_id).css('display', 'none');
                    $("#blocked_div" + user_id).css('display', 'none');
                    $("#inactive_div" + user_id).css('display', 'inline-block');
                }
            }
        }, "json");
    }
    function changeEmailVerified(user_id, user_email_verified) {
        /* changing the user status*/
        var obj_params = new Object();
        obj_params.user_id = user_id;
        obj_params.user_email_verified = user_email_verified;
        jQuery.get("{{url('/')}}/admin/user/change-status-email", obj_params, function(msg) {
            if (msg.error == "1") {
                alert(msg.error_message);
            } else {
                /* togling the bloked and active div of user*/
                if (user_email_verified == 1) {
                    //location.reload();
                    $("#active_div_email" + user_id).css('display', 'inline-block');
                    $("#inactive_div_email" + user_id).css('display', 'none');
                } else {
                    // location.reload();
                    $("#inactive_div_email" + user_id).css('display', 'inline-block');
                    $("#active_div_email" + user_id).css('display', 'none');
                }
            }
        }, "json");
    }
</script>