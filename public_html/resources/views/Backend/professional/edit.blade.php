@extends('Backend.layouts.default')
@section('content') 
<section class="content-header">
    <h1>
        Update Professional User Details
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('/')}}/admin/users/list/professional"><i class="fa fa-fw fa-user"></i> Manage Professional Users</a></li>
        <li class="active">
            Update Professional User Details
        </li>
    </ol>
</section>

        @include('Backend.includes.user-edit',array('arr'=>$arr_professional_detail))
       
@stop
@section('footer')  

@include('Backend.includes.user-edit-footer')

@stop
