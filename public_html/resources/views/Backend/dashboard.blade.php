@extends('Backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>
        Admin Dashboard
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
    </ol>
</section>
<section class="content"> 
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row">
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        {!!$total_admin!!}
                    </h3>
                    <p>
                        Total Admin Users
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                @if($role_id==1)
                <a href="{{url('/')}}/admin/admin/list" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
                @else
                <a href="javascript:void(0);" class="small-box-footer" onclick="setSession();">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
                @endif
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        {!!$total_student!!}
                    </h3>
                    <p>
                        Total Student
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{url('/')}}/admin/users/list/student" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        {!!$total_professional!!}
                    </h3>
                    <p>
                        Total Professional
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{url('/')}}/admin/users/list/professional" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        {!!$total_company!!}
                    </h3>
                    <p>
                        Total Company
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{url('/')}}/admin/users/list/company" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {!!$arr_24hr_user!!}
                    </h3>
                    <p>
                        Total New Users From Last 24 hours
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <!-- <a href="{{url('/')}}/admin/users/list/company" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {!!$arr_7day_user!!}
                    </h3>
                    <p>
                        Total New Users From Last 7 days
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <!-- <a href="{{url('/')}}/admin/users/list/company" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {!!$arr_30day_user!!}
                    </h3>
                    <p>
                        Total New Users From Last 30 days
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <!-- a href="{{url('/')}}/admin/users/list/company" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {!!$arr_total_user_count!!}
                    </h3>
                    <p>
                        Total Users
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                @if($role_id==1)
                <a href="{{url('/')}}/admin/users/list/download" class="small-box-footer">
                    CSV Download <i class="fa fa-arrow-circle-right"></i>
                </a>
                @endif
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!--small box--> 
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        {!!$arr_total_verified_users!!}
                    </h3>
                    <p>
                        Total Verified Users
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <!-- <a href="{{url('/')}}/admin/users/list/company" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>-->
            </div>
        </div>
    </div>
</section>
@endsection
<script>
    function setSession() {
        alert('You don\'t have privileges to manage admin users');
    }
</script>
