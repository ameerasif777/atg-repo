@extends('Backend.layouts.default')
@section('content')
<script>
    $(document).ready(function() {
        getNumOfUsersInGroup();
    
    })
</script>
<section class="content-header">
    <h1>
        Send Emails
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
        <li class="active"><i class="fa fa-fw fa-envelope"></i> Send Emails </li>
    </ol>
</section>
<section class="content"> 
    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
    @endif
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box">
                <div class="row" style="padding: 10px;"> 
                    <div class="col-md-2">Select Option : </div>
                    <div class="col-md-6">
                        <select id="sel-opt" name="sel-opt" class="form-control" onchange="onChangeOption($(this).val())">
                            <option value="" >Select</option>
                            <option value="group" >Send Email to a Group</option>
                            <option value="drafts" >Send Email to User who has all Drafted Posts</option>
                            <option value="absent" >Send Email to User not logged since 10 Days</option>
                            <option value="all" >Send Email to all Users</option>
                        </select>
                    </div>
                </div>
                <form id="group-form" name="group-form" method="POST"  action="{{url('/')}}/admin/send-emails/edit-email">
                    <div id="group-div" class="row" style="padding: 10px; display: none;"> 
                        <div class="col-md-2">Select Group : </div>
                        <div class="col-md-10">
                            <div class="form-group">   
                                <ul id="selected" class="selectedclass" onclick="$('#groupPanel').show();" ></ul>
                                <div class="text-danger" for="sub_groups[]" generated="true"></div>
                                @include('Backend.includes.add-group-field-for-backend-features')
                            </div>
                        </div>
                    </div>
                    <div id="users-div" class="row" style="padding: 10px; display: none;">
                    </div>
                    <input type="hidden" id="userids" name="userids"/>
                    <div id="emailtemp-div" class="row" style="padding: 10px; display: none;"> 
                        <div class="col-md-2">Import an existing Email Template : </div>
                        <div class="col-md-10">
                            <div class="form-group">   
                                <select id="email-temp" name="email-temp" class="form-control" onchange="this.form.submit();">  
                                </select>                         
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
        
</section>
@endsection
<script>
    var userids;
    var groupids;   
    var emailtemps;
    
    /*function editEmail(val)
    {
        window.location.href = "/admin/send-emails/edit-email/"+val+"/"+json.stringify(userids);
    }*/
    
    function getNumOfUsersInGroup()
    {
        val = $('#user_group').val();
        if(groupids != val && groupids != '' && val != '')
        {
            groupids = val;       
            if(val != '')
            {
                var param = 'groupid='+val;
                getDataAjax('getPromotionUserInGroup', param);            
            }
        }
        setTimeout(function(){ getNumOfUsersInGroup(); }, 500);
    }
    
    
    function getAllDraftUsers()
    {
        getDataAjax('getAllDraftedUsers');           
    }
    
    function getAllAbsentUsers()
    {
        getDataAjax('getAllAbsentUsers');           
    }
    
    function getAllUsers()
    {
        getDataAjax('getAllUsers');           
    }
    
    function getDataAjax(geturl, param='')
    {
        $.ajax({
            url: '{{url('/')}}/'+geturl,
            dataType: 'json',
            method: 'get',
            data: param,
            success: function(response) {
               if(response[0].length != 0)
               {
                   $('#users-div').empty();
                   $('#users-div').append('<div class="col-md-12"><span style="font-size:x-large; font-style:italic">Your email will be sent to '+response[0].length+' people.</span></div> ')
                   $('#users-div').show();
                   userids = response[0];
                   $('#userids').val(userids.toString());
                   emailtemps = response[1];

                   $('#email-temp').empty();
                   $('#email-temp').append('<option value="">Select</option>');
                   for(var i=0; i<emailtemps.length; i++)
                   {
                      $('#email-temp').append('<option value="'+ emailtemps[i]['id'] +'">'+ emailtemps[i]['email_template_title'] +'</option>'); 
                   }
                   $('#emailtemp-div').show();
               }
               else
               {
                   $('#users-div').empty();
                   $('#users-div').append('<div class="col-md-12"><span style="font-size:x-large; font-style:italic">You have 0 users in this group.</span></div>');
                   $('#users-div').show();
               }                                                                       

            }
        })

    }
    
    function onChangeOption(val)
    {
        if(val == 'group')
        {
            $('#users-div').hide();
            $('#users-div').empty();
            $('#emailtemp-div').hide();
            $('#email-temp').empty();
            $('#group-div').show();
        }
        else if(val == 'drafts')
        {
            resetDiv();
            getAllDraftUsers();
        }
        else if(val == 'absent')
        {
            resetDiv();
            getAllAbsentUsers();
        }
        else if(val == 'all')
        {
            resetDiv();
            getAllUsers();
        }
        else if(val == '')
        {
            resetDiv();
        }
    }
    
    function resetDiv()
    {
        $('#group-div').hide();
        $('#selected').empty();
        $('#users-div').hide();
        $('#users-div').empty();
        $('#emailtemp-div').hide();
        $('#email-temp').empty();
    }
  
    function setSession() {
        alert('You don\'t have privileges to manage admin users');
    }
</script>
