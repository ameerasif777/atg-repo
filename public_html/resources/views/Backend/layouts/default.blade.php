@include('Backend.includes.header')
@include('Backend.includes.top-nav')
@include('Backend.includes.left-nav')

<aside class="right-side">   
        @yield('content')
        @include('Backend.includes.footer')
        @yield('footer') 