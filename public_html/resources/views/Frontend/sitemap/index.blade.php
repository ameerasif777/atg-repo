<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url('sitemap/events') }}</loc>
        <lastmod></lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/meetups') }}</loc>
        <lastmod></lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/education') }}</loc>
        <lastmod></lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/jobs') }}</loc>
        <lastmod></lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/connect') }}</loc>
        <lastmod></lastmod>
    </sitemap>
</sitemapindex>