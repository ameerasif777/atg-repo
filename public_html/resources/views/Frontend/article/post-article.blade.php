@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')

<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<!--<script src="{{url('/assets/Frontend/js/article/article.js')}}"></script>-->

@include('Frontend.includes.froala-editor', ['folderName' => 'article_image'])

<!-- CSS for placeholder text inside Article Description -->
<style type="text/css">
    [contentEditable=true]:empty:not(:focus):before{
        content:attr(data-text);
        color:#999;
    }
</style>

<section class="post-Meet-up-sec">
    <div class="container">
        <div class="text-feder-outer">
            <form name="form_post_article" id="form_post_article" method="post" action="{{url('/')}}/post-article" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" id="base_url" value="{{url('/')}}">
                <div class="rightwer">
                    <ul class="clearfix">
                        <li><a href="{{url('/')}}/question"><i class="icon-Question"></i> Ask Question</a></li>
                        <li><a href="{{url('/')}}/event"><i class="icon-Post"></i> Post Event</a></li>   
                        <li class="active"><a href="javascript:void(0)"><i class="icon-Post"></i> Post Article</a></li>
                        <li><a href="{{url('/')}}/meetup"><i class="icon-Post"></i> Post Meetup</a></li>
                        <li><a href="{{url('/')}}/education"><i class="icon-Post"></i> Post Education</a></li>
                        @if($arr_user_data['user_type'] != 1)
                        <li>
                            <a href="{{url('/')}}/post-job"><i class="icon-Post"></i> Post Job</a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="fealider-area">
                    <div class="title-input">
                        <input type="text" class="form-control" id="title"  name="title" placeholder="Title" value="{{old('title')}}">
                        <input type="hidden" class="form-control" id="old_title"  name="old_title" placeholder="Title" value="">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <textarea class="form-control" name="description"  id="description" placeholder="Article Description" value="{{old('description')}}" style="display: none"></textarea>                                
                            @if ($errors->has('description'))
                            <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                            <div class="error" for="description"></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="uploar-imger">                                
                                <div class="uping-imgs uplode-img-cam">
                                    <input type="file" name='article_pic' onchange="readURL(this);"  id="article_pic" class="upit-img" title="Upload Image" />
                                </div>
                                <div for="profile_pic" class="error"></div>
                                <div class="note_msg">
                                    <strong>Upload a cover image for your post.</strong><br>
                                    jpeg, jpg, png, bmp, max size : 2 Mb. <br>
                                    <strong>(Rec Dimensions: 1200 X 400px)</strong>
                                </div>
                                <div class="title-input uplink-imgs">
                                    <img class="uploding-img-vie" style="display: none" id="blah" src="#" alt="" width="100%" height="200" /><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="title-sl-input">
                            <label for="">TAGS</label>
                            <input type="text" class="form-control" id="tags" name="tags" placeholder="Type relevant tag(s). Press enter or comma after each tag to separate them." value="{{old('tags')}}">
                        </div>
                        <div for="tags" class="error"></div><br/>
                        <div class="title-sl-input">
                            <label for="">GROUP</label>
                            <select id="sub_groups" name="sub_groups[]" multiple="" class="form-control multipleSelect" placeholder="Click here to share your post in relevant group." value="{{old('sub_groups[]')}}" size="{{count($arr_users_group)}}">

                                @if(isset($group_id) && $group_id != '')
                                @foreach($arr_users_group as $key => $value)
                                @if($value->id == $group_id) 
                                <option value="{{$group_id}}" selected>{{$value->group_name}}</option>
                                @endif
                                @endforeach
                                @else
                                @foreach($arr_users_group as $key => $value)
                                <option value="{{$value->id}}">{{$value->group_name}}</option>
                                @endforeach
                                @endif

                            </select>                        
                            <div for="sub_groups[]" class="error"></div>
                        </div>

                        <div class="boter-texter-feat clearfix">
                            <div class="pull-left" id="save_loader" style="display:none;"><img height="25px" width="25px" src="{{ url('/public/assets/Frontend/img/loading1.gif') }}"> <b>Your post is saving...</b></div>
                            <div class="pull-right">         
                                
                                <input type="hidden" id="last_added_article_id" name="id">
                                <input type="hidden" id="table" name="table" value="mst_article">
                                <input type="hidden" id="btn_post" name="btn_post" value="">
                                <input type="hidden" id="cover_type" name="cover_type" value="">
                                <input type="hidden" id="cover_url" name="cover_url" value="">
                                <button type="submit"  class="btn cancel" onclick="$('#btn_post').val('1'); getDescription()">Save & Close</button>
                                <button type="submit" class="btn" onclick="$('#btn_post').val('0'); getDescription();" onmouseover="findimginpost();">Post</button>                  
                            </div>
                        </div>
                    </div>                
            </form>
        </div>
    </div>
</div>
<div id="dialog" title="Enter height and width" style="display: none">
    <form name="frm_content_image" id="frm_content_image" action="" method="POST" enctype="multipart/form-data">
        <fieldset>
            <input type="hidden" name="old_image" id="old_image" value="">
            <input type="hidden" id="auto_save_flag" name="auto_save_flag" value="false">
            <input type="text" name="image_width" id="image_width" value="" class="form-control offset-bot-15" placeholder="Width">
            <input type="text" name="image_height" id="image_height" value="" class="form-control offset-bot-15" placeholder="Height">
            <input type="file" id="content_img" style="display: none" name="content_img" onchange="uploadImage(this)">
            <button type="button" class="btn btn-secondary pull-right" onclick="uploadContentImage() $('#auto_save_flag').val('true');">Submit</button>
        </fieldset>
    </form>
</div>
</section>
@include('Frontend.includes.footer')    

<style>
    .grey-overlay {
        background: rgba(0,0,0,0.7);
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 9998;
    }
</style>
<!-- This div for loader -->
<!-- <div class="grey-overlay" style="display:none;text-align: center;padding-top: 250px;"><img width="150" height="150" src="{{ url('/public/assets/Frontend/img/loading.gif') }}"></div> -->

<script src="{{url('/assets/Frontend/js/imgvideoaudioext.js')}}"></script>
<script type="text/javascript">
$(document).ready(function (e) {
    $("#form_post_article").validate({
        errorElement: 'div',
        ignore: '.hidden',
        rules: {
            title: {
                required: true
            },
            post_short_description: {
                required: true,
                maxlength: 200
            },
            description: {
                required: true
            },
        },
        messages: {
            title: {
                required: "Please enter article title."
            },
            post_short_description: {
                required: "Please enter article title.",
                maxlength: "Please enter maximum 200 characters."
            },
            description: {
                required: "Please enter article description."
            },
            submitHandler: function(form) {
                $("#btn_post").hide();
            }
        },        
    });   
});
    //This code for custom ckeditor
    function getDescription() {
        $('#description').val($('textarea#description').froalaEditor('html.get'));
    }

    jQuery(document).ready(function () {
        $('.multipleSelect').popSelect({
            showTitle: false,
            autoIncrease: true,
            maxAllowed: 5,
            position: 'top',
            placeholderText: 'Click here to share your post in relevant group.',
            note: 'Select a group to share with the community. <br>Do not select any group to share only with connections and followers.'
        });

        $("#frm_content_image").validate({
            ignore: '.hidden',
            errorElement: 'div',
            rules: {
                image_width: {
                    digits: true,
                    max: 1200
                },
                image_height: {
                    digits: true,
                    max: 1200
                },
            }
        });
    });

    $(function () {
        $('#tags').tokenfield({
            autocomplete: {
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
    });

    function readURL(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                        $('#remove_image').css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Please upload image in jpeg, jpg or png format. Maximum file size : 2 Mb. (Optional: Recommended Dimensions : 1200 X 700px)');
                input.value = '';
        }
    }

    function readImage(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah1').css("display", "block");
                        $('#blah2').css("display", "block");
                        $('#blah3').css("display", "block");
                        $('#blah1').attr('src', e.target.result);
                        $('#remove_img').css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Only Image allowed !');
                input.value = '';
        }
    }
    var url = '';
    if ($('#old_title').val() != "") {
        url = "{{url('/')}}/check-article-title-edit";
    } else {
        url = "{{url('/')}}/check-article-title";

    }
   
  
    function removeimage() {
        $('#blah').css("display", "none");
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#article_pic").prop("value", "");
    }
    function removeimg() {
        $('#blah1').css("display", "none");
        $('#blah2').css("display", "none");
        $('#blah3').css("display", "none");
        $('#remove_img').css("display", "none");
        $("#profile_image").prop("value", "");
    }
    function show()
    {
        $("#enddatediv").css("display", "block");
        $("#add_link").css("display", "none");
    }
    function hide()
    {
        $("#enddatediv").css("display", "none");
        $("#add_link").css("display", "block");
    }



    

    //This code for custom ckeditor
    function strip_tags(input, allowed) {

        if (input == undefined) {
            return '';
        }
        allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
    }
    //Ends here
  


    function loadajax() {
        var id = $("#last_added_article_id").val();
        console.log('id', id)
        
        var sub_groups = $("#sub_groups").val();

        $.ajax({
            url: $("#base_url").val() + "/post-article",
            method: 'post',
            
            data: {id: id, sub_groups: sub_groups, title: $("#title").val(), description: $('textarea#description').froalaEditor('html.get'), tags: $("#tags").val(), btn_post: '1', auto_save_flag: "true",cover_type: $('#cover_type').val(),cover_url:$('#cover_url').val()},
            success: function(data) {
                console.log('bef',data)
                $("#last_added_article_id").val("");

                $("#last_added_article_id").prop("value",data);
                console.log('aft', $("#last_added_article_id").val())
                
                $("#save_loader").show();
                setInterval(function() {
                    $("#save_loader").hide();
                }, 4000);            }
        });
    }

    setInterval(function () {
        var title = $("#title").val();
        var desc = $('textarea#description').froalaEditor('html.get');
        var tags = $("#tags").val();


        var validate = false;
        var auto_save = $("#auto_save_flag").val();
        $('#form_post_article input[type=text]').each(function () {
            if ($(this).val() != '')
                validate = true;
        });

        if (validate == true && auto_save == 'false') {
            loadajax();
        }

    }, 10000);

   
    
    //Ends here
</script>