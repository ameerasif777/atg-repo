@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')

<script src="{{ url('/assets/Frontend/js/underscore-min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>

@include('Frontend.includes.froala-editor', ['folderName' => 'article_image'])

<script>
$(document).ready(function () {
    $('.multipleSelect').popSelect({
        showTitle: false,
        autoIncrease: true,
        maxAllowed: 5,
        placeholderText: 'Please select the groups',
        position: 'top',
        note: 'Select a group to share with the community. <br>Do not select any group to share only with connections and followers.'
    });
});
</script>

<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/article_back_edit.jpg') }});">
    <div class="container">
        <h1>Edit your article</h1>
    </div>
</section>
<section class="post-Meet-up-sec">
    <div class="container">
        <div class="text-feder-outer">
            
            {!! csrf_field() !!}
            <form id="post_edit_article" name="post_edit_article" method="POST" action="{{url('/')}}/front-edit-article/{{ base64_encode($arr_article[0]->id) }}"  enctype="multipart/form-data">
                <div class="fealider-area">
                    <div class="title-input">
                        <input type="text" class="form-control" placeholder="Title" id="title" name="title" value="{{ (old('title'))?old('title'):$arr_article[0]->title }}">
                        <input type="hidden" class="form-control" id="old_title" name="old_title" value="{!! $arr_article[0]->title !!}">
                        <input type="hidden" class="form-control" id="btnPost" name="btnPost" value="0">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="title-input1">

                                <textarea class="form-control" name="description" id="description" value="" style="display: none">{!! old('description',$arr_article[0]->description) !!}</textarea>
                                @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="uploar-imger">
                                <div class="uping-imgs uplode-img-cam">
                                    <input type="file" name='article_pic' onchange="readURL(this);"  id="profile_image" class="upit-img " title="Upload Image" />

                                </div>
                                <div for="profile_pic" class="error"></div>
                                <div class="note_msg">
                                    <strong>Upload a cover image for your post.</strong><br>
                                    jpeg, jpg, png, bmp, max size : 2 Mb. <br>
                                    <strong>(Rec Dimensions: 1200 X 400px)</strong>
                                </div>
                                @if($arr_article[0]->profile_image == '')
                                <div id="cross-icon-1" >
                                    <img class="uploding-img-vie" style="" id="blah" src=""  alt=""  width="100%" height="200"/>
                                    <a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image-1" title="Remove">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                                @else
                                <div class="title-input uplink-imgs">
                                    @if($arr_article[0]->cover_type==0)
<?php $post_image_path = config('feature_pic_url').'article_image/thumb/' . trim($arr_article[0]->profile_image); ?>
                                    <img class="uploding-img-vie" style="" id="blah" src="{{ $post_image_path  }}"  alt=""  width="100%" height="200"/>                                    
                                    <a href="javascript:void(0)"  onclick="removeimage();" id="remove_image" title="Remove">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                    @endif
                                </div>

                                @endif


                                <input type="hidden" name='last_profile_pic' id="last_profile_pic" class="upit-img" value="{{trim($arr_article[0]->profile_image)}}"/>
                            </div>
                        </div>


                    </div>    
                    <div class="row">                        
                        <div class="title-sl-input">
                            <label for="">TAGS</label>
                            <input type="text" class="form-control" id="tags" name="tags" value="{{ $arr_article[0]->tags }}">

                        </div></br>
                        <div for="tags" class="error"></div>
                        <div class="title-sl-input">
                            <label for="">GROUP</label>
                            <div class="form-group">
                                <select name="sub_groups[]" id="sub_groups" multiple="" class="form-control multipleSelect" size="{{count($user_group_data)}}">
                                    @foreach($user_group_data as $key=>$value)
                                    <option value="{!! $value->id !!}" @if(in_array($value->id,$arr_selected_groups)) selected @endif >{!! $value->group_name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="boter-texter-feat clearfix">
                            <div class="pull-right">

                                <input type="hidden" id="article_id" value="{{$arr_article[0]->id}}">
                                <input type="hidden" id="btn_post" name="btn_post" value="1">
                                <input type="hidden" id="auto_save_flag" name="auto_save_flag" value="true">
                                <button type="submit" class="btn" onclick="$('#btn_post').val('0'); getDescription(); $('#auto_save_flag').val('false'); " onmouseover="findimginpost()">@if($arr_article[0]->status == '1')Post @else Publish @endif</button>
                                <a href="{{ url('/') }}/mypost"  class="btn">Cancel</a>
                            </div>
                        </div>
                    </div>                   
                </div>
                
                <input type="hidden" id="cover_type" name="cover_type" value="">
                <input type="hidden" id="cover_url" name="cover_url" value="">
            </form>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Frontend/js/imgvideoaudioext.js')}}"></script>
<script>
    $('#tags').tokenfield({
        autocomplete: {
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    function getDescription() {
        $('#description').val($('textarea#description').froalaEditor('html.get'));
    }
    jQuery(document).ready(function () {
        $("#frm_content_image").validate({
            ignore: '.hidden',
            errorElement: 'div',
            rules: {
                image_width: {
                    digits: true,
                    max: 1200
                },
                image_height: {
                    digits: true,
                    max: 1200
                },
            }
        });

        $("#post_edit_article").validate({
            errorElement: 'div',
            ignore: '.hidden',
            rules: {
                title: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                title: {
                    required: "Please enter title.",
                },
                description: {
                    required: "Please enter article description.",
                },
                submitHandler: function (form) {
                    $("#btn_post").hide();
                }
            },
        });
    });

    function readURL(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').css("display", "block");
                        $('.uping-imgs').css("display", "none");
                        $('.note_msg').css("display", "none");
                        $('#blah').attr('src', e.target.result);
                        $('#remove_image').css("display", "block");
                        $('#remove_image-1').css("display", "block");
                        $('#cross-icon-1').addClass("title-input uplink-imgs");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }

    }
    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#profile_pic").prop("value", "");
        $("#last_profile_pic").prop("value", "");
        $('#remove_image-1').css("display", "none");
        $('.uping-imgs').css("display", "block");
        $('.note_msg').css("display", "block");
    }

    function readImage(input) {
        var index = $(input).data("index");
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext)
        {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#guest_img_div' + index).css("display", "block");
                        $('#guest_image_src' + index).attr('src', e.target.result);
                        $('#remove_img' + index).css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Only jpeg, jpg, png extensions are supported. Maximum file size : 2 Mb. (Recommended Dimensions is:1200 X 700px)..!');
                input.value = '';
        }
    }


    function loadajax() {
        var id = $("#article_id").val();
        var sub_groups = $("#sub_groups").val();
        $.ajax({
            url: $("#base_url").val() + "/front-edit-article/" + id,
            method: 'post',
            async:false,
            data: {id: id, sub_groups: sub_groups, title: $("#title").val(), description: $('textarea#description').froalaEditor('html.get'), tags: $("#tags").val(), btn_post: '1', auto_save_flag: "true",cover_type: $('#cover_type').val(),cover_url:$('#cover_url').val()},
            success: function (id) {
                 $("#save_loader").show();
                setInterval(function() {
                    $("#save_loader").hide();
                }, 4000);
            }
        });
    }

    setInterval(function () {

        var validate = false;
        var auto_save = $("#auto_save_flag").val();
        $('#post_edit_article input[type=text]').each(function () {
            if ($(this).val() != '')
                validate = true;
        });
        if (validate == true && auto_save == 'true') {
           loadajax();
        }

    }, 10000);
    //Ends here
</script>
