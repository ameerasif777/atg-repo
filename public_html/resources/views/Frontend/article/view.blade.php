<?php $session_data = Session::get('user_account');
$session_data = Session::get('user_account');
$image_path = config('feature_image_path').'article_image/thumb/'.$articleData[0]->profile_image;
if (Storage::exists($image_path) && $articleData[0]->profile_image != '') {
    $post_image_path = config('feature_pic_url').'article_image/thumb/'.$articleData[0]->profile_image;
} else {
    $post_image_path =config('img').'image-not-found3.jpg';
}
$meta_desc = fixtags($finalData['header']['meta_description']);

function fixtags($text){
$text = htmlspecialchars($text);
$text = preg_replace("/=/", "=\"\"", $text);
$text = preg_replace("/&quot;/", "&quot;\"", $text);
$tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
$text = preg_replace($tags, $replacement, $text);
$text = preg_replace("/=\"\"/", "=", $text);
return $text;
}

$strHead = '<script type="application/ld+json">';
$strHead .='[{';
$strHead .= '"@context": "http://schema.org",';
$strHead .= '"@type": "Article",';
$strHead .= '"name": "' . preg_replace('/[^A-Za-z0-9\s]/', '', $articleData[0]->title) . '",';
$strHead .= '"author": "' . $arr_user1_data['first_name'] . ' ' . $arr_user1_data['last_name'] . '",';
$strHead .= '"@type": "Article",';
$strHead .= '"publisher": "' . $arr_user1_data['first_name'] . ' ' . $arr_user1_data['last_name']  . '",';
$strHead .= '"datePublished": "' . $articleData[0]->created_at . '",';
$strHead .= '"headline": "' . preg_replace('/[^A-Za-z0-9\s]/', '', $articleData[0]->title) . '",';
$strHead .= '"url": "' . url('/') . '/view-article/' . preg_replace('/[^A-Za-z0-9\s]/', '', $articleData[0]->title) . '-' . $articleData[0]->id . '"';
$strHead .= '}]';
$strHead .= '</script>';
?>
@include('Frontend.includes.head')
@if($session_data != "")
@include('Frontend.includes.header')
@else
@include('Frontend.includes.outer-header',['sign_up'=>true])
@endif
@include('Frontend.includes.session-message')

@if($session_data == '')
    @include('Frontend.includes.fixed_signup_footerbutton',['get_groups'=>$get_groups])
@endif

<meta property="og:title" content="{{$finalData['header']['meta_title']}}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{Request::fullUrl()}}">
<meta property="og:tags" content='{{$finalData["header"]["meta_keywords"]}}'>
<meta name="keywords" content="@if(isset($articleData[0]->tags) && $articleData[0]->tags != ''){{$articleData[0]->tags}} @else {{$articleData[0]->title}} @endif">
<meta name="description" content="{{$finalData['header']['meta_title']}}">
<meta property="og:image" content="{{$post_image_path}}">
<link href="{{ url('/public/assets/css/views/post_detail.css') }}" rel="stylesheet" type="text/css"/>
@include('Frontend.includes.post_detail_mention_user_ids_top')
<section class="contact-sec" style="background-image:url({{config('img').'article_back_edit.jpg'}});">
<?php  $title_of_post=$articleData[0]->title?>
@include('Frontend.includes.post_detail_title', [ 'feature' => "article", 'title' => $title_of_post ])
</section>
<style type="text/css">
    .btn-group > .btn:first-child {
        border-bottom-left-radius: 1px solid #33bb87;
        border-top-left-radius: 1px solid #33bb87;
    }
    .btn-group > .btn:last-child {
        border-bottom-right-radius: 1px solid #33bb87;
        border-top-right-radius: 1px solid #33bb87;
    }
    .dow-sav-like-btn .btn {
        font-size: 10px;
    }
</style>
<section class="artical-post-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">

                @include('Frontend.includes.google_ads', ['ads_public_only' => 1])
                <p style="font-size: medium; color: red; font-style: italic">
                    @if($response == '2')
                            <b>&nbsp;*Your Article has been promoted successfully !</b>
                    @elseif($response == '3')
                            <b>&nbsp;*Your payment has failed for some reason. Please try again to promote.</b>
                    @endif
                </p>
                <div class="artical-leftyy">
                    <div class="art-bane">
                        <?php
                            if($articleData[0]->cover_type==0){
                                $post_comment = config('feature_pic_url').'add-comment.png';
                                $image_path = config('feature_image_path').'article_image/' . trim($articleData[0]->profile_image);
                                if (Storage::exists($image_path) && $articleData[0]->profile_image != '') {
                                    $post_image_path = config('feature_pic_url').'article_image/' . trim($articleData[0]->profile_image);
                                } else {
                                    // $post_image_path = config('img').'image-not-found3.jpg';
                                    $post_image_path = '';
                                }
                            }
                       ?>
                        @if($post_image_path != '' && $articleData[0]->cover_type==0)
                        <img width="100%" src="{{$post_image_path}}">
                        @endif
                    </div>
                    <p>{!!ucfirst($articleData[0]->description)!!}</p>
                </div>
                <input type="hidden" name="to_id_fk" id="to_id_fk" value="{{$articleData[0]->user_id_fk }}">
                <input type="hidden" name="article_id" id="article_id" value="{{$articleData[0]->id}}">
                <div class="editable-shareable">

                        @include('Frontend.includes.post_detail_tags_edit',['tags'=>'tags','feature'=>'article'])

                    @if(count($get_groups) != 0)
                    <div class="boter-hash-taber">
                        <span>Posted IN :</span>                        
                        <ul class="clearfix">
                            <?php $temp_grp_array = array(); ?>
                            @foreach($get_groups as $group)                                                
                            @if($session_data != "")

                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                            <?php
                            $temp_grp_array[] = $group->id;
                            ?>
                            <li><span class="cust-posterd" style="cursor:pointer;" title='Click to Report' class="btn repot">{{ $group->group_name }}<a onclick="getGroupName('{{$group->id}}')"> � </a></span></li>                                                   
                            @else
                            <li><span class="cust-posterd" style="cursor:pointer;" class="btn repot">{{ $group->group_name }}<a onclick="getGroupName('{{$group->id}}')"> � </a></span></li>                                                   
                            @endif
                            @else
                            <li>
                                <span class="cust-posterd" style="cursor:pointer;" href="javascript:void(0)" title='Click to Report' class="btn repot">{{ $group->group_name }}<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"> � </a></span>
                            </li>  
                            @endif
                            @endforeach
                            <?php
                            $tt = implode(',', $temp_grp_array);
                            ?>
                            <input type="hidden" id="group_ids" name="group_ids[]" value="{{$tt}}">
                        </ul>
                    </div>
                    @endif
                    @if($arr_user1_data['id'] != $arr_user_data['id'])
                    <div class="modal fade"  id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog"  role="document">
                            <div class="modal-content report-pop">
                                <form name="group_report" id="group_report">
                                    <div class="modal-body">
                                        <div class="report-infos-cont">
                                            <h1>Report</h1>
                                            <ul>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="0"> 
                                                    <label>Spam</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="1">
                                                    <label>Unintresting</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="2">
                                                    <label>Hatred or Vulgarity</label>
                                                </li>
                                                @if(count($get_groups) > 0)
                                                <li>
                                                    <input type="radio" name="report" id="make_group" value="">
                                                    <label>Does not belong in</label>
                                                    <select name="sub_group" id="make_sub_group" disabled="" class="form-control">
                                                        @foreach($get_groups as $group)
                                                        <option value="{{$group->id}}">{{$group->group_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </li>
                                                @endif
                                                <label class="error" for="report"></label>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="boter-texter-feat modal-footer">
                                        <div class="pull-right">
                                            <button type="button" id="group_report_ok" class="btn" onclick="makeGroupReport({{ $articleData[0]->id }})" >Post</button>
                                            <button type="button" class="btn" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                @include('Frontend.includes.google_ads', ['ads_public_only' => 1])

                <div class="arti-coment-div">
                    <h3 class="clearfix">Comments
                    </h3>
                    <h4 class="clearfix">
                        <span>{{{isset($arr_article[0]->count)?$arr_article[0]->count:'0' }}}</span> comments
                    </h4>
                    <div class="post-arti-comt">
                        <div class="media">
                            <form class="frm_comment" name="frm_comment" id="frm_comments" method="post" action="{{ url('/') }}/article-comment" enctype="multipart/form-data">
                                <div class="media-left">
                                    <div class="post-img">
                                        <?php
                                        $user_profile_image = config('profile_path'). $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                                        if (Storage::exists($user_profile_image) && $arr_user_data['profile_picture'] != '') {
                                            $profile_picture =config('profile_pic_url') . $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                                        } else {
                                            if ($arr_user_data['gender'] == 0) {
                                                $profile_picture = config('avatar_male');
                                            } else {
                                                $profile_picture = config('avatar_female');
                                            }
                                        }
                                        ?>
                                        @if($session_data != "")
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user_data['id'])}}" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{$profile_picture}}" alt="profile"/>
                                        </a>
                                        @else
                                        <a class="more" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{ $profile_picture }}" alt="profile"/>
                                        </a>
                                        @endif
                                    </div> 
                                </div>

                                <div class="media-body" >
                                    <div class="input-group">

                                        <textarea class="form-control mention" name="comment" id="comment" placeholder="comment here..." style="height: 55px;">{{old('comment')}}</textarea>                                        
                                        <input type="hidden" name="user_id" value="{{$finalData['user_session']['id'] }}">
                                        <input type="hidden" name="post_article_id" value="{{ucfirst($arr_article[0]->id)}}">
                                        <input type="hidden" name="post_article_title" value="{{ucfirst($arr_article[0]->title)}}">
                                        <span class="input-group-btn" style="vertical-align: top;"> 
                                            @if($session_data != "")
                                                <button type="submit" class="btn btn-default" name="btn_comment" value="POST COMMENT">
                                            @else
                                                <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-default"  value="POST COMMENT">
                                            @endif
                                            <i class='fa fa-paper-plane' aria-hidden='true' style='font-size:25px;'></i>
                                            </button>
                                        </span> 
                                    </div>                                    
                                    <div class="comment-pic">
                                        @if($session_data != "")
                                        <input type="file" name='commnet_pic' onchange="readURL(this);"  id="commnet_pic" class="upit-imgse" title="Upload Image" />
                                        @else
                                        <input name='commnet_pic' data-toggle="modal" data-target="#myModal"  class="upit-imgse" title="Upload Image" />
                                        @endif
                                    </div>
                                    @if($errors->has('comment'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{$errors->first('comment')}}</strong>
                                    </span>
                                    @endif
                                    <label for="comment" class="error"></label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="posted-arti-comment mCustomScrollbar">
                        @if(isset($arr_article[0]->comment) ? $arr_article[0]->comment: '')
                        @foreach ($arr_article[0]->comment as $value)
                       
                        @include('Frontend.includes.comments')
                                
                                @if($value->comment_image != '')
                                <?php
                                $chk_comment_image = config('feature_image_path').'article_comment_images/' . $value->comment_image;
                                if (Storage::exists($chk_comment_image) && $value->comment_image != '') {
                                    $comment_image = config('feature_pic_url').'article_comment_images/' . $value->comment_image;
                                } else {
                                    $comment_image = config('img').'image-not-found.png';
                                }
                                ?>
                                <div class="comment-img">
                                    <a  href="{{config('feature_pic_url')}}article_comment_images/{{$value->comment_image}}"  download>Attachment(s)</a>
                                    <div class="img-hover">
                                        <img src="{{$comment_image}}"/>
                                    </div>
                                </div>
                                @endif
                                <div id="rep_d{{$value->id}}" style="display: none;" >
                                </div>
                                @if(isset($value->replies[0]) && count($value->replies[0])>0)
                                <div id="rep{{$value->id}}" style="display: none;" >
                                    @if(isset($value->replies))
                                    @foreach($value->replies as $r_key => $all_replies)
                                    @if(isset($all_replies->id))
                                    <div class="media">
                                        <div class="media-left" id="{{ $all_replies->id }}">
                                            <?php
                                            $user_profile_image = config('profile_path'). $all_replies->commented_by . '/thumb/' . $all_replies->profile_picture;
                                            if (Storage::exists($user_profile_image) && $all_replies->profile_picture != '') {
                                                $profile_picture =config('profile_pic_url') . $all_replies->commented_by . '/thumb/' . $all_replies->profile_picture;
                                            } else {
                                                $profile_picture = config('avatar_male');
                                            }
                                            ?>
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($all_replies->commented_by)}}" title="Visit Page" data-placement="left">
                                                <div class="post-ari-img"><img src="{{$profile_picture}}"/></div>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($all_replies->commented_by)}}" title="Visit Page" data-placement="left">
                                                <h4 class="media-heading">{{ $all_replies->user_name }} - <span class="sl-italic">On {{  date("l,jS F Y", strtotime($all_replies->created_at)) }}</span> <span class="pull-right"></span></h4>
                                            </a>
                                            <p>{{ $all_replies->comment }}</p>
                                            <div class="replies-count">
                                                <a href="javascript:void(0);" onclick="postReplyR('{{$all_replies->id}}','{{$value->id}}', '{{$post_comment}}')">Reply</a>
                                                <a href="javascript:void(0);"><i class="fa fa-comment" aria-hidden="true" onclick="repliesR('{{$all_replies->id}}')"></i>
                                                    {{(isset($all_replies->replied)&& count($all_replies->replied)) ? count($all_replies->replied[0]) : '0'}}
                                                </a>
                                                @if($all_replies->comment_image != '')
                                                <?php
                                                $chk_comment_image = config('feature_image_path').'article_comment_images/' . $all_replies->comment_image;
                                                if (Storage::exists($chk_comment_image) && $all_replies->comment_image != '') {
                                                    $comment_image = config('feature_pic_url').'article_comment_images/thumb/' . $all_replies->comment_image;
                                                } else {
                                                    $comment_image = config('img').'image-not-found.png';
                                                }
                                                ?>
                                                <div class="comment-img">
                                                    <a  href="{{config('feature_pic_url')}}article_comment_images/thumb/{{$all_replies->comment_image}}"  download>Attachment(s)</a>
                                                    <div class="img-hover">
                                                        <img src="{{$comment_image}}"/>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div id="repliedR_div{{$all_replies->id}}" class="" style="display: none;"></div>
                                        </div>
                                        <div id="reply_div{{$all_replies->id}}" class="" style="display: none;" >
                                            @if(isset($all_replies->replied))
                                            @foreach($all_replies->replied as $rep_replieds)
                                            @foreach($rep_replieds as $rep_replied) 
                                            <div class="media">
                                                <div class="media-left" id="{{$rep_replied->id}}">
                                                    <?php
                                                    $user_profile_image = config('profile_path'). $rep_replied->commented_by . '/thumb/' . $rep_replied->profile_picture;
                                                    if (Storage::exists($user_profile_image) && $rep_replied->profile_picture != '') {
                                                        $profile_picture =config('profile_pic_url') . $rep_replied->commented_by . '/thumb/' . $rep_replied->profile_picture;
                                                    } else {
                                                        $profile_picture = config('avatar_male');
                                                    }
                                                    ?>
                                                    <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($rep_replied->commented_by)}}" title="Visit Page" data-placement="left">
                                                        <div class="post-ari-img"></span><img src="{{$profile_picture}}"/></div>
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($rep_replied->commented_by)}}" title="Visit Page" data-placement="left">
                                                        <h4 class="media-heading">{{ $rep_replied->user_name }} - <span class="sl-italic">On {{  date("l,jS F Y", strtotime($rep_replied->created_at)) }}</span> <span class="pull-right"></span></h4>
                                                    </a>
                                                    <p>{!! $rep_replied->comment !!}</p>
                                                    <div class="replies-count">
                                                        <a href="javascript:void(0);" onclick="postReplyRe('{!!$rep_replied->id!!}', '{!!$all_replies->id!!}', '{!!$value->id!!}', '{{$post_comment}}')">Reply</a>
                                                        @if($rep_replied->comment_image != '')
                                                        <?php
                                                        $chk_comment_image = config('feature_image_path').'article_comment_images/' . $rep_replied->comment_image;
                                                        if (Storage::exists($chk_comment_image) && $rep_replied->comment_image != '') {
                                                            $comment_image = config('feature_pic_url').'article_comment_images/thumb/' . $rep_replied->comment_image;
                                                        } else {
                                                            $comment_image = config('img').'image-not-found.png';
                                                        }
                                                        ?>
                                                        <div class="comment-img">
                                                            <a  href="{{config('feature_pic_url')}}article_comment_images/thumb/{{$rep_replied->comment_image}}"  download>Attachment(s)</a>
                                                            <div class="img-hover">
                                                                <img src="{{$comment_image}}" />
                                                                <div class="img-hover">
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div id="repliedR_div{{$rep_replied->id}}" class="" style="display: none;" ></div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">

                        @include('Frontend.includes.google_ads', ['ads_public_only' => 1])
                        <div class="right-aticalss">
                            <!-- User Profile Image , Follow Section -->
                            @include('Frontend.includes.post_detail_user_profile_image', [ 'feature' => "article", 'time' => $articleData[0]->created_at ])
                            
                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                            <div class="dow-sav-like-btn">
                                <div class="btn-group" role="group" aria-label="..." id="up-down-votes">                                        

                                    @if($session_data != "")
                                        <button id="like" name="like" class="btn {{(isset($is_upvotes) && count($is_upvotes)>0 && $is_upvotes[0]->status=='0') ? 'acti-up-dw disabled' : ''}}"  onclick="upvoteDownvote('{!!$articleData[0]->id!!}', 0, 1);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters">{{(isset($total_upvotes)) ? $total_upvotes : '0'}}</span></button>
                                        <button id="unlike" name="attend" class="btn {{(isset($is_upvotes) && count($is_upvotes)>0 && $is_upvotes[0]->status=='1') ? 'acti-up-dw disabled' : ''}}" onclick="upvoteDownvote('{!!$articleData[0]->id!!}', 1, 2);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters">{{(isset($total_downvotes)) ? $total_downvotes : '0'}}</span></button>

                                        @if(isset($is_postfollower) && count($is_postfollower)>0 && $is_postfollower[0]->status=='0')
                                        <button id="followPost" name="followPost" class="btn {{(isset($is_postfollower) && count($is_postfollower)>0 && 
                                                $is_postfollower[0]->status=='0') ? '' : ''}}" onclick = "addFollowPost('{!!$articleData[0]->id!!}', 1,
                                                            2);"><i class="fa fa-foursquare"></i> Unfollow <span class="btn-counters">{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></button>
                                        @else
                                        <button id="followPost" name="followPost" class="btn {{(isset($is_postfollower) && count($is_postfollower)>0 && 
                                                $is_postfollower[0]->status=='1') ? '' : ''}}" 
                                                onclick = "addFollowPost('{!!$articleData[0]->id!!}', 0, 1);"><i class="fa fa-foursquare"></i> Follow <span class="btn-counters">{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></button>
                                        @endif

                                    @else
                                        <button class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters">{{(isset($total_upvotes)) ? $total_upvotes : '0'}}</span></button>
                                        <button class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters">{{(isset($total_downvotes)) ? $total_downvotes : '0'}}</span></button>

                                        @if(isset($is_postfollower) && count($is_postfollower)>0 && $is_postfollower[0]->status=='0')
                                        <a  class="btn btnPost" href='javascript:void(0)' data-toggle="modal" data-target="#myModal" title="Click to follow"><i class="fa fa-foursquare"></i> follow <span class="sas-counter btn-counters follower_count" id='follower_count'>{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></a>
                                        @else
                                        <a  class="btn btnPost" href='javascript:void(0)' 
                                                                      data-toggle="modal" data-target="#myModal" title="Click to 
                                                                      unfollow"><i class="fa fa-foursquare"></i> follow <span class="sas-counter btn-counters follower_count" id='follower_count'>{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></a>
                                        @endif

                                    @endif                                                                    

                                </div>
                                <input type="hidden" name="follower_user_id" value="{{$arr_user1_data['id']}}" id="follower_user_id">     
                            </div>
                            @endif 
				<!-- PROMOTE POST -->
                                @if($arr_user1_data['id'] == $arr_user_data['id'] && $arr_article[0]->status == '1')
                                    @if(count($promotion)>0)

                                            <div class="dow-sav-like-btn"> 
                                                <button  class="btn" style="width:100%; font-size: medium; white-space: normal;" ><i class="fa fa-bullhorn fa-lg"></i>&nbsp;POST Promoted From {{$promotion[0]->promo_start_date}} To {{$promotion[0]->promo_end_date}}</button>
                                            </div>

                                    
                                        <div class="arti-share-btns">
                                            <div class="dow-sav-like-btn"> 
                                                <h2>Promotion Data:</h2>
                                                <table border="0" width="100%">
                                                    <tr><td align="center" width="60%"><p>Number of Views : </p></td><td align="center" width="40%"><p><span id="views"></span></p></td></tr>
                                                    <tr><td align="center" ><p>Number of Clicks : </p></td><td align="center" ><p><span id="clicks"></span></p></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    @else
                                        
                                            <div class="dow-sav-like-btn"> 
                                                <button  class="btn" style="width:100%; font-size: medium; white-space: normal"  data-toggle="modal" data-target="#promote-modal"><i class="fa fa-bullhorn fa-lg"></i>&nbsp;PROMOTE YOUR POST</button>
                                            </div>
                                        
                                    
                                
                                <div id="promote-modal" style="display:none;" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              
                                    <div class="modal-dialog"  role="document">
                                        <div class="modal-content report-pop">
                                            <form  name="promption-frm" id="promption-frm" action="{{url('/')}}/order/promo" method="POST" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="report-infos-cont"> 
                                                    <h1>PROMOTE YOUR POST</h1>
                                                    <div style="padding:10px">
                                                            <div class="even-det">
                                                                <span class="help-block">Get more people to view your post..</span>
                                                            </div>
                                                            <div class="clearfix"></div>  <br/>    
                                                            <div class="even-det">
                                                                <div class="col-md-6">
                                                                    <label for="">Select Budget: </label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <select id="budget" name="budget" class="form-control offset-top-15" onchange="onSelectPromotion($(this).val());">
                                                                        <option value=''>--Select--</option>
                                                                        @foreach($promotion_det as $key => $value)
                                                                        <option value='{{$value->budget}}'>{{$value->currency}} {{$value->budget}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div><br/>   
                                                            <div class="even-det">
                                                                <div class="col-md-6">
                                                                    <label for="">Currency: </label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <select class="form-control offset-top-15" id='currency' name='currency'>
                                                                        <option val='INR' selected>INR</option>
                                                                    </select>
                                                                </div>
                                                            </div>  
                                                            <div class="clearfix"></div>   <br/> 
                                                            <div class="even-det">
                                                                <div class="col-md-6">
                                                                    <label for="">Duration: </label>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input id="duration" name="duration" class="form-control offset-bot-14 " type="text"  readonly="readonly"/>
                                                                </div>
                                                                <div class="col-md-1">Days</div>
                                                                
                                                            </div>  
                                                            <div class="clearfix"></div>   <br/>
                                                    </div>
                                                
                                                    </div>   
                                            </div>
                                            <div class="boter-texter-feat modal-footer">
                                                <div class="pull-right">
                                                    <button type="submit" id="promo_ok" class="btn" disabled>Promote</button>
                                                    <button type="button"  class="btn" data-dismiss="modal" onclick="$('#promote-modal').hide();" >Close</button>
                                                </div>
                                            </div>
                                                <input id="post-id" name="post-id" type="hidden" value="{{$articleData[0]->id}}" />
                                                <input id="post-type" name="post-type" type="hidden"  value="article" />
                                                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                @endif
                                @endif
                                <!-- END OF PROMOTE POST -->
                               
                            <!-- SHARE SECTION -->
                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                                <div class="arti-share-btns">
                            @else
                                <div class="arti-share-btns even-post-art">
                            @endif

                            @include('Frontend.includes.post_detail_share')
                          
                            </div>

                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                            <div class="art-joint-btn">
                                <div class="btn-group sat-btn-group" role="group" aria-label="...">
                                    @if(count($is_reported) == 0)
                                        <a  href="javascript:void(0)" class="btn repot" id="report_disabled" data-toggle="modal" data-target="#myModal">
                                            Report
                                        </a>
                                    @else
                                        <a disabled href="javascript:void(0)" class="btn repot"  data-toggle="modal" data-target="#myModal">Report</a>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="modal fade"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog"  role="document">
                                    <div class="modal-content report-pop">
                                        <form name="report" id="report">
                                            <div class="modal-body  ">
                                                <div class="report-infos-cont">
                                                    <h1>Report</h1>
                                                    <ul>
                                                        <li>
                                                            <input type="radio" name="report" id="get_report" value="0"> 
                                                            <label>Spam</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" name="report" id="get_report" value="1">
                                                            <label>Uninteresting</label>
                                                        </li>
                                                        <li>
                                                            <input type="radio" name="report" id="get_report" value="2">
                                                            <label>Hatred or Vulgarity</label>
                                                        </li>                                                                        
                                                        <label class="error" for="report"></label>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="boter-texter-feat modal-footer">
                                                <div class="pull-right">
                                                    <button type="button" id="report_ok" class="btn" onclick="check({{ $articleData[0]->id }})" >Post</button>
                                                    <button type="submit" class="btn" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @if(count($related_article) > 0)
                                @include('Frontend.includes.related_posts',['post_name'=>'Articles','related_posts'=>$related_article])
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<input type="hidden" value="{{ config('feature_pic_url').'article_image/' . $articleData[0]->profile_image }}" id="image_url">
<input type="hidden" value="{{ url('/') . '/view-article/' . '-' . $articleData[0]->id }}" id="page_link">
<input type="hidden" value="{{ $articleData[0]->title }}" id="article_title">
<input type="hidden" value="{{ $articleData[0]->description }}" id="article_description">
<input type="hidden" value="{{ $arr_article[0] -> id }}" id="article_id">
<input type="hidden" value="{{$finalData['user_session']['id']}}" id="user_id">
<!-- Share Article Code ; Including share_post file and passing feature as argument -->
@include('Frontend.includes.post_detail_share_fx', [ 'feature' => "article"])

<script>
function onSelectPromotion(cost)
    {
        if(cost == '')
        {
            $('#duration').val('');
            $('#promo_ok').prop('disabled', true);
        }
        else
        {
            var ar = <?php echo json_encode($promotion_det); ?>; 
            for(var i=0; i < ar.length; i++)
            {
                if(ar[i]['budget'] == cost)
                {
                    $('#duration').val(ar[i]['duration']);
                    $('#promo_ok').prop('disabled', false);
                    break;
                }
            }
        }
    }

    $(document).ready(function() {

    if('{{$response}}' == 'p')
    {
        $("#promote-modal").modal();
    }

        $('textarea#comment').mentionsInput({
            elastic       : false,
            onDataRequest:function (mode, query, callback) {
                $.getJSON(base_url + '/get-username-tagging?q='+query, function(responseData) {
                    responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                    callback.call(this, responseData);
                });
            }
        });
   
        var elem = $('#_' + window.location.hash.replace('#', ''));
        if(elem) {
             //$.scrollTo(elem.left, elem.top);
             //location.hash = "#_" + window.location.hash.replace('#', '');
             //$('html,body').animate({scrollTop: elem.offset().top},'slow');
        }
   


        $("#report_ok").click(function(e) {

        });
        var article_id = $("#article_id").val();
        var to_id_fk = $("#to_id_fk").val();
        var type = '3';
        var base_url = jQuery("#base_url").val();
        $.ajax({
            url: base_url + '/viewer',
            method: 'get',
            data: {
                article_id: article_id,
                to_article_visitor: to_id_fk,
                type: type,
            },
            success: function(result) {
                $('.audience_count').html('<b>AUDIENCE : ' + result + '</b>');
            }
        });
        @if($arr_user1_data['id'] == $arr_user_data['id'])
            @if(count($promotion)>0)
                $.ajax({
                    url: base_url + '/viewer',
                    method: 'get',
                    data: {
                        article_id: article_id,
                        to_article_visitor: to_id_fk,
                        type: type,
                        flag: '0',
                        start_date: '{{$promotion[0]->promo_start_date}}',
                        end_date: '{{$promotion[0]->promo_end_date}}'       
                    },
                    success: function (result) {
                        $('#clicks').html(result);
                    }
                });
        
                $.ajax({
                    url: base_url + '/viewer',
                    method: 'get',
                    data: {
                        article_id: article_id,
                        to_article_visitor: to_id_fk,
                        type: type,
                        flag: '1',
                        start_date: '{{$promotion[0]->promo_start_date}}',
                        end_date: '{{$promotion[0]->promo_end_date}}'       
                    },
                    success: function (result) {
                        $('#views').html(result);
                    }
                });
            @endif
        @endif
  


        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        $("#select_group").click(function() {
            $("#sub_group").prop('disabled', false)
        })
        $("input[id=get_report]").click(function(e) {
            e.stopPropagation();
            $("#sub_group").prop('disabled', true)
        })
        $("#make_group").click(function() {
            $("#make_sub_group").prop('disabled', false)
        })
        $("input[id=get_group_report]").click(function(e) {
            e.stopPropagation();
            $("#make_sub_group").prop('disabled', true)
        })

        $("#report").validate({
            rules: {
                report: {
                    required: true,
                }
            },
            messages: {
                report: {
                    required: "Please select one option.",
                }
            },
        });
        $("#frm_comments").validate({
            errorElement: 'div',
            rules: {
                comment: {
                    required: true
                }
            },
            messages: {
                comment: {
                    required: "Please enter the comment.",
                }
            },
        });
        $("#frm_comments_level_1").validate({
            errorElement: 'div',
            rules: {
                comment: {
                    required: true
                }
            },
            messages: {
                comment: {
                    required: "Please enter the comment.",
                }
            },
        });
        $("#frm_comments_level_2").validate({
            errorElement: 'div',
            rules: {
                comment: {
                    required: true
                }
            },
            messages: {
                comment: {
                    required: "Please enter the comment.",
                }
            },
        });
        $("#frm_comments_level_3").validate({
            errorElement: 'div',
            rules: {
                comment: {
                    required: true
                }
            },
            messages: {
                comment: {
                    required: "Please enter the comment.",
                }
            },
        });
    });

    function upvoteDownvote(article_id, status, flag) {
        var group_ids = $('#group_ids').val();
        $.ajax({
            type: "post",
            url: '{!!url("/")!!}/up-down-vote-article',
            data: {
                article_id: article_id,
                status: status,
                group_ids: group_ids
            },
            success: function(data) {
                $('#up-down-votes').html(data);
                if (flag == 1) {
                    $("#like").attr('disabled', 'disabled')
                    $("#unlike").removeAttr('disabled')

                } else {
                    $("#unlike").attr('disabled', 'disabled')
                    $("#like").removeAttr('disabled')
                }
            }
        });
    }

    function addFollowPost(article_id, status, flag) {
    var follower_user_id = $('#follower_user_id').val();
    $("#like").removeClass("acti-up-dw");
    $("#unlike").removeClass("acti-up-dw");
    $("#followPost").addClass("acti-up-dw");
    $('.grey-overlay-loader').show();
    $("#followPost").css("cursor", "wait");
    $.ajax({
    type: "post",
            url: '{!!url("/")!!}/follow-post-article',
            data: {
            article_id:article_id,
                    follower_user_id:follower_user_id,
                    status:status
            },
            success: function(data) {
            $("#followPost").addClass("acti-up-dw");
            $("#followPost").css("cursor", "pointer");
            $('#up-down-votes').html(data);
            $('.grey-overlay-loader').hide();
            }
    });
    }

    function repost(article_id, created_at) {
        swal({
                title: "Are you sure want to repost this Article?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Repost",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "get",
                        url: '{!!url("/")!!}/front-article-repost/' + btoa(article_id) + "/" + btoa(
                            created_at),
                        dataType: "json",
                        success: function(data) {
                            document.location = '{!!url("/")!!}/front-edit-article/' + btoa(
                                article_id);
                            if (data.msg == '0') {

                            }
                        }
                    });
                } else {
                    swal("Cancelled", "You Cancelled", "error");
                }
            });
    }

    function like(meetup_id, status) {
        $.ajax({
            type: "get",
            url: '{!!url("/")!!}/like-event/' + btoa(meetup_id) + "/" + btoa(status),
            data: '',
            dataType: "json",
            success: function(data) {
                var attend = data.event_users_attend_count[0].user_count
                var maybe = data.event_users_maybe_count[0].user_count
                $("#maybe").html('Like' + '(' + maybe + ')')
                $("#att").html('Unlike' + '(' + attend + ')')
                if (status == 1) {
                    $("#like").prop('disabled', false);
                    $("#unlike").prop('disabled', true);
                } else {
                    $("#unlike").prop('disabled', false);
                    $("#like").prop('disabled', true);
                }
            }
        })
    }

    function addFollower(follower_user_id) {
        $.ajax({
            type: "get",
            url: '{!!url("/")!!}/make-follower',
            data: {
                follower_user_id: follower_user_id
            },
            dataType: "json",
            success: function(msg) {
                if (msg.success == 1) {
                    $("#follower_count").html(msg.follower_count[0].follower)
                    $("#follow").text('Unfollow');
                    $("#follow").prop('title', 'Click to Unfollow');
                } else {
                    $("#follow").text('Follow');
                    $("#follower_count").html(msg.follower_count[0].follower)
                    $("#follow").prop('title', 'Click to Follow');
                }
            }
        })
    }

    function addConnection(to_user_id) {
        $.get('{!!url("/")!!}/add-connection', {
            to_user_id: to_user_id
        }, function(msg) {
            if (msg == true) {
                $(".connection" + to_user_id).addClass('active');
                $(".connection" + to_user_id).html('<i class="fa fa-check"></i>');
                $(".connection" + to_user_id).prop('title', 'click to disconnect');
            } else {
                $(".connection" + to_user_id).removeClass('active');
                $(".connection" + to_user_id).html('<i class="icon-Add"></i>');
                $(".connection" + to_user_id).prop('title', 'click to connect');
            }
        });
    }

    function readURL(input) {
        var ext = input.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#blah').css("display", "block");
                        $('#blah').prop('src', e.target.result);
                        $('#remove_image').css("display", "block");
                        $(input).attr('title', input.value)
                    }
                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Only Image allowed !');
                input.value = '';
        }
    }

    function check(article_id) {
        if ($('#report').valid()) {
            $('#report_ok').prop('data-dismiss', 'modal');
            $.ajax({
                type: "get",
                url: '{!!url("/")!!}/make-article-report/' + btoa(article_id),
                data: $("#report").serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.msg == '1') {
                        $("#report_disabled").prop('disabled', true)
                        window.location.reload()
                    }
                }
            })
        }
    }
    $("#group_report").validate({
        rules: {
            report: {
                required: true,
            }

        },
        messages: {
            report: {
                required: "Please select one option.",
            }
        },
    });

    function getGroupName(value) {
        $("#make_sub_group option[value=" + value + "]").prop("selected", "selected");
        $('#myModal1').modal('show')
        $("#make_group").prop('checked', 'checked')
        $("#make_sub_group").prop('disabled', false)
    }

    function makeGroupReport(article_id) {
        if ($('#group_report').valid()) {
            $('#group_report_ok').prop('data-dismiss', 'modal');
            $.ajax({
                type: "get",
                url: '{!!url("/")!!}/make-article-group-report/' + btoa(article_id),
                data: $("#group_report").serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.msg == '1') {
                        window.location.reload()
                    }
                }
            })
        }
    }

    function postReply(value) {
        var article_id = $('#article_id').val();
        var user_id = $('#user_id').val();
        value = value;
        var comment_id = value + 1;
        $('#rep_d' + value).html('');
        str = '';
        str += '<div class="form-group " id="myDiv">';
        str += '<form enctype="multipart/form-data" action="{{ url("/") }}/article-comment" method="post"> '
        str += ' <input type="text" required="" name="reply" id="reply' + comment_id +
            '" class="form-control" placeholder="comment here...">';
        str += ' <input type=hidden name="parent_id" value="' + value + '"  class="form-control" >';
        str += ' <input type="hidden" name="user_id" value="' + user_id + '">';
        str += ' <input type="hidden" name="post_article_id" value="' + article_id + ' ">';
        str += '<button class="btn btn-default" type="submit" value="POST COMMENT" name="btn_comment">POST COMMENT</button>';
        str += '<div class="uping-imgs mar-upload-reply">';
        str += '<input  value="Choose a file" class="upit-img" name="commnet_pic" onchange="readURL(this);"  type="file">';
        str += '</div>';
        str += '</form>';
        str += '</div>';
        $('#rep_d' + value).append(str);
        $('#rep_d' + value).show();
    }

    function postReplyR(value, parent_id) {
        var article_id = $('#article_id').val();
        var user_id = $('#user_id').val();
        value = value;
        var comment_id = value + 1;
        $('#repliedR_div' + value).html('');
        str = '';
        str += '<div class="form-group" id="myDiv2">';
        str += '<form  enctype="multipart/form-data" action="{{ url("/") }}/article-comment" method="post" > '
        str += ' <input type="text" required="" name="reply" id="repliedR' + comment_id +
            '" class="form-control" placeholder="comment here...">';
        str += ' <input type=hidden name="r_parent_id" value="' + value + '"  class="form-control" >';
        str += ' <input type=hidden name="parent_id" value="' + parent_id + '"  class="form-control" >';
        str += ' <input type="hidden" name="user_id" value="' + user_id + ' ">';
        str += ' <input type="hidden" name="post_article_id" value=" ' + article_id + ' ">';
        str +=
            '<button class="btn btn-default" type="submit" value="POST COMMENT" name="btn_comment">POST COMMENT</button>'
        str += '<div class="uping-imgs mar-upload-reply">';
        str +=
            '<input  value="Choose a file" class="upit-img" name="commnet_pic" onchange="readURL(this);"  type="file">';
        str += '</form>';
        str += '</div>';
        $('#repliedR_div' + value).append(str);
        $('#repliedR_div' + value).show();
        $('#repliedR' + comment_id).focus();
    }

    function postReplyRe(value, value2, parent_id) {
        var article_id = $('#article_id').val();
        var user_id = $('#user_id').val();
        value = value;
        var comment_id = value2 + 1;
        $('#repliedR_div' + value).html('');
        str = '';
        str += '<div class="form-group" id="myDiv3">';
        str += '<form  enctype="multipart/form-data" action="{{ url("/") }}/article-comment" method="post"> '
        str += ' <input type="text" required=""  name="reply" id="repliedR' + comment_id +
            '" class="form-control" placeholder="comment here...">';
        str += ' <input type=hidden name="r_parent_id" value="' + value2 + '"  class="form-control" >';
        str += ' <input type=hidden name="parent_id" value="' + parent_id + '"  class="form-control" >';
        str += ' <input type="hidden" name="user_id" value="' + user_id + ' ">';
        str += ' <input type="hidden" name="post_article_id" value=" ' + article_id + ' ">'
        str +=
            '<button class="btn btn-default" type="submit" value="POST COMMENT" name="btn_comment">POST COMMENT</button>'
        str += '<div class="uping-imgs mar-upload-reply">';
        str +=
            '<input  value="Choose a file" class="upit-img" name="commnet_pic" onchange="readURL(this);"  type="file">';
        str += '</div>';
        str += '</form>';
        str += '</div>';
        $('#repliedR_div' + value).append(str);
        $('#repliedR_div' + value).show();
        $('#repliedR' + comment_id).focus();
    }

    function replies(value) {
        id = value;
        $('#rep' + id).toggle();
    }

    function repliesR(value) {
        id = value;
        $('#reply_div' + id).toggle();
    }

    jQuery('#myDiv').click(function() {
        $(this).data('clicked', true);
    });
    $("a").click(function() {
        if (jQuery('#myDiv').data('clicked')) {
            alert('clicked')
        } else {

        }
    });
</script>
@include('Frontend.includes.post_detail_common_fx')
</body>
</html>

@include('Frontend.includes.footer')
