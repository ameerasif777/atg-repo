<html>
    <head>
        <link href="{{ url('/assets/Frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/main.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <script src="{{url('/assets/Frontend/js/moment.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery-ui.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/bootstrap.min.js')}}"></script>
    </head>
<body>

<section class="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="">
                         <p>{!!$post_description[0]->description!!}</p>
    </div></div></div>
  </div>
</section>
@include('Frontend.includes.post_detail_common_fx_wv')
</body>
</html>
