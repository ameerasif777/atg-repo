@extends('Frontend.layouts.default')
@section('content')
<section class="signup-content">
    <div class="container">
        <div class="contact-us-form">
            <h2 class="contact-heading">FAQ</h2>
            <div>
                <div class="faq-section">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($faqs_list as $faq) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $faq['id']; ?>">
                                    <h4 class="panel-title">
                                        <a class="collapsed fa fa-plus-circle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['id']; ?>"> <?php echo stripslashes($faq['question']); ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $faq['id']; ?>" class="panel-collapse collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $faq['id']; ?>">
                                    <div class="panel-body"> <?php echo stripslashes($faq['answer']); ?></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
