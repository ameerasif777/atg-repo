@if(count($arr_selected_filter_data) > 0)
@foreach($arr_selected_filter_data as $key => $value) 
@if($value->type == 'people')
<article class="white-panel">
    <div class="atg-world-one pos-relative">
        <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}'><img src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" alt="img" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif/></a>
        <div class="grid-cont"><a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' style="color:#269b70;">{{ ucfirst($value->first_name) }} {{ ucfirst($value->last_name) }}</a></div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}'><h4 class="media-heading">{{ $value->profession }} </h4>
                </a><p>People</p>
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'event')


<?php
$image_path = config('feature_image_path').'event_image/thumb/' . $value->image;
if (Storage::exists($image_path) && $value->image != '') {
    $post_image_path = config('feature_pic_url').'event_image/thumb/' . $value->image;
} else {
    $post_image_path = '';
}
?>
<article class="white-panel">
    <div class="atg-world-one">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view-event/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
            <img src="{{ config('feature_pic_url').'event_image/thumb/'.$value->image }}" alt="img"/></a>
        @endif
        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/view-event/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                </a><p>Event</p>
            </div>
        </div>
    </div>
</article>

@elseif($value->type == 'job')

<?php $post_image_path = ''; ?>
<article class="white-panel">
    <div class="atg-world-one pos-relative">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}' class="text-color">
            <img src="{{ config('img').'image-not-found2.jpg') }}" alt="img"/></a>
        @endif

        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <!--<div class="onter-img-media">-->
            <div class="media otg-found border-top">
                <div class="media-left">
                    <div class="media-user">
                        @if(isset($value->profile_picture) && !empty($value->profile_picture))
                        <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                             @elseif($value->gender == 0)
                             <img alt="users" src="{{ config('avatar_male') }}">
                        @else
                        <img alt="users" src="{{ config('avatar_female') }}">
                        @endif
                    </div>
                </div>
                <div class="media-body">
                    <a href='{{url('/')}}/view/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}' class="text-color"><h4 class="media-heading">{{ $value->title }}</h4></a><p>Job</p>
                </div>
            </div>
        <!--</div>-->
    </div>
</article>

@elseif($value->type == 'meetup')
<?php
$image_path = config('feature_image_path').'meetup_image/thumb/' . $value->image;
if (Storage::exists($image_path) && $value->image != '') {
    $post_image_path = config('feature_pic_url').'meetup_image/thumb/' . $value->image;
} else {
    $post_image_path = '';
}
?>
<article class="white-panel">
    <div class="atg-world-one">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view-meetup/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
            <img src="{{ config('feature_pic_url').'meetup_image/thumb/'.$value->image }}" alt="img"/></a>
        @endif
        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/view-meetup/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                </a><p>Meetup</p>
            </div>
        </div>
    </div>
</article>

@elseif($value->type == 'qrious')
<?php
$image_path = config('feature_image_path').'qrious_image/thumb/' . $value->image;
if (Storage::exists($image_path) && $value->image != '') {
    $post_image_path = config('feature_pic_url').'qrious_image/thumb/' . $value->image;
} else {
    $post_image_path = '';
}
?>
<article class="white-panel">
    <div class="atg-world-one">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
            <img src="{{ config('feature_pic_url').'qrious_image/thumb/'.$value->image }}" alt="img"/></a>
        @endif
        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                </a><p>Qrious</p>
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'article')
<?php
$image_path = config('feature_image_path').'article_image/thumb/' . $value->image;
if (Storage::exists($image_path) && $value->image != '') {
    $post_image_path = config('feature_pic_url').'article_image/thumb/' . $value->image;
} else {
    $post_image_path = '';
}
?>
<article class="white-panel ">
    <div class="atg-world-one">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
            <img src="{{ config('feature_pic_url').'article_image/thumb/'.$value->image }}" alt="img"/></a>
        @endif
        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                <h4 class="media-heading">{{ $value->title }}</h4>
                </a>
                <p>Article</p>
            </div>
        </div>
    </div>
</article>

@elseif($value->type == 'company')
<article class="white-panel">
    <div class="atg-world-one pos-relative">
        <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' class="text-color" ><img src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" alt="img" onerror="src='{{ url('/public/assets/Frontend/img/company.png') }}'"/></a>
        <div class="onter-img-media">
            <div class="media otg-found border-top">
                <div class="media-left">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' class="text-color" >
                    {{ $value->first_name }} {{ $value->last_name }}</a></h4>
                </div>
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'education')
<?php
$image_path = config('feature_image_path').'education_image/thumb/' . $value->image;
if (Storage::exists($image_path) && $value->image != '') {
    $post_image_path = config('feature_pic_url').'education_image/thumb/' . $value->image;
} else {
    $post_image_path = '';
}
?>
<article class="white-panel ">
    <div class="atg-world-one">
        @if($post_image_path != '')
        <a href='{{url('/')}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
            <img src="{{ config('feature_pic_url').'education_image/thumb/'.$value->image }}" alt="img"/></a>
        @endif
        <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
        <div class="media otg-found border-top">
            <div class="media-left">
                <div class="media-user">
                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                    <img alt="users" src="{{config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif >
                         @elseif($value->gender == 0)
                         <img alt="users" src="{{ config('avatar_male') }}">
                    @else
                    <img alt="users" src="{{ config('avatar_female') }}">
                    @endif
                </div>
            </div>
            <div class="media-body">
                <a href='{{url('/')}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                <h4 class="media-heading">{{ $value->title }}</h4>
                </a>
                <p>Education</p>
            </div>
        </div>
    </div>
</article>
@else
No records found...
@endif

@endforeach
@else
<h4>No Records found...</h4>
@endif
<script id="script_id">
    $("#total_arr_count").val({{$arr_count}});
    $("#slice_arr_data").val({{$slice_arr_data}});
</script>