<link href="{{ url('/public/assets/css/views/search.css') }}" rel="stylesheet" type="text/css" />
@include('Frontend.includes.head')
@include('Frontend.includes.header') 
@include('Frontend.includes.session-message')


<section class="user-dash-sec" >
    <div class="dash-header">
        <div class="dash-header-top">

        </div>
        <div class="dash-header-bot clearfix">
            <div class="recomended-f"><img src="{{config('img')}}filter_icon.png" class="filter-icon hidden-xs">
                <div class="filter-text hidden-lg hidden-xl hidden-md hidden-sm ">Filter</div>
            </div>
            <div class="dash-middile-nav">
                <ul>
                    <li style="cursor:pointer"  id="all1" onclick="filter('all')" >All</a></li>
                    <li style="cursor:pointer"  id="article1" onclick="filter('article')" >Articles</a></li>
                    <li style="cursor:pointer" id="meetups1" onclick="filter('meetups')" >Meetups</a></li>
                    <li style="cursor:pointer" id="events1" onclick="filter('events')" >Events</a></li>
                    <li style="cursor:pointer" id="jobs1" onclick="filter('jobs')" >Jobs</a></li>
                    <li style="cursor:pointer" id="education1" onclick="filter('educ')" >Education</a></li>
                    <li style="cursor:pointer" id="qrious1" onclick="filter('qurious')" >Qrious?</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="resp-btn"><a href="javascript:void(0)"><i class="fa fa-angle-down"></i></a></div>
    <div class="dash-cate-sec">
        <div class="container">

            <div class="row">
                <div class="col-sm-3">
                    <div class="serch-acorde">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="more-less glyphicon glyphicon-menu-up"></i>
                                            My Groups
                                        </a>
                                    </h4>
                                </div>

                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <form id="frm_post_groups" name="frm_post_groups">
                                        <div class="panel-body">
                                            <div class="ser-ntor-info">
                                                <ul class="clearfix">
                                                    <li>
                                                        <div class="boxesss">
                                                            <input type="checkbox" class="checkbox-all"  name="group[]" value="0" id="box-1">
                                                            <label for="box-1">All</label>
                                                        </div>
                                                    </li>
                                                    <input type="hidden" name="search_word" id="search_word" value="@if(isset($search_not_found) && $search_not_found != ''){{ $search_not_found }}@endif">
                                                    @if(count($arr_groups) > 0)
                                                    <?php $counter = 39; ?>
                                                    @foreach($arr_groups as $group)
                                                    <li>
                                                        <div class="boxesss">
                                                            <input type="checkbox" class="checked-single" name="group[]" value="{{ $group->id }}" id="box-{{ $counter }}" >
                                                            <label for="box-{{ $counter }}">{{ $group->group_name }} </label>
                                                        </div>
                                                    </li>
                                                    <?php $counter++; ?>
                                                    @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 border-left">
                    @if(isset($search_not_found) && $search_not_found != '')<h5>You searched for: "<b>{{ $search_not_found }}</b>"</h5>@endif
                    <div class="marketing">
                        <section id="blog-landing">

                            @if(count($arr_selected_filter_data) > 0)
                            @foreach($arr_selected_filter_data as $key => $value)  
                            @if($value->type == 'people')
                            <article class="white-panel " >
                                <div class="atg-world-one pos-relative">
                                    <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}'>
                                        @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                            <img src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" alt="img" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif/> 
                                        @elseif($value->gender == 0)
                                            <img alt="users" src="{{config('avatar_male') }}">
                                        @else
                                            <img alt="users" src="{{config('avatar_female') }}">
                                        @endif
                                    </a>
                                    <div class="grid-cont"><a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' style="color:#269b70;">{{ ucfirst($value->first_name) }} {{ ucfirst($value->last_name) }}</a></div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}'><h4 class="media-heading">{{ $value->profession }} </h4>
                                            </a><p>People</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @elseif($value->type == 'event')
                            <?php
                            // Do NOT show any image, if cover image does not exist for an event.
                            $image_path = config('feature_image_path').'event_image/thumb/' . $value->image;
                            if (Storage::exists($image_path) && $value->image != '') {
                                $post_image_path = config('feature_pic_url').'event_image/thumb/' . $value->image;
                            } else {
                                $post_image_path = '';
                            }
                            ?>

                            <article class="white-panel ">
                                <div class="atg-world-one">

                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view-event/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                                        <img src="{{ config('feature_pic_url').'event_image/thumb/'.$value->image }}" alt="img"/></a>
                                    @endif

                                    </a>
                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <a href='{{url('/')}}/view-event/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                                            </a><p>Event</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @elseif($value->type == 'job')
                            <?php $post_image_path = ''; ?>
                            <article class="white-panel ">
                                <div class="atg-world-one pos-relative">
                                    <!-- Jobs do not have cover images, this if section never gets executed -->
                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}' class="text-color">
                                        <img src="{{ config('img').'image-not-found2.jpg' }}" alt="img"/></a>
                                    @endif
                                    
                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <!--<div class="onter-img-media">-->
                                        <div class="media otg-found border-top">
                                            <div class="media-left">
                                                <div class="media-user">
                                                    @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                    <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                         @elseif($value->gender == 0)
                                                         <img alt="users" src="{{config('avatar_male') }}">
                                                    @else
                                                    <img alt="users" src="{{config('avatar_female') }}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <a href='{{url('/')}}/view/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}' class="text-color">
                                                        <h4 class="media-heading">{{ $value->title }}</h4></a><p>Job</p>
                                            </div>
                                        </div>
                                    <!--</div>-->
                                </div>
                            </article>

                            @elseif($value->type == 'meetup')
                            <?php
                            // Do NOT show any image, if cover image does not exist for a meetup
                            $image_path = config('feature_image_path').'meetup_image/thumb/' . $value->image;
                            if (Storage::exists($image_path) && $value->image != '') {
                                $post_image_path = config('feature_pic_url').'meetup_image/thumb/' . $value->image;
                            } else {
                                $post_image_path = '';
                            }
                            ?>

                            <article class="white-panel ">
                                <div class="atg-world-one">

                                    <!-- Show image if it exists -->
                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view-meetup/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                                        <img src="{{ config('feature_pic_url').'meetup_image/thumb/'.$value->image }}" alt="img"/></a>
                                    @endif                                    

                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <a href='{{url('/')}}/view-meetup/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                                            </a><p>Meetup</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @elseif($value->type == 'qrious')
                            <?php
                            $image_path = config('feature_image_path').'qrious_image/thumb/' . $value->image;
                            if (Storage::exists($image_path) && $value->image != '') {
                                $post_image_path = config('feature_pic_url').'qrious_image/thumb/' . $value->image;
                            } else {
                                $post_image_path = '';
                            }
                            ?>

                            <article class="white-panel ">
                                <div class="atg-world-one">
                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                                        <img src="{{ config('feature_pic_url').'qrious_image/thumb/'.$value->image }}" alt="img"/></a>
                                    @endif

                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <a href='{{url('/')}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                                            </a><p>Qrious</p>
                                        </div>
                                    </div>
                                </div>
                            </article>


                            @elseif($value->type == 'article')
                            <?php
                            $image_path = config('feature_image_path').'article_image/thumb/' . $value->image;
                            if (Storage::exists($image_path) && $value->image != '') {
                                $post_image_path = config('feature_pic_url').'article_image/thumb/' . $value->image;
                            } else {
                                $post_image_path = '';
                            }
                            ?>

                            <article class="white-panel ">
                                <div class="atg-world-one">
                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                                        <img src="{{ config('feature_pic_url').'article_image/thumb/'.$value->image }}" alt="img"/>
                                    </a>
                                    @endif

                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <a href='{{url('/')}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                                            </a><p>Article</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @elseif($value->type == 'company')

                            <article class="white-panel " >
                                <div class="atg-world-one pos-relative">
                                    <a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' class="text-color" ><img src="{{ config('profile_pic_url').$value->id.'/thumb/'.$value->profile_picture }}" alt="img" onerror="src='{{ url('/public/assets/Frontend/img/company.png') }}'"/></a>
                                    <div class="onter-img-media">
                                        <div class="media otg-found border-top">
                                            <div class="media-left">

                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href='{{url('/')}}/user-profile/{{base64_encode($value->id)}}' class="text-color" >
                                                        {{ $value->first_name }} {{ $value->last_name }}</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @elseif($value->type == 'education')
                            <?php
                            $image_path = config('feature_image_path').'education_image/thumb/' . $value->image;
                            if (Storage::exists($image_path) && $value->image != '') {
                                $post_image_path = config('feature_pic_url').'education_image/thumb/' . $value->image;
                            } else {
                                $post_image_path = '';
                            }
                            ?>
                            <article class="white-panel ">
                                <div class="atg-world-one">
                                    @if($post_image_path != '')
                                    <a href='{{url('/')}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'>
                                        <img src="{{ config('feature_pic_url').'education_image/thumb/'.$value->image }}" alt="img"/>
                                    </a>
                                    @endif
                                    <div class="grid-cont">"{!! strip_tags($value->description) !!}"</div>
                                    <div class="media otg-found border-top">
                                        <div class="media-left">
                                            <div class="media-user">
                                                @if(isset($value->profile_picture) && !empty($value->profile_picture))
                                                <img alt="users" src="{{ config('profile_pic_url').$value->user_id.'/thumb/'.$value->profile_picture }}" @if($value->gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif >
                                                     @elseif($value->gender == 0)
                                                     <img alt="users" src="{{config('avatar_male') }}">
                                                @else
                                                <img alt="users" src="{{config('avatar_female') }}">
                                                @endif
                                            </div>
                                        </div>                                            
                                        <div class="media-body">
                                            <a href='{{url('/')}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}'><h4 class="media-heading">{{ $value->title }}</h4>
                                            </a><p>Education</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            @else
                            No records found...
                            @endif
                            @endforeach
                            @else
                            <h4>No Records found...</h4>
                            @endif
                        </section>
                        <center><div id="feed_to_show" class="feed_to_show" style="display: none;"></div></center>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="hidden_field_for_scroll" value="true" id="hidden_field_for_scroll">
        </section>
        <input type="hidden" name="qrious" id="qrious" value="{{ $qrious }}">
        <input type="hidden" name="articles" id="articles" value="{{ $articles }}">
        <input type="hidden" name="job" id="job" value="{{ $job }}">
        <input type="hidden" name="people" id="people" value="{{ $people }}">
        <input type="hidden" name="event" id="event" value="{{ $event }}">
        <input type="hidden" name="meetup" id="meetup" value="{{ $meetup }}">
        <input type="hidden" name="education" id="education" value="{{ $education }}">
        <input type="hidden" name="mygroup" id="mygroup" value="{{ $mygroup }}">
        <input type="hidden" name="all" id="all" value="{{ $all }}">
        <input type="hidden" name="atg_group" id="atg_group" value="{{ $atg_group }}">
        @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
        @endif

        <input type="hidden" id="total_arr_count" name="total_arr_count" value="">
        <input type="hidden" id="slice_arr_data" name="slice_arr_data" value="">

        <script type="text/javascript">
            $(document).ready(function() {

                $('#blog-landing').pinterest_grid({
                    no_columns: 3,
                    padding_x: 10,
                    padding_y: 10,
                    margin_bottom: 50,
                    single_column_breakpoint: 700
                });
                function toggleIcon(e) {
                    $(e.target)
                            .prev('.panel-heading')
                            .find(".more-less")
                            .toggleClass('glyphicon-menu-up glyphicon-menu-down');
                }
                $('.panel-group').on('hidden.bs.collapse', toggleIcon);
                $('.panel-group').on('shown.bs.collapse', toggleIcon);
            });
            function addFollower(follower_user_id)
            {
                $.get('{!!url("/")!!}/add-follower', {follower_user_id: follower_user_id}, function(msg) {
                    if (msg == true)
                    {
                        $(".follower" + follower_user_id).addClass('active');
                    } else {
                        $(".follower" + follower_user_id).removeClass('active');
                    }
                });
            }

            function UrlExists(url)
            {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                return http.status != 404;
            }

            $("input:checkbox[name='group[]']").click(function() {

                var group_ids = $('input[name="group[]"]:checked.checked-single').map(function() {

                    return this.value;
                }).get().join(",");
                $("#group_ids").val(group_ids);
                load_contents();
            });
            
         
            
            
            $(".checkbox-all").on("click", function() {
                if ($(this).prop("checked") == true) {
                    $(".checked-single").prop("checked", true);
                    var group_ids = $('input[name="group[]"]:checked.checked-single').map(function() {

                    return this.value;
                }).get().join(",");
                $("#group_ids").val(group_ids);
                load_contents();
                } else {
                    $(".checked-single").prop("checked", false);
                      var group_ids = $('input[name="group[]"]:checked.checked-single').map(function() {

                    return this.value;
                }).get().join(",");
                $("#group_ids").val(group_ids);
                load_contents();
                }
            });
            $(".checked-single").on("click", function() {
                if ($(this).prop("checked") == false) {
                    $(".checkbox-all").prop("checked", false);
                }
            });
            function addConnection(to_user_id, user_name)
            {
                var follow_user_name = user_name;
                $.get('{!!url("/")!!}/add-connection', {to_user_id: to_user_id}, function(msg) {


                    if (msg == true)
                    {
                        $(".connection" + to_user_id).addClass('active');
                        alert("You are now Following  " + follow_user_name);
                        $(".connection" + to_user_id).html('<i class="fa fa-check"></i>');
                    } else {
                        $(".connection" + to_user_id).removeClass('active');

                        $(".connection" + to_user_id).html('<i class="icon-Add"></i>');
                    }

                });
            }






        </script>

        <style>
            .text-color{
                color:white
            }
        </style>


        @include('Frontend.includes.footer')
        <script>


            $(window).ready(function() {

                $('#filter').val('all')
                var s = 0;
            });

            var track_page;
            track_page = "0";
            var ajaxCallFlg = true;
            function filter(value) {
                
                $('#all1').removeClass('active-filter');
                $('#article1').removeClass('active-filter');
                $('#meetups1').removeClass('active-filter');
                $('#qrious1').removeClass('active-filter');
                $('#education1').removeClass('active-filter');
                $('#jobs1').removeClass('active-filter');
                $('#events1').removeClass('active-filter');
                if (value == 'qurious') {
                    $('#apply_filter').val('qurious1');
                    $('#qrious1').addClass('active-filter');
                    load_contents(track_page)

                } else if (value == 'article') {
                    $('#apply_filter').val('article1');
                    $('#article1').addClass('active-filter');
                    $("#blog-landing").html('')
                    load_contents(track_page);
                } else if (value == 'meetups') {
                    $('#apply_filter').val('meetups1');
                    $('#meetups1').addClass('active-filter');
                    $("#blog-landing").html('');
                    load_contents(track_page)
                }
                else if (value == 'jobs') {
                    $('#apply_filter').val('jobs1');
                    $('#jobs1').addClass('active-filter');
                    $("#blog-landing").html('');
                    load_contents(track_page)
                }
                else if (value == 'educ') {
                    $('#apply_filter').val('educ1');
                    $('#education1').addClass('active-filter');
                    $("#blog-landing").html('');
                    load_contents(track_page)
                }
                else if (value == 'events') {
                    $('#apply_filter').val('events1');
                    $('#events1').addClass('active-filter');
                    track_page = 10
                    load_contents(track_page)
                }
                else if (value == 'all') {
                    $('#apply_filter').val('all1');
                    $('#all1').addClass('active-filter');
                    $("#blog-landing").html('');
                    load_contents(track_page)
                }
            }

            function load_contents(track_page) {
                var group_ids = $("#group_ids").val();
                //        var qrious = $("#qrious").val();
                ajaxCallFlg = false;
                var search_word = $('#search_word').val();
                var qrious, all, articles, job, people, event, meetup, education, company, atg_group, mygroup, remember, all;
                all = $('#all').val();
                qrious = $('#qrious').val();
                articles = $('#articles').val();
                job = $('#job').val();
                people = $('#people').val();
                event = $('#event').val();
                meetup = $('#meetup').val();
                education = $('#education').val();
                company = $('#company').val();
                mygroup = $('#mygroup').val();
                atg_group = $('#atg_group').val();




                var apply_filter = $('#apply_filter').val();
                var location = $('#location').val();
                if ($("#load_more_button").text() == "") {
                    $.ajax({
                        url: '{{url("/")}}/search',
                        method: 'post',
                        data: {apply_filter: apply_filter, group_ids: group_ids, track_page: track_page, search_word: search_word, all: all, qrious: qrious, articles: articles, job: job, people: people, event: event, meetup: meetup, education: education, company: company, atg_group: atg_group, mygroup: mygroup, location: location, remember: remember, page_type: 'GET'},
                        success: function(data) {
                            if ($('#apply_filter').val() != 'all') {
                                $("#blog-landing").append(data);
                            } else {
                                $("#blog-landing").append(data);
                            }
                            ajaxCallFlg = true;
                            $("#hidden_field_for_scroll").val('true');
                        }
                    });
                }

            }

            $(window).ready(function() {
                $(window).scroll(function() {

//                    if (document.body.scrollHeight - $(this).scrollTop() <= $(this).height()) {
                    /* Chnages for ajax 50% scrolling */
                    var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
                    var heightss=$(document).height()/2;
                    if (scrollPercent>50) {
                        if ($("#hidden_field_for_scroll").val() == 'true') {
                        var total_records = $("#total_arr_count").val();
                        var page_wise_records = $("#slice_arr_data").val();

                        track_page = parseInt(track_page) + 10;
                        var ids_and_types = new Array();
                        var arr = [];
                        if (parseInt(total_records) <= parseInt(page_wise_records)) {
                            $("#hidden_field_for_scroll").val('false');
                            $("#feed_to_show").show();
                            $("#feed_to_show").text("NO MORE FEED").prop("disabled", true);
                            setTimeout(function() {
                                $('.grey-overlay-loader').hide();

                            }, 200);
                        } else {
                            $("#feed_to_show").hide();
                            $("#hidden_field_for_scroll").val('false');
                            load_contents(track_page);
                            setTimeout(function() {
                                $('.grey-overlay-loader').hide();

                            }, 200);
                        }

                        }

                    }
                });


            });

    
    // Collapse groups section on a mobile device.
    $(document).ready(function(){
    var windowWidth = $(window).width();
    if(windowWidth <= 767) //for iPad & smaller devices
        $('.panel-collapse').removeClass('in')
    });

</script>
