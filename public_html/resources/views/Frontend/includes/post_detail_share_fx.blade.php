<?php 
    $title = $feature . '_title';
?>
<script src="https://connect.facebook.net/en_US/all.js#xfbml=1&appId=906595209472982"></script> 
<script>
    var image_url = $("#image_url").val();
    var page_link = $("#page_link").val();
    var title = "View this {{$feature}} on ATG: " + $("#{{$title}}").val();
    var description = $("#{{$feature}}_description").text();
    
    /*****facebook share********/
    function fb() {
        var page_link = $("#page_link").val();
        window.open('https://www.facebook.com/share.php?u=' + page_link) 
         if (window.location.href.indexOf("/user-dashboard") > -1) {
                   
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
    }
    /********linkdin share**********/
    function callLiknedin() {
        window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + page_link + '&title=' + title +
            '&summary=' + description + '&source=atg.world')
         if (window.location.href.indexOf("/user-dashboard") > -1) {
                   
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
    }

    function callTwitter() {
//        var page_link = $("#page_link").val();
        window.open('https://twitter.com/share?text=' + title + '&url=' + page_link);
         if (window.location.href.indexOf("/user-dashboard") > -1) {
                   
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
    }

    function callGooglePlus() {
        var page_link = $("#page_link").val();
        window.open('https://plus.google.com/share?url=' + page_link);
         if (window.location.href.indexOf("/user-dashboard") > -1) {
                   
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
    }
     function callReddit() {
        var page_link = $("#page_link").val();
        window.open('https://reddit.com/submit?url=' + page_link + '&title=' + title);
          if (window.location.href.indexOf("/user-dashboard") > -1) {
                   
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
    }
    function copyURLtoClipboard()
            {
           
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(page_link).select();
            document.execCommand("Copy");
            $temp.remove();
            
            $(".copy_show").show();
            setTimeout(function() { $(".copy_show").fadeOut(); }, 5000);
                
                if (window.location.href.indexOf("/user-dashboard") > -1) {
                    $(".copy_show").css("display","none");
                    setTimeout(function() { $(".share_div").fadeOut(); }, 500);
                    }
               
            }
</script>

