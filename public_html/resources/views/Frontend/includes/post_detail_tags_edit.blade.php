<?php
$function_name='edit'.ucwords($feature).'Front';
$content_array=${$feature.'Data'};
?>
<div class="hash-taber clearfix">
        @if($content_array[0]->$tags != '')
        <span class="pull-left">
            <ul>
                @foreach (explode(', ', $content_array[0]->$tags) as $val1)
                <li>
                <a style="cursor:pointer;" title="Click to search tag" onclick="searchWithTag('{{$val1}}')">{{ $val1 }}</a>
            </li>,
            @endforeach
        </ul>
    </span>
    @endif

    <span class="pull-right">
    @if($session_data != "")
    @if($content_array[0]->user_id_fk == $arr_user_data['id'])
    @if(!strcmp($feature,'job'))
         @if($arr_user_data['user_type'] != 1)
            <a href='{{ route("$function_name", [ base64_encode($content_array[0]->id) ])}}' class="btn">Edit</a>
         @endif
    @else
    <a href='{{ route("$function_name", [base64_encode($content_array[0]->id)]) }}' class="btn">Edit</a>
    @endif
    @endif
    @endif
    </span>
</div>