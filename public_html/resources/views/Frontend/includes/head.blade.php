<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- <script src="{{url('/assets/Frontend/js/navigatorsw.js')}}"></script> -->
        <!-- getsmartlook.com user tracking -->
        <script type="text/javascript">
            window.smartlook || (function (d) {
                var o = smartlook = function () {
                    o.api.push(arguments)
                }, h = d.getElementsByTagName('head')[0];
                var c = d.createElement('script');
                o.api = new Array();
                c.async = true;
                c.type = 'text/javascript';
                c.charset = 'utf-8';
                c.src = '//rec.smartlook.com/recorder.js';
                h.appendChild(c);
            })(document);
            smartlook('init', '{{ env('SMARTLOOK_KEY') }}');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
        if (isset($arr_public_user_data['is_google_crawalable'])) {

            if ($arr_public_user_data['is_google_crawalable'] == 1) {
                echo '<meta name="robots" content="index,follow">';
            } else {
                echo '<meta name="robots" content="noindex,nofollow">';
            }
        }
        ?>
        <title>{!!($finalData['header']['title'])?$finalData['header']['title'] :$finalData['site_title']!!}</title>
        <meta name="description" content="{!!(isset($finalData['header']['description']))?$finalData['header']['description'] :''!!}">
        <!--All css's links-->     
        <link href="{{ url('/public/assets/css/views/header.css') }}" rel="stylesheet" type="text/css" />   
        <link rel="icon" type="image/png" href="{{ url('/public/assets/Frontend/img/favicon.png') }}" />
        <link href="{{ url('/public/assets/Frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/owl.theme.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/animated.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/jquery-ui.theme.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/main.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/prism.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/chosen.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/lightslider.css') }}" rel="stylesheet" type="text/css" />

        <!--All java script's links-->
        <script src="{{url('/assets/Frontend/js/moment.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery-ui.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/bootstrap.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/scrollIt.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/skrollr.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.nicescroll.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.wookmark.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/pana_grid.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/wow.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/select-all-delete.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/device.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/handleCounter.js')}}"></script>
        
        
        
        @if (Request::segment(1) == 'profile' || Request::segment(1) == 'user-profile')
        <!-- Profile.js for twitter  section on company profile page -->
        <script src="{{url('/assets/Frontend/js/profile.js')}}"></script>
        @else
        <script src="{{url('/assets/Frontend/js/custom.js')}}"></script>
        @endif
        <?php $user_account = Session::get('user_account'); ?>
        <script src="{{url('/assets/Frontend/js/jquery.validate.min.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/registration/registration.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/jquery.validate.password.js')}}"></script>
        <!-- <script src="{{url('/assets/Frontend/js/jquery-ui.js')}}"></script> -->
        <!-- <script src="{{url('/assets/Frontend/js/jquery-ui.min.js')}}"></script> -->
        <script src="{{url('/assets/Frontend/js/chosen.jquery.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/sweetalert-dev.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/ajaxfunctions/ajax-feeds-functions.js')}}"></script>
        <script src="{{url('/assets/Frontend/js/job/job.js')}}"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('G_MAPS_KEY') }}&libraries=places"></script>
        <script src="{{url('/assets/Backend/js/jquery.geocomplete.min.js')}}"></script>
        <link rel="manifest" href="{{ url('/manifest.json') }}">
        <script src="{{url('/assets/Frontend/js/webpushnotification.js')}}"></script>
        <script>
        window.vapid_public_key="{{env('VAPID_PUBLIC_KEY')}}";
        window.user_id="{{$user_account['id']}}";
        </script>

        <!-- Event Meetup SEO -->
        <?php if(isset($strHead)){?>
        <?php echo $strHead;?>
        <?php }?>

        <script src="{{url('/assets/Frontend/js/lightslider.js')}}"></script>

        <script>
            $(document).ready(function () {
                $("#content-slider").lightSlider({
                    loop: true,
                    item: 2,
                    keyPress: true,
                    pager: false,
                });

                if (typeof bodyonload === 'function') {
                    bodyonload();
                }
            });
            
                  
            
         
        </script>

        <script id="fr-fek">
            try {
                (function (k) {
                    localStorage.FEK = k;
                    t = document.getElementById('fr-fek');
                    t.parentNode.removeChild(t);
                })('2wcD-16A2mtnqz==')
            } catch (e) {
            }
        </script>

    </head>
    <input type="hidden" id="base_url" name="base_url" value="{!! url('/') !!}">
