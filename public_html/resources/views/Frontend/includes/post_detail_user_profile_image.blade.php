<div class="art-user-infor even-post-art">
<div class="user-art-media">
<div class="media">

<div class="media-left">
    <div class="pos-art-profile">
        <!-- Fetching user image location & rendering user image if it exists; else rendering a  default image -->
        <?php
        $user_profile_image = config('profile_path'). $arr_user1_data['id'] . '/thumb/' . $arr_user1_data['profile_picture'];
        if (Storage::exists($user_profile_image) && $arr_user1_data['profile_picture'] != '') {
            $profile_picture = config('profile_pic_url').$arr_user1_data['id'] . '/thumb/' . $arr_user1_data['profile_picture'];
        } else {
            if ($arr_user1_data['gender'] == 0) {
                $profile_picture = config('avatar_male');
            } else {
                $profile_picture = config('avatar_female');
            }
        }
        ?>

        <!-- If user is logged in; we link name to profile ; else show sign in / sign up popup -->
        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user1_data['id'])}}" title="Visit Page" data-placement="left">
            <img src="{{$profile_picture}}" alt="profile"/>
        </a>
        <!--<div class="sas-counter" id='follower_count'>{{$arr_follower['0']->follower}}</div>--> 
    </div>
</div>

<!-- Show the name of the user who posted the post -->
<div class="media-body">
    <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user1_data['id'])}}" title="Visit Page" data-placement="left">
        <h4 class="media-heading">{{($arr_user1_data['first_name'].' '.$arr_user1_data['last_name'])}}</h4>
    </a>
</div>


<!-- Follow user section -->
<div class="green-sterp clearfix">
    @if ($session_data != "")
        @if($arr_user1_data['id'] != $arr_user_data['id'])
          <span class="pull-left">
            <a  href='javascript:void(0)'  onclick="addFollower('{{$arr_user1_data['id']}}')" id="follow" @if(count($is_follower) == 0) title="Click to follow" @else title="Click to unfollow" @endif>
            @if(count($is_follower) == 0) follow {{$arr_user1_data['first_name']}}  @else unfollow {{$arr_user1_data['first_name']}} @endif
            </a>
          </span>
        @else
          <span class="audience_count"></span>
        @endif
    @else
    <span class="pull-left"><a  href='javascript:void(0)' data-toggle="modal" data-target="#myModal" title="Click to follow">follow {{$arr_user1_data['first_name']}}</a></span>
    @endif
</div>

</div>
</div>


<div class="art-user-date">                                        
    <span class="pull-left"><i class="icon-calendar"></i> {{date("d M Y", strtotime($time))}}</span>
    <span class="pull-right"><i class="icon-Time"></i> {{date("g:i A", strtotime($time))}}</span>
    @if($arr_user1_data['id'] != $arr_user_data['id'])
    <span class="audience_count"></span>
    @else
    </div> <div class="art-user-date">
    @endif
</div>

</div>
