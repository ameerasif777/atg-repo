<script type="text/javascript">
    var myVar;
    function myFunction() {
        myVar = setTimeout(function () {
            $('.alert').slideUp(1000);
        }, 5000);
    }
    function myStopFunction() {
        clearTimeout(myVar);
    }

    $(document).ready(function () {   
        setTimeout(function () {
            $('.alert').slideDown(1000);
            myFunction();
        }, 100);

        $('.alert').mouseover(function () {
            myStopFunction();
        });
        
        $('.alert').mouseout(function () {
            myFunction();
        });
        $('.alert .close').click(function () {
            $('.alert').slideUp(1000);
        });
    });

</script>
<style type="text/css">
    /*--ERROR MESSAGES START--*/
    .custom-error{
        position:fixed;
        top:0;
        width:100%;
        z-index:9999;
        left:0;
    }
    .custom-error .alert{
        text-align:center;
        border-radius:0 0 10px 10px;
        border:none;
        box-shadow:none;
        width:80%;
        margin:0px auto;
    }
    
 .custom-error .alert-one{
  text-align:center;
  background: #38c591 none repeat scroll 0 0;
  border-radius: 0 0 10px 10px;
  color: #ffffff !important;
  margin: 0 auto;
  padding: 15px;
  width: 80%;
}
     .custom-error .alert-one-success{
        background:#00a940 !important;
        color:#fff;
         
    }
    .custom-error .alert-success{
        background:#00a940 !important;
        color:#fff;
    }
    .custom-error .alert-danger{
        background:#e35238 !important;
        color:#fff;
    }
    .custom-error .alert-warning{
        background:#DF7401 !important;
        color:#fff;
    }
    .custom-error .alert-info{
        background:#013ADF !important;
        color:#fff;
    }
    .custom-error .close{
        color:#fff;
        opacity:1;
    }
    .custom-error .close-one{
        color:#fff;
        opacity:1;
    }
    /*--ERROR MESSAGES END--*/
</style>
<div class="custom-error">  
    @if(Session::has('error_msg'))    
    <div class="alert alert-danger" style="display: none;">
        <button type="button" class="close">&times;</button>
         {!! Session::get('error_msg') !!}
    </div>  
    @endif          
    @if(Session::has('success_msg') || (isset($message) && $message != ''))
    <div class="alert alert-success" style="">
        <button type="button" class="close">&times;</button>
        <strong><i class="fa fa-smile-o"></i></strong>  {!! isset($message) ? $message : Session::get('success_msg') !!}
    </div>
    <?php Session::forget('success_msg'); ?>
    @endif          
    
    @if(Session::has('success_msg_resend_mail') != '')
    <div class="alert-one alert-success" style="">
        <button type="button" class="close" onclick="this.parentElement.style.display='none';">&times;</button>
         {!! Session::get('success_msg_resend_mail') !!}
    </div>
    @endif          
    @if(Session::has('warning_msg'))
    <div class="alert alert-warning" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong><i class="icon-warning-sign"></i> Warning !</strong>  {!! Session::get('warning_msg') !!}
    </div>
    @endif
    @if(Session::has('warning_msg_permanent'))
    <div class="alert-one alert-warning" style="">
        <button type="button" class="close" onclick="this.parentElement.style.display='none';">&times;</button>
          {!! Session::get('warning_msg_permanent') !!}
    </div>
    @endif 
</div>


