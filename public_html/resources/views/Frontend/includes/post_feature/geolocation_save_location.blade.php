<script>
    var map;
    var infowindow = new google.maps.InfoWindow();
    var marker;

    function initialize() {

            $("#venue").geocomplete({
                detailsAttribute: "data-geo",
                types: [
                    'establishment','geocode'
                ]
            }).bind("geocode:result", function (event, result) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': result.formatted_address}, function (results, status) {
                    if (typeof (marker) != "undefined" && marker !== null) {
                        marker.setMap(null);
                    }

                    /* Retrieve location and lat/long from GAPI */
                    for (var i = 0; i < results[0].address_components.length; i++) {
                        var addressType = results[0].address_components[i].types[0];
                        if (addressType == 'locality') {
                            var val = results[0].address_components[i].long_name;
                            $("input[name=location]").val(val);
                            break;
                        }
                        if (addressType == 'administrative_area_level_2') {
                            var val = results[0].address_components[i].long_name;
                            $("input[name=location]").val(val);
                            break;
                        }
                        if (addressType == 'administrative_area_level_1') {
                            var val = results[0].address_components[i].long_name;
                            $("input[name=location]").val(val);
                            break;
                        }
                        if (addressType == 'country') {
                            var val = results[0].address_components[i].long_name;
                            $("input[name=location]").val(val);
                            break;
                        }
                    }

                    var location = results[0].geometry.location;
                    var lat = location.lat();
                    var lng = location.lng();
                    $("input[name=latitude]").val(lat);
                    $("input[name=longitude]").val(lng);
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, lng),
                        map: map,
                        draggable: true,
                    });
                    map.panTo(new google.maps.LatLng(lat, lng));
                    google.maps.event.addListener(marker, 'dragend', function () {
                        geocodePositionAdd(marker.getPosition());
                    });
                })
            });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    function geocodePositionAdd(pos) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({latLng: pos}, function (results, status) {
            var location = results[0].geometry.location;
            $("input[name=latitude]").val(location.lat());
            $("input[name=longitude]").val(location.lng);

            if (status == google.maps.GeocoderStatus.OK) {
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
                $('#venue').val(results[0].formatted_address);
            }
        });
    }
</script>
