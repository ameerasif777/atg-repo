<script>

$("#location").geocomplete({
    details: ".geo-details",
    autoselect: true,
    detailsAttribute: "data-geo",
    types: [
        'establishment','geocode'
    ]
});

$("#location").bind("geocode:result", function (event, result) {
var geocoder = new google.maps.Geocoder();
geocoder.geocode({'address': result.formatted_address}, function (results, status) {
    var location = results[0].geometry.location;
    var lat = location.lat();
    var lng = location.lng();
    $("input[name=latitude]").val(lat);
    $("input[name=longitude]").val(lng);

});
});

</script>