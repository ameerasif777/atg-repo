<link href="{{ url('/public/assets/css/views/signupinmodal.css') }}" rel="stylesheet" type="text/css" />
<header class="outer-header">
    <nav class="outer-custom-nav clearfix">
        <div class="nav-outer">
            <div class="logo"><a href="{!! url('/') !!}" ><img src="{{url('/public/assets/Frontend/img/logo01.png')}}" alt="SITE-LOGO"/></a></div>
        </div>
        <div class="links">
            <div class="menu-slp">
                <input type="checkbox" href="#" class="menu-open" name="menu-open" id="menu-open" />
                <label class="menu-open-button" for="menu-open">
                    <img src="{{url('/public/assets/Frontend/img/Share.png')}}" alt="share" title="Social links"/>
                </label>
                <a target="_blank" href="{{url($finalData['global']['facebook_link'])}}" class="menu-social blue-face"> <i class="fa fa-facebook-f"></i> </a>
                <a target="_blank" href="{{url($finalData['global']['google_plus_link'])}}" class="menu-social google-pluss"> <i class="fa fa-google-plus"></i> </a>
                <a target="_blank" href="{{url($finalData['global']['twitter_link'])}}" class="menu-social tweter-sol"> <i class="fa fa-twitter"></i> </a>
                <a target="_blank" href="{{url($finalData['global']['linkedin_link'])}}" class="menu-social linkeed"> <i class="fa fa-linkedin"></i> </a>
                <a target="_blank" href="{{url($finalData['global']['instagram_link'])}}" class="menu-social insta-icon"><i class="fa fa-instagram"></i> </a>
            </div>
        </div>
        <div class="users-one">
            <div class="switch-button">

                <button class="switch-button-case left active-case" data-toggle="modal" data-target="#myModal">Sign In / Sign Up</button>

            </div>
        </div>
    </nav>

    <div class="models-cust">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <ul role="tablist" class="nav nav-tabs" id="myTabs">
                                    <li class="active cust-devider" role="presentation"><a aria-expanded="true" aria-controls="log-in" data-toggle="tab" role="tab" id="log-in-tab" href="#log-in">SIGN IN</a></li>
                                    <li role="presentation"><a aria-controls="reg-in" data-toggle="tab" id="reg-in-tab" role="tab" href="#reg-in">SIGN UP</a></li>
                                </ul>


                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form id="frm_login" name="frm_login" action="{{url('/signin')}}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Sign In with social</div>
                                                <div class="cust-solc">
                                                    <ul class="clearfix">
                                                        <li><a href="{{url('/facebook')}}"><i class="fa fa-facebook-f"></i></a></li>
                                                        <li><a href="{{url('/gmail')}}"><i class="fa fa-google-plus"></i></a></li>
                                                        <li><a href="{{url('/twitter')}}"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="or-head">Or</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" name="email" value="{{ (isset($cookie_data['email'])) ? $cookie_data['email'] : '' }}" placeholder="Email address">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password"  id="password" name="password"  class="form-control" value="{{ (isset($cookie_data['password'])) ? $cookie_data['password'] : '' }}"  placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="forg-remi clearfix">
                                                <div class="reminder-me">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="remember" name="remember" <?php if (!empty($cookie_data)) { ?>checked="checked" <?php } ?>>Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="forgot-pass"><a href="javascript:void(0);" onClick="getEmailId();" data-toggle="modal" data-target="#myForgotModal">Reset Password</a></div>
                                            </div>
                                            <div class="btns-loger"><input type="hidden" id="redirect_uri" name="redirect_uri" value="{{ Request::path() }}"><button type="submit" class="btn log-btn">Sign in</button></div>
                                        </form> 
                                    </div>
                                    <div aria-labelledby="reg-in-tab" id="reg-in" class="tab-pane fade" role="tabpanel">
                                        <div class="inner-socials">
                                            <div class="sol-head">Sign Up with social</div>
                                            <div class="cust-solc">
                                                <ul class="clearfix">
                                                    <li><a href="{{url('/facebook')}}"><i class="fa fa-facebook-f"></i></a></li>
                                                    <li><a href="{{url('/gmail')}}"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="{{url('/twitter')}}"><i class="fa fa-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="or-head">Or</div>
                                        </div>
                                        <?php Session::put('ruser', Request::get('r', '')); ?>
                                        <form id="frm_user_registration1" name="frm_user_registration1" method="POST" action="{{url('/signup1?r=' . Request::get('r'))}}"  enctype="multipart/form-data">
                                            {!!csrf_field()!!}
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{old('first_name')}}">
                                                    @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{old('last_name')}}">
                                                    @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Address" value="{{old('user_email')}}">
                                                    @if ($errors->has('user_email'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="user_password" name="user_password"  placeholder="password" value="{{old('user_password')}}">
                                                    <!--<div class="reg-info">(Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters)</div>-->
                                                    @if ($errors->has('user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="cnf_user_password" name="cnf_user_password" placeholder="Confirm Password">
                                                    @if ($errors->has('cnf_user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('cnf_user_password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>                             
                                            </div>

                                            <div class="reg-info">By clicking on the 'Sign up' button you agree to the <a href="{{url('/cms/terms-services')}}"> Terms and conditions </a> and our<a href="{{url('/cms/privacy')}}"> privacy policy</a></div>

                                            <div class="btns-loger">

                                                <button type="submit" name="btn_register" id="btn_register" class="btn log-btn">Sign up</button></div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalForget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form id="frm_login" name="frm_login" action="{!!url('/signin')!!}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Signin with social</div>
                                                <div class="cust-solc">
                                                    <ul class="clearfix">
                                                        <li><a href="{!!url('/facebook')!!}"><i class="fa fa-facebook-f"></i></a></li>
                                                        <li><a href="{!!url('/gmail')!!}"><i class="fa fa-google-plus"></i></a></li>
                                                        <li><a href="{!!url('/twitter')!!}"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="or-head">Or</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email address">
                                                </div>
                                                <div class="form-group">

                                                    <input type="password"  id="password" name="password"  class="form-control"   placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="forg-remi clearfix">
                                                <div class="reminder-me">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="remember" name="remember" value="1"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="forgot-pass"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModalForget">Forgot Password?</a></div>
                                            </div>

                                            <div class="btns-loger"><button type="submit" id="reset_button" class="btn log-btn">Sign in</button></div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="models-cust">
        <div class="modal fade" id="myForgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form id="frm_forgot_send" name="frm_forgot_send" action="{!!url('/resetPassword')!!}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Forget Password</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="forgot_email" name="email" placeholder="Email address">
                                                </div>
                                            </div>
                                            <div class="btns-loger"><button type="submit"  class="btn log-btn" >Reset</button></div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('user_code'))
<!--            <p class="alert alert-success">{{ Session::get('user_code') }}<a class="close">&times;</a></p>-->

    <div class="models-cust">
        <div class="modal fade" id="myResetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form action="{{url('/')}}/front/reset-password" method="post" name="frm_user_reset" id="frm_user_reset" >
                                            {!!csrf_field()!!}
                                            <input type="hidden" name="activation_code" value="{{Session::get('user_code')}}"/>
                                            <div class="inner-socials">
                                                <div class="sol-head">Reset Password</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input  class="form-control" autofocus type="password" name="user_password" size="" id="main_password" value="" placeholder="New Password" >                    
                                                </div>
                                                <div class="form-group">
                                                    <input  type="password" class="form-control" name="confim_user_password" id="confim_user_password" value="" placeholder="Confirm Password" >      
                                                </div>
                                            </div>
                                            <div class="btns-loger"><button type="submit"  class="btn log-btn" >Reset</button></div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
    <div class="sole-popup">
        <div class="modal fade" id="myModal-one" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
                    </div>
                    <div class="modal-body">
                        <a  href="{!!url('/facebook')!!}" class="btn face-hov"><i class="fa fa-facebook-f"></i> Sign in with Facebook</a>
                        <a href="{!!url('/gmail')!!}" class="btn g-plus"><i class="fa fa-google-plus"></i> Sign in with Google+</a>
                        <a href="{!!url('/twitter')!!}" class="btn twit-hov"><i class="fa fa-twitter"></i> Sign in with Twitter</a>
                        <a class="btn twit-hov" onClick="email_modal()" > Sign in with email and password</a>
                        <div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '124713081587922'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=124713081587922&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<!-- Google Analytics Code for Logged out user for -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', '{{ env('G_ANALYTICS_OUTER_HEADER_KEY') }}', 'auto');
  ga('set', 'dimension1', '0');
  ga('send', 'pageview');
</script>
</header>
<style>
    .contact-sec {
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        margin-top: 64px;
        padding: 40px 0;
        position: relative;
        z-index: 1;
    }
</style>

<script>
    function email_modal() {
        $("#myModal-one").hide();
        $("#myModal").modal("show");
    }
    $(document).ready(function() {
        $('#frm_user_registration1')[0].reset();
    });
    function getEmailId() {
        var email = $('#email').val();
        $('#forgot_email').val(email);
    }
    $(window).load(function() {
        $('.grey-overlay-loader').hide();

    });
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

@if(isset($sign_up) && $sign_up == true)
<script>
    $('#myModal>div>div>div>div>div>ul li:first').removeClass('active');
    $('#myModal>div>div>div>div>div>ul li:last').addClass('active');
    $('#myModal>div>div>div>div>div>ul li:first>a').removeAttr('aria-expanded');
    $('#myModal>div>div>div>div>div>ul li:last>a').attr('aria-expanded','true');
    $('#myModal>div>div>div>div>div>div #log-in').removeClass('in active');
    $('#myModal>div>div>div>div>div>div #reg-in').addClass('in active');
</script>
@endif
