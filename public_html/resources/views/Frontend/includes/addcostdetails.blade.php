<div class="background-fade" tabindex="-1" aria-hidden="true">
    <div class="row popups">

        <div class="main-popup" id="mainwindow">
            <div class="popup-header">
                Cost
            </div>
            <div class="load-track"><div class="animload"></div></div>
            <div class="popup-body">        
                    <div class="disp">
                        <div class="body-part1">
                                <div class="chkboxdes" onclick="chk(1)">
                                        <div class="chkbox">
                                        <input type="checkbox" value="None" id="chkbox1" name="free_check" <?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) == 0) && (isset($arr_event_details) && $arr_event_details[0]->cost == '0')) ? 'checked="checked"' : ''; ?> />
                                        <label for="chkbox"></label>
                                        </div>
                                        <div class="title">Free</div>                                                                           
                                </div>           
                        </div>
                        <div class="ordig">OR</div>
                        <div class="body-part1">
                                <div class="chkboxdes" onmouseup="autonext(2)" onmousedown="chk(2)">
                                        <div class="chkbox">
                                        <input type="checkbox" value="None" id="chkbox2" name="cost_check" <?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) > 0) || (isset($arr_event_details) && $arr_event_details[0]->cost != '' && $arr_event_details[0]->cost != '0')) ? 'checked="checked"' : ''; ?> />
                                        <label for="chkbox"></label>
                                        </div>
                                        <div class="title">Paid</div>                                    
                                </div>
                        </div>
                    </div>
<input type="hidden" value="<?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) == 0) && ($arr_event_details[0]->cost == '0')) ? 'on' : 'off'; ?>" name="free_checkbox" />
<input type="hidden" value="<?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) > 0) && ($arr_event_details[0]->cost != '0')) ? 'on' : 'off'; ?>" name="cost_checkbox" />
                    <div class="disp">
                            <div class="body-part1">
                                    <div class="chkboxdes" onmouseup="autonext(3)" onmousedown="chk(3)">
                                            <div class="chkbox">
                                            <input type="checkbox" value="None" id="chkbox3" name="single_cost" <?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) == 1) && ($arr_event_details[0]->cost != '0')) ? 'checked="checked"' : ''; ?> />
                                            <label for="chkbox"></label>
                                            </div>
                                            <div class="title">Single Cost</div>
                                        
                                            
                                    </div>           
                            </div>
                            <div class="ordig">OR</div>
                            <div class="body-part1">
                                    <div class="chkboxdes" onmouseup="autonext(4)" onmousedown="chk(4)">
                                            
                                            <div class="chkbox">
                                            <input type="checkbox" value="None" id="chkbox4" name="multiple_cost" <?php echo ((isset($arr_cost_detail) && count($arr_cost_detail) > 1) && ($arr_event_details[0]->cost != '0')) ? 'checked="checked"' : ''; ?>/>
                                            <label for="chkbox"></label>
                                            </div>
                                            <div class="title">Multiple Cost</div>
                                        
                                    </div>
                            </div>
                    </div>
                   <div class="disp">
                        <div class="singlecost">
                            <div class="body-part1">
                                <div class="even-det">
                                    <div class="col-md-6">
                                        <select class="form-control" name="currency" id="currency" onchange="$('#advanced_opt_currency').val($(this).val()); ">
                                            <option value="₹" <?php echo (isset($arr_cost_detail[0]) && $arr_cost_detail[0]->currency == '₹') ? 'selected' : '' ?>>₹</option>
                                            <option value="$" <?php echo (isset($arr_cost_detail[0]) && $arr_cost_detail[0]->currency == '$') ? 'selected' : '' ?>>$</option>
                                        </select>
                                    </div>   
                                    <div class="col-md-6">
                                        <input type="text" class="form-control offset-bot-15"  id="cost" name="cost" placeholder="Cost" onchange="$('#advanced_opt_cost').val($(this).val()); $('#advanced_opt_category').val('single');" value="{{ (isset($arr_cost_detail[0]) && $arr_cost_detail[0]->cost != '0' ) ? $arr_cost_detail[0]->cost : ''}}" >
                                        <div for="cost" class="errorofcost" style="display:none;color: red;font-size: 13px;">Please Enter Valid Cost. Two points after decimal</div>        
                                    </div> 
                                </div>
                            </div>
                        </div>
                       <input id="add_mpre_cost_total" name="add_mpre_cost_total" type="hidden" value="{{(isset($arr_cost_detail) && count($arr_cost_detail)>0) ? count($arr_cost_detail) : '0'}}">
                        <div class="multiplecost">
                            <div class="body-part2">
                                @if(isset($arr_cost_detail) && count($arr_cost_detail)>0)
                                <?php $counter = 1?>
                                @foreach($arr_cost_detail as $key => $cost)
                                <div class="even-det">
                                    <input class="form-control offset-bot-15" type="text" name="advanced_opt_category[{{$key}}]" id="advanced_opt_category" placeholder="Category">
                                    <textarea class="form-control offset-bot-15" name="advanced_opt_description[{{$key}}]" id="advanced_opt_description" placeholder="Description"></textarea>
                                    <div class="even-det">
                                        <div class="col-md-3">
                                            <select class="form-control" name="advanced_opt_currency[{{$key}}]" id="advanced_opt_currency">
                                                <option value="₹">₹</option>
                                                <option value="$">$</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control offset-bot-15" placeholder="Cost"  id="advanced_opt_cost{{$counter}}" name="advanced_opt_cost[{{$key}}]" placeholder="Cost" value="{{$cost->cost}}">
                                            <div class="advanced_cos_error" style="display: none;color:red;">Please Enter Valid Cost.Two Points after decimal</div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <input readonly type="text" class="form-control cost-last-date offset-bot-15" placeholder="Last Date"  id="advanced_opt_last_date{{$counter}}" name="advanced_opt_last_date[{{$key}}]"  value="{{$cost->last_date}}">
                                                <span class="input-group-addon" id="basic-addon2"  ><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>    
                                    </div>
                                </div>
                                <?php $counter++;?>
                                @endforeach
                                @else
                                <div class="even-det">
                                    <input class="form-control offset-bot-15" type="text" name="advanced_opt_category[0]" id="advanced_opt_category" placeholder="Category">
                                    <textarea class="form-control offset-bot-15" name="advanced_opt_description[0]" id="advanced_opt_description" placeholder="Description"></textarea>
                                    <div class="even-det">
                                        <div class="col-md-3">
                                            <select class="form-control" name="advanced_opt_currency[0]" id="advanced_opt_currency">
                                                <option value="₹">₹</option>
                                                <option value="$">$</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control offset-bot-15" placeholder="Cost"  id="advanced_opt_cost" name="advanced_opt_cost[0]" placeholder="Cost">
                                            <div class="advanced_cos_error" style="display: none;color:red;">Please Enter Valid Cost.Two Points after decimal</div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="input-group">
                                                <input readonly type="text" class="form-control cost-last-date offset-bot-15" placeholder="Last Date"  id="advanced_opt_last_date" name="advanced_opt_last_date[0]">
                                                <span class="input-group-addon" id="basic-addon2"  ><i class="icon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>    
                                    </div>
                                </div>
                                @endif
                                <div id="add_more_cost"></div>
                                <div id="add_more_buttons" class="even-det">
                                    <div class="col-md-6">
                                        <a href="javascript:void(0)" id="btAdd" class="btn" ><i class="icon-Add"></i></a>
                                    </div>
                                    <div class="col-md-6">

                                        <a href="javascript:void(0)" id="btRemove"  class="btn"><i class="icon-Cancel"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<input type="hidden" value="<?php echo (isset($arr_event_details)) ? $arr_event_details[0]->onlineTicket : ''; ?> " name="onlineflag" />
                    <div class="disp">
                        <div class="body-part1">
                                <div class="chkboxdes"  onclick="chk(5)" id="chkboxmsg5">
                                        <div class="chkbox">
                                        <input class="form-control" type="checkbox" value="online" id="chkbox5" name="chkbox5" <?php echo (isset($arr_event_details) && $arr_event_details[0]->onlineTicket == '1') ? 'checked="checked"' : ''; ?>  />
                                        <label for="chkbox"></label>
                                        </div>
                                        <div class="title">Online</div>
    
                                        
                                </div>  
                         
                        </div>
                        <div class="message" id="online">You will be charged 4% comission on every sale. 2% are bank charges and 2% convenience fees.</div>

                        <div class="ordig">OR</div>
                        <div class="body-part1">
                                        
                                <div class="chkboxdes" onclick="chk(6)" id="chkboxmsg6">
                                        
                                        <div class="chkbox">
                                       <input class="form-control" type="checkbox" value="offline" id="chkbox6" name="chkbox6" <?php echo (isset($arr_event_details) && $arr_event_details[0]->onlineTicket == '0') ? 'checked="checked"' : ''; ?> />
                                        
                                        <label for="chkbox"></label>
                                        </div>
                                        <div class="title">Offline</div>
                                    
                                </div>
                        </div>
                        <div class="message" id="offline">Get Pay at your Event Venue</div>
                    </div>
                


                    
            </div>
            <div class="popup-footer">
               
                <div class="col-sm-6">
                    <button id="prevbtn" type="button" onclick="nextprev(-1,0)"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="col-sm-6">
                    <button id="nextbtn" type="button" onclick="nextprev(1,1)" >Close</button>
                </div>
            </div>


        </div>
    </div>

</div>