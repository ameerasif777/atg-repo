<div class="venue-event-art ">
    <h2>Related {{$post_name}}</h2>
    <div class="relar-job-posr mCustomScrollbar">
        <ul>
            @foreach($related_posts as $value)
            <li>
                @if($post_name == 'Articles')
                <a href="{{url('/')}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{!!$value->id!!}"><h3>{{ $value->title }}</h3></a>
                @elseif($post_name == 'Education')
                <a href="{{url('/')}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{!!$value->id!!}"><h3>{{ $value->title }}</h3></a>
                @elseif($post_name == 'Questions')
                <a href="{{url('/')}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{!!$value->id!!}"><h3>{{ $value->title }}</h3></a>
                @elseif($post_name == 'Meetup')
                <a href="{{url('/')}}/view-meetup/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->name) ?>-{!!$value->id!!}"><h3>{{ $value->name }}</h3></a>
                @elseif($post_name == 'Events')
                <a href="{{url('/')}}/view-event/{!!$value->name!!}-{!!$value->id!!}"><h3>{{ $value->name }}</h3></a>
                @endif
            </li>
            @endforeach
        </ul>
    </div>
</div>