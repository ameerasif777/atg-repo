<div class="media">
    <div class="media-up" id="{{ $value->id }}" style='float:left;'>
        @if($session_data != "")
            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->commented_by)}}" title="Visit Page" data-placement="left">
                <div class="post-ari-img"></span><img src="{{config('profile_pic_url').$value->commented_by.'/thumb/'.$value->profile_picture}}" alt="profile" onerror="src={{config('avatar_male')}}"/></div>
            </a>
        @else
                <a class="more" title="Visit Page" data-placement="left">
                    <div class="post-ari-img">
                    <img  id="main_profile" src="{{config('profile_pic_url').$value->commented_by.'/thumb/'.$value->profile_picture}}" alt="profile" onerror="src={{config('avatar_male')}}"/>
                    </div>
                </a>
        @endif
    </div>
   <!-- <div class='media-up-right'>-->
    
        <h4 class="media-heading">
        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->commented_by)}}" title="Visit Page" style="color:black;">
        <?php if ($value->user_name != ''): 
                 echo $value->user_name ;  
              else:
             echo $value->first_name ;
              endif;   
        ?>
    </a>
    <span id="onbeforedate"> on <span>
    <span class="date_display">
          {{  date("l, jS F Y", strtotime("$value->created_at")) }}
     </span> 
     <span class="pull-right"></span>
    </h4>
   <!--</div>-->
    <div class="media-body2">                                         
        
        <p style="text-align: justify; padding-right: 5%;">{!! $value->comment !!}</p>