<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/image.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/video.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/fullscreen.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/char_counter.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/code_view.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/help.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/froala_style.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/froala_gray.min.css')}}" rel='stylesheet' type='text/css' />
<link href="{{url('/assets/Frontend/css/froala/table.min.css')}}" rel='stylesheet' type='text/css' />
<!-- Include JS file. -->
<!-- Include JS file. -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/js/froala_editor.min.js'></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/image.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/video.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/fullscreen.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/char_counter.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/link.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/code_view.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/help.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/table.min.js')}}"></script>
<script type='text/javascript' src="{{url('/assets/Frontend/js/froala/url.min.js')}}"></script>

<script>
$(document).ready(function () {
    $('textarea#description').froalaEditor({
        imageUploadURL: "{{ url('upload-image?folder=' . $folderName) }}",
        imageStyles: {
            class1: 'img-responsive',
            class2: 'center-fr-image'
        },
        toolbarButtons: [
            'fullscreen', 'bold', 'italic', 'strikeThrough', 'subscript', 'superscript',
            'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent',
            'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'insertTable', 'undo', 'redo', 'help', //'html'
        ],
        toolbarButtonsMD: [
            'fullscreen', 'bold', 'italic', 'strikeThrough', 'subscript', 'superscript',
            'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent',
            'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'insertTable', 'undo', 'redo', 'help', //'html'
        ],
        toolbarButtonsSM: [
            'fullscreen', 'bold', 'italic',
            'paragraphFormat', 'formatOL', 'formatUL',
            'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'insertTable', 'undo', 'redo', 'help', //'html'
        ],
        toolbarButtonsXS: [
            'fullscreen', 'bold', 'italic',
            'insertImage', 'insertLink', 'insertFile', 'insertVideo', 'insertTable', 'help', //'html'
        ],
        heightMin: 250,
        heightMax: 700,
        charCounterMax: 4000,
        pastePlain: false,
        theme: 'gray',
        imageEditButtons: ['imageReplace', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-']
    });
    $('textarea#description').on('froalaEditor.contentChanged', function (e, editor) {
        $('#description').val($(this).froalaEditor('html.get'));
    });
});
</script>
