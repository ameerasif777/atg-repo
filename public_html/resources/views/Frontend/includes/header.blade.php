<?php
$data['user_session'] = Session::get('user_account');
$user_id = $data['user_session']['id'];
$arr_messages = DB::table('trans_ims_replies');
$arr_messages->where('message_to', $user_id);
$arr_messages->where('is_read', '0');
$arr_messages->select(DB::raw('COUNT(message_to) as message_counter'));
$arr_messages = $arr_messages->get();
$unread_messages_count=$arr_messages[0]->message_counter;
Session::put('unread_messages_count', $unread_messages_count);
?>
<script>
function onloadfunc()
{
    $('#row-main').css('height',window.innerHeight - $('.in-head').outerHeight());
    $('.sidebar').css('height', window.innerHeight  - $('.in-head').outerHeight());
    $('.sidebar').css('min-height', window.innerHeight  - $('.in-head').outerHeight());
    //$('#content').css('padding-left', '$('.sidebar').width() '); 
}

var sidebar_width;

$(window).resize(function () {
    bodyonload();
});    


function bodyonload() {   
    onloadfunc();
    if(window.innerWidth >= 767) // For Desktop version
    {           
        var sidebar_val = localStorage.getItem('sidebar');
        if(sidebar_val == 'min')
        {      
            $("#sidebar-exp").addClass("active");
            $("#sidebar-short").removeClass("active");
            sidebar_width = $('#sidebar-short').width();
            $('#content').css('padding-left', sidebar_width + 20);  
            $('.sidebar').css('display', 'block');
            $('#content').css('display', 'block');  
        }
        else
        {
            $("#sidebar-exp").removeClass("active");
            sidebar_width = $('#sidebar-exp').width();
            $('#content').css('padding-left', sidebar_width + 20);
            $('.sidebar').css('display', 'block');
            $('#content').css('display', 'block');  
        }  
        
    }
    else if(window.innerWidth <= 480 || (window.innerWidth > 480 && window.innerWidth < 767))    // For Mobile version
    {
        if($("#nav-icon1").hasClass('open'))
        {
            $("#sidebar-short").removeClass("active"); 
            $("#sidebar-exp").removeClass("active");
        }
        else
        {
            $("#sidebar-short").addClass("active"); 
            $("#sidebar-exp").addClass("active");  
        }
    }
}  
</script>

<body>
    <div class="bodycontent">
    <div class="in-head row">
        <nav class="custom-nav clearfix">
            <div class="nav-outer clearfix">
                <?php if ($data['user_session'] != '' && Request::segment(1) != 'signup2' && Request::segment(1) != 'signup3' && Request::segment(1) != 'signup3') { ?>      
                <div class="sidebarlogo">
                    <a href="javascript:void(0)" class="toggle-sidebar" >
                    <div id="nav-icon1">
                        <span></span>
                        <span></span>
                        <span></span>
                      </div>
                    </a>
                </div>
                <?php } ?>
                <div class="logo">
                    <?php if (Request::segment(1) != 'signup3' && Request::segment(1) != 'signup3') { ?>
                        <a href="{!!url('/user-dashboard')!!}"  ><img src="{{ url('/public/assets/Frontend/img/logo-in.png') }}" alt="SITE-LOGO"/></a>
                    <?php } else { ?>
                        <a href="javascript:void(0)"  ><img src="{{ url('/public/assets/Frontend/img/logo-in.png') }}" alt="SITE-LOGO"/></a>
                    <?php } ?>
                </div>
                <?php
                    if ($data['user_session'] != '') {
                        if (Request::segment(1) != 'signup2' && Request::segment(1) != 'signup3' && Request::segment(1) != 'signup3') {
                            ?>
                <div class="main-serching">
                <div class="pull-right ds-show">
                    <div class="input-group ds-show" data-toggle="modal" data-target="#searchModel">
                        <span class="input-group-addon"><i class="icon-search"></i></span>
                        <input type="text" class="form-control search-box-focus" placeholder="Search for things you love...">
                        <span class="input-group-addon cust-lan"><a href="javascript:void(0)" ><i class="icon-Dropdown-Arrow"></i></a></span>
                    </div>
                    <div class="input-group mb-show" data-toggle="modal" data-target="#searchModel">
                        <span class="input-group-addon"><i class="icon-search"></i></span>

                    </div>
                </div>
                </div>
                <table border='0' class="main-right-user">
                    <tr>
                        <td class="search_group_user"> 
                                    <a class="" data-toggle="modal" data-target="#searchModel">
                                        <i class="icon-search" style="font-size:25px" ></i>
                                    </a>
                        </td>
                        <td ><a href="{{url('/notification')}}"><i class="icon-bell" style="font-size:25px"  title="Notification"></i></a>@if(Session::get('unread_notification_count') != 0)<span class="hole-counte">@if(Session::has('unread_notification_count'))  {{Session::get('unread_notification_count')}}  @endif</span>@endif</td>
                        <td class="prof">
                            @if(isset($arr_user_data))
                            <a id="prof-name" href="{{url('/profile')}}"><b>{{ucfirst($arr_user_data['first_name'])}} {{ucfirst($arr_user_data['last_name'])}}</b></a>
                            @endif
                            
                            <a href="{{url('/profile')}}"  id="dLabel" >
                            <span class="user-profile">
                                @if(isset($arr_user_data['profile_picture']) && ($arr_user_data['profile_picture']) != '')
                                <img id="main_profile" src="{{ isset($arr_user_data['profile_picture']) ? config('profile_pic_url').$arr_user_data['id'].'/thumb/'.$arr_user_data['profile_picture']:''}}"  @if($arr_user_data['gender'] == 0) onerror="src={{ config('avatar_male') }};" @else onerror="src={{ config('avatar_female') }};" @endif alt="user-pros"/>
                                     @elseif(isset($arr_user_data) && $arr_user_data['gender'] == 0)
                                     <img id="main_profile" src="{{config('avatar_male')}}"  alt="user-pros"/>
                                @else
                                <img id="main_profile" src="{{config('avatar_female')}}" alt="user-pros"/>
                                @endif
                            </span>
                            </a>
                        </td>
                    </tr>
                </table>
                 <?php
                            }
                        }
                        ?>
            </div>
        </nav>
        
    </div>
  
    <div  class="row" id="row-main" >
    <?php if ($data['user_session'] != '' && Request::segment(1) != 'signup2' && Request::segment(1) != 'signup3' && Request::segment(1) != 'signup3') { ?>    
    <div  id="sidebar-short" class="sidebar active" style="">
        <ul>
            <li <?php if (Request::segment(1) == 'user-dashboard') { ?> class="active" <?php } ?>><a href="{!!url('/user-dashboard')!!}" class="btn btn-sidebar" title="Home"><i class="fa fa-home"></i></a></li>
            <li class = "pencil_icon_style" <?php if (Request::segment(1) == 'article' || Request::segment(1) == 'event' || Request::segment(1) == 'meetup' || Request::segment(1) == 'question' || Request::segment(1) == 'post-job' || Request::segment(1) == 'education') { ?> class="active" <?php } ?>><a  title="Write a Post" href="{{url('/article')}}" class="btn btn-sidebar"><i class="fa fa-pencil"></i></a></li>
<!--            <li <?php if (Request::segment(1) == 'make-connection') { ?> class="active" <?php } ?>><a href="{{url('/make-connection')}}" class="btn btn-sidebar" title="Connect"><i class="icon-connect icon-grey"></i></a></li>-->
            <li <?php if (Request::segment(1) == 'message') { ?> class="active" <?php } ?>><a href="{{url('/message')}}" class="btn btn-sidebar" title="Messages"><i class="fa fa-envelope"></i>@if(Session::get('unread_messages_count') != 0)<span class="msg-counter counter-min"> @if(Session::has('unread_messages_count'))  {{Session::get('unread_messages_count')}}  @endif</span>@endif</a></li>
            <li <?php if (Request::segment(1) == 'my-group' && Request::getQueryString() == '') { ?> class="active" <?php } ?> ><a href="{!!url('/my-group')!!}" class="btn btn-sidebar" title="My Groups"><i class="fa fa-users"></i></a></li>
            <li <?php if (Request::segment(1) == 'my-group' && Request::getQueryString() == 'explore') { ?> class="active" <?php } ?> ><a href="{!!url('/my-group?explore')!!}" class="btn btn-sidebar" title="Explore Groups"><i class="icon-explore icon-grey"></i></a></li>
            <li <?php if (Request::segment(1) == 'mypost') { ?> class="active" <?php } ?> ><a href="{!!url('/mypost')!!}" class="btn btn-sidebar" title="My Posts"><i class="fa fa-user"></i></a></li>
            <li <?php if (Request::segment(1) == 'invite-friends') { ?> class="active" <?php } ?> ><a href="{!!url('/invite-friends')!!}" class="btn btn-sidebar" title="Invite Friends"><i class="fa fa-user-plus"></i></a></li>
            <li <?php if (Request::segment(1) == 'calendar') { ?> class="active" <?php } ?>><a href="{{url('/calendar')}}" class="btn btn-sidebar" title="Upcoming"><i class="fa fa-calendar"></i>@if(Session::has('calender-count') && Session::get('calender-count') != 0 )<span id="calender-count-min"  class="msg-counter counter-min">  {{Session::get('calender-count')}} </span> @else <span id="calender-count-min"  ></span> @endif</a></li>
            <li <?php if (Request::segment(1) == 'edit-user-bio') { ?> class="active" <?php } ?>><a href="{{url('/edit-user-bio')}}" class="btn btn-sidebar" title="Settings"><i class="fa fa-cog"></i></a></li>
        </ul>
        <div class="sidebar-footer">
        <a href="javascript:void(0)" class="btn btn-arrow toggle-sidebar-right"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <div  id="sidebar-exp" class="sidebar active" style="" >
        <ul>
            <li <?php if (Request::segment(1) == 'user-dashboard') { ?> class="active" <?php } ?>>
                <a href="{!!url('/user-dashboard')!!}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-home"></i></li><li>Home</li>
                    </ul>
               </a>
            </li>
            <li <?php if (Request::segment(1) == 'article' || Request::segment(1) == 'event' || Request::segment(1) == 'meetup' || Request::segment(1) == 'question' || Request::segment(1) == 'post-job' || Request::segment(1) == 'education') { ?> class="active" <?php } ?>>
                <a class="btn btn-sidebar" data-toggle="collapse" href="#collapse1">
                    <ul>
                        <li class = "pencil_icon_style"><i class="fa fa-pencil"></i></li>
                        <li>Write a Post&nbsp;&nbsp;&nbsp;&nbsp;<i id="arrow" class="fa fa-angle-right" ></i></li>
                    </ul>
                </a>
                <div id="collapse1" class="panel-collapse collapse">
                    <ul class="submenu list-group">
                        <li class="list-group-item"><a href="{{url('/event')}}" class="btn btn-sidebar">Event</a></li>
                        <li class="list-group-item"><a href="{{url('/meetup')}}" class="btn btn-sidebar">Meetup</a></li>
                        <li class="list-group-item"><a href="{{url('/article')}}" class="btn btn-sidebar">Article</a></li>
                        <li class="list-group-item"><a href="{{url('/education')}}" class="btn btn-sidebar">Education</a></li>
                        <li class="list-group-item"><a href="{{url('/question')}}" class="btn btn-sidebar">Qrious</a></li>
                        @if(isset($arr_user_data) && $arr_user_data['user_type'] != 1)
                        <li class="list-group-item"><a href="{{url('/post-job')}}" class="btn btn-sidebar">Job</a></li>
                        @endif
                    </ul>
                </div>
            </li>
<!--            <li <?php if (Request::segment(1) == 'make-connection') { ?> class="active" <?php } ?>>
                <a href="{{url('/make-connection')}}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="icon-connect icon-grey"></i></li><li>Connect</li>
                    </ul>
                </a>
            </li>-->
            <li <?php if (Request::segment(1) == 'message') { ?> class="active" <?php } ?>>
                <a href="{{url('/message')}}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-envelope"></i></li>
                        <li>Messages @if(Session::get('unread_messages_count') != 0)<span class="msg-counter"> @if(Session::has('unread_messages_count'))  {{Session::get('unread_messages_count')}}  @endif</span>@endif</li>
                    </ul>
                </a> 
            </li>
            <li <?php if (Request::segment(1) == 'my-group'  && Request::getQueryString() == '') { ?> class="active" <?php } ?> >
                <a href="{!!url('/my-group')!!}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-users"></i></li>
                        <li>My Groups</li>
                    </ul>
                </a>
            </li>
            <li <?php if (Request::segment(1) == 'my-group' && Request::getQueryString() == 'explore') { ?> class="active" <?php } ?> >
                <a href="{!!url('/my-group?explore')!!}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="icon-explore icon-grey"></i></li>
                        <li>Explore Groups</li>
                    </ul>
                </a>
            </li>
            <li <?php if (Request::segment(1) == 'mypost') { ?> class="active" <?php } ?> >
                <a href="{!!url('/mypost')!!}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-user"></i></li>
                        <li>My Posts</li>
                    </ul>
                </a>
            </li>
            <li <?php if (Request::segment(1) == 'invite-friends') { ?> class="active" <?php } ?> >
                <a href="{!!url('/invite-friends')!!}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-user-plus"></i></li>
                        <li>Invite Friends</li>
                    </ul>
                </a>
            </li>
            <li <?php if (Request::segment(1) == 'calendar') { ?> class="active" <?php } ?> >
                <a href="{{url('/calendar')}}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-calendar"></i></li>
                        <li>Upcoming @if(Session::has('calender-count') && Session::get('calender-count') != 0 )<span id="calender-count"  class="msg-counter">  {{Session::get('calender-count')}} </span> @else <span id="calender-count" ></span> @endif</li>
                    </ul>    
                </a>
            </li>
            <li <?php if (Request::segment(1) == 'edit-user-bio') { ?> class="active" <?php } ?>>
                <a href="{{url('/edit-user-bio')}}" class="btn btn-sidebar">
                    <ul>
                        <li><i class="fa fa-cog"></i></li><li>Settings</li>
                    </ul>
                </a>
            </li>
            <li></li>
        </ul>
        <div class="sidebar-footer">
        <p><a href="{{url('cms/privacy')}}" style="display: inline-block">Privacy</a>
            &#8226;
            <a href="{{url('cms/help')}}" style="display: inline-block">Help</a>
            &#8226;
            <a href="{{url('cms/terms-services')}}" style="display: inline-block">Terms</a>
            </br>
            <a href="{{url('send-feedback')}}" style="display: inline-block">Send Feedback</a>
        </p>
        <a href="javascript:void(0)" class="btn btn-arrow toggle-sidebar-left"><i class="fa fa-angle-left"></i></a>
        </div> 
       
    </div>
     <?php } ?>   
        
     <div class="modal fade" id="searchModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form name="frm_search" id="frm_search" action="{{url('/')}}/search" method="post">
                    <div class="modal-header medi-head-search">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="icon-search"></i></button>
                            </span>
                            <input type="text" class="form-control" value="{{(isset($request['search_word']))?$request['search_word']:''}}"  autofocus name="search_word" id="search" placeholder="Search for things you love...">
                        </div>
                    </div>
                    <div class="modal-body medi-midl-search">
                        <div class="ser-ntor-info ser-ntor-grid">
                            <ul class="clearfix">
                                <li>
                                    <div class="boxesss">
                                        <input class="all_check" type="checkbox"   @if(Session::get('all_checked') == "selected")  checked="true" @endif @if(Session::get('event_checked') != "selected" && Session::get('meetup_checked') != "selected" && Session::get('articles_checked') != "selected" && Session::get('qrious_checked') != "selected" && Session::get('company_checked') != "selected" && Session::get('people_checked') != "selected" && Session::get('education_checked') != "selected")  checked="true" @endif id="box-21" name="all">
                                               <label for="box-21">All</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" name="qrious" @if(Session::get('qrious_checked') == "selected") checked="true" @endif id="box-22">
                                               <label for="box-22">Qrious</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" id="box-23" @if(Session::get('articles_checked') == "selected") checked="true" @endif name="articles">
                                               <label for="box-23">Articles</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" @if(Session::get('job_checked') == "selected") checked="true" @endif name="job" id="box-24">
                                               <label for="box-24">jobs</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" @if(Session::get('people_checked') == "selected") checked="true" @endif name="people" id="box-25">
                                               <label for="box-25">People</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" @if(Session::get('event_checked') == "selected") checked="true" @endif name="event" id="box-26">
                                               <label for="box-26">Events</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" @if(Session::get('meetup_checked') == "selected") checked="true" @endif name="meetup" id="box-27">
                                               <label for="box-27">MEETUPS</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" name="education" @if(Session::get('education_checked') == "selected") checked="true" @endif  id="box-28">
                                               <label for="box-28">Education</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" class="item_check" id="box-29" @if(Session::get('company_checked') == "selected") checked="true" @endif name="company">
                                               <label for="box-29">Company</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer medi-foot-search">
                        <div class="ser-ntor-info ser-ntor-grid">
                            <ul class="clearfix">
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox"  @if(Session::get('atg_group_checked') == "selected")  @else checked="true" @endif name="mygroup" id="box-31">
                                               <label for="box-31">My Groups</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="boxesss">
                                        <input type="checkbox" name="atg_group" @if(Session::get('atg_group_checked') == "selected") checked="true" @endif id="box-30">
                                               <label for="box-30">ATG GROUPS</label>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="seling-sorting-grp">
                            <div>
                                <select data-placeholder="Select Groups..." id="select_group_id" name="select_group_ids[]" class="chosen-select" multiple style="width:100%;" tabindex="4">                                    
                                    <option value=""></option>
                                    @if(isset($finalData['globle_group_arr']))
                                    @foreach($finalData['globle_group_arr'] as $group)
                                    <option value="{{ $group->id }}" @if(Session::get('selected_group') != null && count(Session::get('selected_group')) > 0) @if(in_array($group->id,Session::get('selected_group'))) selected @endif @endif>{{  $group->group_name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="serter-btns">
                            <input type="hidden" value="" id="track_page" name="track_page">
                            <input type="hidden"  id="page_type" name="page_type" value="POST">
                            <button type="submit"  id="search_posts" class="btn">Search Now</button>
                        </div>
                    </div>
                    <input type="hidden" id="base_url" name="base_url" value="{!! url('/') !!}">
                </form>
            </div>
        </div>
    </div>    
    <div class="" id="content">    
    
<style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop {
        left: -9000px; 
    }
    .pac-container {
        z-index:9999 !important;
    } 
    .chosen-container-multi{
        width:100% !important;
    }

</style>
<link href="{{url('/assets/Frontend/css/croppie.css')}}" rel="stylesheet" type="text/css">
<script src="{{url('/assets/Frontend/js/croppie-2.5.0.js')}}"></script>

<script type="text/javascript">




    $(document).ready(function() {
        
        /***********Code to handle Left Swipe of Sidebar on Mobiles *************/
        document.addEventListener('touchstart', handleTouchStart, false);        
        document.addEventListener('touchmove', handleTouchMove, false);

        var xDown = null;                                                        
        var yDown = null;                                                        

        function handleTouchStart(evt) {                                         
            xDown = evt.touches[0].clientX;                                      
            yDown = evt.touches[0].clientY;                                      
        };                                                

        function handleTouchMove(evt) {
            if ( ! xDown || ! yDown ) {
                return;
            }

            var xUp = evt.touches[0].clientX;                                    
            var yUp = evt.touches[0].clientY;

            var xDiff = xDown - xUp;
            var yDiff = yDown - yUp;

            if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
                if ( xDiff > 0 ) {
                    if(window.innerWidth <= 480 || (window.innerWidth <= 767 && window.innerWidth > window.innerHeight))
                    {
                        if(!$("#sidebar-exp").hasClass("active"))
                        {
                            $("#sidebar-exp").addClass("active");
                            $('#nav-icon1').removeClass('open');
                        }
                    }
                } else {
                    /* right swipe */
                }                       
            } else {
                if ( yDiff > 0 ) {
                    /* up swipe */ 
                } else { 
                    /* down swipe */
                }                                                                 
            }
            /* reset values */
            xDown = null;
            yDown = null;                                             
        };
    /*****************************************End*************************************/
    
        $.ajax({
            type: "get",
            url: '{!!url("/")!!}/calendar-notification',
            success: function(data) {
                if(data != '' && data != '0') {
                    $("#calender-count").text(data);
                    $("#calender-count-min").text(data);
                    $("#calender-count").addClass('msg-counter');
                    $("#calender-count-min").addClass('msg-counter');
                    $("#calender-count-min").addClass('counter-min');
                } else {
                    $("#calender-count").removeClass('msg-counter');
                    $("#calender-count-min").removeClass('counter-min');
                    $("#calender-count").text('');
                    $("#calender-count-min").text('');
                }
            }
        });
        
        $('#collapse1').on('show.bs.collapse', function () {
            $('#arrow').addClass('fa-angle-down');
            $('#arrow').removeClass('fa-angle-right');
          });
          
        $('#collapse1').on('hide.bs.collapse', function () {
            $('#arrow').removeClass('fa-angle-down');
            $('#arrow').addClass('fa-angle-right');
          })  
        
        $('#nav-icon1').click(function(){
		$(this).toggleClass('open');
	});
        
        $(".toggle-sidebar-right").click(function () {
                sidebar_width = $('#sidebar-exp').width();
                $("#sidebar-exp").toggleClass("active");
                $('#content').css('padding-left',  sidebar_width + 20);
                $("#sidebar-short").toggleClass("active");
                //document.cookie = "sidebar=exp;";
                localStorage.setItem('sidebar', 'exp');
                return false;
        });
        
        $(".toggle-sidebar-left").click(function () {
                sidebar_width = $('#sidebar-short').width();
                $("#sidebar-short").toggleClass("active");
                $('#content').css('padding-left',  sidebar_width + 20);
                $("#sidebar-exp").toggleClass("active");
                //document.cookie = "sidebar=min;";
                localStorage.setItem('sidebar', 'min');
                return false;
        });
        
        $(".toggle-sidebar").click(function () {
                $("#sidebar-exp").toggleClass("active");
                return false;
        });
    });

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

$("#search_posts").click(function () {
});
var config = {
    '.chosen-select': {}
}
for (var selector in config) {
    $(selector).chosen(config[selector]);
}


/* For checkbox check uncheck */

$(".item_check").on("click", function () {
    $(".all_check").prop("checked", false);
});
$(".all_check").on("click", function () {
    $(".item_check").prop("checked", false);
    $(".all_check").prop("checked", true);
});



$("#box-31").on("click", function () {
    $("#box-30").prop("checked", false);
    $("#box-31").prop("checked", true);
    $(".seling-sorting-grp").show();

});
$("#box-30").on("click", function () {
    $("#box-31").prop("checked", false);
    $("#box-30").prop("checked", true);
    $(".seling-sorting-grp").hide();
});

function getUnreadMessageCount() {
    $.ajax({
        url: '{{url("/")}}/unread-message-counter',
        method: 'post',
        data: {name: 'example', value: 'someval'},
        success: function (response) {
            var counter = 0;
            if (response.counter != 0) {
                counter = response.counter;
                //$('<span class="hole-counte ">' + counter + '</span>').insertAfter('.msg-counter');

            }
        }
    })
}

function searchWithTag(tag_name) {
    $.ajax({
        url: '{{url("/")}}/search-result-with-tag',
        method: 'get',
        data: {search_text: tag_name},
        success: function (msg) {
            $("body").html(msg);
        }
    })
}

var elemBottom=0;
$(window).load(function()
{
    var sticky = $('.aller-nav');
    var scroll = $(window).scrollTop();
    var elemTop = sticky.offset().top;
    elemBottom = elemTop + sticky.height();
 });

$(window).scroll(function() {
    var sticky = $('.aller-nav');
    var scroll = $(window).scrollTop();
    if(elemBottom<scroll)
    {
        sticky.css("position","fixed");
        sticky.css("top","0");
        sticky.css("z-index","100");
        sticky.css("width","100%");
        sticky.css("animation","example 4s ease-out 1s infinite");
    }
    else
    {
        sticky.css("position","relative");
        sticky.css("z-index","0");
    }
});
</script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '{{ env('FB_PIXEL') }}'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id={{ env('FB_PIXEL') }}&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


<!-- Google Analytics Code for Logged IN user www.atg.world -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '{{ env('GOOGLE_ANALYTICS_KEY') }}', 'auto');
  ga('set', {userID: '{{ isset($arr_user_data->id) ? $arr_user_data->id : '' }}' , dimension1: '1' });
  ga('send', 'pageview');
</script>

<!--Ends Search Modal -->
