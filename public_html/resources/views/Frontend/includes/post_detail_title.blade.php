<div class="container">
    <h1>{{$title_of_post}}</h1>
    @if($feature != 'article') 
    <div class="post_detail_feature_name">{{ucfirst($feature)}}</div>
    @endif
</div>
