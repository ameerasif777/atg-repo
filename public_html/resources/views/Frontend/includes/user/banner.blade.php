
<section class="banner-sec col-offset-top-88" style="background-image:url(@if(isset($arr_cover_pic['cover_pic']))
														{{url('/public/assets/Frontend/user/cover_pics/'.$user_id.'/'.$arr_cover_pic['cover_pic'])}}
														@else
														{{url('/public/assets/Frontend/img/banner-user.png')}}
														@endif );background-size: 100% 100%">
<!--<section class="banner-sec col-offset-top-88" style="background-image:url(@if(isset($arr_cover_pic['cover_pic']))
														{{url('/public/assets/Frontend/img/banner-user.png')}}
														@else
														{{url('/public/assets/Frontend/img/banner-user.png')}}
														@endif );">-->
  <div class="container clearfix">
    <div class="sol-icons pull-right">
      <ul class="clearfix">
        <li><a href="javascript:void(0)" class="twe"><i class="fa fa-twitter"></i></a></li>
        <li><a href="javascript:void(0)" class="face-f"><i class="fa fa-facebook-f"></i></a></li>
        <li><a href="javascript:void(0)" class="go-plus"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="javascript:void(0)" class="inl"><i class="fa fa-linkedin"></i></a></li>
        <li><a href="javascript:void(0)"  class="insta"><i class="fa fa-instagram"></i></a></li>
        <li><a href="javascript:void(0)" class="sky"><i class="fa fa-skype"></i></a></li>
      </ul>
    </div>
  </div>
  <div class="colage-name">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        		
				   <div class="mca-logo"> <img alt="ins-logo" src="@if(isset($arr_profile_pic['profile_pic']))
				{{url('/public/assets/Frontend/user/profile_pics/'.$user_id.'/thumb/thumb_'.$arr_profile_pic['profile_pic'])}}
				@else
				{{url('/public/assets/Frontend/img/banner-user-pic.png')}}
				@endif ">
				
				</div>
<!--        <div class="mca-logo"> <img alt="ins-logo" src="@if(isset($arr_profile_pic['profile_pic']))
				{{url('/public/assets/Frontend/img/banner-user-pic.png')}}
				@else
				{{url('/public/assets/Frontend/img/banner-user-pic.png')}}
				@endif ">
		</div>-->
      </div>
      <div class="col-md-10">
        <div class="college-head">
          <h1>Nikhil Bagal</h1>
          <h2>PCCOE College, Pune.</h2>
        </div>
      </div>
    </div>
  </div>
	</div>
</section>
<!-------------------------------------------------------RAM START------------------------------------------------------>
<section class="ram-sec">
  <div class="container">
    <div class="inner-ram">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-2 clearfix">
          <div class="opt-normal">
            <div class="opt-rev"><span>&nbsp;75%</span><img src="{{url('/public/assets/Frontend/img/rev-01.png')}}"></div>
            <p class="opt-man">Recommendation</p>
          </div>
          <div class="opt-normal">
            <div class="opt-rev"><span><b> &nbsp; 3.5</b></span><img src="{{url('/public/assets/Frontend/img/rev-02.png')}}"></div>
            <p class="opt-man">&nbsp;&nbsp;  &nbsp; Rating</p>
          </div>
          <div class="opt-normal">
            <div class="opt-rev"><span>&nbsp;38%</span><img src="{{url('/Frontend/img/rev-03.png')}}"></div>
            <p class="opt-man">&nbsp;&nbsp; &nbsp; Placement</p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="rev-btn-outer clearfix">
          
        <span>
            <button class="btn rev-btn" type="button"><i class="fa fa-users"></i> Connect</button>
            </span>
            <span>
            <button type="button" class="btn rev-btn"><i class="fa fa-share-square-o"></i> Follow</button>
            </span> 
            <span>
            <button type="button" class="btn rev-btn"><i class="fa fa-envelope-o"></i> Message</button>
            </span> </div>
        </div>
      </div>
    </div>
  </div>
</section>

