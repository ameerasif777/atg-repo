<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{$site_title}}</title>
		<link rel="icon" type="image/png" href="{{url('/public/assets/Frontend/img/fav-icon.png')}}" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
		<script src="{{url('/assets/Frontend/js/jquery-2.0.2.min.js')}}"></script>
		<link href="{{url('/assets/Frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/animated.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/main.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/jquery-ui.theme.css')}}" rel="stylesheet" type="text/css">
								<style>
													#country-list{float:left;list-style:none;margin:0;padding:0;width:190px;}
													#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
													#country-list li:hover{background:#F0F0F0;}
													ol, ul {
														list-style: outside none none;
													}
													.hash-tag {
														list-style-type: none;
													}
													.hash-tag ul {
														border: 1px solid #ccc;
														display: block;
														float: left;
														margin: 3px 1px;
														padding: 3px 10px;
													}
													.error
													{
														color:#a94442;
													}
												</style>
	</head>
<body class="gray-bakgound">
<!-------------------------------------------------------HEADER START------------------------------------------------------>
<section class="header">
  <div class="top-header navbar-fixed-top clearfix">
  <div class="container">
    <div class="logo"> <img src="{{url('/public/assets/Frontend/img/logo.png')}}" alt="site-logo"> </div>
    <div class="header-serch">
      <div class="input-group"> <span class="input-group-btn">
        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
        </span>
        <input type="text" class="form-control" placeholder="Search for anything...">
      </div>
    </div>
    <div class="header-right-med">
      <ul>
        <li class="spl-user notification"> <a title="" data-placement="bottom" data-toggle="tooltip" id="toggle_notification" href="javascript:void(0)" data-original-title="Notification"><i class="fa fa-bell"></i> 2 </a>
          <div class="notification-open" style="display: none;">
          <ul role="tablist" class="nav nav-tabs">
            <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="notifications" href="#notifications">Notifications</a></li>
            <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="noticeboard" href="#noticeboard">Noticeboard</a></li>
          </ul>
          <div class="tab-content">
          <div id="notifications" class="tab-pane active" role="tabpanel">
            <div class="noticion-count"> You have 10 Notifications </div>
            <div class="notifiction-info">
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
            </div>
          </div>
          <div id="noticeboard" class="tab-pane active" role="tabpanel">
            <div class="noticion-count"> You have 10 Notifications </div>
            <div class="notifiction-info">
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
              <a class="media" href="#">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <span class="name">Nikhil Bagal</span> <span>like you post Wish</span>
                  <p>you happy diwali..!</p>
                </div>
              </a>
            </div>
          </div>
          </div>
          </div>
        </li>
        <li class="spl-user"> <a title="" data-placement="bottom" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Messages"><i class="fa fa-envelope"></i> 10</a> </li>
        <li class="spl-user"> <a title="" data-placement="bottom" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Users"><i class="fa fa-user"></i> 0</a> </li>
        <li>
					<div class="user-div clearfix">
						<div class="head-user-pro"><img alt="user-pro" src="@if(isset($arr_profile_pic['profile_pic']))
																						{{url('/public/assets/Frontend/user/profile_pics/'.$user_id.'/thumb/thumb_'.$arr_profile_pic['profile_pic'])}}
																						@else
																						{{url('/public/assets/Frontend/img/profile-pic.png')}}
																						@endif"></div>
						<div class="user-pro-name">{{$first_name}}<a href="javascript:void(0)"><i class="fa fa-angle-down"></i></a></div>
					</div>
          <div class="communities-div" style="">
            <h4 class="comm-head"><i class="fa fa-users"></i> &nbsp;My Communities</h4>
            <div class="comm-blok">
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body">
                <p> MBA 2015</p>
                </div>
              </a>
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body">
                 <p>Sports</p>
                </div>
              </a>
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body">
                <p>Training &amp; Placement
                    Cell</p>
                </div>
              </a>
            </div>
            <a class="dash-head"><i class="fa fa-dashboard"></i> &nbsp;Counselor Dashboard</a>
            <div class="comm-blok comm-dash">
            <div>
            
              <a href="#">
                 <i class="fa fa-question-circle"></i> 
                          <span>Help</span>
              </a>
              </div>
               <div>
              <a href="#">
             <i class="fa fa-exclamation-circle"></i> 
                          <span>Report problem</span>
              </a>
              </div>
              <div>
              <a href="#">
            <i class="fa fa-cogs"></i>
                          <span>Setting</span>
              </a>
              </div>
              <div>
              <a href="#">
              <i class="fa fa-graduation-cap"></i>
                          <span>Swich to career</span>
              </a>
              
              </div>
              <div class="logout">
              <a href="{{url('/')}}/auth/logout" >
                <i class="fa fa-sign-out"></i>Logout
              </a>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="chat-btn"> <a title="" data-placement="bottom" data-toggle="tooltip" id="chat-togle" href="javascript:void(0)" data-original-title="Chat"><i class="fa fa-comments"></i></a>
    <div class="chat-togle-open">
          <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="presentation"><a title="People" aria-expanded="true" href="#all-peoples" aria-controls="all-peoples" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
            <li role="presentation"><a title="Conselors" href="#classmate" aria-controls="classmate" role="tab" data-toggle="tab"><i class="fa fa-user"></i></a></li>
          </ul>
          <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="all-peoples">
           <div class="chat-holdr">
            <div class="chat-input"><i class="fa fa-search"></i> <input type="text" class="form-control"> </div>
            <div class="chat-info">
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> 
                <p>James Gibson </p>
                </div>
                <div class="media-right">
                 <i class="fa fa-circle"></i>
                </div>
              </a>
              
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> 
                <p>James Gibson </p>
                </div>
                <div class="media-right">
                 <i class="fa fa-circle"></i>
                </div>
              </a>
              <a class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> 
                <p>James Gibson </p>
                </div>
                <div class="media-right">
                 <i class="fa fa-circle"></i>
                </div>
              </a>
            </div>
            
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="classmate">
            <div class="noticion-count"> You have 10 Notifications </div>
            <div class="notifiction-info">
              <div class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body">  <div>James Gibson </div>
                </div>
              </div>
              <div class="media">
                <div class="media-left">
                  <div class="notficn-info"> <img src="{{url('/public/assets/Frontend/img/profile-pic.png')}}"> </div>
                </div>
                <div class="media-body"> <div>James Gibson </div>
                </div>
              </div>
            </div>
          </div>
        </div></div>
     </div>
  </div>

			