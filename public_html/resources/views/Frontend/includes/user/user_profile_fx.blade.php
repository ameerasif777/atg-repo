<script>
    $(document).ready(function(){
        var viewportWidth = $(window).width();
        if (viewportWidth < 820) {
            $('#top-bio').html($('#bottom-bio').html());
            $("#top-bio").css("display","block");
            $("#bottom-bio").css("display","none");
        } else {
            $("#bottom-bio").css("display","block");
            $("#top-bio").css("display","none");
        }
    });
    
    $(window).resize(function () {
        var viewportWidth = $(window).width();
        if (viewportWidth < 820) {
            $('#top-bio').html($('#bottom-bio').html());
            $("#top-bio").css("display","block");
            $("#bottom-bio").css("display","none");
        } else {
            $("#bottom-bio").css("display","block");
            $("#top-bio").css("display","none");
        }
    });
</script>