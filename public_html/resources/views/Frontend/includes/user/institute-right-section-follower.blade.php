      
<div class="col-md-4 col-sm-4 col-xs-12 col-width-30">
	@if(Request::segment(2)!='institutes-may-follw')
        <div class="top-med-colle">
        <h1><i class="fa fa-university"></i> Colleges you may follow</h1>
			<?php
			$i=1;
			?>
       @foreach($institute_details as $institute)
		
			 @if(!isset($institute->follow_id)||($institute->status=='3'))
				@if($i==5)
				<?php  break;?>
				@endif
			 <div class="media">
				 <div class="media-left">
					 <div class="media-r-pros"><img src="@if(isset($institute->profile_pic))
																					{{url('/public/assets/Frontend/user/profile_pics/'.$institute->id.'/thumb/thumb_'.$institute->profile_pic)}}
																					@else
																					{{url('/public/assets/Frontend/img/college01.png')}} @endif" alt=""></div>
				 </div>
				 <div class="media-body clearfix"> <a href="#">{{$institute->institute_name}} </a>
					 <p><i class="fa fa-map-marker"></i> {{$institute->address}}</p>
				 </div>
				 <div class="media-right"> <span class="pull-right"><a title="Share" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Share"><i class="fa fa-share-alt"></i> </a> <a title="Follow" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Follow"><i class="fa fa-share-square-o"></i></a></span> </div>
			 </div>
			 <?php
			 $i++;
			 ?>
			 @endif
			 
	@endforeach

        <a class="btn view-more-col" href="{{url('/')}}/user/institutes-may-follw">View All Colleges</a>
      </div>
	@endif

	

