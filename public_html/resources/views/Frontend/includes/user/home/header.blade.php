<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>{{$site_title}}</title>
		<link rel="icon" type="image/png" href="{{url('/public/assets/Frontend/img/fav-icon.png')}}" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
		<script src="{{url('/assets/Frontend/js/jquery-2.0.2.min.js')}}"></script>
		<link href="{{url('/assets/Frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/owl.theme.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/animated.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/main.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('/assets/Frontend/css/jquery-ui.theme.css')}}" rel="stylesheet" type="text/css">
								<style>
													#country-list{float:left;list-style:none;margin:0;padding:0;width:190px;}
													#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
													#country-list li:hover{background:#F0F0F0;}
													ol, ul {
														list-style: outside none none;
													}
													.hash-tag {
														list-style-type: none;
													}
													.hash-tag ul {
														border: 1px solid #ccc;
														display: block;
														float: left;
														margin: 3px 1px;
														padding: 3px 10px;
													}
													.error
													{
														color:#a94442;
													}
												</style>
	</head>
<body>
		<section class="header">
			<div class="top-header navbar-fixed-top clearfix">
				<div class="container">
					<div class="logo"> <img alt="site-logo" src="{{url('/public/assets/Frontend/img/logo.png')}}"> </div>
					<div class="header-serch">
						<div class="input-group"> <span class="input-group-btn">
								<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
							</span>
							<input type="text" placeholder="Search for anything..." class="form-control">
						</div>
					</div>
					<div class="header-right-med">
						<ul>
							<li class="spl-user notification" > <a href="javascript:void(0)" id="toggle_notification"><i class="fa fa-bell"></i> 2 </a>
								<div class="notification-open">
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation"><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab">Notifications</a></li>
										<li role="presentation"><a href="#noticeboard" aria-controls="noticeboard" role="tab" data-toggle="tab">Noticeboard</a></li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane active" id="notifications">
											<div class="noticion-count"> You have 10 Notifications </div>
											<div class="notifiction-info">
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane active" id="noticeboard">
											<div class="noticion-count"> You have 10 Notifications </div>
											<div class="notifiction-info">
												<div class="media">
													<div class="media-left">
														<div class="notficn-info">  </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
												<div class="media">
													<div class="media-left">
														<div class="notficn-info"> <img src="img/profile-pic.png" /> </div>
													</div>
													<div class="media-body"> <a href="#">Nikhil Bagal</a> <span>like you post Wish</span>
														<p>you happy diwali..!</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li class="spl-user"> <a href="javascript:void(0)"><i class="fa fa-envelope"></i> 10</a> </li>
							<li class="spl-user"> <a href="javascript:void(0)"><i class="fa fa-user"></i> 0</a> </li>
							<li>
								<div class="user-div clearfix">
									<div class="head-user-pro"><img alt="user-pro" src="@if(isset($arr_profile_pic['profile_pic']))
																									{{url('/public/assets/Frontend/user/profile_pics/'.$user_id.'/thumb/thumb_'.$arr_profile_pic['profile_pic'])}}
																									@else
																									{{url('/public/assets/Frontend/img/profile-pic.png')}}
																									@endif"></div>
									<div class="user-pro-name">{{$first_name}}<a href="javascript:void(0)"><i class="fa fa-angle-down"></i></a></div>
								</div>
								<div class="communities-div">
									<h4 class="comm-head"><i class="fa fa-users"></i> &nbsp;My Communities</h4>
									<div class="comm-blok">
										<div class="media">
											<div class="media-left">
												<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
											</div>
											<div class="media-body">
												<a href="#">MBA 2015</a>
											</div>
										</div>
										<div class="media">
											<div class="media-left">
												<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
											</div>
											<div class="media-body">
												<a href="#">Sports</a>
											</div>
										</div>
										<div class="media">
											<div class="media-left">
												<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
											</div>
											<div class="media-body">
												<a href="#">Training & Placement
													Cell</a>
											</div>
										</div>
									</div>
									<h4 class="comm-head"><i class="fa fa-dashboard"></i> &nbsp;Counselor Dashboard</h4>
									<div class="comm-blok comm-dash">
										<div>
											<a href="#">
												<i class="fa fa-question-circle"></i> 
												<span>Help</span>
											</a>
										</div>
										<div>
											<a href="#">
												<i class="fa fa-exclamation-circle"></i> 
												<span>Report problem</span>
											</a>
										</div>
										<div>
											<a href="#">
												<i class="fa fa-cogs"></i>
												<span>Setting</span>
											</a>
										</div>
										<div>
											<a href="#">
												<i class="fa fa-graduation-cap"></i>
												<span>Swich to career</span>
											</a>

										</div>
										<div class="logout">
											<a href="{{url('/')}}/auth/logout" >
												<i class="fa fa-sign-out"></i>Logout
											</a>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="chat-btn"> <a href="javascript:void(0)" id="chat-togle"><i class="fa fa-comments"></i></a>
						<div class="chat-togle-open">
							<ul role="tablist" class="nav nav-tabs">
								<li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="all-peoples" href="#all-peoples" aria-expanded="true"><i class="fa fa-users"></i></a></li>
								<li role="presentation"><a data-toggle="tab" role="tab" aria-controls="classmate" href="#classmate"><i class="fa fa-user"></i></a></li>
							</ul>
							<div class="tab-content">
								<div id="all-peoples" class="tab-pane active" role="tabpanel">
									<div class="chat-holdr">
										<div class="chat-input"><i class="fa fa-search"></i> <input type="text" class="form-control"/> </div>
										<div class="chat-info">
											<div class="media">
												<div class="media-left">
													<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
												</div>
												<div class="media-body"> 
													<a href="#">James Gibson </a>
												</div>
												<div class="media-right">
													<i class="fa fa-circle"></i>
												</div>
											</div>
											<div class="media">
												<div class="media-left">
													<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
												</div>
												<div class="media-body"> 
													<a href="#">James Gibson </a>
												</div>
												<div class="media-right">
													<i class="fa fa-circle"></i>
												</div>
											</div>
											<div class="media">
												<div class="media-left">
													<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
												</div>
												<div class="media-body"> 
													<a href="#">James Gibson </a>
												</div>
												<div class="media-right">
													<i class="fa fa-circle"></i>
												</div>
											</div>
										</div>

									</div>
								</div>
								<div id="classmate" class="tab-pane" role="tabpanel">
									<div class="noticion-count"> You have 10 Notifications </div>
									<div class="notifiction-info">
										<div class="media">
											<div class="media-left">
												<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
											</div>
											<div class="media-body">  <div>James Gibson </div>
											</div>
										</div>
										<div class="media">
											<div class="media-left">
												<div class="notficn-info"> <img src="img/profile-pic.png"> </div>
											</div>
											<div class="media-body"> <div>James Gibson </div>
											</div>
										</div>
									</div>
								</div>
							</div></div>
					</div>
				</div>