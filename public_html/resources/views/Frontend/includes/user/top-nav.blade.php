<div class="header-navigations yapiskan">
	<div class="container">
		<ul class="clearfix">
			<li><a href="{{url('/')}}/user/home" @if(Request::segment(2)=='wall' || Request::segment(2)=='home') class='active' @endif >
						 <i class="fa fa-list-alt"></i>Sync</a></li>
			<li @if(Request::segment(2)=='portfolio') class='active' @endif ><a href="{{url('/')}}/user/portfolio"><i class="fa fa-user"></i>Portfolio</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-users"></i>People </a></li>
			<li @if((Request::segment(2)=='institutes')||(Request::segment(2)=='institutes-may-follw')) class='active' @endif><a href="{{url('/')}}/user/institutes"><i class="fa fa-building-o"></i>Institutes</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-sitemap"></i>Communities</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-comments-o"></i> Reviews</a></li>
			<li><a href="javascript:void(0)"><i class="fa fa-calendar-check-o"></i>Events</a></li>
			<li><a href="#"><i class="fa fa-file-image-o"></i> Forms</a></li>
			<li><a href="#"><i class="fa fa-file-text-o"></i> Documents</a></li>
			<li><a href="#"> <i class="fa fa-film"></i> Media</a></li> 
		</ul>
	</div>
</div>
</div>
</section>
<input type="hidden" id="base_url" name="base_url" value="{!! url('/')!!}">
<div class="loader" style="display: none"></div> 

