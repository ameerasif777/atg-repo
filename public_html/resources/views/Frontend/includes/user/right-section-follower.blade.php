      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="right-tabi">
          <div class="top-med-colle">
            <h1><i class="fa fa-university"></i> Colleges you may follow</h1>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college01.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">DY PATIL </a>
                <p><i class="fa fa-map-marker"></i> Nigdi, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college02.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">PCCOE College </a>
                <p><i class="fa fa-map-marker"></i> Pimpri, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college01.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">PCCOE College </a>
                <p><i class="fa fa-map-marker"></i> Nigdi, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college02.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">DY PATIL </a>
                <p><i class="fa fa-map-marker"></i> Pimpri, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <button type="button" class="btn view-more-col">View All Colleges</button>
          </div>
          <div class="top-med-colle">
            <h1><i class="fa fa-university"></i> Colleges you may follow</h1>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college01.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">DY PATIL </a>
                <p><i class="fa fa-map-marker"></i> Nigdi, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college02.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">PCCOE College </a>
                <p><i class="fa fa-map-marker"></i> Pimpri, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college01.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">PCCOE College </a>
                <p><i class="fa fa-map-marker"></i> Nigdi, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <div class="media">
              <div class="media-left">
                <div class="media-r-pros"><img src="{{url('/public/assets/Frontend/img/college02.png')}}" alt=""></div>
              </div>
              <div class="media-body clearfix"> <a href="#">DY PATIL </a>
                <p><i class="fa fa-map-marker"></i> Pimpri, Pune</p>
              </div>
              <div class="media-right"> <span class="pull-right"><a href="#"><i class="fa fa-share-alt"></i> </a> <a href="#"><i class="fa fa-share-square-o"></i></a></span> </div>
            </div>
            <button type="button" class="btn view-more-col">View All Colleges</button>
          </div>
          <div class="top-med-colle">
            <h1>featured</h1>
            <div class="slider-post clearfix" >
              <div class="feat-valu owl-carousel">
                <div class="item" style="background-image:url({{url('/public/assets/Frontend/img/college03.png')}});">
                  <p>Dr D Y Patil College of Engineering, Pune</p>
                </div>
                <div class="item" style="background-image:url({{url('/public/assets/Frontend/img/college01.png')}});">
                  <p>PCCOE College, Pune</p>
                </div>
                <div class="item" style="background-image:url({{url('/public/assets/Frontend/img/college02.png')}});">
                  <p>Dr D Y Patil College of Engineering, Pune</p>
                </div>
                <div class="item" style="background-image:url({{url('/public/assets/Frontend/img/college03.png')}});">
                  <p>PCCOE College, Pune</p>
                </div>
              </div>
            </div>
          </div>
     


