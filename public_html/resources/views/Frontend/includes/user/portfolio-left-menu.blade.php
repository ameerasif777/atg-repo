<section class="collage-block col-offset-top-88">
  <div class="container">
    <h1 class="profile-heading">Welcome to your Portfolio!</h1>
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12 col-width-30">
        <ul class="nav nav-tabs profile-ul" role="tablist">
          <!--<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
          </ul>-->
          <li role="presentation" @if(Request::segment(2)=='portfolio') class='active' @endif > <a href="{{url('/')}}/user/portfolio" aria-controls="profile1" role="tab" ><i class="fa fa-user"></i> <span>Personal</span> </a></li>
          <li role="presentation" @if(Request::segment(2)=='academics') class='active' @endif > <a href="{{url('/')}}/user/academics" aria-controls="profile2" role="tab" ><i class="fa fa-graduation-cap"></i><span> Academics</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='certifications') class='active' @endif > <a  href="{{url('/')}}/user/certifications" aria-controls="profile3" role="tab" ><i class="fa fa-certificate"></i><span> Certifications</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='achievements') class='active' @endif> <a href="{{url('/')}}/user/achievements" aria-controls="profile4" role="tab" ><i class="fa fa-trophy"></i><span> Participations &amp; Achievements</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='interest') class='active' @endif> <a href="{{url('/')}}/user/interest" aria-controls="profile5" role="tab" ><i class="fa fa-pencil"></i><span>Interest and Skills</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='language') class='active' @endif> <a  href="{{url('/')}}/user/language" aria-controls="profile6" role="tab" ><i class="fa fa-language"></i><span> Language</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='educational_documents') class='active' @endif> <a  href="{{url('/')}}/user/educational_documents" aria-controls="profile6" role="tab" ><i class="fa fa-language"></i><span> Educational Documents</span></a></li>
          <li role="presentation" @if(Request::segment(2)=='profile_settings') class='active' @endif> <a href="{{url('/')}}/user/profile_settings" aria-controls="profile7" role="tab" ><i class="fa fa-cogs"></i><span>Profile Settings</span></a></li>
          <li  role="presentation" class="other-link"> <a href="#profile8" aria-controls="profile8" role="tab" data-toggle="tab"><i class="fa fa-eye"></i> <span>View Profile</span></a></li>
        </ul>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 col-width-70">
        <div class="academics-content clearfix"> 
          <!-- Tab panes -->
          <div class="tab-content">






