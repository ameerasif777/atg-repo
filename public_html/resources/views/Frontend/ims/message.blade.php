@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/public/assets/css/views/messages.css') }}" rel="stylesheet" type="text/css" />
<input type="hidden" id="abs_path_for_emojies" value="{{url('/assets/Frontend/js/emoji/')}}">
<script src="{{url('/assets/Frontend/js/emoji/js/EmojiPicker.js')}}"></script>
<script src="{{url('/assets/Frontend/js/emoji/js/emojies.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/js/emoji/css/EmojiPicker.css')}}">
<script>
jQuery(document).ready(function($) {
    $('.live-search-list li').each(function() {
        $(this).attr('data-search-term', $(this).text().toLowerCase());
    });
    $('.live-search-box').on('keyup', function() {
        var searchTerm = $(this).val().toLowerCase();
        $('.live-search-list li').each(function() {
            if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
        var counter = $(".user-message-list:visible").length;
        if (counter == 0) {
            $('.search-result').css('display', 'block')
        } else {
            $('.search-result').css('display', 'none')
        }
    });
});

function openFileUpload() {
    $("#attachment").click();
}

function readImage(input) {
    $('#file_name').html(input.files[0].name);
}

function openFileUploadModal() {
    $("#attachment_modal").click();
}

function readImageModal(input) {
    $('#file_name_modal').html(input.files[0].name);
}
</script>
<style>
    .modal-tools{
        top: 100px !important;
    }
    .cust-text-area {
        height: 100px !important;
    }
    .mid-ims-div {
        float: left;
        min-height: 750px;
        width: 75%;
    }
    <!-- WIDTH for mid-ims-div was initially 50%, but we have removed the right section for now. Right section needs to be floatable -->

    .posted-arti-comment .media-body img {
        height: 25px;
        width: 25px;
    }
    .grey-overlay {
        background: rgba(0,0,0,0.7);
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 9998;
    }
    .live-search-list {
        max-height: 600px;
        overflow: auto;
    }
    .posted-arti-comment .media .media-heading a {
        margin: 0 2px;
    }
/*    #file_name{
    margin-top: 10px;
    }*/
</style>
<section class="ims-main-sec" onload="initEmojiPicker()">
    <div class="inter-ims clearfix">
        <div class="left-ims-div">
            <div class="ims-sub-head clearfix">
                <!--<a href="{{url('/')}}/messages" class="pull-left" title="Settings"><i class="icon-Setting"></i></a>-->
                <a href="javascript:void(0)" class="pull-right" data-toggle="modal" data-target="#compose-message-modal-bd" title="Compose Message "><i class="icon-Post"></i></a>
            </div>
            <div class="letre-ims-tag">

                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="icon-search"></i></button>
                    </span>
                    <input type="text" class="form-control live-search-box" placeholder="Search for...">
                </div>
                 <div class="">
                <ul class="clearfix live-search-list content-slider message_mobile" id="content-slider" style="min-height: 100px;">
                    @if(count($all_sent_messages_users)>0)
                    @if($all_sent_messages_users[0]->message_from == $login_user_id)
                    <input type="hidden" name="to_user_id" id="to_user_id" value="{{$all_sent_messages_users[0]->message_to}}">
                    <input type="hidden" name="message_id" id="message_id" value="{{$all_sent_messages_users[0]->message_id}}">
                    @else
                    <input type="hidden" name="to_user_id" id="to_user_id" value="{{$all_sent_messages_users[0]->message_from}}">
                    <input type="hidden" name="message_id" id="message_id" value="{{$all_sent_messages_users[0]->message_id}}">
                    @endif

                    @foreach($all_sent_messages_users as $key => $user)
                    @if($login_user_id == $user->message_from)
                    <li class="user-message-list @if($key == 0) active @endif" id="left_side_li{{$user->message_id}}_{{$user->message_to}}">
                        <a href="javascript:void(0)" onclick="getMessage('{!!$user->message_to!!}', '{!!$user->message_id!!}')">
                            <div class="media">
                                <div class="media-left">
                                    <div class="ims-ustre"><img class="remov-slide-img" src="{{ url('/assets/Frontend/img/def_fav_icon.png') }}"/><img class="dis-slide-img" src="{{config('profile_pic_url').$user->message_to.'/'.$user->to_profile_picture}}" alt="" <?php if ($user->to_user_gender == 0) { ?> onerror="src={{ config('avatar_male') }}" <?php } else { ?> onerror="src={{config('avatar_female')}}" <?php } ?> /></div>

                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading clearfix">{{$user->to_first_name.' '.$user->to_last_name}} @if($user->unread_message_counter > 0)<span class="hole-counte" id="message_counter_span{{$user->message_id}}">{{$user->unread_message_counter}}</span>@endif</h4>
                                    <p>{{$user->title}}</p>

                                </div>
                            </div>
                        </a>
                    </li>
                    @else
                    <li class="user-message-list @if($key == 0) active @endif" id="left_side_li{{$user->message_id}}_{{$user->message_from}}">
                        <a href="javascript:void(0)" onclick="getMessage('{!!$user->message_from!!}', '{!!$user->message_id!!}')">
                            <div class="media">
                                <div class="media-left">
                                    <div class="ims-ustre"><img class="remov-slide-img" src="{{ config('img').'def_fav_icon.png' }}"/><img class="dis-slide-img" @if($user->from_user_gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif src="{{config('profile_pic_url').$user->message_from.'/'.$user->from_profile_picture}}" alt=""/></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading clearfix">{{$user->from_first_name.' '.$user->from_last_name}}</h4>
                                    <p>{{$user->title}}</p>
                                </div>
                            </div>
                        </a>
                    </li>

                    @endif
                    @endforeach
                    @else
                    <div class="ims-post-igod">Your conversations list will show up here.</div>
                    @endif
                </ul>
                 <ul class="clearfix live-search-list mCustomScrollbar message_ds">
                    @if(count($all_sent_messages_users)>0)
                    @if($all_sent_messages_users[0]->message_from == $login_user_id)
                    <input type="hidden" name="to_user_id" id="to_user_id" value="{{$all_sent_messages_users[0]->message_to}}">
                    <input type="hidden" name="message_id" id="message_id" value="{{$all_sent_messages_users[0]->message_id}}">
                    @else
                    <input type="hidden" name="to_user_id" id="to_user_id" value="{{$all_sent_messages_users[0]->message_from}}">
                    <input type="hidden" name="message_id" id="message_id" value="{{$all_sent_messages_users[0]->message_id}}">
                    @endif

                    @foreach($all_sent_messages_users as $key => $user)
                    @if($login_user_id == $user->message_from)
                    <li class="user-message-list @if($key == 0) active @endif" id="left_side_li{{$user->message_id}}_{{$user->message_to}}">
                        <a href="javascript:void(0)" onclick="getMessage('{!!$user->message_to!!}', '{!!$user->message_id!!}')">
                            <div class="media">
                                <div class="media-left">
                                    <div class="ims-ustre"><img class="remov-slide-img" src="{{ config('img').'def_fav_icon.png' }}"/><img class="dis-slide-img" src="{{config('profile_pic_url').$user->message_to.'/'.$user->to_profile_picture}}" alt="" <?php if ($user->to_user_gender == 0) { ?> onerror="src={{config('avatar_male')}}" <?php } else { ?> onerror="src={{config('avatar_female')}}" <?php } ?> /></div>

                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading clearfix">{{$user->to_first_name.' '.$user->to_last_name}} @if($user->unread_message_counter > 0)<span class="hole-counte" id="message_counter_span{{$user->message_id}}">{{$user->unread_message_counter}}</span>@endif</h4>
                                    <p>{{$user->title}}</p>

                                </div>
                            </div>
                        </a>
                    </li>
                    @else
                    <li class="user-message-list @if($key == 0) active @endif" id="left_side_li{{$user->message_id}}_{{$user->message_from}}">
                        <a href="javascript:void(0)" onclick="getMessage('{!!$user->message_from!!}', '{!!$user->message_id!!}')">
                            <div class="media">
                                <div class="media-left">
                                    <div class="ims-ustre"><img class="remov-slide-img" src="{{ config('img').'def_fav_icon.png' }}"/><img class="dis-slide-img" @if($user->from_user_gender == 0) onerror="src={{config('avatar_male')}}" @else onerror="src={{config('avatar_female')}}" @endif src="{{config('profile_pic_url').$user->message_from.'/'.$user->from_profile_picture}}" alt=""/></div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading clearfix">{{$user->from_first_name.' '.$user->from_last_name}}</h4>
                                    <p>{{$user->title}}</p>
                                </div>
                            </div>
                        </a>
                    </li>

                    @endif
                    @endforeach
                    @else
                    <div class="ims-post-igod">Conversation details will show up here.</div>
                    @endif
                </ul>
                </div>

                 
                


                <div class="ims-post-igod search-result" style="display: none">Matching record not found.</div>
            </div>
        </div>


        <div class="mid-ims-div">
            <div class="ims-sub-head clearfix" id="message_title"></div>
            <div class="mid-ims-msg-div">
                <div class="posted-arti-comment mCustomScrollbar">
                    @if(count($all_sent_messages_users)== 0)
                    <div class="ims-post-igod">Why don't you strike a conversation!</div>
                    @endif
                </div>
                <hr>
                <form id="compose-message" name="compose-message" method="post" action="" enctype="multipart/form-data">
                    <input type="hidden" id="to_user_id_chat" name="to_user_id_chat" value="">
                    <input type="hidden" id="message_id_chat" name="message_id_chat" value="">
                    <div class="text-feder-outer ims-dinvis-text">
                        <div class="fealider-area">
                            <div class="title-input">
                                <div id="message_content" class="cust-text-area form-control"></div>
                                <textarea class="form-control" name="message" id="message" value="" style="display: none"></textarea>
                            </div>
                            <div class="subsc-ims clearfix">
                                <span class="pull-left"> 
                                    <input type="file" title="Upload Attachment" class="hidden" id="attachment" name="attachment" onchange="readImage(this);">
                                    <a href="javascript:void(0)" onclick="openFileUpload(this)"><i class="fa fa-paperclip"></i></a>
                                    <span id="file_name"></span>
				    <div id = "addemojibtn"></div>
                                </span>
                                <span class="pull-right">
                                    <div class="boxesss">
                                        <input type="checkbox" id="box-41" @if(Session::get('session-ims-checkbox') == "checked") checked="true" @endif name="enter_to_send_chk">
                                        <label for="box-41">Enter to send</label>
                                    </div>	
                                    <button type="button" class="btn ims-send-btn" id="compose_btn" onclick="composeMessage()">Send</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="compose-message-modal-bd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title" id="exampleModalLabel">New message</h4>
            </div>
             <form id="compose-message-modal" name="compose-message-modal" method="post" action="" enctype="multipart/form-data">
              <div class="modal-body">
               
                    <div class="text-feder-outer ims-dinvis-text">
                        <div class="form-group">
                            <input type="text" class="form-control" id="model_title" name="model_title" placeholder="Title" >
                        </div>
                        <div class="form-group">
                            <select data-placeholder="Recipients" placeholder = "recipients" style = "width:100%;" id="recipient_ids" name="recipient_ids[]" class="chosen-select" multiple tabindex="4">
                                <option value=""></option>
                                @if(isset($connection))
                                @foreach($connection as $con)
                                @if($con->from_user_id == $login_user_id)
                                <option value="{{$con->to_user_id}}">{{$con->to_first_name.' '.$con->to_last_name}}</option>
                                @else
                                <option value="{{$con->from_user_id}}">{{$con->from_first_name.' '.$con->from_last_name}}</option>
                                @endif
                                @endforeach
                                @endif
                            </select>
                            <div for="recipient_ids" class="text-danger"></div>
                        </div>
                        <div class="fealider-area">
                            <div class="title-input">
                                <div id="message_content_modal" class="cust-text-area form-control" contenteditable="true"></div>
                                <textarea class="form-control" name="message_modal" id="message_modal" value="" style="display: none"></textarea>
                            </div>
<!--
                
-->
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <div class="subsc-ims">
                    <span class="pull-left">
                        <input type="file" title="Upload Attachment" class="hidden" id="attachment_modal" name="attachment_modal" onchange="readImageModal(this);">
                        <a href="javascript:void(0)" onclick="openFileUploadModal(this)"><i class="fa fa-paperclip"></i></a>
                        <span id="file_name_modal"></span>
                    </span>
                </div>
                <button type="button" id="compose_btn_modal" class="btn ims-send-btn" onclick="getDescriptionModal()">Send </button>
            </div>
           </form>
         </div>
    </div>
</div>
<input type="hidden" name="profile_image_path" id="profile_image_path" value="{{config('profile_pic_url')}}">
<input type="hidden" name="profile_error_image" id="profile_error_image" value="src={{config('avatar_male')}}">
<input type="hidden" name="profile_error_image" id="profile_error_image_female" value="src={{config('avatar_female')}}">
<input type="hidden" name="attachment_path" id="attachment_path" value="{{config('ims_attachment')}}">
<script>
    function getDescriptionModal() {
        var message_content_modal = $('#message_content_modal').html();
        $('#message_modal').val(message_content_modal);
    }

    var user_id = $('#to_user_id').val();
    var message_id = $('#message_id').val();

    $(document).ready(function() {
        var config = {
            '.chosen-select': {}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        /* landing Form Validation Start */
        jQuery("#compose-message-modal").validate({
            errorElement: 'div',
            debug: true,
            errorClass: 'text-danger',
            ignore: '.hidden',
            rules: {
                model_title: {
                    required: true
                },
                'recipient_ids[]': {
                    required: true
                },
                message_modal: {
                    required: true
                }
            },
            messages: {
                model_title: {
                    required: "Please enter title."
                },
                'recipient_ids[]': {
                    required: "Please select atleast one user to send message."
                },
                message_modal: {
                    required: "Please enter your message."
                }
            }, submitHandler: function(form) {
                jQuery("#btn_otp_submit").hide();
                jQuery("#loader").show();
                form.submit();
            }
        });


        if (user_id != '' && typeof (user_id) != 'undefined' && message_id != '' && typeof (message_id) != 'undefined') {
            getMessage(user_id, message_id);
        }
    });

    $('#compose_btn_modal').on('click', function() {
        var form = document.getElementById('compose-message-modal');
        var formData = new FormData(form);
        if ($('#compose-message-modal').valid()) {
            $('#compose-message-modal-bd').modal('hide');
            $('#message_content_modal').html('');
            $('#file_name_modal').html('');
            $('#compose-message-modal')[0].reset();

            $.ajax({
                url: '{{url('/')}}/compose-message-modal',
                method: 'post',
                contentType: false,
                processData: false,
                dataType: 'json',
                data: formData,
                success: function(response) {
                    window.location.reload(true);
                }
            });
        }
    })
    function getMessage(user_id, message_id) {
        $('.user-message-list').removeClass('active');
        $('.grey-overlay').css('display', 'block');
        if (user_id != '' && message_id != '') {
            $.ajax({
                url: '{{url('/')}}/get-message',
                method: 'post',
                data: {
                    user_id: user_id,
                    message_id: message_id
                },
                success: function(response) {
                    setTimeout(function() {
                        $('.grey-overlay').css('display', 'none');
                    }, 500);
                    if (response.error != '1') {
                        $('#message_counter_span' + message_id).remove();
                        $('#left_side_li' + message_id + '_' + user_id).addClass('active');
                        var str = '';
                        $('.posted-arti-comment').html('');
                        var profile_image_path = $('#profile_image_path').val();
                        var profile_error_image = $('#profile_error_image').val();
                        var profile_error_image_female = $('#profile_error_image_female').val();
                        var attachment_path = $('#attachment_path').val();
                        $.each(response.total_response, function(index, value) {

                            str += '<div class="media" id="message_div' + value.reply_id + '">';
                            str += '<div class="ims-post-date pad-to-ins">';
                            if (value.is_attachment == '1') {
                                str += '<a href="' + attachment_path + '/' + value.attachment_name + '" target="_blank"><i class="fa fa-paperclip"></i></a>';
                            }
                            str += '</div>';   
                            str += '<div class="media-left">';
                            str += '<div class="post-ari-img"><img alt="users" src="' + profile_image_path + value.message_from + '/thumb/' + value.from_profile_picture + '" onerror="' + profile_error_image + '"></div>';
                            str += '</div>';
                            str += '<div class="media-body">';
                            str += '<h4 class="media-heading">' + value.from_first_name + ' ' + value.from_last_name;
                            if (value.message_type == '1') {
                                str += '<span class="pull-right"><a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>';
                                str += '<ul class="dropdown-menu" aria-labelledby="dLabel">';
                                str += '<li><a href="javascript:void(0);" onClick="delectMessage()">Delete</a></li>';
                                str += '<li>' + value.sent_date + '</li>';
                                str += '</ul>';
                                str += '</span>';
                            }
                            str += '</h4>';
                            str += '<p>' + value.message + '</p>';
                            str += '</div>'
                            str += '</div>';

                        })
                        $('.posted-arti-comment').append(str);
                        var message_title = '<h1>' + response.first_message.title + '<span class="pull-right">' + response.first_message.sent_date + '</span></h1>';
                        $('#message_title').html(message_title);
                    }

                    $('#to_user_id').val(user_id);
                    $('#message_id').val(message_id);
                    chatMessages();
                }
            });
        }
    }

    function chatMessages() {
        var user_id = $('#to_user_id').val();
        var message_id = $('#message_id').val();
        if (user_id != '' && message_id != '') {
            $.ajax({
                url: '{{url('/')}}/get-message',
                method: 'post',
                data: {
                    user_id: user_id,
                    message_id: message_id
                },
                success: function(response) {
                    if (response.error != '1') {
                        getUnreadMessageCount();
                        var str = '';
                        $('.posted-arti-comment').html('');
                        var profile_image_path = $('#profile_image_path').val();
                        var profile_error_image = $('#profile_error_image').val();
                        var attachment_path = $('#attachment_path').val();
                        $.each(response.total_response, function(index, value) {

                            str += '<div class="media" id="message_div' + value.reply_id + '">';
                            str += '<div class="ims-post-date pad-to-ins">';
//                            str += '<div class="clearfix">';
//                            if (value.is_attachment == '1') {
//                                str += '<span class="pull-right"><a href="' + attachment_path + '/' + value.attachment_name + '" target="_blank"><i class="fa fa-paperclip"></i></span></a>';
//                            }
                            str += '</div>';
                            str += '<div class="media-left">';
                            str += '<div class="post-ari-img"><img alt="users" src="' + profile_image_path + value.message_from + '/thumb/' + value.from_profile_picture + '" onerror="' + profile_error_image + '"></div>';
                            str += '</div>';
                            str += '<div class="media-body">';
                            str += '<h4 class="media-heading">' + value.from_first_name + ' ' + value.from_last_name;
                            if (value.message_type == '1') {
                                str += '<span class="pull-right"><a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>';
                                str += '<ul class="dropdown-menu" aria-labelledby="dLabel">';
                                str += '<li><a href="javascript:void(0)"  onclick="deleteMessage(' + value.reply_id + ')">Delete</a></li>';
                                str += '<li>' + value.sent_date + '</li>';
                                str += '</ul>';
                                str += '</span>';
                            }
                            
//                             str += '</h4>';
//                             str += '<h4 class="media-heading">'
                             if (value.is_attachment == '1') {
//                                alert('s');
                                 str += '<span class="pull-right"><a href="' + attachment_path + '/' + value.attachment_name + '" target="_blank"   aria-haspopup="true" aria-expanded="false"><i class="fa fa-paperclip"></i></a>';
                            }
                            str += '</h4>';
                            str += '<p>' + value.message + '</p>';
                            str += '</div>';
                            str += '</div>';
                        })
                        $('.posted-arti-comment').append(str);
                        $(".posted-arti-comment").animate({scrollTop: $(".posted-arti-comment")[0].scrollHeight}, 1000)
                    }
                }
            });
        }
    }

    $('#message_content').on('keydown', function(e) {
        var chk_to_send = $('input[name=enter_to_send_chk]').is(':checked');
        if (e.keyCode == 13 && chk_to_send) {
            getDescription();
            composeMessage();
            $('input[name=enter_to_send_chk]').attr('checked', true);
        }
    });


    function getDescription() {
        var message_content = $('#message_content').html();
        if (message_content != '') {
            $('#message').val(message_content);
        }
    }

    function composeMessage() {
//        alert('sss');
        getDescription();
        var user_id = $('#to_user_id').val();
        var message_id = $('#message_id').val();
        $('#to_user_id_chat').val(user_id);
        $('#message_id_chat').val(message_id);
        var form = document.getElementById('compose-message');
        var formData = new FormData(form);
        var content = $('#message').val();
        var attachment = $('#attachment').val();
        if (content != '' || attachment!="" ) {
            if(content==""){
//                alert('sss');
                content=$('#message').val("Attachement");
            }
            $.ajax({
                url: '{{url('/')}}/compose-message',
                method: 'post',
                contentType: false,
                processData: false,
                dataType: 'json',
                data: formData,
                success: function(response) {
                    chatMessages();
                    $(".posted-arti-comment").animate({scrollTop: $(".posted-arti-comment")[0].scrollHeight}, 1000)
                }
            });
            $('#message_content').html('');
            $('#file_name').html('');
            $('#compose-message')[0].reset();
        }
    }
    if (user_id != '' && typeof (user_id) != 'undefined' && message_id != '' && typeof (message_id) != 'undefined') {
//        setInterval("chatMessages()", 5000);
    }

    function deleteMessage(reply_id) {
        if (reply_id != '') {
            $('#message_div' + reply_id).remove();
            $.ajax({
                url: '{{url('/')}}/delete-message',
                method: 'post',
                data: {
                    reply_id: reply_id
                },
                success: function(response) {

                }
            })
        }
    }
    $('#box-41').on('click', function(e) {
        var check_value = $('#box-41').prop("checked");
        $.ajax({
            method: 'post',
            url: '{{url('/')}}/remember-send-checkbox',
            data: {check_value: check_value},
            success: function(res) {
            }
        });

    });

    

</script>
