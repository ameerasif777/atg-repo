<?php
$image_path = $finalData['absolute_path'] . 'public/assets/Frontend/images/event_image/' . $list[0]->profile_image;
if (file_exists($image_path) && $list[0]->profile_image != '') {
    $post_image_path = url('/') . '/assets/Frontend/images/event_image/' . $list[0]->profile_image;
} else {
    $post_image_path = url('/') . '/assets/Frontend/img/image-not-found3.jpg';
}
$meta_desc = fixtags($finalData['header']['meta_description']);

function fixtags($text)
{
    $text = htmlspecialchars($text);
    $text = preg_replace("/=/", "=\"\"", $text);
    $text = preg_replace("/&quot;/", "&quot;\"", $text);
    $tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
    $replacement = "<$1$2$3$4$5$6$7$8$9$10>";
    $text = preg_replace($tags, $replacement, $text);
    $text = preg_replace("/=\"\"/", "=", $text);
    return $text;
}

$session_data = Session::get('user_account');

?>
@include('Frontend.includes.head')
@if($session_data != "")
    @include('Frontend.includes.header')
@else
    @include('Frontend.includes.outer-header')
@endif
@include('Frontend.includes.session-message')

@if($session_data == '')
    @include('Frontend.includes.fixed_signup_footerbutton',['get_groups'=>$get_groups])
@endif

<meta property="og:title" content="{{$finalData['header']['meta_title']}}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{Request::fullUrl()}}">
<meta name="keywords"
      content="@if(isset($list[0]->tag) && $list[0]->tag != ''){{$list[0]->tag}} @else {{$list[0]->title}} @endif">
<meta property="og:tags" content='{{$finalData["header"]["meta_keywords"]}}'>
<meta name="description" content="{{$finalData['header']['meta_title']}}">
<meta property="og:description" content="{{$meta_desc}}">
<meta property="og:image" content="{{$post_image_path}}">
<link rel="stylesheet" href="{{ url('/assets/Frontend/css/horizontal-slider-reset.css') }}">
<link rel="stylesheet" href="{{ url('/assets/Frontend/css/horizontal-slider-style.css') }}">
<script src="{{url('/assets/Frontend/js/horizontal-slider-main.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<link href="{{ url('/public/assets/css/views/post_detail.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/calendar.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/calendar_compact.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/style.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{url('/assets/Frontend/js/calendar/en.js')}}"></script>
<!--<script src="{{url('/assets/Frontend/js/calendar/calendar.js')}}"></script>-->
@include('Frontend.includes.post_detail_mention_user_ids_top')
<!-------------------------------------------------------LOG-STEP-ONE START------------------------------------------------------>
<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/event_bg.jpg') }});">
    <?php  $title_of_post = $list[0]->title?>
    @include('Frontend.includes.post_detail_title', [ 'feature' => "event", 'title' => $title_of_post ])
</section>


<section class="artical-post-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">

                @include('Frontend.includes.google_ads', ['ads_public_only' => 1])

                <div class="artical-leftyy">
                    <div class="art-bane">
                        <?php
                        $image_path = $finalData['absolute_path'] . 'public/assets/Frontend/images/event_image/' . trim($list[0]->profile_image);
                        if (file_exists($image_path) && $list[0]->profile_image != '') {
                            $post_image_path = url('/public/assets/Frontend/images/event_image/' . trim($list[0]->profile_image));
                        } else {
                            $post_image_path = '';
                        }
                        ?>
                        @if($post_image_path != '')
                            <img src="{{ $post_image_path }}" width="100%">
                        @endif
                    </div>
                </div>
                <?php
                $session_data = Session::get('user_account');
                ?>
                @if($response == '1')
                    <span style="font-size: large; font-style: oblique;"><b>Your {{ $type }} has been booked successfully. To know more about the {{ $type }}, <a href="{{url('/')}}/view-{{$type}}/{{$list[0]->title}}-{{$list[0]->id}}">click here</a></b></span>
                @else
                    <span style="font-size: large; font-style: oblique;"><b>Your {{ $type }} booking has failed. Please try again, <a href="{{url('/')}}/view-{{$type}}/{{$list[0]->title}}-{{$list[0]->id}}">click here</a></b></span>
                @endif
           </div>
            </div>
            @include('Frontend.includes.google_ads', ['ads_public_only' => 1])
        </div>
    </div>
    </div>
</section>



<input type="hidden" value="{{ url('/').'/public/assets/Frontend/images/event_image/'.$list[0]->profile_image }}"
       id="image_url">
<input type="hidden" value="{{ url('/').'/public/assets/Frontend/img/image-not-found3.jpg' }}" id="default_image_url">
<input type="hidden" value="{{ url('/').'/view-event/'.$list[0]->id  }}" id="page_link">
<input type="hidden" value="{{ $list[0]->title }}" id="event_title">
<input type="hidden" value="{{ $list[0]->description }}" id="event_description">
<input type="hidden" value="{{ $list[0]->id }}" id="event_id">
<input type="hidden" value="{{$finalData['user_session']['id']}}" id="user_id">


<!-- Share Event Code ; Including share_post file and passing feature as argument -->
@include('Frontend.includes.post_detail_share_fx', [ 'feature' => "event"])

<script>


    $(document).ready(function () {


    });


</script>


</body>
</html>
@include('Frontend.includes.footer')
