<?php
$image_path = $finalData['absolute_path'] . 'public/assets/Frontend/images/event_image/' . $list[0]->profile_image;
if (file_exists($image_path) && $list[0]->profile_image != '') {
    $post_image_path = url('/') . '/assets/Frontend/images/event_image/' . $list[0]->profile_image;
} else {
    $post_image_path = url('/') . '/assets/Frontend/img/image-not-found3.jpg';
}
$meta_desc = fixtags($finalData['header']['meta_description']);

function fixtags($text)
{
    $text = htmlspecialchars($text);
    $text = preg_replace("/=/", "=\"\"", $text);
    $text = preg_replace("/&quot;/", "&quot;\"", $text);
    $tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
    $replacement = "<$1$2$3$4$5$6$7$8$9$10>";
    $text = preg_replace($tags, $replacement, $text);
    $text = preg_replace("/=\"\"/", "=", $text);
    return $text;
}

$session_data = Session::get('user_account');

?>
@include('Frontend.includes.head')
@if($session_data != "")
    @include('Frontend.includes.header')
@else
    @include('Frontend.includes.outer-header')
@endif
@include('Frontend.includes.session-message')



<meta property="og:title" content="{{$finalData['header']['meta_title']}}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{Request::fullUrl()}}">
<meta name="keywords"
      content="@if(isset($list[0]->tag) && $list[0]->tag != ''){{$list[0]->tag}} @else {{$list[0]->title}} @endif">
<meta property="og:tags" content='{{$finalData["header"]["meta_keywords"]}}'>
<meta name="description" content="{{$finalData['header']['meta_title']}}">
<meta property="og:description" content="{{$meta_desc}}">
<meta property="og:image" content="{{$post_image_path}}">
<link rel="stylesheet" href="{{ url('/assets/Frontend/css/horizontal-slider-reset.css') }}">
<link rel="stylesheet" href="{{ url('/assets/Frontend/css/horizontal-slider-style.css') }}">
<script src="{{url('/assets/Frontend/js/horizontal-slider-main.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<link href="{{ url('/public/assets/css/views/post_detail.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/calendar.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/calendar_compact.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/assets/Frontend/css/calendar/style.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{url('/assets/Frontend/js/calendar/en.js')}}"></script>
<!--<script src="{{url('/assets/Frontend/js/calendar/calendar.js')}}"></script>-->
@include('Frontend.includes.post_detail_mention_user_ids_top')
<!-------------------------------------------------------LOG-STEP-ONE START------------------------------------------------------>
<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/event_bg.jpg') }});">
    <?php  $title_of_post = $list[0]->title?>
    @include('Frontend.includes.post_detail_title', [ 'feature' => $type, 'title' => $title_of_post ])
</section>


<section class="artical-post-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">

                @include('Frontend.includes.google_ads', ['ads_public_only' => 1])

                <div class="artical-leftyy">
                    <div class="art-bane">
                        <?php
                        $image_path = $finalData['absolute_path'] . 'public/assets/Frontend/images/event_image/' . trim($list[0]->profile_image);
                        if (file_exists($image_path) && $list[0]->profile_image != '') {
                            $post_image_path = url('/public/assets/Frontend/images/event_image/' . trim($list[0]->profile_image));
                        } else {
                            $post_image_path = '';
                        }
                        ?>
                        @if($post_image_path != '')
                            <img src="{{ $post_image_path }}" width="100%">
                        @endif
                    </div>
                </div>
                <?php
                $session_data = Session::get('user_account');
                ?>
                <input type="hidden" name="to_id_fk" id="to_id_fk" value="{{$list[0]->user_id_fk }}">
                <input type="hidden" name="event_id" id="event_id" value="{{ $list[0]->id }}">
                <div class="venue-event-art">
                    <h2>{{ucfirst($type)}} Details

                    </h2>
                    <div class="inter-venue-art">
                        <div class="row">
                            @if($list[0]->venue != '')
                                <div class="col-sm-12">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-globe"></i> Address:</dt>

                                        <dd>{{ ucfirst($list[0]->venue)  }}</dd>

                                    </dl>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @if($list[0]->location != '')
                                <div class="col-sm-12">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-Location"></i> Location:</dt>

                                        <dd>{{ ucfirst($list[0]->location) }}</dd>

                                    </dl>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @if(isset($list[0]->start_date) && $list[0]->start_date != '' && $list[0]->start_date != "1970-01-01 00:00:00")
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-Date"></i> Starting Date:</dt>

                                        <dd>{{ date("d-m-Y", strtotime($list[0]->start_date)) }}</dd>
                                    </dl>
                                </div>
                            @endif
                            @if(isset($list[0]->start_time) && $list[0]->start_time != '')
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-Time"></i> Starting TIME:</dt>
                                        <dd>{{ ucfirst($list[0]->start_time) }}</dd>
                                    </dl>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @if(isset($list[0]->end_date) && $list[0]->end_date != '' && $list[0]->end_date != "0000-00-00" && $list[0]->end_date != "1970-01-01")
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-Date"></i> Ending Date:</dt>
                                        <dd>{{ date("d-m-Y", strtotime($list[0]->end_date)) }} </dd>
                                    </dl>
                                </div>
                            @endif
                            @if(isset($list[0]->end_time) && $list[0]->end_time != '')

                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="icon-Time"></i> Ending TIME:</dt>
                                        <dd>@if($list[0]->end_time != ''){{ $list[0]->end_time }}@else
                                                - @endif</dd>
                                    </dl>
                                </div>
                            @endif
                            @if(isset($list[0]->min_age) && $list[0]->min_age != 0)
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="fa fa-user"></i> Min Age:</dt>
                                        <dd>@if(count($list[0]->min_age) != ''){{ $list[0]->min_age }}@else
                                                - @endif</dd>
                                    </dl>
                                </div>
                            @endif
                            @if(isset($list[0]->max_age) && $list[0]->max_age != 0)
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="fa fa-user"></i> Max Age:</dt>
                                        <dd>@if(count($list[0]->max_age) != ''){{ $list[0]->max_age }}@else
                                                - @endif</dd>
                                    </dl>
                                </div>
                            @endif
                            @if(isset($list[0]->ccost))
                                <div class="col-sm-6">
                                    <dl class="dl-horizontal">
                                        <dt><i class="fa ">{{$list[0]->ccurrency}}&nbsp;</i> Cost:</dt>
                                        <dd>{{$list[0]->ccurrency}} {{$list[0]->ccost}}</dd>
                                    </dl>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-sm-3">
                <div class="right-aticalss">
                    <div class="art-bane"></div>
                    <div class="venue-event-art">
                        <h2>Bill Summary</h2>
                        <div class="inter-venue-art">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <b><div style="width:160px; height:80px; overflow: hidden; text-overflow: ellipsis">{{$list[0]->title}} - <p>{{$list[0]->description}}</p></b></div></td>
                                    <td align="right">{{$ticket_count}} Tickets</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="border: 1px dashed lightgrey;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Subtotal</b></td>
                                    <td align="right">Rs. {{$list[0]->ccost * $ticket_count}}</td>
                                </tr>
                                <tr>
                                    <td><b>GST</b></td>
                                    <td align="right">- 0</td>
                                </tr>
                                <tr>
                                    <td><b>Coupon Discount</b></td>
                                    <td align="right">- 0</td>
                                </tr>

                            </table>
                        </div>
                        <table style="width:100%; height: 40px">
                            <tr style="background-color: #fdffbd; width:100%; padding: 20px">
                                <td><b>&nbsp;PAYABLE AMOUNT</b></td>
                                <td align="right"><b>Rs. {{$list[0]->ccost * $ticket_count}}&nbsp;</b></td>
                            </tr>
                        </table>
                    </div>


                    <a href="javascript:void(0)" class="conee-btn-new btn" id="buy" style="width: 100%"
                       onclick="location.href='{{url('/')}}/order/confirm/{{$type}}/{{$list[0]->cost_id}}/{{$list[0]->ccost}}/{{$ticket_count}}'"><b>PROCEED</b></a>

                </div>
            </div>
            @include('Frontend.includes.google_ads', ['ads_public_only' => 1])
        </div>
    </div>
    </div>
</section>

<input type="hidden" id="latitude" name="latitude" value="{{ $list[0]->latitude }}">
<input type="hidden" id="longitude" name="longitude" value="{{ $list[0]->longitude }}">
<input type="hidden" id="venue" name="venue" value="{{ $list[0]->venue }}">


<input type="hidden" value="{{ url('/').'/public/assets/Frontend/images/event_image/'.$list[0]->profile_image }}"
       id="image_url">
<input type="hidden" value="{{ url('/').'/public/assets/Frontend/img/image-not-found3.jpg' }}" id="default_image_url">
<input type="hidden" value="{{ url('/').'/view-event/'.$list[0]->id  }}" id="page_link">
<input type="hidden" value="{{ $list[0]->title }}" id="event_title">
<input type="hidden" value="{{ $list[0]->description }}" id="event_description">
<input type="hidden" value="{{ $list[0]->id }}" id="event_id">
<input type="hidden" value="{{$finalData['user_session']['id']}}" id="user_id">


<!-- Share Event Code ; Including share_post file and passing feature as argument -->
@include('Frontend.includes.post_detail_share_fx', [ 'feature' => "event"])

<script>


    $(document).ready(function () {


    });


</script>


</body>
</html>
@include('Frontend.includes.footer')
