@include('Frontend.includes.head')
@include('Frontend.includes.header')
<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/contactus-banner.png') }});">
    <div class="container">
        <h1>{!!$arr_cms[0]->page_title!!}</h1>
    </div>
</section>
<section class="signup-content col-offset-top-99">
    <div class="container">
        <div class="contact-us-form">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-section">
                        <p>{!!$arr_cms[0]->page_content!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')