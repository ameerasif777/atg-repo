@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
    <div class="container">
        <div id="wrapper">
            <h4>Please let us know if you are a:</h4>
            <ul>
                <li data-value="1">Student</li>
                <li data-value="3">Professional</li>
                <li data-value="4">Company</li>
            </ul>
            <div style="width: 49%; display: inline-block; padding-left: 5vw;">
                <a id="skip" href="{{ url('/user-dashboard') }}">Skip</a>
            </div>
            <div style="width: 49%; text-align: right; display: inline-block; padding-right: 5vw;">
                <a id="next" href="javascript:void(0)">Next</a>
            </div>
        </div>
        <form method="post" action="{{url('signup3')}}" >
            {!! csrf_field() !!}
            <input type="hidden" name="user_type" id="user_type">
        </form>
    </div>

<style>
    #content {
        background: linear-gradient(to right bottom, #a9dfa582, #c7f0e82e,#ebf5e4a1, #f8ffe936, #f0fff173);
    }
    h4 {
        text-align: center;
        color: #616161;
    }
    #wrapper {
        max-width: 500px;
        margin: 50px auto 0 auto;
        background-color: #fff;
        border-radius: 5px;
        padding: 25px 5px 40px 5px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    }
    ul {
        padding: 15px 0 30px 0;
    }
    li {
        list-style: none;
        padding: 10px 0 10px 20px;
        margin: 15px;
        border-radius: 37px;
        box-shadow: 0 0 1px 0px #00000091;
        cursor: pointer;
        transition-duration: 0.3s;
        color: #757575;
        font-weight: 600;
    }
    li.selected {
         border: 4px solid #5aba67;
    }

    a#skip, a#next {
        padding: 8px 35px;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        border-radius: 50px;
        font-size: 15px;
    }
    a#skip {
        color: #757575;
    }
    a#next {
        background: #53b154;
        color: #fff;
    }
</style>
<script>
    $('ul>li').click(function () {
        $('ul>li').removeClass('selected');
        $(this).addClass('selected');
    });
    $('#next').click(function () {
        let value = $('.selected').data('value');
        if (value === undefined) {
            swal({
                type: 'info',
                title: 'Please select an option',
                text: ''
            });
            return;
        }
        $('#user_type').val(value).parent('form').submit();
    });
</script>