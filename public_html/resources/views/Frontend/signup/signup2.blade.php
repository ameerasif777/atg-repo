@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/public/assets/css/views/mygroup.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ url('assets/Frontend/js/sweetalert2-7.19.3.all.min.js') }}" ></script>
<link rel="stylesheet" href="{{ url('public/assets/css/views/signup2.css') }}">

<section id="signup2">
    <section id="signup2-step1">
        <div id="search-overlay"></div>
        <div id="group-search-bar" style="padding-top: 25px">
            <i class="fa fa-search material-shadow-1"></i>
            <input type="text" class="material-shadow-1" placeholder="Search for groups on ATG">
            <div id="group-search-result" class="material-shadow-1"></div>
        </div>

        <div id="trending-group-container" class="material-shadow-1">
            <div class="group-type">
                <p>Trending Groups</p>
            </div>
            <div class="trending-group-wrapper">
                @foreach ($trending_groups as $trending_group)
                    <?php
                        if (file_exists(__DIR__ . '/../../../public/assets/Backend/img/group_img/icon/' . $trending_group->icon_img)) {
                            $group_image_path = url('public/assets/Backend/img/group_img/icon/' . $trending_group->icon_img);
                        } else {
                            $group_image_path = url('public/assets/Backend/img/group_image.png');
                        }
                    ?>
                    <div class="trending-group material-shadow-2" data-group-id="{{ $trending_group->id }}" title="{{ $trending_group->group_name }}">
                        <img src="{{ $group_image_path }}" alt="">
                        <span>{{ $trending_group->group_name }}</span>
                    </div>
                @endforeach
            </div>
        </div>

        <div id="major-group-container" class="material-shadow-1">
            <div class="group-type">
                <p>All Groups on ATG</p>
            </div>
            <div class="major-group-wrapper"></div>
            <div id="major-group-loading-icon" style="text-align: center">Loading <i class="fa fa-spin fa-spinner"></i></div>
        </div>

        <div class="signup2-footer row" style="z-index: 1">
            <div class="col-xs-6" style="padding-top: 8px;">
                <span>Selected Groups(<span id="selected-group-count">0</span>)</span>
            </div>
            <div class="col-xs-6">
                <button id="btnStep2" class="material-shadow-1" onclick="gotoStep2()">NEXT</button>
            </div>
        </div>
    </section>

    <section id="signup2-step2" style="display: none"></section>
    <div class="signup2-footer row" style="z-index: 0">
        <div class="col-xs-6">
            <button class="material-shadow-1" onclick="gotoStep1()">BACK</button>
        </div>
        <div class="col-xs-6">
            <button id="btnStep3" class="material-shadow-1" onclick="gotoStep3()">NEXT</button>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')
<script src="{{ url('public/assets/js/views/signup2.js') }}"></script>
<script>
function openNewTab(group_name) {
        var url = '{{url('/')}}/get-group-event/' + group_name;
        window.open(url, '_blank');
    }
    function openNewTab1(group_name) {
        var url = '{{url('/')}}/go/' +group_name;
        window.open(url, '_blank');
    }
    var g = $("#user_already_groups").val();
    var group = [];
    var sub_main_parent = [];
    $(function() {
        $("#search_group").autocomplete({
            source: function(request, response) {
                $.ajax({
                    type: "get",
                    url: '{!!url("/")!!}/get-group-name',
                    data: {
                        search_group: $("#search_group").val(),
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data != null) {
                            response($.map(data, function(item) {
                                return {
                                    label: item.group_name,
                                    id: item.id,
                                }
                            }))
                        } else
                            $(".ui-autocomplete").css({
                                "display": "none"
                            });
                    },
                    error: function(data) {
                    }
                })
            }, delay: 100,
            minLength: 1,
            focus: function(event, item) {
                $('#search_group').val(item.item.label);
                return false;
            },
            select: function(e, u) {
                $.get('{!!url("/")!!}/get-parent-group-details', {parent_id: u.item.id,user_already_groups: $("#user_already_groups").val(),}, function(msg) {
                    if (msg != '') {
                        //$(".search_group").html("");
                        $('#text_msg_for_grp').remove();
                        addGroup(u.item.id)
                        $('.category-listing-form1').html("");
                        $('.category-listing-form1').html(msg);
                        $('#btn_submit').show();
                        ready();
                    }
                    $('input[type="checkbox"]').click(function() {

                        if ($(this).prop("checked") == true) {
                            $('#btn_submit').show();
                        } else if ($(this).prop("checked") == false) {
                            var a = $("input[type='checkbox']");
                            if ((a.filter(":checked").length / 2) - 1 > 0) {
                                $('#btn_submit').show();
                            } else {
                                $('#btn_submit').hide();
                            }
                        }

                    });
                });
            }
        });
    });
    function getParentGroup(parent_id)
    {
        $('#text_msg_for_grp').remove();
        //$(".search_group").html("");
        $.get('{!!url("/")!!}/get-parent-group', {parent_id: parent_id,user_already_groups:group}, function(msg) {
            if (msg != '') {
                $("#main_parent").val(parent_id);
                $('.category-listing-form1').html(msg);
                ready();
                if (group.length > 0)
                {
                } else {
                    $('#btn_submit').hide();
                }

            }

            var wid= $( window ).width();
          if(wid<767)
          {
           $("#content").animate({  scrollTop: $("#frm_user_registration2").offset().top + 10}, 1000);
            }
            $('input[type="checkbox"]').click(function() {

                if ($(this).prop("checked") == true) {
                    $('#btn_submit').show();
                } else if ($(this).prop("checked") == false) {
                    var a = $("input[type='checkbox']");
                    if ((a.filter(":checked").length / 2) - 1 > 0) {
                        $('#btn_submit').show();
                    } else {
                        $('#btn_submit').hide();
                    }
                }

            });
        });
    }

    function dropDown(parent_id){


        if ($('.parent' + parent_id).css("display") == 'block')
        {
            $('.dropdown-menu').hide();
            $('.parent' + parent_id).css("display", 'none');
        } else if ($('.parent' + parent_id).css("display") == 'none') {
            $('.dropdown-menu').hide();
            $('.parent' + parent_id).css("display", 'block');
        }
    }


    function showSubChild(child_id, parent_id)
    {
        $('.subparent' + parent_id).css('display', "none");
        $('.parent' + parent_id).css('display', "none");
        $('.parent' + parent_id).css('display', "none");
        if ($('.child' + child_id).hasClass('left-caret'))
        {

            $('.child' + child_id).addClass('right-caret');
            $('.child' + child_id).removeClass('left-caret');
        } else if ($('.child' + child_id).hasClass('right-caret')) {
            $('.child' + child_id).removeClass('right-caret');
            $('.child' + child_id).addClass('left-caret');
        }
    }

    function showChild(child_id, parent_id)
    {
        $('.subchild' + child_id).toggle();
        if ($('.child' + child_id).css("display") == 'block') {
            $('.parent' + parent_id).css("display", 'block');
            if ($('.subchild' + child_id).css("display") == 'block') {
                $('.sub-menu').css("display", 'none');
                $('.subchild' + child_id).css("display", 'block');
            } else if ($('.subchild' + child_id).css("display") == 'none')
            {
                $('.sub-menu').css("display", 'none');
                $('.subchild' + child_id).css("display", 'none');
            }

            if ($('.child' + child_id).hasClass('left-caret'))
            {
                $('.trigger').addClass('left-caret');
                $('.child' + child_id).addClass('right-caret');
                $('.trigger').removeClass('left-caret');
                $('.child' + child_id).removeClass('left-caret');
            } else if ($('.child' + child_id).hasClass('right-caret')) {
                $('.trigger').addClass('right-caret');
                $('.child' + child_id).removeClass('right-caret');
                $('.trigger').removeClass('left-caret');
                $('.child' + child_id).addClass('left-caret');
            }
        }
    }

//    function removeGroup(child_id)
//    {
//        var removeItem = child_id;
//        var index = group.indexOf(String(removeItem));
//        if (index > -1) {
//            group.splice(index, 1);
//            $("#user_group").val(group);
//            $(".usergroup" + child_id).remove();
//        }
//
//        if (group.length > 0)
//        {
//
////    $('#btn_submit').show();
//        } else {
//            console.log(group.length);
//            $('#btn_submit').hide();
//        }
//    }
    var temp_arr = [];
    function addGroup(event)
    {
        $.get('{!!url("/")!!}/get-group', {parent_id: event}, function(msg) {
//            $(".search_group").html("");
           // group=[];
            if (msg != '') {
                    $.each(msg, function(i, e) {
                        group.push(e.id);
//                        if($.inArray(e.id,temp_arr) == -1){
                            $(".search_group").append("<span class='user_selected_group " + e.parent_id + "' id='span_" + e.id + "'><li class='usergroup" + e.id + "'><span>" + e.group_name + "<a href='javascript:void(0)'  onClick='return removeGroup(" + e.id + ")'><i class='fa fa-remove'></i></a></span></li></span>");
                             $("#user_group").val(group);
//                        }
                    });

                     $("#g_num").text(group.length);
                    if (group.length == 0){
                        $('#btn_submit').hide();
                    }
                return false;
            }
        });
    }
    
    function removeGroup(value){
         $.get('{!!url("/")!!}/get-group', {parent_id: value}, function(msg) {
            if (msg != '') {
                    $.each(msg, function(i, e) {
                        $("#"+e.id).prop('checked',false);
                        group.pop(e.id);
                        $('.usergroup'+e.id).remove();
                         $("#user_group").val(group);
                    });
                return false;
            }
        });
         
          $("#g_num").text(group.length-1);
        if (group.length == 0){
            $('#btn_submit').hide();
        }
    }


    function ready() {
        $('.chk_parent').click(function() {

            if ($(this).prop("checked") == true){
                var child = $(this).parent().parent().find('input.chk_child').prop("checked", true);
                var sub_child = $(this).parent().parent().find('input.chk_sub_child').prop("checked", true);
                addGroup(this.value)
                $('#btn_submit').show();
            }
            if ($(this).prop("checked") == false)
            {
               
                $(this).parent().parent().find('input.chk_child').prop("checked", false);
                $(this).parent().parent().find('input.chk_sub_child').prop("checked", false);
               
                $.each($(this).parent().parent().find('input.chk_child'),function(index,value){
                    $("."+this.value).remove();
                    removeGroup(this.value)
                     
                })
                $("#span_"+this.value).remove();
                $('#btn_submit').hide();
            }

        });
        $('.chk_child').click(function() {

            if ($(this).prop("checked") == true){
                $(this).parent().find('input.chk_sub_child').prop("checked", true);
                $(this).parent().parent().parent().parent().find('input.chk_parent').prop("checked", true);
                addGroup(this.value)
                addGroup($(this).parent().find('input.chk_sub_child').val())
                $('#btn_submit').show();
            }
            if ($(this).prop("checked") == false){
                removeGroup(this.value)
                $(this).parent().find('input.chk_sub_child').prop("checked", false);
                $.each($(this).parent().find('input.chk_sub_child'),function(index,value){
                    $("."+this.value).remove();
                    removeGroup(this.value)
                })
                $("#span_"+this.value).remove();
                $('#btn_submit').hide();
            }

        });
        $("input.chk_sub_child").click(function() {

            if ($(this).prop("checked") == true) {
                $(this).parent().parent().parent().find('.chk_child').prop("checked", true);
                $(this).parent().parent().parent().parent().parent().find('input.chk_parent').prop("checked", true);
                $(this).parent().parent().parent().parent().find('.chk_parent').prop("checked", true);
                addGroup(this.value)
                $('#btn_submit').show();
            } else {
                removeGroup(this.value)
                if($('[name="chk_end_grp[]"]:checked').length == 0){
                   $(this).parent().parent().parent().find('.chk_child').prop("checked", false);
                   $(this).parent().parent().parent().parent().parent().find('input.chk_parent').prop("checked", false);
                   var chk_child_id = $(this).parent().parent().parent().find('.chk_child').val();
                   $('.usergroup'+chk_child_id).remove();
                }
                $.each($(this).parent().find('input.chk_child'),function(index,value){
                    removeGroup(this.value)
                })
                $('#btn_submit').hide();
            }
        });
    }

    $(window).on('resize', function(){
      var win = $(this);
      if (win.width() < 763) { 

      $('#demo').addClass('collapse');

      }
    else
    {
        $('#demo').removeClass('collapse');
    }

});
</script>

<script>

    /*******************************
* ACCORDION WITH TOGGLE ICONS
*******************************/
  
   $("#demo").on('show.bs.collapse', function(){
         $(".c_btn").html('<span class="glyphicon glyphicon-chevron-down"></span>');
    });
   
    $("#demo").on('hide.bs.collapse', function(){
        $(".c_btn").html('<span class="glyphicon glyphicon-chevron-up"></span>');
    });
   
    
  </script>
