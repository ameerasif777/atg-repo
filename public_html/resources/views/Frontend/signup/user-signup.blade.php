@extends('Frontend.layouts.default')
@section('content') 
<?php
if (Session::has('linkedin_data'))
    $user = Session::get('linkedin_data');
?>
@if((Request::segment(2) == 'register'))
@include('Frontend.includes.bottom-nav')
@endif
<?php Session::put('ruser', Request::get('r', '')); ?>
<section class="signup-content col-offset-top-99">
    <div class="container">
        <div class="sign-up-form">
            <h2 class="sign-up-head">User Sign Up</h2>
            <p class="user-reg">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam d do eiusmod tempor incididunt.</p>
            <form id="frm_user_registration" name="frm_user_registration" method="POST" action="{{url('/signup1?r=' . Request::get('r'))}}" class="form-horizontal sign-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="col-sm-12 col-xs-12">
                        <label class="control-label">Email ID  :</label>
                        <div class="sign-input">
                            <input type="text" class="form-control" placeholder="Email address" name="user_email" id="user_email" value="{{isset($user->emailAddress)?$user->emailAddress:''}}">
                            <i class="fa fa-pencil"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">First Name :</label>
                        <div class="sign-input">
                            <input type="text" class="form-control" id="first_name" name="first_name" autofocus placeholder="First name" value="{{isset($user->firstName)?$user->firstName:''}}">                                            
                            <i class="fa fa-institution"></i>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Last Name :</label>
                        <div class="sign-input">
                            <input type="text" class="form-control" placeholder="Last name" name="last_name" id="last_name" value="{{isset($user->lastName)?$user->lastName:''}}">
                            <i class="fa fa-envelope"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-6 col-xs-12">
                        <label class="control-label">Password :</label>
                        <div class="sign-input">
                            <input type="password" class="form-control" placeholder="Enter password" name="user_password" id="user_password">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-6 col-xs-12">
                        <label class="control-label">Confirm Password :</label>
                        <div class="sign-input">
                            <input type="password" class="form-control" placeholder="Confirm password" name="cnf_user_password" id="cnf_user_password">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Your Professional Headline :</label>
                        <div class="sign-input">
                            <textarea class="form-control" id="professional_headline" name="professional_headline" autofocus placeholder="Professional Headline"></textarea>                                            
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Ad Head Shot :</label>
                        <div class="sign-input">
                            <input type="file" name="head_shot" id="head_shot">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-7 col-sm-7 col-xs-12  text-left">
                        <div class="terms-conditon clearfix">
                            <div class="checkbox"> <label> <input type="checkbox">I agree 

                                    <a href="{{url('/')}}/cms/Terms-Conditions" target="_blank">Terms & Conditions, Data & Cookies Policy</a>
                                </label> </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-sm-5 col-xs-12 text-right">
                        <button type="submit" class="sign-up-btn btn">Sign up</button>

                    </div>
                </div>
            </form>
            <div class="or-btns-div">
                <div class="row">
                    <div class="col-md-6">
                        <a  class="btn fb-cust-btn" id="facebook_connect"><i class="fa fa-facebook-f"></i> Log in with facebook</a>
                    </div>
                    <div class="col-md-6">
                        <a class="btn fb-cust-btn twe-cust-btn" ><i class="fa fa-twitter"></i> Log in with twitter</a>
                    </div>
                    <div class="col-md-6">
                        <a  class="btn fb-cust-btn gp-cust-btn" ><i class="fa fa-google-plus"></i> Log in with google+</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('/')}}/user/linkedin-connect" class="btn fb-cust-btn linked-cust-btn" ><i class="fa fa-linkedin"></i> Log in with linkedin</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@stop

@section('footer')
<script src="{{url('/')}}/assets/Frontend/js/registration/user-registration.js"></script>
<script src="{{url('/')}}/assets/Frontend/js/jquery.validate.password.js"></script>
<script src="{{url('/')}}/assets/Frontend/js/jquery-ui.min.js"></script>
<script>
$(function() {
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1960:2003",
    });

    $("#dob").keydown(function() {
        return false;
    });
});
</script>
@stop