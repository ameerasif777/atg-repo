<?php
$user_id = Illuminate\Support\Facades\Auth::user()->id;

$first_name = Illuminate\Support\Facades\Auth::user()->first_name;

$arr_profile_condition = ['user_id' => $user_id, 'profile_current_status' => '1'];
$arr_profile_pic = (array) DB::table('trans_user_profile_images')->where($arr_profile_condition)->select('profile_pic')->first();
$arr_cover_condition = ['user_id' => $user_id, 'cover_current_status' => '1'];
$arr_cover_pic = (array) DB::table('trans_user_profile_images')->where($arr_cover_condition)->select('cover_pic')->first();
?>

@include('Frontend.includes.user.header')
@include('Frontend.includes.user.top-nav')

@yield('content')	

<?php
//$arr_profile_pic = (array)DB::table('trans_user_institute_follow_join')->where('user_id_fk','!=',$user_id)->select('profile_pic')->first();

		$institute_details = DB::table('mst_institutes as mst_i')
			->join('mst_users as u', 'mst_i.id', '=', 'u.id')
			->leftJoin('trans_user_profile_images as pi', 'mst_i.id', '=', 'pi.user_id')
			->leftJoin('trans_user_institute_follow_join as fj', 'mst_i.id', '=', 'fj.institute_id_fk')
			->select('mst_i.*', 'pi.*','fj.*')
			->where('u.user_status', '=','1')
			->get();


?>


@include('Frontend.includes.user.institute-right-section-follower')

@include('Frontend.includes.user.right-section-adv')
@include('Frontend.includes.user.footer-cms-nav')
@include('Frontend.includes.user.footer-home')

@yield('footer') 