<?php
$user_id=Illuminate\Support\Facades\Auth::user()->id;

$first_name=Illuminate\Support\Facades\Auth::user()->first_name;

$arr_profile_condition = ['user_id' => $user_id ,'profile_current_status' => '1'];
$arr_profile_pic = (array)DB::table('trans_user_profile_images')->where($arr_profile_condition)->select('profile_pic')->first();
$arr_cover_condition = ['user_id' => $user_id ,'cover_current_status' => '1'];
$arr_cover_pic = (array)DB::table('trans_user_profile_images')->where($arr_cover_condition)->select('cover_pic')->first();
?>

@include('Frontend.includes.user.header')
@include('Frontend.includes.user.top-nav')
@include('Frontend.includes.user.banner')

@yield('content')	

@include('Frontend.includes.user.right-section-follower')
@include('Frontend.includes.user.footer-cms-nav')
@include('Frontend.includes.user.footer-home')

@yield('footer') 