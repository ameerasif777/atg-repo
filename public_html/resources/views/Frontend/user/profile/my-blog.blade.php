@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<!--<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>Account Settings</h1>
    </div>
</section>-->
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <h6>account settings</h6>
                    <div class="list-group">
                        <a class="list-group-item  text-center"  href="{{url('/')}}/edit-user-bio">
                            Profile Settings
                        </a>
                        <a class="list-group-item  text-center"  href="{{url('/')}}/bio-setting">
                            Bio
                        </a>
                        @if($arr_user_data['user_type'] != 4)
                        <a class="list-group-item text-center " href="{{url('/')}}/team">
                            Create Team
                        </a>
                        <a class="list-group-item text-center" href="{{url('/')}}/my-teams">
                            My Team
                        </a>
                        @endif 
                        @if($arr_user_data['user_type'] == 1)
                        <a class="list-group-item text-center" href="{{url('/')}}/manage-resumes">
                            Manage Resumes
                        </a>
                        <a class="list-group-item text-center" href="{{url('/')}}/manage-job-alert">
                            Manage Job Alert
                        </a>
                        @endif
                        <a class="list-group-item text-center " href="{{url('/')}}/notification-setting">
                            Notification Settings
                        </a>
                        <a class="list-group-item text-center " href="{{url('/')}}/pwd-setting">
                            Password Settings
                        </a>
                        <a class="list-group-item text-center " href="{{url('/')}}/messages">
                            Messages
                        </a>
                        <a class="list-group-item text-center active" href="javascript:void(0);">
                            Blogs
                        </a>
                        <a class="list-group-item text-center" href="{{url('/')}}/saved-events">
                            Saved Events
                        </a>
                        <a class="list-group-item text-center" href="{{url('/')}}/saved-meetups">
                            Saved Meetups
                        </a>
                        @if($arr_user_data['user_type'] != 1)
                        <a class="list-group-item text-center" href="{{url('/')}}/saved-job">
                            Saved Jobs
                        </a>
                        @endif
                        @if($arr_user_data['user_type'] == 1)
                        <a class="list-group-item text-center" href="{{url('/')}}/saved-jobs">
                            Saved Jobs
                        </a>
                        @endif
                        <a class="list-group-item text-center" href="{!!url(//logout')!!}">
                            Logout
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">

                        </div>
                    </div>

                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">

                        </div>
                    </div>
                    @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content  tem-capter clearfix">

                    </div>
                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">

                        </div>
                    </div>
                    @endif
                    @if($arr_user_data['user_type'] == 1)
                    <div class="bhoechie-tab-content">
                        <div class="tema-members-outs">

                        </div>
                    </div>

                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">

                        </div>
                    </div>
                    @endif
                    <div class="bhoechie-tab-content ">
                        <div class="main-inner-tab-info clearfix">

                        </div>
                    </div>
                    <div class="bhoechie-tab-content ">

                    </div>
                    <div class="bhoechie-tab-content ">

                    </div>
                    <div class="bhoechie-tab-content active">
                        <div class="main-inner-tab-info clearfix">

                            <div class="marketing">
                                <section id="blog-landing">
                                    @if(count($arr_blog) == 0)
                                    No Records Found.
                                    @endif
                                    @foreach ($arr_blog as $blog) 
                                    <article class="white-panel">
                                        <div class="atg-meet-up atg-save-events">
                                            <?php
                                            $path = url('/public/assets/Frontend/images/blog_image/' . $blog->blog_image);
                                            if (!(file_exists($path)) && $blog->blog_image != '') {
                                                $path = url('/public/assets/Frontend/images/blog_image/' . $blog->blog_image);
                                            } else {
                                                $path = url('/public/assets/Frontend/img/image-not-found2.jpg');
                                            }
                                            ?>

                                            <div class="meet-imger" style="background-image:url({{ $path }})"></div>
                                            <a href="{{url('/')}}/blog-detail/{!! base64_encode($blog->rel_id) !!}"><h3>{!! $blog->title !!}</h3></a>
                                            <div class="reviesa"><span><i class="icon-Date"> Posted On:</i>{{ date("d-m-Y", strtotime($blog->created_at)) }}</div>
                                            <span class="reviesa"><i class="fa fa-user"></i> by {!! $blog->user_name !!}</span>
                                            <div class="ent-coments clearfix">                                                
                                                <span class="pull-right"><i class="fa fa-comments-o"></i> {!! $blog->count !!}</span>
                                            </div>
                                        </div>
                                    </article>
                                    @endforeach

                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>

                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')
<script>
    $(document).ready(function() {
//                                            $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
//                                            e.preventDefault();
//                                                    $(this).siblings('a.active').removeClass("active");
//                                                    $(this).addClass("active");
//                                                    var index = $(this).index();
//                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
//                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
//                                            });
        $("img").error(function() {
            $(this).attr("src", "{{url('/')}}/public/assets/Backend/img/image-not-found.png");
        });
    });

</script>
<script>
    function del(team_id) {
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: 'delete-team/' + btoa(team_id),
                data: '',
                dataType: 'json'
            }).success(function(msg) {
                location.reload();
            })


        }
        return false;
    }
    function rem(resume_id) {
        if (confirm("Are you sure want to remove this resume?")) {
            $.ajax({
                url: 'delete-resume/' + btoa(resume_id),
                data: '',
                dataType: 'json'
            }).success(function(msg) {
                location.reload();
            })


        }
        return false;
    }
    function remov(job_id) {
        if (confirm("Are you sure want to remove this saved job?")) {
            $.ajax({
                url: 'delete-saved-job/' + btoa(job_id),
                data: '',
                dataType: 'json'
            }).success(function(msg) {
                location.reload();
            })


        }
        return false;
    }

    function readURL(input) {
//    var ext = input.value.match(/\.(.+)$/)[1];
//            switch (ext)
//    {
//    case 'jpg':
//            case 'bmp':
//            case 'png':
//            case 'tif':
//            if (input.files && input.files[0]) {
//                console.log(input.files)
//                console.log(input.files[0].height)
//    var reader = new FileReader();
//    console.log(reader)
//            reader.onload = function(e) {
//            $('#blah').css("display", "block");
//                    $('#blah').attr('src', e.target.result);
//                    $('#remove_image').css("display", "block");
//            }
//
//    reader.readAsDataURL(input.files[0]);
//    }
//    break;
//            default:
//            alert('Only Image allowed !');
//            input.value = '';
//    }

    }

    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#profile_pic").prop("value", "");
    }


</script> 
<script>

    function checkResumeCount() {
        $.ajax({
            url: 'resume-count',
            data: '',
            dataType: 'json'
        }).success(function(msg) {
            var resume_count = msg.resume_count[0].count
            if (resume_count == 3) {
                if (confirm("Are you sure want to remove first uploaded resume?")) {
                    $('#upload_resumes').submit();
                } else {
                    return false;
                }
            }
        })
    }

    $(document).ready(function() {

        $('#file').change(function() {
            var val = $(this).val();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf)$");
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please upload a file only of type pdf,doc,docx.');
            }
            var f = this.files[0]

            if (f.size > 10000000 || f.fileSize > 10000000)
            {
                alert("Allowed file size less than 10 MB.")
                this.value = null;
            }



        });
        var _URL = window.URL || window.webkitURL;
        var flag = true;

        $("#profile_pic").change(function(e) {

            var file, img;
            var ext = this.files[0].name.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert('Only JPG, PNG or GIF files are allowed');
                $("#profile_pic").val('');
            } else {

                if (file = this.files[0]) {
                    img = new Image();
                    img.onload = function() {
                        if (this.width >= 1900 && this.height >= 1200) {
                            var reader = new FileReader();
                            console.log(reader)
                            reader.onload = function(e) {
                                $('#blah').css("display", "block");
                                $('#blah').attr('src', e.target.result);
                                $('#remove_image').css("display", "block");
                            }

                            reader.readAsDataURL(file);
                        } else {
                            //flag = false;
                            alert("Image height and width should be greater than 1900*1200");
                            // $('#blah').attr('src', './media/front/img/image-not-found.png');
                            $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                        }

                    };
                    img.src = _URL.createObjectURL(file);
                    readURL(this);

                }
            }
        });


    });
</script> 





