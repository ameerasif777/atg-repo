<?php $session_data = Session::get('user_account'); ?>
@include('Frontend.includes.head')

@if ($session_data != "")
@include('Frontend.includes.header')
@else
@include('Frontend.includes.outer-header',['sign_up'=>true])
@endif
@include('Frontend.includes.session-message')
<link href="{{ url('/assets/Frontend/css/gallery/demo.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/css/gallery/component.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/public/assets/css/views/profile.css') }}" rel="stylesheet" type="text/css" />

<script src="{{url('/assets/Frontend/js/gallery/modernizr.custom.js')}}"></script>
<?php
$user_cover_image = config('cover_path') . $arr_public_user_data["id"] . '/' . $arr_public_user_data['cover_photo'];
if (Storage::exists($user_cover_image) && $arr_public_user_data['cover_photo'] != '') {
    $cover_image = config('cover_image_url'). $arr_public_user_data["id"] . '/' . $arr_public_user_data['cover_photo'];
} else {
    $cover_image = config('img').'image-not-found3.jpg';
}
?>
<section class="persnal-sec" style="background-image:url({{$cover_image}});">
    <div class="container">
        <div class="inner-bios">
            <div class="row">
                <div class="col-md-4">
                    <div class="main-pros-information">
                        <?php
                        $session_data = Session::get('user_account');
                        ?>                        
                        <div class="main-pross">
                            <?php
                            $user_profile_image = config('profile_path'). $arr_public_user_data['id'] . '/thumb/' . $arr_public_user_data['profile_picture'];
                            if (Storage::exists($user_profile_image) && $arr_public_user_data['profile_picture'] != '') {
                                $profile_picture = config('profile_pic_url'). $arr_public_user_data['id'] . '/thumb/' . $arr_public_user_data['profile_picture'];
                            } else {
                                if ($arr_public_user_data['gender'] == '0') {
                                    $profile_picture = config('avatar_male');
                                } else {
                                    $profile_picture =config('avatar_female');
                                }
                            }
                            ?>                            
                               <img  id="main_profile1" src="{{$profile_picture}}"/>                            
                        </div>
                        <!-- --username start----- -->
                        @if(isset($arr_public_user_data['user_name']) && (!empty($arr_public_user_data['user_name'])))
                        <h2><span style="color:#337ab7;">@</span><u><a href="">{!!$arr_public_user_data['user_name']!!}</u></a></h2>
                        @endif
                        <!-- --username end------- -->
                        @if(isset($arr_public_user_data) && (!empty($arr_public_user_data)))
                        <h2>{!!$arr_public_user_data['first_name'].' '.$arr_public_user_data['last_name']!!}</h2>
                        @endif
                        <div class="address-user">{!!isset($arr_public_user_data['location'])  && $arr_public_user_data['location']!='' ?$arr_public_user_data['location'] :'---' !!}</div>
                        <div class="address-thougt">{!!isset($arr_public_user_data['tagline']) && $arr_public_user_data['tagline']!=''  ?$arr_public_user_data['tagline'] :'---' !!}</div>
                        <h2>{!!isset($arr_public_user_data['profession']) && $arr_public_user_data['profession']!='' ?$arr_public_user_data['profession'] :'---' !!}</h2>
                        <!--<div class="address-user">{!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} | 12,558 views</div>-->
                        
                        @if($arr_public_user_data['user_type'] == 4)
                            <div class="address-user abc">
                            @if($jobcount > 0)
                                {!!($jobcount == 1 || $jobcount == 0)  ? $jobcount.' Job' :$jobcount.' Jobs' !!} 
                            @else    
                                {!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} 
                            @endif
                            </div>
                        @else
                            <div class="address-user">{!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} </div>
                        @endif

                    </div>
                </div>                
                @if(isset($session_data) && ($session_data != ''))
                
                @if($session_data['id'] == $arr_public_user_data['id']) 
                <form name="profilepicfrm" id="profilepicfrm" action="" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="file" id="profile_picture" style="opacity: 0" name="profile_picture" onchange="uploadProfileImage(this)">
                </form>
                @endif
                @endif
                <div class="col-md-8">
                    <div class="inters-abter">                        
                        @if(isset($session_data) && ($session_data != ''))
                        @if($session_data['id'] == $arr_public_user_data['id'])
                        <form name="coverform" id="coverform" action="" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="editors-pop"><a href="javascript:void(0)" onclick="getCoverPhoto();" title="Upload cover image"><i class="icon-Cover-Edit"></i></a></div>
                            <input type="file" id="cover_photo" style="opacity: 0" name="cover_photo" onchange="uploadImage(this)">
                        </form>
                        @endif
                        @endif                            
                        <div class="btin-aab">                            
                            
                            @if($session_data['id'] != $arr_public_user_data['id'])                               
                            @if($arr_public_user_data['user_type'] != '4')    
                          
                            @if($is_connect != '' && isset($is_connect) && (!empty($is_connect)))    
                        <?php // echo "if";die; ?>
                                    @if($is_connect[0]['from_user_id'] == $session_user_id && $is_connect[0]['status'] == '0' && $is_connect[0]['status'] != '3')                            

                                    <a href="javascript:void(0)" style="display: none;" class="conee-btn btn"  id="status" value="1" title="Click to connect"  onclick="profileConfirm({{$session_data['id']}},{{$arr_public_user_data['id']}})">Confirm </a>
                                    <a href="javascript:void(0)" class="conee-btn btn"  id="request"  title="Click to cancel request"  onclick="cancleRequest({{$session_data['id']}},{{$arr_public_user_data['id']}})">Requested </a>
                                    <a href="javascript:void(0)" style="display: none;"  class="conee-btn btn" id="disconnect" title="Click to disconnect"  onclick="DisConnected({{$session_data['id']}},{{$arr_public_user_data['id']}})">Connected </a>
                                    <a href="javascript:void(0);" style="display: none;" id="connect"  class="conee-btn btn connection{!!$arr_public_user_data['id']!!}" title="Click to connect" onclick="addConnectionUpper({{$arr_public_user_data['id']}})"><i class="icon-Add"></i> Connect</a>                            
                                    @elseif($is_connect[0]['status'] == '1' && $is_connect[0]['status'] != '3')                            
                                    <a href="javascript:void(0)" style="display: none;" class="conee-btn btn"  id="status" value="1" title="Click to connect"  onclick="profileConfirm({{$session_data['id']}},{{$arr_public_user_data['id']}})">Confirm </a>
                                    <a href="javascript:void(0)" style="display: none;" class="conee-btn btn"  id="request"  title="Click to cancel request"  onclick="cancleRequest({{$session_data['id']}},{{$arr_public_user_data['id']}})">Requested </a>
                                    <a href="javascript:void(0)"   class="conee-btn btn" id="disconnect" title="Click to disconnect"  onclick="DisConnected({{$session_data['id']}},{{$arr_public_user_data['id']}})">Connected </a>
                                    <a href="javascript:void(0);" style="display: none;" id="connect"  class="conee-btn btn connection{!!$arr_public_user_data['id']!!}" title="Click to connect" onclick="addConnectionUpper({{$arr_public_user_data['id']}})"><i class="icon-Add"></i> Connect</a>
                                    @elseif($is_connect[0]['status'] == '3' && $is_connect[0]['block_by'] == $session_data['id'])
                                    <a href="javascript:void(0)"  style="display: none;" class="conee-btn btn" id="disconnect" title="Click to disconnect"  onclick="DisConnected({{$session_data['id']}},{{$arr_public_user_data['id']}})">Connected </a>
                                    <span class="conee-btn btn"><i class="icon-Cancel"></i> Blocked</span>
                                    @elseif(isset($is_connect[0]['status'])  && $is_connect[0]['status'] != '3')
                                    <a href="javascript:void(0)"   class="conee-btn btn"  id="status" value="1" title="Click to connect"  onclick="profileConfirm({{$arr_public_user_data['id']}},{{$session_data['id']}})">Confirm </a>
                                    <a href="javascript:void(0)" style="display: none;"  class="conee-btn btn"  id="request" value="1" title="Click to cancel request"  onclick="cancleRequest({{$session_data['id']}},{{$arr_public_user_data['id']}})">Requested </a>
                                    <a href="javascript:void(0)"  style="display: none;" class="conee-btn btn" id="disconnect" title="Click to disconnect"  onclick="DisConnected({{$session_data['id']}},{{$arr_public_user_data['id']}})">Connected </a>
                                    <a href="javascript:void(0);" style="display: none;" id="connect"  class="conee-btn btn connection{!!$arr_public_user_data['id']!!}" title="Click to connect" onclick="addConnectionUpper({{$arr_public_user_data['id']}})"><i class="icon-Add"></i> Connect</a>
                                    @endif
                            @else
                            
                            <a href="javascript:void(0)" style="display: none;" class="conee-btn btn"  id="status" value="1" title="Click to connect"  onclick="profileConfirm({{$session_data['id']}},{{$arr_public_user_data['id']}})">Confirm </a>
                            <a href="javascript:void(0)" style="display: none;" class="conee-btn btn"  id="request" value="1" title="Click to cancel request"  onclick="cancleRequest({{$session_data['id']}},{{$arr_public_user_data['id']}})">Requested </a>
                            <a href="javascript:void(0)"  style="display: none;" class="conee-btn btn" id="disconnect" title="Click to disconnect"  onclick="DisConnected({{$session_data['id']}},{{$arr_public_user_data['id']}})">Connected </a>
                                @if ($session_data != "") 
                                    <a href="javascript:void(0);"  id="connect"  class="rahul conee-btn btn connection{!!$arr_public_user_data['id']!!}" title="Click to connect" onclick="addConnectionUpper({{$arr_public_user_data['id']}})"><i class="icon-Add"></i> Connect</a>
                                @else
                                    <a  href="javascript:void(0);" class="conee-btn btn" data-toggle="modal" data-target="#myModal"><i class="icon-Add"></i> Connect</a>
                                @endif    
                            @endif
                            @endif                           
                            
                            @if($session_data['id'] != $arr_public_user_data['id'])                               
                            
                            @if(((!isset($is_connect[0]['status'])) || (isset($is_connect[0]['status']) && $is_connect[0]['status'] != '3')) || (isset($is_connect[0]['block_by']) && $is_connect[0]['block_by'] == $session_data['id']))                            
                            @if(count($is_follower) == 0 ) 
                                @if ($session_data != "")
                                    <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_public_user_data['id']!!}" title="click to follow" onclick="addFollower({{$arr_public_user_data['id']}})"><i class="icon-Add"></i> Follow</a>
                                @else
                                    <a  href="javascript:void(0);" class="folll-btn btn" data-toggle="modal" data-target="#myModal"><i class="icon-Add"></i> Follow</a>
                                @endif
                            @else
                            <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_public_user_data['id']!!}" title="click to unfollow" onclick="addFollower({{$arr_public_user_data['id']}})"><i class="icon-Follow"></i> Unfollow</a>
                            @endif
                            @endif
                            @if((!isset($is_connect[0]['status'])) || (isset($is_connect[0]['status'])  && $is_connect[0]['status'] != '3'))
                                @if ($session_data != "")
                                    <button type="button" data-toggle="modal" data-target="#compose-message-modal-bd" class="masss-btn btn"><i class="icon-Message"></i> Message</button>
                                @else
                                    <button type="button" class="masss-btn btn" data-toggle="modal" data-target="#myModal"><i class="icon-Message"></i> Message</button>
                                @endif    
                            @endif
                            
                            @endif
                            @endif

                        </div>
                    </div>
                </div>
                <div class="modal fade"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog"  role="document">
                        <div class="modal-content report-pop">
                            <form name="report" id="report">
                                <div class="modal-body">
                                    <div class="report-infos-cont">
                                        <h1>Report</h1>
                                        <ul>
                                            What would you like to do?
                                            <li>

                                                <input type="radio" name="report" id="get_report" value="0"> 
                                                <label>Fake Profile</label>
                                            </li>
                                            <li>
                                                <input type="radio" name="report" id="get_report" value="1">
                                                <label>Hatred OR Vulgarity</label>
                                            </li>
                                            <li>
                                                <input type="radio" name="report" id="get_report" value="2">
                                                <label>Hacked Profile</label>
                                            </li>

                                            <label class="error" for="report"></label>
                                        </ul>

                                    </div>
                                </div>
                                <div class="boter-texter-feat modal-footer">
                                    <div class="pull-right">
                                        <button type="button" id="report_ok" class="btn" onclick="profileReport('{{$arr_public_user_data['id']}}')" >Post</button>
                                        <button type="submit" class="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade"  id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog"  role="document">
                        <div class="modal-content report-pop">
                            <form name="group_report" id="group_report">
                                @if(isset($is_connect[0]->status) && $is_connect[0]->status != '3' )
                                <div class="modal-body">
                                    <div class="report-infos-cont">
                                        <h1>Block</h1>

                                        <ul>
                                            Are you sure you want to block this user?

                                        </ul>
                                    </div>

                                </div>
                                <div class="boter-texter-feat modal-footer">
                                    <div class="pull-right">
                                        <button type="button" id="group_report_ok" class="btn" onclick="block('{{$arr_public_user_data['id']}}', '3')" >Block</button>
                                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                @else
                                <div class="modal-body">
                                    <div class="report-infos-cont">
                                        <h1>Unblock</h1>

                                        <ul>
                                            Are you sure you want to unblock this user?

                                        </ul>
                                    </div>

                                </div>
                                <div class="boter-texter-feat modal-footer">
                                    <div class="pull-right">
                                        <button type="button" id="group_report_ok" class="btn" onclick="block('{{$arr_public_user_data['id']}}', '0')" >Unblock</button>
                                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="btin-aab">

                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-profile-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 user-with-content" id="top-bio" style="display:none;">
                
            </div>
             
        </div>
           <div class="row">
            <div class="col-sm-8">
                @if(isset($arr_public_user_data['about_me']) && $arr_public_user_data['about_me']!='')
                <div class="user-with-content">
                    <h2>About {!!$arr_public_user_data['first_name'].' '.$arr_public_user_data['last_name']!!}</h2>
                    <div class="inner-imfo-tion">
                        {!!isset($arr_public_user_data['about_me']) && $arr_public_user_data['about_me']!='' ?$arr_public_user_data['about_me'] :'---' !!}
                    </div>
                </div>
                @endif
                <div class="user-with-content">
                    <h2>Member of groups <a href="javascript:void(0)" class="pull-right"><!--<i class="fa fa-chevron-right"></i>--></a></h2>
                    <div class="inner-imfo-tion user-hor-li">
                        <ul class="clearfix">
                            @foreach ($arr_user_group as $user_group)
                            <a href="/go/{{$user_group['group_name']}}">
                            <li>{!!isset($user_group['group_name']) ?$user_group['group_name'] :'---' !!}</li>
                            </a>
                            @endforeach 
                        </ul>
                    </div>
                </div>

                <!-- Twitter and Facebook section on company account pages -->
                @if($arr_public_user_data['user_type'] == 4)
                <div class="row">
                    @if($arr_public_user_data['twitter_page'] != 'https://twitter.com/' && $arr_public_user_data['twitter_page'] != '')
                        <?php
                        preg_match("/[^\/]+$/", $arr_public_user_data['twitter_page'], $matches);
                        $twitter_username = $matches[0];
                        ?>
                        <div class="col-sm-6">
                            <a class="twitter-timeline" href="{!! $arr_public_user_data['twitter_page'] !!}" data-width="380"
                            data-height="400">
                                Tweets by {{$twitter_username}}
                            </a>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    @endif
                    @if($arr_public_user_data['facebook_page'] != '' && $arr_public_user_data['facebook_page'] != 'https://facebook.com/')
                        <div class="col-sm-6">
                        <iframe src="https://www.facebook.com/plugins/page.php?href={!! urlencode($arr_public_user_data['facebook_page']) !!}&tabs=timeline&height=400&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div>
                    @endif
                </div>
                @endif

                <!-- User Activity Section --> 
                @if(count($arr_activities) > 0)
                <div class="user-with-content ">
                    <h2>{!! $arr_public_user_data['first_name'] !!}'s Activity <a href="javascript:void(0)" class="pull-right"><i class="fa fa-chevron-right"></i></a></h2>
                    <div class="inner-imfo-tion oneline-user-li mCustomScrollbar min-hieght-opt">
                        <ul id="user_activity">
                            @if(count($arr_activities) > 0)
                                @foreach($arr_activities as $ua)
                                    <li>
                                        <div class="media main-user-acti">
                                            <div class="media-left">
                                                <i class="icon-Post"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"> 
                                                    <span>
                                                        @if(strlen($ua->activity_title) < 50)
                                                        {{ ucfirst($ua->user['first_name']) }} {{ ucfirst($ua->user['last_name']) }} @if($ex=explode('-',$ua->activity_title))
                                                        {!! ($ex[0]) !!}- <a class="actlink" href="{{url('/')}}{{$ua->activity_link}}"> {!! ucwords($ex[1]) !!}</a>
                                                        @endif
                                                        @else           
                                                        @if($ex=explode('-',$ua->activity_title))                                             
                                                        {{ ucfirst($ua->user['first_name']) }} {{ ucfirst($ua->user['last_name']) }} {!! ($ex[0]) !!}- <a class="actlink" href="{{url('/')}}{{$ua->activity_link}}">{{substr(strip_tags($ex[1]), 0, 10)}}<br>{{substr(strip_tags($ex[1]), 10,strlen($ex[1]))}}
                                                        @endif
                                                        </a>
                                                        @endif
                                                    </span>
                                                </h4>
                                              {{--  checkout was here  --}}
                                                <div class="posting-day" style="display:block">{!! date('M d, Y', strtotime($ua['created_at'])) !!}</div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                            
                        </ul>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <button class="btn btn-default btn-cons hide" id="activityloadbtn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> loading"></button>
                                <div class="ajax-loading"></div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div>

                @endif
                @if ($session_data != "")

                <div class="photos-followers">
                    <ul class="clearfix">
                        @if(count($get_user_profile_picture) > 0)
                        <li>
                            <div class="user-with-content">
                                <h2>Photos<a href="javascript:void(0)" class="pull-right"><i class="fa fa-chevron-right"></i></a></h2>
                                <div class="inner-imfo-tion photos-info-img cust-scol-div mCustomScrollbar">
                                    @if(count($get_user_profile_picture) > 0)
                                    <ul class="clearfix">
                                        <div class="dash-mid-cont gallery-details share-gallery">
                                            <div class="gallery-inn">
                                                <div id="grid-gallery" class="grid-gallery">
                                                    <section class="grid-wrap">
                                                        <ul class="grid gallry">
                                                            @foreach ($get_user_profile_picture as $image)
                                                            <?php
                                                            $user_profile_image = config('profile_path') . $arr_public_user_data['id'] . '/thumb/' . $image->profile_picture;
                                                            if (Storage::exists($user_profile_image) && $image->profile_picture != '') {
                                                                $flag = 1;
                                                                $profile_picture = config('profile_pic_url') . $arr_public_user_data['id'] . '/thumb/' . $image->profile_picture;
                                                                ?>
                                                            <li>
                                                                <figure>
                                                                    <img src="{{$profile_picture}}" alt="No Image Found"/>
                                                                </figure>
                                                            </li>
                                                                 <?php
                                                            }
                                                            ?>
                                                            @endforeach

                                                        </ul>
                                                    </section>
                                                    <section class="slideshow">
                                                        <ul>

                                                            @foreach ($get_user_profile_picture as $image)
                                                             <?php
                                                            $user_profile_image = config('profile_path'). $arr_public_user_data['id'] . '/' . $image->profile_picture;
                                                            if (Storage::exists($user_profile_image) && $image->profile_picture != '') {
                                                                $flag == 1;
                                                                $profile_picture = config('profile_pic_url'). $arr_public_user_data['id'] . '/' . $image->profile_picture;
                                                                ?>
                                                            <li>
                                                                <figure>
                                                                    <img src="{{$profile_picture}}" alt="No Image Found"/>
                                                                </figure>
                                                            </li>
                                                             <?php } ?>
                                                            @endforeach
                                                        </ul>
                                                        <nav>
                                                            <span class="nav-prev"><i class="fa fa-angle-left arrow-back-color" ></i></span>
                                                            <span class="nav-next"><i class="fa fa-angle-right arrow-back-color" ></i></span>
                                                            <span class="nav-close"><i class="fa fa-close"></i></span>
                                                        </nav>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>

                                    </ul>
                                    @else
                                   No Record Found!
                                   @endif

                                </div>
                            </div>
                        </li>
                        @endif
                        @if ( (count($arr_user_connection)>0) || (count($arr_follower)>0) || (count($arr_following)>0) )
                        <li>
                            <div class="foll-conec-folow-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @if (count($arr_user_connection)>0)
                                    <li role="presentation" class="active"><a href="#connections" aria-controls="connections" role="tab" data-toggle="tab">connections</a></li>
                                    @endif
                                    @if (count($arr_follower)>0)
                                    <li role="presentation"><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab">followers</a></li>
                                    @endif
                                    @if (count($arr_following)>0)
                                    <li role="presentation"><a href="#following" aria-controls="following" role="tab" data-toggle="tab">following</a></li>
                                    @endif
                                </ul>

                                <div class="tab-content">
                                    @if (count($arr_user_connection)>0)
                                    <div role="tabpanel" class="tab-pane active" id="connections">

                                        <div class="conn-mediass">
                                            @if (count($arr_user_connection)>0)
                                            @foreach ($arr_user_connection as $user_connection)
                                            @if($arr_user_data['id']==$user_connection['from_user_id'] || $arr_user_data['id']==$user_connection['to_user_id'] )                                            

                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <?php
                                                    if ($user_connection['from_user_id'] != $arr_public_user_data['id']) {
                                                        $user_id = $user_connection['from_user_id'];
                                                        $url = config('profile_pic_url') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                    } else if ($user_connection['to_user_id'] != $arr_public_user_data['id']) {
                                                        $user_id = $user_connection['to_user_id'];
                                                        $url = config('profile_pic_url') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                    }
                                                    ?>
                                                    <div class="floo-users"><a 
                                                        <?php
                                                        if ($user_connection['from_user_id'] != $arr_public_user_data['id']) {
                                                            ?>
                                                                href="{{url('/')}}/user-profile/{{base64_encode($user_connection['from_user_id'])}}"
                                                                <?php
                                                            } else if ($user_connection['to_user_id'] != $arr_public_user_data['id']) {
                                                                ?>
                                                                href="{{url('/')}}/user-profile/{{base64_encode($user_connection['to_user_id'])}}"
                                                                <?php
                                                            }
                                                            ?>
                                                            target="_blank" ><img onerror="src={{config('avatar_male')}}" src="<?php echo $url; ?>" alt=""/></a></div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> 
							@if(($user_connection['from_user_id'] != $arr_public_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
							{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} 
							@elseif((($user_connection['to_user_id'] == $arr_public_user_data['id']) || ($user_connection['from_user_id'] == $arr_public_user_data['id'])) && $user_connection['status'] == '1')  
							@if($user_connection['to_first_name'] == $arr_public_user_data['first_name'] && $user_connection['to_last_name'] == $arr_public_user_data['last_name']) {!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} 
							@else {!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} 
							@endif  
							@else {!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}
							@endif  
						    </h4>
                                                </div>
	                                    </div> 
                                            @elseif($user_connection['status']==1)
                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <?php $url = ''; ?>
                                                    <?php
                                                    if ($user_connection['from_user_id'] != $arr_public_user_data['id']) {
                                                        $user_id = $user_connection['from_user_id'];
                                                        $url = config('profile_pic_url') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                    } else if ($user_connection['to_user_id'] != $arr_public_user_data['id']) {
                                                        $user_id = $user_connection['to_user_id'];
                                                        $url = config('profile_pic_url') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                    }
                                                    ?>
                                                    <div class="floo-users"><a 
                                                        <?php
                                                        if ($user_connection['from_user_id'] != $arr_public_user_data['id']) {
                                                            ?>
                                                                href="{{url('/')}}/user-profile/{{base64_encode($user_connection['from_user_id'])}}"
                                                                <?php
                                                            } else if ($user_connection['to_user_id'] != $arr_public_user_data['id']) {
                                                                ?>
                                                                href="{{url('/')}}/user-profile/{{base64_encode($user_connection['to_user_id'])}}"
                                                                <?php
                                                            }
                                                            ?>
                                                            target="_blank" ><img onerror="src={{config('avatar_male')}}" src="<?php echo $url; ?>" alt=""/></a></div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading ">
 							@if(($user_connection['from_user_id'] != $arr_public_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                                        @elseif((($user_connection['to_user_id'] == $arr_public_user_data['id']) || ($user_connection['from_user_id'] == $arr_public_user_data['id'])) && $user_connection['status'] == '1')  
                                                        @if($user_connection['to_first_name'] == $arr_public_user_data['first_name'] && $user_connection['to_last_name'] == $arr_public_user_data['last_name'])
							<a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                                        @else
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} </a>
                                                        @endif 
                                                        @else 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}</a>
                                                         @endif</h4>
                                                </div>
                                	    </div>
                                            @endif 
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                No Record Found!
                                            </div> 
                                            @endif
                                        </div>

                                    </div>
                                    @endif
                                    @if (count($arr_follower)>0)
                                    <div role="tabpanel" class="tab-pane" id="followers">
                                        <div class="conn-mediass">
                                            @if (count($arr_follower)>0)
                                            @foreach ($arr_follower as $follower_user)
                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <div class="floo-users"><a href="{{url('/')}}/user-profile/{{base64_encode($follower_user['follower_id_fk'])}}" target="_blank" ><img  src="{{ ($follower_user['profile_picture']!='') ? config('profile_pic_url').$follower_user["follower_id_fk"].'/'.$follower_user['profile_picture'] : '' }}" @if($follower_user['gender'] == '0') onerror="src={{ config('avatar_male') }}" @else onerror="src={{config('avatar_female')}}" @endif alt=""/></a></div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                    <!--<h4 class="media-heading">  {!!$follower_user['first_name'].' '.$follower_user['last_name']!!}<a href="javascript:void(0)" class="pull-right">Following</a></h4>-->
                                                    <a href="{{url('/')}}/user-profile/{{base64_encode($follower_user['follower_id_fk'])}}" class="med-user-click">{!!$follower_user['first_name'].' '.$follower_user['last_name']!!}
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div> 
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                No Record Found!
                                            </div> 
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if (count($arr_following)>0)
                                    <div role="tabpanel" class="tab-pane" id="following">
                                        <div class="conn-mediass">
                                            @if (count($arr_following)>0)
                                            @foreach ($arr_following as $following_user)
                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <div class="floo-users"><a href="{{url('/')}}/user-profile/{{base64_encode($following_user['user_id_fk'])}}" target="_blank" ><img src="{{ ($following_user['profile_picture']!='') ? config('profile_pic_url').$following_user["user_id_fk"].'/'.$following_user['profile_picture'] :'' }}" @if($following_user['gender'] == '0') onerror="src={{ config('avatar_male') }};" @else onerror="src={{config('avatar_female')}};" @endif  alt=""/></a></div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($following_user['user_id_fk'])}}" class="med-user-click">{!!$following_user['first_name'].' '.$following_user['last_name']!!}
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div> 
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                No Record Found!
                                            </div> 
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
                @endif
            </div>
            <div class="col-sm-4">
                <div class="user-with-content" id="bottom-bio">
                    <h2>BIO @if($session_data['id'] == $arr_public_user_data['id']) <a href="{{url('user-bio')}}" class="pull-right" title="view"><i class="fa fa-eye"></i></a>@endif</h2>
                    <div class="inner-imfo-tion bio-leb-profile">
                        <form class="form-horizontal">
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Name :</label>
                                <div class="col-sm-7">{!!$arr_public_user_data['first_name'].' '.$arr_public_user_data['last_name']!!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Location :</label>
                                <div class="col-sm-7">{!!( isset($arr_public_user_data['location']) && $arr_public_user_data['location']!='' ) ?$arr_public_user_data['location'] :'---' !!}</div>
                            </div>
<!--                             <div class="clearfix">
                                <label for="" class="col-sm-5">Height :</label>
                                <div class="col-sm-7">{!! ( isset($arr_public_user_data['height'] ) && $arr_public_user_data['height']!='') ? $arr_public_user_data['height'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Weight :</label>
                                <div class="col-sm-7">{!!( isset($arr_public_user_data['weight'] ) && $arr_public_user_data['weight']!='')? $arr_public_user_data['weight'] :'---' !!}</div>
                            </div> 
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Body type :</label>
                                <div class="col-sm-7">{!! isset($arr_public_user_data['body_type'] ) ? $arr_public_user_data['body_type'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Hair :</label>
                                <div class="col-sm-7">{!! isset($arr_public_user_data['hair_color'] ) ? $arr_public_user_data['hair_color'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Eyes :</label>
                                <div class="col-sm-7">{!! isset($arr_public_user_data['eye_color'] ) ? $arr_public_user_data['eye_color'] :'---' !!}</div>
                            </div>
                            
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Ethnicity :</label>
                                <div class="col-sm-7">{!! isset($arr_public_user_data['ethnicity_type'] ) ? $arr_public_user_data['ethnicity_type'] :'---' !!}</div>
                            </div>-->
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Profession :</label>
                                <div class="col-sm-7">{!!(isset($arr_public_user_data['profession'] ) && $arr_public_user_data['profession']!='')  ? $arr_public_user_data['profession'] :'---' !!}</div>
                            </div>
        
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Account Type :</label>
                                <div class="col-sm-7"> @if ($arr_public_user_data['user_type'] == '1') Student @elseif($arr_public_user_data['user_type'] == '4') Company @elseif($arr_public_user_data['user_type'] == '3') Professional @endif </div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Tag Line :</label>
                                <div class="col-sm-7">{!! isset($arr_public_user_data['tagline'] ) ? $arr_public_user_data['tagline'] :'---' !!}</div>
                            </div>
                        </form>  
                    </div>
                </div>
                <!--                <div class="go-to-connect">
                                    <h4>Get More Followers</h4>
                                    <div class="pros-user-design">
                                        <img  id="main_profile" onerror="src='{{ config('avatar_male') }}';" src="{{ count($arr_user_profile_picture) ? config('profile_pic_url').$arr_public_user_data["id"].'/'.$arr_public_user_data['profile_picture'] : config('avatar_male') }}" alt="profile"/>
                                        <div class="go-icons go-gpluse"><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></div>
                                        <div class="go-icons go-face"><a href="javascript:void(0)"><i class="fa fa-facebook-f"></i></a></div>
                                        <div class="go-icons go-tweet"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></div>
                                        <div class="go-icons go-insta"><a href="javascript:void(0)"><i class="fa fa-youtube"></i></a></div>
                                        <div class="go-icons go-liin"><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></div>
                                    </div>
                                </div>    -->
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="compose-message-modal-bd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content report-pop">
             <form id="compose-message-modal" name="compose-message-modal" method="post" action="" enctype="multipart/form-data">
               <div class="modal-body">
                
               
               <div class="report-infos-cont">
                   <h1>New message</h1></br>
                    <div style="padding-left:12px" class="text-feder-outer ims-dinvis-text">
                        <div class="form-group">
                            <input type="text" class="form-control" id="model_title" name="model_title" placeholder="Title" >
                        </div>
                        <div class="form-group">
                            <input type="text" readonly="" class="form-control" data-placeholder="Recipients" id="" name="" value="{{ $arr_public_user_data['first_name'].' '.$arr_public_user_data['last_name'] }}" >                            
                            <input type="hidden"  class="form-control" data-placeholder="Recipients" id="recipient_ids" name="recipient_ids[]" value="{{ $arr_public_user_data['id'] }}" >                            
                        </div>
                        <div class="fealider-area">
                            <div class="title-input">
                                <div id="message_content_modal" class="cust-text-area form-control" contenteditable="true"></div>
                                <textarea class="form-control" name="message_modal" id="message_modal" value="" style="display: none"></textarea>
                            </div>
<!--
                            
-->
                        </div>
                    </div>
                    </div>
                    </div>
                

            <div class="modal-footer">
               <div class="subsc-ims">
                                <span class="pull-left">
                                    <input type="file" title="Upload Attachment" class="hidden" id="attachment_modal" name="attachment_modal" onchange="readImageModal(this);">
                                    <a href="javascript:void(0)" onclick="openFileUploadModal(this)"><i class="fa fa-paperclip"></i></a>
                                    <span id="file_name_modal"></span>
                                </span>
                </div>
                <button type="button" id="compose_btn_modal" class="btn ims-send-btn" onclick="getDescriptionModal()">Send </button>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="{{url('/assets/Frontend/js/gallery/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/masonry.pkgd.min.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/classie.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/cbpGridGallery.js')}}"></script>
<script>
    $(document).ready(function() {
        var config = {
            '.chosen-select': {}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        /* landing Form Validation Start */
        jQuery("#compose-message-modal").validate({
            errorElement: 'div',
            debug: true,
            errorClass: 'text-danger',
            ignore: '.hidden',
            rules: {
                model_title: {
                    required: true
                },
                'recipient_ids[]': {
                    required: true
                },
                message_modal: {
                    required: true
                }
            },
            messages: {
                model_title: {
                    required: "Please enter title."
                },
                'recipient_ids[]': {
                    required: "Please select atleast one user to send message."
                },
                message_modal: {
                    required: "Please enter your message."
                }
            }, submitHandler: function(form) {
                jQuery("#btn_otp_submit").hide();
                jQuery("#loader").show();
                form.submit();
            }
        });
//        if (user_id != '' && typeof (user_id) != 'undefined' && message_id != '' && typeof (message_id) != 'undefined') {
//            getMessage(user_id, message_id);
//        }
    });
    $('#compose_btn_modal').on('click', function() {
        var form = document.getElementById('compose-message-modal');
        var formData = new FormData(form);
        if ($('#compose-message-modal').valid()) {
            $('#compose-message-modal-bd').modal('hide');
            $('#message_content_modal').html('');
            $('#file_name_modal').html('');
            $('#compose-message-modal')[0].reset();
            $.ajax({
                url: '{{url('/')}}/compose-message-modal',
                method: 'post',
                contentType: false,
                processData: false,
                dataType: 'json',
                data: formData,
                success: function(response) {
                    window.href = '{{url('/')}}/message';
                    window.location.reload()
                }
            });
        }
    })
    function getDescriptionModal() {
        var message_content_modal = $('#message_content_modal').html();
        $('#message_modal').val(message_content_modal);
    }
    function openFileUpload() {
        $("#attachment").click();
    }

    function readImage(input) {
        $('#file_name').html(input.files[0].name);
    }

    function openFileUploadModal() {
        $("#attachment_modal").click();
    }

    function readImageModal(input) {
        $('#file_name_modal').html(input.files[0].name);
    }
new CBPGridGallery(document.getElementById('grid-gallery'));
        $(".preventGallery").click(function(event) {
        event.stopPropagation();
});
function block(user_id, status) {
    if ($('#group_report').valid()) {
        $('#group_report_ok').prop('data-dismiss', 'modal');
        var base_url = jQuery("#base_url").val();
        $.ajax({
        url: base_url + '/block-user',
                dataType: 'json',
                method: 'get',
                data: {user_id: user_id, status: status},
                success: function(data) {
                if (data.msg == '1') {
                window.location.reload()
                }
                }
        })
    }
}
function profileReport(user_id) {
    if ($('#report').valid()) {
        $('#report_ok').prop('data-dismiss', 'modal');
        $.ajax({
        type: "get",
                url: '{!!url("/")!!}/user-report/' + btoa(user_id),
                data: $("#report").serialize(),
                dataType: "json",
                success: function(data) {
                if (data.msg == '1') {
                $("#report_disabled").prop('disabled', true)
                        window.location.reload()
                }
                }
        })
    }
}

function addConnection(to_user_id)
{
$.get('{!!url(' / ')!!}/add-connection', {to_user_id: to_user_id}, function (msg) {
if (msg == true)
{
$(".connection" + to_user_id).html('<i class="fa fa-check"></i> Connected');
} else{
$(".connection" + to_user_id).html('<i class="icon-Add"></i> Connect');
}

});
}
function addConnectionUpper(to_user_id)
{
$.get('{!!url("/")!!}/add-connection', {to_user_id: to_user_id}, function(msg) {
if (msg == true)
{
$('#request').css('display', ' inline');
        $('#connect').css('display', 'none');
}

});
}

function addFollower(follower_user_id)
{

$.ajax({
type: "get",
        url: '{!!url("/")!!}/make-follower',
        data: {follower_user_id: follower_user_id},
        dataType: "json",
        success: function(msg) {
            console.log(msg.follower_count);
        if (msg.success == 1)
        {
        $(".follow" + follower_user_id).html('<i class="fa fa-check"></i> Unfollow');
        } else {
        $(".follow" + follower_user_id).html('<i class="icon-Add"></i> Follow');
        }

        }
})
}

function removeFollower(follower_user_id)
{
console.log("Ok here");
$.ajax({
type: "get",
    url: '{!!url("/")!!}/remove-follower',
    data: {follower_user_id: follower_user_id},
    dataType: "json",
    success: function(msg) {
        console.log(msg.success);
        if (msg.success == 1)
        {
        $(".follow" + follower_user_id).html('<i class="fa fa-check"></i> Unfollow');
        } else {
        $(".follow" + follower_user_id).html('<i class="icon-Add"></i> Follow');
        }
    }
})
}

function getCoverPhoto()
{
document.getElementById('cover_photo').click();
}

function getProfilePicture()
{
document.getElementById('profile_picture').click();
}

function profileConfirm(from_id, user_id) {
var status = $("#status").attr("value");
        var base_url = jQuery("#base_url").val();
        $.ajax({
        url: base_url + '/confirm',
                dataType: 'json',
                method: 'get',
                data: {from_id: from_id, user_id: user_id, status: status},
                success: function(response) {

                if (response.msg == '1') {
                $('#disconnect').css('display', 'inline');
                        $('#status').css('display', 'none');
                }

                }
        });
}
function cancleRequest(from_id, user_id) {
var x = confirm("Are you sure you want to cancel request ?");
if (x) {
    var status = $("#status").attr("value");
        var base_url = jQuery("#base_url").val();
        $.ajax({
        url: base_url + '/deleted-connection',
                dataType: 'json',
                method: 'get',
                data: {from_id: from_id, user_id: user_id, status: status},
                success: function(response) {

                if (response.msg == '1') {
                $('#request').css('display', 'none');
                        $('#connect').css('display', 'inline');
                }

                }
        });
    }
}

function DisConnected(from_id, user_id) {
var x = confirm("Are you sure you want to disconnect ?");
if (x) {
    var base_url = jQuery("#base_url").val();
        $.ajax({
        url: base_url + '/deleted-connection',
            dataType: 'json',
            method: 'get',
            data: {from_id: from_id, user_id: user_id, status: status},
            success: function(response) {

            if (response.msg == '1') {
            $('#connect').css('display', 'inline');
                    $('#disconnect').css('display', 'none');
            }

            }
        });
    }
}

function connectedConfirm(from_id, user_id, status) {
var base_url = jQuery("#base_url").val();
    $.ajax({
    url: base_url + '/confirm',
        dataType: 'json',
        method: 'get',
        data: {from_id: from_id, user_id: user_id, status: status},
        success: function(response) {

        if (response.msg == '1') {
        $('#confirm').text('Connected');
        }

        }
    });
}

function uploadImage(value) {
var form = document.getElementById('coverform');
    var formData = new FormData(form);
    var base_url = jQuery("#base_url").val();
    $.ajax({
    url: base_url + '/cover-photo',
        contentType: false,
        processData: false,
        dataType: 'json',
        data: formData,
        method: 'post',
        success: function(response) {

        if (response.error == '0') {

        var url = "{{config('cover_image_url')}}{!!$arr_public_user_data["id"]!!}/" + response.cover_photo;
                $('.persnal-sec').css('backgroundImage', 'url(' + url + ')');
                return false;
        } else if (response.error == '1')
        {
        alert('error uploading the file!');
                return false;
        }
        }
    })
}

function uploadProfileImage(value) {
var form = document.getElementById('profilepicfrm');
    var formData = new FormData(form);
    var base_url = jQuery("#base_url").val();
    $.ajax({
    url: base_url + '/upload-profile-picture',
        contentType: false,
        processData: false,
        dataType: 'json',
        data: formData,
        method: 'post',
        success: function(response) {

        if (response.error == '0') {
        var url = "{{config('profile_pic_url')}}{!!$arr_public_user_data['id']!!}/" + response.profile_picture;
                var thumb_url = "{{config('profile_pic_url')}}{!!$arr_public_user_data['id']!!}/thumb/" + response.profile_picture;
                document.getElementById("main_profile").src = url;
                document.getElementById("thumb_profile").src = thumb_url;
                return false;
        } else if (response.error == '1')
        {
        alert('error uploading the file!');
                return false;
        }
        }
    })
}
</script>
<script>
    var page = 1; //track user scroll as page number, right now page number is 1
    //load_more(page); //initial content load
    var ajaxInProgress = false;
    $(window).scroll(function() { //detect page scroll
        //if($(window).scrollTop() + $(window).height() >= $(document).height()) { 
        if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
        //if user scrolled from top to bottom of the page
            if (!ajaxInProgress){
                page++; //page number increment
                load_more(page); //load content   
            }    
        }
    });     
    function load_more(page){
        if($('.ajax-loading').html() != 'No more records!')
        {
            $.ajax(
            {
                url: '{{url("/")}}/user-activity-load/{{$arr_public_user_data["id"]}}?page=' + page,
                type: "get",
                datatype: "html",
                beforeSend: function()
                {
                    $('#activityloadbtn').removeClass('hide');
                    $('#activityloadbtn').button('loading');
                    ajaxInProgress = true;
                }
            })
            .done(function(data)
            {
                console.log(data.length);
                ajaxInProgress = false;
                $('#activityloadbtn').button('reset');
                $('#activityloadbtn').addClass('hide');
                if(data.length == 0){
                    //notify user if nothing to load
                    $('.ajax-loading').html("No more records!");
                    $('.ajax-loading').addClass("hide");
                    return;
                }
                
                //$('.ajax-loading').hide(); //hide loading animation once data is received
                $("#user_activity").append(data); //append data into #results element          
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                console.log('No response from server');
            });
        }    
    }
</script>
<script>
$(document).ready(function(){
    var listLength = $('div.foll-conec-folow-tab>ul>li').length;
    if(listLength == 3){
        $('div.foll-conec-folow-tab>ul>li').css("width","33.333%");
        $('div.foll-conec-folow-tab>ul li:first').addClass("active");
        $('div.tab-content div:first').addClass("active");
    }
    else if(listLength == 2){
        $('div.foll-conec-folow-tab>ul>li').css("width","50%");
        $('div.foll-conec-folow-tab>ul li:first').addClass("active");
        $('div.tab-content div:first').addClass("active");
    }
    else if(listLength == 1){
        $('div.foll-conec-folow-tab>ul>li').css("width","100%");
        $('div.foll-conec-folow-tab>ul li:first').addClass("active");
        $('div.tab-content div:first').addClass("active");
    }
});
</script>
@include('Frontend.includes.user.user_profile_fx')
@include('Frontend.includes.footer')
