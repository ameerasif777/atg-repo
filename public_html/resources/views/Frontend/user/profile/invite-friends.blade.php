
@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>
<script src="https://connect.facebook.net/en_US/all.js"></script>

<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
                    <h6>Invite Friends</h6>
                    @if($arr_user_data['user_name'] != '')
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active clearfix">
                        <div class="main-inner-tab-info clearfix">
                            <div class="go-to-connect">                   
                                <p> Your personal referral URL is </p>
                                <h4> atg.world/?r={{$arr_user_data['user_name']}}</h4>
                                <p> Any one you refer using this link (aka URL) will get connected to you.</p>
                                <div class="pros-user-design">
                                    <?php
                                    $user_profile_image = config('profile_path'). $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                                    if (Storage::exists($user_profile_image) && $arr_user_data['profile_picture'] != '') {
                                        $profile_picture =config('profile_pic_url'). $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                                    } else {
                                        if ($arr_user_data['gender'] == 0) {
                                            $profile_picture = config('avatar_male');
                                        } else {
                                            $profile_picture = config('avatar_female');
                                        }
                                    }
                                    ?>
                                    <img  id="main_profile" src="{{$profile_picture}}"/>
                                    <div class="go-icons go-gpluse go-whatsapp"><a href="javascript:void(0);" onclick="callGooglePlus();"><i class="fa fa-google-plus"></i></a></div>
                                    <div class="go-icons go-face"><a href="javascript:void(0);" onclick="callFbShare();"><i class="fa fa-facebook-f"></i></a></div>
                                    <div class="go-icons go-tweet"><a href="javascript:void(0);" onclick="callTwitter();"><i class="fa fa-twitter"></i></a></div>
                                    <div class="go-icons go-liin"><a href="javascript:void(0);" onclick="callLiknedin();"><i class="fa fa-linkedin"></i></a></div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">&nbsp;</div>
                    </div>
                    @if(count($arr_user_connection)>0)
                    <div class="bhoechie-tab-content tem-capter clearfix">&nbsp;</div>
                    <div class="bhoechie-tab-content">&nbsp;</div>                    
                    <div class="clearfix">&nbsp;</div>
                    <h6>My Referred Connections:  {{count($arr_user_connection)}}</h6>
                    <div class="bhoechie-tab-content active clearfix">
                        <div class="foll-conec-folow-tab">
                            <div class="conn-mediass mCustomScrollbar">
                                @foreach ($arr_user_connection as $user_connection)
                                @if($user_connection['status'] != '3')
                                @if($arr_user_data['id']==$user_connection['from_user_id'] || $arr_user_data['id']==$user_connection['to_user_id'] )                                            
                                <div class="media conne-medias">
                                    <div class="media-left">
                                        <?php
                                        if ($user_connection['from_user_id'] != $arr_user_data['id']) {
                                            $user_id = $user_connection['from_user_id'];
                                            $gender = $user_connection['from_gender'];
                                            $user_profile_image = config('profile_path'). $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                            if (Storage::exists($user_profile_image) && $user_connection['from_profile_picture'] != '') {
                                                $profile_picture = config('profile_pic_url'). $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                            } else {
                                                if ($gender == 0) {
                                                    $profile_picture = config('avatar_male');
                                                } else {
                                                    $profile_picture =config('avatar_female');
                                                }
                                            }
                                        } else if ($user_connection['to_user_id'] != $arr_user_data['id']) {
                                            $user_id = $user_connection['to_user_id'];
                                            $gender = $user_connection['to_gender'];
                                            $url = url('/public/assets/Frontend/user/profile_pics/' . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture']);
                                            $user_profile_image = config('profile_path'). $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                            if (Storage::exists($user_profile_image) && $user_connection['to_profile_picture'] != '') {
                                                $profile_picture = config('profile_pic_url') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                            } else {
                                                if ($gender == 0) {
                                                    $profile_picture = config('avatar_male');
                                                } else {
                                                    $profile_picture = config('avatar_female');
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="floo-users">
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" target="_blank" >
                                                <img src="{{$profile_picture}}" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"> 
                                            @if(($user_connection['from_user_id'] != $arr_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} </a>
                                            <a href="javascript:void(0)" class="pull-right" id="confirm"  onclick="confirm({{$user_connection['from_user_id']}},{{$arr_user_data['id']}}, 1)">Confirm </a>
                                            @elseif((($user_connection['to_user_id'] == $arr_user_data['id']) || ($user_connection['from_user_id'] == $arr_user_data['id'])) && $user_connection['status'] == '1') 
                                            @if($user_connection['to_first_name'] == $arr_user_data['first_name'] && $user_connection['to_last_name'] == $arr_user_data['last_name']) 
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} </a>
                                            @else 
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} </a>
                                            @endif
                                            @else
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}</a>
                                            @endif  </h4>
                                    </div>
                                </div> 
                                @elseif($user_connection['status']==1)
                                <div class="media conne-medias">
                                    <div class="media-left">
                                        <?php
                                        if ($user_connection['from_user_id'] != $arr_user_data['id']) {
                                            $user_id = $user_connection['from_user_id'];
                                            $gender = $user_connection['from_gender'];
                                            $user_profile_image = config('profile_path'). $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                            if (Storage::exists($user_profile_image) && $user_connection['from_profile_picture'] != '') {
                                                $profile_picture = config('profile_pic_url'). $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                            } else {
                                                if ($gender == 0) {
                                                    $profile_picture = config('avatar_male');
                                                } else {
                                                    $profile_picture = config('avatar_female');
                                                }
                                            }
                                        } else if ($user_connection['to_user_id'] != $arr_user_data['id']) {
                                            $user_id = $user_connection['to_user_id'];
                                            $gender = $user_connection['to_gender'];
                                            $user_profile_image = config('profile_path') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                            if (Storage::exists($user_profile_image) && $user_connection['to_profile_picture'] != '') {
                                                $profile_picture =config('profile_pic_url'). $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                            } else {
                                                if ($gender == 0) {
                                                    $profile_picture = config('avatar_male');
                                                } else {
                                                    $profile_picture = config('avatar_female');
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="floo-users">
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" target="_blank" >                                                            
                                                <img src="{{$profile_picture}}"  alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading "> 
                                            @if(($user_connection['from_user_id'] != $arr_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                            <a href="javascript:void(0)" class="pull-right" id="confirm"  onclick="confirm({{$user_connection['from_user_id']}},{{$arr_user_data['id']}}, 1)">Confirm </a> 
                                            @elseif((($user_connection['to_user_id'] == $arr_user_data['id']) || ($user_connection['from_user_id'] == $arr_user_data['id'])) && $user_connection['status'] == '1')  
                                            @if($user_connection['to_first_name'] == $arr_user_data['first_name'] && $user_connection['to_last_name'] == $arr_user_data['last_name'])
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                            @else
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} </a>
                                            @endif 
                                            <a href="javascript:void(0)" class="pull-right" >Connected </a>
                                            @else 
                                            <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}</a>
                                            <a href="javascript:void(0)" class="pull-right" >Requested </a> @endif</h4>
                                    </div>
                                </div>
                                @endif 
                                @endif 
                                @endforeach                                    
                            </div>
                        </div>
                        </di
                        @endif
                        @if($arr_user_data['user_type'] != 4)
                        <div class="bhoechie-tab-content tem-capter clearfix"></div>
                        <div class="bhoechie-tab-content "></div>
                        @endif
                        @if($arr_user_data['user_type'] == 1)
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        @endif
                        <div class="bhoechie-tab-content">
                            <div class="main-inner-tab-info clearfix"></div>
                        </div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        <div class="bhoechie-tab-content"></div>
                        @else 
                        <div class="bhoechie-tab-content active clearfix">
                            <div class="main-inner-tab-info clearfix">
                                <form id="account_setting" name="account_setting" method="post">
                                    <input type="hidden" name="base_url" id="base_url" value="<?php echo url('/'); ?>">
                                    {!! csrf_field() !!}
                                    <div class="text-center">Select a username so that you can easily remember your referral code.</div>
                                    <div class="clearfix">&nbsp;</div>
                                    <dl class="dl-horizontal">
                                        <dt>Username<sup class="mandatory">*</sup> :</dt>
                                        <dd>
                                            <input type="text" id="new_user_name" name="new_user_name" value="{!!$arr_user_data['user_name']!!}" placeholder="Username" onkeyup="nospaces(this)">
                                            <span class="usr_msg1" style='text-align:center;color:red;font-size:12px;'></span>
                                        </dd>
                                        <input type="hidden" id="old_username" name="old_username" value="{!!$arr_user_data['user_name']!!}" >
                                    </dl>   
                                    <button class="save-btn btn" type="button" name="btn_account_setting" id="btn_account_setting" onclick="save_username();">SAVE</button>
                                    <div id='verifyUsernameMsg' style='text-align:left;color:red;font-size:12px;'></div>
                                </form>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script type="text/javascript">
    $(function () {
        $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo"
        });
        $("#location").bind("geocode:result", function (event, result) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': result.formatted_address}, function (results, status) {
                var location = results[0].geometry.location;
                var lat = location.lat();
                var lng = location.lng();
                $("input[name=latitude]").val(lat);
                $("input[name=longitude]").val(lng);
            });
        });
    });
    $(document).ready(function () {
        $("img").error(function () {
            $(this).attr("src", "{{url('/assets/Backend/img/image-not-found.png')}}");
        });
    });
    $(document).ready(function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('.go-gpluse').html('<a href="javascript:void(0);" onclick="callWhatsapp();"><i class="fa fa-whatsapp"></i></a>')
        } else {
            $('.go-gpluse').html('<a href="javascript:void(0);" onclick="callGooglePlus();"><i class="fa fa-google-plus"></i></a>')
        }
    });
    function nospaces(t) {
        if (t.value == '') {
            $('#btn_account_setting').removeAttr('disabled');
        } else if (t.value.match(/\s/g)) {
            t.value = t.value.replace(/\s/g, '');
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Username cannot contain space');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else if (/^[a-zA-Z0-9 _]*$/.test(t.value) == false) {
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Please do not enter special characters');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else if ($.trim(t.value).length < 4) {
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Please Enter more than 4 character');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else {
            $('.usr_msg1').html('');
            var base_url = jQuery("#base_url").val();
            var key = $('#new_user_name').val();
            $.ajax({
                url: base_url + '/verify_username',
                dataType: 'json',
                data: {'key': key, id: "{{ $arr_user_data['id'] }}"},
                method: 'post',
                success: function (response) {
                    if (response == '0') {
                        $('.usr_msg1').html('Good choice, but this username is already taken. Please try another.');
                        $('#btn_account_setting').attr('disabled', 'true');
                    } else if (response == '1') {
                        $('.usr_msg1').html('Username is unavailable. Please try another.');
                        $('#btn_account_setting').attr('disabled', 'true');
                    } else {
                        $('.usr_msg1').html('');
                        $('#btn_account_setting').removeAttr('disabled');
                    }
                }
            });
        }
    }

    function save_username() {
        var base_url = jQuery("#base_url").val();
        //alert(base_url);

        var u_val = $('#new_user_name').val();
        //alert(u_val);

        if (u_val == '') {
            alert('Please Enter the value.');
            return false;
        }

        if (/^[a-zA-Z0-9_ ]*$/.test(u_val) == false) {
            $('.usr_msg1').html('Please do not enter special characters.');
            return false;
        }
        if ($.trim(u_val).length < 4) {
            $('.usr_msg1').html('Please Enter more than 4 character.');
            return false;
        }
        if (u_val.match(/\s/g)) {
            $('.usr_msg1').html('Username cannot contain space.');
            return false;
        }

        $.ajax({
            url: base_url + '/add_new_username',
            dataType: 'json',
            data: {'u_val': u_val},
            method: 'post',
            success: function (response) {
                if (response.error == '0') {
                    $('#verifyUsernameMsg').html('Good choice, but this username is already taken. Please try another.');
                    return false;
                } else if (response.error == '1') {
                    $('#verifyUsernameMsg').html('Username is unavailable. Please try another.');
                    return false;
                } else {
                    $('#verifyUsernameMsg').css('color', 'green').html('Username has been added successfully. Please wait which page is reloading..');
                    setTimeout(function () {
                        window.location.href = window.location.href;
                    }, 4000);
                }
            }
        });
    }

    function callFbShare() {
        FB.init({
            appId: "{{ config('facebook.appID') }}",
            cookie: true,
            status: true,
            xfbml: true
        });
        var base_url = jQuery("#base_url").val();
        var shareUrl = base_url + "/?r={{ $arr_user_data['user_name'] }}";
        FB.ui({
            method: 'share',
            //link: shareUrl,
            href: shareUrl,
            name: "You are invited to join Across The Globe (ATG)",
            caption: "You are invited to join Across The Globe (ATG)",
            description: ""
        });
    }

    function callLiknedin() {
        var base_url = jQuery("#base_url").val();
        var shareUrl = base_url + "/?r={{ $arr_user_data['user_name'] }}";
        window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + shareUrl + '&title=You are invited to join Across The Globe (ATG)&summary=&source=ATG World', "_blank")
    }

    function callTwitter() {
        var base_url = jQuery("#base_url").val();
        var shareUrl = base_url + "/?r={{ $arr_user_data['user_name'] }}";
        window.open('http://twitter.com/share?text=You are invited to join Across The Globe (ATG)&url=' + shareUrl, "_blank")
    }

    function callGooglePlus() {
        var base_url = jQuery("#base_url").val();
        var shareUrl = base_url + "/?r={{ $arr_user_data['user_name'] }}";
        $('.grey-overlay-loader').hide();
        window.open('https://plus.google.com/share?url=' + shareUrl, "_blank")
    }

    function callWhatsapp() {
        var base_url = jQuery("#base_url").val();
        var shareUrl = base_url + "/?r={{ $arr_user_data['user_name'] }}";
        $('.grey-overlay-loader').hide();
        var whatsappUrl = "whatsapp://send?text=" + encodeURIComponent('You are invited to join Across The Globe (ATG)') + " - " + encodeURIComponent(shareUrl);
        window.open(whatsappUrl, "_blank")
    }

</script>

