@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>
<script>
        $(document).ready(function () {
$('.multipleSelect').popSelect({
showTitle: false,
        autoIncrease: true,
        placeholderText: 'Please select the members',
        position: 'bottom'
});

        $('.popover-select-wrapper').css('width','100%')
        })
</script>
<!--<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>Account Settings</h1>
    </div>
</section>-->
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>

                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                     @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content active tem-capter clearfix">
                        <form name="team" id="team" method="post" action="{{url('/')}}/create-team" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <h6>Create a new team/project</h6>
                            <div class="main-inner-tab-info my-team-tab clearfix">
                                <dl class="dl-horizontal">
                                    <dt>Name of Team :</dt>
                                    <dd><input type="text" class='form-control' name="team_name" id="team_name" placeholder="Team Name">
                                    <div for="team_name" class="text-danger"></div></dd>
                                    
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Project Name :</dt>
                                    <dd><input type="text" class='form-control' name="project_name" id="project_name" placeholder="Project Name">
                                    <div for="project_name" class="text-danger"></div></dd>
                                    
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Project Description :</dt>
                                    <dd><input type="text" class='form-control' name="project_description" id="project_description"  placeholder="Project Description">
                                    <div for="project_description" class="text-danger"></div></dd>
                                    
                                </dl>
                                <dl class="dl-horizontal">
                                    <div class='col-sm-4'>
                                        <div class="uploar-imger">

                                            <label for="">Upload Image</label>

                                            <div class="uping-imgs">
                                                <input type="file" name='profile_pic'  id="profile_pic" class="upit-img" />
                                            </div>
                                            <div class="title-input uplink-imgs">
                                                <img class="uploding-img-vie" style="display: none"  id="blah" src="#" alt="" width=100%" /><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                                            </div>
                                            
                                        </div>
                                        <div class="text-danger" for="profile_pic"></div>
                                    </div>

                                </dl>
                                
                            </div>
                            <div class="tema-members-outs">
                                <h4>Team Members</h4>
                                <div class="team-mem-inner">
                                    <div class="users-conectry-nav temb-slids">
                                        <select name="member[]" multiple=""  class="form-control multipleSelect">
                                            @foreach($arr_user_connection as $value)
                                            <option value="{{$value['id']}}">{{ucfirst($value['first_name']).'  '.ucfirst($value['last_name'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div for="member[]" class="text-danger"></div>
                                </div>
                            </div>
                            <button class="btn teme-save-btn" id="btn_create_team" name="btn_create_team" >Create Team</button>
                        </form>
                    </div>
                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">
                            
                        </div>
                    </div>
                     @endif
                     @if($arr_user_data['user_type'] == 1)
                     <div class="bhoechie-tab-content">
                        
                    </div>

                    <div class="bhoechie-tab-content">
                       
                    </div>
                     @endif
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                     
                     <div class="bhoechie-tab-content">
                        
                    </div>
                     <div class="bhoechie-tab-content">
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')
<script src="{{url('/assets/Frontend/js/team/team.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script>
                                                    
                                                    
                                                     $(document).ready(function () {
//                                            $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
//                                            e.preventDefault();
//                                                    $(this).siblings('a.active').removeClass("active");
//                                                    $(this).addClass("active");
//                                                    var index = $(this).index();
//                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
//                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
//                                            });
                                                    $("img").error(function () {
                                            $(this).attr("src", "{{url('/')}}/public/assets/Backend/img/image-not-found.png");
                                            });
                                            });
                                                                                                    
</script>
<script>
            function del(team_id) {
            if (confirm("Are you sure want to delete?")) {
            $.ajax({
            url: 'delete-team/' + btoa(team_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
             function rem(resume_id) {
            if (confirm("Are you sure want to remove this resume?")) {
            $.ajax({
            url: 'delete-resume/' + btoa(resume_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
              function remov(job_id) {
            if (confirm("Are you sure want to remove this saved job?")) {
            $.ajax({
            url: 'delete-saved-job/' + btoa(job_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }

    function readURL(input) {
//    var ext = input.value.match(/\.(.+)$/)[1];
//            switch (ext)
//    {
//    case 'jpg':
//            case 'bmp':
//            case 'png':
//            case 'tif':
//            if (input.files && input.files[0]) {
//                console.log(input.files)
//                console.log(input.files[0].height)
//    var reader = new FileReader();
//    console.log(reader)
//            reader.onload = function(e) {
//            $('#blah').css("display", "block");
//                    $('#blah').attr('src', e.target.result);
//                    $('#remove_image').css("display", "block");
//            }
//
//    reader.readAsDataURL(input.files[0]);
//    }
//    break;
//            default:
//            alert('Only Image allowed !');
//            input.value = '';
//    }

    }

    function removeimage() {
    $('#blah').css("display", "none");
            $('#remove_image').css("display", "none");
            $("#profile_pic").prop("value", "");
    }


</script> 
<script>
    
      function checkResumeCount(){
            $.ajax({
            url: 'resume-count',
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
               var resume_count = msg.resume_count[0].count
                if(resume_count == 3){
                   if (confirm("Are you sure want to remove first uploaded resume?")) {
                        $('#upload_resumes').submit();
                   }else{
                       return false;
                   }
                }
            })
        }
        
    $(document).ready(function () {
      
        $('#file').change(function () {
            var val = $(this).val();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf)$");
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please upload a file only of type pdf,doc,docx.');
            }
            var f = this.files[0]

            if (f.size > 10000000 || f.fileSize > 10000000)
            {
                alert("Allowed file size less than 10 MB.")
                this.value = null;
            }
            
            
            
        });
        var _URL = window.URL || window.webkitURL;
    var flag = true; 
        
        $("#profile_pic").change(function(e) {
           
        var file, img;
        var ext = this.files[0].name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert('Only JPG, PNG or GIF files are allowed');
            $("#profile_pic").val('');
        } else {
            
            if (file = this.files[0]) {
                img = new Image();
                img.onload = function() {
                    if (this.width >= 1900 && this.height >= 1200) {
                        var reader = new FileReader();
    console.log(reader)
            reader.onload = function(e) {
            $('#blah').css("display", "block");
                    $('#blah').attr('src', e.target.result);
                    $('#remove_image').css("display", "block");
            }

    reader.readAsDataURL(file);
                    } else {
                        //flag = false;
                        alert("Image height and width should be greater than 1900*1200");
                       // $('#blah').attr('src', './media/front/img/image-not-found.png');
                        $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                    }
                     
                };
                 img.src = _URL.createObjectURL(file);
                readURL(this);
              
            }
        }
    }); 
    

    });
</script> 





