@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<script type="text/javascript" src="{{url('/assets/Frontend/js/jquery.simplePagination.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{url('/assets/Frontend/css/simplePagination.css')}}"/>

<script>
    $(function() {
        var ctotal = $('#ctotal').val();
        var perPage = '10';
    $('#page-nav').pagination({
        items: ctotal,
        itemsOnPage: perPage,
        cssStyle: 'light-theme',
        onPageClick: function(pageNum) {
            getData(pageNum-1, perPage);
        }
    });
    
    getData('0',perPage);
});
     function getData(pageNum, perPage)
     {
        $.ajax({
        type: "get",
            url: '{!!url("/")!!}/trans-history-ajax/'+pageNum+'/'+perPage,
            success: function(data) {
                $('#trans_tb > tbody').empty();
                if(data != '')
                {
                    data = jQuery.parseJSON(data);
                    for(var i = 0, len = data.length; i < len; i++)
                    {
                        $('#trans_tb > tbody').append("<tr><td>"+data[i]['date']+"</td><td>"+data[i]['summary']+"</td><td align='center'>&#8377;"+data[i]['amount']+"</td><td align='center'><a href='"+data[i]['dwld_link']+"' title='download'><img  src={{ url('/public/assets/Frontend/img/download.png') }} width='25' height='25' ></a></td></tr>  ");
                    }
                }
                $('#page-nav').css('display', 'block');
            }
        }); 
     }
</script>

<!--<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>Account Settings</h1>
    </div>
</section>-->
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Transaction History</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    <input type="hidden" id="ctotal" value="{{$transaction_count}}"/>
                    <div class="bhoechie-tab-content active">
                        <div class="main-inner-tab-info clearfix">
                            <div class="table-responsive">  
                                <table id="trans_tb" class="table">
                                <thead>
                                <tr>
                                    <th width="25%">DATE</th>
                                    <th width="40%">TRANSACTION SUMMARY</th>
                                    <th>TRANSACTION AMOUNT</th>
                                    <th>DOWNLOAD RECEIPT</th>
                                </tr>   
                                </thead>
                                <tbody></tbody>
<!--                               {{-- @foreach($trans_data as $key => $value)
                                <tr>
                                    <td>{{$value['date']}}</td>
                                    <td>{{$value['summary']}}</td>
                                    <td align='center'>&#8377;{{$value['amount']}}</td>
                                    <td><a href="{{$value['dwld_link']}}" title="download"/></td>
                                </tr>   
                                @endforeach--}}-->
                            </table>
                                <div style="float: right; display: none;" id="page-nav"></div>
                            </div>
                        </div>
                    </div>
                     
                     
                </div>
                
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Frontend/js/setting/account-setting.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>








