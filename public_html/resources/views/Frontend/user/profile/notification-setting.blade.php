@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<!--<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>Account Settings</h1>
    </div>
</section>-->
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content  tem-capter clearfix">
                        
                    </div>
                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">
                            
                        </div>
                    </div>
                    @endif
                    @if($arr_user_data['user_type'] == 1)
                    <div class="bhoechie-tab-content">
                        <div class="tema-members-outs">
                            
                        </div>
                    </div>
                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">
                            
                        </div>
                    </div>
                    @endif
                    <div class="bhoechie-tab-content active">
                        <div class="main-inner-tab-info clearfix">
                            <form id="edit_notifitcation_setting" name="edit_notifitcation_setting" method="post" action='{{url('edit-notification-setting')}}'>
                                {!! csrf_field() !!}
                                <div>
                                    <dl class="dl-horizontal">
                                        <dt>Follower<sup class="mandatory"></sup> :</dt>
                                        <dd>
                                        @if(isset($arr_user_data-> getUserEmailSchedule->follower_notification))
                                        <label class="radio-inline"><input  value="1" type="radio" name="follower_notification"  @if ( $arr_user_data-> getUserEmailSchedule->follower_notification == 1  ) checked="checked" @endif>On</label>
                                        <label class="radio-inline"><input   value="0"   type="radio" name="follower_notification" @if($arr_user_data-> getUserEmailSchedule->follower_notification == 0)  checked="checked" @endif >Off</label>
                                        @else
                                        <label class="radio-inline"><input  value="1" type="radio" name="follower_notification">On</label>
                                        <label class="radio-inline"><input   value="0"   type="radio" name="follower_notification" checked="checked" >Off</label>
                                        

                                        @endif
                                        <div for="follower" class="error"></div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Connection<sup class="mandatory"></sup> :</dt>
                                        <dd>
                                        @if(isset($arr_user_data-> getUserEmailSchedule->connection_notification))
                                        <label class="radio-inline"><input  value="1" type="radio" name="connection_notification"  @if ( $arr_user_data-> getUserEmailSchedule->connection_notification == 1) checked="checked" @endif>On</label>
                                        <label class="radio-inline"><input   value="0"   type="radio" name="connection_notification" @if( $arr_user_data-> getUserEmailSchedule->connection_notification == 0)  checked="checked" @endif >Off</label>
                                        @else
                                        <label class="radio-inline"><input  value="1" type="radio" name="connection_notification">On</label>
                                        <label class="radio-inline"><input   value="0"   type="radio" name="connection_notification" checked="checked" >Off</label>
                                        
                                        @endif
                                        <div for="connection" class="error"></div>
                                        </dd>
                                    </dl>


                                    <div id="schedule_div" style="display: block;">
                                        <dl class="dl-horizontal">
                                            <dt>Digest Email Frequency:</dt>
                                            <dd>
                                            <select class='schedule_email form-control' name="schedule_email">
                                                <option value=''>--Select--</option>
                                                <option @if(count($arr_user_data->getUserEmailSchedule) == 0 || isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule == 0)) selected @endif value='0'>Daily</option>
                                                <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule == 1) selected @endif value='1'>Weekly</option>
                                                <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule == 2) selected @endif value='2'>Monthly</option>
                                                <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule == 3) selected @endif value='3'>Off</option>
                                            </select>
                                            <div for="connection" class="error"></div>
                                            </dd>
                                        </dl>
                                        <div id='weekly' @if(count($arr_user_data->getUserEmailSchedule) == 0 || isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule != 1)) style="display: none" @endif>
                                            <dl class="dl-horizontal">
                                                <dt>Day:</dt>
                                                <dd>
                                                <select class='form-control' name="schedule_day" id="schedule_day">
                                                    <option value=''>--Select--</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Mon') selected @endif value='Mon'>Monday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Tue') selected @endif value='Tue'>Tuesday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Wed') selected @endif value='Wed'>Wednesday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Thu') selected @endif value='Thu'>Thursday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Fri') selected @endif value='Fri'>Friday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Sat') selected @endif value='Sat'>Saturday</option>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->day == 'Sun') selected @endif value='6'>Sunday</option>
                                                </select>
                                                <div for="connection" class="error"></div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div id='monthly'@if(count($arr_user_data->getUserEmailSchedule) == 0 || isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->email_schedule != 2)) style="display: none" @endif>
                                            <dl class="dl-horizontal">
                                                <dt>Date:</dt>
                                                <dd>
                                                <select class='form-control' name="schedule_date" id="schedule_date">
                                                    <option value=''>--Select--</option>
                                                    <?php for ($i = 1; $i <= 28; $i++) { ?>
                                                    <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->date == $i) selected @endif value='{{ $i }}'>{{ $i }}</option>
                                                    <?php } ?>
                                                </select>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>


                                    <dl @if(count($arr_user_data->getUserEmailSchedule) == 0 || ((isset($arr_user_data-> getUserEmailSchedule))) && ($arr_user_data->getUserEmailSchedule->event == 1 || $arr_user_data->getUserEmailSchedule->meetup == 1 || $arr_user_data->getUserEmailSchedule->education == 1 || $arr_user_data->getUserEmailSchedule->article == 1 || $arr_user_data->getUserEmailSchedule->qurious == 1)) style="display: block;" @else style="display: none;" @endif class="choose-features-email dl-horizontal dl-horiz-height">
                                        <dt>Which features?</dt>
                                        <dd>
                                        <p class="chk-feed"> <label class="radio-inline"><input value="1" class="common-notify" type="checkbox" name="event"  @if (count($arr_user_data->getUserEmailSchedule) == 0 || (isset($arr_user_data-> getUserEmailSchedule) && $arr_user_data-> getUserEmailSchedule->event == 1)) checked="checked" @endif> Event</label></p>
                                        <p class="chk-feed"><label class="radio-inline"><input value="1" class="common-notify"  type="checkbox" name="meetup" @if (count($arr_user_data->getUserEmailSchedule) == 0 || (isset($arr_user_data-> getUserEmailSchedule) &&  $arr_user_data->getUserEmailSchedule->meetup == 1))  checked="checked" @endif > Meetup</label></p>
                                        <p class="chk-feed"><label class="radio-inline"><input value="1"  class="common-notify" type="checkbox" name="education" @if (count($arr_user_data->getUserEmailSchedule) == 0 || (isset($arr_user_data-> getUserEmailSchedule) &&  $arr_user_data->getUserEmailSchedule->education == 1))  checked="checked" @endif > Education</label></p>
                                        <p class="chk-feed"><label class="radio-inline"><input value="1" class="common-notify"  type="checkbox" name="article" @if (count($arr_user_data->getUserEmailSchedule) == 0 || (isset($arr_user_data-> getUserEmailSchedule) &&  $arr_user_data->getUserEmailSchedule->article == 1))  checked="checked" @endif > Article</label></p>
                                        <p class="chk-feed"><label class="radio-inline"><input value="1" class="common-notify"  type="checkbox" name="qurious" @if (count($arr_user_data->getUserEmailSchedule) == 0 || (isset($arr_user_data-> getUserEmailSchedule) &&  $arr_user_data->getUserEmailSchedule->qurious == 1))  checked="checked" @endif > Qrious</label></p>
                                        </dd>
                                    </dl>
                                </div>



                                <div>
                                    <dl class="dl-horizontal dl-horiz-height">
                                        <dt>Views on your posts :</dt>
                                        <dd>
                                        <select class='form-control' name="post_audience" id="post_audience">
                                            <option @if(count($arr_user_data->getUserEmailSchedule) == 0 || isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->post_audience == 1)) selected @endif value='1'>Daily</option>
                                            <option @if(isset($arr_user_data->getUserEmailSchedule) && $arr_user_data->getUserEmailSchedule->post_audience == 0) selected @endif value='0'>Off</option>
                                        </select>
                                        <div for="post_audience" class="error"></div>
                                        </dd>
                                    </dl>
                                </div>


                                <dl class="dl-horizontal dl-horiz-new ">
                                    <dt>Other :</dt>
                                    <dd>
                                    <label class="radio-inline"><input  value="1" type="checkbox" name="new_group_notification"  @if (count($arr_user_data->getUserEmailSchedule) == 0 || $arr_user_data-> getUserEmailSchedule->set_group_email == 1) checked="checked" @endif> When new group is created</label>
                                    <div for="follower" class="error"></div>
                                    </dd>
                                    @if($arr_user_data->user_type != 1)
                                    <dd>
                                    <label class="radio-inline"><input  value="1" type="checkbox" name="job_notification"  @if (count($arr_user_data->getUserEmailSchedule) == 0 || $arr_user_data-> getUserEmailSchedule->job_notification == 1  ) checked="checked"  @endif> When an applicant applied for job</label>
                                    <div for="follower" class="error"></div>
                                    </dd>
                                    @endif
                                    <dd>
                                    <label class="radio-inline"><input value="1" type="checkbox" name="promotion_notification"  @if (count($arr_user_data->getUserEmailSchedule) == 0 || $arr_user_data-> getUserEmailSchedule->ATG_promotion_noti == 1) checked="checked" @endif> Promotional mails and news</label>
                                    <div for="promotion" class="error"></div>
                                    </dd>
                                </dl>
                                <button class="save-btn btn" type="submit" name="btn_noti_setting" id="btn_noti_setting">SAVE</button>
                            </form>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')
<script>

$(document).ready(function () {
$("img").error(function () {
$(this).attr("src", "{{config('backend_img')}}image-not-found.png");
});
});


function del(team_id) {
if (confirm("Are you sure want to delete?")) {
$.ajax({
url: 'delete-team/' + btoa(team_id),
data: '',
dataType: 'json'
}).success(function (msg) {
location.reload();
})
}
return false;
}
function rem(resume_id) {
if (confirm("Are you sure want to remove this resume?")) {
$.ajax({
url: 'delete-resume/' + btoa(resume_id),
data: '',
dataType: 'json'
}).success(function (msg) {
location.reload();
})
}
return false;
}
function remov(job_id) {
if (confirm("Are you sure want to remove this saved job?")) {
$.ajax({
url: 'delete-saved-job/' + btoa(job_id),
data: '',
dataType: 'json'
}).success(function (msg) {
location.reload();
})
}
return false;
}
function readURL(input) {

}
function removeimage() {
    $('#blah').css("display", "none");
    $('#remove_image').css("display", "none");
    $("#profile_pic").prop("value", "");
}

$(".schedule_email").on('focus', function () {
    var ddl = $(this);
    ddl.data('previous', ddl.val());
}).on('change', function () {
    if ($(".schedule_email").val() == 1) {
        $("#weekly").show();
        $("#monthly").hide();
        $("#schedule_date").val('');
        $(".choose-features-email").show();
    } else if ($(".schedule_email").val() == 2) {
        $("#weekly").hide();
        $("#schedule_day").val('');
        $("#monthly").show();
        $(".choose-features-email").show();
    } else if ($(".schedule_email").val() == 0) {
        $("#monthly").hide();
        $("#weekly").hide();
        $("#schedule_date").val('');
        $("#schedule_day").val('');
        $(".choose-features-email").show();
    } else {
        $("#monthly").hide();
        $("#weekly").hide();
        $(".choose-features-email").hide();
    }

    var ddl = $(this);
    var previous = ddl.data('previous');
    if (previous == 3) {
        $(".common-notify").prop('checked', 1);
    }

});

$(document).ready(function () {

var _URL = window.URL || window.webkitURL;
var flag = true;
$("#profile_pic").change(function (e) {
var file, img;
var ext = this.files[0].name.split('.').pop().toLowerCase();
if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
alert('Only JPG, PNG or GIF files are allowed');
$("#profile_pic").val('');
} else {
if (file = this.files[0]) {
img = new Image();
img.onload = function () {
if (this.width >= 1900 && this.height >= 1200) {
var reader = new FileReader();
console.log(reader)
reader.onload = function (e) {
$('#blah').css("display", "block");
$('#blah').attr('src', e.target.result);
$('#remove_image').css("display", "block");
}
reader.readAsDataURL(file);
} else {
alert("Image height and width should be greater than 1900*1200");
$("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
}
};
img.src = _URL.createObjectURL(file);
readURL(this);
}
}
});
$("#edit_notifitcation_setting").validate({
errorElement: 'div',
rules: {
schedule_email: {
required: true,
},
schedule_day: {
required: true,
},
schedule_date: {
required: true,
},
},
messages: {
schedule_email: {
required: "Please select schedule for email.",
},
schedule_day: {
required: "Please select schedule day.",
},
schedule_date: {
required: "Please select schedule date.",
},
submitHandler: function (form) {
}
},
});
});
</script>