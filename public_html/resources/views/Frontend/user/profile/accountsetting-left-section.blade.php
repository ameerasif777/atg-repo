<style>
    #wrapper.active {
        width: 140px;
        transition: all 0.5s ease;
    }
    #settings{
        display: flex;
    }
    @media (max-width: 768px) {
        #wrapper{
            width: 45%;
            transition: all 0.75s ease;
        }
        #wrapper.active {
            width: 59px;
            transition: all 0.75s ease;
        }
        .bhoechie-tab.active{
            width: 55%;
            transition: all 0.5s ease;
        }
        .bhoechie-tab{
            /*width: 80%;*/
            transition: all 0.5s ease;
        }
        .bhoechie-tab>.bhoechie-tab-content,.bhoechie-tab>h6{
            padding-left: 10px;
            transition: all 0.5s ease;
        }
        /*#wrapper>.small-nav1 ,#wrapper.active .small-nav1{*/
            /*display: block;*/
        /*}*/
        /*#wrapper .full-nav{*/
            /*font-size: 10px;*/
            /*font-weight: bold;*/
        /*}*/
        /*#wrapper .list-group-item{*/
            /*padding: 6px 7px;*/
        /*}*/
        /*#wrapper>h6{*/
            /*padding: 6px 5px;*/
        /*}*/
    }
    #wrapper > .list-group .full-nav,#wrapper .full-nav {
        display: block;
        transition: top 300ms cubic-bezier(0.17, 0.04, 0.03, 0.94);
        overflow: hidden;

    }
    #wrapper.active > .list-group .full-nav,#wrapper.active .full-nav {
        display: none;
        transition: top 300ms cubic-bezier(0.17, 0.04, 0.03, 0.94);
        overflow: hidden;
    }
    #wrapper > .list-group .small-nav,#wrapper .small-nav {
        display: none;
        transition: top 300ms cubic-bezier(0.17, 0.04, 0.03, 0.94);
        overflow: hidden;
    }

    #wrapper.active > .list-group .small-nav,#wrapper.active .small-nav {
        display: block;
        transition: top 300ms cubic-bezier(0.17, 0.04, 0.03, 0.94);
        overflow: hidden;
    }


</style>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu" id="wrapper">
    <h6 id="settings"><span id="wrapper-toggle" class="small-nav1" data-toggle="tooltip" data-placement="right" title="">
							<span class="glyphicon glyphicon-list"></span>
						</span>&nbsp;
        <span class="full-nav"> account settings</span></h6>
    <div class="list-group">

        <form method="post" id="account_setting_2 account_setting_left" action="">
            <input type="hidden" name="is_head_load" id="is_head_load" value="true">
        </form>

        <a <?php if (Request::segment(1) == 'edit-user-bio') { ?> class="active list-group-item text-center" <?php } ?> class="list-group-item  text-center" id="profile_setting"  href="{{url('/')}}/edit-user-bio">
           <span class="small-nav"  data-placement="right" title="Profile Settings">
							<span class="glyphicon glyphicon-home"></span>
						</span>
            <span class="full-nav"> Profile Settings </span>
        </a>

        @if($arr_user_data['user_type'] == 1 || $arr_user_data['user_type'] == 3)
        <a <?php if (Request::segment(1) == 'manage-resumes') { ?> class="active list-group-item text-center" <?php } ?> class="list-group-item text-center" id="manage_resume" href="{{url('/')}}/manage-resumes">
            <span class="small-nav"  data-placement="right" title="Manage Resumes">
							<span class="glyphicon glyphicon-file"></span>
						</span>
            <span class="full-nav"> Manage Resumes </span>
        </a>

        @endif
        <a <?php if (Request::segment(1) == 'notification-setting') { ?> class="active list-group-item text-center" <?php } ?> class="list-group-item text-center" id="notification_setting" href="{{url('/')}}/notification-setting">
           <span class="small-nav"  data-placement="right" title="Notification Settings">
							<span class="glyphicon glyphicon-bell"></span>
						</span>
            <span class="full-nav">  Notification Settings </span>
        </a>
        <a <?php if (Request::segment(1) == 'pwd-setting') { ?> class="active list-group-item text-center" <?php } ?> class="list-group-item text-center" id="pwd_setting" href="{{url('/')}}/pwd-setting">
           <span class="small-nav"  data-placement="right" title="Password Settings">
							<span class="glyphicon glyphicon-lock"></span>
						</span>
            <span class="full-nav"> Password Settings </span>
        </a>
        @if($transaction_count>0)
        <a <?php if (Request::segment(1) == 'trans-history') { ?> class="active list-group-item text-center" <?php } ?> class="list-group-item text-center" id="trans_history" href="{{url('/')}}/trans-history">
           <span class="small-nav"  data-placement="right" title="Transaction History">
							<span class="glyphicon glyphicon-lock"></span>
						</span>
            <span class="full-nav"> Transaction History </span>
        </a>
        @endif

        @if($arr_user_data['user_type'] == 1 || $arr_user_data['user_type'] == 3)
        <a class="list-group-item text-center" style="cursor: pointer;" id="my_job_student" >
           <span class="small-nav"  data-placement="right" title="My Jobs">
							<span class="glyphicon glyphicon-saved"></span>
						</span>
            <span class="full-nav"> My Applied Jobs </span>
        </a>
        @endif

        <a class="list-group-item text-center" style="cursor: pointer;" href="{{url('/')}}/logout">
           <span class="small-nav"  data-placement="right" title="Logout">
							<span class="glyphicon glyphicon-off"></span>
						</span>
            <span class="full-nav"> Logout </span>
        </a>
    </div>
</div>
<script>
    
    $('#invite_friends').click(function () {
        location.href = '{{url("/")}}/invite-friends';
    });

    $('#my_job_student').click(function () {
        $('#account_setting').attr('action', '{{url("/")}}/jobs-applied');
        $('#account_setting').submit();
    });
    
    function expand(){
        $('#wrapper').toggleClass('active');
        $('.bhoechie-tab').toggleClass('active');
    }
    
    $('#wrapper-toggle').click(function(){
        expand();
    });

    $(document).ready(function() { 
        
        var viewportWidth = $(window).width();
        if (viewportWidth<600) {
            $('#wrapper').addClass('active');
            $('#wrapper-toggle').show();
            $('.bhoechie-tab-container').addClass('active');
        } else {
            $('#wrapper').removeClass('active');
            $('#wrapper-toggle').hide();
        }
        
        $('.list-group [data-toggle="tooltip"]').tooltip();
        
        $('.nav-style-toggle').on('click', function(event) {
            event.preventDefault();
            var $current = $('.nav-style-toggle.disabled');
            $(this).addClass('disabled');
            $current.removeClass('disabled');
            $('#wrapper').removeClass('navbar-'+$current.data('type'));
            $('#wrapper').addClass('navbar-'+$(this).data('type'));
        });
        
    });
</script>
