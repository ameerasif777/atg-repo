@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/public/assets/css/views/profile.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/css/gallery/demo.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/css/gallery/component.css') }}" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="{{url('/assets/Frontend/js/gallery/modernizr.custom.js')}}"></script>
<script src="{{url('/assets/Frontend/js/ajaxupload.3.5.js')}}"></script>

<style>

#add_user_name {
    background-color: #36BF8A;
    color: white;
}

.progress-bar:after, .animate > .progress-bar{
    animation: .6s linear 0s normal none infinite running move;
    background-image: -moz-linear-gradient(-45deg, rgba(255, 255, 255, 0.2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.2) 50%, rgba(255, 255, 255, 0.2) 75%, transparent 75%, transparent);
    background-size: 50px 50px;
    border-radius: 20px 8px 8px 20px;
    bottom: 0;
    content: "";
    left: 0;
    overflow: hidden;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 1;
}
.progress-bar{
    background-color: rgb(43, 194, 83);
    background-image: -moz-linear-gradient(center bottom , rgb(43, 194, 83) 37%, rgb(84, 240, 84) 69%);
    border-radius: 20px 8px 8px 20px;
    box-shadow: 0 2px 9px rgba(255, 255, 255, 0.3) inset, 0 -2px 6px rgba(0, 0, 0, 0.4) inset;
    display: block;
    height: 100%;
    overflow: hidden;
    position: relative;
}
#progress-wrp {
    padding: 1px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #fff;
   
}
#progress-wrp .progress-bar{
    height: 20px;
    border-radius: 3px;
    background-color: #36BF8A;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);

}
#progress-wrp .status{
    top:1px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
    font-weight: bold;
}
#progress-wrp1 {
    padding: 1px;
    position: relative;
    border-radius: 3px;
    margin: 10px;
    text-align: left;
    background: #fff;
    
}
#progress-wrp1 .progress-bar{
    height: 20px;
    border-radius: 3px;
    background-color: #36BF8A;
    width: 0;
    box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
}
#progress-wrp1 .status{
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
}

</style>
<?php
$user_cover_image = config('cover_path') . $arr_user_data["id"] . '/' . $arr_user_data['cover_photo'];
if (Storage::exists($user_cover_image) && $arr_user_data['cover_photo'] != '') {
    $cover_image = config('cover_image_url'). $arr_user_data["id"] . '/' . $arr_user_data['cover_photo'];
} else {
    $cover_image = config('img').'image-not-found3.jpg';
}
?>
<section class="persnal-sec" style="background-image:url({{$cover_image}});">
    <div class="container">
        <div class="inner-bios">
            <div class="row">
                <div class="col-md-4">
                    <div class="main-pros-information">
                        <?php
                        $session_data = Session::get('user_account');
                        ?>

                        <div class="main-pross">
                            <?php
                            $user_profile_image = config('profile_path') . $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                            if (Storage::exists($user_profile_image) && $arr_user_data['profile_picture'] != '') {
                                $profile_picture = $profile_pic->thumb_path;
                            } else {
                                if ($arr_user_data['gender'] == 0) {
                                    $profile_picture = config('avatar_male');
                                } else {
                                    $profile_picture = config('avatar_female');
                                }
                            }
                            ?>
                            <a href='javascript:void(0);'  onclick="getProfilePicture();" title="Upload profile Image" >
                                <img  id="main_profile1" src="{{$profile_picture}}"/>
                                <div class="fix-cam-ra"><i class="fa fa-camera"></i></div>
                            </a>

                        </div>
                        @if(isset($arr_user_data) && (!empty($arr_user_data)))
                        <!-- --username start----- -->
                        @if(isset($arr_user_data['user_name']) && (!empty($arr_user_data['user_name'])))
                        <h2><span style="color:#337ab7;">@</span><u><a>{!!$arr_user_data['user_name']!!}</u></a></h2>
                        @endif
                        <!-- --username end------- -->
                        <h2>{!!$arr_user_data['first_name'].' '.$arr_user_data['last_name']!!}</h2>
                        @endif
                        <div class="address-user">{!!isset($arr_user_data['location'])  && $arr_user_data['location']!='' ?$arr_user_data['location'] :'---' !!}</div>
                        <div class="address-thougt">{!!isset($arr_user_data['tagline']) && $arr_user_data['tagline']!=''  ?$arr_user_data['tagline'] :'---' !!}</div>
                        <h2>{!!isset($arr_user_data['profession']) && $arr_user_data['profession']!='' ?$arr_user_data['profession'] :'---' !!}</h2>

                        <!--<div class="address-user">{!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} </div>-->
                        <div class="address-user abc">
                        @if($arr_user_data['user_type'] == 4)
                            @if($jobcount > 0)
                                {!!($jobcount == 1 || $jobcount == 0)  ? $jobcount.' Job' :$jobcount.' Jobs' !!} 
                            @else    
                                {!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} 
                            @endif
                        @else
                            {!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} 
                        @endif    
                        </div>
                    </div>
                </div>
                @if(isset($session_data) && ($session_data != ''))
                @if($session_data['id'] == $arr_user_data['id']) 
                <div class="modal fade " id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog image_modal_align" role="document">
                        <div class="modal-content report-pop">
                            <div class="modal-body">
                                <div class="report-infos-cont">
                                    <h1 >Crop Profile Picture</h1>
                                    <form action="{{ url('upload-profile-picture')}}" id="frm-upload-profile" class="text-cnter-picture" method="post" >
                                        <div id="upload-demo">
                                        </div>
                                        <input type="hidden" id="imagebase64" name="imagebase64">
                                    </form>
                                </div>
                            </div>
                            <div class="boter-texter-feat modal-footer">
                                <div class="uping-imgs mar-auto-center filetype">
                                    <input type="file" id="upload" value="Choose a file" class="upit-img " name="upload_profile" onchange="btn_visible();">
                                </div>
                                <div id="progress-wrp" style="display:none;">
                                    <div class="progress-bar"></div >
                                    <div class="status">0%</div>
                                </div>
                                </br>
                                <button class="vanilla-rotate btn rotate_profile_btn" id = "rotate_profile" data-deg="-90">Rotate</button>
                                <button type="button" id="profile_submit" onclick="" class="btn update_profile_btn">Update Profile Picture</button>
                                <button type="submit" class="btn" data-dismiss="modal" id = "close_profile">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endif
                <div class="col-md-8">
                    <div class="inters-abter">                        
                        @if(isset($session_data) && ($session_data != ''))
                        @if($session_data['id'] == $arr_user_data['id'])
                        <form class="form-horizontal" name="frm_profile_cover_pic" id="frm_profile_cover_pic" action="" method="post" enctype="multipart/form-data"> 
                            <input type="hidden" name="base_url" id="base_url" value="<?php echo url('/'); ?>">
                            <input type="hidden" name="old_cover_img" id="old_cover_img" value="<?php echo (isset($arr_user_data) && $arr_user_data['cover_photo']) ? $arr_user_data['cover_photo'] : ''; ?>">
                            {!! csrf_field() !!}
                            <img  id="cover_image_img"> 
                            <div class="editors-pop"><a href="javascript:void(0)" id="upload_cover_img" onclick="getCoverPhoto();" title="Upload cover image"><i class="icon-Cover-Edit"></i></a></div>
                            <input type="file" id="profile_cover_pic" style="opacity: 0" name="profile_cover_pic" onchange="uploadImage(this)">

                        </form>
                        <div class="modal fade" id="profile-modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog image_modal_align" role="document">
                                <div class="modal-content report-pop model-resize">
                                    <div class="modal-body">
                                        <div class="report-infos-cont">
                                            <h1 >Crop Cover Image</h1>
                                            <form action="{{ url('upload-profile-picture')}}" id="frm-upload-profile" class="text-cnter-picture" method="post"  >
                                                <div id="upload-demo1">
                                                </div>
                                                <input type="hidden" id="coverImagec" name="coverImagec">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="boter-texter-feat modal-footer">
                                        <div class="uping-imgs mar-auto-center filetype1">
                                            <input type="file" id="upload1" value="Choose a file" class="upit-img" name="upload1" onchange="btn_visible1();">
                                        </div>
                                        <div id="progress-wrp1" style="display:none;">
                                            <div class="progress-bar"></div >
                                            <div class="status">0%</div>
                                        </div>
                                        </br>
                                        <button class="cover-rotate btn rotate_profile_btn" data-deg="-90" id = "rotate_profile1">Rotate</button>
                                        <button type="button" id="profile_submit1" onclick="" class="btn update_profile_btn">Update Cover Picture</button>
                                        <button type="submit" class="btn" data-dismiss="modal" id = "close_profile1">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif
                        <div class="models-cust">
                            <div class="btin-aab">                            
                                @if($session_data['id'] != $arr_user_data['id'])                      
                                @if(isset($is_connect) && (!empty($is_connect[0])))                           
                                @if($is_connect[0]->status == '0' || $is_connect[0]->status == '2' )
                                <a href="javascript:void(0)" class="conee-btn btn" id="status" value="1" title="Click to connect"  onclick="profileConfirm({{$is_connect[0]->from_user_id}},{{$is_connect[0]->to_user_id}})">Confirm </a>                            
                                @elseif((($is_connect[0]->to_user_id == $arr_user_data['id']) || ($is_connect[0]->from_user_id == $arr_user_data['id'])) && $is_connect[0]->status == '1')                               
                                <a href="javascript:void(0)" class="conee-btn btn" id="status" value="0" title="Click to disconnect"  onclick="profileConfirm({{$is_connect[0]->from_user_id}},{{$is_connect[0]->to_user_id}})">Connected </a>    
                                @endif
                                @else
                                <a href="javascript:void(0);"  class="conee-btn btn connection{!!$arr_user_data['id']!!}" onclick="addConnection({{$arr_user_data['id']}})"><i class="icon-Add"></i> Connect</a>

                                @endif

                                @endif
                                @if($session_data['id'] != $arr_user_data['id'])                             
                                @if(count($is_follower) == 0)
                                <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_user_data['id']!!}" title="click to follow" onclick="addFollower({{$arr_user_data['id']}})"><i class="icon-Add"></i> Follow</a>
                                @else
                                <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_user_data['id']!!}" title="click to unfollow" onclick="addFollower({{$arr_user_data['id']}})"><i class="icon-Follow"></i> Unfollow</a>
                                @endif
                                <!--<button type="button" class="folll-btn btn"><i class="icon-Follow"></i> Follow</button>-->
                                <button type="button" class="masss-btn btn"><i class="icon-Message"></i> Message</button>
                                <div class="dotss">
                                    <a href="javascript:void(0)" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="crxl"></span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="javaScript:void(0);">Block</a></li>
                                        <li><a href="javaScript:void(0);">Report</a></li>
                                    </ul>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="btin-aab">
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="divMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <div class="modal-title"></div>
            </div>
            <div class="modal-body text-center">
                <span id="msgSpan">Image can not be greater than 200 KB</span>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="call-me-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</section>
<section class="main-profile-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 user-with-content" id="top-bio" style="display:none;">
                
            </div>
             
        </div>
           <div class="row">
            <div class="col-sm-8">
                <div class="user-with-content">
                    <h2>About Me
                        <a href="{{url('edit-user-bio')}}" class="pull-right" title="Edit About Me"><i class="icon-Edit pull-right" title="Edit About Me"></i></a>
                    </h2>
                    <div class="inner-imfo-tion">
                        {!!isset($arr_user_data['about_me']) && $arr_user_data['about_me']!='' ?$arr_user_data['about_me'] :'---' !!}
                    </div>
                </div>
                <div class="user-with-content">
                    <h2>Member of groups <a href="javascript:void(0)" class="pull-right"><!--i class="fa fa-chevron-right"></i--></a></h2>
                    <div class="inner-imfo-tion user-hor-li">
                        <ul class="clearfix">
                            @foreach ($arr_user_group as $user_group)
                            <a href="/go/{{$user_group['group_name']}}">
                            <li>{!!isset($user_group['group_name']) ?$user_group['group_name'] :'---' !!}</li>
                            </a>
                            @endforeach 
                        </ul>
                    </div>
                </div>
		        @if($arr_user_data['user_type'] == 4)
                <div class="row">
                    @if($arr_user_data['twitter_page'] != 'https://twitter.com/' && $arr_user_data['twitter_page'] != '')
                        <?php
                        preg_match("/[^\/]+$/", $arr_user_data['twitter_page'], $matches);
                        $twitter_username = $matches[0];
                        ?>
                        <div class="col-sm-6">    
                            <a class="twitter-timeline" href="{{$arr_user_data['twitter_page']}}" data-width="380" data-height="400">Tweets by {{$twitter_username}}
                            </a> 
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> 
                        </div>
                    @endif
                    @if($arr_user_data['facebook_page'] != '' && $arr_user_data['facebook_page'] != 'https://facebook.com/')
                        <div class="col-sm-6">
                        <iframe src="https://www.facebook.com/plugins/page.php?href={!! urlencode($arr_user_data['facebook_page']) !!}&tabs=timeline&height=400&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div>
                    @endif
                </div>
                @endif
                <!-- Comment Start -->
                
                <div class="user-with-content ">
                    <h2>@if($session_data['id'] == $arr_user_data['id']) <!-- Updated user's to MY-->
                            MY
                        @else
                            {!! $arr_user_data['first_name'] !!}'s  
                        @endif
                        Activity<a href="javascript:void(0)" class="pull-right"><!--i class="fa fa-chevron-right"></i--></a></h2>
                    <div class="inner-imfo-tion oneline-user-li mCustomScrollbar @if(count($arr_activities) > 0) min-hieght-opt @endif">
                        <ul id="user_activity">
                            @if(count($arr_activities) > 0)
                                @foreach($arr_activities as $ua)
                                    <li>
                                        <div class="media main-user-acti">
                                            <div class="media-left">
                                                <i class="icon-Post"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"> 
                                                    <span>
                                                        @if(strlen($ua->activity_title) < 50)
                                                        {{ ucfirst($ua->user['first_name']) }} {{ ucfirst($ua->user['last_name']) }} @if($ex=explode('-',$ua->activity_title))
                                                        {!! ($ex[0]) !!}- <a class="actlink" href="{{url('/')}}{{$ua->activity_link}}"> {!! ucwords($ex[1]) !!}</a>
                                                        @endif
                                                        @else           
                                                        @if($ex=explode('-',$ua->activity_title))                                             
                                                        {{ ucfirst($ua->user['first_name']) }} {{ ucfirst($ua->user['last_name']) }} {!! ($ex[0]) !!}- <a class="actlink" href="{{url('/')}}{{$ua->activity_link}}">{{substr(strip_tags($ex[1]), 0, 10)}}<br>{{substr(strip_tags($ex[1]), 10,strlen($ex[1] ))}}
                                                        @endif
                                                        </a>
                                                        @endif
                                                    </span>
                                                </h4>
                                                <div class="posting-day">{!! date('M d, Y', strtotime($ua['created_at'])) !!}</div> <!--removed time from here -->
                                                                                               
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li>You will see your activity here but there is nothing here. 
                                <br>
                                <a href="{{url('/')}}/article" style="color: #269b70;">
                                Make your first post on ATG. Introduce yourself, share your knowledge or ask a question!
                                </a>
                                </li>   
                            @endif
                            
                        </ul>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <button class="btn btn-default btn-cons hide" id="activityloadbtn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> loading"></button>
                                <div class="ajax-loading"></div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                </div> <!-- comment end -->

                <div class="photos-followers">
                    <ul class="clearfix">
                        <li>
                            <div class="user-with-content">
                                <h2>Photos <a href="javascript:void(0)" class="pull-right"><!--i class="fa fa-chevron-right"></i--></a></h2>
                                <div class="inner-imfo-tion photos-info-img cust-scol-div mCustomScrollbar" id="reloadimg">
                                    @if(count($get_user_profile_picture) > 0 )
                                    <ul class="clearfix">
                                        <div class="dash-mid-cont gallery-details share-gallery">
                                            <div class="gallery-inn">
                                                <div id="grid-gallery" class="grid-gallery">
                                                    <?php $flag = 0; ?>
<!-- edit here-->
                                                    <section class="grid-wrap">      
                                                        <ul class="grid gallry"> 
                                                            @foreach ($get_user_profile_picture as $image)
                                                            <?php
                                                            $user_profile_image = config('profile_path'). $arr_user_data['id'] . '/thumb/' . $image->profile_picture;
                                                            if (Storage::exists($user_profile_image) && $image->profile_picture != '') {
                                                                $flag = 1;
                                                                $profile_picture =config('profile_pic_url') . $arr_user_data['id'] . '/thumb/' . $image->profile_picture;
                                                                ?>
                                                                <li>
                                                                    <figure>
                                                                        <img src="{{$profile_picture}}" alt="No Image Found"/>
                                                                    </figure>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                            @endforeach
                                                        </ul>
                                                    </section>
                                                    <section class="slideshow">
                                                        <ul>
                                                            @foreach ($get_user_profile_picture as $image)
                                                            <?php
                                                            $user_profile_image = config('profile_path') . $arr_user_data['id'] . '/' . $image->profile_picture;
                                                            if (Storage::exists($user_profile_image) && $image->profile_picture != '') {
                                                                $flag == 1;
                                                                $profile_picture = config('profile_pic_url') . $arr_user_data['id'] . '/' . $image->profile_picture;
                                                                ?>
                                                                <li>
                                                                    <figure>

                                                                        <img src="{{$profile_picture}}" alt="No Image Found"/>
                                                                    </figure>
                                                                </li>
                                                            <?php } ?>
                                                            @endforeach
                                                        </ul>
                                                        <nav>
                                                            <span class="nav-prev"><i class="fa fa-angle-left arrow-back-color" ></i></span>
                                                            <span class="nav-next"><i class="fa fa-angle-right arrow-back-color" ></i></span>
                                                            <span class="nav-close"><i class="fa fa-close"></i></span>
                                                        </nav>
                                                    </section>
                                                    <a href='javascript:void(0);'  onclick="getProfilePicture();" title="Upload profile Image" style="color: #269b70;">
                                                    <?php
                                                    if ($flag == 0) {
                                                        //echo 'Records not found';
                                                        echo 'Upload your first photo. Click here.';
                                                    }
                                                    ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                    @else
                                    <a href='javascript:void(0);'  onclick="getProfilePicture();" title="Upload profile Image" style="color: #269b70;">Upload your first photo. Click here.</a>
                                    @endif

                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="foll-conec-folow-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#connections" aria-controls="connections" role="tab" data-toggle="tab">connections</a></li>
                                    <li role="presentation"><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab">followers</a></li>
                                    <li role="presentation"><a href="#following" aria-controls="following" role="tab" data-toggle="tab">following</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="connections">

                                        <div class="conn-mediass mCustomScrollbar">
                                            @if (count($arr_user_connection)>0 )
                                            @foreach ($arr_user_connection as $user_connection)
                                            @if($user_connection['status'] != '3')
                                            @if($arr_user_data['id']==$user_connection['from_user_id'] || $arr_user_data['id']==$user_connection['to_user_id'] )                                            

                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <?php
                                                    if ($user_connection['from_user_id'] != $arr_user_data['id']) {
                                                        $user_id = $user_connection['from_user_id'];
                                                        $gender = $user_connection['from_gender'];
                                                        $user_profile_image = config('profile_path') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $user_connection['from_profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                        } else {
                                                            if ($gender == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                    } else if ($user_connection['to_user_id'] != $arr_user_data['id']) {
                                                        $user_id = $user_connection['to_user_id'];
                                                        $gender = $user_connection['to_gender'];
                                                        $url = config('profile_pic_url'). $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                        $user_profile_image = config('profile_path') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $user_connection['to_profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                        } else {
                                                            if ($gender == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <div class="floo-users">
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" target="_blank" >
                                                            <img src="{{$profile_picture}}" alt=""/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> 
                                                        @if(($user_connection['from_user_id'] != $arr_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} </a>
                                                        <a href="javascript:void(0)" class="pull-right" id="confirm"  onclick="confirm({{$user_connection['from_user_id']}},{{$arr_user_data['id']}}, 1)">Confirm </a>
                                                        @elseif((($user_connection['to_user_id'] == $arr_user_data['id']) || ($user_connection['from_user_id'] == $arr_user_data['id'])) && $user_connection['status'] == '1') 
                                                        @if($user_connection['to_first_name'] == $arr_user_data['first_name'] && $user_connection['to_last_name'] == $arr_user_data['last_name']) 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!} </a>
                                                        @else 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} </a>
                                                        @endif
                                                        <a href="javascript:void(0)" class="pull-right" >Connected </a>
                                                        @else
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}</a>
                                                        <a href="javascript:void(0)" class="pull-right" >Requested </a> 
                                                        @endif  </h4>
                                                </div>
                                            </div> 

                                            @elseif($user_connection['status']==1)
                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <?php
                                                    if ($user_connection['from_user_id'] != $arr_user_data['id']) {
                                                        $user_id = $user_connection['from_user_id'];
                                                        $gender = $user_connection['from_gender'];
                                                        $user_profile_image = config('profile_path') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $user_connection['from_profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $user_connection['from_user_id'] . '/thumb/' . $user_connection['from_profile_picture'];
                                                        } else {
                                                            if ($gender == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                    } else if ($user_connection['to_user_id'] != $arr_user_data['id']) {
                                                        $user_id = $user_connection['to_user_id'];
                                                        $gender = $user_connection['to_gender'];
                                                        $user_profile_image = config('profile_path') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $user_connection['to_profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $user_connection['to_user_id'] . '/thumb/' . $user_connection['to_profile_picture'];
                                                        } else {
                                                            if ($gender == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <div class="floo-users">
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" target="_blank" >                                                            
                                                            <img src="{{$profile_picture}}"  alt=""/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading "> 
                                                        @if(($user_connection['from_user_id'] != $arr_user_data['id']) && ($user_connection['status'] == '0' || $user_connection['status'] == '2' ) ) 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                                        <a href="javascript:void(0)" class="pull-right" id="confirm"  onclick="confirm({{$user_connection['from_user_id']}},{{$arr_user_data['id']}}, 1)">Confirm </a> 
                                                        @elseif((($user_connection['to_user_id'] == $arr_user_data['id']) || ($user_connection['from_user_id'] == $arr_user_data['id'])) && $user_connection['status'] == '1')  
                                                        @if($user_connection['to_first_name'] == $arr_user_data['first_name'] && $user_connection['to_last_name'] == $arr_user_data['last_name'])
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['from_first_name']).' '.ucfirst($user_connection['from_last_name']) !!}</a>
                                                        @else
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!!  ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!} </a>
                                                        @endif 
                                                        <a href="javascript:void(0)" class="pull-right" >Connected </a>
                                                        @else 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($user_id)}}" class="med-user-click">{!! ucfirst($user_connection['to_first_name']).' '.ucfirst($user_connection['to_last_name']) !!}</a>
                                                        <a href="javascript:void(0)" class="pull-right" >Requested </a> @endif</h4>
                                                </div>
                                            </div>
                                            @endif 
                                            @endif 
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                <!--No connections found.-->
                                                You are not connected to anyone. If you wish to, you can go to <a href="{{ url('/make-connection') }}" title="Connect" style="color: #269b70;">Connect</a> and meet people who love doing what you like to do.
                                            </div> 
                                            @endif
                                        </div>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="followers">
                                        <div class="conn-mediass mCustomScrollbar">
                                            @if (count($arr_follower)>0)
                                            @foreach ($arr_follower as $follower_user)
                                            @if($follower_user['status'] != '3')
                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <div class="floo-users">
                                                        <?php
                                                        $user_profile_image = config('profile_path') . $follower_user['follower_id_fk'] . '/thumb/' . $follower_user['profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $follower_user['profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $follower_user['follower_id_fk'] . '/thumb/' . $follower_user['profile_picture'];
                                                        } else {
                                                            if ($follower_user['gender'] == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                        ?>
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($follower_user['follower_id_fk'])}}" target="_blank" >
                                                            <img  id="main_profile" src="{{$profile_picture}}"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($follower_user['follower_id_fk'])}}" class="med-user-click">{!!$follower_user['first_name'].' '.$follower_user['last_name']!!}
                                                        </a>
                                                        <a href="javascript:void(0)" class="pull-right">Following</a></h4>
                                                </div>
                                            </div> 
                                            @endif
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                No Record Found!
                                            </div> 
                                            @endif
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="following">
                                        <div class="conn-mediass mCustomScrollbar">
                                            @if (count($arr_following)>0)
                                            @foreach ($arr_following as $following_user)
                                            @if($following_user['status'] != '3')

                                            <div class="media conne-medias">
                                                <div class="media-left">
                                                    <div class="floo-users">
                                                        <?php
                                                        $user_profile_image = config('profile_path') . $following_user['user_id_fk'] . '/thumb/' . $following_user['profile_picture'];
                                                        if (Storage::exists($user_profile_image) && $following_user['profile_picture'] != '') {
                                                            $profile_picture = config('profile_pic_url') . $following_user['user_id_fk'] . '/thumb/' . $following_user['profile_picture'];
                                                        } else {
                                                            if ($following_user['gender'] == 0) {
                                                                $profile_picture = config('avatar_male');
                                                            } else {
                                                                $profile_picture = config('avatar_female');
                                                            }
                                                        }
                                                        ?>
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($following_user['user_id_fk'])}}" target="_blank" >
                                                            <img  id="main_profile" src="{{$profile_picture}}"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"> 
                                                        <a href="{{url('/')}}/user-profile/{{base64_encode($following_user['user_id_fk'])}}" class="med-user-click">{!!$following_user['first_name'].' '.$following_user['last_name']!!}
                                                        </a>
                                                        <a href="javascript:void(0)" class="pull-right">Following</a></h4>
                                                </div>
                                            </div> 
                                            @endif
                                            @endforeach 
                                            @else
                                            <div class="media conne-medias">
                                                <!--No Record Found!-->
                                                You are not following anyone. If you wish to, you can go to <a href="{{ url('/make-connection') }}" title="Connect" style="color: #269b70;">Connect</a> and meet people who love doing what you like to do.
                                            </div> 
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <!--user_name-->
                @if(isset($arr_user_data['user_name']) && (empty($arr_user_data['user_name'])))
                    <div class="user-with-content">
                        <h2>ADD USERNAME</h2>
                        <div class="inner-imfo-tion bio-leb-profile">
                            <label for="" class="col-sm-4">Username :</label>

                            <input type="text" id="new_user_name" class="col-sm-5" name="new_user_name">

                            <button class="btn text-center col-sm-2" type="button" onclick="save_username();" name="add_user_name" id="add_user_name"
                                    style="margin-left: 2%;">ADD</button>
                            <br/><br><br>
                            <div id='verifyUsernameMsg' style='text-align:center;color:red;font-size:12px;'></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endif
                <!--user_name-->
                <div class="user-with-content" id="bottom-bio">
                    <h2>BIO @if($session_data['id'] == $arr_user_data['id']) 
                        <a href="{{url('edit-user-bio')}}" class="pull-right" title="Edit Bio">
                            <i class="icon-Edit pull-right" title="Edit Bio"></i>
                        </a>
                        @endif
                    </h2>
                    <div class="inner-imfo-tion bio-leb-profile">
                        <form class="form-horizontal">
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Name :</label>
                                <div class="col-sm-7">{!!$arr_user_data['first_name'].' '.$arr_user_data['last_name']!!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Location :</label>
                                <div class="col-sm-7">{!!( isset($arr_user_data['location']) && $arr_user_data['location']!='' ) ?$arr_user_data['location'] :'---' !!}</div>
                            </div>

                            <!--<div class="clearfix">
                                <label for="" class="col-sm-5">Height :</label>
                                <div class="col-sm-7">{!! ( isset($arr_user_data['height'] ) && $arr_user_data['height']!='') ? $arr_user_data['height'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Weight :</label>
                                <div class="col-sm-7">{!!( isset($arr_user_data['weight'] ) && $arr_user_data['weight']!='')? $arr_user_data['weight'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Body type :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['body_type'] ) ? $arr_user_data['body_type'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Hair :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['hair_color'] ) ? $arr_user_data['hair_color'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Eyes :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['eye_color'] ) ? $arr_user_data['eye_color'] :'---' !!}</div>
                            </div>-->
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Mobile No :</label>
                                <div class="col-sm-7">{!!  (isset($arr_user_data['mob_no'] ) && $arr_user_data['mob_no']!='') ? $arr_user_data['mob_no'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Phone No :</label>
                                <div class="col-sm-7">{!! (isset($arr_user_data['phone_no'] ) && $arr_user_data['phone_no']!='') ? $arr_user_data['phone_no'] :'---' !!}</div>
                            </div>
                            <!--<div class="clearfix">
                                <label for="" class="col-sm-5">Ethnicity :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['ethnicity_type'] ) ? $arr_user_data['ethnicity_type'] :'---' !!}</div>
                            </div>-->
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Profession :</label>
                                <div class="col-sm-7">{!!(isset($arr_user_data['profession'] ) && $arr_user_data['profession']!='')  ? $arr_user_data['profession'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Account Type :</label>
                                <div class="col-sm-7"> @if ($arr_user_data['user_type'] == '1') Student @elseif($arr_user_data['user_type'] == '4') Company @elseif($arr_user_data['user_type'] == '3') Professional @endif </div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Tag Line :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['tagline'] ) ? $arr_user_data['tagline'] :'---' !!}</div>
                            </div>
                            @if($arr_user_data['user_type'] != 4)
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Birth Date :</label>
                                <div class="col-sm-7">{!! isset($arr_user_data['user_birth_date'] ) ? $arr_user_data['user_birth_date'] :'---' !!}</div>
                            </div>
                            <div class="clearfix">
                                <label for="" class="col-sm-5">Gender :</label>
                                <div class="col-sm-7">{!!($arr_user_data['gender'] == '0') ? 'Male' :'Female' !!}</div>
                            </div>
                            @endif
                            @if($arr_user_data['user_type'] == 4 && $arr_user_data['website'] != '')
                                <div class="clearfix">
                                    <label for="" class="col-sm-5">Website :</label>
                                    <div class="col-sm-7"><a href="{!! $arr_user_data['website'] !!}" target="_blank">{!! $arr_user_data['website'] !!}</a></div>
                                </div>
                            @endif
                        </form>  
                    </div>
                </div>
                
				
				<div style="visibility:hidden; height: 50px;" class="go-to-connect">                   
                    <h4>Get More Followers</h4>
                    <div class="pros-user-design">
                        <?php
                        $user_profile_image = config('profile_path') . $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                        if (Storage::exists($user_profile_image) && $arr_user_data['profile_picture'] != '') {
                            $profile_picture = config('profile_pic_url') . $arr_user_data['id'] . '/thumb/' . $arr_user_data['profile_picture'];
                        } else {
                            if ($arr_user_data['gender'] == 0) {
                                $profile_picture = config('avatar_male');
                            } else {
                                $profile_picture = config('avatar_female');
                            }
                        }
                        ?>
                        <img  id="main_profile" src="{{$profile_picture}}"/>
                        <div class="go-icons go-gpluse"><a @if($arr_user_data['google_id']!='') href="{!!url('edit-user-bio')!!}"  @elseif($arr_user_data['google_id']=='') href="{!!url('gmail')!!}"  @endif  ><i class="fa fa-google-plus"></i></a></div>
                        <div class="go-icons go-face"><a @if($arr_user_data['fb_id']!='') href="{!!url('edit-user-bio')!!}"  @elseif($arr_user_data['fb_id']=='') href="{!!url('facebook')!!}"  @endif  ><i class="fa fa-facebook-f"></i></a></div>
                        <div class="go-icons go-tweet"><a @if($arr_user_data['twitter_id']!='') href="{!!url('edit-user-bio')!!}"  @elseif($arr_user_data['twitter_id']=='') href="{!!url('twitter')!!}"  @endif ><i class="fa fa-twitter"></i></a></div>
                        <div class="go-icons go-liin"><a @if($arr_user_data['linkdin_id']!='') href="{!!url('edit-user-bio')!!}"  @elseif($arr_user_data['linkdin_id']=='') href="{!!url('linkedin')!!}"  @endif><i class="fa fa-linkedin"></i></a></div>
                    </div>
                </div>
				
            </div>
        </div>
    </div>
</section>

<script src="{{url('/assets/Frontend/js/gallery/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/masonry.pkgd.min.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/classie.js')}}"></script>
<script src="{{url('/assets/Frontend/js/gallery/cbpGridGallery.js')}}"></script>

<script langauge="javascript">
							
							
                            new CBPGridGallery(document.getElementById('grid-gallery'));

                            $(".preventGallery").click(function(event) {
                            event.stopPropagation();
                            });
                            function addConnection(to_user_id)
                            {
                            $.get('{!!url(' / ')!!}/add-connection', {to_user_id: to_user_id}, function (msg) {
                            if (msg == true)
                            {
                            $(".connection" + to_user_id).html('<i class="fa fa-check"></i> Connected');
                            } else{
                            $(".connection" + to_user_id).html('<i class="icon-Add"></i> Connect');
                            }

                            });
                            }

                            function addFollower(follower_user_id)
                            {
                            $.ajax({
                            type: "get",
                                    url: '{!!url(' / ')!!}/make-follower',
                                    data: {follower_user_id: follower_user_id},
                                    dataType: "json",
                                    success: function(msg) {
                                    if (msg.success == 1)
                                    {
                                    $(".follow" + follower_user_id).html('<i class="fa fa-check"></i> Unfollow');
                                    } else {
                                    $(".follow" + follower_user_id).html('<i class="icon-Add"></i> Follow');
                                    }

                                    }
                            })
                            }
                            function getCoverPhoto()
                            {
                            $('#profile-modal1').modal('show');
                            return false;
                            // document.getElementById('cover_photo').click();
                            }

                            function getProfilePicture()
                            {
                            $('#profile-modal').modal('show');
                            $('#reloadimg').load(document.URL +  ' #reloadimg');
                            return false;
                            // document.getElementById('profile_picture').click();
                            }

                            function profileConfirm(from_id, user_id){
                            var status = $("#status").attr("value");
                            var base_url = jQuery("#base_url").val();
                            $.ajax({
                            url: base_url + '/confirm',
                                    dataType: 'json',
                                    method: 'get',
                                    data :{from_id:from_id, user_id:user_id, status:status},
                                    success: function (response) {

                                    if (response.msg == '1') {
                                    $('#status').text('Connected');
                                    $("#status").attr("value", "0");
                                    }
                                    else {
                                    $('#status').text('confirm');
                                    $("#status").attr("value", "1");
                                    }
                                    }
                            });
                            }

                            function confirm(from_id, user_id, status){
                            var base_url = jQuery("#base_url").val();
                            $.ajax({
                            url: base_url + '/confirm',
                                    dataType: 'json',
                                    method: 'get',
                                    data :{from_id:from_id, user_id:user_id, status:status},
                                    success: function (response) {

                                    if (response.msg == '1') {
                                    $('#confirm').text('Connected');
                                    }

                                    }
                            });
                            }

                            function uploadImage(value) {

                            var form = document.getElementById('coverform');
                            var coverImagec = $('#coverImagec').val();
                            var base_url = jQuery("#base_url").val();
                            var progress_bar_id = '#progress-wrp1';
                            $.ajax({
                            url: base_url + '/cover-photo',
//                                                                            contentType: false,
//                                                                            processData: false,
                                    dataType: 'json',
                                    data: {'coverImagec':coverImagec},
                                    method: 'post',
                                    xhr: function() {
                                        var xhr = new window.XMLHttpRequest();
                                        $(progress_bar_id).css("display","block");
                                        xhr.upload.addEventListener("progress", function(evt) {
                                          if (evt.lengthComputable) {
                                            var percentComplete = evt.loaded / evt.total;
                                            percentComplete = parseInt(percentComplete * 100);
                                            console.log(percentComplete);
                                            $(progress_bar_id +" .progress-bar").css("width", + percentComplete +"%");
                                            $(progress_bar_id + " .status").text(percentComplete +"%");
                                            if (percentComplete === 100) {
                                                //$('.filetype1').addClass('hide');
                                            }

                                          }
                                        }, false);

                                        return xhr;
                                    },
                                    success: function (response) {
                                    
                                    $(progress_bar_id +" .progress-bar").css("width", "0%");
                                    $(progress_bar_id + " .status").text("0%");
                                    $(progress_bar_id).css("display","none");    
                                    if (response.error == '0') {
                                    $('#upload1').val('');
                                    $('#coverImagec').val('');
                                    $('#profile-modal1').modal('hide');
                                    var url = "{{config('cover_image_url')}}{!!$arr_user_data['id']!!}/" + response.cover_photo;
                                    $('.persnal-sec').css('backgroundImage', 'url(' + url + ')');
//                                                                                    document.location.reload();
                                    return false;
                                    } else if (response.error == '1')
                                    {
                                    alert('error uploading the file!');
                                    return false;
                                    }
                                    }
                            })
                            }

                            function uploadProfileImage(value) {
                            var imagebase = $('#imagebase64').val();
                            var base_url = jQuery("#base_url").val();
                            var progress_bar_id = '#progress-wrp'; 
                            $.ajax({
                                    url: base_url + '/upload-profile-picture',
//                                  contentType: false,
//                                  processData: false,
                                    dataType: 'json',
                                    data: { 'imagebase':imagebase },
                                    type: 'post',
                                    xhr: function() {
                                        var xhr = new window.XMLHttpRequest();
                                        $(progress_bar_id).css("display","block");
                                        xhr.upload.addEventListener("progress", function(evt) {
                                          if (evt.lengthComputable) {
                                            var percentComplete = evt.loaded / evt.total;
                                            percentComplete = parseInt(percentComplete * 100);
                                            console.log(percentComplete);
                                            $(progress_bar_id +" .progress-bar").css("width", + percentComplete +"%");
                                            $(progress_bar_id + " .status").text(percentComplete +"%");
                                            if (percentComplete == 100) {
                                            //    $('.filetype').addClass('hide');
                                            }

                                          }
                                        }, false);

                                        return xhr;
                                    },
                                    success: function (response) {
                                    console.log(response);
                                    $.ajax({
                                        url:"getfullimagepath/{!!$arr_user_data['id']!!}",
                                        type: 'get',
                                        dataType: 'json',
                                        success: function(response){
                                            var url =response.path;
                                            var thumb_url = response.thumb_path;
                                            document.getElementById("main_profile").src = url;
                                            document.getElementById("main_profile1").src = url;
                                            document.getElementById("thumb_profile").src = thumb_url;
                                            
                                        }
                                    })

                                    $('#reloadimg').load(document.URL +  ' #reloadimg');
                                    
                                   
                                    
                                    $(progress_bar_id +" .progress-bar").css("width", "0%");
                                    $(progress_bar_id + " .status").text("0%");
                                    $(progress_bar_id).css("display","none");
                                    if (response.error == '0') {
                                    $('#upload').val('');
                                    $('#imagebase64').val('');
                                    $('#profile-modal').modal('hide');
                                    $('#grid-gallery').load(document.URL +  ' #grid-gallery');
                                     
//                                                                                    document.location.reload();
                                    return false;
                                    } else if (response.error == '1')
                                    {
                                    alert('error uploading the file!');
                                    return false;
                                    }
                                    }
                            })

                            // $.ajax({
                            //     url:"getfullimagepath/{!!$arr_user_data['id']!!}",
                            //     type: 'get',
                            //     dataType: 'json',
                            //     success: function(response){
                            //         var url =response.path;
                            //         var thumb_url = response.thumb_path;
                            //         document.getElementById("main_profile").src = url;
                            //         document.getElementById("main_profile1").src = url;
                            //         document.getElementById("thumb_profile").src = thumb_url;
                                     
                            //     }
                            // })
                            
                            }

</script>

<script>
    //profile crop code

    
$(document).ready(function () {

    var $uploadCrop;
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready'); // Where is Upload-Demo class, shoud not it be id?
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: { width: 300, height: 300, type: 'square' },
        boundary: { width: 270, height: 400 },
        enableOrientation: true
    });

    $('.vanilla-rotate').on('click', function(ev) {
        $uploadCrop.croppie('rotate',90);
    });

    $('#upload').on('change', function () {
        readFile(this);
    });
    
    $('#profile_submit').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas', size:'viewport'
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            uploadProfileImage();
            // $('#frm-upload-profile').submit();
        });
        //return false;
    });


    //Upload cover pic
    var $uploadCrop1;
    function readFile1(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop1.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo1').addClass('ready');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop1 = $('#upload-demo1').croppie({
        viewport: { width: 500, height: 300, type: 'rectangle' },
        boundary: { width: 400, height: 400 },
        enableOrientation: true
    });

    $('#upload1').on('change', function () {
        readFile1(this);
    });

    $('#profile_submit1').on('click', function (ev) {
        $uploadCrop1.croppie('result', { type: 'canvas', size: 'original' })
        .then(function (resp) { $('#coverImagec').val(resp); uploadImage(); });
    });

    $('.cover-rotate').on('click', function(ev) {
        $uploadCrop1.croppie('rotate',90);
    });

}); // document.ready end

    $('#verifyUsernameMsg').html('');
    $('#new_user_name').keyup(function(){
        var base_url = jQuery("#base_url").val();
        var key = $('#new_user_name').val();
        $.ajax({
        url: base_url + '/verify_username',
            dataType: 'json',
            data: {'key':key, id: "{{ $arr_user_data['id'] }}"},
            method: 'post',
            success: function (response) {
                if (response == '0'){
                    $('#verifyUsernameMsg').html('Good choice, but this username is already taken. Please try another.');
                } else if (response == '1') {
                    $('#verifyUsernameMsg').html('Username is unavailable. Please try another.');
                } else {
                    $('#verifyUsernameMsg').html('');
                }
            }
        });
    })


    function save_username() {
        var base_url = jQuery("#base_url").val();
        //alert(base_url);
        var u_val = $('#new_user_name').val();
        //alert(u_val);
        if (u_val == ''){
        alert('Please Enter the value.');
        return false;
        }

        if (/^[a-zA-Z0-9_ ]*$/.test(u_val) == false) {
        $('#verifyUsernameMsg').html('Please do not enter special characters.');
        return false;
        }
        if ($.trim(u_val).length < 4){
        $('#verifyUsernameMsg').html('Please Enter more than 4 character.');
        return false;
        }
        if (!(/^\S{3,}$/.test(u_val))) {
        $('#verifyUsernameMsg').html('Username cannot contain space.');
        return false;
        }

        $.ajax({
            url: base_url + '/add_new_username',
                    dataType: 'json',
                    data: {'u_val':u_val},
                    method: 'post',
                    success: function (response) {
                    if (response.error == '0') {
                    $('#verifyUsernameMsg').html('Good choice, but this username is already taken. Please try another.');
                    return false;
                    } else if (response.error == '1') {
                    $('#verifyUsernameMsg').html('Username is unavailable. Please try another.');
                    return false;
                    } else {
                    $('#verifyUsernameMsg').css('color', 'green').html('Username has been added successfully.');
                    window.location.href = base_url + '/' + response.username + '?m=1';
                    }
                }
        });
    }
    $('#upload').change(function(){
        if($(this).val() != '')
            $('.filetype').addClass('hide');
    });
    $('#upload1').change(function(){
        if($(this).val() != '')
            $('.filetype1').addClass('hide');
    });
</script>

<script>
    var page = 1; //track user scroll as page number, right now page number is 1
    //load_more(page); //initial content load
    var ajaxInProgress = false;
    $(window).scroll(function() { //detect page scroll
        //if($(window).scrollTop() + $(window).height() >= $(document).height()) { 
        if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
        //if user scrolled from top to bottom of the page
            if (!ajaxInProgress){
                page++; //page number increment
                load_more(page); //load content   
            }    
        }
    });     
    function load_more(page){
        if($('.ajax-loading').html() != 'No more records!')
        {
            $.ajax(
            {
                url: '{{url("/")}}/user-activity-load?page=' + page,
                type: "get",
                datatype: "html",
                beforeSend: function()
                {
                    $('#activityloadbtn').removeClass('hide');
                    $('#activityloadbtn').button('loading');
                    ajaxInProgress = true;
                }
            })
            .done(function(data)
            {
                console.log(data.length);
                ajaxInProgress = false;
                $('#activityloadbtn').button('reset');
                $('#activityloadbtn').addClass('hide');
                if(data.length == 0){
                    //notify user if nothing to load
                    $('.ajax-loading').html("No more records!");
                    $('.ajax-loading').addClass("hide");
                    return;
                }
                
                //$('.ajax-loading').hide(); //hide loading animation once data is received
                $("#user_activity").append(data); //append data into #results element          
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                console.log('No response from server');
            });
        }    
    }

	
     
</script>
<script>
		$(document).ready(function(){
		
		
	if ($(window).width() < 500) {
           console.log("Width < 500");
			$("#rotate_profile").empty();
			$('<i class="fa fa-rotate-right"></i>').appendTo("#rotate_profile");
			$("#profile_submit").empty();
			$('<i class="fa fa-check"></i>').appendTo("#profile_submit");
			$("#close_profile").empty();
			$('<i class="fa fa-close"></i>').appendTo("#close_profile");
			
			$("#rotate_profile1").empty();
			$('<i class="fa fa-rotate-right"></i>').appendTo("#rotate_profile1");
			$("#profile_submit1").empty();
			$('<i class="fa fa-check"></i>').appendTo("#profile_submit1");
			$("#close_profile1").empty();
			$('<i class="fa fa-close"></i>').appendTo("#close_profile1");
        } else {
            console.log("More than 400 resize");
			$("#rotate_profile").empty();
			$("#rotate_profile").text("Rotate");
			$("#profile_submit").empty();
			$("#profile_submit").text("Update Profile Picture");
			$("#close_profile").empty();
			$("#close_profile").text("Close");
			
			$("#rotate_profile1").empty();
			$("#rotate_profile1").text("Rotate");
			$("#profile_submit1").empty();
			$("#profile_submit1").text("Update cover Picture");
			$("#close_profile1").empty();
			$("#close_profile1").text("Close");
			
		}
    
		

	$(window).on('resize', function() {
			
        if ($(window).width() < 500) {
           console.log("Width < 500");
			$("#rotate_profile").empty();
			$('<i class="fa fa-rotate-right"></i>').appendTo("#rotate_profile");
			$("#profile_submit").empty();
			$('<i class="fa fa-check"></i>').appendTo("#profile_submit");
			$("#close_profile").empty();
			$('<i class="fa fa-close"></i>').appendTo("#close_profile");
			
			$("#rotate_profile1").empty();
			$('<i class="fa fa-rotate-right"></i>').appendTo("#rotate_profile1");
			$("#profile_submit1").empty();
			$('<i class="fa fa-check"></i>').appendTo("#profile_submit1");
			$("#close_profile1").empty();
			$('<i class="fa fa-close"></i>').appendTo("#close_profile1");
        } else {
            console.log("More than 400 resize");
			$("#rotate_profile").empty();
			$("#rotate_profile").text("Rotate");
			$("#profile_submit").empty();
			$("#profile_submit").text("Update Profile Picture");
			$("#close_profile").empty();
			$("#close_profile").text("Close");
			
			$("#rotate_profile1").empty();
			$("#rotate_profile1").text("Rotate");
			$("#profile_submit1").empty();
			$("#profile_submit1").text("Update cover Picture");
			$("#close_profile1").empty();
			$("#close_profile1").text("Close");
			
		}
    
		});
});	
</script>
<script>
function btn_visible()
		{
			$("#rotate_profile").css("display","inline-block");
			$("#profile_submit").css("display","inline-block");
		}
		
		function btn_visible1()
		{
			$("#rotate_profile1").css("display","inline-block");
			$("#profile_submit1").css("display","inline-block");
		}
</script>


@include('Frontend.includes.user.user_profile_fx')
@include('Frontend.includes.footer')
