@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>

<!--<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>Account Settings</h1>
    </div>
</section>-->
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>

                    <div class="bhoechie-tab-content active">
                        <div class="main-inner-tab-info clearfix">
                            <form id="user_bio" name="user_bio" method="post" action="{{url('edit-bio')}}">
                                {!! csrf_field() !!}
                                
                                <dl class="dl-horizontal">
                                    <dt>Height<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <div class="input-bio">
                                            <input type="text" id="height" name="height" value="{!!$arr_user_data['height']!!}"  placeholder="Height" aria-describedby="basic-addon2">
                                            <!--<span class="input-group-addon" id="basic-addon2">Inches</span>-->
                                            <div class="select-box"> <select  class="select-group-addon form-control" name="height_unit">
                                                    <option <?php if ($arr_user_data['height_unit'] == '0') { ?>selected="selected"<?php } ?>value="0">Inches</option>
                                                    <option <?php if ($arr_user_data['height_unit'] == '1') { ?>selected="selected"<?php } ?> value="1">CM</option>
                                                </select></div>
                                        </div>
                                        <div for="height" class="error"></div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Weight<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <div class="input-group">
                                            <input type="text" id="weight" name="weight" value="{!!$arr_user_data['weight']!!}"  placeholder="Weight" aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2">Kg</span>
                                        </div>
                                        <div for="weight" class="error"></div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Body type<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <?php
                                        if (isset($arr_user_data['body_id']) && $arr_user_data['body_id'] != 'NULL') {
                                            $val = $arr_user_data['body_id'];
                                        } else {
                                            $val = '';
                                        }
                                        ?>
                                        <select class="form-control" name="body_type" id="body_type">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($arr_body_type as $parent) {
                                                ?>
                                                <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->body_type; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Hair<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <?php
                                        if (isset($arr_user_data['hair_id']) && $arr_user_data['hair_id'] != 'NULL') {
                                            $val = $arr_user_data['hair_id'];
                                        } else {
                                            $val = '';
                                        }
                                        ?>
                                        <select class="form-control" name="hair_color" id="hair_color">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($arr_hair_color as $parent) {
                                                ?>
                                                <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->hair_color; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Eyes<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <?php
                                        if (isset($arr_user_data['eye_id']) && $arr_user_data['eye_id'] != 'NULL') {
                                            $val = $arr_user_data['eye_id'];
                                        } else {
                                            $val = '';
                                        }
                                        ?>
                                        <select class="form-control" name="eye_color" id="eye_color">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($arr_eye_color as $parent) {
                                                ?>
                                                <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->eye_color; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Ethnicity<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <?php
                                        if (isset($arr_user_data['ethnicity_id']) && $arr_user_data['ethnicity_id'] != 'NULL') {
                                            $val = $arr_user_data['ethnicity_id'];
                                        } else {
                                            $val = '';
                                        }
                                        ?>
                                        <select class="form-control" name="ethnicity_type" id="ethnicity_type">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($arr_ethnicity_type as $parent) {
                                                ?>
                                                <option <?php if ($parent->id === $val) { ?>selected="selected"<?php } ?> value="<?php echo $parent->id; ?>"><?php echo $parent->ethnicity_type; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </dd>
                                </dl>
                                
                                <button class="save-btn btn" type="submit" name="btn_account_setting" id="btn_account_setting">SAVE</button>
                            </form>
                        </div>
                    </div>
                     @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content tem-capter clearfix">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="tema-members-outs">
                           
                        </div>
                    </div>
                     @endif
                     @if($arr_user_data['user_type'] == 1)
                     <div class="bhoechie-tab-content">
                        
                    </div>

                    <div class="bhoechie-tab-content">
                       
                    </div>
                     @endif
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                     
                     <div class="bhoechie-tab-content">
                        
                    </div>
                     <div class="bhoechie-tab-content">
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Backend/js/jquery.geocomplete.min.js')}}"></script>
<script src="{{url('/assets/Frontend/js/setting/account-setting.js')}}"></script>
<script src="{{url('/assets/Frontend/js/team/team.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/job.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script>
$(function () {
$("#location").geocomplete({
details: ".geo-details",
        detailsAttribute: "data-geo",
        //  map: ".map_canvas",
});
        $("#location").bind("geocode:result", function (event, result) {
var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': result.formatted_address}, function (results, status) {
        var location = results[0].geometry.location;
                var lat = location.lat();
                var lng = location.lng();
                $("input[name=latitude]").val(lat);
                $("input[name=longitude]").val(lng);
        });
});
        $('#skill').tokenfield({
autocomplete: {
// source: ['css3', 'php', 'java', 'javascript', 'jquery', '.net', 'html5', 'laravel', 'codeigniter'],
delay: 100
},
        showAutocompleteOnFocus: true
})
});

$(function () {
$("#dob").datepicker({
changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-m-d',
        yearRange: '1980:2016',
});
});
                                                    
$(document).ready(function () {
    $("img").error(function () {
        $(this).attr("src", "{{url('/')}}/public/assets/Backend/img/image-not-found.png");
    });
});
                                                                                                    
</script>
<script>
            function del(team_id) {
            if (confirm("Are you sure want to delete?")) {
            $.ajax({
            url: 'delete-team/' + btoa(team_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
             function rem(resume_id) {
            if (confirm("Are you sure want to remove this resume?")) {
            $.ajax({
            url: 'delete-resume/' + btoa(resume_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
              function remov(job_id) {
            if (confirm("Are you sure want to remove this saved job?")) {
            $.ajax({
            url: 'delete-saved-job/' + btoa(job_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }

    function readURL(input) {
//    var ext = input.value.match(/\.(.+)$/)[1];
//            switch (ext)
//    {
//    case 'jpg':
//            case 'bmp':
//            case 'png':
//            case 'tif':
//            if (input.files && input.files[0]) {
//                console.log(input.files)
//                console.log(input.files[0].height)
//    var reader = new FileReader();
//    console.log(reader)
//            reader.onload = function(e) {
//            $('#blah').css("display", "block");
//                    $('#blah').attr('src', e.target.result);
//                    $('#remove_image').css("display", "block");
//            }
//
//    reader.readAsDataURL(input.files[0]);
//    }
//    break;
//            default:
//            alert('Only Image allowed !');
//            input.value = '';
//    }

    }

    function removeimage() {
    $('#blah').css("display", "none");
            $('#remove_image').css("display", "none");
            $("#profile_pic").prop("value", "");
    }


</script> 
<script>
    
      function checkResumeCount(){
            $.ajax({
            url: 'resume-count',
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
               var resume_count = msg.resume_count[0].count
                if(resume_count == 3){
                   if (confirm("Are you sure want to remove first uploaded resume?")) {
                        $('#upload_resumes').submit();
                   }else{
                       return false;
                   }
                }
            })
        }
        
    $(document).ready(function () {
      
        $('#file').change(function () {
            var val = $(this).val();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf)$");
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please upload a file only of type pdf,doc,docx.');
            }
            var f = this.files[0]

            if (f.size > 10000000 || f.fileSize > 10000000)
            {
                alert("Allowed file size less than 10 MB.")
                this.value = null;
            }
            
            
            
        });
        var _URL = window.URL || window.webkitURL;
    var flag = true; 
        
        $("#profile_pic").change(function(e) {
           
        var file, img;
        var ext = this.files[0].name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert('Only JPG, PNG or GIF files are allowed');
            $("#profile_pic").val('');
        } else {
            
            if (file = this.files[0]) {
                img = new Image();
                img.onload = function() {
                    if (this.width >= 1900 && this.height >= 1200) {
                        var reader = new FileReader();
    console.log(reader)
            reader.onload = function(e) {
            $('#blah').css("display", "block");
                    $('#blah').attr('src', e.target.result);
                    $('#remove_image').css("display", "block");
            }

    reader.readAsDataURL(file);
                    } else {
                        //flag = false;
                        alert("Image height and width should be greater than 1900*1200");
                       // $('#blah').attr('src', './media/front/img/image-not-found.png');
                        $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                    }
                     
                };
                 img.src = _URL.createObjectURL(file);
                readURL(this);
              
            }
        }
    }); 
    

    });
</script> 





