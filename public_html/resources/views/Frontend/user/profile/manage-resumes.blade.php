@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content clearfix">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>

                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                     @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content  tem-capter clearfix">
                        
                    </div>
                    <div class="bhoechie-tab-content ">
                        <div class="tema-members-outs">
                            
                        </div>
                    </div>
                     @endif
                     @if($arr_user_data['user_type'] == 1 || $arr_user_data['user_type'] == 3)
                     
                     <div class="bhoechie-tab-content active">
                        <div class="tema-members-outs">
                            <h4>Upload Resumes</h4>
                            @if(isset($resumes_count) && count($resumes_count) > 0)
                            <div class="team-mem-inner">
                                <div class="users-conectry-nav temb-slids">
                                    <ul class="clearfix">
                                        @foreach($resumes_count as $key=>$val)
                                            <li ><a href="{{url('/')}}/view-resume/{{ base64_encode($val->uploaded_path) }}" title="See Resume"> {{ ucfirst($val->uploaded_path) }} ({{$val->created_at}})</a><div class="remores"><a href="javascript:void(0)" onclick="rem({{ $val->resume_id }})"><i class="fa fa-trash-o"></i></a></div></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            
                            <form name='upload_resumes' id='upload_resumes' method='post' action='{{url('/')}}/upload-resumes' enctype="multipart/form-data">
                            
                                <div class="team-mem-inner team-men-remove-padd">
                                <div class="users-conectry-nav csuto-otp temb-slids ">
                                    
                                    <!--<div class="uping-imgs">-->
                                    <input type="file" class="form-control" name="file"  class="upit-img"  onchange='checkResumeCount()' id="file">
                                      <!--</div>-->
                                </div>
                                  
                            </div>
                                
                                <button class="btn teme-save-btn" id="btn_upload_resumes"  name="btn_upload_resumes" >Upload</button>
                                
                            </form>
                            
                        </div>
                    </div>
                    

                    <div class="bhoechie-tab-content">
                       
                    </div>
                     @endif
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                    <div class="bhoechie-tab-content">
                        
                    </div>
                     
                     <div class="bhoechie-tab-content">
                        
                    </div>
                     <div class="bhoechie-tab-content">
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script>
                                                    
$(document).ready(function () {
    $("img").error(function () {
        $(this).attr("src", "{{url('/')}}/public/assets/Backend/img/image-not-found.png");
    });
});
                                                                                                    
</script>
<script>
            function del(team_id) {
            if (confirm("Are you sure want to delete?")) {
            $.ajax({
            url: 'delete-team/' + btoa(team_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
             function rem(resume_id) {
            if (confirm("Are you sure want to remove this resume?")) {
            $.ajax({
            url: 'delete-resume/' + btoa(resume_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
              function remov(job_id) {
            if (confirm("Are you sure want to remove this saved job?")) {
            $.ajax({
            url: 'delete-saved-job/' + btoa(job_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }

    function readURL(input) {


    }

    function removeimage() {
    $('#blah').css("display", "none");
            $('#remove_image').css("display", "none");
            $("#profile_pic").prop("value", "");
    }


</script> 
<script>
    
      function checkResumeCount(){
            $.ajax({
            url: 'resume-count',
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
               var resume_count = msg.resume_count[0].count
                if(resume_count == '3'){
                   if (confirm("If you upload more than three resumes old resume will be discarded.Are you sure you want to continue?")) {
                        $('#upload_resumes').submit();
                   }else{
                       return false;
                   }
                }
            })
        }
        
    $(document).ready(function () {
      
        $('#file').change(function () {
            var val = $(this).val();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf)$");
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please upload a file only of type pdf,doc,docx.');
            }
            var f = this.files[0]

            if (f.size > 10000000 || f.fileSize > 10000000)
            {
                alert("Allowed file size less than 10 MB.")
                this.value = null;
            }
            
            
            
        });
        var _URL = window.URL || window.webkitURL;
    var flag = true; 
        
        $("#profile_pic").change(function(e) {
           
        var file, img;
        var ext = this.files[0].name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert('Only JPG, PNG or GIF files are allowed');
            $("#profile_pic").val('');
        } else {
            
            if (file = this.files[0]) {
                img = new Image();
                img.onload = function() {
                    if (this.width >= 1900 && this.height >= 1200) {
                        var reader = new FileReader();
    console.log(reader)
            reader.onload = function(e) {
            $('#blah').css("display", "block");
                    $('#blah').attr('src', e.target.result);
                    $('#remove_image').css("display", "block");
            }

    reader.readAsDataURL(file);
                    } else {
                        //flag = false;
                        alert("Image height and width should be greater than 1900*1200");
                       // $('#blah').attr('src', './media/front/img/image-not-found.png');
                        $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                    }
                     
                };
                 img.src = _URL.createObjectURL(file);
                readURL(this);
              
            }
        }
    }); 
    

    });
</script> 





