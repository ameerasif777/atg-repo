@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')

<style>
    @media (max-width: 768px) {
    .users-conectry-nav > ul > li{
        width: 100%;;
    }
    }
</style>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/bootstrap-tokenfield.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/Frontend/js/job/tokenfield-typeahead.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/bootstrap-tokenfield.js')}}"></script>

<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                @include('Frontend.user.profile.accountsetting-left-section')

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <h6>My Account Summary</h6>
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active clearfix">
                        <div class="main-inner-tab-info clearfix">
                            <form id="account_setting" name="account_setting" method="post" action='{{url('/account-setting')}}'>
                                {{ csrf_field() }}
                                <dl class="dl-horizontal">  
                                    
                                    <dt style="white-space: normal;">Please tell us if you are a student, professional or a company<sup class="mandatory">*</sup> :</dt>
                                    <dd style="margin-bottom:35px;">
                                        <?php
                                        if (isset($arr_user_data['user_type']) && $arr_user_data['user_type'] != 'NULL') {
                                            $val = $arr_user_data['user_type'];
                                        } else {
                                            $val = '';
                                        }
                                        ?>
                                        @if($finalData['ut_update'] == 'disabled')
                                        <input type="hidden" name="user_type" value="{{$val}}">
                                        @endif
                                        <select {{ str_replace('disabled','readonly' ,$finalData['ut_update'])}} data-val="{{$val}}" class="form-control" name="user_type" id="user_type">
                                            <option value="">Select</option>
                                            <option <?php if ("1" == $val) { ?>selected="selected"<?php } ?> value="1">Student</option>
                                            <option <?php if ("3" == $val) { ?>selected="selected"<?php } ?> value="3">Professional</option>
                                            <option <?php if ("4" == $val) { ?>selected="selected"<?php } ?> value="4">Company</option>
                                        </select>
                                        @if($finalData['ut_update'] == 'disabled')
                                        <span id='usr_ut_msg' class="hide" style="color:red"> {{$finalData['ut_date_diff']}} </span>
                                        @else 
                                        <span id='usr_ut_msg' class="hide" style="color:lightslategrey">This field can only be edited once during six months. If you change it now, you can NOT change it for another six months.</span>
                                        @endif
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Username<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <input type="text" id="user_name" name="user_name" value="{{$arr_user_data['user_name']}}" placeholder="Username" onkeyup="nospaces(this)">
                                        <span class="usr_msg1" style='text-align:center;color:red;font-size:12px;'></span>
                                    </dd>
                                    <input type="hidden" id="old_username" name="old_username" value="{{$arr_user_data['user_name']}}" > 
                                </dl>
                                <dl class="dl-horizontal">
                                    
                                    <dt>First Name<sup class="mandatory">*</sup> :</dt>
                                    <dd style="margin-bottom:35px;" id="first_name_dd"><input type="text" {{ str_replace('disabled','readonly' ,$finalData['fn_update'])}} id="first_name" name="first_name" value="{{$arr_user_data['first_name']}}" data-val="{{$arr_user_data['first_name']}}" placeholder="First Name">
                                    @if($finalData['fn_update'] == 'disabled')
                                    <span id='usr_fname_msg' class="hide" style="color:red"> {{$finalData['fn_date_diff']}}</span>
                                    @else 
                                    <span id='usr_fname_msg' class="hide" style="color:lightslategrey">This field can only be edited once during six months. If you change it now, you can NOT change it for another six months.</span>
                                    @endif
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    
                                    <dt>Last Name<sup class="mandatory">*</sup> :</dt>
                                    <dd style="margin-bottom:35px;"><input type="text" {{str_replace('disabled','readonly' ,$finalData['ln_update'])}} id="last_name" name="last_name" value="{{$arr_user_data['last_name']}}" data-val="{{$arr_user_data['last_name']}}" placeholder="Last Name">@if($finalData['ln_update'] == 'disabled')
                                    <span id='usr_lname_msg' class="hide" style="color:red"> {{$finalData['ln_date_diff']}} </span>
                                    @else 
                                    <span id='usr_lname_msg' class="hide" style="color:lightslategrey">This field can only be edited once during six months. If you change it now, you can NOT change it for another six months.</span>
                                    @endif</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Email address<sup class="mandatory">*</sup> :</dt>
                                    <dd><input type="text" <?php if(isset($arr_user_data['email']) && $arr_user_data['email']!=''){ ?> readonly="readonly" <?php }?> id="email" name="email" value="{{$arr_user_data['email']}}"   placeholder="Email address"></dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Mobile Number :</dt>
                                    <dd><input type="text" id="mob_no" name="mob_no" value="{{$arr_user_data['mob_no']}}" placeholder="Enter your mobile. This remains private to you."></dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Phone Number :</dt>
                                    <dd><input type="text" id="phone_no" name="phone_no" value="{{$arr_user_data['phone_no']}}"   placeholder="Enter secondary phone. This remains private to you."></dd>

                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Location<sup class="mandatory">*</sup> :</dt>
                                    <dd><input type="text" id="location" class="location" name="location" value="{{$arr_user_data['location']}}" placeholder="Enter your location. Helps us provide you better updates."></dd>
                                    <input type="hidden"  name="latitude"   class="form-control" value="{{ $arr_user_data['latitude']}}">
                                    <input type="hidden"  name="longitude"   class="form-control" value="{{ $arr_user_data['longitude']}}">
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>About me<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                    <textarea maxlength="1000" name="about_me" id="about_me" style="margin: 0px; resize: none; width: 100%; height: 56px;" onKeyDown="limitText(this.form.about_me,this.form.countdown,1000);" 
                                    onKeyUp="limitText(this.form.about_me,this.form.countdown,1000);">{{trim($arr_user_data['about_me'])}}</textarea>
                                    </dd>
                                    <dd>
                                    <font size="1">(Maximum characters: 1000)
                                    You have
                                    <input readonly type="text" style="width:30px; height: 20px;" name="countdown" size="3" value="1000"> characters left.</font>
                                    <!-- <input type="text" id="about_me" name="about_me" value="{{$arr_user_data['about_me']}}"   placeholder="A one-liner about yourself."> -->
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Tagline :</dt>
                                    <dd><input type="text" id="tagline" name="tagline" value="{{$arr_user_data['tagline']}}"   placeholder="What are your various interests?"></dd>
                                </dl>
                                <dl class="dl-horizontal">  
                                    <dt>Profession<sup class="mandatory">*</sup> :</dt>
                                    <dd><input type="text" id="profession" name="profession" value="{{$arr_user_data['profession']}}"   placeholder="What's your primary profession?"></dd>
                                </dl>
                                @if($val != 4)
                                <dl class="dl-horizontal">
                                    <dt>Gender<sup class="mandatory">*</sup> :</dt>
                                    <dd>
                                        <label class="radio-inline"><input   value="0"   type="radio" name="gender" @if($arr_user_data['gender'] == 0)  checked="checked" @endif >Male</label>
                                        <label class="radio-inline"><input  value="1" type="radio" name="gender"  @if ($arr_user_data['gender'] == 1) checked="checked" @endif>Female</label>
                                    </dd>
                                </dl>
                                
                                <dl class="dl-horizontal">  
                                    <dt>Birth Date<sup class="mandatory">*</sup> :</dt>
                                    <dd><input type="text" readonly="" id="dob" name="dob" value="{{ date('m/d/Y',strtotime($arr_user_data['user_birth_date']))}}"   placeholder="Date Of Birth"></dd>
                                </dl>
                                @endif
                                <dl class="dl-horizontal">  
                                    <dt>Google Crawalable :  <span id="mark" class="customized_tooltip" data-toggle="tooltip" data-placement="bottom" title="Do you want your profile to appear in Google searches?">?</span></dt>
                                    <dd><input type="checkbox" id="is_google_crawalable" name="is_google_crawalable" value="1" @if($arr_user_data['is_google_crawalable'] == 1) checked="checked" @endif></dd>
                                    <script> 
                                        jQuery.fn.bstooltip = jQuery.fn.tooltip; 
                                        $('#mark').bstooltip(); 
                                    </script>
                                </dl>
                                @if($val == 4)
                                    <dl class="dl-horizontal">
                                        <dt>Website :</dt>
                                        <dd><input type="url" id="website" name="website" value="{{$arr_user_data['website']}}" placeholder="http://yourcompany.com"></dd>
                                    </dl>

                                    <dl class="dl-horizontal">
                                        <dt>Facebook Page :</dt>
                                        <dd><input type="url" id="facebook_page" name="facebook_page" value="@if($arr_user_data['facebook_page'] == '') https://facebook.com/ @else {{$arr_user_data['facebook_page']}} @endif" placeholder="Your Company Facebook Page"></dd>
                                    </dl>

                                    <dl class="dl-horizontal">
                                        <dt>Twitter Page :</dt>
                                        <dd><input type="url" id="twitter_page" name="twitter_page" value="@if($arr_user_data['twitter_page'] == '') https://twitter.com/ @else {{$arr_user_data['twitter_page']}} @endif" placeholder="Your Company Twiiter Page"></dd>
                                    </dl>
                                @else
                                    <input type="hidden" name="website" value="">
                                    <input type="hidden" name="facebook_page" value="">
                                    <input type="hidden" name="twitter_page" value="">
                                @endif
                                <button class="save-btn btn btn_account_setting" type="submit" name="btn_account_setting" id="btn_account_setting">SAVE</button>
                            </form>
                        </div>
                    </div>

                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">

                        </div>
                    </div>
                    @if($arr_user_data['user_type'] != 4)
                    <div class="bhoechie-tab-content tem-capter clearfix">

                    </div>
                    <div class="bhoechie-tab-content ">

                    </div>
                    @endif
                    @if($arr_user_data['user_type'] == 1)
                    <div class="bhoechie-tab-content">

                    </div>

                    <div class="bhoechie-tab-content">

                    </div>
                    @endif
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">

                        </div>
                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>

                    <div class="bhoechie-tab-content">

                    </div>
                    <div class="bhoechie-tab-content">

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="connected-countions col-lg-9 col-lg-offset-3 col-xs-12 col-sm-12">
                    <h6>My Account Summary</h6>
                    <div class="main-sel-secentry clearfix">
                        <p>Bring your reputation with you! Link accounts from these sites to tell us more about you.</p>
                        <div class="users-conectry-nav">
                            <ul class="clearfix">
                                @if($arr_user_data['fb_id'] != '')
                                <li class="selcted-sol"><a href="javascript:void(0)"><i class="fa fa-facebook-f"></i> Facebook</a> @if($arr_user_data['first_social_login'] != 1)<div class="remores"><a href="{{url('/remove-social-connection/fb')}}"><i class="fa fa-remove"></i></a></div>@endif</li>
                                @else
                                <li><a href="{{url('/facebook/1')}}"><i class="fa fa-facebook-f"></i> Facebook</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-remove"></i></a></div></li>
                                @endif
                                @if($arr_user_data['twitter_id'] != '')
                                <li class="selcted-sol"><a href="javascript:void(0)"><i class="fa fa-twitter"></i> Twitter</a> @if($arr_user_data['first_social_login'] != 2)<div class="remores"><a href="{{url('/remove-social-connection/twitter')}}"><i class="fa fa-remove"></i></a></div>@endif</li>
                                @else
                                <li><a href="{{url('/twitter/1')}}"><i class="fa fa-twitter"></i> Twitter</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-remove"></i></a></div></li>
                                @endif
                                @if($arr_user_data['google_id'] != '')
                                <li class="selcted-sol"><a href="javascript:void(0)"><i class="fa fa-google-plus"></i> Google+</a> @if($arr_user_data['first_social_login'] != 3)<div class="remores"><a href="{{url('/remove-social-connection/google')}}"><i class="fa fa-remove"></i></a></div>@endif</li>
                                @else
                                <li><a href="{{url('/gmail/1')}}"><i class="fa fa-google-plus"></i> Google+</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-remove"></i></a></div></li>
                                @endif
                                @if($arr_user_data['linkdin_id'] != '')
                                <li class="selcted-sol"><a href="javascript:void(0)"><i class="fa fa-linkedin"></i> LinkedIn</a> @if($arr_user_data['first_social_login'] != 4)<div class="remores"><a href="{{url('/remove-social-connection/linkdin')}}"><i class="fa fa-remove"></i></a></div>@endif</li>
                                @else
                                <li><a href="{{url('/linkedin/1')}}"><i class="fa fa-linkedin"></i> LinkedIn</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-remove"></i></a></div></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Frontend/js/setting/account-setting.js')}}"></script>
<script src="{{url('/assets/Frontend/js/team/team.js')}}"></script>
<script src="{{url('/assets/Frontend/js/job/job.js')}}"></script>
<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Backend/css/jquery.datetimepicker.css')}}"></script>
<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
        limitCount.value = limitNum - limitField.value.length;
    }
}
</script>
<script>
$(function () {
    $(".location").geocomplete({
        detailsAttribute: "data-geo",
        types: [
            'geocode'
        ]
    }).bind("geocode:result", function (event, result) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': result.formatted_address}, function (results, status) {
            var location = results[0].geometry.location;
            var lat = location.lat();
            var lng = location.lng();
            $("input[name=latitude]").val(lat);
            $("input[name=longitude]").val(lng);
        });
    });
    $('#skill').tokenfield({
        autocomplete: {
            delay: 100
        },
        showAutocompleteOnFocus: true
    });
});

$(function () {
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-m-d',
        yearRange: '1980:2016',
    });
});

$(document).ready(function () {
    $("img").error(function () {
        $(this).attr("src", "{{url('/assets/Backend/img/image-not-found.png')}}");
    });
    @if($finalData['fn_update'] == 'disabled')
        $('#first_name').click(function(e){
            e.stopPropagation();
            $('#usr_fname_msg').removeClass('hide');
        });
        
        $("body").click(function(){
            $('#usr_fname_msg').addClass('hide');
        });
    @else    
        $('#first_name').focus(function(){ $('#usr_fname_msg').removeClass('hide'); });
        $('#first_name').blur(function(){
            //e.stopPropagation();
            console.log($(this).data('val') +'=='+ $(this).val());
            if($(this).data('val') == $(this).val()){
                $('#usr_fname_msg').addClass('hide');
            }
            else
            {
                $('#usr_fname_msg').removeClass('hide');
            }
        });
        
        
    @endif  

    @if($finalData['ln_update'] == 'disabled')
        $('#last_name').click(function(e){
            e.stopPropagation();
            $('#usr_lname_msg').removeClass('hide');
        });
        $("body").click(function(){
            $('#usr_lname_msg').addClass('hide');
        });

    @else
        $('#last_name').focus(function(){ $('#usr_lname_msg').removeClass('hide'); });
        $('#last_name').blur(function(){
            //e.stopPropagation();
            if($(this).data('val') == $(this).val()){
                $('#usr_lname_msg').addClass('hide');
            }
            else
            {
                $('#usr_lname_msg').removeClass('hide');
            }
        });
    @endif  

    @if($finalData['ln_update'] == 'disabled')
        $('#user_type').click(function(e){
            e.stopPropagation();
            $('#usr_ut_msg').removeClass('hide');
        });
        
        $("body").click(function(){
            $('#usr_ut_msg').addClass('hide');
        });
    @else
        $('#user_type').focus(function(){ $('#usr_ut_msg').removeClass('hide'); });
        $('#user_type').blur(function(){
            if($(this).data('val') == $(this).val()){
                $('#usr_ut_msg').addClass('hide');
            }
            else
            {
                $('#usr_ut_msg').removeClass('hide');
            }
        });
    @endif
    
});

</script>
<script>
    function del(team_id) {
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: 'delete-team/' + btoa(team_id),
                data: '',
                dataType: 'json'
            }).success(function (msg) {
                location.reload();
            });
        }
        return false;
    }
    function rem(resume_id) {
        if (confirm("Are you sure want to remove this resume?")) {
            $.ajax({
                url: 'delete-resume/' + btoa(resume_id),
                data: '',
                dataType: 'json'
            }).success(function (msg) {
                location.reload();
            })
        }
        return false;
    }
    function remov(job_id) {
        if (confirm("Are you sure want to remove this saved job?")) {
            $.ajax({
                url: 'delete-saved-job/' + btoa(job_id),
                data: '',
                dataType: 'json'
            }).success(function (msg) {
                location.reload();
            })
        }
        return false;
    }

    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#profile_pic").prop("value", "");
    }

</script> 
<script>
    
    function checkResumeCount() {
        $.ajax({
            url: 'resume-count',
            data: '',
            dataType: 'json'
        }).success(function (msg) {
            var resume_count = msg.resume_count[0].count
            if (resume_count == 3) {
                if (confirm("Are you sure want to remove first uploaded resume?")) {
                    $('#upload_resumes').submit();
                } else {
                    return false;
                }
            }
        })
    }

    $(document).ready(function () {
        $('#file').change(function () {
            var val = $(this).val();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf)$");
            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please upload a file only of type pdf,doc,docx.');
            }
            var f = this.files[0]

            if (f.size > 10000000 || f.fileSize > 10000000)
            {
                alert("Allowed file size less than 10 MB.")
                this.value = null;
            }
        });
        var _URL = window.URL || window.webkitURL;
        var flag = true;

        $("#profile_pic").change(function (e) {
            var file, img;
            var ext = this.files[0].name.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert('Only JPG, PNG or GIF files are allowed');
                $("#profile_pic").val('');
            } else {
                if (file = this.files[0]) {
                    img = new Image();
                    img.onload = function () {
                        if (this.width >= 1900 && this.height >= 1200) {
                            var reader = new FileReader();
                            console.log(reader)
                            reader.onload = function (e) {
                                $('#blah').css("display", "block");
                                $('#blah').attr('src', e.target.result);
                                $('#remove_image').css("display", "block");
                            }

                            reader.readAsDataURL(file);
                        } else {
                            alert("Image height and width should be greater than 1900*1200");
                            $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });
    });

    function nospaces(t) {
        if (t.value == '') {
            $('#btn_account_setting').removeAttr('disabled');
        } else if (t.value.match(/\s/g)) {
            t.value = t.value.replace(/\s/g, '');
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Username cannot contain space');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else if (/^[a-zA-Z0-9 _]*$/.test(t.value) == false) {
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Please do not enter special characters');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else if ($.trim(t.value).length < 4) {
            $('.usr_msg1').html('');
            $('.usr_msg1').html('Please Enter more than 4 character');
            $('.btn_account_setting').attr('disabled', 'true');
            return false;
        } else {
            $('.usr_msg1').html('');
            var base_url = jQuery("#base_url").val();
            var key = $('#user_name').val();
            $.ajax({
                url: base_url + '/verify_username',
                dataType: 'json',
                data: {'key': key, id: "{{ $arr_user_data['id'] }}"},
                method: 'post',
                success: function (response) {
                    if (response == '0'){
                        $('.usr_msg1').html('Good choice, but this username is already taken. Please try another.');
                        $('#btn_account_setting').attr('disabled', 'true');
                    } else if (response == '1') {
                        $('.usr_msg1').html('Username is unavailable. Please try another.');
                        $('#btn_account_setting').attr('disabled', 'true');
                    } else {
                        $('.usr_msg1').html('');
                        $('#btn_account_setting').removeAttr('disabled');
                    } 
                }
            });
        }
    }


    $("#facebook_page").keydown(function(e) {
        var oldvalue=$(this).val();
        var field=this;
        setTimeout(function () {
            if(field.value.indexOf('https://facebook.com/') !== 0) {
                $(field).val(oldvalue);
            } 
        }, 1);
    });
    $("#twitter_page").keydown(function(e) {
        var oldvalue=$(this).val();
        var field=this;
        setTimeout(function () {
            if(field.value.indexOf('https://twitter.com/') !== 0) {
                $(field).val(oldvalue);
            } 
        }, 1);
    });

    if('{{ url()->previous() }}' == '{{url('/')}}/profile'){
        var header_height= $( "header" ).height();
        if ($(window).width() < 450) { 
            if(header_height>50)
            {
                header_height = parseInt(header_height)+parseInt('620');
            }
        } else if (($(window).width() < 600) && ($(window).width() >= 450)) {
            if(header_height>50)
            {
                header_height = parseInt(header_height)+parseInt('550');
            }
        } else { 
            if(header_height>50)
            {
                header_height = parseInt(header_height)+parseInt('75');
            }
        }  
        
        $("html, body").animate({  scrollTop: $("#about_me").offset().top-(header_height)}, 1000);
    }

</script>


<style>
    /* Tooltip */
    .customized_tooltip + .tooltip > .tooltip-inner {
        background-color: #fff; 
        color: #000; 
        border: 1px solid #aaa; 
        padding: 12px;
        font-size: 15px;
        white-space: normal;
        box-shadow: -2px 2px 0 #222;
    }
    /* Tooltip on bottom */
    .customized_tooltip + .tooltip.bottom > .tooltip-arrow {
        border-bottom: 10px solid #ccc;
        top: -5px;
    }
    /* Tooltip on top 
    .customized_tooltip + .tooltip.top > .tooltip-arrow {
        border-bottom: 10px solid #ccc;
    }*/
    /* Tooltip on left 
    .customized_tooltip + .tooltip.left > .tooltip-arrow {
        border-bottom: 10px solid #ccc;
    }*/
    /* Tooltip on right 
    .customized_tooltip + .tooltip.right > .tooltip-arrow {
        border-bottom: 10px solid #ccc;
    }*/
</style>
