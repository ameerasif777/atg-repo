@include('Frontend.includes.head')
@include('Frontend.includes.session-message')


<section class="contact-info-sec">
    <div class="container text-center">
        <img src="{{ url('/public/assets/Frontend/img/404-error-page-not-found.png') }}">
        <div class="logo">
            <?php if (Request::segment(1) != 'signup3' && Request::segment(1) != 'signup3') { ?>
                <a href="{!!url('/')!!}"  ><img src="{{ url('/public/assets/Frontend/img/logo-in.png') }}" alt="SITE-LOGO"/></a>
            <?php } ?>
        </div>
    </div>
</section>


@include('Frontend.includes.footer')
