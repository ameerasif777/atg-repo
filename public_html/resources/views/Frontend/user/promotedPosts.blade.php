<div id="myCarousel" class="carousel slide" style="height:200px;" data-ride="carousel">
<!-- Indicators -->
<ol class="carousel-indicators">
  @foreach($postArray as $key => $value)
  @if($key==0)
    <li data-target="#myCarousel" data-slide-to="{{$key}}" class="active"></li>
  @else
    <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>
  @endif
  @endforeach
</ol>

<!-- Wrapper for slides -->
<div class="carousel-inner" style="height:100%">
    @foreach($postArray as $key => $value)
    @if($key==0)
    <div class="item active" style="position:absolute; width:100%; height:100%">
    @else
    <div class="item" style="position:absolute; width:100%; height:100%">
    @endif
    <a href='{{url('/')}}/view-{{$value['post_type']}}/{{preg_replace('/[^A-Za-z0-9\s]/', '', $value['title'])}}-{{$value['post_id_fk']}}'>
            <img href='{{url('/')}}/view-{{$value['post_type']}}/{{preg_replace('/[^A-Za-z0-9\s]/', '', $value['title'])}}-{{$value['post_id_fk']}}' style="height: 100%; margin-left: auto; margin-right: auto;"  src="{{url('/')}}/public/assets/Frontend/images/{{$value['post_type']}}_image/{{$value['profile_image']}}">
            <div class="carousel-caption">
              <h3>{{$value['title']}}</h3>
            </div>
            
    </a>
    </div>
    @endforeach
  

  
</div>

<!-- Left and right controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right"></span>
  <span class="sr-only">Next</span>
</a>
</div>
<script>
  !function ($) {
    $(function(){
      $('#myCarousel').carousel()
    })
  }(window.jQuery)
</script>