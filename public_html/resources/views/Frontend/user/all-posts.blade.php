@if(count($arr_all_records)>0)
@foreach($arr_all_records as $value)
<?php
$user_profile_image = config('profile_path') . $value->user_id . '/thumb/' . $value->profile_picture;
if (Storage::exists($user_profile_image) && $value->profile_picture != '') {
    $profile_picture = config('profile_pic_url') . $value->user_id . '/thumb/' . $value->profile_picture;
} else {
    $profile_picture = config('img').'pro-2.png';
}
?>
@if($value->type == 'Article')
<article class='white-panel' style= 'display: block; '>
    
    <div class='atg-world-one ' >
        <div class="media otg-found header">
            <div class="media-left">
                <a href="{{url("/")}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}">
                    <h4 class="media-heading">{{ucfirst($value->title)}}</h4>
                </a>      
            </div>          
<!--            <div  class="media-right options dropdown">
                <a class="open-menu" title="Open" onclick="" class="dropdown-toggle"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </a> 
               <div class="option-menu dropdown-menu"  aria-labelledby="open-menu">
                    <ul>
                        <li class="dropdown-item"><a href="#">Hide Story</a></li>
                        <li class="dropdown-item"><a href="#">Show fewer stories like this</a></li>
                        <li class="dropdown-item"><a href="#">Report Story</a></li>
                    </ul>
               </div>
                
           </div> -->
            
        </div>
        <?php
            if($value->cover_type==0){
             $image_path = config('feature_image_path').'article_image/thumb/' . trim($value->image);
             if (Storage::exists($image_path) && $value->image != '') {
                 $post_image_path = config('feature_pic_url').'article_image/thumb/' . trim($value->image);
             } else {
                 //$post_image_path = config('img').'image-not-found2.jpg';
                 $post_image_path = '';
             }
            }
            elseif($value->cover_type==1){
                $post_image_path =trim($value->image);
            }
        ?>
            @if($value->cover_type==0 || $value->cover_type==1)    
                @if($post_image_path != '')
                
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><img src="{{$post_image_path}}"/></a>
                    </div> 
                @endif
            @elseif($value->cover_type==2 || $value->cover_type==3)
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-article/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"> <div class="embvideo"><iframe width="100%" height="100%" src="{!!$value->image!!}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div> </a>
                    </div> 
    
            
            @endif



        <div class="media otg-found footer" >
            <div class="media-left">
                <ul class="media-user">
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><img alt="users" src="{{$profile_picture}}"></a></li>
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><div class="pname">@if($value->user_name == ''){{ucwords($value->first_name)}} {{ucwords($value->last_name)}}@else{{strtolower($value->user_name)}}@endif</div></a><br><div class="ptype">@if(isset($value->promoted))Sponsored @endif{{$value->type}}</div></li>
                </ul>       
            </div>
            <div class="media-right thumb-up-down">
                @include('Frontend.includes.post_detail_share_fx', [ 'feature' => "article"])
                <input type="hidden" value="{{"View this article on ATG:-"}}{{$value->title}}" id="article_title" class="title">             
                <input type= "hidden" value = "{{url("/")}}/view-article/-{{$value->id}}" id = "page_link">
                <a class="{{($value->user_upvote_count == 0) ? 'fa fa-thumbs-o-up' : 'fa fa-thumbs-up'}}" id= "article_icon_vote_{{$value->id}}"  title="Up Vote" onclick="upvoteDownvoteArticle('{{$value->id}}', '0', '1');"> </a>  
                <a id= "share_{{$value->id}}"  title="Share" onclick="share_div_show('share_{{$value->id}}')"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                <a class="{{($value->user_downvote_count == 0) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-down'}}" id= "article_icon_down_{{$value->id}}" title="Down Vote" onclick="upvoteDownvoteArticle('{{$value->id}}', '1', '2');" ></a>
             <div class="share_div">
                 @include('Frontend.includes.post_detail_share')
        </div>
             
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'Qrious')
<article class='white-panel' style= 'display: block;'>
    <div class="atg-world-one">
        <div class="media otg-found header">
            <div class="media-left">
                <a href="{{url("/")}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}">
                    <h4 class="media-heading">{{ucfirst($value->title)}}</h4>
                </a>      
            </div>          
<!--            <div  class="media-right options dropdown">
                <a class="open-menu" title="Open" onclick="" class="dropdown-toggle"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </a> 
               <div class="option-menu dropdown-menu"  aria-labelledby="open-menu">
                    <ul>
                        <li class="dropdown-item"><a href="#">Hide Story</a></li>
                        <li class="dropdown-item"><a href="#">Show fewer stories like this</a></li>
                        <li class="dropdown-item"><a href="#">Report Story</a></li>
                    </ul>
               </div>
                
           </div> -->
           
        </div>
        @if($value->upvote_count != '0')
        <div class="media media-body likesbody">
           {{$value->upvote_count}} likes           
        </div> 
        @endif
        <?php
            if($value->cover_type==0){
                $image_path = config('feature_image_path').'question_image/thumb/' . trim($value->image);
                $image_path2 = config('feature_image_path').'qrious_image/thumb/' . trim($value->image);
                
                if (Storage::exists($image_path) && $value->image != '') {
                    $post_image_path = config('feature_pic_url').'question_image/thumb/' . trim($value->image);
                } elseif (Storage::exists($image_path2) && $value->image != '') {
                    $post_image_path = config('feature_pic_url').'qrious_image/thumb/' . trim($value->image);
                }else {
                    //$post_image_path = config('img').'image-not-found2.jpg';
                    $post_image_path = '';
                }
                
            }
            elseif($value->cover_type==1){
                $post_image_path=trim($value->image);
            }
             ?>
             @if($value->cover_type==0 || $value->cover_type==1)    
                @if($post_image_path != '')
                
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><img src="{{$post_image_path}}"/></a>
                    </div> 
                @endif
            @elseif($value->cover_type==2 || $value->cover_type==3)
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-question/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"> <div class="embvideo"><iframe width="100%" height="100%" src="{!!$value->image!!}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div> </a>
                    </div> 
    
                
            @endif


        <div class="media otg-found footer" >
            <div class="media-left">
                <ul class="media-user">
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><img alt="users" src="{{$profile_picture}}"></a></li>
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><div class="pname">@if($value->user_name == ''){{ucwords($value->first_name)}} {{ucwords($value->last_name)}}@else{{strtolower($value->user_name)}}@endif</div></a><br><div class="ptype">@if(isset($value->promoted))Sponsored @endif{{$value->type}}</div></li>
                </ul>        
            </div>
            <div class="media-right thumb-up-down">
                @include('Frontend.includes.post_detail_share_fx', [ 'feature' => "qrious"])
                 <input type="hidden" value="{{"View this qrious on ATG:-"}}{{$value->title}}" id="qrious_title" class="title">                 
                <input type= "hidden" value = "{{url("/")}}/view-qrious/-{{$value->id}}" id = "page_link">
                <a class="{{($value->user_upvote_count == 0) ? 'fa fa-thumbs-o-up' : 'fa fa-thumbs-up'}}" id= "qrious_icon_vote_{{$value->id}}"  title="Up Vote" onclick="upvoteDownvoteQrious('{{$value->id}}', '0', '1');"> </a>  
                 <a id= "share_{{$value->id}}"  title="Share" onclick="share_div_show('share_{{$value->id}}')"><i class="fa fa-share-alt" aria-hidden="true"></i></a> 
                <a  class="{{($value->user_downvote_count == 0) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-down'}}" id= "qrious_icon_down_{{$value->id}}" title="Down Vote" onclick="upvoteDownvoteQrious('{{$value->id}}', '1', '2');"></a>
                 
                 <div class="share_div">
                 @include('Frontend.includes.post_detail_share')
                 </div>
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'Job')
<article class='white-panel' style= 'display: block;'>
    <div class="atg-world-one">
        <div class="media otg-found header">
            <div class="media-left">
                <a href="{{url("/")}}/view-job/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}">
                    <h4 class="media-heading job-media-heading">{{ucfirst($value->title)}}</h4>
                </a>      
            </div>          
<!--            <div  class="media-right options dropdown">
                <a class="open-menu" title="Open" onclick="" class="dropdown-toggle"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </a> 
               <div class="option-menu dropdown-menu"  aria-labelledby="open-menu">
                    <ul>
                        <li class="dropdown-item"><a href="#">Hide Story</a></li>
                        <li class="dropdown-item"><a href="#">Show fewer stories like this</a></li>
                        <li class="dropdown-item"><a href="#">Report Story</a></li>
                    </ul>
               </div>
                
           </div> -->
           
        </div>
        <div class="media descr">
           {{ substr(strip_tags($value->description), 0, 100) }}...        
        </div>
        <div class="media descr">
           <i class="fa fa-map-marker" aria-hidden="true"></i> {{substr($value->job_location, 0, 15)}}...          
        </div> 
        <div class="media otg-found footer" >
            <div class="media-left">
                <ul class="media-user">
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><img alt="users" src="{{$profile_picture}}"></a></li>
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><div class="pname">@if($value->user_name == ''){{ucwords($value->first_name)}} {{ucwords($value->last_name)}}@else{{strtolower($value->user_name)}}@endif</div></a><br><div class="ptype">@if(isset($value->promoted))Sponsored @endif{{$value->type}}</div></li>
                </ul>        
            </div>
            <div class="media-right thumb-up-down">
               @include('Frontend.includes.post_detail_share_fx', [ 'feature' => "job"])
                <input type="hidden" value="{{"View this job on ATG:-"}}{{$value->title}}" id="job_title" class="title">             
                <input type= "hidden" value = "{{url("/")}}/view-job/-{{$value->id}}" id = "page_link">
                <a id= "message_{{$value->id}}"  title="Message" onclick=""><i class="fa fa-envelope" aria-hidden="true"></i></a> 
                <a class="{{($value->user_upvote_count == 0) ? 'fa fa-thumbs-o-up' : 'fa fa-thumbs-up'}}" id= "job_icon_vote_{{$value->id}}"  title="Up Vote" onclick="upvoteDownvoteJob('{{$value->id}}', '0', '1');"> </a>  
                  <a id= "share_{{$value->id}}"  title="Share" onclick="share_div_show('share_{{$value->id}}')"><i class="fa fa-share-alt" aria-hidden="true"></i></a> 
                <a  class="{{($value->user_downvote_count == 0) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-down'}}" id= "job_icon_down_{{$value->id}}" title="Down Vote" onclick="upvoteDownvoteJob('{{$value->id}}', '1', '2');"></a>
                <div class="share_div">
                 @include('Frontend.includes.post_detail_share')
                 </div>
            </div>
        </div>
    </div>
</article>
@elseif($value->type == 'Education')
<article class='white-panel' style= 'display: block;'>
    <div class="atg-world-one">
        <div class="media otg-found header">
            <div class="media-left">
                <a href="{{url("/")}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}">
                    <h4 class="media-heading job-media-heading">{{ucfirst($value->title)}}</h4>
                </a>      
            </div>          
<!--            <div  class="media-right options dropdown">
                <a class="open-menu" title="Open" onclick="" class="dropdown-toggle"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </a> 
               <div class="option-menu dropdown-menu"  aria-labelledby="open-menu">
                    <ul>
                        <li class="dropdown-item"><a href="#">Hide Story</a></li>
                        <li class="dropdown-item"><a href="#">Show fewer stories like this</a></li>
                        <li class="dropdown-item"><a href="#">Report Story</a></li>
                    </ul>
               </div>
                
           </div> -->
           
        </div>
        <div class="media descr">
           {{ substr(strip_tags($value->description), 0, 100) }}...        
        </div>
        <?php
            if($value->cover_type==0){
                $image_path = config('feature_image_path').'education_image/thumb/' . trim($value->image);
                if (Storage::exists($image_path) && $value->image != '') {
                    $post_image_path = config('feature_pic_url').'education_image/thumb/' . trim($value->image);
                } else {
                    //$post_image_path = config('img').'image-not-found2.jpg';
                    $post_image_path = '';
                }
            }
            elseif($value->cover_type==1){
                $post_image_path=trim($value->image);
            }
             ?>
            @if($value->cover_type==0 || $value->cover_type==1)    
                @if($post_image_path != '')
                
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><img src="{{$post_image_path}}"/></a>
                    </div> 
                @endif
            @elseif($value->cover_type==2 || $value->cover_type==3)
                    <div class="media media-body imagebody">
                        <a href="{{url("/")}}/view-education/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"> <div class="embvideo"><iframe width="100%" height="100%" src="{!!$value->image!!}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div> </a>
                    </div> 
    
                
            @endif
        <div class="media otg-found footer" >
            <div class="media-left">
                <ul class="media-user">
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><img alt="users" src="{{$profile_picture}}"></a></li>
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><div class="pname">@if($value->user_name == ''){{ucwords($value->first_name)}} {{ucwords($value->last_name)}}@else{{strtolower($value->user_name)}}@endif</div></a><br><div class="ptype">@if(isset($value->promoted))Sponsored @endif{{$value->type}}</div></li>
                </ul>     
            </div>
            <div class="media-right thumb-up-down">
                 
                @if($value->onlineTicket == '1')
                   
                    <a id= "buy_{{$value->id}}"  title="Buy Ticket" href="{{url("/")}}/view-{{strtolower($feature_type)}}/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><i class="fa fa-ticket" aria-hidden="true"></i></a>
                @endif
                @include('Frontend.includes.post_detail_share_fx', [ 'feature' => "education"])
                 <input type="hidden" value="{{"View this education on ATG:-"}}{{$value->title}}" id="education_title" class="title">             <input type= "hidden" value = "{{url("/")}}/view-education/-{{$value->id}}" id = "page_link">
                <a class="{{($value->user_upvote_count == 0) ? 'fa fa-thumbs-o-up' : 'fa fa-thumbs-up'}}" id= "education_icon_vote_{{$value->id}}"  title="Up Vote" onclick="upvoteDownvoteEducation('{{$value->id}}', '0', '1');"> </a>  
                 <a id= "share_{{$value->id}}"  title="Share" onclick="share_div_show('share_{{$value->id}}')"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                <a  class="{{($value->user_downvote_count == 0) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-down'}}" id= "education_icon_down_{{$value->id}}" title="Down Vote" onclick="upvoteDownvoteEducation('{{$value->id}}', '1', '2');"></a>
             <div class="share_div">
                 @include('Frontend.includes.post_detail_share')
                
                 </div>
            </div>
        </div>
    </div>
</article>
@else
<?php
if ($value->type == 'Event') {
    $link = url("/") . '/view-event/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
    $post_id = $value->id;
    $feature_type = "Event";
    if($value->cover_type==0){
        $image_path = config('feature_image_path').'event_image/thumb/' . trim($value->image);
        if (Storage::exists($image_path) && $value->image != '') {
            $post_image_path = config('feature_pic_url').'event_image/thumb/' . trim($value->image);
        } else {
            //$post_image_path = config('img').'image-not-found2.jpg';
            $post_image_path = '';
        }
    }
    elseif($value->cover_type==1){
        $post_image_path=trim($value->image);
    }
    $rep_decription = strip_tags($value->rep_decription);
} elseif ($value->type == 'Meetup') {
    $link = url("/") . '/view-meetup/' . preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) . '-' . $value->id;
    $post_id = $value->id;
    $feature_type = "Meetup";
    if($value->cover_type==0){
        $image_path = config('feature_image_path').'meetup_image/thumb/' . trim($value->image);
        if (Storage::exists($image_path) && $value->image != '') {
            $post_image_path = config('feature_pic_url').'meetup_image/thumb/' . trim($value->image);
        } else {
            //$post_image_path = config('img').'image-not-found2.jpg';
            $post_image_path = '';
        }
    }
    elseif($value->cover_type==1){
        $post_image_path=trim($value->image);
    }
    $rep_decription = wordwrap($value->rep_decription, 10, " ", true);
}
?>
<article class='white-panel' style= 'display: block;'>
    <div class="atg-world-one">
        <div class="media otg-found header">
            <div class="media-left">
                <a href="{{url("/")}}/view-{{strtolower($feature_type)}}/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}">
                    <h4 class="media-heading job-media-heading">{{ucfirst($value->title)}}</h4>
                </a>      
            </div>          
<!--            <div  class="media-right options dropdown">
                <a class="open-menu" title="Open" onclick="" class="dropdown-toggle"  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                </a> 
               <div class="option-menu dropdown-menu"  aria-labelledby="open-menu">
                    <ul>
                        <li class="dropdown-item"><a href="#">Hide Story</a></li>
                        <li class="dropdown-item"><a href="#">Show fewer stories like this</a></li>
                        <li class="dropdown-item"><a href="#">Report Story</a></li>
                    </ul>
               </div>
                
           </div> -->
           
        </div>
        <div class="media descr">
         <i class="fa fa-calendar" aria-hidden="true"></i> Starts {{$value->start_time}}, {{date('F jS, Y',strtotime($value->start_date))}}&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-map-marker" aria-hidden="true"></i> {{substr($value->location, 0, 15)}}...          
        </div> 
        @if($value->cover_type==0 || $value->cover_type==1)    
            @if($post_image_path != '')
            
                <div class="media media-body imagebody">
                    <a href="{{url("/")}}/view-{{strtolower($feature_type)}}/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><img src="{{$post_image_path}}"/></a>
                </div> 
            @endif
        @elseif($value->cover_type==2 || $value->cover_type==3)
                <div class="media media-body imagebody">
                    <a href="{{url("/")}}/view-{{strtolower($feature_type)}}/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"> <div class="embvideo"><iframe width="100%" height="100%" src="{!!$value->image!!}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div> </a>
                </div> 

           
        @endif 
        
        <div class="media otg-found footer" >
            <div class="media-left">
                <ul class="media-user">
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><img alt="users" src="{{$profile_picture}}"></a></li>
                    <li><a href="{{url("/")}}/user-profile/{{base64_encode($value->user_id)}}"><div class="pname">@if($value->user_name == ''){{ucwords($value->first_name)}} {{ucwords($value->last_name)}}@else{{strtolower($value->user_name)}}@endif</div></a><br><div class="ptype">@if(isset($value->promoted))Sponsored @endif{{$value->type}}</div></li>
                </ul>       
            </div>
            <div class="media-right thumb-up-down">
                @if($value->onlineTicket == '1')
                
                
                
                <a id= "buy_{{$value->id}}"  title="Buy Ticket" href="{{url("/")}}/view-{{strtolower($feature_type)}}/<?php echo preg_replace('/[^A-Za-z0-9\s]/', '', $value->title) ?>-{{$value->id}}"><i class="fa fa-ticket" aria-hidden="true"></i></a>
                
                @endif 
                <a class="{{($value->user_upvote_count == 0) ? 'fa fa-thumbs-o-up' : 'fa fa-thumbs-up'}}" id= "upvote_post_icon_{{$post_id}}_{{$feature_type}}"  title="Up Vote" onclick="upvoteDownvote('{{$post_id}}', '0','{{$feature_type}}', '1');"> </a>  
                    <a id= "share_{{$value->id}}"  title="Share" onclick="share_div_show('share_{{$value->id}}')"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                <a  class="{{($value->user_downvote_count == 0) ? 'fa fa-thumbs-o-down' : 'fa fa-thumbs-down'}}" id= "downvote_post_icon_{{$post_id}}_{{$feature_type}}" title="Down Vote" onclick="upvoteDownvote('{{$post_id}}', '1','{{$feature_type}}', '2');"></a>
                 <div class="share_div">
                 @include('Frontend.includes.post_detail_share')
                 @include('Frontend.includes.post_detail_share_fx', [ 'feature' => $feature_type])
                <input type="hidden" value="{{"-"}}{{$value->title}}" id="event_title">             
                <input type= "hidden" value = "{{url("/")}}/view-{{strtolower($feature_type)}}/{{preg_replace('/[^A-Za-z0-9\s]/', '', $value->title)}}-{{$value->id}}" id = "page_link"> 
                 </div>
            </div>
        </div>
    </div>
</article>
@endif
@endforeach
@else
@endif

<script id="script_id">
$("#total_count").val({{$total_count}});
$("#slice_arr_count").val({{$slice_arr_count}});
$("#script_id").remove();

@foreach($arr_all_records as $value) 
@if(isset($value->promoted)) 
$.ajax({
    url: "{{url('/')}}/viewer",
    method: 'get',
    data: {
        {{strtolower($value->type)}}_id: '{{$value->id}}',
        to_{{strtolower($value->type)}}_visitor: '{{$value->user_id}}',
        type: '{{strtolower($value->type)}}',
        flag: '1', //for view
    },
    success: function(result) {
        
    }
});
@endif
@endforeach    
</script>
