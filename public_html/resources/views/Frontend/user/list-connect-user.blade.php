
@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <h1>@if(isset($group_name['group_name']) && (!empty($group_name['group_name']))){{ $group_name['group_name']}}'s @endif Connected Users</h1>

    </div>
</section>
<section class="user-dash-sec">
    
    <div class="resp-btn"><a href="javascript:void(0)"><i class="fa fa-angle-down"></i></a></div>
    <div class="dash-cate-sec">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-9 border-left">
                    <div class="marketing">
                        <section id="blog-landing">
                            @if(count($people_data) > 0)
                            <article class="white-panel">
                                <div class="atg-world-one people-to-follows-dash">
                                    <h6>People to connect</h6>
                                    <div class="minimum-heights mCustomScrollbar">

                                        @foreach($people_data as $people)
                                        <div class="media opt-folow-med">
                                            <div class="media-left">
                                                <div class="media-user">
                                                    @if(isset($people->profile_picture))
                                                    <img alt="users" src="{{ url('/public/assets/Frontend/user/profile_pics/'.$people->id.'/thumb/'.$people->profile_picture) }}" @if($people->gender == 0) onerror="src='{{ url('/public/assets/Frontend/img/avtar.png') }}'" @else onerror="src='{{ url('/public/assets/Frontend/img/avtar_female.png') }}'" @endif >
                                                         @elseif($people->gender == 0)
                                                         <img alt="users" src="{{ url('/public/assets/Frontend/img/avtar.png') }}">
                                                    @else
                                                    <img alt="users" src="{{ url('/public/assets/Frontend/img/avtar_female.png') }}">
                                                    @endif
                                                </div>
<!--                                                <div class="media-user">
                                                    <img alt="users" src="{{ url('/public/assets/Frontend/user/profile_pics/'.$people->id.'/thumb/'.$people->profile_picture) }}" onerror="src='{{ url('/public/assets/Frontend/img/avtar.png') }}'">
                                                </div>-->
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading" ><a href='{{url('/')}}/user-profile/{{base64_encode($people->id)}}' style="color:#269b70;">{{ ucfirst($people->first_name) }} {{ ucfirst($people->last_name) }}</a></h4>
                                                <p><i class="icon-4"></i> {{ $people->profession }} </p>
                                                

                                            </div>
                                        </div>
                                        @endforeach

                                    </div><div class="ex-pand-pin-div"><a href="javascript:void(0)">Expand Pin</a></div>
                                </div>
                            </article>
                            @endif



                            








                        </section>
                    </div>

                </div>
            </div>
        </div>
</section>
@if(Session::has('message'))
<p class="alert alert-success">{{ Session::get('message') }}<a class="close">&times;</a></p>
@endif


<script>
            $(document).ready(function() {

    $('#blog-landing').pinterest_grid({
    no_columns: 3,
            padding_x: 10,
            padding_y: 10,
            margin_bottom: 50,
            single_column_breakpoint: 700
    });
            function toggleIcon(e) {
            $(e.target)
                    .prev('.panel-heading')
                    .find(".more-less")
                    .toggleClass('glyphicon-menu-up glyphicon-menu-down');
            }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
            $('.panel-group').on('shown.bs.collapse', toggleIcon);
    });
            function addFollower(follower_user_id)
            {
            $.get('{!!url("/")!!}/add-follower', {follower_user_id: follower_user_id}, function (msg) {
            if (msg == true)
            {
            $(".follower" + follower_user_id).addClass('active');
            } else{
            $(".follower" + follower_user_id).removeClass('active');
            }
            });
            }

    $("input:checkbox").click(function(){

    $.ajax({
    type: 'POST',
            url: '{!!url("/")!!}/post-search',
            data: $('#frm_post_groups').serialize(),
            dataType: 'json',
            success: function(data)
            {
            var val = data.arr_selected_filter_data;
                    console.log(val);
                    var str = '';
                    if (data.success == 1)
            {
            $.each(val, function(index, value) {
            if (value.type == 'event'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view-event/' +value.title+'-'+ value.id + '"><img src="{!!url("/")!!}/assets/Frontend/images/event_image/thumb/' + value.image + '" onError="' + errorImg + '"/></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += '   <a href="{{url("/")}}/view-event/'+value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += '   </a><p>Event</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            } else if (value.type == 'meetup'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view-meetup/'+value.title+'-'+ value.id + '"><img src="{!!url("/")!!}/assets/Frontend/images/meetup_image/thumb/' + value.image + '" onError="' + errorImg + '" /></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += '   <a href="{{url("/")}}/view-meetup/' +value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += '   </a><p>Meetup</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            } else if (value.type == 'job'){

            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one pos-relative">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view/' +value.title+'-'+ value.id + '"><img src="' + errorImg + '" onError="' + errorImg + '"/></a>';
                    str += ' <div class="onter-img-media">';
                    str += '<div class="media otg-found border-top">';
                    str += ' <div class="media-left">';
                    str += '<div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += '</div>';
                    str += '<div class="media-body">';
                    str += ' <h4 class="media-heading"><a href="{{url("/")}}/view/' +value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += ' </div>';
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                    str += '</article>';
            } else if (value.type == 'article'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view-article/' +value.title+'-'+ value.id + '"><img src="{!!url("/")!!}/assets/Frontend/images/article_image/thumb/' + value.image + '" onError="' + errorImg + '"/></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += '<a href="{{url("/")}}/view-article/' +value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += '   </a><p>Articles</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            } else if (value.type == 'company'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/user-profile/' + btoa(value.id) + '"><img src="{!!url("/")!!}/assets/Frontend/img/company.png" onError="' + errorImg + '"/></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += '   <a href="{{url("/")}}/user-profile/' + btoa(value.id) + '"><h4 class="media-heading">' + value.first_name + ' ' + value.last_name + '</h4>';
                    str += '   </a><p>Company</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            } else if (value.type == 'qrious'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view-question/' +value.title+'-'+ value.id + '"><img src="{!!url("/")!!}/assets/Frontend/images/qrious_image/thumb/' + value.image + '" onError="' + errorImg + '" /></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += '   <a href="{{url("/")}}/view-question/' +value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += '   </a><p>Qrious</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            }
            else if (value.type == 'education'){
            str += '<article class="white-panel">';
                    str += '<div class="atg-world-one">';
                    var errorImg = "src='{!!url('/')!!}/assets/Frontend/img/image-not-found2.jpg'";
                    str += '<a href="{{url("/")}}/view-education/' +value.title+'-'+ value.id + '"><img src="{!!url("/")!!}/assets/Frontend/images/education_image/thumb/' + value.image + '" onError="' + errorImg + '" /></a>';
                    str += '<div class="grid-cont">' + value.description + '</div>';
                    str += ' <div class="media otg-found border-top">';
                    str += '<div class="media-left">';
                    str += ' <div class="media-user">';
                    var errorUserImg = "src='{!!url('/')!!}/assets/Frontend/img/pro-2.png'";
                    str += '<img src="{!!url("/")!!}/assets/Frontend/user/profile_pics/' + value.user_id + '/thumb/' + value.profile_picture + '" onError="' + errorUserImg + '"/>';
                    str += ' </div>';
                    str += ' </div>';
                    str += '  <div class="media-body">';
                    str += ' <a href="{{url("/")}}/view-education/' +value.title+'-'+ value.id + '"><h4 class="media-heading">' + value.title + '</h4>';
                    str += '   </a><p>Education</p>';
                    str += ' </div>';
                    str += '</div>';
                    str += '  </div>';
                    str += '   </article>';
            }
            });
            }
            else
            {
            str += 'No Records Found !';
            }
            $("#blog-landing").html(str);
            }
    });
    });
            $(".checkbox-all").on("click", function() {
    if ($(this).prop("checked") == true){
    $(".checked-single").prop("checked", true);
    } else {
    $(".checked-single").prop("checked", false);
    }
    });
            $(".checked-single").on("click", function() {
    if ($(this).prop("checked") == false){
    $(".checkbox-all").prop("checked", false); }
    });
            function addConnection(to_user_id)
            {
            $.get('{!!url("/")!!}/add-connection', {to_user_id: to_user_id}, function (msg) {


            if (msg == true)
            {
            $(".connection" + to_user_id).addClass('active');
                    $(".connection" + to_user_id).html('<i class="fa fa-check"></i>');
            } else{
            $(".connection" + to_user_id).removeClass('active');
                    $(".connection" + to_user_id).html('<i class="icon-Add"></i>');
            }

            });
            }



</script>
<style>
    .text-color{
        color:white
    }
</style>


@include('Frontend.includes.footer')
