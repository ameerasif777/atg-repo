@include('Frontend.includes.head')
@include('Frontend.includes.session-message')
@include('Frontend.includes.header')

<link href="{{ url('/public/assets/css/views/dashboard.css') }}" rel="stylesheet" type="text/css" />


<section class="user-dash-sec" id="search-result">
    <div id="result-div">
    <div class="dash-header">
        <div class="dash-header-bot clearfix">
            <div class="recomended-f">
                <!--<img src="../../../../assets/Frontend/img/filter_icon.png" class="filter-icon hidden-xs">-->
                <div class="filter-text">Filter <i class="icon-Dropdown-Arrow"></i></div>
            </div>
            <div class="dash-middile-nav">
                <ul>                                                   
                    <li><a onclick="$('#hidden_feed_id').val('0');
                            get_data();" class="active" style="cursor: pointer;">All</a></li>
                    <li><a onclick="$('#hidden_feed_id').val('1');
                            get_data();" style="cursor: pointer;">Articles</a></li>    
                    <li><a onclick="$('#hidden_feed_id').val('2');
                            get_data();" style="cursor: pointer;">Education</a></li>                    
                    <li><a onclick="$('#hidden_feed_id').val('3');
                            get_data();" style="cursor: pointer;">Meetups</a></li>                    
                    <li><a onclick="$('#hidden_feed_id').val('4');
                            get_data();" style="cursor: pointer;">Events</a></li>
                    <li><a onclick="$('#hidden_feed_id').val('6');
                            get_data();" style="cursor: pointer;">Qrious?</a></li>
                    <li><a onclick="$('#hidden_feed_id').val('5');
                            get_data();" style="cursor: pointer;">Jobs</a></li>                                        

                </ul>
            </div>
            <div class="dash-right-search">
                <div class="fliterd-by ">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="hidden_feed_id" value="0" id="hidden_feed_id">
    <div class="resp-btn"><a href="javascript:void(0)"><i class="fa fa-angle-down"></i></a></div>
    <div class="dash-cate-sec" id="filter">
        <div class="container">    
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    
                </div>
            </div>       
            <div id="blog-content" class="marketing">
                <section id="blog-landing"></section>               
            </div>
           <h4><center>
                <button type="button" class="btn btn-success btn-xs hide" id="loadbtn"><i class="fa fa-spinner fa-spin "></i> Loading ... </button>
                <span class="no-more-record-found" style="display:none;">No more record found.</span>
            </center></h4> 
        </div> 

    </div> 
    </div>
</section>
</div>
</div>
    
<input type="hidden" name="total_count" id="total_count" value="">
<input type="hidden" id="scroll_end" value="true">
<input type="hidden" name="slice_arr_count" id="slice_arr_count" value="">


<script>
    var scroll_flag = 0;
    var i =0; 
    var track_page = 1; //track user click as page number, righ now page number 1
    var time_session_id = (new Date).getTime();
    $(document).ready(function() {
        
//       $("#blog-landing").waterfall({
//            itemClass: ".white-panel",
//            minColCount: 3,
//            spacingHeight: 10,
//            resizeable: true,
//            ajaxCallback: function(success, end) {
////              var data = {"data": [
////                { "src": "03.jpg" }, { "src": "04.jpg" }, { "src": "02.jpg" }, { "src": "05.jpg" }, { "src": "01.jpg" }, { "src": "06.jpg" }
////              ]};
////              var str = "";
////              var templ = '<div class="box" style="opacity:0;filter:alpha(opacity=0);"><div class="pic"><img src="img/" /></div></div>'
////
////              for(var i = 0; i < data.data.length; i++) {
////                str += templ.replace("", data.data[i].src);
////              }
////              $(str).appendTo($("#div1"));
//
//                load_contents(1,time_session_id);    
//              success();
//              end();
//            }
//          });

//        $("#search-result").mCustomScrollbar({theme: "light-thin" ,
//            scrollbarPosition: "inside",
//            scrollInertia: 5000,
//            autoHideScrollbar:true,
//            autoDraggerLength:false,
//            scrollEasing:"linear"});
//         $("#search-result").mCustomScrollbar({
//         callbacks:{
//             onScrollStart:function(){
//                 
//              //nextReq();
//            },
//            onTotalScroll:function(){
//              nextReq();
//            }, 
//            onOverflowY:function(){
//                scroll_flag = 1; },
//            
//        }});
        
//        $('#blog-landing').pinterest_grid({
//            no_columns: 3,
//            padding_x: 10,
//            padding_y: 10,
//            margin_bottom: 50,
//            single_column_breakpoint: 700
//        });

            
    });
</script>
<style>
    .grey-overlay {
        background: rgba(0,0,0,0.7);
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 9998;
    }

    .embvideo{
        width:100%;
        height:0;
        padding-bottom:56.25%;
        position:relative;
    }
    .embvideo iframe{
        position: absolute;
    }
</style>
<script>


    $('.dash-middile-nav ul li a').click(function(e) {
        e.preventDefault();
        $('a').removeClass('active');
        $(this).addClass('active');
    });

    
    function get_data() {
        arr = [];
        $("#blog-landing").empty('');
        $("#slice_arr_count").val('');
        $(".no-more-record-found").hide();
        $("#scroll_end").val('true');
        $("#total_count").val('');
        load_contents(1, time_session_id);
    }
    //Ajax load function
    var ids_and_types = new Array();
    
    load_contents(track_page, time_session_id); //load content
    var flag = 0 ;
    var arr = [];
    var scrolltop;
    var scroll_handler = function() {
        window.scroll_flag = 1;
        var elemheight = $('#result-div').height();
        var contheight = $(this).height();
        var heightss=$(this).height()/2;
        var c = $(this).scrollTop();
        var scrollPercent = 100 *  (c/ (elemheight - contheight ));
        //console.log(scrollPercent);
        if (scrollPercent>90 && scrolltop < c) {
            if ($("#scroll_end").val() == "true" ) {
                nextReq();
            }
        }
        scrolltop = $(this).scrollTop();
      }; 
      
    function nextReq()
    {
        if(flag == 1 )
        {
            console.log('start');
            track_page++;
             var slice_arr_count = $('#slice_arr_count').val();
             var total_count = $('#total_count').val();
             if (slice_arr_count == '0' && total_count == '0') {
                 $("#scroll_end").val('false');
                 $(".no-more-record-found").css('display', 'block');
             } else {
                 $(".no-more-record-found").css('display', 'none');
                 $("#scroll_end").val('false');
                 load_contents(track_page,time_session_id);
             }
         }
    }
   
   if(window.innerWidth >= 767)
   {
       var itemWidth = '32%';
   }
   else if(window.innerWidth > 480 && window.innerWidth < 767)
   {
       var itemWidth = '49%';
   }
   else 
   {
       var itemWidth = '100%';
   }
   var options = {autoResize: true,
                 offset: 12,
                 flexibleWidth :true,
                 itemWidth :itemWidth,
                 container:$('#blog-landing'),
                 fillEmptySpace: false,
                 align: 'left',
                 resizeDelay: 50,
                 outerOffset: 0};
             
        
    function load_contents(track_page, time_session_id) {
        $('#loadbtn').removeClass('hide');
        var filter_feed_id = $("#hidden_feed_id").val();
        var slice_arr_count = $('#slice_arr_count').val();
        var obj_params = new Object();
        obj_params.page = track_page;
        obj_params.slice_arr_count = slice_arr_count;
        obj_params.filter_feed_id = filter_feed_id;
        obj_params.ids_and_types = ids_and_types;
        obj_params.session_id = time_session_id;
        $('#search-result').off("scroll", scroll_handler);
        flag = 0;

        $.post('{!!url("/")!!}/get-all-post', obj_params, function(data) {
            $('#loadbtn').addClass('hide');
            $("#blog-landing").append(data);
            $("#scroll_end").val('true');
            $('#blog-landing').off('refreshWookmark');  
            $('#blog-landing').wookmark(options);    
            
        }).complete(function() { 
            flag = 1;
            var divelem= document.getElementById('result-div');
            var contentheight = $('#search-result').height();
            if(divelem.scrollHeight > contentheight) 
            {
                $('#search-result').scroll(scroll_handler);
            }
            else {
                nextReq();
            }
            registerDropdownEvent();   
            
        });
    }
    $(document).ready(function() {     
      getLocation();     
      setInterval(function(){
                $('#blog-landing').trigger('refreshWookmark'); 
            }, 500);
            
      window.addEventListener("resize", function() {
               if(window.innerWidth >= 767)
                {
                    var itemWidth = '32%';
                }
                else if(window.innerWidth > 480 && window.innerWidth < 767)
                {
                    var itemWidth = '49%';
                }
                else 
                {
                    var itemWidth = '100%';
                }
                var options = {autoResize: true,
                              offset: 12,
                              flexibleWidth :true,
                              itemWidth :itemWidth,
                              container:$('#blog-landing'),
                              fillEmptySpace: false,
                              align: 'left',
                              resizeDelay: 50,
                              outerOffset: 0};
                          
                $('#blog-landing').wookmark(options);           

        }, false);      
    });

    function registerDropdownEvent()
    {
        $('.dropdown').on('show.bs.dropdown', function () {
            $(this).find("i").removeClass('fa-ellipsis-v');
            $(this).find("i").addClass('fa-ellipsis-h');
         });

         $('.dropdown').on('hide.bs.dropdown', function () {
            $(this).find("i").removeClass('fa-ellipsis-h');
            $(this).find("i").addClass('fa-ellipsis-v');
         });
    }



//function toggleDropDown(elem)
//{
//    if(elem.attr('aria-expanded') === "false")
//    {
//        elem.find("i").removeClass('fa-ellipsis-v');
//        elem.find("i").addClass('fa-ellipsis-h');
//    }
//    else
//    {
//        elem.find("i").removeClass('fa-ellipsis-h');
//        elem.find("i").addClass('fa-ellipsis-v');
//    }
//}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, errorPosition);
    }
}

    function showPosition(position) {
        var latitude  = position.coords.latitude;
        var longitude = position.coords.longitude;
        var obj_params = new Object();
        obj_params.latitude = latitude;
        obj_params.longitude = longitude;
        obj_params.agent = navigator.userAgent;
        $.post('{!!url("/")!!}/update-user-location', obj_params, function() { });
    }

    function errorPosition() {
	   $.post('{!!url("/")!!}/update-user-location', function() {});
    }
    
    function upvoteDownvote(id, status, type, flag) {
        var posturl = '';
        if (type == "Meetup") {
            posturl = "up-down-vote-meetup";

        }
        if (type == "Event") {
            posturl = "up-down-vote-event";

        }

        $.ajax({
            type: "post",
            url: "{!!url('/')!!}/" + posturl,
            data: {
                id: id,
                status: status
            },
            success: function(data) {
                if (flag == 1) {

                    if ($("#upvote_post_icon_" + id + "_" + type).attr("class") == "fa fa-thumbs-up") {
                        $("#upvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-o-up")


                    } else {
                        $("#upvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-up")
                        $("#downvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-o-down")
                    }
                } else {
                    if ($("#downvote_post_icon_" + id + "_" + type).attr("class") == "fa fa-thumbs-down") {
                        $("#downvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-o-down")
                    } else {

                        $("#downvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-down")
                        $("#upvote_post_icon_" + id + "_" + type).attr("class", "fa fa-thumbs-o-up")
                    }
                }
            }
        });
    }
    function upvoteDownvoteArticle(article_id, status, flag) {
        $.ajax({
            type: "post",
            url: "{!!url('/')!!}/up-down-vote-article",
            data: {
                article_id: article_id,
                status: status
            },
            success: function(data) {
                if (flag == 1) {
                    if ($("#article_icon_vote_" + article_id).attr("class") == "fa fa-thumbs-up") {
                        $("#article_icon_vote_" + article_id).attr("class", "fa fa-thumbs-o-up")

                    } else {
                        $("#article_icon_vote_" + article_id).attr("class", "fa fa-thumbs-up")
                        $("#article_icon_down_" + article_id).attr("class", "fa fa-thumbs-o-down")

                    }
                } else {
                    if ($("#article_icon_down_" + article_id).attr("class") == "fa fa-thumbs-o-down") {
                        $("#article_icon_down_" + article_id).attr("class", "fa fa-thumbs-down")
                        $("#article_icon_vote_" + article_id).attr("class", "fa fa-thumbs-o-up")

                    } else {

                        $("#article_icon_down_" + article_id).attr("class", "fa fa-thumbs-o-down")

                    }
                }

            }
        })
    }
    function upvoteDownvoteQrious(question_id, status, flag) {

        $.ajax({
            type: "post",
            url: "{!!url('/')!!}/up-down-vote-question",
            data: {
                question_id: question_id,
                status: status
            },
            success: function(data) {
                if (flag == 1) {
                    if ($("#qrious_icon_vote_" + question_id).attr("class") == "fa fa-thumbs-up") {
                        $("#qrious_icon_vote_" + question_id).attr("class", "fa fa-thumbs-o-up")
                    } else {

                        $("#qrious_icon_vote_" + question_id).attr("class", "fa fa-thumbs-up")
                        $("#qrious_icon_down_" + question_id).attr("class", "fa fa-thumbs-o-down")
                    }
                } else {
                    if ($("#qrious_icon_down_" + question_id).attr("class") == "fa fa-thumbs-down") {
                        $("#qrious_icon_down_" + question_id).attr("class", "fa fa-thumbs-o-down")
                    } else {
                        $("#qrious_icon_down_" + question_id).attr("class", "fa fa-thumbs-down")
                        $("#qrious_icon_vote_" + question_id).attr("class", "fa fa-thumbs-o-up")
                    }
                }
            }
        })
    }
    function upvoteDownvoteEducation(education_id, status, flag) {

        $.ajax({
            type: "post",
            url: "{!!url('/')!!}/up-down-vote-education",
            data: {
                education_id: education_id,
                status: status
            },
            success: function(data) {
                if (flag == 1) {
                    if ($("#education_icon_vote_" + education_id).attr("class") == "fa fa-thumbs-up") {
                        $("#education_icon_vote_" + education_id).attr("class", "fa fa-thumbs-o-up")
                    } else {

                        $("#education_icon_vote_" + education_id).attr("class", "fa fa-thumbs-up")
                        $("#education_icon_down_" + education_id).attr("class", "fa fa-thumbs-o-down")
                    }
                } else {
                    if ($("#education_icon_down_" + education_id).attr("class") == "fa fa-thumbs-down") {
                        $("#education_icon_down_" + education_id).attr("class", "fa fa-thumbs-o-down")
                    } else {
                        $("#education_icon_down_" + education_id).attr("class", "fa fa-thumbs-down")
                        $("#education_icon_vote_" + education_id).attr("class", "fa fa-thumbs-o-up")
                    }
                }
            }
        })
    }

    function upvoteDownvoteJob(job_id, status, flag) {

        $.ajax({
            type: "post",
            url: "{!!url('/')!!}/up-down-vote-job",
            data: {
                job_id: job_id,
                status: status
            },
            success: function(data) {
                if (flag == 1) {
                    if ($("#job_icon_vote_" + job_id).attr("class") == "fa fa-thumbs-up") {
                        $("#job_icon_vote_" + job_id).attr("class", "fa fa-thumbs-o-up")
                    } else {

                        $("#job_icon_vote_" + job_id).attr("class", "fa fa-thumbs-up")
                        $("#job_icon_down_" + job_id).attr("class", "fa fa-thumbs-o-down")
                    }
                } else {
                    if ($("#job_icon_down_" + job_id).attr("class") == "fa fa-thumbs-down") {
                        $("#job_icon_down_" + job_id).attr("class", "fa fa-thumbs-o-down")
                    } else {
                        $("#job_icon_down_" + job_id).attr("class", "fa fa-thumbs-down")
                        $("#job_icon_vote_" + job_id).attr("class", "fa fa-thumbs-o-up")
                    }
                }
            }
        })
    }
    function share_div_show(share_id){
        
        title = $("#"+share_id).parent().find(".title").attr("value");
        console.log(title);
        page_link = $("#"+share_id).parent().find("#page_link").attr("value");
        if($("#"+share_id).data('clicked')) {
                $(".share_div").css("display","none");
                $("#"+share_id).data('clicked', false);
            }else{
                $(".share_div").css("display","none");
                $("#"+share_id).parent().find(".share_div").css("display","block");
                $("#"+share_id).data('clicked', true);
            }
       }
   
</script>

