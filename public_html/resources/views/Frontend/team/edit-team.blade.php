@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script>
$(document).ready(function () {
    $('.multipleSelect').popSelect({
        showTitle: false,
        autoIncrease: true,
        position:'bottom',
        placeholderText: 'Please select the members'
    });
})
</script>
<section class="contact-sec"  style="background-image:url({{ url('/public/assets/Frontend/img/team_profile/'.$team_details['0']->profile_image) }});">
    <div class="container">
        <h1>{{ $team_details['0']->team_name }}</h1>
    </div>
</section>

<section class="team-detai-sec fullHt">
	<div class="container">
    	<div class="row">
             <form name="team" id="team" method="post" action="{{url('/')}}/edit-team/{{ base64_encode($team_details['0']->id) }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
        	<div class="col-sm-6">
            	<div class="steper-two-inner team-imfor-det clearfix">
                    <h2>Team Details</h2>
                    <div class="form-group">
                        <label for="">Team Name</label>
                       <input type="text" name="team_name" class="form-control"  value="{{ $team_details['0']->team_name }}" >
                    </div>
                    <div class="form-group">
                        <label for="">Project Name</label>
                        <input type="text" name="project_name" class="form-control"  value="{{ $team_details['0']->project_name }}" >
                    </div>
                    <div class="form-group">
                        <label for="">Project Description</label>
                        <input type="text" name="project_description" class="form-control"  value="{{ $team_details['0']->project_description }}" >
                    </div>
                    @if($team_details['0']->profile_image == '' || $team_details['0']->profile_image == null)
                        <div class='col-sm-12'>     
                        <div class="uploar-imger">
                               <label for="">Upload Image</label>
                                 <div class="uping-imgs">
                                    <input type="file" name='profile_pic' onchange="readURL(this);"  id="profile_pic" class="upit-img" />
                                </div>
                                <div for="profile_pic" class="error"></div>
                                <div class="title-input uplink-imgs">
                                    <img class="uploding-img-vie" id="blah" src="#" alt="" width=100%" /><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class='col-sm-12'>
                        <!--<img src="{{ url('/public/assets/Frontend/img/team_profile/thumb/'.$team_details['0']->profile_image) }}" alt="No image found" height="230px" width="100%" >-->
                            <div class="uploar-imger">
                               <label for="">Edit Image</label>
                                 <div class="uping-imgs">
                                    <input type="file" name='edit_profile_pic' onchange="readURL(this);"  id="profile_pic" class="upit-img" />
                                </div>
                                <div class="title-input uplink-imgs">
                                    <img class="uploding-img-vie" id="blah" src="#" style="display: none" alt="" width=100%" /><a href="javascript:void(0)" style="display: none" onclick="removeimage();" id="remove_image" title="Remove"><i class="fa fa-remove"></i></a>
                                </div>
                            </div>
                            </div>
                        @endif
                </div>
                    
            </div>
        	<div class="col-sm-6">
                    <select name="member[]" multiple="" class="form-control multipleSelect">
                                            @foreach($arr_user_connection as $value)
                                            <option value="{{$value['id']}}" @if(in_array($value['id'],$arr_user)) selected @endif  >{{ucfirst($value['first_name']).'  '.ucfirst($value['last_name'])}}</option>
                                            @endforeach
                                        </select>
<!--            	<div class="steper-two-inner steper-teams-inner">
                    <h2>Team Members</h2>
                    
                    
                    <ul class="clearfix add-connections scrollDiv" style="overflow: hidden;" tabindex="5001">
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-4.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Akshay Jagtap</h4>
                                <p>( Group Admin )</p>
                                <a class="more" href="javascript:void(0)" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-3.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Dattas B.s</h4>
                                <p>( Group Member )</p>
                                <a class="more" href="javascript:void(0)" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-1.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Vishnu Nimbalkar</h4>
                                <p>( Group Member )</p>
                                <a class="more" href="javascript:void(0)" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-2.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Armin Doe</h4>
                                <p>( Group Member )</p>
                                <a class="more" href="javascript:void(0)" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-4.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Big Ali</h4>
                                <p>( Group Member )</p>
                                <a class="more" href="javascript:void(0)" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                              </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                              <div class="media-left">
                                <div class="media-user">
                                  <img alt="users" src="img/pro-4.png">
                                </div>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">Big Ali</h4>
                                <p>( Group Member )</p>
                                <a class="more" href="javascript:void(0)" title="Leave" data-placement="left"><i class="fa fa-remove"></i></a>
                              </div>
                            </div>
                        </li>
                    </ul>
                </div>-->
            </div>
                     <div class="row">
                    <div class='col-sm-2'>
                        <button class="btn teme-save-btn" id="btn_create_team" name="btn_create_team" >Edit Team</button>
                    </div>
                    </div>
                    
                </form>
        </div>
    </div>
</section>
<div class="map-cont-sec">
    <div class="map clearfix  "> 
        <div id="map_canvas" style="width:100%; height:380px;"></div>
    </div>
</div>
@include('Frontend.includes.footer')
<script src="{{url('/assets/Frontend/js/team/team.js')}}"></script>
<script>
function del(member_id,team_id) {
    if (confirm("Are you sure want to remove?")) {
        $.ajax({
            url: '{!!url("/")!!}/remove-member',
            data:  {member_id: member_id,team_id:team_id},
            dataType: 'json'
        }).success(function (msg) {
            location.reload();
        })
    }
    return false;
}
//
               function readURL(input) {
//        var ext = input.value.match(/\.(.+)$/)[1];
//        switch (ext)
//        {
//            case 'jpg':
//            case 'bmp':
//            case 'png':
//            case 'tif':
//                if (input.files) {
//                    var reader = new FileReader();
//                    reader.onload = function(e) {
//                        $('#blah').css("display", "block");
//                        $('#blah').attr('src', e.target.result);
//                        $('#remove_image').css("display", "block");
//                    }
//
//                    reader.readAsDataURL(input.files[0]);
//                }
//                break;
//            default:
//                alert('Only Image allowed !');
//                input.value = '';
//        }

    }
    
    
    
    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#profile_pic").prop("value", "");
    }

</script>
<script>
    $(document).ready(function () {
        var _URL = window.URL || window.webkitURL;
    var flag = true; 
        
        $("#profile_pic").change(function(e) {
           
        var file, img;
        var ext = this.files[0].name.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert('Only JPG, PNG or GIF files are allowed');
            $("#profile_pic").val('');
        } else {
            
            if (file = this.files[0]) {
                img = new Image();
                img.onload = function() {
                    if (this.width >= 1900 && this.height >= 1200) {
                        var reader = new FileReader();
    console.log(reader)
            reader.onload = function(e) {
            $('#blah').css("display", "block");
                    $('#blah').attr('src', e.target.result);
                    $('#remove_image').css("display", "block");
            }

    reader.readAsDataURL(file);
                    } else {
                        //flag = false;
                        alert("Image height and width should be greater than 1900*1200");
                       // $('#blah').attr('src', './media/front/img/image-not-found.png');
                        $("#profile_pic").replaceWith($("#profile_pic").val('').clone(true));
                    }
                     
                };
                 img.src = _URL.createObjectURL(file);
                readURL(this);
              
            }
        }
    }); 
        var url = $("#base_url").val()
        $("img").error(function () {
            $(this).attr('src', url + '/assets/Frontend/img/avtar.png');
        });
    })
</script>




