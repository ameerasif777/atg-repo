@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
@include('Frontend.includes.user.account-setting-css')
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script>
        $(document).ready(function () {
$('.multipleSelect').popSelect({
showTitle: false,
        autoIncrease: true,
        placeholderText: 'Please select the members',
        position: 'bottom'
        });
})
</script>
<section class="contact-sec" style="background-image:url(assets/Frontend/img/dash-bg.png);">
    <div class="container">
        <h1>Team</h1>
    </div>
</section>

<!-------------------------------------------------------LOG-STEP-ONE START------------------------------------------------------>
<section class="acount-tabers">
    <div class="container">
        <div class="ver-tab-bg clearfix">
            <div class="bhoechie-tab-container clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <h6>account settings</h6>
                    <div class="list-group">
                        <a class="list-group-item active text-center" href="javascript:void(0);">
                            Create Team
                        </a>
                        <a class="list-group-item text-center" href="javascript:void(0);">
                            My Team
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <!-- flight section -->
                    <div class="bhoechie-tab-content tem-capter clearfix active">
                        <form name="team" id="team" method="post" action="{{url('/')}}/create-team">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <h6>Create a new team/project</h6>
                            <div class="main-inner-tab-info my-team-tab clearfix">
                                <dl class="dl-horizontal">
                                    <dt>Name of Team :</dt>
                                    <dd><input type="text" name="team_name" id="team_name" placeholder="Team Name"></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Project Name :</dt>
                                    <dd><input type="text" name="project_name" id="project_name" placeholder="Project Name"></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Project Description :</dt>
                                    <dd><input type="text" name="project_description" id="project_description"  placeholder="Project Name"></dd>
                                </dl>
                            </div>
                            <div class="tema-members-outs">
                                <h4>Team Members</h4>
                                <!--                                                            <div class="team-mem-inner">
                                                                                                <div class="users-conectry-nav temb-slids">
                                                                                                    <ul class="clearfix">
                                                                                                        <li class="selcted-sol"><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan (Admin)</a> <div class="remores"><a href="javascript:void(0)"></a></div></li>
                                                                                                        <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                                                        <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                                                        <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                                                        <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                                                        <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                                                    </ul>
                                                                                                    <div class="adder-tem-mem"><a href="javascript:void(0)"><i class="fa fa-user-plus"></i> Add New Member</a></div>
                                                                                                </div>
                                                                                            </div>-->
                                <div class="team-mem-inner">
                                    <div class="users-conectry-nav temb-slids">
                                        <select name="member[]" multiple="" class="form-control multipleSelect">
                                            @foreach($arr_user_connection as $value)
                                            <option value="{{$value['to_user_id']}}">{{ucfirst($value['to_first_name']).'  '.ucfirst($value['to_last_name'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <button class="btn teme-save-btn" id="btn_create_team" name="btn_create_team" >Create Team</button>
                            <!--<button type="button" id="btn_create_team" name="btn_create_team" class="btn teme-save-btn">Save</button>-->
                        </form>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="tema-members-outs">
                            <h4>Team Names</h4>
                            <div class="team-mem-inner">
                                <div class="users-conectry-nav temb-slids">
                                    <!--                                    <ul class="clearfix">
                                                                            <li class="selcted-sol"><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan (Admin)</a> <div class="remores"><a href="javascript:void(0)"></a></div></li>
                                                                            <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                            <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                            <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                            <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                            <li><a href="javascript:void(0)"><span class="tema-pros"><img src="img/pro-4.png" alt="pros-img"/></span> Ahmed Pathan</a> <div class="remores"><a href="javascript:void(0)"><i class="fa fa-trash-o"></i></a></div></li>
                                                                        </ul>-->
                                   @if(count($my_team) == 0)
                                   <ul class="clearfix">
                                       <li>
                                           <h4>No,Records Available</h4>
                                       </li>
                                   </ul>
                                   @else
                                   
                                   @endif
                                    <ul class="clearfix">
                                        @foreach($my_team as $key=>$value)
                                        <li ><a href="{{url('/')}}/team-details/{{base64_encode($value->id)}}"><span class="tema-pros"><img src="img/pro-4.png" alt=""/></span>{!! ucfirst($value->team_name) !!}</a><div class="remores"><a href="javascript:void(0)" onclick="del({{ $value->id }})"><i class="fa fa-trash-o"></i></a></div></li>

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            <center>
                                <h1>B</h1>
                                <h2>Cooming Soon</h2>
                                <h3>Train Reservation</h3>
                            </center>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            <center>
                                <h1>C</h1>
                                <h2>Cooming Soon</h2>
                                <h3>Train Reservation</h3>
                            </center>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            <center>
                                <h1>D</h1>
                                <h2>Cooming Soon</h2>
                                <h3>Train Reservation</h3>
                            </center>
                        </div>
                    </div>
                    <div class="bhoechie-tab-content">
                        <div class="main-inner-tab-info clearfix">
                            <center>
                                <h1>E</h1>
                                <h2>Cooming</h2>
                                <h3>Train Reservation</h3>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="map-cont-sec">
    <div class="map clearfix  "> 
        <div id="map_canvas" style="width:100%; height:380px;"></div>
    </div>
</div>
@include('Frontend.includes.footer')
<script src="{{url('/assets/Frontend/js/team/team.js')}}"></script>
<script>
                                                    $(document).ready(function() {
                                            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                                            e.preventDefault();
                                                    $(this).siblings('a.active').removeClass("active");
                                                    $(this).addClass("active");
                                                    var index = $(this).index();
                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                                                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                                            });
                                            });</script>

<script>
            function del(team_id) {
            if (confirm("Are you sure want to delete?")) {
            $.ajax({
            url: 'delete-team/' + btoa(team_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
                    }
</script>




