@include('Frontend.includes.head')
@include('Frontend.includes.header')
<section class="persnal-sec" style="background-image:url({{ ($arr_user1_data['cover_photo']!='') ? asset('assets/Frontend/user/cover_photo/'.$arr_user1_data['id'].'/'.$arr_user1_data['cover_photo']) : asset('assets/Frontend/img/dash-bg.png') }});">
    <div class="container">
        <div class="inner-bios">
            <div class="row">
                <div class="col-md-4">
                    <div class="main-pros-information">
                        
                        <div class="main-pross">
                            <a href='javascript:void(0);'  >
                                @if(isset($arr_user1_data['profile_picture']))
                                <img  id="main_profile" @if($arr_user1_data['gender'] == 0) onerror="src='{{ url('/public/assets/Frontend/img/avtar.png') }}';" @else onerror="src='{{ url('/public/assets/Frontend/img/avtar_female.png') }}';" @endif  src="{{ $arr_user1_data['profile_picture']!=''? url('/public/assets/Frontend/user/profile_pics/'.$arr_user1_data['id'].'/'.$arr_user1_data['profile_picture']) : url('/public/assets/Frontend/img/avtar.png') }}" alt="profile"/>
                                      
                                      @elseif(isset($arr_user1_data) && $arr_user1_data['gender'] == 0)
                                      <img  id="main_profile" src="{{ url('/public/assets/Frontend/img/avtar.png') }}" alt="profile"/>
                                      @else
                                      <img  id="main_profile" src="{{ url('/public/assets/Frontend/img/avtar_female.png') }}" alt="profile"/>
                                      @endif
                            </a>
                        </div>

                        <h2>{{ucfirst($arr_user1_data['first_name']).' '.ucfirst($arr_user1_data['last_name']) }}</h2>
                        <div class="address-user">{!!isset($arr_user1_data['location']) ?$arr_user1_data['location']:'---' !!}</div>

                        <div class="address-thougt">{!!isset($arr_user1_data['tagline']) ?$arr_user1_data['tagline']:'---' !!}</div>

                        <h2>{!!isset($arr_user1_data['profession']) ?$arr_user1_data['profession'] :'---' !!}</h2>
                        <div class="address-user">{!!(count($arr_follower) == 1 || count($arr_follower) == 0)  ? count($arr_follower).' Follower' :count($arr_follower).' Followers' !!} </div>


                    </div>
                </div>
                <form name="profilepicfrm" id="profilepicfrm" action="" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="file" id="profile_picture" style="opacity: 0" name="profile_picture" >
                </form>
                <div class="col-md-8">
                    <div class="inters-abter">
<!--                        <form name="coverform" id="coverform" action="" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="editors-pop"><a href="javascript:void(0)"  title="setting"><i class="icon-Cover-Edit"></i></a></div>
                            <input type="file" id="cover_photo" style="opacity: 0" name="cover_photo" onchange="uploadImage(this)">
                        </form>-->
                        <div class="btin-aab">
                            @if($finalData['user_session']['id']!= $arr_user1_data['id'])
                            @if(isset($is_connect) && (!empty($is_connect[0])))
                            @if($is_connect[0]->status == 0 )
                            <a href="javascript:void(0);"  class="conee-btn btn connection{!!$arr_user1_data['id']!!}" onclick="addConnection({{$arr_user1_data['id']}})"><i class="icon-Add"></i> Connect</a>
                            @elseif($is_connect[0]->status == 1)
                            <a href="javascript:void(0);"  class="conee-btn btn connection{!!$arr_user1_data['id']!!}"><i class="fa fa-check"></i> Connected</a>
                            @else
                            <a href="javascript:void(0);"  class="conee-btn btn connection{!!$arr_user1_data['id']!!}"><i class="fa fa-cross"></i> Rejected</a>
                            @endif
                            @endif
                            @if(count($is_follower) == 0)
                             <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_user1_data['id']!!}" onclick="addFollower({{$arr_user1_data['id']}})"><i class="icon-Add"></i> Follow</a>
                            @else
                             <a  href='javascript:void(0);' class="folll-btn btn follow{!!$arr_user1_data['id']!!}" onclick="addFollower({{$arr_user1_data['id']}})"><i class="icon-Follow"></i> Unfollow</a>
                            @endif
                            <!--<button type="button" class="folll-btn btn"><i class="icon-Follow"></i> Follow</button>-->
                            <button type="button" class="masss-btn btn"><i class="icon-Message"></i> Message</button>
                            <div class="dotss">
                                <a href="javascript:void(0)" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="crxl"></span></a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="javaScript:void(0);">Block</a></li>
                                    <li><a href="javaScript:void(0);">Report</a></li>

                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bio-details-sec">
    <div class="container">
        <div class="ab-spaner-cross"><a href="javascript:void(0)"></a></div>
        <div class="inner-main-bio-det">
            <h2>Profile | Bio  <a href="javascript:void(0);"></a></h2>
            <div class="inters-bio clearfix">
                <div class="inters-bio-left">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="" class="col-sm-5">Name :</label>

                            <div class="col-sm-7">{!!$arr_user1_data['first_name'].' '.$arr_user1_data['last_name']!!}</div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-5">Email Address :</label>
                            <div class="col-sm-7">{!!$arr_user1_data['email']!!}</div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-5">Mobile Number :</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['mob_no'])  && $arr_user1_data['mob_no']!='' ?$arr_user1_data['mob_no']:'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Phone Number :</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['phone_no'])  && $arr_user1_data['phone_no']!='' ?$arr_user1_data['phone_no'] :'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Location :</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['location'])  && $arr_user1_data['location']!='' ?$arr_user1_data['location'] :'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">About Me :</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['about_me'])  && $arr_user1_data['about_me']!='' ?$arr_user1_data['about_me'] :'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5"> Tag Line :</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['tagline'])  && $arr_user1_data['tagline']!='' ?$arr_user1_data['tagline'] :'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Profession:</label>
                            <div class="col-sm-7">
                                {!!isset($arr_user1_data['profession'])  && $arr_user1_data['profession']!='' ?$arr_user1_data['profession'] :'---' !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Account Type:</label>
                            <div class="col-sm-7">
                                @if ($arr_user1_data['user_type'] == '1') Student @elseif($arr_user1_data['user_type'] == '3') Company @elseif($arr_user1_data['user_type'] == '4') Professional @endif
                            </div>
                        </div>

                    </form>  
                </div>
                <div class="inters-bio-left inters-bio-right">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="" class="col-sm-5">Gender :</label>
                            <div class="col-sm-7">{!!($arr_user1_data['gender'] == '0') ? 'Male' :'Female' !!}</div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Height :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['height'] )  && $arr_user1_data['height'] !='' ? $arr_user1_data['height'] :'---' !!}  
                                {!! ( isset($arr_user1_data['height_unit'] ) && $arr_user1_data['height_unit']=='1') ? 'CM' :'Inches' !!}</div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Weight :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['weight'] ) && $arr_user1_data['weight']!='' ? $arr_user1_data['weight']:'---' !!}</div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-5">Body type :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['body_type'] ) && $arr_user1_data['body_type']!='' ? $arr_user1_data['body_type']:'---' !!}</div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Hair :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['hair_color']) && $arr_user1_data['hair_color']!='' ? $arr_user1_data['hair_color']:'---' !!}	</div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-5">Eyes :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['eye_color'] )  && $arr_user1_data['eye_color']!='' ? $arr_user1_data['eye_color'] :'---' !!}</div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-5">Ethnicity :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['ethnicity_type'] ) && $arr_user1_data['ethnicity_type']!=''  ? $arr_user1_data['ethnicity_type'] :'---' !!}</div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-5">Birth Date :</label>
                            <div class="col-sm-7">{!! isset($arr_user1_data['user_birth_date'])  && $arr_user1_data['profession']!='' ? $arr_user1_data['user_birth_date'] :'---' !!}</div>
                        </div>

                    </form> 
                </div>
            </div>
        </div>
    </div>
</section>

<script>
            function getCoverPhoto()
            {
            document.getElementById('cover_photo').click();
            }

    function getProfilePicture()
    {
    document.getElementById('profile_picture').click();
    }

    function addConnection(to_user_id)
    {
    $.get('{!!url("/")!!}/add-connection', {to_user_id: to_user_id}, function (msg) {

    if (msg == true)
    {
            $(".connection" + to_user_id).html('<i class="fa fa-check"></i> Connected');
    } else{
            $(".connection" + to_user_id).html('<i class="icon-Add"></i> Connect');
    }

    });
    }

    function addFollower(follower_user_id)
    {
    $.ajax({
    type: "get",
            url: '{!!url("/")!!}/make-follower',
            data: {follower_user_id: follower_user_id},
            dataType: "json",
            success: function(msg) {
            if (msg.success == 1)
            {
                    $(".follow" + follower_user_id).html('<i class="fa fa-check"></i> Unfollow');
            } else{
                    $(".follow" + follower_user_id).html('<i class="icon-Add"></i> Follow');
            }

            }
    })


    }



    function uploadImage(value) {
    var form = document.getElementById('coverform');
            var formData = new FormData(form);
            var base_url = jQuery("#base_url").val();
            $.ajax({
            url: base_url + '/cover-photo',
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    data: formData,
                    method: 'post',
                    success: function(response) {

                    if (response.error == '0') {

                    var url = base_url + '/public/assets/Frontend/user/cover_photo/{!!$arr_user1_data['id']!!}/' + response.cover_photo;
                            $('.persnal-sec').css('backgroundImage', 'url(' + url + ')');
                            return false;
                    } else if (response.error == '1')
                    {
                    alert('error uploading the file!');
                            return false;
                    }
                    }
            })
    }
   

</script>

@include('Frontend.includes.footer')

