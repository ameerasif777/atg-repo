@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script>
        $(document).ready(function () {
$('.multipleSelect').popSelect({
showTitle: false,
        autoIncrease: true,
        placeholderText: 'Please select the members'
});
        })
</script>
<section class="contact-sec" style="background-image:url({{ url('/public/assets/Frontend/img/team_profile/'.$team_details['0']->profile_image) }});">
    <div class="container">
        <h1>{{ $team_details['0']->team_name }}</h1>
    </div>
</section>
<section class="team-detai-sec fullHt">
    <div class="container">
        <div class="row">
            <form name="team" id="team" method="post" action="{{url('/')}}/create-team">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="col-sm-6">
                    <div class="steper-two-inner team-imfor-det">
                        <h2>Team Details</h2>
                        <div class="form-group">
                            <label for="">Team Name</label>
                            <p>{{ $team_details['0']->team_name }}</p>
                        </div>
                        <div class="form-group">
                            <label for="">Project Name</label>
                            <p>{{ $team_details['0']->project_name }}</p>
                        </div>
                        <div class="form-group">
                            <label for="">Project Description</label>
                            <p>{{ $team_details['0']->project_description }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="steper-two-inner steper-teams-inner">
                        <h2>Team Members</h2>
                        <ul class="clearfix add-connections scrollDiv" style="overflow: hidden;" tabindex="5001">
                            @foreach($team_details as $value)
                            @if($value->role == 1)
                            <li>
                                <div class="media">

                                    <div class="media-left">

                                        <div class="media-user">
                                            @if(isset($value->profile_picture))
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{url('/')}}/assets/Frontend/user/profile_pics/{{ $value->user_id }}/thumb/{{ $value->profile_picture }}" @if($value->gender == 0) onerror="src='{{ url('/public/assets/Frontend/img/avtar.png') }}';" @else onerror="src='{{ url('/public/assets/Frontend/img/avtar_female.png') }}';" @endif >
                                            </a>
                                            @elseif(isset($value->gender) && $value->gender == 0)
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{ url('/public/assets/Frontend/img/avtar.png') }}" alt="profile"/>
                                            </a>
                                            @else
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{ url('/public/assets/Frontend/img/avtar_female.png') }}" alt="profile"/>
                                            </a>
                                            @endif
                                        </div>


                                    </div>

                                    <div class="media-body">
                                        <h4 class="media-heading">{{ ucfirst($value->first_name) }} {{ ucfirst($value->last_name) }}</h4>
                                        <p>( Group Admin )</p>
                                        @if($user_id != $value->user_id)
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @else
                            <li>
                                <div class="media">

                                    <div class="media-left">

                                        <div class="media-user">
                                            @if(isset($value->profile_picture))

                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{url('/')}}/assets/Frontend/user/profile_pics/{{ $value->user_id }}/thumb/{{ $value->profile_picture }}" @if($value->gender == 0) onerror="src='{{ url('/public/assets/Frontend/img/avtar.png') }}';" @else onerror="src='{{ url('/public/assets/Frontend/img/avtar_female.png') }}';" @endif>
                                            </a>
                                            @elseif(isset($value->gender) && $value->gender == 0)
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{ url('/public/assets/Frontend/img/avtar.png') }}" alt="profile"/>
                                            </a>
                                            @else
                                            <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left">
                                                <img  src="{{ url('/public/assets/Frontend/img/avtar_female.png') }}" alt="profile"/>
                                            </a>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="media-body">
                                        <h4 class="media-heading">{{ ucfirst($value->first_name) }} {{ ucfirst($value->last_name) }}</h4>
                                        <p>( Group Member )</p>
                                        @if($user_id == $value->user_id)
                                        <!--<a href="javascript:void(0)" class="pull-right" id="leave"  onclick="leave({{$team_details['0']->team_id}},{{$value->user_id}})">Leave </a>-->

                                        <a class="more" href="javascript:void(0)" id="leave" title="Leave" data-placement="left" onclick="leave({{$team_details['0']->team_id}},{{$value->user_id}})"><i class="fa fa-remove"></i></a>
                                        @else
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->user_id)}}" title="Visit Page" data-placement="left"><i class="icon-User"></i></a>

                                        @endif
                                    </div>
                                </div>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class='col-sm-12'>
                    @if($team_details['0']->user_id_fk == $user_id )
                    <a class="btn edit-ss-btn" href="{{url('/')}}/edit-team/{{ base64_encode($team_details['0']->team_id) }}">Edit</a>
                    @endif

                </div>
            </form>
        </div>
    </div>
</section>
<div class="map-cont-sec">
    <div class="map clearfix  "> 
        <div id="map_canvas" style="width:100%; height:380px;"></div>
    </div>
</div>
@include('Frontend.includes.footer')

<script>
            function del(team_id) {
            if (confirm("Are you sure want to delete?")) {
            $.ajax({
            url: 'delete-team/' + btoa(team_id),
                    data: '',
                    dataType: 'json'
            }).success(function (msg) {
            location.reload();
            })


            }
            return false;
            }
    function leave(team_id, user_id){
    if (confirm("Are you sure want to leave {{ucfirst($team_details['0']->team_name)}} team?")) {

    $.ajax({
    url: '{{url('/')}}/leave-team',
            dataType: 'json',
            method: 'get',
            data :{team_id:team_id, user_id:user_id},
            success: function (response) {
            if (response.msg == '1') {
            $('#leave').text('');
            }
            location.reload();
            }
    });
    }
    }
</script>
<script>
    $(document).ready(function () {
//        var url = $("#base_url").val()
//        $("img").error(function () {
//            $(this).attr('src', url + '/assets/Frontend/img/avtar.png');
//        });
    })
</script>




