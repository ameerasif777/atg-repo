
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{!!($finalData['global']['site_title'])?$finalData['global']['site_title'] :$finalData['site_title']!!}</title>

        <link rel="icon" type="image/png" href="{{ url('/public/assets/img/favicon.ico') }}" />
        {{-- libs css--}}
        <link href="{{ url('/public/assets/css/libs/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/public/assets/css/libs/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/public/assets/css/libs/animated.css') }}" rel="stylesheet" type="text/css" />

        {{-- Plugins css--}}
        <link href="{{ url('/public/assets/css/plugins/owl.theme.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ url('/public/assets/css/plugins/jquery.mCustomScrollbar.css') }}" type="text/css"/>
        <link rel="stylesheet" href="{{ url('/public/assets/css/plugins/owl.carousel.css') }}" type="text/css"/>
        <link rel="stylesheet" href="{{ url('/public/assets/css/plugins/jquery-ui.min.css') }}" type="text/css"/>

        {{-- Custom CSS --}}
        <link href="{{ url('/public/assets/css/views/welcome.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/assets/Frontend/css/main.css') }}" rel="stylesheet" type="text/css" />

        {{-- libs js--}}
        <script src="{{url('/public/assets/js/libs/jquery.js')}}"></script>
        <script src="{{url('/public/assets/js/libs/bootstrap.min.js')}}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        {{-- Plugins js--}}
        <script src="{{url('/public/assets/js/plugins/skrollr.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.nicescroll.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/wow.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/device.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.validate.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.validate.password.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/scrollIt.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/owl.carousel.min.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery-ui.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.validate-1.14.0.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/jquery.sequentialLoad.js')}}"></script>


        <script src="{{url('/public/assets/js/plugins/custom-plugins/registration/registration.js')}}"></script>
        <script src="{{url('/public/assets/js/plugins/custom-plugins/login/user-reset.js')}}"></script>
        {{-- Custom js --}}
        <script src="{{url('/public/assets/js/views/welcome.js')}}"></script>

        <link rel="manifest" href="{{ url('/manifest.json') }}">
    </head>
    <input type="hidden" id="base_url" name="base_url" value="{!!url('/')!!}">
