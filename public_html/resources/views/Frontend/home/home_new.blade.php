@include('Frontend.home.includes.head')
@include('Frontend.includes.session-message')

<style>
@font-face {
    font-family: 'gothic';
    src: url('../../Frontend/fonts/gothic.eot');
    src: local('gothic'), url('../../Frontend/fonts/gothic.woff') format('woff'), url('../../Frontend/fonts/gothic.ttf') format('truetype');
}
@font-face {
    font-family: 'Century_Gothic';
    src: url('../../Frontend/fonts/Century-Gothic.ttf') format('truetype'),
         url('../../Frontend/fonts/CenturyGothic.ttf') format('truetype'),
         url('../../Frontend/fonts/gothic.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'Gothic_bold';
    src: url('../../Frontend/fonts/GOTHICB.TTF') format('truetype'),
         url('../../Frontend/fonts/GOTHICB0.TTF') format('truetype');
    font-weight: bold;
    font-style: normal;
}
@font-face {
    font-family: 'Gothic_bold_italic';
    src: url('../../Frontend/fonts/GOTHICBI.TTF') format('woff2');
    font-weight: bold;
    font-style: italic;
}
@font-face {
    font-family: 'Gothic_italic';
    src: url('../../Frontend/fonts/GOTHICI.TTF') format('truetype');
    font-weight: normal;
    font-style: italic;
}

html, body {
    font-family: 'Century_Gothic';
}

body {
    height: auto !important;
}
header {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 9989;
}

.row {
    margin-left:-05px !important;
    margin-right:-05px !important;
}

[class^="icon-"], [class*=" icon-"] {
    font-family: 'icomoon' !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;

    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

/*---------------------LOG-IN REGISTER-SEC START-----------------------------*/
.models-cust {
}
.models-cust .modal-dialog {
    width:400px;
    margin:60px auto 40px;
}
.models-cust > .fade.in, .sole-popup > .fade.in {
    background:rgba(56, 197, 145, 0.9);
}
.models-cust .modal-content, .sole-popup .modal-content {
    background:none;
    box-shadow:none;
    border:1px solid rgba(207,235,245,0.3);
    border-radius:0px;
    cursor:default;
}
.models-cust .modal-header {
    border:none;
    padding:0px;
    min-height:0px;
}
.models-cust .modal-body {
    padding:0px;
}
.main-log-tab .nav-tabs > li > a {
    margin:0px;
    border-radius:0px;
    text-align:center;
    font-size:20px;
    color:#fff;
    letter-spacing:0.5px;
    text-transform:uppercase;
    padding:20px 0;
    border:1px solid transparent;
    transition:all 0.3s linear 0s;-o-transition:all 0.3s linear 0s;-moz-transition:all 0.3s linear 0s;-webkit-transition:all 0.3s linear 0s;
}
.main-log-tab .nav-tabs > li {
    width:50%;
}
.main-log-tab .nav-tabs > li > a:hover {
    background:none;
    border:1px solid transparent;
}
.main-log-tab .nav-tabs > li.active > a, .main-log-tab .nav-tabs > li.active > a:focus, .main-log-tab .nav-tabs > li.active > a:hover {
    background:none;
    border:none;
    color:#fff;
    font-weight:bold;
    border-bottom:3px solid #fff;
}
.cust-devider {
    position:relative;
}
.cust-devider:after {
    height:40px;
    width:1px;
    position:absolute;
    right:-0.5px;
    top:50%;
    margin-top:-20px;
    content:'';
    background:rgba(207,235,245,0.3);
}
.models-cust .nav-tabs {
    border-bottom:1px solid rgba(207,235,245,0.3);
}
.inner-socials {
    position:relative;
    padding:20px;
    text-transform:uppercase;
}
.sol-head {
    position:relative;
    color:#fff;
    font-size:12px;
    text-align:center;
    font-weight:bold;
}
.sol-head:before{
    position:absolute;
    left:2%;
    top:50%;
    height:1px;
    width:100px;
    content:'';
    background:#fff;
}
.sol-head:after {
    position:absolute;
    right:2%;
    top:50%;
    height:1px;
    width:100px;
    content:'';
    background:#fff;
}
.cust-solc {
    margin:15px 0;
}
.cust-solc ul {
    margin:0px;
    padding:0px;
    text-align:center;
}
.cust-solc ul li {
    display:inline-block;
    margin:0 05px;
}
.cust-solc ul li a {
    display:block;
    height:40px;
    width:55px;
    border:1px solid #cfebf6;
    color:#fff;
    line-height:40px;
    border-radius:4px;
    font-size:16px;
    text-align:center;
    transition:all 0.3s linear 0s;-o-transition:all 0.3s linear 0s;-moz-transition:all 0.3s linear 0s;-webkit-transition:all 0.3s linear 0s;
}
.cust-solc ul li a:hover {
    border-radius:0px;
}
.or-head {
    position:relative;
    color:#fff;
    font-size:12px;
    text-align:center;
    font-weight:bold;
}
.or-head:before{
    position:absolute;
    left:2%;
    top:50%;
    height:1px;
    width:150px;
    content:'';
    background:#fff;
}
.or-head:after {
    position:absolute;
    right:2%;
    top:50%;
    height:1px;
    width:150px;
    content:'';
    background:#fff;
}
.loger-form input {
    background:none;
    box-shadow:none;
    border:none;
    height:40px;
    border-bottom:1px solid rgba(207,235,245,0.3);
    padding:0px 30px;
    color:#fff;
    border-radius:0px;
    font-size:14px;
    transition:all 0.3s linear 0s;-o-transition:all 0.3s linear 0s;-moz-transition:all 0.3s linear 0s;-webkit-transition:all 0.3s linear 0s;
}
.loger-form input:hover, .loger-form input:focus {
    box-shadow:none;
    border-bottom:1px solid #fff;
}
.loger-form .form-control::-moz-placeholder {
    text-transform:uppercase;
    color:rgba(255,255,255,0.6);
}
.loger-form .form-control:-moz-placeholder {
    text-transform:uppercase;
    color:rgba(255,255,255,0.6);
}
.loger-form .form-control:-ms-input-placeholder {
    text-transform:uppercase;
    color:rgba(255,255,255,0.6);
}
.loger-form .form-control::-webkit-input-placeholder {
    text-transform:uppercase;
    color:rgba(255,255,255,0.6);
}
.forg-remi {
    padding:0 30px;
    color:#fff;
    margin:10px 0 20px;
}
.forg-remi .reminder-me {
    float:left;
    text-align:left;
}
.forg-remi .reminder-me label {
}
.forg-remi .forgot-pass {
    float:right;
    text-align:right;
}
.forg-remi .checkbox {
    margin:0px;
}
.forgot-pass a {
    color:#fff;
}
.btns-loger {
    text-align:center;
    margin:10px 0 30px;
}
.log-btn {
    font-size:18px;
    color:#fff;
    background:none;
    border-radius:25px;
    padding:0 55px;
    height:45px;
    text-transform:uppercase;
    border:1px solid #fff;
    transition:all 0.3s linear 0s;-o-transition:all 0.3s linear 0s;-moz-transition:all 0.3s linear 0s;-webkit-transition:all 0.3s linear 0s;
}
.log-btn:hover {
    color:#fff;
    background:#207655;
    border:1px solid transparent;
}
.reg-info {
    margin:20px;
    text-align:center;
    font-size:13px;
    color:#fff;
    padding:0 15px;
}
#myModal, #myModal-one,#myForgotModal, #myModal1, #myModal2, #myModal3, #myModal4, #profile-modal, #profile-modal1{
    cursor:url(../Frontend/img/cross-one.png), default;
}

.top-ban-cont-sec {
    background-position: center center;
    position: relative;
    background-size: cover;
    overflow:hidden;
    z-index:5;
}
.top-ban-cont-sec:after {
    position:absolute;
    left:0;
    top:0;
    content:'';
    height:100%;
    width:100%;
    z-index:10;
}
.top-ban-cont-sec {
    padding:8% 0 0;
}
.top-ban-cont-sec .mau-drag img {
    width:100%;
    height:100%;
}
.top-ban-cont-sec:after {
    position:absolute;
    left:0;
    top:0;
    height:100%;
    width:100%;
    background:rgba(56,197,145,0.8);
    content:'';
}
.top-ban-cont-sec:after {
    visibility: hidden;
}
.sub-head {
    font-family: 'Century_Gothic';
    margin-top: 14px;
    margin-bottom: 45px;
}
.sub-head span {
    display: unset;
    text-align: center;
}
.sub-head span span {
    color: #f5de1e;
    display:inline;
}
.mybtn {
    background-color: #fff;
    color: #000;
    font-family: 'Century_Gothic';
    font-size: 25px;
    border-radius: 25px;
    width: 200px;
    height: 50px;
    text-transform: uppercase;
    font-weight: bold;
    margin-bottom: 12px;
}
.mybtn:hover {
    color: #000;
}
.video_logo_image:hover {
    cursor: pointer;
}
.footer-vid-title {
    font-family: 'Century_Gothic';
    color: #f5de1e;
}
.navbar-default .navbar-nav >li > a {
    font-family: 'Century_Gothic';
    color: #f5de1e;
    font-size: 18px;
    padding-right: 25px;
    padding-left: 25px;
}
.navbar-default .navbar-nav >li > a:hover, .navbar-default .navbar-nav >li > a:focus {
    color: #f5de1e;
    /*color: #47af4e;*/
}
.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {
    background-color: unset;
}
.navbar-collapse {
    padding-left: 0px;
}
.navbar-default {
    margin: 0 5px;
}
.navbar-header a img {
    height: 20px;
}
.top-ban-cont-sec {
    padding-top: 8%;
}

#myModal, #myModal-one,#myForgotModal, #myModalForget, #myResetModal {
    cursor:url(../../Frontend/img/cross-one_new.png), default;
}
.models-cust > .fade.in, .sole-popup > .fade.in {
    background-color: #000;
}
#myModal {
    background-color: transparent;
}
.models-cust .modal-content, .sole-popup .modal-content {
    background-color: #fff;
}
.models-cust .nav-tabs {
    border-bottom: 1px solid rgba(69, 69, 69, 0.45);
}
.main-log-tab .nav-tabs > li > a {
    color: #33bb87;
}
.main-log-tab .nav-tabs > li.active > a, .main-log-tab .nav-tabs > li.active > a:focus, .main-log-tab .nav-tabs > li.active > a:hover {
    color: #33bb87;
    border: none;
    border-bottom: 3px solid #33bb87;
}
.cust-devider:after {
    background-color: rgba(69, 69, 69, 0.45);
}
.sol-head, .or-head {
    color: #33bb87;
}
.sol-head:after, .sol-head:before, .or-head:after, .or-head:before {
    background-color: #454545;
}
.cust-solc ul li a {
    border: none;
}
.facebook_btn {
    background-color: #515787;
}
.gmail_btn {
    background-color: #cf4b3c;
}
.twitter_btn {
    background-color: #63a2d5;
}
.loger-form input {
    color: #000;
    border-bottom: 1px solid rgba(69, 69, 69, 0.2);
}
.loger-form input:hover, .loger-form input:focus {
    box-shadow:none;
    border-bottom:1px solid #000;
}
.loger-form input::-webkit-input-placeholder {
    color: rgba(69, 69, 69, 0.5) !important;
}
.loger-form input:-moz-placeholder {
    color: red !important;
}
.loger-form input::-moz-placeholder {
    color: red !important;
}
.loger-form input:-ms-input-placeholder {
    color: red !important;
}
.log-btn {
    background-color: #46b04c;
}
.reg-info,.forg-remi, .forgot-pass a {
    color: #454545;
}
.sub-head {
    font-size: 48px;
    color: #fff;
    text-align:center;
    letter-spacing: 0.5px;
    position: relative;
    line-height:70px;
    margin-bottom:50px;
}

.well {
    font-family: 'Century_Gothic';
    background-color: #454545;
    opacity: 0.8;
    color: #f5de1e;
    font-size: 13px;
    border: none;
    text-align: center;
    padding-left: 30px;
}
.additional_signin {
    color: #fff;
    font-size: 20px;
    text-transform: uppercase;
}

@media (min-width: 768px) {
    .navbar-default .navbar-nav li a:after {
        height: 30px;
        width: 1px;
        position: absolute;
        right: -0.5px;
        top: 50%;
        margin-top: -15px;
        content: '';
        background: #454545;
    }
    .navbar-default .navbar-nav li a:before {
        content: '.';
        font-size: 50px;
        display: inline-block;
        position: absolute;
        bottom: 0.1em;
        left: 0;
        text-align: center;
        width: 100%;
        background-color: transparent;
        color: #454545;
    }
    .sub-head {
        font-size: 48px;
        line-height: 70px;
    }
    .well {
        width: 200px;
        padding: 19px;
    }
    .custom_collapse {
        position: absolute;
    }
    .custom_collapse_last {
        margin-left: -110%;
    }

    .additional_signin {
        visibility: hidden;
    }
}
@media (max-width: 1315px) and (min-width: 1226px) {
    .navbar-default .navbar-nav >li > a {
        padding-right: 18px;
        padding-left: 18px;
    }
}
@media (max-width: 1225px) and (min-width: 1131px) {
    .navbar-default .navbar-nav >li > a {
        padding-right: 12px;
        padding-left: 12px;
    }
}
@media (max-width: 1130px) and (min-width: 1106px) {
    .navbar-default .navbar-nav >li > a {
        padding-right: 10px;
        padding-left: 10px;
    }
}
@media (max-width: 1105px) and (min-width: 1001px) {
    .navbar-default .navbar-nav >li > a {
        font-size: 16px;
        padding-right: 10px;
        padding-left: 10px;
    }
    .navbar-header a img {
        height: 18px;
    }
}
@media (max-width: 1000px) and (min-width: 881px) {
    .navbar-default .navbar-nav >li > a {
        padding-right: 5px;
        padding-left: 5px;
        font-size: 15px;
    }
    .navbar-header a img {
        height: 17px;
    }
}
@media (max-width: 880px) and (min-width: 768px){
    .navbar-default .navbar-nav >li > a {
        font-size: 13px;
        padding-right: 5px;
        padding-left: 5px;
    }
    .navbar-header a img {
        height: 15px;
    }
    .navbar-brand {
        padding-left: 5px;
    }
}
@media (min-width: 1301px) {
    .sub-head {
        font-size: 48px;
        line-height: 70px;
    }
}
@media (max-width: 1300px) {
    .sub-head {
        font-size: 44px;
        line-height: 60px;
    }
}
@media (max-width: 1000px) {
    .top-ban-cont-sec {
        padding-top: 10%;
    }
    .sub-head {
        font-size: 36px;
        line-height: 55px;
    }
}
@media (max-width: 767px) {
    .models-cust .modal-dialog { width:95%; }
    .sol-head:before, .sol-head:after { width: 22%; }
    .or-head:before, .or-head:after { width: 40%; }
    .top-ban-cont-sec {
        padding-top: 15%;
    }
    .navbar-collapse {
        background-color: #000;
    }
    .sub-head {
        font-size: 28px;
        line-height: 32px;
    }
}
@media (max-width: 480px) {
    .top-ban-cont-sec {
        padding-top: 20%;
    }
    .navbar-collapse {
        background-color: #000;
    }
    .atg_logo {
        visibility: hidden;
    }
    .sub-head {
        font-size: 23px;
        line-height: 28px;
    }
    .mybtn {
        font-size: 23px;
        width: 190px;
        height: 45px;
    }
}
@media (max-width: 320px) {
    .sub-head {
        font-size: 19px;
        line-height: 24px;
    }
    .mybtn {
            font-size: 20px;
            width: 180px;
            height: 40px;
    }
}

#trailerdivbox {
    display: none;
    width: 100%;
    height: 100%;
    position: fixed;
    overflow: auto;
}

.videoWrapper {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 8%;
    height: 0;
    text-align: right;
}

.videoWrapper iframe {
    position: absolute;
    max-width: 760px;
    max-height: 420px;
    width: 95%;
    height: 95%;
    left: 0;
    right: 0;
    margin: auto;
}

.video_close_btn {
    color: #fff;
    width: 750px;
    margin: auto;
}

.video_close_btn span:hover, .video_close_btn span:focus {
    cursor: pointer;
}

.video_back:after {
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    opacity: 0.9;
    background: #000;
    z-index: -1;
    content: '';
}

.video_text {
    cursor: pointer;
}

@media (max-width: 790px) {
    .video_close_btn {
        width: unset;
        margin-right: 20px;
    }
}



.cb-slideshow li div {
    position: absolute;
    opacity: 0;
    color: #fff;
    width: 100%;
    text-align: center;
    left: 0;
    -webkit-animation: titleAnimation 36s linear infinite 0s;
    -moz-animation: titleAnimation 36s linear infinite 0s;
    -o-animation: titleAnimation 36s linear infinite 0s;
    -ms-animation: titleAnimation 36s linear infinite 0s;
    animation: titleAnimation 36s linear infinite 0s;
}
.cb-slideshow li:nth-child(2) div {
    -webkit-animation-delay: 6s;
    -moz-animation-delay: 6s;
    -o-animation-delay: 6s;
    -ms-animation-delay: 6s;
    animation-delay: 6s;
}
.cb-slideshow li:nth-child(3) div {
    -webkit-animation-delay: 12s;
    -moz-animation-delay: 12s;
    -o-animation-delay: 12s;
    -ms-animation-delay: 12s;
    animation-delay: 12s;
}
.cb-slideshow li:nth-child(4) div {
    -webkit-animation-delay: 18s;
    -moz-animation-delay: 18s;
    -o-animation-delay: 18s;
    -ms-animation-delay: 18s;
    animation-delay: 18s;
}
.cb-slideshow li:nth-child(5) div {
    -webkit-animation-delay: 24s;
    -moz-animation-delay: 24s;
    -o-animation-delay: 24s;
    -ms-animation-delay: 24s;
    animation-delay: 24s;
}
.cb-slideshow li:nth-child(6) div {
    -webkit-animation-delay: 30s;
    -moz-animation-delay: 30s;
    -o-animation-delay: 30s;
    -ms-animation-delay: 30s;
    animation-delay: 30s;
}
/* Animation for the title */
@-webkit-keyframes titleAnimation {
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-moz-keyframes titleAnimation {
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-o-keyframes titleAnimation {
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@-ms-keyframes titleAnimation {
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}
@keyframes titleAnimation {
    0% { opacity: 0 }
    8% { opacity: 1 }
    17% { opacity: 1 }
    19% { opacity: 0 }
    100% { opacity: 0 }
}



.cb-slideshow li h1 {
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: -1;
    background-size: cover;
    margin: 0;
    background-position: 50% 50%;
    background-repeat: none;
    opacity: 0;
    -webkit-animation: imageAnimation 36s linear infinite 0s;
    -moz-animation: imageAnimation 36s linear infinite 0s;
    -o-animation: imageAnimation 36s linear infinite 0s;
    -ms-animation: imageAnimation 36s linear infinite 0s;
    animation: imageAnimation 36s linear infinite 0s;
}
.cb-slideshow li:nth-child(1) h1 {
}
.cb-slideshow li:nth-child(2) h1 {
    -webkit-animation-delay: 6s;
    -moz-animation-delay: 6s;
    -o-animation-delay: 6s;
    -ms-animation-delay: 6s;
    animation-delay: 6s;
}
.cb-slideshow li:nth-child(3) h1 {
    -webkit-animation-delay: 12s;
    -moz-animation-delay: 12s;
    -o-animation-delay: 12s;
    -ms-animation-delay: 12s;
    animation-delay: 12s;
}
.cb-slideshow li:nth-child(4) h1 {
    -webkit-animation-delay: 18s;
    -moz-animation-delay: 18s;
    -o-animation-delay: 18s;
    -ms-animation-delay: 18s;
    animation-delay: 18s;
}
.cb-slideshow li:nth-child(5) h1 {
    -webkit-animation-delay: 24s;
    -moz-animation-delay: 24s;
    -o-animation-delay: 24s;
    -ms-animation-delay: 24s;
    animation-delay: 24s;
}
.cb-slideshow li:nth-child(6) h1 {
    -webkit-animation-delay: 30s;
    -moz-animation-delay: 30s;
    -o-animation-delay: 30s;
    -ms-animation-delay: 30s;
    animation-delay: 30s;
}
/* Animation for the slideshow images */
@-webkit-keyframes imageAnimation {
    0% { opacity: 0;
    -webkit-animation-timing-function: ease-in; }
    8% { opacity: 0.45;
         -webkit-animation-timing-function: ease-out; }
    17% { opacity: 0.45 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}
@-moz-keyframes imageAnimation {
    0% { opacity: 0;
    -moz-animation-timing-function: ease-in; }
    8% { opacity: 0.45;
         -moz-animation-timing-function: ease-out; }
    17% { opacity: 0.45 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}
@-o-keyframes imageAnimation {
    0% { opacity: 0;
    -o-animation-timing-function: ease-in; }
    8% { opacity: 0.45;
         -o-animation-timing-function: ease-out; }
    17% { opacity: 0.45 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}
@-ms-keyframes imageAnimation {
    0% { opacity: 0;
    -ms-animation-timing-function: ease-in; }
    8% { opacity: 0.45;
         -ms-animation-timing-function: ease-out; }
    17% { opacity: 0.45 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}
@keyframes imageAnimation {
    0% { opacity: 0;
    animation-timing-function: ease-in; }
    8% { opacity: 0.45;
         animation-timing-function: ease-out; }
    17% { opacity: 0.45 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}
.no-cssanimations .cb-slideshow li h1 {
    opacity: 0.45;
}
.extra_dark:after {
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    opacity: 0.25;
    background: #000;
    z-index: -1;
    content: '';
}
</style>

<header style="position: absolute;">
	<div class="row">
		<nav class="navbar navbar-default" style="background-color: transparent; border: 1px solid #454545; z-index: 6; width: 100%;">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nb" id="toggle_button">
      				<div id="navbar-hamburger">
			          	<span class="icon-bar"></span>
			          	<span class="icon-bar"></span>
			          	<span class="icon-bar"></span>
		          	</div>
					<div id="navbar-close" class="hidden">
						<span class="glyphicon glyphicon-remove" style="color: #fff; font-size: 18px"></span>
					</div>
		      	</button>
      			<a class="navbar-brand" href="{{url('/')}}"><img src="{{url('assets/Frontend/img/Shape_13.png')}}"></a>
    		</div>

    		<div class="collapse navbar-collapse" id="nb">
      			<ul class="nav navbar-nav navbar-right" id="toprightnavbar">
			        <li data-toggle="collapse" data-target="#connect_and_share" aria-expanded="false" aria-controls="connect_and_share">
			        	<a href="#">Connect and Share</a>
						<div class="collapse custom_collapse" id="connect_and_share">
						  <div class="well">Connect to people with similar professional or personal interests. Talk about the things you love with people who share your enthusiasm. Share your experiences and knowledge, provide and get feedback. Collaborate and grow.</div>
						</div>
			        </li>
			        <li data-toggle="collapse" data-target="#questions_and_answer" aria-expanded="false" aria-controls="questions_and_answer">
			        	<a href="#">Questions and Answers</a>
			        	<div class="collapse custom_collapse" id="questions_and_answer">
						  <div class="well">Get your questions answered by experts who have been there, done that!</div>
						</div>
			        </li>
			        <li data-toggle="collapse" data-target="#events_and_meetups" aria-expanded="false" aria-controls="events_and_meetups">
			        	<a href="#">Events and Meetups</a>
			        	<div class="collapse custom_collapse" id="events_and_meetups">
						  <div class="well">Remain updated about the latest events and meetups. Meet interesting people who can change your life for good.</div>
						</div>
			        </li>
			        <li data-toggle="collapse" data-target="#jobs" aria-expanded="false" aria-controls="jobs">
			        	<a href="#">Jobs</a>
			        	<div class="collapse custom_collapse" id="jobs">
						  <div class="well">Find jobs relevant to your interests. Switching your domain to find a job that fulfill your dream was never easier.</div>
						</div>
			        </li>
			        <li data-toggle="collapse" data-target="#education" aria-expanded="false" aria-controls="education">
			        	<a href="#">Education</a>
			        	<div class="collapse custom_collapse" id="education">
						  <div class="well">The best free and paid education from the internet, globally or locally curated for you.</div>
						</div>
			        </li>
			        <li>
			        	<a href="" onclick="signupmodalchange('signup');" data-toggle="modal" data-target="#myModal">Signup</a>
			        </li>
			        <li>
			        	<a href="" onclick="signupmodalchange('signin');" data-toggle="modal" data-target="#myModal">Login</a>
			        </li>
		      	</ul>
    		</div>
		</nav>
	</div>
	<script>
		$(document).ready(function() {
    		setTimeout(function(){ $('[data-toggle="tooltip"]').tooltip('hide'); }, 0.1);
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
	<div class="models-cust">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <ul role="tablist" class="nav nav-tabs" id="myTabs">
                                    <li class="cust-devider" role="presentation"><a aria-controls="log-in" data-toggle="tab" role="tab" id="log-in-tab" href="#log-in">SIGN IN</a></li>
                                    <li class="active" role="presentation"><a aria-expanded="true" aria-controls="reg-in" data-toggle="tab" id="reg-in-tab" role="tab" href="#reg-in">SIGN UP</a></li>
                                </ul>


                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade" role="tabpanel">
                                        <form id="frm_login" name="frm_login" action="{{url('/signin')}}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Sign In with social</div>
                                                <div class="cust-solc">
                                                    <ul class="clearfix">
                                                        <li><a href="{{url('/facebook')}}" class="facebook_btn"><i class="fa fa-facebook-f"></i></a></li>
                                                        <li><a href="{{url('/gmail')}}" class="gmail_btn"><i class="fa fa-google-plus"></i></a></li>
                                                        <li><a href="{{url('/twitter')}}" class="twitter_btn"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="or-head">Or</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" name="email" value="{{ (isset($cookie_data['email'])) ? $cookie_data['email'] : '' }}" placeholder="Email address">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password"  id="password" name="password"  class="form-control" value="{{ (isset($cookie_data['password'])) ? $cookie_data['password'] : '' }}"  placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="forg-remi clearfix">
                                                <div class="reminder-me">
                                                    <div class="checkbox hidden-sm hidden-xs">
                                                        <label>
                                                            <input type="checkbox" id="remember" name="remember" <?php if (!empty($cookie_data)) { ?>checked="checked" <?php } ?>>Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="forgot-pass"><a href="javascript:void(0);" onClick="getEmailId();" data-toggle="modal" data-target="#myForgotModal">Reset Password</a></div>
                                            </div>
                                            <div class="btns-loger"><input type="hidden" id="redirect_uri" name="redirect_uri" value="{{ Request::path() }}"><button type="submit" class="btn log-btn">Sign in</button></div>
                                        </form>
                                    </div>
                                    <div aria-labelledby="reg-in-tab" id="reg-in" class="tab-pane fade in active" role="tabpanel">
                                        <div class="inner-socials">
                                            <div class="sol-head">Sign Up with social</div>
                                            <div class="cust-solc">
                                                <ul class="clearfix">
                                                    <li><a href="{{url('/facebook')}}" class="facebook_btn"><i class="fa fa-facebook-f"></i></a></li>
                                                    <li><a href="{{url('/gmail')}}" class="gmail_btn"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="{{url('/twitter')}}" class="twitter_btn"><i class="fa fa-twitter"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="or-head">Or</div>
                                        </div>
                                        <?php Session::put('ruser', Request::get('r', '')); ?>
                                        <form id="frm_user_registration1" name="frm_user_registration1" method="POST" action="{{url('/signup1?r=' . Request::get('r'))}}"  enctype="multipart/form-data">
                                            {!!csrf_field()!!}
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{old('first_name')}}">
                                                    @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{old('last_name')}}">
                                                    @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Address" value="{{old('user_email')}}">
                                                    @if ($errors->has('user_email'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="user_password" name="user_password"  placeholder="password" value="{{old('user_password')}}">
                                                    <!--<div class="reg-info">(Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters)</div>-->
                                                    @if ($errors->has('user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="cnf_user_password" name="cnf_user_password" placeholder="Confirm Password">
                                                    @if ($errors->has('cnf_user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('cnf_user_password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="reg-info">By clicking on the 'Sign up' button you agree to the <a href="{{url('/cms/terms-services')}}"> Terms and conditions </a> and our<a href="{{url('/cms/privacy')}}"> privacy policy</a></div>

                                            <div class="btns-loger">

                                                <button type="submit" name="btn_register" id="btn_register" class="btn log-btn">Sign up</button></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalForget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form id="frm_login" name="frm_login" action="{!!url('/signin')!!}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Signin with social</div>
                                                <div class="cust-solc">
                                                    <ul class="clearfix">
                                                        <li><a href="{!!url('/facebook')!!}" class="facebook_btn"><i class="fa fa-facebook-f"></i></a></li>
                                                        <li><a href="{!!url('/gmail')!!}" class="gmail_btn"><i class="fa fa-google-plus"></i></a></li>
                                                        <li><a href="{!!url('/twitter')!!}" class="twitter_btn"><i class="fa fa-twitter"></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="or-head">Or</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email address">
                                                </div>
                                                <div class="form-group">

                                                    <input type="password"  id="password" name="password"  class="form-control"   placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="forg-remi clearfix">
                                                <div class="reminder-me">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="remember" name="remember" value="1"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="forgot-pass"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModalForget">Forgot Password?</a></div>
                                            </div>

                                            <div class="btns-loger"><button type="submit" id="reset_button" class="btn log-btn">Sign in</button></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="models-cust">
        <div class="modal fade" id="myForgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form id="frm_forgot_send" name="frm_forgot_send" action="{!!url('/resetPassword')!!}" method="post">
                                            {!!csrf_field()!!}
                                            <div class="inner-socials">
                                                <div class="sol-head">Forget Password</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="forgot_email" name="email" placeholder="Email address">
                                                </div>
                                            </div>
                                            <div class="btns-loger"><button type="submit"  class="btn log-btn" >Reset</button></div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('user_code'))

    <div class="models-cust">
        <div class="modal fade" id="myResetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="main-log-tab">
                            <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                <div class="tab-content" id="myTabContent">
                                    <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade in active" role="tabpanel">
                                        <form action="{{url('/')}}/front/reset-password" method="post" name="frm_user_reset" id="frm_user_reset" >
                                            {!!csrf_field()!!}
                                            <input type="hidden" name="activation_code" value="{{Session::get('user_code')}}"/>
                                            <div class="inner-socials">
                                                <div class="sol-head">Reset Password</div>
                                            </div>
                                            <div class="loger-form">
                                                <div class="form-group">
                                                    <input  class="form-control" autofocus type="password" name="user_password" size="" id="main_password" value="" placeholder="New Password" >
                                                </div>
                                                <div class="form-group">
                                                    <input  type="password" class="form-control" name="confim_user_password" id="confim_user_password" value="" placeholder="Confirm Password" >
                                                </div>

                                            </div>
                                            <div class="btns-loger"><button type="submit"  class="btn log-btn" >Reset</button></div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
    <div class="sole-popup">
        <div class="modal fade" id="myModal-one" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
                    </div>
                    <div class="modal-body">
                        <a  href="{!!url('/facebook')!!}" class="btn face-hov"><i class="fa fa-facebook-f"></i> Sign in with Facebook</a>
                        <a href="{!!url('/gmail')!!}" class="btn g-plus"><i class="fa fa-google-plus"></i> Sign in with Google+</a>
                        <a href="{!!url('/twitter')!!}" class="btn twit-hov"><i class="fa fa-twitter"></i> Sign in with Twitter</a>
                        <a class="btn twit-hov" onClick="email_modal()" > Sign in with email and password</a>
                        <div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '124713081587922');
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1"
src="https://www.facebook.com/tr?id=124713081587922&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->



<!-- Google Analytics Code for Logged out user for -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '{{ env('G_ANALYTICS_OUTER_HEADER_KEY') }}', 'auto');
  ga('set', 'dimension1', '0');
  ga('send', 'pageview');

</script>

<script>
    function email_modal() {
        $("#myModal-one").hide();
        $("#myModal").modal("show");
    }
    $(document).ready(function() {
        $('#frm_user_registration1')[0].reset();
    });
    function getEmailId() {
        var email = $('#email').val();
        $('#forgot_email').val(email);
    }

</script>

</header>
<div class="top-ban-cont-sec fullHt" style="background-image:url({{url('assets/Frontend/img/pattren_polygon.png')}}); text-align: center;">
	<ul id="mk" class="cb-slideshow extra_dark" style="list-style: none;">
            <li><h1 class="md1" data-src='{{url("assets/Frontend/img/front_home_images/computer-472016.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/computer-472016.jpg");''></h1></li>
            <li><h1 class="md2" data-src='{{url("assets/Frontend/img/front_home_images/pexels-photo-270837.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/pexels-photo-270837.jpg");'></h1></li>
            <li><h1 class="md3" data-src='{{url("assets/Frontend/img/front_home_images/pexels-photo-190574.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/pexels-photo-190574.jpg");'></h1></li>
            <li><h1 class="md4" data-src='{{url("assets/Frontend/img/front_home_images/pexels-photo-210922.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/pexels-photo-210922.jpg");'></h1></li>
            <li><h1 class="md5" data-src='{{url("assets/Frontend/img/front_home_images/pexels-photo-221446.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/pexels-photo-221446.jpg");'></h1></li>
            <li><h1 class="md6" data-src='{{url("assets/Frontend/img/front_home_images/pexels-photo-260409.jpg")}}' style='
		background-image: url("assets/Frontend/img/front_home_images/pexels-photo-260409.jpg");'></h1></li>
    </ul>
	<div class="container">
		<img src="{{url('assets/Frontend/img/VectorSmartObject.png')}}" class="atg_logo" style="height: 7%; width: 7%;">
		<div class="sub-head wow">
			<span>
				<ul class="cb-slideshow" style="list-style: none; margin-bottom: 0;">
					<li><div>Connect To Professionals</div></li>
					<li><div>Connect To Dancers</div></li>
					<li><div>Connect To Engineers</div></li>
					<li><div>Connect To Musicians</div></li>
					<li><div>Connect To Artists</div></li>
					<li><div>Connect To Athletes</div></li>
				</ul></span><br>
			<span>
				<span>A</span>cross <span>T</span>he <span>G</span>lobe
			</span>
		</div>
		<button type="button" class="btn mybtn" onclick="signupmodalchange('signup');" data-toggle="modal" data-target="#myModal">Sign Up</button><br>
		<div style="margin-bottom: 50px;"><a href="" class="additional_signin" onclick="signupmodalchange('signin');" data-toggle="modal" data-target="#myModal">Log In</a></div>
		<img src="{{url('assets/Frontend/img/video_home.png')}}" id="watchbutton" class="video_logo_image" style="height: 6%; width: 6%; border-radius: 50%;">
		<div class="footer-vid-title"><span style="font-size: 25px;" class="video_text">Play Video</span><br><span class="video_text">know what we do</span></div>

		<header>
			<div id="close">
			  	<div id="trailerdivbox" class="video_back">
			    	<div class="videoWrapper">
			    		<!--<h3 class="video_close_btn"><span>X</span></h3>
			    		<iframe id="video" class="trailervideo" width="760" height="420" data-src="https://www.youtube.com/embed/TUbYY8rpWxc?vq=large&enablejsapi=1&version=3&playerapiid=ytplayer" src="about:blank" frameborder="0" allowscriptaccess="always" allowfullscreen>
			    		</iframe>-->
			    	</div>
			  	</div>
			</div>
		</header>
	</div>
</div>

@include('Frontend.home.includes.footer')
<script>
	function signupmodalchange($type) {
		if($type=='signin') {
		    $('#myModal>div>div>div>div>div>ul li:first').addClass('active');
		    $('#myModal>div>div>div>div>div>ul li:last').removeClass('active');
		    $('#myModal>div>div>div>div>div>ul li:first>a').attr('aria-expanded','true');
		    $('#myModal>div>div>div>div>div>ul li:last>a').removeAttr('aria-expanded');
		    $('#myModal>div>div>div>div>div>div #log-in').addClass('in active');
		    $('#myModal>div>div>div>div>div>div #reg-in').removeClass('in active');
		}
		else if($type=='signup') {
		    $('#myModal>div>div>div>div>div>ul li:first').removeClass('active');
		    $('#myModal>div>div>div>div>div>ul li:last').addClass('active');
		    $('#myModal>div>div>div>div>div>ul li:first>a').removeAttr('aria-expanded');
		    $('#myModal>div>div>div>div>div>ul li:last>a').attr('aria-expanded','true');
		    $('#myModal>div>div>div>div>div>div #log-in').removeClass('in active');
		    $('#myModal>div>div>div>div>div>div #reg-in').addClass('in active');
		}
	}
</script>

<script>
    // Get the modal
	var modal = document.getElementById('trailerdivbox');

	function playVideo() {
		$('.videoWrapper').html('<h3 class="video_close_btn"><span>X</span></h3><iframe id="video" class="trailervideo" width="760" height="420" data-src="https://www.youtube.com/embed/TUbYY8rpWxc?vq=large&enablejsapi=1&version=3&playerapiid=ytplayer" src="about:blank" frameborder="0" allowscriptaccess="always" allowfullscreen>');
	  	var video = document.getElementById('video');
	  	var src = video.dataset.src;
	  	video.src = src + '&autoplay=1';
	  	//$('#video').show();
	}

	function resetVideo() {
		$('#video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
	  	var video = document.getElementById('video');
	  	var src = video.src.replace('&autoplay=1', '&autoplay=0');
	  	video.src = '';
	  	video.src = src;
	  	$('#video').remove();
	  	//$('#video').hide();
	}

	// When the user clicks the button, open the modal
	$('#watchbutton').click(function() {
		$('header').css('z-index','0');
	  	modal.style.display = "block";
	  	playVideo();
	});
	$('.video_text').click(function() {
		$('header').css('z-index','0');
	  	modal.style.display = "block";
	  	playVideo();
	});

	$('#close').click(function() {
	  	modal.style.display = "none";
	  	resetVideo();
		$('header').css('z-index','9989');
	});

	$('.video_close_btn span').click(function() {
	  	modal.style.display = "none";
	  	resetVideo();
		$('header').css('z-index','9989');
	});

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	  	if (event.target == modal) {
	    	modal.style.display = "none";
	    	resetVideo();
			$('header').css('z-index','9989');
	  	}
	}
</script>
<script>
	if($(window).width()>767)
	{
		$(document).ready(function(){
			$('[data-target="#connect_and_share"]').hover(
				function() {$('.custom_collapse').removeClass('in'); $('#connect_and_share').addClass('in');},
				function() {$('#connect_and_share').removeClass('in');} );
			$('[data-target="#questions_and_answer"]').hover(
				function() {$('.custom_collapse').removeClass('in'); $('#questions_and_answer').addClass('in');},
				function() {$('#questions_and_answer').removeClass('in');} );
			$('[data-target="#events_and_meetups"]').hover(
				function() {$('.custom_collapse').removeClass('in'); $('#events_and_meetups').addClass('in');},
				function() {$('#events_and_meetups').removeClass('in');} );
			$('[data-target="#jobs"]').hover(
				function() {$('.custom_collapse').removeClass('in'); $('#jobs').addClass('in');},
				function() {$('#jobs').removeClass('in');} );
			$('[data-target="#education"]').hover(
				function() {$('.custom_collapse').removeClass('in'); $('#education').addClass('in');},
				function() {$('#education').removeClass('in');} );
		});
	}

	if($(window).width()<=767)
	{
		$(document).ready(function(){
			$('[data-target="#connect_and_share"]').click(function(){
				if(!$('#connect_and_share').hasClass('in'))
					$('.custom_collapse').removeClass('in');
			});
			$('[data-target="#questions_and_answer"]').click(function(){
				if(!$('#questions_and_answer').hasClass('in'))
					$('.custom_collapse').removeClass('in');
			});
			$('[data-target="#events_and_meetups"]').click(function(){
				if(!$('#events_and_meetups').hasClass('in'))
					$('.custom_collapse').removeClass('in');
			});
			$('[data-target="#jobs"]').click(function(){
				if(!$('#jobs').hasClass('in'))
					$('.custom_collapse').removeClass('in');
			});
			$('[data-target="#education"]').click(function(){
				if(!$('#education').hasClass('in'))
					$('.custom_collapse').removeClass('in');
			});
		});
	}

	$(function() {
		$('#toggle_button').click(function() {
			$('#navbar-hamburger').toggleClass('hidden');
			$('#navbar-close').toggleClass('hidden');
		});
	});

	$(function() {
		$('#md1, #md2, #md3, #md4, #md5, #md6').sequentialLoad();
		console.log('ok');
	});
</script>
