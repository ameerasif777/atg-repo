<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        @include('Frontend.home.includes.head')
    </head>
    <body>
        <section class="header">
            @include('Frontend.home.includes.header')
        </section>
        @include('Frontend.home.includes.banner')
        <section class="last-menu">
            @include('Frontend.home.includes.last-menu')
        </section>
        <section class="main-section">
            @yield('content')
        </section>
        @include('Frontend.home.includes.footer')
    </body>
    <script src="{{ url('/assets/Frontend/js/jquery.js') }}"></script>
    <script src="{{ url('/assets/Frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('/assets/Frontend/js/custom.js') }}"></script>
</html>
