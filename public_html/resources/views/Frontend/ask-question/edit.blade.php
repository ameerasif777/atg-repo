@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')


<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>

@include('Frontend.includes.froala-editor', ['folderName' => 'qrious_image'])
<style>
    .outer-counter {
        display: none;
        float: left;
        margin-left: 12px;
        background-color: #36bf8a;
        color: white;
        padding: 4px 0px 4px 6px;
        margin-top: 15px;
        padding-right: 8px;
        border-color: #d43f3a;
        vertical-align: middle;
        -webkit-transition: 0.25s linear;
        transition: 0.25s linear;
        height: 28px;
        border-radius: 50%;
    }

    @media only screen and (max-width: 500px) {
        .outer-counter {
            font-size: 12px;
            padding: 1px 0px 0px 3px;
            padding-right: 3px;
            height: 20px;
            margin-top: 30px;
        }
    }
</style>
<script>
    $(document).ready(function () {
        $('.multipleSelect').popSelect({
            showTitle: false,
            autoIncrease: true,
            maxAllowed: 5,
            placeholderText: 'Please select the groups',
            position: 'top',
            note: 'Select a group to share with the community. <br>Do not select any group to share only with connections and followers.'
        });
    });
</script>
<section class="contact-sec"
         style="background-image:url({{ url('/public/assets/Frontend/img/article_back_edit.jpg') }});">
    <div class="container">
        <h1>Edit your Question</h1>
    </div>
</section>
<section class="post-Meet-up-sec">
    <div class="container">
        <div class="text-feder-outer">

            {!! csrf_field() !!}
            <form id="post_edit_question" name="post_edit_question" method="POST"
                  action="{{url('/')}}/front-edit-question/{{ base64_encode($arr_question[0]->id) }}"
                  enctype="multipart/form-data">
                <div class="fealider-area">
                    <div class="row">
                        <div class="col-sm-11 col-xs-10">
                            <div class="title-input">
                                <input type="text" class="form-control" placeholder="Ask Question" id="title" name="title"
                                       value="{{ (old('title'))?old('title'):$arr_question[0]->title }}" onkeydown="limitText(this.form.question,200);"
                                       onkeyup='limitText(this.form.question,200);' onclick="showForm();"
                                       onmouseleave="hideForm();">
                                <input type="hidden" class="form-control" id="old_title" name="old_title"
                                       value="{!! $arr_question[0]->title !!}">
                                <input type="hidden" class="form-control" id="question_id" name="question_id"
                                       value="{!! $arr_question[0]->id!!}">
                                <input type="hidden" id="btn_post" name="btn_post" value="0">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('title') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-2">
                            <div class="outer-counter" id="counters">
                                <span>200</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <textarea class="form-control" name="description" id="description" value=""
                                      style="display: none">{!! old('description',$arr_question[0]->description) !!}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="col-sm-4">
                            <div class="uploar-imger">
                                <div class="uping-imgs uplode-img-cam">
                                    <input type="file" name='profile_image' onchange="readURL(this);" id="profile_image"
                                           class="upit-img" title="Upload Image"/>

                                </div>
                                <div for="profile_pic" class="error"></div>
                                <div class="note_msg">Only jpeg, jpg, png extensions are supported. Maximum file size :
                                    2 MB. (Recommended Dimensions is:1200 X 700px)..!
                                </div>

                                @if($arr_question[0]->profile_image == '')
                                    <div id="cross-icon-1">
                                        <img class="uploding-img-vie" style="" id="blah" src="" alt="" width="100%"
                                             height="200"/>
                                        <a href="javascript:void(0)" style="display: none" onclick="removeimage();"
                                           id="remove_image-1" title="Remove">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </div>
                                @else

                                    <div class="title-input uplink-imgs">
                                        @if($arr_question[0]->cover_type==0)
                                            <img class="uploding-img-vie" style="" id="blah"
                                                src="{{ url('/public/assets/Frontend/images/qrious_image/thumb/'.$arr_question[0]->profile_image) }}"
                                                alt="" width="100%" height="200"/>
                                            <a href="javascript:void(0)" onclick="removeimage();" id="remove_image"  title="Remove">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        @endif

                                       
                                    </div>
                                @endif
                                <input type="hidden" name='last_profile_pic' id="last_profile_pic" class="upit-img"
                                       value=" {{ $arr_question[0]->profile_image }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">


                        <div class="title-sl-input">
                            <label for="">TAGS</label>
                            <input type="text" class="form-control" id="tags" name="tags"
                                   value="{{ $arr_question[0]->tags }}">
                            <div class='TagButtonClass'>(Note: Please press the tab button, after each tag.)</div>
                        </div>
                        </br>
                        <div for="tags" class="error"></div>
                        <div class="title-sl-input">
                            <label for="">GROUP</label>
                            <div class="form-group">

                                <select name="sub_groups[]" id="sub_groups" multiple=""
                                        class="form-control multipleSelect" size="{{count($user_group_data)}}">
                                    @foreach($user_group_data as $key=>$value)
                                        <option value="{!! $value->id !!}"
                                                @if(in_array($value->id,$arr_selected_groups)) selected @endif >{!! $value->group_name !!}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="boter-texter-feat clearfix">
                            <div class="pull-right">
                                <button type="submit" id="btn_post" name="btn_post" value="0" class="btn"
                                        onclick="getDescription()" onmouseover="findimginpost()">@if($arr_question[0]->status == '1')Post @else
                                        Publish @endif</button>
                                <a href="{{ url('/') }}/mypost" class="btn">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <input type="hidden" id="cover_type" name="cover_type" value="">
                <input type="hidden" id="cover_url" name="cover_url" value="">
            </form>
        </div>
    </div>
</section>
@include('Frontend.includes.footer')

<script src="{{url('/assets/Frontend/js/imgvideoaudioext.js')}}"></script>
<script>
    $('#tags').tokenfield({
        autocomplete: {
            delay: 100
        },
        showAutocompleteOnFocus: true
    })
    function getDescription() {
        $('#description').val($('textarea#description').froalaEditor('html.get'));
    }
    jQuery(document).ready(function () {
        $("#frm_content_image").validate({
            ignore: '.hidden',
            errorElement: 'div',
            rules: {
                image_width: {
                    digits: true,
                    max: 1200
                },
                image_height: {
                    digits: true,
                    max: 1200
                },
            }
        });

        $("#post_edit_question").validate({
            errorElement: 'div',
            ignore: '.hidden',
            rules: {
                title: {
                    required: true,
                    remote: {
                        url: "{{url('/')}}/check-question-title-edit",
                        type: "post",
                        data: {
                            'old_title': function (ele) {
                                return $('#old_title').val();
                            }
                        },
                    }
                },
                post_short_description: {
                    required: true,
                    maxlength: 200
                },
                description: {
                    required: true,
                },
            },
            messages: {
                title: {
                    required: "Please enter question.",
                    remote: "Question with same name already exist."
                },
                post_short_description: {
                    required: "Please enter question short description.",
                    maxlength: "Please enter maximum 200 charecter."

                },
                description: {
                    required: "Please enter question description.",
                },
                submitHandler: function (form) {
                    $("#btn_post").hide();
                }
            },
        });

        var q=$('#title').val().length;
        var v=200-q;
        $('#counters').html('<span>' + v + '</span>');
    });

    function readURL(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').css("display", "block");
                        $('.uping-imgs').css("display", "none");
                        $('.note_msg').css("display", "none");
                        $('#blah').attr('src', e.target.result);
                        $('#remove_image').css("display", "block");
                        $('#remove_image-1').css("display", "block");
                        $('#cross-icon-1').addClass("title-input uplink-imgs");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }

    }
    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#profile_pic").prop("value", "");
        $("#last_profile_pic").prop("value", "");
        $('#remove_image-1').css("display", "none");
        $('.uping-imgs').css("display", "block");
        $('.note_msg').css("display", "block");
    }

    function readImage(input) {
        var index = $(input).data("index");
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#guest_img_div' + index).css("display", "block");
                        $('#guest_image_src' + index).attr('src', e.target.result);
                        $('#remove_img' + index).css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }

    }
    setInterval(function () {
        var title = $("#title").val();
        if (title != '') {
            autoSave();
        }
    }, 10000);

    function autoSave() {
        var id = $("#question_id").val();
        var sub_groups = $("#sub_groups").val();
        $.ajax({
            url: $("#base_url").val() + "/front-edit-question/" + id,
            method: 'post',
            data: {
                id: id,
                sub_groups: sub_groups,
                title: $("#title").val(),
                description: $('textarea#description').froalaEditor('html.get'),
                tags: $("#tags").val(),
                btn_post: 1,
                auto_save_flag: "true",
                cover_type: $('#cover_type').val(),
                cover_url:$('#cover_url').val(),
                
            },
            success: function (id) {
                $("#save_loader").show();
                setInterval(function () {
                    $("#save_loader").hide();
                }, 4000);
            }
        });
    }

    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            $('#counters').html('<span>0</span>');
            limitField.value = limitField.value.substring(0, limitNum);
            $('.title-input input').css({'border-bottom': '1px solid #e50505'});
            return false;
        } else {
            var v = limitNum - limitField.value.length;
            $('#counters').html('<span>' + v + '</span>');
            $('.title-input input').css({'border-bottom': '1px solid #66afe9'});
        }
        $('#counters').css({'display': 'block'});
    }
    function showForm() {
        $('#counters').css({'display': 'block'});
    }

    function hideForm() {
        $('#counters').css({'display': 'none'});
    }

    document.addEventListener('keydown', function (e) {
        if (e.keyCode === 9) {
            hideForm();
        }
    });

</script>
