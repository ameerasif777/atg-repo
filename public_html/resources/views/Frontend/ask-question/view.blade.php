<?php $session_data = Session::get('user_account'); ?>
@include('Frontend.includes.head')
@if($session_data != "")
@include('Frontend.includes.header')
@else
@include('Frontend.includes.outer-header',['sign_up'=>true])
@endif
@include('Frontend.includes.session-message')

@if($session_data == '')
@include('Frontend.includes.fixed_signup_footerbutton',['get_groups'=>$get_groups])
@endif

<?php
$image_path = config('feature_image_path').'qrious_image/' . $qriousData[0]->profile_image;
if (Storage::exists($image_path) && $qriousData[0]->profile_image != '') {
    $post_image_path = config('feature_pic_url').'qrious_image/' . $qriousData[0]->profile_image;
} else {
    $post_image_path = config('img').'image-not-found3.jpg';
}

$meta_desc = fixtags($finalData['header']['meta_description']);

function fixtags($text) {
    $text = htmlspecialchars($text);
    $text = preg_replace("/=/", "=\"\"", $text);
    $text = preg_replace("/&quot;/", "&quot;\"", $text);
    $tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
    $replacement = "<$1$2$3$4$5$6$7$8$9$10>";
    $text = preg_replace($tags, $replacement, $text);
    $text = preg_replace("/=\"\"/", "=", $text);
    return $text;
}
?>

<link href="{{ url('/public/assets/css/views/post_detail.css') }}" rel="stylesheet" type="text/css"/>
<meta property="og:title" content="{{$finalData['header']['meta_title']}}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{Request::fullUrl()}}">
<meta property="og:tags" content='{{$finalData["header"]["meta_keywords"]}}'>
<meta name="keywords" content="@if(isset($qriousData[0]->tags) && $qriousData[0]->tags != ''){{$qriousData[0]->tags}} @else {{$qriousData[0]->title}} @endif">
<meta name="description" content="{{$finalData['header']['meta_title']}}">
<meta property="og:image" content="{{$post_image_path}}">

@include('Frontend.includes.post_detail_mention_user_ids_top')
<!-------------------------------------------------------LOG-STEP-ONE START------------------------------------------------------>
<section class="contact-sec" style="background-image:url({{config('img')}}article_back_edit.jpg);">
    <?php $title_of_post = $qriousData[0]->title ?>
    @include('Frontend.includes.post_detail_title', [ 'feature' => "qrious?", 'title' => $title_of_post ])
</section>
<style type="text/css">
    .btn-group > .btn:first-child {
        border-bottom-left-radius: 1px solid #33bb87;
        border-top-left-radius: 1px solid #33bb87;
    }
    .btn-group > .btn:last-child {
        border-bottom-right-radius: 1px solid #33bb87;
        border-top-right-radius: 1px solid #33bb87;
    }
    .dow-sav-like-btn .btn {
        font-size: 10px;
    }
</style>
<section class="artical-post-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">

                @include('Frontend.includes.google_ads', ['ads_public_only' => 1])
                <p style="font-size: medium; color: red; font-style: italic">
                    @if($response == '2')
                    <b>&nbsp;*Your Event has been promoted successfully !</b>
                    @elseif($response == '3')
                    <b>&nbsp;*Your payment has failed for some reason. Please try again to promote.</b>
                    @endif
                </p>
                <div class="artical-leftyy">
                    <div class="art-bane">
                        <!--<input type="hidden" id="" value="{{ $finalData['user_session']['id'] }}">--> 
                        <?php
                        if($qriousData[0]->cover_type==0){
                            $image_path = config('feature_image_path').'qrious_image/' . $qriousData[0]->profile_image;
                            if (Storage::exists($image_path) && $qriousData[0]->profile_image != '') {
                                $post_image_path = config('feature_pic_url').'qrious_image/' . $qriousData[0]->profile_image;
                            } else {
                                $post_image_path = '';
                            }
                        }
                        
                    
                        ?>
                        @if($qriousData[0]->cover_type==0 )
                            @if($post_image_path != '')
                            <img src="{{ $post_image_path }}" width="100%" >
                            @endif
                       
                        @endif
                    </div>
                    <p>{!! ucfirst($qriousData[0]->description) !!}</p>
                </div>
                <!--for audiance count-->
                <input type="hidden" name="to_id_fk" id="to_id_fk" value="{{$qriousData[0]->user_id_fk }}">
                <input type="hidden" name="question_id" id="question_id" value="{{ $qriousData[0]->id }}">
                <!--end-->   

                <div class="editable-shareable">
                    @include('Frontend.includes.post_detail_tags_edit',['tags'=>'tags','feature' => 'qrious'])

                    @if(count($get_groups) > 0)
                    <div class="boter-hash-taber">
                        <span>Posted IN :</span>
                        <?php $temp_grp_array = array(); ?>
                        <ul class="clearfix">
                            @foreach($get_groups as $group)
                            @if($session_data != "")                        
                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                            <?php
                            $temp_grp_array[] = $group->id;
                            ?>
                            <li><span class="cust-posterd" style="cursor:pointer;" title='Click to Report' class="btn repot">{{ $group->group_name }}<a onclick="getGroupName('{{$group->id}}')"> × </a></span></li>                                                   
                            @else
                            <li><span class="cust-posterd" style="cursor:pointer;" class="btn repot">{{ $group->group_name }}<a onclick="getGroupName('{{$group->id}}')"> × </a></span></li>                                                   
                            @endif                                                  
                            @else
                            <li>
                                <span class="cust-posterd" style="cursor:pointer;" href="javascript:void(0)" title='Click to Report' class="btn repot">{{ $group->group_name }}<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"> × </a></span>
                            </li> 
                            @endif
                            @endforeach
                            <?php
                            $tt = implode(',', $temp_grp_array);
                            ?>
                            <input type="hidden" id="group_ids" name="group_ids[]" value="{{$tt}}">
                        </ul>
                    </div>
                    @endif
                    @if($arr_user1_data['id'] != $arr_user_data['id'])
                    <!-- Model For Posted In -->
                    <div class="modal fade"  id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog"  role="document">
                            <div class="modal-content report-pop">
                                <form name="group_report" id="group_report">
                                    <div class="modal-body">
                                        <div class="report-infos-cont">
                                            <h1>Report</h1>
                                            <ul>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="0"> 
                                                    <label>Spam</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="1">
                                                    <label>Uninteresting</label>
                                                </li>
                                                <li>
                                                    <input type="radio" name="report" id="get_group_report" value="2">
                                                    <label>Hatred or Vulgarity</label>
                                                </li>                                                                
                                                @if(count($get_groups) > 0)
                                                <li>
                                                    <input type="radio" name="report" id="make_group" value="">
                                                    <label>Does not belong in</label>
                                                    <select name="sub_group" id="make_sub_group" disabled="" class="form-control">
                                                        @foreach($get_groups as $group)
                                                        <option value="{{$group->id}}">{{$group->group_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </li>
                                                @endif
                                                <label class="error" for="report"></label>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="boter-texter-feat modal-footer">
                                        <div class="pull-right">
                                            <button type="button" id="group_report_ok" class="btn" onclick="makeGroupReport({{ $qriousData[0]->id }})" >Post</button>
                                            <button type="button" class="btn" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                @include('Frontend.includes.google_ads', ['ads_public_only' => 0])

                <div class="arti-coment-div">
                    <h3 class="clearfix">Answers

                    </h3>
                    <h4 class="clearfix">

                        <span>{{{isset($array_question[0]->count)?$array_question[0]->count:'0' }}}</span> Answers

                    </h4>
                    <div class="post-arti-comt">
                        <div class="media">
                            <form name="form_detail_question" id="form_detail_question" method="post" action="{{ url('/') }}/question-answer" enctype="multipart/form-data">
                                <div class="media-left">
                                    <div class="post-img">
                                        @if($session_data != "")
                                        @if(isset($arr_user_data['profile_picture']) != 0)
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user_data['id'])}}" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{ config('profile_pic_url').$arr_user_data['id'].'/thumb/'.$arr_user_data['profile_picture'] }}" @if($arr_user_data['gender'] == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif/>
                                        </a>   
                                        @elseif($arr_user_data['gender'] == 0)
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user_data['id'])}}" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{ config('avatar_male') }}" alt="profile"/>
                                        </a>
                                        @else
                                        <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($arr_user_data['id'])}}" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{ config('avatar_female') }}" alt="profile"/>
                                        </a>
                                        @endif                       
                                        @else
                                        <a class="more" title="Visit Page" data-placement="left">
                                            <img  id="main_profile" src="{{ config('avatar_male') }}" alt="profile"/>
                                        </a>
                                        @endif
                                    </div>    
                                </div>

                                <div class="media-body">
                                    <div class="input-group">
                                        <textarea class="form-control mention" name="answer" id="answer" placeholder="comment here..." style="height: 55px;">{{old('answer')}}</textarea>                   
                                        <input type="hidden" name="user_id" value="{{ $finalData['user_session']['id'] }}">
                                        <input type="hidden" name="post_question_id" value="{{ ucfirst($array_question[0]->id) }}">
                                        <input type="hidden" name="post_question_name" value="{{ ucfirst($array_question[0]->title) }}">
                                        <span class="input-group-btn">   
                                            @if($session_data != "")
                                                <button type="submit" class="btn btn-default" name="btn_answer" value="POST ANSWER">
                                            @else
                                                <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-default"  value="POST ANSWER">
                                            @endif
                                            <i class='fa fa-paper-plane' aria-hidden='true' style='font-size:25px;'></i>
                                            </button>
                                        </span>                                        
                                    </div>
                                    <div class="comment-pic">
                                        @if($session_data != "")
                                        <input type="file" name='answer_pic' onchange="readURL(this);"  id="answer_pic" class="upit-imgse" title="Upload Image" />
                                        @else
                                        <input data-toggle="modal" data-target="#myModal" class="upit-imgse" title="Upload Image" />
                                        @endif
                                    </div>
                                    @if ($errors->has('answer'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('answer') }}</strong>
                                    </span>
                                    @endif
                                    <label for="answer" class="error"></label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="posted-arti-comment mCustomScrollbar">
                        @if(isset($array_question[0]->answer)?$array_question[0]->answer:'')
                        @foreach ($array_question[0]->answer as $value)
                        <div class="media">
                            <div class="media-up" id="{{ $value->id }}" style='float:left;'>
                                @if($session_data != "")
                                <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->answered_by)}}" title="Visit Page" data-placement="left">
                                    <div class="post-ari-img"></span><img src="{{config('profile_pic_url')}}{{$value->answered_by}}/thumb/{{$value->profile_picture}}" alt="profile" @if($array_question[0]->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif/></div>
                                </a>
                                @else
                                <a class="more" title="Visit Page" data-placement="left">
                                    <div class="post-ari-img">
                                        <img  id="main_profile" src="{{config('profile_pic_url')}}{{$value->answered_by}}/thumb/{{$value->profile_picture}}" alt="profile" @if($array_question[0]->gender == 0) onerror="src={{ config('avatar_male') }}" @else onerror="src={{ config('avatar_female') }}" @endif alt="profile"/>
                                    </div>
                                </a>
                                @endif
                            </div>
                            <!-- <div class='media-up-right'>-->

                            <h4 class="media-heading">
                                <a class="more" href="{{url('/')}}/user-profile/{{base64_encode($value->answered_by)}}" title="Visit Page" style="color:black;">
                                    <?php
                                    if ($value->user_name != ''):
                                        echo $value->user_name;
                                    else:
                                        echo $value->first_name;
                                    endif;
                                    ?>
                                </a>
                                <span id="onbeforedate"> on <span>
                                        <span class="date_display">
                                            {{  date("l, jS F Y", strtotime("$value->created_at")) }}
                                        </span> 
                                        <span class="pull-right"></span>
                                        </h4>
                                        <!--</div>-->
                                        <div class="media-body">                                         

                                            <p style="text-align: justify; padding-right: 5%;">{!! $value->answer !!} 

                                            </p>
                                            <!--<div aria-label="..." role="group" class="btn-group comment-up-dp">
            
                                                <button id="like" name="like" class="btn {{(isset($value->is_upvotes) && count($value->is_upvotes)>0 && $value->is_upvotes[0]->status=='0') ? 'acti-up-dw disabled' : ''}}" onclick="upvoteAnswerDownvote({{$value-> id }}, 0);"><i class="fa fa-thumbs-o-up"> </i><span class="btn-counters">{{(isset($value->total_upvotes)) ? $value->total_upvotes : '0'}}</span></button>
                                                <button id="unlike" name="attend" class="btn {{(isset($value->is_upvotes) && count($value->is_upvotes)>0 && $value->is_upvotes[0]->status=='1') ? 'acti-up-dw disabled' : ''}}" onclick="upvoteAnswerDownvote({{$value-> id}}, 1);"><i class="fa fa-thumbs-o-down"></i> <span class="btn-counters">{{(isset($value->total_downvotes)) ? $value->total_downvotes : '0'}}</span></button>
                                                
                                            </div>-->

                                            @if($value->answer_image != '')
                                            <p><img src="{{config('feature_pic_url')}}question_answer_images/thumb/{{$value->answer_image}}"  onerror="src={{config('img')}}image-not-found.png"/></p>
                                            @endif
                                        </div>
                                        </div>

                                        @endforeach
                                        @endif
                                        </div>
                                        </div>         
                                        </div>
                                        <div class="col-sm-3">

                                            @include('Frontend.includes.google_ads', ['ads_public_only' => 1])

                                            <div class="right-aticalss">

                                                <!-- User Profile Image , Follow Section -->
                                                @include('Frontend.includes.post_detail_user_profile_image', [ 'feature' => "question" , 'time' => $qriousData[0]->created_at ])
                                                @if($arr_user1_data['id'] != $arr_user_data['id'])
                                                <div class="dow-sav-like-btn">
                                                    <div class="btn-group" role="group" aria-label="..." id="up-down-votes">
                                                        @if($session_data != "")
                                                        <button id="like" name="like" class="btn {{(isset($is_upvotes) && count($is_upvotes)>0 && $is_upvotes[0]->status=='0') ? 'acti-up-dw disabled' : ''}}" onclick="upvoteDownvote({{ $qriousData[0]-> id }}, 0);"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters">{{(isset($total_upvotes)) ? $total_upvotes : '0'}}</span></button>
                                                        <button id="unlike" name="attend" class="btn {{(isset($is_upvotes) && count($is_upvotes)>0 && $is_upvotes[0]->status=='1') ? 'acti-up-dw disabled' : ''}}" onclick="upvoteDownvote({{ $qriousData[0]-> id }}, 1);"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters">{{(isset($total_downvotes)) ? $total_downvotes : '0'}}</span></button>
                                                        @if(isset($is_postfollower) && count($is_postfollower)>0 && $is_postfollower[0]->status=='0')
                                                        <button id="followPost" name="followPost" class="btn {{(isset($is_postfollower) && count($is_postfollower)>0 && 
                                                $is_postfollower[0]->status=='0') ? '' : ''}}" onclick = "addFollowPost('{!!$array_question[0]->id!!}', 1,
                                                            2);"><i class="fa fa-foursquare"></i> Unfollow <span class="btn-counters">{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></button>
                                                        @else
                                                        <button id="followPost" name="followPost" class="btn {{(isset($is_postfollower) && count($is_postfollower)>0 && 
                                                $is_postfollower[0]->status=='1') ? '' : ''}}" 
                                                                onclick = "addFollowPost('{!!$array_question[0]->id!!}', 0, 1);"><i class="fa fa-foursquare"></i> Follow <span class="btn-counters">{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></button>
                                                        @endif
                                                        @else
                                                        <button class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-thumbs-o-up"></i> Up Vote <span class="btn-counters">{{(isset($total_upvotes)) ? $total_upvotes : '0'}}</span></button>
                                                        <button class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-thumbs-o-down"></i> Down Vote <span class="btn-counters">{{(isset($total_downvotes)) ? $total_downvotes : '0'}}</span></button>

                                                        @if(isset($is_postfollower) && count($is_postfollower)>0 && $is_postfollower[0]->status=='0')
                                                        <a  class="btn btnPost" href='javascript:void(0)' data-toggle="modal" data-target="#myModal" title="Click to follow"><i class="fa fa-foursquare"></i> follow <span class="sas-counter btn-counters follower_count" id='follower_count'>{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></a>
                                                        @else
                                                        <a  class="btn btnPost" href='javascript:void(0)' 
                                                            data-toggle="modal" data-target="#myModal" title="Click to 
                                                            unfollow"><i class="fa fa-foursquare"></i> follow <span class="sas-counter btn-counters follower_count" id='follower_count'>{{(isset($total_followPost)) ? $total_followPost : '0'}}</span></a>

                                                        @endif

                                                        @endif
                                                    </div>
                                                    <input type="hidden" name="follower_user_id" value="{{$arr_user1_data['id']}}" id="follower_user_id"> 
                                                </div>
                                                @endif

                                                <!-- PROMOTE POST -->
                                                @if($arr_user1_data['id'] == $arr_user_data['id'] && $qriousData[0]->status == '1')
                                                @if(count($promotion)>0)
                                                
                                                    <div class="dow-sav-like-btn"> 
                                                        <button  class="btn" style="width:100%; font-size: medium; white-space: normal;" ><i class="fa fa-bullhorn fa-lg"></i>&nbsp;POST Promoted From {{$promotion[0]->promo_start_date}} To {{$promotion[0]->promo_end_date}}</button>
                                                    </div>
                                                
                                                @else
                                                
                                                    <div class="dow-sav-like-btn"> 
                                                        <button  class="btn" style="width:100%; font-size: medium; white-space: normal"  data-toggle="modal" data-target="#promote-modal"><i class="fa fa-bullhorn fa-lg"></i>&nbsp;PROMOTE YOUR POST</button>
                                                    </div>
                                                


                                                <div id="promote-modal" style="display:none;" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                                    <div class="modal-dialog"  role="document">
                                                        <div class="modal-content report-pop">
                                                            <form  name="promption-frm" id="promption-frm" action="{{url('/')}}/order/promo" method="POST" enctype="multipart/form-data">
                                                                <div class="modal-body">
                                                                    <div class="report-infos-cont"> 
                                                                        <h1>PROMOTE YOUR POST</h1>
                                                                        <div style="padding:10px">
                                                                            <div class="even-det">
                                                                                <span class="help-block">Get more people to view your post..</span>
                                                                            </div>
                                                                            <div class="clearfix"></div>  <br/>    
                                                                            <div class="even-det">
                                                                                <div class="col-md-6">
                                                                                    <label for="">Select Budget: </label>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <select id="budget" name="budget" class="form-control offset-top-15" onchange="onSelectPromotion($(this).val());">
                                                                                        <option value=''>--Select--</option>
                                                                                        @foreach($promotion_det as $key => $value)
                                                                                        <option value='{{$value->budget}}'>{{$value->currency}} {{$value->budget}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div><br/>   
                                                                            <div class="even-det">
                                                                                <div class="col-md-6">
                                                                                    <label for="">Currency: </label>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <select class="form-control offset-top-15" id='currency' name='currency'>
                                                                                        <option val='INR' selected>INR</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>  
                                                                            <div class="clearfix"></div>   <br/> 
                                                                            <div class="even-det">
                                                                                <div class="col-md-6">
                                                                                    <label for="">Duration: </label>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <input id="duration" name="duration" class="form-control offset-bot-14 " type="text"  readonly="readonly"/>
                                                                                </div>
                                                                                <div class="col-md-1">Days</div>

                                                                            </div>  
                                                                            <div class="clearfix"></div>   <br/>
                                                                        </div>

                                                                    </div>   
                                                                </div>
                                                                <div class="boter-texter-feat modal-footer">
                                                                    <div class="pull-right">
                                                                        <button type="submit" id="promo_ok" class="btn" disabled>Promote</button>
                                                                        <button type="button"  class="btn" data-dismiss="modal" onclick="$('#promote-modal').hide();" >Close</button>
                                                                    </div>
                                                                </div>
                                                                <input id="post-id" name="post-id" type="hidden" value="{{ $qriousData[0]->id }}" />
                                                                <input id="post-type" name="post-type" type="hidden"  value="question" />

                                                            </form>
                                                        </div>
                                                    </div                                   
                                                </div>
                                            </div>
                                            @endif
                                            @endif
                                            <!-- END OF PROMOTE POST -->

                                            <!-- SHARE SECTION -->
                                            @if($arr_user1_data['id'] != $arr_user_data['id'])
                                                <div class="arti-share-btns">
                                            @else
                                                <div class="arti-share-btns even-post-art">
                                            @endif
                                            @include('Frontend.includes.post_detail_share')
                                            </div>


                                                <div class="art-joint-btn">
                                                    <div class="btn-group sat-btn-group" role="group" aria-label="...">
                                                        <!--<button type="button" class="btn repot">Report</button>-->
                                                        @if($session_data != "")
                                                        @if($arr_user1_data['id'] != $arr_user_data['id'])                                        
                                                        @if(count($is_reported) == 0)
                                                        <a  href="javascript:void(0)" class="btn repot" id="report_disabled" data-toggle="modal" data-target="#myModal">
                                                            Report
                                                        </a>
                                                        @else
                                                        <a disabled href="javascript:void(0)" class="btn repot"  data-toggle="modal" data-target="#myModal">
                                                            Report
                                                        </a>
                                                        @endif                                       
                                                        @endif
                                                        @else
                                                        <a href="javascript:void(0)" class="btn repot"  data-toggle="modal" data-target="#myModal">
                                                            Report
                                                        </a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- Model For Report -->
                                                <div class="modal fade"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog"  role="document">
                                                        <div class="modal-content report-pop">
                                                            <form name="report" id="report">
                                                                <div class="modal-body">
                                                                    <div class="report-infos-cont">
                                                                        <h1>Report</h1>
                                                                        <ul>
                                                                            <li>
                                                                                <input type="radio" name="report" id="get_report" value="0"> 
                                                                                <label>Spam</label>
                                                                            </li>
                                                                            <li>
                                                                                <input type="radio" name="report" id="get_report" value="1">
                                                                                <label>Uninteresting</label>
                                                                            </li>
                                                                            <li>
                                                                                <input type="radio" name="report" id="get_report" value="2">
                                                                                <label>Hatred or Vulgarity</label>
                                                                            </li>                                                                       
                                                                            @if(count($get_groups) > 0)

                                                                            @endif
                                                                            <label class="error" for="report"></label>
                                                                        </ul>
                                                                    </div>

                                                                </div>
                                                                <div class="boter-texter-feat modal-footer">
                                                                    <div class="pull-right">
                                                                        <button type="button" id="report_ok" class="btn" onclick="check({{ $qriousData[0]->id }})" >Post</button>
                                                                        <button type="submit" class="btn" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(count($related_question) > 0)
                                                @include('Frontend.includes.related_posts',['post_name'=>'Questions','related_posts'=>$related_question])
                                                @endif
                                            </div>
                                        </div>


                                        </div>
                                        </div>
                                        </section>


                                        @include('Frontend.includes.footer')



                                        <input type="hidden" value="{{ config('feature_pic_url').'qrious_image/' . $qriousData[0]->profile_image }}" id="image_url">
                                        <input type="hidden" value="{{ url('/') . '/view-question/' . '-' . $qriousData[0]->id }}" id="page_link">
                                        <input type="hidden" value="{{ $qriousData[0]->title }}" id="question_title">
                                        <input type="hidden" value="{{ $qriousData[0]->description }}" id="question_description">

                                        <input type="hidden" value="{{$finalData['user_session']['id']}}" id="user_id">

                                        <!-- Share Article Code ; Including share_post file and passing feature as argument -->
                                        @include('Frontend.includes.post_detail_share_fx', [ 'feature' => "question"])

<script>
    function onSelectPromotion(cost)
    {
    if (cost == '')
    {
    $('#duration').val('');
    $('#promo_ok').prop('disabled', true);
    }
    else
    {
    var ar = <?php echo json_encode($promotion_det); ?>;
    for (var i = 0; i < ar.length; i++)
    {
    if (ar[i]['budget'] == cost)
    {
    $('#duration').val(ar[i]['duration']);
    $('#promo_ok').prop('disabled', false);
    break;
    }
    }
    }
    }


    $(document).ready(function() {

    if ('{{$response}}' == 'p')
    {
    $("#promote-modal").modal();
    }

    $('textarea#answer').mentionsInput({
    elastic : false,
            onDataRequest:function (mode, query, callback) {
            $.getJSON(base_url + '/get-username-tagging?q=' + query, function(responseData) {
            responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > - 1 });
            callback.call(this, responseData);
            });
            }
    });
    var question_id = $("#question_id").val();
    var to_id_fk = $("#to_id_fk").val();
    var type = '6';
    var base_url = jQuery("#base_url").val();
    $.ajax({
    url: base_url + '/viewer',
            method: 'get',
            data: {
            question_id: question_id,
                    to_question_visitor: to_id_fk,
                    type: type,
            },
            success: function(result) {
            $('.audience_count').html('<b>AUDIENCE : ' + result + '</b>');
            }
    });
    
    @if ($arr_user1_data['id'] == $arr_user_data['id'])
        @if (count($promotion) > 0)
            $.ajax({
            url: base_url + '/viewer',
                    method: 'get',
                    data: {
                            question_id: question_id,
                            to_question_visitor: to_id_fk,
                            type: type,
                            flag: '0',
                            start_date: '{{$promotion[0]->promo_start_date}}',
                            end_date: '{{$promotion[0]->promo_end_date}}'
                    },
                    success: function (result) {
                    $('#clicks').html(result);
                    }
            });
            $.ajax({
            url: base_url + '/viewer',
                    method: 'get',
                    data: {
                            question_id: question_id,
                            to_question_visitor: to_id_fk,
                            type: type,
                            flag: '1',
                            start_date: '{{$promotion[0]->promo_start_date}}',
                            end_date: '{{$promotion[0]->promo_end_date}}'
                    },
                    success: function (result) {
                    $('#views').html(result);
                    }
            });
        @endif
    @endif

    });
    function repost(question_id, created_at){
    swal({
    title: "Are you sure want to repost this Question?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Repost",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
    },
            function(isConfirm) {
            if (isConfirm) {
            $.ajax({
            type: "get",
                    url: '{!!url("/")!!}/front-question-repost/' + btoa(question_id) + '/' + btoa(created_at),
                    dataType: "json",
                    success: function(data) {
                    document.location = "{!!url('/')!!}/front-edit-question/" + btoa(question_id);
                    if (data.msg == '0'){
                    }
                    }
            });
            } else {
            swal("Cancelled", "You Cancelled", "error");
            }
            });
    }
    function upvoteDownvote(question_id, status) {
    var group_ids = $('#group_ids').val();
    $('.grey-overlay-loader').show();
    $.ajax({
    type: "post",
            url: '{!!url("/")!!}/up-down-vote-question',
            data: {
            question_id:question_id,
                    status:status,
                    group_ids:group_ids
            },
            success: function(data) {
            $('#up-down-votes').html(data);
            $('.grey-overlay-loader').hide();
            }
    })
    }

    function addFollowPost(question_id, status, flag) {
    var follower_user_id = $('#follower_user_id').val();
    $("#like").removeClass("acti-up-dw");
    $("#unlike").removeClass("acti-up-dw");
    $("#followPost").addClass("acti-up-dw");
    $('.grey-overlay-loader').show();
    $("#followPost").css("cursor", "wait");
    $.ajax({
    type: "post",
            url: '{!!url("/")!!}/follow-post-queries',
            data: {
            question_id: question_id,
                    follower_user_id: follower_user_id,
                    status: status
            },
            success: function(data) {
            $("#followPost").addClass("acti-up-dw");
            $("#followPost").css("cursor", "pointer");
            $('#up-down-votes').html(data);
            $('.grey-overlay-loader').hide();
            }
    });
    }

    function upvoteAnswerDownvote(answer_id, status) {

    $.ajax({
    type: "post",
            url: '{!!url("/")!!}/up-down-vote-answer',
            data: {
            answer_id:answer_id,
                    status:status
            },
            success: function(data) {
            document.location.reload(true);
            }
    })
    }
    $("#form_detail_question").validate({
    rules:
    {
    answer:
    {
    required: true,
    }

    },
            messages: {
            answer: {
            required: "Please enter the answer.",
            }
            },
    });
    $("#report").validate({
    rules: {
    report: {
    required: true,
    }

    },
            messages: {
            report: {
            required: "Please select one option.",
            }
            },
    });
    $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
    e.preventDefault();
    $(this).siblings('a.active').removeClass("active");
    $(this).addClass("active");
    var index = $(this).index();
    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    $("#select_group").click(function(){
    $("#sub_group").prop('disabled', false)
    })
            $("input[id=get_report]").click(function(e){
    e.stopPropagation();
    $("#sub_group").prop('disabled', true)
    })
            $("#make_group").click(function(){
    $("#make_sub_group").prop('disabled', false)
    })
            $("input[id=get_group_report]").click(function(e){
    e.stopPropagation();
    $("#make_sub_group").prop('disabled', true)
    })

    });
    function like(answer_id, status) {

    $.ajax({
    type: "get",
            url: '{!!url("/")!!}/like-answer/' + btoa(answer_id) + '/' + btoa(status),
            data: '',
            dataType: "json",
            success: function(data) {
            //                                console.log(data);
            //                            var attend = data.event_users_attend_count[0].user_count
            //                                    var maybe = data.event_users_maybe_count[0].user_count
            //                                    $("#maybe").html('Like' + '(' + maybe + ')')
            //                                    $("#att").html('Unlike' + '(' + attend + ')')
            if (status == 1)
            {
            $("#like").prop('disabled', false);
            $("#unlike").prop('disabled', true);
            }
            else
            {
            $("#unlike").prop('disabled', false);
            $("#like").prop('disabled', true);
            }
            }
    })
    }

    function addFollower(follower_user_id)
    {
    $.ajax({
    type: "get",
            url: '{!!url("/")!!}/make-follower',
            data: {follower_user_id: follower_user_id},
            dataType: "json",
            success: function(msg) {
            if (msg.success == 1)
            {
            $("#follower_count").html(msg.follower_count[0].follower)
                    $("#follow").text('Unfollow');
            $("#follow").prop('title', 'Click to Unfollow');
            } else{
            $("#follow").text('Follow');
            $("#follower_count").html(msg.follower_count[0].follower)
                    $("#follow").prop('title', 'Click to Follow');
            }

            }
    })


    }
    function addConnection(to_user_id)
    {
    $.get('{!!url("/")!!}/add-connection', {to_user_id: to_user_id}, function (msg) {

    if (msg == true)
    {
    $(".connection" + to_user_id).addClass('active');
    $(".connection" + to_user_id).html('<i class="fa fa-check"></i>');
    $(".connection" + to_user_id).prop('title', 'click to disconnect');
    } else{
    $(".connection" + to_user_id).removeClass('active');
    $(".connection" + to_user_id).html('<i class="icon-Add"></i>');
    $(".connection" + to_user_id).prop('title', 'click to connect');
    }

    });
    }
    /****for viewers******/

    $(document).ready(function() {

    $("#report").validate({
    rules: {
    report: {
    required: true,
    }

    },
            messages: {
            report: {
            required: "Please select one option.",
            }
            },
    });
    });
    function readURL(input) {
    var ext = input.value.match(/\.(.+)$/)[1];
    switch (ext){
    case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':
            if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    $('#blah').css("display", "block");
    $('#blah').prop('src', e.target.result);
    $('#remove_image').css("display", "block");
    $(input).attr('title', input.value)
    }

    reader.readAsDataURL(input.files[0]);
    }
    break;
    default:
            alert('Only Image allowed !');
    input.value = '';
    }

    }

    function check(article_id){
    if ($('#report').valid()){
    $('#report_ok').prop('data-dismiss', 'modal');
    $.ajax({
    type: "get",
            url: '{!!url("/")!!}/make-question-report/' + btoa(article_id),
            data: $("#report").serialize(),
            dataType: "json",
            success: function(data) {
            if (data.msg == '1'){
            $("#report_disabled").prop('disabled', true)
                    window.location.reload()
            }
            }
    })
    }
    }
    $("#group_report").validate({
    rules: {
    report: {
    required: true,
    }

    },
            messages: {
            report: {
            required: "Please select one option.",
            }
            },
    });
    function getGroupName(value){
    $("#make_sub_group option[value=" + value + "]").prop("selected", "selected");
    $('#myModal1').modal('show')
            $("#make_group").prop('checked', 'checked')
            $("#make_sub_group").prop('disabled', false)
    }

    function makeGroupReport(question_id){
    if ($('#group_report').valid()){
    $('#group_report_ok').prop('data-dismiss', 'modal');
    $.ajax({
    type: "get",
            url: '{!!url("/")!!}/make-question-group-report/' + btoa(question_id),
            data: $("#group_report").serialize(),
            dataType: "json",
            success: function(data) {
            if (data.msg == '1'){

            window.location.reload()
            }
            }
    })
    }

    }

</script>    
@include('Frontend.includes.post_detail_common_fx')    
</body>
</html>
