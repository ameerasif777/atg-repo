@include('Frontend.includes.head')
@include('Frontend.includes.header')
@include('Frontend.includes.session-message')
<?php
$session_data = Session::get('user_account');
?>

<link rel="stylesheet" type="text/css" href="{{url('/assets/Frontend/css/bootstrap-tokenfield.css')}}"/>
<link href="{{ url('/assets/Frontend/js/job/jquery.popSelect.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('/public/assets/css/views/signupinmodal.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url('/assets/Frontend/js/job/jquery.popSelect.js')}}"></script>

@include('Frontend.includes.froala-editor', ['folderName' => 'qrious_image'])

<style>
    .grey-overlay {
        background: rgba(0, 0, 0, 0.7);
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 9998;
    }

    .social-btn {
        display: block;
        height: 40px;
        width: 55px;
        border: 1px solid #cfebf6;
        color: #fff;
        line-height: 40px;
        border-radius: 4px;
        font-size: 16px;
        text-align: center;
        transition: all 0.3s linear 0s;
        -o-transition: all 0.3s linear 0s;
        -moz-transition: all 0.3s linear 0s;
        -webkit-transition: all 0.3s linear 0s;
        background-color: transparent;
    }

    .outer-counter {
        display: none;
        float: left;
        margin-left: 12px;
        background-color: #36bf8a;
        color: white;
        padding: 4px 0px 4px 6px;
        margin-top: 15px;
        padding-right: 8px;
        border-color: #d43f3a;
        vertical-align: middle;
        -webkit-transition: 0.25s linear;
        transition: 0.25s linear;
        height: 28px;
        border-radius: 50%;
    }

    @media only screen and (max-width: 500px) {
        .outer-counter {
            font-size: 12px;
            padding: 1px 0px 0px 3px;
            padding-right: 3px;
            height: 20px;
            margin-top: 30px;
        }
    }
</style>
<script>
    jQuery(document).ready(function () {
        $('#tags').tokenfield({
            autocomplete: {
                delay: 100
            },
            showAutocompleteOnFocus: true
        });

        $('.multipleSelect').popSelect({
            showTitle: false,
            autoIncrease: true,
            maxAllowed: 5,
            position: 'top',
            placeholderText: 'Please select the groups',
            note: 'Select a group to share with the community. <br>Do not select any group to share only with connections and followers.'
        });
    });
</script>
<section class="post-Meet-up-sec">
    <div class="container">
        <div class="text-feder-outer">
            <form name="form_post_question" id="form_post_question" method="post" action="{{url('/')}}/post-question"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" id="base_url" value="{{url('')}}">
                <input type="hidden" id="session_data" value="@if(isset($session_data[0])){{$session_data[0]}}@endif">
                <input type="hidden" id="group_id" value="@if(isset($group_id)){{$group_id}}@endif">
                <input type="hidden" id="socialicons" name="socialicons" value="">
                <div class="rightwer">
                    <ul class="clearfix">
                        <li class="active"><a href="javascript:void(0)"><i class="icon-Question"></i> Ask Question</a>
                        </li>
                        <li><a href="{{url('/')}}/event"><i class="icon-Post"></i> Post Event</a></li>
                        <li><a href="{{url('/')}}/article"><i class="icon-Post"></i> Post Article</a></li>
                        <li><a href="{{url('/')}}/meetup"><i class="icon-Post"></i> Post Meetup</a></li>
                        <li><a href="{{url('/')}}/education"><i class="icon-Post"></i> Post Education</a></li>
                        @if($arr_user_data['user_type'] != 1)
                            <li>
                                <a href="{{url('/')}}/post-job"><i class="icon-Post"></i> Post Job</a>
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="fealider-area">
                    <div class="row">
                        <div class="col-sm-11 col-xs-10">
                            <div class="title-input">
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Ask Question ?"
                                       value="{{old('title')}}" onkeydown="limitText(this.form.question,200);"
                                       onkeyup='limitText(this.form.question,200);' onclick="showForm();"
                                       onmouseleave="hideForm();">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('title') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-2">
                            <div class="outer-counter" id="counters">
                                <span>200</span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-8">
                            <textarea class="form-control" name="description" id="description"
                                      placeholder="Question Description" value="{{old('description')}}"
                                      style="display: none"></textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                            <div class="error" for="description"></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="uploar-imger">
                                <div class="uping-imgs uplode-img-cam">
                                    <input type="file" name='question_pic' onchange="readURL(this);" id="question_pic"
                                           class="upit-img" title="Upload Image"/>
                                </div>
                                <div for="profile_pic" class="error"></div>
                                <div class="note_msg">
                                    <strong>Upload a cover image for your post.</strong><br>
                                    jpeg, jpg, png, bmp, max size : 2 Mb. <br>
                                    <strong>(Rec Dimensions: 1200 X 400px)</strong>
                                </div>
                                <div class="title-input uplink-imgs">
                                    <img class="uploding-img-vie" style="display: none" id="blah" src="#" alt=""
                                         width="100%" height="200"/><a href="javascript:void(0)" style="display: none"
                                                                       onclick="removeimage();" id="remove_image"
                                                                       title="Remove"><i class="fa fa-remove"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="title-sl-input">
                            <label for="">TAGS</label>
                            <input type="text" class="form-control" id="tags" name="tags"
                                   placeholder="Type relevant tag(s). Press enter or comma after each tag to separate them."
                                   value="{{old('tags')}}">
                            @if ($errors->has('tags'))
                                <span class="help-block">
                                <strong class="text-danger">{{ $errors->first('tags') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div for="tags" class="error"></div>
                        <br/>
                        <div class="title-sl-input">
                            <label for="">GROUP</label>
                            @if($session_data!="")
                                <select name="sub_groups[]" id="sub_groups[]" multiple=""
                                        class="form-control multipleSelect" placeholder="Groups"
                                        value="{{old('sub_groups[]')}}" size="{{count($arr_users_group)}}">
                                @if(isset($group_id) && $group_id != '')
                                    @foreach($arr_users_group as $key => $value)
                                        @if($value->id == $group_id)
                                            <option value="{{$group_id}}" selected>{{$value->group_name}}</option>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach($arr_users_group as $key => $value)

                                        <option value="{{$value->id}}">{{$value->group_name}}</option>
                                        @endforeach
                                        @endif
                                        </select>
                                        @else
                                            @if(isset($group_id) && $group_id != '')
                                                <select name="sub_groups[]" id="sub_groups[]" multiple=""
                                                        class="form-control multipleSelect" placeholder="Groups"
                                                        value="{{old('sub_groups[]')}}" size="{{count($arr_users_group)}}
                                                ">

                                                @foreach($arr_users_group as $key => $value)
                                                    @if($value->id == $group_id)
                                                        <option value="{{$group_id}}"
                                                                selected>{{$value->group_name}}</option>
                                                        @endif
                                                        @endforeach
                                                        </select>
                                                    @else
                                                        <input type="text" class="form-control" data-toggle="modal"
                                                               data-target="#myModal1"
                                                               placeholder="Please select the groups">
                                                    @endif
                                                    @endif
                                                    <div for="sub_groups[]" class="error"></div>
                        </div>
                        <div class="boter-texter-feat clearfix">
                            <div class="pull-left" id="save_loader" style="display:none;"><img height="25px"
                                                                                               width="25px"
                                                                                               src="{{ url('/public/assets/Frontend/img/loading1.gif') }}">
                                <b>Your post is saving...</b></div>
                            <div class="pull-right">
                                <input type="hidden" id="last_id" name="id">
                                <input type="hidden" id="auto_save_flag" name="auto_save_flag" value="false">
                                <input type="hidden" id="btn_post" name="btn_post" value="">
                                
                                <input type="hidden" id="cover_type" name="cover_type" value="">
                                <input type="hidden" id="cover_url" name="cover_url" value="">

                                @if($session_data != "")

                                    <button type="submit" class="btn cancel"
                                            onclick="$('#btn_post').val('1'); getDescription()">Save & Close
                                    </button>
                                    <button type="submit" class="btn"
                                            onclick="$('#btn_post').val('0'); getDescription()" onmouseover="findimginpost()">Post
                                    </button>
                                @else
                                    <button id="permJoin" name="permJoin" title="Post" class="masss-btn btn"
                                            type="button" data-toggle="modal" data-target="#myModal"
                                            onclick="$('#btn_post').val('0');"><i class="icon-Message"></i> Post
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="models-cust">
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="main-log-tab">
                                        <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                                            <ul role="tablist" class="nav nav-tabs" id="myTabs">
                                                <li role="presentation"><a aria-expanded="true" aria-controls="log-in"
                                                                           data-toggle="tab" role="tab" id="log-in-tab"
                                                                           href="#log-in"
                                                                           onclick="$('#posttype').val('1');">SIGN
                                                        IN</a></li>
                                                <li class="active cust-devider" role="presentation"><a
                                                            aria-controls="reg-in" data-toggle="tab" id="reg-in-tab"
                                                            role="tab" href="#reg-in"
                                                            onclick="$('#posttype').val('0');">SIGN UP</a></li>
                                            </ul>


                                            <div class="tab-content" id="myTabContent">
                                                <div aria-labelledby="log-in-tab" id="log-in" class="tab-pane fade"
                                                     role="tabpanel">
                                                    {{--<form id="frm_login" name="frm_login" action="{{url('/signin')}}" method="post">--}}
                                                    {!!csrf_field()!!}
                                                    <div class="inner-socials">
                                                        <div class="sol-head">Sign In with social</div>
                                                        <div class="cust-solc">
                                                            <ul class="clearfix">
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('f');"><i
                                                                                class="fa fa-facebook-f"></i></button>
                                                                    {{--<a href="#" onclick="$('#socialicons').val('f');"><i--}}
                                                                    {{--class="fa fa-facebook-f"></i></a>--}}
                                                                </li>
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('g');"><i
                                                                                class="fa fa-google-plus"></i></button>
                                                                    {{--<a href="{{url('/gmail')}}"><i--}}
                                                                    {{--class="fa fa-google-plus"></i></a>--}}
                                                                </li>
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('t');"><i
                                                                                class="fa fa-twitter"></i></button>
                                                                    {{--<a href="{{url('/twitter')}}"><i--}}
                                                                    {{--class="fa fa-twitter"></i></a>--}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="or-head">Or</div>
                                                    </div>
                                                    <div class="loger-form">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="email"
                                                                   name="email"
                                                                   value="{{ (isset($cookie_data['email'])) ? $cookie_data['email'] : '' }}"
                                                                   placeholder="Email address">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" id="password" name="password"
                                                                   class="form-control"
                                                                   value="{{ (isset($cookie_data['password'])) ? $cookie_data['password'] : '' }}"
                                                                   placeholder="Password">
                                                        </div>
                                                    </div>

                                                    <div class="forg-remi clearfix">
                                                        <div class="reminder-me">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" id="remember" name="remember"
                                                                           <?php if (!empty($cookie_data)) { ?>checked="checked" <?php } ?>>Remember
                                                                    me
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="forgot-pass"><a href="javascript:void(0);"
                                                                                    onClick="getEmailId();"
                                                                                    data-toggle="modal"
                                                                                    data-target="#myForgotModal">Reset
                                                                Password</a></div>
                                                    </div>
                                                    <div class="btns-loger"><input type="hidden" id="redirect_uri"
                                                                                   name="redirect_uri"
                                                                                   value="{{ Request::path() }}">
                                                        <button type="submit" class="btn log-btn">Sign in</button>
                                                    </div>
                                                    {{--</form>--}}
                                                </div>
                                                <div aria-labelledby="reg-in-tab" id="reg-in"
                                                     class="tab-pane fade in active" role="tabpanel">
                                                    <div class="inner-socials">
                                                        <div class="sol-head">Sign Up with social</div>
                                                        <div class="cust-solc">
                                                            <ul class="clearfix">
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('f');"><i
                                                                                class="fa fa-facebook-f"></i></button>
                                                                    {{--<a href="#" onclick="$('#socialicons').val('f');"><i--}}
                                                                    {{--class="fa fa-facebook-f"></i></a>--}}
                                                                </li>
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('g');"><i
                                                                                class="fa fa-google-plus"></i></button>
                                                                    {{--<a href="{{url('/gmail')}}"><i--}}
                                                                    {{--class="fa fa-google-plus"></i></a>--}}
                                                                </li>
                                                                <li>
                                                                    <button type="submit" class="social-btn"
                                                                            onclick="$('#socialicons').val('t');"><i
                                                                                class="fa fa-twitter"></i></button>
                                                                    {{--<a href="{{url('/twitter')}}"><i--}}
                                                                    {{--class="fa fa-twitter"></i></a>--}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="or-head">Or</div>
                                                    </div>
                                                    <?php Session::put('ruser', Request::get('r', '')); ?>
                                                    {{--<form id="frm_user_registration1" name="frm_user_registration1" method="POST" action="{{url('/signup1?r=' . Request::get('r'))}}"  enctype="multipart/form-data">--}}
                                                    {!!csrf_field()!!}
                                                    <input type="hidden" name="posttype" id="posttype" value="0">
                                                    <div class="loger-form">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="first_name"
                                                                   name="first_name" placeholder="First Name"
                                                                   value="{{old('first_name')}}">
                                                            @if ($errors->has('first_name'))
                                                                <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="last_name"
                                                                   name="last_name" placeholder="Last Name"
                                                                   value="{{old('last_name')}}">
                                                            @if ($errors->has('last_name'))
                                                                <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" id="user_email"
                                                                   name="user_email" placeholder="Email Address"
                                                                   value="{{old('user_email')}}">
                                                            @if ($errors->has('user_email'))
                                                                <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_email') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" class="form-control"
                                                                   id="user_password" name="user_password"
                                                                   placeholder="password"
                                                                   value="{{old('user_password')}}">
                                                            <!--<div class="reg-info">(Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters)</div>-->
                                                            @if ($errors->has('user_password'))
                                                                <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_password') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" class="form-control"
                                                                   id="cnf_user_password" name="cnf_user_password"
                                                                   placeholder="Confirm Password">
                                                            @if ($errors->has('cnf_user_password'))
                                                                <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('cnf_user_password') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="reg-info">By clicking on the 'Sign up' button you agree
                                                        to the <a href="{{url('/cms/terms-services')}}"> Terms and
                                                            conditions </a> and our<a href="{{url('/cms/privacy')}}">
                                                            privacy policy</a></div>

                                                    <div class="btns-loger">

                                                        <button type="submit" name="btn_register" id="btn_register"
                                                                class="btn log-btn">Sign up
                                                        </button>
                                                    </div>
                                                    {{--</form>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<div class="models-cust">
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="main-log-tab">
                        <div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
                            <ul role="tablist" class="nav nav-tabs" id="myTabs">
                                <li class="active cust-devider" role="presentation"><a aria-controls="log-in"
                                                                                       data-toggle="tab" role="tab"
                                                                                       id="log-in-tab1" href="#log-in1">SIGN
                                        IN</a></li>
                                <li role="presentation"><a aria-expanded="true" aria-controls="reg-in" data-toggle="tab"
                                                           id="reg-in-tab1" role="tab" href="#reg-in1">SIGN UP</a></li>
                            </ul>


                            <div class="tab-content" id="myTabContent">
                                <div aria-labelledby="log-in-tab1" id="log-in1" class="tab-pane fade in active"
                                     role="tabpanel">
                                    <form id="frm_login" name="frm_login" action="{{url('/signin')}}" method="post">
                                        {!!csrf_field()!!}
                                        <div class="inner-socials">
                                            <div class="sol-head">Sign In with social</div>
                                            <div class="cust-solc">
                                                <ul class="clearfix">
                                                    <li><a href="{{url('/facebook')}}"><i class="fa fa-facebook-f"></i></a>
                                                    </li>
                                                    <li><a href="{{url('/gmail')}}"><i
                                                                    class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="{{url('/twitter')}}"><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="or-head">Or</div>
                                        </div>
                                        <div class="loger-form">
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="email" name="email"
                                                       value="{{ (isset($cookie_data['email'])) ? $cookie_data['email'] : '' }}"
                                                       placeholder="Email address">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="password" name="password"
                                                       class="form-control"
                                                       value="{{ (isset($cookie_data['password'])) ? $cookie_data['password'] : '' }}"
                                                       placeholder="Password">
                                            </div>
                                        </div>

                                        <div class="forg-remi clearfix">
                                            <div class="reminder-me">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="remember" name="remember"
                                                               <?php if (!empty($cookie_data)) { ?>checked="checked" <?php } ?>>Remember
                                                        me
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="forgot-pass"><a href="javascript:void(0);"
                                                                        onClick="getEmailId();" data-toggle="modal"
                                                                        data-target="#myForgotModal">Reset Password</a>
                                            </div>
                                        </div>
                                        <div class="btns-loger"><input type="hidden" id="redirect_uri"
                                                                       name="redirect_uri"
                                                                       value="{{ Request::path() }}">
                                            <button type="submit" class="btn log-btn">Sign in</button>
                                        </div>
                                    </form>
                                </div>
                                <div aria-labelledby="reg-in-tab1" id="reg-in1" class="tab-pane fade" role="tabpanel">
                                    <div class="inner-socials">
                                        <div class="sol-head">Sign Up with social</div>
                                        <div class="cust-solc">
                                            <ul class="clearfix">
                                                <li><a href="{{url('/facebook')}}"><i class="fa fa-facebook-f"></i></a>
                                                </li>
                                                <li><a href="{{url('/gmail')}}"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                                <li><a href="{{url('/twitter')}}"><i class="fa fa-twitter"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="or-head">Or</div>
                                    </div>
                                    <?php Session::put('ruser', Request::get('r', '')); ?>
                                    <form id="frm_user_registration1" name="frm_user_registration1" method="POST"
                                          action="{{url('/signup1?r=' . Request::get('r'))}}"
                                          enctype="multipart/form-data">
                                        {!!csrf_field()!!}
                                        <div class="loger-form">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="first_name"
                                                       name="first_name" placeholder="First Name"
                                                       value="{{old('first_name')}}">
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="last_name" name="last_name"
                                                       placeholder="Last Name" value="{{old('last_name')}}">
                                                @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="user_email"
                                                       name="user_email" placeholder="Email Address"
                                                       value="{{old('user_email')}}">
                                                @if ($errors->has('user_email'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" id="user_password"
                                                       name="user_password" placeholder="password"
                                                       value="{{old('user_password')}}">
                                                <!--<div class="reg-info">(Password must be combination of atleast 1 number, 1 special character and 1 upper case letter with minimum 8 characters)</div>-->
                                                @if ($errors->has('user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('user_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" id="cnf_user_password"
                                                       name="cnf_user_password" placeholder="Confirm Password">
                                                @if ($errors->has('cnf_user_password'))
                                                    <span class="help-block">
                                                        <strong class="text-danger">{{ $errors->first('cnf_user_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="reg-info">By clicking on the 'Sign up' button you agree to the <a
                                                    href="{{url('/cms/terms-services')}}"> Terms and conditions </a> and
                                            our<a href="{{url('/cms/privacy')}}"> privacy policy</a></div>

                                        <div class="btns-loger">

                                            <button type="submit" name="btn_register" id="btn_register"
                                                    class="btn log-btn">Sign up
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('Frontend.includes.footer')

<script src="{{url('/assets/Backend/js/jquery-ui.js')}}"></script>
<script src="{{url('/assets/Frontend/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{url('/assets/Frontend/js/imgvideoaudioext.js')}}"></script>
<script>
    setInterval(function () {
        var title = $("#title").val();
        if (title != '') {
            autoSave();
        }
    }, 10000);

    $(document).ready(function (e) {

        var group = $('#group_id').val();
        var session_data = $('#session_data').val();
        if (group != '') {
            $('#myModal1').remove();
            if (session_data == "") {
                $('.popSelect-close').css({'display': 'none'});
            }
        } else {
            $('#myModal').remove();
        }
        $("#form_post_question").validate({
            errorElement: 'div',
            ignore: '.hidden',
            rules: {
                title: {
                    required: true,
                },
                post_short_description: {
                    required: true,
                    maxlength: 200
                },
                description: {
                    required: true,
                },
            },
            messages: {
                title: {
                    required: "Please enter question.",
                    remote: "Question with same name already exist.",
                },
                post_short_description: {
                    required: "Please enter question short description.",
                    maxlength: "Please enter maximum 200 charecter."

                },
                description: {
                    required: "Please enter question description.",
                },
                submitHandler: function (form) {
                    $("#btn_post").hide();
                }
            },
        });
    });
    function autoSave() {
        var id = $("#last_id").val();
        var sub_groups = $("#sub_groups").val();
        $.ajax({
            url: $("#base_url").val() + "/post-question",
            method: 'post',
            data: {
                id: id,
                sub_groups: sub_groups,
                title: $("#title").val(),
                description: $('#question_decription').html(),
                tags: $("#tags").val(),
                btn_post: '1',
                auto_save_flag: "true",
                cover_type: $('#cover_type').val(),
                cover_url:$('#cover_url').val()
            },
            success: function (id) {
                $('#last_id').val('');
                $("#last_id").prop("value", id);
                $("#save_loader").show();
                setInterval(function () {
                    $("#save_loader").hide();
                }, 4000);
            }
        });
    }
    function getDescription() {
        $('#description').val($('textarea#description').froalaEditor('html.get'));
    }

    function readURL(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah').css("display", "block");
                        $('#blah').attr('src', e.target.result);
                        $('#remove_image').css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }

    }
    function readImage(input) {
        var ext = input.value.match(/\.(\w+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'bmp':
            case 'png':
            case 'tif':
            case 'jpeg':

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#blah1').css("display", "block");
                        $('#blah2').css("display", "block");
                        $('#blah3').css("display", "block");
                        $('#blah1').attr('src', e.target.result);
                        $('#remove_img').css("display", "block");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
                break;
            default:
                alert('Image allowed should be of extensions : jpg, bmp, png, jpeg');
                input.value = '';
        }

    }
    function removeimage() {
        $('#blah').css("display", "none");
        $('#remove_image').css("display", "none");
        $("#article_pic").prop("value", "");
    }
    function removeimg() {
        $('#blah1').css("display", "none");
        $('#blah2').css("display", "none");
        $('#blah3').css("display", "none");
        $('#remove_img').css("display", "none");
        $("#profile_image").prop("value", "");
    }
    function show() {
        $("#enddatediv").css("display", "block");
        $("#add_link").css("display", "none");
    }
    function hide() {
        $("#enddatediv").css("display", "none");
        $("#add_link").css("display", "block");
    }

    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            $('#counters').html('<span>0</span>');
            limitField.value = limitField.value.substring(0, limitNum);
            $('.title-input input').css({'border-bottom': '1px solid #e50505'});
            return false;
        } else {
            var v = limitNum - limitField.value.length;
            $('#counters').html('<span>' + v + '</span>');
            $('.title-input input').css({'border-bottom': '1px solid #66afe9'});
        }
        $('#counters').css({'display': 'block'});
    }
    function showForm() {
        $('#counters').css({'display': 'block'});
    }

    function hideForm() {
        $('#counters').css({'display': 'none'});
    }

    document.addEventListener('keydown', function (e) {
        if (e.keyCode === 9) {
            hideForm();
        }
    });


    {{--function donetyping(callback,timeout){--}}
    {{--timeout = timeout || 1e3; // 1 second default timeout--}}
    {{--var timeoutReference,--}}
    {{--doneTyping = function(el){--}}
    {{--if (!timeoutReference) return;--}}
    {{--timeoutReference = null;--}}
    {{--callback.call(el);--}}
    {{--};--}}
    {{--return this.each(function(i,el){--}}
    {{--var $el = $(el);--}}
    {{--// Chrome Fix (Use keyup over keypress to detect backspace)--}}
    {{--// thank you @palerdot--}}
    {{--$el.is(':input') && $el.on('keyup keypress paste',function(e){--}}
    {{--// This catches the backspace button in chrome, but also prevents--}}
    {{--// the event from triggering too preemptively. Without this line,--}}
    {{--// using tab/shift+tab will make the focused element fire the callback.--}}
    {{--if (e.type=='keyup' && e.keyCode!=8) return;--}}

    {{--// Check if timeout has been set. If it has, "reset" the clock and--}}
    {{--// start over again.--}}
    {{--if (timeoutReference) clearTimeout(timeoutReference);--}}
    {{--timeoutReference = setTimeout(function(){--}}
    {{--// if we made it here, our timeout has elapsed. Fire the--}}
    {{--// callback--}}
    {{--doneTyping(el);--}}
    {{--}, timeout);--}}
    {{--}).on('blur',function(){--}}
    {{--// If we can, fire the event since we're leaving the field--}}
    {{--doneTyping(el);--}}
    {{--});--}}
    {{--}--}}
</script>
