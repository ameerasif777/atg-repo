<div class="modal fade" id="institute_login_modal" tabindex="-1" role="dialog" aria-labelledby="institute_login_modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="institute_login_modalLabel">Log in to your account</h4>
            </div>
            <form method="POST" name="frm_institute_login" id="frm_institute_login" action="{{url('/')}}/auth/institute-login">
                <div class="modal-body">
                    @if(Session::has('login_error'))
                    <p class="alert alert-info">{{ Session::get('login_error') }}<a class="close">&times;</a></p>
                    @endif
                    {{csrf_field()}}
                    <div class="form-group row clearfix">
                        <div class="col-md-6">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Syncampus ID"/>
                        </div>
                        <div class="col-md-6">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox2" value="option2"> <span class="rembr-me">Remember Me</span>
                            </label>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="#" class="forget-pass">Forget Password</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer model-login">
                    <button type="submit" class="btn sign-up-btn" id="btn_submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

