<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{url('/')}}/assets/Frontend/js/registration/institute-registration.js"></script>
<script src="{{url('/')}}/assets/Backend/js/jquery.validate.min.js"></script>
<script src="{{url('/')}}/assets/Backend/js/jquery.validate.js"></script>
<script src="{{url('/')}}/assets/Backend/js/jquery.validate.password.js"></script>
<script>
$(document).ready(function (e) {
    $("#state_id").change(function () {

        var val = $(this).val();

        $.ajax({
            url: "{{url('/')}}/admin/add-institute/get-city",
            type: "get",
            data: {state_id: val
            },
            success: function (res)
            {
                $("#city_result").html(res);
            }
        });
    });
    $("#state_id").trigger("change");
});
</script>

@if(Session::has('login_error'))
{{Session::get('login_error')}}
@elseif(Session::has('message'))
{{Session::get('message')}}
@else
{{Session::get('admin_logout')}}
@endif


<section class="register-page cust-registration">
    <div class="container">
        <div class="page-ttl text-center">
            <h3 class="relative">SIGN UP</h3>
        </div>
        <div class="row col-sm-6">
            <form id="frm_institute_registration" name="frm_institute_registration" method="POST" action="{{url('/')}}/auth/institute-register">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-user"></i>
                            <input type="text" class="form-control" id="institute_name" name="institute_name" autofocus placeholder="Institute Name">                                            
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-user"></i>
                            <select class="form-control" name="institute_type" id="institute_type">
                                <option>Institute Type</option>
                                <option value="1">University</option>
                                <option value="2">College</option>
                                <option value="3">Private Institutes</option>

                            </select>
                        </div>
                    </div>




                    <div class="col-xs-12">
                        <div class="select-gender row">
                            <div class="col-xs-12 ">
                                <textarea class="form-control" name="institue_address" id="institue_address" rows="3" placeholder="Address"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-5">
                            <div class="form-control-div">
                                <label for="parametername">State Name<sup class="mandatory">*</sup></label>
                                <select id="state_id" name="state_id" class="form-control">
                                    <option value="">--Select State--</option>

                                    @foreach ($state_details as $row) 
                                    <option value="{{$row['id']}}">{{$row['state_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-5">
                            <div class="form-control-div">
                                <label for="city">City <sup class="mandatory">*</sup></label>
                                <div id="city_result">
                                    <select class="form-control" name='city_id' id='city_id'>
                                        <option value="">--Select City--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <input type="text" class="form-control" placeholder="Email address" name="user_email" id="user_email">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <input type="text" class="form-control" placeholder="Contact person name" name="contact_person_name" id="contact_person_name">
                        </div>
                    </div>
                    <input type="hidden" name="base_url" id="base_url" value="{{url('/')}}">
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <input type="text" class="form-control" placeholder="Contact number" name="contact_number" id="contact_number">
                        </div>
                    </div>



                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Enter password" name="user_password" id="user_password">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Confirm password" name="cnf_user_password" id="cnf_user_password">
                        </div>
                    </div>
                    <div class="col-xs-12 agree-terms">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms" id="terms"> I agree to the <a href="{{ url('/') }}/cms/terms-condition" target="_new">Terms and conditions</a>
                            </label>
                        </div>
                        <div class="error" for="terms" generated="true"></div>
                        <!--						<div class="checkbox">
                                                                                <label>
                                                                                        <input type="checkbox" name="subscription_for_news_letter" id="subscription_for_news_letter" value="1"> Subscribe for newsletter
                                                                                </label>
                                                                        </div>-->
                    </div>

                    <div class="btn-grp offset-top-15">
                        <input type="submit" value="Register" class="btn btn-blue" name="btn_register" id="btn_register"> 
                        <a href="{{ url('/')}}" class="btn btn-link">Cancel</a>
                        <img id="btn_loader" style="display: none;" src="{{ url('/') }}/assets/Frontend/img/wait_icon.gif" border="0">
                    </div>
            </form>
        </div>
    </div>
</section>
</div>