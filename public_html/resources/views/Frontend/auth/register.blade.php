<!--@extends('Frontend.layouts.default')
@section('content')  -->
<section class="register-page cust-registration">
    <div class="container">
        <div class="page-ttl text-center">
            <h3 class="relative">SIGN UP</h3>
        </div>
        <div class="reg-inn">
            <form id="frm_user_registration" name="frm_user_registration" method="POST" action="{{url('/')}}/auth/register">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <select class="form-control" name="user_type" id="user_type" onclick="change_user();">
                                <option value="">Select Type</option>
                                <option value="1">Student</option>
                                <option value="3">General User</option>
                                <option value="4">College/Institute</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <input type="text" class="form-control" placeholder="Email address" name="user_email" id="user_email">
                        </div>
                    </div>
                    <div id="student">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-control-div">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" id="first_name" name="first_name" autofocus placeholder="First name">                                            
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-control-div">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" placeholder="Last name" name="last_name" id="last_name">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-control-div">
                                <i class="fa fa-calendar-o"></i>
                                <input type="text" style="cursor: default;" readonly="" class="form-control datepicker" placeholder="Birth date" name="birth_date" id="birth_date">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="select-gender row">
                                <label class="col-xs-12 col-sm-3">Gender</label>
                                <div class="col-xs-12 col-sm-9">
                                    <div class="radio-inline"><label><input type="radio" name="gender" id="gender" checked="" value="1"> Male</label></div>
                                    <div class="radio-inline"><label><input type="radio" name="gender" id="gender" value="2"> Female</label></div>
                                </div>
                            </div>
                        </div>
                    </div>   

                    <div id="general_user" style="display: none;">
                        <div class="col-xs-12 col-sm-12">
                            <div class="form-control-div">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" id="name" name="name" autofocus placeholder="Name">                                            
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div class="form-control-div">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" placeholder="Address" name="address" id="address">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-control-div">
                                <i class="fa fa-envelope"></i>
                                <input type="text" class="form-control" placeholder="Field of expertise" name="field_of_expertise" id="field_of_expertise">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-control-div">
                                <i class="fa fa-envelope"></i>
                                <input type="text" class="form-control" placeholder="Experience" name="experience" id="experience">
                            </div>
                        </div>
                    </div>  
                    <div id="college" style="display: none;">
                        <div class="col-xs-12">
                            <div class="form-control-div">
                                <i class="fa fa-navicon"></i>
                                <input type="text" class="form-control" placeholder="College / institute name" name="college_name" id="college_name">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Enter password" name="user_password" id="user_password">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Confirm password" name="cnf_user_password" id="cnf_user_password">
                        </div>
                    </div>
                    <div class="col-xs-12 agree-terms">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms" id="terms"> I agree to the <a href="{{ url('/') }}/cms/terms-condition" target="_new">Terms and conditions</a>
                            </label>
                        </div>
                        <div class="error" for="terms" generated="true"></div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="subscription_for_news_letter" id="subscription_for_news_letter" value="1"> Subscribe for newsletter
                            </label>
                        </div>
                    </div>
                </div>
                <div class="btn-grp offset-top-15">
                    <input type="submit" value="Register" class="btn btn-blue" name="btn_register" id="btn_register"> 
                    <a href="{{ url('/')}}" class="btn btn-link">Cancel</a>
                    <img id="btn_loader" style="display: none;" src="{{ url('/') }}/assets/Frontend/img/wait_icon.gif" border="0">
                </div>
            </form>
        </div>
    </div>
</section>
</div>

<script>
    function change_user() {
        user_type = $("#user_type").val();
        if (user_type == '1') {
            $("#student").show();
            $("#college").hide();
            $("#general_user").hide();
        } else if (user_type == '3') {
            $("#general_user").show();
            $("#college").hide();
            $("#student").hide();
        } else if (user_type == '4') {
            $("#college").show();
            $("#student").hide();
            $("#general_user").hide();
        }
    }
</script>