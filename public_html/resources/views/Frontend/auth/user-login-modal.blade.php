



           @if(Session::has('message_error'))
                <p class="alert alert-info">{{ Session::get('message_error') }}<a class="close">&times;</a></p>
                @endif
            <form method="POST" name="frm_user_login" class="log-form" id="frm_user_login" action="{{url('/')}}/auth/login">
                
                {{csrf_field()}}
                <div class="modal-body">
                    <!--<form class="log-form">-->
                    <div class="form-group row clearfix">
                        <div class="col-md-6">
                            <input autofocus="" type="email" name="email" id="email" class="form-control" placeholder="" value="<?php if ((Session::has('email'))) echo $email; ?>"/>
                            <input type="hidden" name="hidden_email" id="email" class="form-control"/>
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="col-md-6">
                            <input type="password" name="password" id="password" class="form-control" placeholder="" value="<?php if (isset($_COOKIE['password'])) echo $_COOKIE['password']; ?>"/>
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="remember" name="remember" value="1" <?php if (isset($_COOKIE['email'])) echo 'checked=""'; ?>> <span class="rembr-me">Remember Me</span>
                            </label>

                        </div>
                        <div class="col-md-6 text-right">

                            <a href="javascript:void(0);"  class="forget-pass">Reset Password</a>
                        </div>
                    </div>
                    <!--</form>-->
                </div>

                <div class="modal-footer model-login">

                    <button type="submit" class="btn sign-up-btn"  >Login</button>
                </div>
            </form>

    

