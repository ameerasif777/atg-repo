<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{url('/')}}assets/Frontend/js/registration/user-registration.js"></script>
<script src="{{url('/')}}assets/Backend/js/jquery.validate.min.js"></script>
<script src="{{url('/')}}assets/Backend/js/jquery.validate.js"></script>
<script src="{{url('/')}}assets/Backend/js/jquery.validate.password.js"></script>

@if(Session::has('login_error'))
{{Session::get('login_error')}}
@elseif(Session::has('message'))
{{Session::get('message')}}
@else
{{Session::get('admin_logout')}}
@endif


<section class="register-page cust-registration">
    <div class="container">
        <div class="page-ttl text-center">
            <h3 class="relative">SIGN UP</h3>
        </div>
        <div class="row col-sm-6">
            <form id="frm_user_registration" name="frm_user_registration" method="POST" action="{{url('/')}}/auth/user-register">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-control-div">
                            <i class="fa fa-user"></i>
                            <input type="text" class="form-control" id="first_name" name="first_name" autofocus placeholder="First name">                                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-control-div">
                            <i class="fa fa-user"></i>
                            <input type="text" class="form-control" placeholder="Last name" name="last_name" id="last_name">
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="select-gender row">
                            <label class="col-xs-12 col-sm-3">Gender</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="radio-inline"><label><input type="radio" name="gender" id="gender" checked="" value="1"> Male</label></div>
                                <div class="radio-inline"><label><input type="radio" name="gender" id="gender" value="2"> Female</label></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-envelope"></i>
                            <input type="text" class="form-control" placeholder="Email address" name="user_email" id="user_email">
                        </div>
                    </div>


                    <input type="hidden" name="base_url" id="base_url" value="{{url('/')}}">
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Enter password" name="user_password" id="user_password">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-control-div">
                            <i class="fa fa-lock"></i>
                            <input type="password" class="form-control" placeholder="Confirm password" name="cnf_user_password" id="cnf_user_password">
                        </div>
                    </div>
                    <div class="col-xs-12 agree-terms">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms" id="terms"> I agree to the <a href="{{ url('/') }}/cms/terms-condition" target="_new">Terms and conditions</a>
                            </label>
                        </div>

                        <div class="error" for="terms" generated="true"></div>
                    </div>
                    <div class="btn-grp offset-top-15">
                        <input type="submit" value="Register" class="btn btn-blue" name="btn_register" id="btn_register"> 
                        <a href="{{ url('/')}}" class="btn btn-link">Cancel</a>
                        <img id="btn_loader" style="display: none;" src="{{ url('/') }}/assets/Frontend/img/wait_icon.gif" border="0">
                    </div>
                </div>
            </form>
        </div>
</section>
</di